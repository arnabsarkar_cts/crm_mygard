<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Ports that the Company covers.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>Comments__c</fullName>
        <description>Correspondent Comments</description>
        <externalId>false</externalId>
        <inlineHelpText>Correspondent Comments placed before the company name</inlineHelpText>
        <label>Comments</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Company__c</fullName>
        <externalId>false</externalId>
        <label>Company</label>
        <referenceTo>Account</referenceTo>
        <relationshipName>Covering_Ports</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Hull__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Correspondent Hull</description>
        <externalId>false</externalId>
        <inlineHelpText>Correspondent Hull</inlineHelpText>
        <label>Hull</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Order__c</fullName>
        <description>Order on the Correspondent List</description>
        <externalId>false</externalId>
        <inlineHelpText>Order on the Correspondent List</inlineHelpText>
        <label>Order</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1</fullName>
                    <default>true</default>
                    <label>1</label>
                </value>
                <value>
                    <fullName>2</fullName>
                    <default>false</default>
                    <label>2</label>
                </value>
                <value>
                    <fullName>3</fullName>
                    <default>false</default>
                    <label>3</label>
                </value>
                <value>
                    <fullName>4</fullName>
                    <default>false</default>
                    <label>4</label>
                </value>
                <value>
                    <fullName>5</fullName>
                    <default>false</default>
                    <label>5</label>
                </value>
                <value>
                    <fullName>6</fullName>
                    <default>false</default>
                    <label>6</label>
                </value>
                <value>
                    <fullName>7</fullName>
                    <default>false</default>
                    <label>7</label>
                </value>
                <value>
                    <fullName>8</fullName>
                    <default>false</default>
                    <label>8</label>
                </value>
                <value>
                    <fullName>9</fullName>
                    <default>false</default>
                    <label>9</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>P_I__c</fullName>
        <defaultValue>true</defaultValue>
        <description>Correspondent P&amp;I</description>
        <externalId>false</externalId>
        <inlineHelpText>Correspondent P&amp;I</inlineHelpText>
        <label>P&amp;I</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Port__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Port</label>
        <referenceTo>Port__c</referenceTo>
        <relationshipLabel>Correspondents in Port</relationshipLabel>
        <relationshipName>Covering_Ports</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Start_Date_Hull__c</fullName>
        <defaultValue>Today()</defaultValue>
        <description>Start Date Hull Correspondent</description>
        <externalId>false</externalId>
        <inlineHelpText>Start Date Hull Correspondent</inlineHelpText>
        <label>Start Date Hull</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Start_Date_P_I__c</fullName>
        <defaultValue>Today()</defaultValue>
        <description>Start Date P&amp;I Correspondent</description>
        <externalId>false</externalId>
        <inlineHelpText>Start Date P&amp;I Correspondent</inlineHelpText>
        <label>Start Date P&amp;I</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Stop_Date_Hull__c</fullName>
        <description>Stop Date Hull Correspondent</description>
        <externalId>false</externalId>
        <inlineHelpText>Stop Date Hull Correspondent</inlineHelpText>
        <label>Stop Date Hull</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Stop_Date_P_I__c</fullName>
        <description>Stop Date P&amp;I Correspondent</description>
        <externalId>false</externalId>
        <inlineHelpText>Stop Date P&amp;I Correspondent</inlineHelpText>
        <label>Stop Date P&amp;I</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Correspondent in Port</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CREATED_DATE</columns>
        <columns>OBJECT_ID</columns>
        <columns>CREATEDBY_USER</columns>
        <columns>Port__c</columns>
        <columns>Company__c</columns>
        <columns>Order__c</columns>
        <columns>Comments__c</columns>
        <columns>P_I__c</columns>
        <columns>Start_Date_P_I__c</columns>
        <columns>Stop_Date_P_I__c</columns>
        <columns>Hull__c</columns>
        <columns>Start_Date_Hull__c</columns>
        <columns>Stop_Date_Hull__c</columns>
        <columns>UPDATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>{0}</displayFormat>
        <label>Correspondent Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Correspondents in Port</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Company__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Port__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Comments__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Order__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>P_I__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Start_Date_P_I__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Stop_Date_P_I__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Hull__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Start_Date_Hull__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Stop_Date_Hull__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Company__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Port__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Comments__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Order__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>P_I__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Start_Date_P_I__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Stop_Date_P_I__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Hull__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Start_Date_Hull__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Stop_Date_Hull__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Company__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Port__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Comments__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Order__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>P_I__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Start_Date_P_I__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Stop_Date_P_I__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Hull__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Start_Date_Hull__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Stop_Date_Hull__c</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>Company__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Port__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Comments__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Order__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>P_I__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Start_Date_P_I__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Stop_Date_P_I__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Hull__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Start_Date_Hull__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Stop_Date_Hull__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Order_must_be_entered</fullName>
        <active>true</active>
        <description>A Correspondent in Port must have an order number assigned.</description>
        <errorConditionFormula>ISPICKVAL(Order__c, &quot;&quot;)</errorConditionFormula>
        <errorDisplayField>Order__c</errorDisplayField>
        <errorMessage>Order must be entered.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
