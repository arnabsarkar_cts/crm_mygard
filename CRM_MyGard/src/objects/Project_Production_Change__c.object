<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>true</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Approved_by__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Approved for Migration by:</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Project_Production_Changes</relationshipName>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Approved_for_Development_by__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Approved for Development by</label>
        <referenceTo>User</referenceTo>
        <relationshipName>Project_Production_Changes1</relationshipName>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Description_of_Change__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>What is the change that is required? Provide as much detail as possible including all business rules, exceptions, and any impacts/consequences on other business processes. This description should also describe the tangible benefits to the business.</inlineHelpText>
        <label>Description of Change</label>
        <length>32000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Migration_Procedure__c</fullName>
        <externalId>false</externalId>
        <label>Migration Procedure</label>
        <length>32000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Priority__c</fullName>
        <externalId>false</externalId>
        <label>Priority</label>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>1 - High</fullName>
                    <default>false</default>
                    <label>1 - High</label>
                </value>
                <value>
                    <fullName>2 - Medium</fullName>
                    <default>false</default>
                    <label>2 - Medium</label>
                </value>
                <value>
                    <fullName>3 - Low</fullName>
                    <default>false</default>
                    <label>3 - Low</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Project_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Project Name</label>
        <referenceTo>Project__c</referenceTo>
        <relationshipLabel>Project Production Changes</relationshipLabel>
        <relationshipName>Project_Production_Changes</relationshipName>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Release_Number__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>E.g. Release 1.2  - the second change of major release 1.</inlineHelpText>
        <label>Release Number</label>
        <length>20</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Remaining_Days_Effort_Estimate__c</fullName>
        <externalId>false</externalId>
        <label>Remaining Days Effort Estimate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Short_Name__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Provide a short name by which the change can easily be identified</inlineHelpText>
        <label>Short Name</label>
        <length>150</length>
        <required>false</required>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Solution_Design__c</fullName>
        <externalId>false</externalId>
        <label>Solution Design</label>
        <length>32000</length>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Requirements Analysis</fullName>
                    <default>false</default>
                    <label>Requirements Analysis</label>
                </value>
                <value>
                    <fullName>Estimates Complete</fullName>
                    <default>false</default>
                    <label>Estimates Complete</label>
                </value>
                <value>
                    <fullName>Solution Design</fullName>
                    <default>false</default>
                    <label>Solution Design</label>
                </value>
                <value>
                    <fullName>Approved for Development</fullName>
                    <default>false</default>
                    <label>Approved for Development</label>
                </value>
                <value>
                    <fullName>In Development</fullName>
                    <default>false</default>
                    <label>In Development</label>
                </value>
                <value>
                    <fullName>User Testing</fullName>
                    <default>false</default>
                    <label>User Testing</label>
                </value>
                <value>
                    <fullName>Ready for Migration</fullName>
                    <default>false</default>
                    <label>Ready for Migration</label>
                </value>
                <value>
                    <fullName>Complete</fullName>
                    <default>false</default>
                    <label>Complete</label>
                </value>
                <value>
                    <fullName>Cancelled</fullName>
                    <default>false</default>
                    <label>Cancelled</label>
                </value>
                <value>
                    <fullName>Ready for Migration to UAT</fullName>
                    <default>false</default>
                    <label>Ready for Migration to UAT</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Target_Date_for_Go_live__c</fullName>
        <externalId>false</externalId>
        <label>Target Date for Migration</label>
        <required>false</required>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Total_Development_Effort_Esitmate__c</fullName>
        <externalId>false</externalId>
        <label>Total Development Effort Estimate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>1</scale>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UAT_Tested__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>UAT Tested?</label>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Unit_Tested__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Unit Tested?</label>
        <trackFeedHistory>true</trackFeedHistory>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Project Production Change</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Status__c</columns>
        <columns>Description_of_Change__c</columns>
        <columns>Release_Number__c</columns>
        <columns>UAT_Tested__c</columns>
        <columns>Unit_Tested__c</columns>
        <columns>Approved_by__c</columns>
        <columns>Migration_Procedure__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>AllChanges</fullName>
        <columns>NAME</columns>
        <columns>Short_Name__c</columns>
        <columns>Description_of_Change__c</columns>
        <columns>Release_Number__c</columns>
        <columns>Status__c</columns>
        <columns>Approved_for_Development_by__c</columns>
        <columns>Approved_by__c</columns>
        <columns>Target_Date_for_Go_live__c</columns>
        <columns>UAT_Tested__c</columns>
        <columns>Unit_Tested__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <label>All Changes</label>
    </listViews>
    <listViews>
        <fullName>MDM_Phase_2</fullName>
        <columns>NAME</columns>
        <columns>Short_Name__c</columns>
        <columns>Unit_Tested__c</columns>
        <columns>Release_Number__c</columns>
        <columns>Status__c</columns>
        <columns>Approved_for_Development_by__c</columns>
        <columns>Approved_by__c</columns>
        <columns>Target_Date_for_Go_live__c</columns>
        <columns>UAT_Tested__c</columns>
        <columns>LAST_UPDATE</columns>
        <columns>Total_Development_Effort_Esitmate__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>notEqual</operation>
            <value>Complete</value>
        </filters>
        <filters>
            <field>Release_Number__c</field>
            <operation>equals</operation>
            <value>MDM Phase2</value>
        </filters>
        <label>MDM Phase 2</label>
    </listViews>
    <listViews>
        <fullName>Open_Changes</fullName>
        <columns>NAME</columns>
        <columns>Short_Name__c</columns>
        <columns>Description_of_Change__c</columns>
        <columns>Unit_Tested__c</columns>
        <columns>Release_Number__c</columns>
        <columns>Status__c</columns>
        <columns>Approved_for_Development_by__c</columns>
        <columns>Approved_by__c</columns>
        <columns>Target_Date_for_Go_live__c</columns>
        <columns>UAT_Tested__c</columns>
        <columns>LAST_UPDATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Status__c</field>
            <operation>notEqual</operation>
            <value>Complete,Cancelled</value>
        </filters>
        <label>Open Changes</label>
    </listViews>
    <listViews>
        <fullName>Release_1</fullName>
        <columns>NAME</columns>
        <columns>Status__c</columns>
        <columns>Description_of_Change__c</columns>
        <columns>Release_Number__c</columns>
        <columns>UAT_Tested__c</columns>
        <columns>Unit_Tested__c</columns>
        <columns>Approved_by__c</columns>
        <columns>Migration_Procedure__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Release_Number__c</field>
            <operation>equals</operation>
            <value>Release 1</value>
        </filters>
        <label>Release 1</label>
    </listViews>
    <listViews>
        <fullName>Release_2</fullName>
        <columns>NAME</columns>
        <columns>Status__c</columns>
        <columns>Description_of_Change__c</columns>
        <columns>Release_Number__c</columns>
        <columns>UAT_Tested__c</columns>
        <columns>Unit_Tested__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Release_Number__c</field>
            <operation>equals</operation>
            <value>Release 2</value>
        </filters>
        <label>Release 2</label>
    </listViews>
    <listViews>
        <fullName>X2012_R1</fullName>
        <columns>NAME</columns>
        <columns>Status__c</columns>
        <columns>Description_of_Change__c</columns>
        <columns>Release_Number__c</columns>
        <columns>UAT_Tested__c</columns>
        <columns>Unit_Tested__c</columns>
        <columns>Approved_by__c</columns>
        <columns>Migration_Procedure__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Release_Number__c</field>
            <operation>equals</operation>
            <value>2012 R1</value>
        </filters>
        <label>2012 R1</label>
    </listViews>
    <listViews>
        <fullName>X2012_R3</fullName>
        <booleanFilter>1 OR 2</booleanFilter>
        <columns>NAME</columns>
        <columns>Short_Name__c</columns>
        <columns>Status__c</columns>
        <columns>Unit_Tested__c</columns>
        <columns>UAT_Tested__c</columns>
        <columns>Priority__c</columns>
        <columns>Total_Development_Effort_Esitmate__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Project_Name__c</field>
            <operation>equals</operation>
            <value>2013 Release 1</value>
        </filters>
        <filters>
            <field>Release_Number__c</field>
            <operation>equals</operation>
            <value>2013 R1,2013R1</value>
        </filters>
        <label>2013 Release 1</label>
    </listViews>
    <listViews>
        <fullName>X2012_Release_2</fullName>
        <columns>NAME</columns>
        <columns>Short_Name__c</columns>
        <columns>Description_of_Change__c</columns>
        <columns>Status__c</columns>
        <columns>Solution_Design__c</columns>
        <columns>Approved_by__c</columns>
        <columns>Target_Date_for_Go_live__c</columns>
        <columns>Total_Development_Effort_Esitmate__c</columns>
        <columns>Remaining_Days_Effort_Estimate__c</columns>
        <columns>UAT_Tested__c</columns>
        <columns>Unit_Tested__c</columns>
        <columns>Migration_Procedure__c</columns>
        <columns>Release_Number__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Project_Name__c</field>
            <operation>equals</operation>
            <value>2012 Release 2</value>
        </filters>
        <label>2012 Release 2</label>
    </listViews>
    <listViews>
        <fullName>X2012_Release_2_Deploy</fullName>
        <booleanFilter>(1 AND 4) OR ((2 OR 5) AND 3)</booleanFilter>
        <columns>NAME</columns>
        <columns>Short_Name__c</columns>
        <columns>Description_of_Change__c</columns>
        <columns>Status__c</columns>
        <columns>Migration_Procedure__c</columns>
        <columns>Unit_Tested__c</columns>
        <columns>UAT_Tested__c</columns>
        <columns>Solution_Design__c</columns>
        <columns>Approved_by__c</columns>
        <columns>Target_Date_for_Go_live__c</columns>
        <columns>Total_Development_Effort_Esitmate__c</columns>
        <columns>Remaining_Days_Effort_Estimate__c</columns>
        <columns>Release_Number__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Project_Name__c</field>
            <operation>equals</operation>
            <value>2012 Release 2</value>
        </filters>
        <filters>
            <field>Project_Name__c</field>
            <operation>equals</operation>
            <value>2012 R1</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>notEqual</operation>
            <value>Complete,Requirements Analysis</value>
        </filters>
        <filters>
            <field>Status__c</field>
            <operation>notEqual</operation>
            <value>Requirements Analysis</value>
        </filters>
        <filters>
            <field>Project_Name__c</field>
            <operation>equals</operation>
            <value>2012 Release 1</value>
        </filters>
        <label>2012 Release 2 Deployment</label>
    </listViews>
    <listViews>
        <fullName>X2013_Release_2</fullName>
        <booleanFilter>1 OR 2</booleanFilter>
        <columns>NAME</columns>
        <columns>Short_Name__c</columns>
        <columns>Status__c</columns>
        <columns>Unit_Tested__c</columns>
        <columns>UAT_Tested__c</columns>
        <columns>Priority__c</columns>
        <columns>Total_Development_Effort_Esitmate__c</columns>
        <columns>Release_Number__c</columns>
        <columns>Project_Name__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Project_Name__c</field>
            <operation>equals</operation>
            <value>2013 Release 2</value>
        </filters>
        <filters>
            <field>Release_Number__c</field>
            <operation>equals</operation>
            <value>2013 R2,2013R2</value>
        </filters>
        <label>2013 Release 2</label>
    </listViews>
    <listViews>
        <fullName>X2014_R1</fullName>
        <columns>NAME</columns>
        <columns>Short_Name__c</columns>
        <columns>Status__c</columns>
        <columns>Unit_Tested__c</columns>
        <columns>UAT_Tested__c</columns>
        <columns>Approved_by__c</columns>
        <columns>Target_Date_for_Go_live__c</columns>
        <columns>LAST_UPDATE</columns>
        <columns>Total_Development_Effort_Esitmate__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Release_Number__c</field>
            <operation>equals</operation>
            <value>2014 - R1</value>
        </filters>
        <label>2014 R1</label>
    </listViews>
    <nameField>
        <displayFormat>PCI-{000000}</displayFormat>
        <label>Production Change ID</label>
        <trackFeedHistory>false</trackFeedHistory>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Project Production Changes</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Description_of_Change__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Release_Number__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Approved_for_Development_by__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Approved_by__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Target_Date_for_Go_live__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>UAT_Tested__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Unit_Tested__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Status__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Description_of_Change__c</lookupDialogsAdditionalFields>
        <searchResultsAdditionalFields>Solution_Design__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Development_Approval</fullName>
        <active>true</active>
        <description>ensures the approved by field is populated if the status is approved for development</description>
        <errorConditionFormula>AND( ISPICKVAL( Status__c , &apos;Approved for Development&apos; ), OR( ISNULL(  Approved_for_Development_by__c ), ISBLANK(  Approved_for_Development_by__c ) ) )</errorConditionFormula>
        <errorDisplayField>Approved_for_Development_by__c</errorDisplayField>
        <errorMessage>You must identify the approver if the change moves to this status. Please select an approver.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Migration_Approval</fullName>
        <active>true</active>
        <description>ensures the approved by field is populated if the status is approved for migration</description>
        <errorConditionFormula>AND( ISPICKVAL( Status__c , &apos;Ready for Migration&apos; ), OR( ISNULL( Approved_by__c ), ISBLANK(  Approved_by__c ) ) )</errorConditionFormula>
        <errorDisplayField>Approved_by__c</errorDisplayField>
        <errorMessage>You must identify the approver if the change moves to this status. Please select an approver.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Solution_Design_Required</fullName>
        <active>true</active>
        <description>ensures that once past the Solution Design phase, a design has been established and documented</description>
        <errorConditionFormula>AND( NOT( OR( ISPICKVAL( Status__c, &apos;Requirements Analysis&apos; ), ISPICKVAL( Status__c, &apos;Solution Design&apos; ) ) ), OR( ISNULL(  Solution_Design__c ), ISBLANK(  Solution_Design__c ) ) )</errorConditionFormula>
        <errorDisplayField>Solution_Design__c</errorDisplayField>
        <errorMessage>A Solution Design must be present and documented in detail before moving to any development, user testing, or migration state. Please document the design before continuing.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Testing_Complete</fullName>
        <active>true</active>
        <description>ensures that the testing has been completed before approving for migration</description>
        <errorConditionFormula>AND( ISPICKVAL(  Status__c , &apos;Ready for Migration&apos; ),  NOT( UAT_Tested__c ),  NOT( Unit_Tested__c ) )</errorConditionFormula>
        <errorDisplayField>Status__c</errorDisplayField>
        <errorMessage>The change MUST be unit tested and UAT tested in order to be ready for migration to production. Please ensure testing has been successful, then update this record.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
