<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Gard Administration</label>
    <logo>Logos_and_admin_documents/Gard_Logo_v1_Small.png</logo>
    <tabs>standard-Account</tabs>
    <tabs>Admin_System_Company__c</tabs>
    <tabs>Company_Reconciliation__c</tabs>
    <tabs>Reconciliation_Mapping__c</tabs>
    <tabs>standard-report</tabs>
    <tabs>Quotes</tabs>
    <tabs>Meeting_Minute__c</tabs>
    <tabs>Country__c</tabs>
    <tabs>Market_Area__c</tabs>
    <tabs>Budget_Products</tabs>
    <tabs>Port__c</tabs>
    <tabs>ESP_Contact_Skills__c</tabs>
    <tabs>CorrespondentPort__c</tabs>
    <tabs>Deleted_Record__c</tabs>
    <tabs>Extranet</tabs>
    <tabs>Document_Metadata__c</tabs>
    <tabs>Extranet_Favourite__c</tabs>
    <tabs>Extranet_Global_Client__c</tabs>
    <tabs>Client_Activity__c</tabs>
    <tabs>Claim__c</tabs>
</CustomApplication>
