({
    doInit : function(component, event, helper){
    	console.log('new code Controller and followerView removal');
        helper.getFollowers(component, event, helper);
    },
    closeModal : function(component, event, helper){
        var modal = component.find("followerModal");
        var modalBackdrop = component.find("followerModalBackdrop");
        $A.util.removeClass(modal,"slds-fade-in-open");
        $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
    },
    openModal: function(component, event, helper) {
    },
    getPreviousPage : function(component, event, helper){//gets previous page
        var thisPageNumber = component.get('v.pageNumber');
    	component.set('v.pageNumber',thisPageNumber-1);
        helper.getFollowers(component, event, helper);
	},
    getNextPage : function(component, event, helper){//gets next page
        var thisPageNumber = component.get('v.pageNumber');
    	component.set('v.pageNumber',thisPageNumber+1);
    	helper.getFollowers(component, event, helper);
	},
    followUser: function(component, event, helper){//not needed after SF-4923
        helper.followUnfollowUser(component, event, helper,"follow");
    },
    unFollowUser: function(component, event, helper){//not needed after SF-4923
        helper.followUnfollowUser(component, event, helper,"unfollow");
    },
	openUserDetails : function(component, event, helper){
        helper.goToUser(component, event, helper);
	},
    hideFollowerModal : function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire()
	},
    changeFollowing : function(component, event, helper){//added for SF-4923
    	helper.changeFollowing(component, event, helper);
	}
})