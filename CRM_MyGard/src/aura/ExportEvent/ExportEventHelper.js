({
        openEventExporter : function(component, event, helper){
            var recordId = component.get("{!v.recordId}");
            
            //Param Action
            var getEvent = component.get('c.getEvent');
            getEvent.setParams({
                "eventId" : recordId
            });
            getEvent.setCallback(this,function(response){
                if(response.getState() === 'SUCCESS' || response.getState() === 'DRAFT'){
                    //Parameters
                    var thisEvent = response.getReturnValue();
                    var paramURL;
                    paramURL = "?start="+thisEvent.StartDateTime+"&end="+thisEvent.EndDateTime+"&subject="+(typeof thisEvent.Subject != typeof undefined ? thisEvent.Subject : '')+"&description="+(typeof thisEvent.Description != typeof undefined ? thisEvent.Description : '')+"&location="+(typeof thisEvent.Location != typeof undefined ? thisEvent.Location : '');
                    
                    
                    //Base Action,nesting inside params because synchronized flow is needed
                    var baseURLAction = component.get("c.getBaseURL");
                    baseURLAction.setCallback(this,function(response){
                        if(response.getState() === 'SUCCESS' || response.getState() === 'DRAFT'){
                            
                            //setting Base url
                            var baseURL;
                            baseURL = response.getReturnValue();
                            
                            //set attribute on component
                            {//remove these later
                                console.log('thisEvent - '+thisEvent);
                                console.log('start - '+thisEvent.StartDateTime);
                                console.log('end - '+thisEvent.EndDateTime);
                                console.log('subject - '+thisEvent.Subject);
                                console.log('description - '+thisEvent.Description);
                                console.log('location - '+thisEvent.Location);
                                console.log('baseURL - '+baseURL);
                                console.log('navigateEventURL - '+paramURL);
                            }
                            component.set("v.urlToExportEvent",baseURL+paramURL);
                            helper.upProgress(component, event, helper);//100
                            
                            component.set("v.doneProcessing",true);
                            
                            //redirect back to the record page
                            setTimeout(function(){
                                var navigateBack = $A.get("e.force:navigateToSObject");
                                navigateBack.setParams({
                                    "recordId" : recordId,
                                    "isredirect" : true
                                });
                                navigateBack.fire();
                            },1000);
                        }
                    });
                    helper.upProgress(component, event, helper);//75
                    $A.enqueueAction(baseURLAction);
                    
                    
                    //create URL to navigate to
                    //navigateEventURL = "https://gardas--uat.my.salesforce.com/apex/ExportEvent?"+"start="+thisEvent.StartDateTime+"&end="+thisEvent.EndDateTime+"&subject="+(typeof thisEvent.Subject != typeof undefined ? thisEvent.Subject : '')+"&description="+(typeof thisEvent.Description != typeof undefined ? thisEvent.Description : '')+"&location="+(typeof thisEvent.Location != typeof undefined ? thisEvent.Location : '');
                }
            });
                helper.upProgress(component, event, helper);//50
                $A.enqueueAction(getEvent);
            },
                                 upProgress : function(component, event, helper){//called twice based on the code run progress rather than the timed progress
                var currentProgress = component.get("v.progress");
                currentProgress+=25;
                component.set("v.progress",currentProgress);
            }
        })