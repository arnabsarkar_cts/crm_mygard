({
    createFeedbackHelper : function(cmp, entObject) {
        var createFeedbackEvent = $A.get("e.force:createRecord");
        console.log('helper'+entObject);
        var iscompany=entObject.WhatId;
        if(iscompany.substr(0,3)=='001') {
            createFeedbackEvent.setParams({
                "entityApiName": "Customer_Feedback__c",
                "defaultFieldValues": {
                    'Source__c' : 'meeting',
                    'From_Company__c':entObject.WhatId,
                    'MeetingId__c':entObject.Id,
                    'From_Meeting__c':entObject.Subject
                }
            });
        } else{
            createFeedbackEvent.setParams({
                "entityApiName": "Customer_Feedback__c",
                "defaultFieldValues": {
                    'Source__c' : 'meeting',
                    'MeetingId__c':entObject.Id,
                    'From_Meeting__c':entObject.Subject
                }
            });         
        }
        createFeedbackEvent.fire();
        $A.get("e.force:closeQuickAction").fire();
    }
})