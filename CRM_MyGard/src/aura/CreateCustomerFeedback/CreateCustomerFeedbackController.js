({
    doInit : function(component, event, helper) {
        var recordId=component.get("v.recordId");
        console.log('recordId'+recordId);
        var entobject={};
        var action =component.get("c.getEventRecord");
        action.setParams({
            "recordId": recordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                entobject= response.getReturnValue();
             // window.open('https://gardsalesdev--lightning.lightning.force.com/a0n/e?retURL=%2Fa0n%2Fo&00ND0000006Upla='+entobject.Id+'&00ND0000005E4if=Meeting&00ND0000006UplZ='+entobject.Subject+'&CF00ND0000005E4i1='+entobject.What.Name)
              helper.createFeedbackHelper(component,entobject);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
       // helper.createFeedbackHelper(component,entobject);
        
    }
})