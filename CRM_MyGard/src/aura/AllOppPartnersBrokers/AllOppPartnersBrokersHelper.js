({
	getPartnersOrBrokers  : function(component, event, helper) {
        var oppId =  component.get('v.recordId');        
        var action = component.get('c.getPartnersOrBrokers');
        var records = {};
        var recordCount = '';
        action.setParams({
            "opportunityId":oppId
        });
        action.setCallback(this,function(res){            
            if(res.getState()==='SUCCESS' ||res.getState()==='DRAFT'){                
                records = res.getReturnValue();
                recordCount = records.length;
                component.set('v.partnersOrBrokers',res.getReturnValue());
                component.set('v.recordCount',recordCount);
                console.log(component.get('v.partnersOrBrokers'));
                console.log('count--'+recordCount);
            }else{
                console.log('Error');
            }
        });
        $A.enqueueAction(action);		
	},
    getOppName  : function(component, event, helper) {
        var oppId =  component.get('v.recordId');        
        var action = component.get('c.getOppName');
        //var records = {};
        //var recordCount = '';
        action.setParams({
            "opportunityId":oppId
        });
        action.setCallback(this,function(res){            
            if(res.getState()==='SUCCESS' ||res.getState()==='DRAFT'){                
                var records = res.getReturnValue();
                console.log('records:::::::::'+records);
                //recordCount = records.length;
                component.set('v.oppName',res.getReturnValue());
                //component.set('v.recordCount',recordCount);
                console.log(component.get('v.oppName'));
                //console.log('count--'+recordCount);
            }else{
                console.log('Error');
            }
        });
        $A.enqueueAction(action);		
	},
})