trigger setOpptyStage on Quote (after update) {
    String strAcceptedOpptyIds = '';
    for (Quote qt: Trigger.new) {
        String strStatus = qt.Status;
        if (strStatus=='Accepted') {
            strAcceptedOpptyIds+='\''+qt.OpportunityId+'\',';
        }
    }
    List<Opportunity> lsOppty = new List<Opportunity>();
    if (strAcceptedOpptyIds!='') {
        strAcceptedOpptyIds = strAcceptedOpptyIds.substring(0, strAcceptedOpptyIds.length()-1);
        for (Opportunity opp: Database.query('SELECT Id, Name, StageName FROM Opportunity WHERE Id IN ('+strAcceptedOpptyIds+')')) {
            opp.StageName = 'Closed Won';
            lsOppty.add(opp);
        }
    }
    if (lsOppty.size()>0) {update lsOppty;}
}