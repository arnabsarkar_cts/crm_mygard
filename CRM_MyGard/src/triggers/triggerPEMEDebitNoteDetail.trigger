/*
Purpose:Update the address from requested section and populate addess according to the account selected.
*/
trigger triggerPEMEDebitNoteDetail on PEME_Debit_note_detail__c (before update,before insert) {

    for(PEME_Debit_note_detail__c objNew:trigger.new){
        if(Trigger.isUpdate && Trigger.oldMap.get(objNew.Id).Status__c=='Pending Update')
           {
                if(objNew.Status__c=='Approved' && Trigger.isBefore){
                    objNew.Contact_person_Name__c = objNew.Requested_Contact_person_Name__c;
                    objNew.Contact_person_Email__c = objNew.Requested_Contact_person_Email__c;
                    //commented for SF-3917
                    /*
                    objNew.Address__c = objNew.Requested_Address__c;
                    objNew.Address_Line_2__c = objNew.Requested_Address_Line_2__c;
                    objNew.Address_Line_3__c = objNew.Requested_Address_Line_3__c;
                    objNew.Address_Line_4__c = objNew.Requested_Address_Line_4__c;
                    objNew.City__c = objNew.Requested_City__c;
                    objNew.Country__c = objNew.Requested_Country__c;
                    objNew.State__c = objNew.Requested_State__c;
                    objNew.Zip_code__c = objNew.Requested_Zip_Code__c;
                    */
                    objNew.Comments__c = objNew.RequestedComments__c;
                    objNew.Requested_Contact_person_Name__c = null; 
                    objNew.Requested_Contact_person_Email__c= null; 
                    objNew.Requested_Address__c= null; 
                    objNew.Requested_Address_Line_2__c= null; 
                    objNew.Requested_Address_Line_3__c= null; 
                    objNew.Requested_Address_Line_4__c= null; 
                    objNew.Requested_City__c= null; 
                    objNew.Requested_Country__c= null; 
                    objNew.Requested_State__c= null;
                    objNew.Requested_Zip_Code__c= null;
                    objNew.Draft_Company_Name__c = null;
                    objNew.RequestedComments__c = null;
                }
           }
        
        //else if(Trigger.isUpdate || Trigger.isInsert)
        //{ 
        //    if(Trigger.isBefore && objNew.Status__c=='Approved')
        //      {
                  //Commenting below line for SF-258 
               // Profile objProf = [Select Id,Name From Profile where Name='Partner Community Login User Custom' LIMIT 1];
              //  if(objNew.Company_Name__c!=NULL /*&& objProf.Id!=UserInfo.getProfileId()*/)//Commenting line for SF-258
          /*
                {
                        Account acc = [select BillingCity__c,BillingPostalcode__c,Email__c,BillingCountry__c,BillingState__c,Billing_Street_Address_Line_1__c,Billing_Street_Address_Line_2__c,Billing_Street_Address_Line_3__c,Billing_Street_Address_Line_4__c 
                                       from Account 
                                       where id=:objNew.Company_Name__c];
                        //objNew.Phone__c = String.valueof(acc.Phone);
                        objNew.Address__c = acc.Billing_Street_Address_Line_1__c ;
                        objNew.Address_Line_2__c = acc.Billing_Street_Address_Line_2__c ;
                        objNew.Address_Line_3__c = acc.Billing_Street_Address_Line_3__c ;
                        objNew.Address_Line_4__c = acc.Billing_Street_Address_Line_4__c ;
                        objNew.City__c = acc.BillingCity__c;
                        objNew.Country__c = acc.BillingCountry__c ;
                        objNew.State__c = acc.BillingState__c ;
                        objNew.Zip_code__c = acc.BillingPostalcode__c ;   
                }
            }
        }
        */   //commented for SF-3917
    }
    
}