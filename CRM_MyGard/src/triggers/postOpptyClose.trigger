trigger postOpptyClose on Opportunity (after insert,after update) {

    //PCI-000140 Post to account when opp closed won
    
    List<FeedItem> feedItems = new List<FeedItem>();
    String renewalIndex;
    //Check if opp is newly closed won
    for(Opportunity o:trigger.new)
    {
        renewalIndex = '';
        Boolean oppIsNewlyClosedWon = false;
        if(trigger.isInsert && o.isClosed == true && o.isWon == true)
        {
            oppIsNewlyClosedWon = true;
        }
        else if(trigger.isUpdate)
        {
            if(o.isClosed == true && trigger.oldMap.get(o.Id).isClosed == false)
            {
                if(o.isWon == true && trigger.oldMap.get(o.Id).isWon == false)
                {
                    oppIsNewlyClosedWon = true;
                }
            }
        }
        
        if(oppIsNewlyClosedWon == true && !Gard_RecursiveBlocker.blocker)
        {   
            Gard_RecursiveBlocker.blocker = true;
            FeedItem fitem = new FeedItem();
            if(o.Type == 'Renewal'){
                renewalIndex =  '\n' + Schema.Opportunity.fields.Opportunity_Renewal_Index__c.getDescribe().getLabel() + ': ' + o.Opportunity_Renewal_Index__c;
            }
            fitem.type = 'LinkPost';
            fitem.ParentId = o.AccountId;
            fitem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + o.Id;
            fitem.Title = 'View Opportunity';
            
            fitem.Body = 'Opportunity won: ' + o.Name
                        + '\n' + Schema.Opportunity.fields.Amount.getDescribe().getLabel() + ': ' + o.CurrencyIsoCode + ' ' + o.Amount +
                        renewalIndex +//+ '\n' + Schema.Opportunity.fields.Opportunity_Renewal_Index__c.getDescribe().getLabel() + ': ' + o.Opportunity_Renewal_Index__c
                        + '\n' + Schema.Opportunity.fields.Budget_Year__c.getDescribe().getLabel() + ': ' + o.Budget_Year__c
                        + '\n' + Schema.Opportunity.fields.Type.getDescribe().getLabel() + ': ' + o.Type;
                                                  
            feedItems.add(fitem);
        }
    }
    
    //Save the FeedItems all at once.
    if (feedItems.size() > 0) {
        Database.insert(feedItems,false); //notice the false value. This will allow some to fail if Chatter isn't available on that object
    }
    

}