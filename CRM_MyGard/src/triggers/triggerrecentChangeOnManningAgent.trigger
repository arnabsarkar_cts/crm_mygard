trigger triggerrecentChangeOnManningAgent on PEME_Manning_Agent__c (after update, after insert,before update,before insert) { 
      Recent_Updates__c ru =new Recent_Updates__c();
      PEME_Manning_Agent__c pmaOld;
      Account acc = new account();
      Account acc1 = new account();
      PEME_Enrollment_Form__c pmf = new PEME_Enrollment_Form__c();
      List<Account> tcl=[select name,company_id__c from Account where company_id__c='22384'];  //SF-4176
      Boolean testCompanyFoundFlag=false;
      String tcid='';
      if(tcl!=null&&tcl.size()==1){
            tcid=tcl[0].company_id__c;
            testCompanyFoundFlag=true;
      }
      if((Trigger.isUpdate || Trigger.isInsert) && Trigger.isAfter){
          for(PEME_Manning_Agent__c pma:Trigger.new){
              if(Trigger.isUpdate){                 //SF-4204
                  pmaOld=Trigger.oldMap.get(pma.Id);
              }
              if(pma.Client__c!=null && pma.Status__c=='Approved')
              {   
                  acc = [Select name,company_id__c from account where id =:pma.Client__c];
                  pmf = [Select ClientName__c from PEME_Enrollment_Form__c where id =:pma.PEME_Enrollment_Form__c];
                  acc1 = [Select name,company_id__c from account where id =:pmf.ClientName__c];
                  if(((!testCompanyFoundFlag)||(acc.company_id__c!=tcid && acc1.company_id__c!=tcid)) && ((pmaOld!=null && ((pmaOld.Client__c!=null && pma.Client__c!=pmaOld.Client__c) || (pmaOld.Status__c!='Approved'))) || (Trigger.isInsert) ) ){                   //SF-4204
                  ru.Update_Value__c = '<span class="text-bold">' + acc.name + '</span> is now associated with <span class="text-bold">'+acc1.name+'</span> as a manning agent effective since <span class="text-bold">'+ pma.LastModifiedDate.format('dd-MM-yyyy') + '</span>';
                  insert ru;
                  }                  
              }
          }
      } 
      Recent_Updates__c ru1 =new Recent_Updates__c();  
      Account acc11 = new account();
      Account acc12 = new account();
      PEME_Enrollment_Form__c pmf1 = new PEME_Enrollment_Form__c();
      if(Trigger.isUpdate && Trigger.isAfter){
          for(PEME_Manning_Agent__c pma:Trigger.new){
              pmaOld=Trigger.oldMap.get(pma.Id);
              if(pmaOld.Client__c != null)
              {
                  if(pma.Client__c != null)
                  {   
                      if((pma.Client__c != pmaOld.Client__c && pma.Status__c=='Approved') ||  (pmaOld.Status__c=='Approved' && pma.Status__c!='Approved'))
                      {
                          acc11 = [Select name,company_id__c from account where id =:pmaOld.Client__c];
                          pmf1 = [Select ClientName__c from PEME_Enrollment_Form__c where id =:pma.PEME_Enrollment_Form__c];
                          acc12 = [Select name,company_id__c from account where id =:pmf1.ClientName__c];
                          if((!testCompanyFoundFlag)||(acc11.company_id__c!=tcid && acc12.company_id__c!=tcid)){                 //SF-4176
                          ru1.Update_Value__c ='Manning agent - <span class="text-bold">'+acc11.name+'</span> is now no longer associated with <span class="text-bold">'+acc12.name+'</span> as a manning agent effective since <span class="text-bold">'+ pma.LastModifiedDate.format('dd-MM-yyyy') + '</span>';
                          insert ru1;
                          }    
                      }
                  }
              }
          }
      }
      //commented for SF-3917
      /*
      if(Trigger.isBefore){       
        for(PEME_Manning_Agent__c objNew:trigger.new){        
            if(objNew.Client__c != null){
                Account accnt = [select BillingCity__c,Phone,BillingPostalcode__c,Corporate_email__c,BillingCountry__c,BillingState__c,Billing_Street_Address_Line_1__c,Billing_Street_Address_Line_2__c,Billing_Street_Address_Line_3__c,Billing_Street_Address_Line_4__c 
                          from Account 
                          where id=:objNew.Client__c];
            
                System.debug('objNew.Client__r.Billing_Street_Address_Line_1__c--------------'+objNew.Client__r.Billing_Street_Address_Line_1__c);
                objNew.Address__c = accnt.Billing_Street_Address_Line_1__c ;
                objNew.Address_Line_2__c = accnt.Billing_Street_Address_Line_2__c ;
                objNew.Address_Line_3__c = accnt.Billing_Street_Address_Line_3__c ;
                objNew.Address_Line_4__c = accnt.Billing_Street_Address_Line_4__c ;
                objNew.City__c = accnt.BillingCity__c;
                objNew.Country__c = accnt.BillingCountry__c ;
                objNew.State__c = accnt.BillingState__c ;
                objNew.Zip_code__c = accnt.BillingPostalcode__c ;
            }
        }
    } 
    */
}