trigger CustomerFeedbackTrigger on Customer_Feedback__c (after insert, after update, before insert, before update) {
    /*for(Customer_Feedback__c aCustomerFeedback : trigger.new){
        if(aCustomerFeedback.Responsible__c == null){
            if(aCustomerFeedback.Company_to__c != null){
                Account selectedAccount = [SELECT ID, OwnerId FROM ACCOUNT 
                                          WHERE ID =: aCustomerFeedback.Company_to__c];
                aCustomerFeedback.Responsible__c = selectedAccount.OwnerId ;
            }else if(aCustomerFeedback.Company_to__c == null && aCustomerFeedback.From_Company__c != null){
                Account selectedAccount = [SELECT ID, OwnerId FROM ACCOUNT 
                                           WHERE ID =: aCustomerFeedback.From_Company__c];
                aCustomerFeedback.Responsible__c = selectedAccount.OwnerId ;
            }
        }
    }*/
    CustomerFeedbackTriggerHandler handler = new CustomerFeedbackTriggerHandler();
    
    if( Trigger.isInsert )
    {
        if(Trigger.isBefore)
        {
            handler.OnBeforeInsert(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }
        else
        {
            handler.OnAfterInsert(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }
    }
    else if ( Trigger.isUpdate )
    {
        if(Trigger.isBefore)
        {
            handler.OnBeforeUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }
        else
        {
            handler.OnAfterUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }
    }
}