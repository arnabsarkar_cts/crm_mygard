trigger updateMemberStatus on Company_Membership_Review__c (after update) {

    List<String> memberNameList = new List<String>();
    List<Company_Membership_Review__c> memberReviewList;
    integer count = 0;
    
    //Account acc = [select id, name,PI_member_review_count__c, membership_status__c from Account  where id = :trigger.new[0].Company_Name__c];
    memberReviewList = [select id,name from Company_Membership_Review__c 
                          where Company_Name__c = :trigger.new[0].Company_Name__c order by createdDate desc];
          if(memberReviewList!=null && memberReviewList.size() >0 ){
              if(trigger.new[0].name == memberReviewList[0].name){
 
                 /*if (trigger.new[0].status__c == 'Approved') {
                 
                  Account accnt = [select id, name, membership_status__c 
                  from Account 
                  where id = :trigger.new[0].Company_Name__c];
                 
                 accnt.Membership_Status__c = 'Approved';
                 update accnt;
                  
                 }
                  if (trigger.new[0].status__c == 'Declined') {
                 
                  Account accnt = [select id, name, membership_status__c 
                  from Account 
                  where id = :trigger.new[0].Company_Name__c];
                 
                 accnt.Membership_Status__c = 'Declined';
                 update accnt;
                  
                 }*/
                   //added on 13-10-2015 for CRM-81. Improved TOMOLG 30 10 2017 SF-3994
                 if ((trigger.new[0].status__c == 'Approved' || trigger.new[0].status__c =='Declined') ) {
                 
                  Account accnt = [select id, name, membership_status__c , Membership_Type__c 
                  from Account 
                  where id = :trigger.new[0].Company_Name__c];
                 
                 accnt.Membership_Type__c = trigger.new[0].Approval_type_required__c;
                 accnt.membership_status__c =  trigger.new[0].status__c;
                 update accnt;
                  
                 }
                  if ((trigger.new[0].status__c == 'Approved' || trigger.new[0].status__c =='Declined') ) {
                 
                  Account accnt = [select id, name, Authority_Country__c,   Broker_type__c, Licensed__c, Licensed_by_Authority__c,License_description__c,License_number__c          
                  from Account 
                  where id = :trigger.new[0].Company_Name__c];
                 
                 accnt.Authority_Country__c = trigger.new[0].Authority_Country__c;
                 accnt.Broker_type__c = trigger.new[0].Broker_type__c;
                 accnt.Licensed__c = trigger.new[0].Licensed__c;
                 accnt.Licensed_by_Authority__c =  trigger.new[0].Licensed_by_Authority__c;
                 accnt.License_description__c =  trigger.new[0].License_description__c;
                 accnt.License_number__c =  trigger.new[0].License_number__c;
                 update accnt;
                  
                 }

          }
    }
}