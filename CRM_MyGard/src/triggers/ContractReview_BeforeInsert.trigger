trigger ContractReview_BeforeInsert on Contract_Review__c (before insert) {

    if(trigger.isInsert)
    {
        for(Contract_Review__c cr:trigger.new)
        {
            if(cr.Contract_Reviewer__c == null)
            {
                cr.Contract_Reviewer__c = UserInfo.getUserId();
            }
        }
    }
}