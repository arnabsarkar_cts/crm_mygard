trigger fluidoInvitation_BeforeInsert on fluidoconnect__Invitation__c (before insert) {

    //cDecisions Ltd - 9/9/13 - Check survey status before adding invitee
    
    //Build map of parent surveys and status
    
    Set<Id> flSurveyIds = new Set<Id>();
    
    for(fluidoconnect__Invitation__c flInvitation : trigger.new)
    {
        flSurveyIds.add(flInvitation.fluidoconnect__Survey__c);
    }
    
    Map<Id, fluidoconnect__Survey__c> flSurveys = new Map<Id, fluidoconnect__Survey__c>([SELECT Id, Invitations_Locked__c, fluidoconnect__Status__c, fluidoconnect__Event_Owner__c, fluidoconnect__Event_Organiser__c FROM fluidoconnect__Survey__c WHERE Id IN :flSurveyIds]);
    
    for(fluidoconnect__Invitation__c flInvitation : trigger.new)
    {
        if(flSurveys.get(flInvitation.fluidoconnect__Survey__c).Invitations_Locked__c == true)
        {
            if((UserInfo.getUserId() != flSurveys.get(flInvitation.fluidoconnect__Survey__c).fluidoconnect__Event_Owner__c) && (UserInfo.getUserId() != flSurveys.get(flInvitation.fluidoconnect__Survey__c).fluidoconnect__Event_Organiser__c))
            {
                flInvitation.addError(Label.SurveyOnlyEventOrganisersCanAdd);
            }
        }
    }

}