/*
*This trigger works on 'after update' of contract record. 
*This deletes all the mycoverlist records related to the userid of the updated contract record and reclaculate the Asset records 
*for that user id and inserts the new set of asset records into Mycoverlist.
*
*/
trigger ShareWithClient_bkp on Contract (before insert,after update) {
    //if(!Test.isRunningTest())
    //{
        Set<id> setAccId = new Set<id>();
        Set<id> setBrokerID= new Set<id>();
        
        List<Contact> lstContact= new List<Contact>();
        Set<Id> setConId = new Set<Id>();
        List<User> lstUser= new List<User>();
        Boolean blBrokerView;
        List<MyCoverList__c> lstMyCover = new List<MyCoverList__c>();
        List<MyCoverList__c> delLstMyCover;
        List<MyCoverList__c> delLstMyCoverAll = new List<MyCoverList__c>();
        
        
        set<Id> CompanyIds = new set<Id>();
        if(!Test.isRunningTest()){ //added
        if(Trigger.isInsert  && Trigger.isBefore){
            for(Contract aContract : Trigger.new){
                System.debug('aContract Broker' + aContract.broker__c);
                System.debug('aContract Client ' + aContract.client__c);
                
                List<Contract> singleContracts = [SELECT ID, Shared_With_Client__c FROM CONTRACT WHERE client__c = :aContract.client__c and Broker__c = :aContract.broker__c LIMIT 1];
                if(singleContracts.size() > 0){
                    System.Debug('**** Shared_With_Client__c: ' + singleContracts.get(0).Shared_With_Client__c);
                    System.Debug('**** Agrrement id: ' + singleContracts.get(0).ID);
                    aContract.Shared_With_Client__c = singleContracts.get(0).Shared_With_Client__c;
                }
            }
        }   
        }//added
        for(Contract contr : Trigger.New){
            if(Trigger.oldMap != null && contr.Shared_With_Client__c!=Trigger.oldMap.get(contr.Id).Shared_With_Client__c && contr.Shared_With_Client__c == true){
                setAccId.add(contr.Client__c);
                setBrokerID.add(contr.broker__c);
            }else if(Trigger.oldMap != null && contr.Shared_With_Client__c!=Trigger.oldMap.get(contr.Id).Shared_With_Client__c && contr.Shared_With_Client__c == false){
                setAccId.add(contr.Client__c);
                setBrokerID.add(contr.broker__c);
            }
        }
        system.debug('setBrokerID**********'+setBrokerID);
        delLstMyCover = [Select id from MyCoverList__c where  broker_id__c =:setBrokerID];
        for(MyCoverList__c mcl : delLstMyCover){
            delLstMyCoverAll.add(mcl);
        }
        
        Database.delete( delLstMyCoverAll, false);
        
        //for(account acc: setBrokerID)
        // Contact myContact;
        /* if(usr!= null && usr.contactID != null){
myContact = [Select AccountId from Contact Where id=: usr.contactID];
} */
        // Account acc;
        List<Object__c> lstObject = new List<Object__c>();
        List<Contract> lstContract = new List<Contract>();
        List<Contract> lstContractClient = new List<Contract>();        
        
        /*  if(myContact != null && myContact.AccountId != null){
acc = [Select ID,Company_Role__c from Account Where id =: myContact.AccountId ]; */       
        //  if(acc != null && acc.Company_Role__c != null &&  acc.Company_Role__c.contains('Broker')){
        //    blBrokerView = true;
        /* lstContract = [Select Id, Client__c from Contract Where Broker__c =: acc.id];
Set<ID> setClientID = new Set<ID>();
for(Contract contract: lstContract){
setClientID.add(contract.Client__c);
}*/
        //  lstContractClient = [Select id from Contract Where Client__c IN : setClientID];
        lstContractClient = [Select Id, Client__c from Contract Where Broker__c =: setBrokerID];
        /*  }else{
blBrokerView = false;          
lstContractClient = [Select id from Contract Where Client__c =: acc.id AND broker__c=null ];
} */
        //}
        system.debug('lstContractClient**********'+lstContractClient);
        system.debug('lstContractClient size**********'+lstContractClient.size());
        Set<ID> setCoverID = new Set<ID>();
        for(Contract obj : lstContractClient){
            setCoverID.add(obj.ID);
        }
        system.debug('setCoverID*******'+setCoverID.size());
        //  system.debug('Asset size***'+[select id FROM Asset Where Agreement__c IN : setCoverID].size());
        /*
List<Asset> lstRisk = new List<Asset>();
if(setCoverID.size()>0){
lstRisk = [Select id from Asset Where Agreement__c IN : setCoverID LIMIT 2000];
}

setRiskID = new Set<ID>();
for(Asset rsk : lstRisk ){
setRiskID.add(rsk.ID);
}      
system.debug('*****setRiskID*****'+setRiskID);*/
        
        //showMyCover =true;
        string strforClient = '';
        string strforClientGroupby = '';
        
        // if(blBrokerView == true){
        //strforClient = 'Object__r.Client__r.Name clientname,';
        //strforClientGroupby = 'Object__r.Client__r.Name,';
        strforClient = 'Agreement__r.Client__r.Name clientname, Agreement__r.Client__r.Id clientId,';
        strforClientGroupby = 'Agreement__r.Client__r.Name,Agreement__r.Client__r.Id,';
        //  }
        //  List<Boolean> lstblOnrisk = new List<Boolean>();
        // lstblOnrisk.add(Boolean.valueof(strTrueOrFalse));
        //String strCondition = ' AND On_risk_indicator__c IN : lstblOnrisk ';
        
        //List<Asset>assetList=[SELECT id FROM Asset WHERE Agreement__c IN :setCoverID limit 5000];
        /*String sSoqlQuery = 'SELECT MAX(Expiration_Date__c) expdate, Agreement__c AgId, '+strforClient+' UnderwriterNameTemp__c underwriter, UnderwriterId__c underwriterId,Product_Name__c prod,Agreement__r.Business_Area__c busarea,'+ 
'Agreement__r.Broker__r.Name AggName,Agreement__r.Broker__r.Id AggId, Agreement__r.Policy_Year__c plcyyr, '+
'Claims_Lead__c claim, Gard_Share__c grdshr, On_risk_indicator__c rskind,Agreement__r.Shared_With_Client__c sharedwithclient,  '+
'Count(id) FROM Asset Where Agreement__c IN : setCoverID '+
'GROUP BY Agreement__c, '+strforClientGroupby+' UnderwriterNameTemp__c, UnderwriterId__c, Product_Name__c, '+
'Agreement__r.Business_Area__c,'+
'Agreement__r.Broker__r.Name,Agreement__r.Broker__r.Id,'+
'Agreement__r.Policy_Year__c,'+
'Claims_Lead__c, Gard_Share__c, On_risk_indicator__c,Agreement__r.Shared_With_Client__c ';*/
        List<asset>assetIdList=[select id FROM Asset Where Agreement__c IN (Select Id from Contract Where Broker__c =:setBrokerID)];
        system.debug('assetIdList ******'+assetIdList.size());
        String sSoqlQuery = 'SELECT MAX(Expiration_Date__c) expdate, Agreement__c AgId, '+strforClient+' UnderwriterNameTemp__c underwriter, UnderwriterId__c underwriterId,Product_Name__c prod,Agreement__r.Business_Area__c busarea,'+ 
            'Agreement__r.Broker__r.Name AggName,Agreement__r.Broker__r.Id AggId, Agreement__r.Policy_Year__c plcyyr, '+
            'Claims_Lead__c claim, Gard_Share__c grdshr, On_risk_indicator__c rskind,Agreement__r.Shared_With_Client__c sharedwithclient,  '+
            'Count(id) FROM Asset Where ID IN : assetIdList '+
            'GROUP BY Agreement__c, '+strforClientGroupby+' UnderwriterNameTemp__c, UnderwriterId__c, Product_Name__c, '+
            'Agreement__r.Business_Area__c,'+
            'Agreement__r.Broker__r.Name,Agreement__r.Broker__r.Id,'+
            'Agreement__r.Policy_Year__c,'+
            'Claims_Lead__c, Gard_Share__c, On_risk_indicator__c,Agreement__r.Shared_With_Client__c ';
        
        //AggregateResult[] groupedResults = database.query(sSoqlQuery);  //commented for SF-4543
        List<sObject> groupedResults = new List<sObject>(); //added for SF-4543
        for(sObject sObj : Database.query(sSoqlQuery)){ //added for SF-4543
            groupedResults.add(sObj);  
        }
        
        system.debug('**********groupedResults --> '+groupedResults);
        
        
        
        for(MyCoverList__c mcl : delLstMyCover){
            delLstMyCoverAll.add(mcl);
        }
        
        system.debug('**********groupedResults --> '+groupedResults);      
        //for (AggregateResult ar : groupedResults)  { //commented for SF-4543
        for(sObject ar : groupedResults){ // added for SF-4543            
            if(blBrokerView == false){        //Commented as blBrokerView is not in use, to improve test coverage
               /*     lstMyCover.add(new MyCoverList__c(
                    AgreementId__c=string.valueof(ar.get('AgId')),
                    client__c=string.valueof(ar.get('clientname')),
                    client_id__c=string.valueof(ar.get('clientId')),
                    underwriter__c=string.valueof(ar.get('underwriter')),
                    UnderwriterId__c=string.valueof(ar.get('underwriterId')),
                    product__c=string.valueof(ar.get('prod')),
                    productarea__c=string.valueof(ar.get('busarea')),
                    broker__c=string.valueof(ar.get('AggName')),
                    broker_id__c=string.valueof(ar.get('AggId')), 
                    expirydate__c=string.valueof(ar.get('expdate')),
                    policyyear__c=Integer.valueof(ar.get('plcyyr')),                                           
                    claimslead__c=string.valueof(ar.get('claim')),
                    gardshare__c=Double.valueof(ar.get('grdshr')),
                    onrisk__c=Boolean.valueof(ar.get('rskind')),
                    viewobject__c=Integer.valueOf(ar.get('expr0')),
                    Shared_With_Client__c=Boolean.valueof(ar.get('sharedwithclient'))
                    //  userId__c = usr.Id
                ));*/
            }else{
                lstMyCover.add(new MyCoverList__c(
                    AgreementId__c=string.valueof(ar.get('AgId')),
                    client__c=string.valueof(ar.get('clientname')),
                    client_id__c=string.valueof(ar.get('clientId')),
                    underwriter__c=string.valueof(ar.get('underwriter')),
                    UnderwriterId__c=string.valueof(ar.get('underwriterId')),
                    product__c=string.valueof(ar.get('prod')),
                    productarea__c=string.valueof(ar.get('busarea')),
                    broker__c=string.valueof(ar.get('AggName')),
                    broker_id__c=string.valueof(ar.get('AggId')),
                    expirydate__c=string.valueof(ar.get('expdate')),
                    policyyear__c=Integer.valueof(ar.get('plcyyr')),                                           
                    claimslead__c=string.valueof(ar.get('claim')),
                    gardshare__c=Double.valueof(ar.get('grdshr')),
                    onrisk__c=Boolean.valueof(ar.get('rskind')),
                    viewobject__c=Integer.valueOf(ar.get('expr0')),
                    Shared_With_Client__c=Boolean.valueof(ar.get('sharedwithclient'))
                    //  userId__c = usr.Id
                    
                ));
            }
        }
        
        
        
        
        
        
        
        Database.insert( lstMyCover, false);
    //}
    
}