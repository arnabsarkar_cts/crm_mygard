trigger updateModifiedDate on Account
                                         (before update) {


    // Trigger.new is a list of the Accounts that will be updated  
    
    // This loop iterates over the list, 
    
    for (Integer i = 0; i < Trigger.new.size(); i++) {
        if (   (Trigger.old[i].objectives__c != Trigger.new[i].objectives__c) || (Trigger.old[i].plan__c != Trigger.new[i].plan__c))  
            {
                                      Trigger.new[i].last_modified__c = date.today();
            }
    }
}