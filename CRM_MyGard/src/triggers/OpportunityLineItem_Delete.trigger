trigger OpportunityLineItem_Delete on OpportunityLineItem (after delete, after undelete) {
	DQ__c DQSetting = DataQualitySupport.GetSetting(UserInfo.getUserId());
	System.Debug('*** DQ Setting = ' + DQSetting);
	String objectName = Schema.SObjectType.OpportunityLineItem.getName();
	System.Debug('*** objectName = ' + objectName);
	if(DQSetting.Enable_Opportunity_Line_Delete_History__c){	
		System.Debug('*** DQSetting.Enable_Opportunity_Line_Delete_History__c = ' + DQSetting.Enable_Opportunity_Line_Delete_History__c);
		System.Debug('*** Trigger.isDelete = ' + Trigger.isDelete);
		System.Debug('*** Trigger.isAfter = ' + Trigger.isAfter);
		System.Debug('*** Trigger.isUndelete = ' + Trigger.isUndelete);
		if(Trigger.isDelete && Trigger.isAfter){
			Map<Id, String> recMap = new Map<Id, String>();
			//build a map of id to dwh id
			for(OpportunityLineItem aRec:Trigger.old){
				recMap.put(aRec.Id, aRec.DWH_Section_Id__c);
			}
			System.Debug('*** recMap = ' + recMap);			
			ManageDeletedRecord.RememberDeletedRecords(recMap, objectName);
		}else if(Trigger.isUndelete && Trigger.isAfter){
			System.Debug('*** Trigger.old = ' + Trigger.old);			
			System.Debug('*** Trigger.new = ' + Trigger.new);
			System.Debug('*** Trigger.newMap = ' + Trigger.newMap);						
			ManageDeletedRecord.UpdateUnDeletedRecords(Trigger.newMap.keySet(), objectName);
		}
	}
}