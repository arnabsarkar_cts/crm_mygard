trigger MeetingMinute_AfterInsert on Meeting_Minute__c (after insert, after update) {
	
	String taskSubject = 'Complete Meeting Minutes';
	
	if(trigger.isInsert)
	{
		List<Task> newTasks = new List<Task>();
		for (Meeting_Minute__c mm : trigger.new){
			MeetingMinutesSetupAttendees.setupAttendeesNonUI(mm.Event_Id__c, mm.Id, mm.Distributed__c);
			Task t = new Task();
			t.whatId = mm.Id;
			t.OwnerId = mm.OwnerId;
			t.Subject = taskSubject;
			t.Status = 'Not Started';
			t.Priority = 'Normal';
			//SOC 17/4/13 - PCI-000109 - Add Task Due Date
			t.ActivityDate = mm.End_Date__c;
			newTasks.add(t);
			
			//Make sure that the meeting minutes have been initialised
			
		}
		if(newTasks.size() > 0){
			insert(newTasks);
		}
	}
	
	if(trigger.isUpdate)
	{
		//SOC 17/4/13 - PCI-000109 - Need to cater for updating MM - due date of event will reflect this
		Set<Id> mmIds = new Set<Id>();
		Map<Id, Date> mmEndDates = new Map<Id, Date>();
		for(Meeting_Minute__c mm:trigger.New)
		{
			if(mm.End_Date__c != trigger.oldMap.get(mm.Id).End_Date__c)
			{
				mmIds.add(mm.Id);
				mmEndDates.put(mm.Id, mm.End_Date__c);
			}
		}
		
		List<Task> mmTasks = [SELECT Id, WhatId, ActivityDate FROM Task WHERE whatId IN :mmIds AND Subject=:taskSubject AND isClosed=false];
		for(Task t:mmTasks)
		{
			t.ActivityDate = mmEndDates.get(t.whatId);
		}
		
		update mmTasks;
		
	}
}