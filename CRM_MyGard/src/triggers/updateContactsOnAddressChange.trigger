//Test Class - TestTrigUpdateContactsOnAddressChange
trigger updateContactsOnAddressChange on Account (before update, after update) {
    
    if(Trigger.isafter){
        // The map allows us to keep track of the accounts that have  
        
        System.debug('SF-4462 AccountHelper.AccountBeforeInsert START DML used - '+Limits.getDmlStatements());
        System.debug('SF-4462 AccountHelper.AccountBeforeInsert START SOQL used - '+Limits.getQueries());
        System.debug('SF-4462 AccountHelper.AccountBeforeInsert START Rows returned used - '+Limits.getQueryRows());
        // new addresses  
        
        Map<Id, Account> acctsWithNewAddresses = new Map<Id, Account>();
    
        // Trigger.new is a list of the Accounts that will be updated  
        
        // This loop iterates over the list, and adds any that have new  
        
        // addresses to the acctsWithNewAddresses map.  
        
        for (Integer i = 0; i < Trigger.new.size(); i++) {
            if ((Trigger.old[i].Billing_Street_Address_Line_1__c != Trigger.new[i].Billing_Street_Address_Line_1__c) ||
                (Trigger.old[i].Billing_Street_Address_Line_2__c != Trigger.new[i].Billing_Street_Address_Line_2__c) ||
                (Trigger.old[i].Billing_Street_Address_Line_3__c != Trigger.new[i].Billing_Street_Address_Line_3__c) ||
                (Trigger.old[i].Billing_Street_Address_Line_4__c != Trigger.new[i].Billing_Street_Address_Line_4__c) ||
                (Trigger.old[i].BillingCity__c != Trigger.new[i].BillingCity__c) ||
                (Trigger.old[i].BillingState__c != Trigger.new[i].BillingState__c) ||
                (Trigger.old[i].BillingPostalCode__c != Trigger.new[i].BillingPostalCode__c) ||
                (Trigger.old[i].BillingCountry__c != Trigger.new[i].BillingCountry__c)) {
                    acctsWithNewAddresses.put(Trigger.old[i].id, Trigger.new[i]);
                }
        }
    
        List<Contact> updatedContacts = new List<Contact>();
    
        //Here we can see two syntatic features of Apex:  
        
        //  1) iterating over an embedded SOQL query  
        
        //  2) binding an array directly to a SOQL query with 'in'  
        
    
           
        for (Contact c : [SELECT id, accountId, MailingCity, MailingCountry, MailingPostalCode, MailingState, MailingStreet FROM contact WHERE accountID in :acctsWithNewAddresses.keyset()]) {
            
            Account a = acctsWithNewAddresses.get(c.accountId);
                
            String streetAddress = (a.Billing_Street_Address_Line_1__c != null ? a.Billing_Street_Address_Line_1__c : '') +
                                   (a.Billing_Street_Address_Line_2__c != null ? '\n' + a.Billing_Street_Address_Line_2__c : '') +
                                   (a.Billing_Street_Address_Line_3__c != null ? '\n' + a.Billing_Street_Address_Line_3__c : '') +
                                   (a.Billing_Street_Address_Line_4__c != null ? '\n' + a.Billing_Street_Address_Line_4__c : '');
            
            c.MailingCity = a.BillingCity__c;
            c.MailingCountry = a.BillingCountry__c;
            c.MailingPostalCode = a.BillingPostalCode__c;
            c.MailingState = a.BillingState__c;
            c.MailingStreet = streetAddress;        
            
            updatedContacts.add(c);
        }
        
        update updatedContacts;
        
        /*commenting code as part of sf-3812
        // *****************Updating Secondary Claim Handler details
        
        Map<Id, Account> acctsWithNewSecondClaimHandler = new Map<Id, Account>();
    
        // Trigger.new is a list of the Accounts that will be updated  
        
        // This loop iterates over the list, and adds any that have new  
        
        // addresses to the acctsWithNewSecondClaimHandler map.  
        //SF-3812 - Start - Removed references od ID field since we lhave lookup(user) fields available
        for (Integer i = 0; i < Trigger.new.size(); i++) {
            if ((Trigger.old[i].Claim_Adjuster_Marine_lk__c != Trigger.new[i].Claim_Adjuster_Marine_lk__c) ||
                (Trigger.old[i].Claim_handler_Cargo_Dry_2_lk__c != Trigger.new[i].Claim_handler_Cargo_Dry_2_lk__c) ||
                (Trigger.old[i].Claim_handler_Cargo_Liquid_2_lk__c != Trigger.new[i].Claim_handler_Cargo_Liquid_2_lk__c) ||
                (Trigger.old[i].Claim_handler_CEP_2_lk__c != Trigger.new[i].Claim_handler_CEP_2_lk__c) ||
                (Trigger.old[i].Claim_handler_Charterers_2_lk__c  != Trigger.new[i].Claim_handler_Charterers_2_lk__c) ||
                (Trigger.old[i].Claim_handler_Crew_2_lk__c != Trigger.new[i].Claim_handler_Crew_2_lk__c) ||
                (Trigger.old[i].Claim_handler_Defence_2_lk__c != Trigger.new[i].Claim_handler_Defence_2_lk__c) ||
                (Trigger.old[i].Claim_handler_Energy_2_lk__c != Trigger.new[i].Claim_handler_Energy_2_lk__c ) ||
                (Trigger.old[i].Claim_handler_Marine_2_lk__c != Trigger.new[i].Claim_handler_Marine_2_lk__c ) ||
                (Trigger.old[i].Claims_handler_Builders_Risk_2_lk__c != Trigger.new[i].Claims_handler_Builders_Risk_2_lk__c )) {
                    acctsWithNewSecondClaimHandler.put(Trigger.old[i].id, Trigger.new[i]);
                }
        }
        List<Account> updatedAccount = new List<Account>();
        
        for (Account acc : [SELECT id, Claim_Adjuster_Marine_lk__c, Claim_handler_Cargo_Dry_2_lk__c, Claim_handler_Cargo_Liquid_2_lk__c, 
                          Claim_handler_CEP_2_lk__c, Claim_handler_Charterers_2_lk__c, Claim_handler_Crew_2_lk__c, Claim_handler_Defence_2_lk__c,
                          Claim_handler_Energy_2_lk__c, Claim_handler_Marine_2_lk__c, Claims_handler_Builders_Risk_2_lk__c from Account
                          where ID in :acctsWithNewSecondClaimHandler.keyset()]) {
            
            Account a = acctsWithNewSecondClaimHandler.get(acc.id);
            
            
            acc.Claim_Adjuster_Marine_lk__c = a.Claim_Adjuster_Marine_lk__c;
            acc.Claim_handler_Cargo_Dry_2_lk__c = a.Claim_handler_Cargo_Dry_2_lk__c;  
            acc.Claim_handler_Cargo_Liquid_2_lk__c = a.Claim_handler_Cargo_Liquid_2_lk__c;      
            acc.Claim_handler_CEP_2_lk__c = a.Claim_handler_CEP_2_lk__c;
            acc.Claim_handler_Charterers_2_lk__c = a.Claim_handler_Charterers_2_lk__c;
            acc.Claim_handler_Crew_2_lk__c = a.Claim_handler_Crew_2_lk__c;
            acc.Claim_handler_Defence_2_lk__c = a.Claim_handler_Defence_2_lk__c;
            acc.Claim_handler_Energy_2_lk__c = a.Claim_handler_Energy_2_lk__c ;
            acc.Claim_handler_Marine_2_lk__c = a.Claim_handler_Marine_2_lk__c ;
            acc.Claims_handler_Builders_Risk_2_lk__c = a.Claims_handler_Builders_Risk_2_lk__c ;
            
            updatedAccount.add(acc);
        }
        //SF-3812 - End
        update updatedAccount;*/
        
    }
    
    if(Trigger.isbefore){
        /*
        //This part is commented out because of SF 4362, where there is a 
        //need to enable companies for portal access by externals.
        //To stop non admin users from creating partner accounts
        String profileName = [SELECT Name from profile where id = :userinfo.getProfileId()].name;
        
        System.debug('Trigger.old[0].ispartner-->' + Trigger.old[0].ispartner);
        System.debug('Trigger.new[0].ispartner-->' + Trigger.new[0].ispartner);
        System.debug('profileName-->' + profileName);
        
        if(Trigger.old[0].ispartner == false && Trigger.new[0].ispartner == true && !profileName.equalsIgnoreCase('System Administrator')){
           // Trigger.new[0].addError('To enable Partner Account please contact your system administrator');
        }
        */
        
        //START - SF 4469
        //populateTeamInformation();
        Set<String> countryIdSet = new Set<String>();
        for (Account a : Trigger.new)
            countryIdSet.add(a.country__c);
        Map<String, Country__c> countryMap = new Map<String, Country__c>();
        for (Country__c c : [SELECT id, Gard_Team__c, Claims_Support_Team__c from Country__c where id IN :countryIdSet]){
            countryMap.put(c.id,c);    
        }
        
        //SF - 4569 START
        List <Gard_Team__c> CTST_Team = [SELECT id from Gard_Team__c WHERE Region_code__c = 'CTST'];
        String CTST_Team_Id = null;
        if(CTST_team != null && CTST_Team.size() > 0){
            CTST_Team_Id = CTST_Team[0].id;
        }
        //SF - 4569 END
        
        for(Account a : Trigger.new){
           //Copy the CS info from Country record //changed for sf 4469, to udpate only when cs is empty.
           if(a.Claims_Support_Team__c == null && countryMap != null && countryMap.size()>0){
               a.Claims_Support_Team__c = countryMap.get(a.country__c).Claims_Support_Team__c;
           }
           //if the Gard Team is empty, then only copy it from country's gard team info
           if(a.Gard_Team__c == null && countryMap != null && countryMap.size()>0){
               //CHATR checking added for SF-4569/ to nullify effect of SF-4469
               //So that for Market area = CHATR, it does not assign any CT. 
               //The next batch for SF-4569 will fill up CT eventually.
               if(a.Market_Area_Code__c != 'CHATR')    
                   a.Gard_Team__c = countryMap.get(a.country__c).Gard_Team__c;
               else
                   a.gard_team__c = CTST_Team_Id;
                   
           }        
        }
        //END - SF 4469
        
    }
    
    /*
    populateTeamInformation(){
        List<String> countryIdSet = new Set<String>();
        for (Account a : Trigger.new)
            countryIdSet.add(a.country__c);
        List<String, Country__c> countryMap = new List<String, Country__c>();
        for (Country__c : [SELECT id, Gard_Team__c, Claims_Support_Team__c from Country__c where id IN :countryIdSet){
            countryMap.put(c.id,c);    
        }
        
        for(Account a : Trigger.new){
           //If the CS Team is empty, copy it from country's gard team info
           if(newRecord.Claims_Support_Team__c == null)
               a.Claims_Support_Team__c = countryMap.get(a.country__c).Claims_Support_Team__c;
           
           //if the Gard Team is empty, copy it from country's gard team info
           if(newRecord.Gard_Team__c == null)
               a.Gard_Team__c = countryMap.get(a.country__c).Gard_Team__c;
        }
    }
    */
                            
}