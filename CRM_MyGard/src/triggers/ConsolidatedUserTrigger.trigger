trigger ConsolidatedUserTrigger on User (after insert, after update, before insert, before update) //before part added for MYGP 190 
{
    Integer ALLOWED_MAX_BATCHES = 100;//SF-4663 changed from 3 to 100,this should be removed later
    Boolean isBatchExecuted = true;
    Map<ID, User> contactIdUserMap = new Map<ID, User>();
    Integer runningBatchJobs;
    
    if(Trigger.isBefore && Trigger.isInsert || Trigger.isBefore && Trigger.isUpdate){
        for(User anUser : Trigger.New){
          //  if(anUser.Username.contains('.mygard')){
                contactIdUserMap.put(anUser.ContactId, anUser);
           // }
        }   
        
        List<Contact> allContacts = [SELECT Id, Account.Name FROM Contact where ID IN: contactIdUserMap.keySet()];
        for(Contact aContact : allContacts){
            contactIdUserMap.get(aContact.ID).companyName = aContact.Account.Name;
        }
    }
    
    if(Trigger.isAfter && Trigger.isInsert)
    {
       // if(GardUtils.inCreateContactRecordFirst) - need to check 
        { 
            GardUtils.createContactRecord(Trigger.NewMap.keyset());
        }
        GardUtils.createContactShare(Trigger.NewMap.keyset());
        if(!test.isRunningTest()){
            runningBatchJobs = [
                select count()
                    from AsyncApexJob
                    where JobType = 'BatchApex'
                    and status in ('Queued','Processing','Preparing')
            ];
        }else{//for test coverage
            runningBatchJobs = ALLOWED_MAX_BATCHES-1;
        }
            if(runningBatchJobs >= ALLOWED_MAX_BATCHES) {
                isBatchExecuted = false;
            }
            
            else{
                isBatchExecuted = true;
                /* 
                Commented out for modified class and method signature //SF 56 //Nov 16
                
                    BatchInsertMyCover_bkp  prodBatch_true = new BatchInsertMyCover_bkp('true');
                    database.executebatch(prodBatch_true, 20);
                    BatchInsertMyCover_bkp  prodBatch_false = new BatchInsertMyCover_bkp('false');
                    Database.executebatch(prodBatch_false, 1);
                 */   
                
                // -New code - sf - 56//
                
                String existingContactId = Trigger.new[0].contactId;
                if(existingContactId != null && existingContactId != ''){
                
                    String accountId = [SELECT accountID from Contact where id = :existingContactId].accountID;
                    Integer count = [SELECT count() from Contact where accountId = :accountId];    // if count == 1, this is the first user created after company is enabled for MyGard
                    
                    if(count == 1){              
                        List<String> accountIdList = new List<String>();
                        accountIdList.add(accountId);
                        BatchInsertMyCover2  prodBatch_true = new BatchInsertMyCover2('true', accountIdList);
                        database.executebatch(prodBatch_true, 20);
                        
                        BatchInsertMyCover2  prodBatch_false = new BatchInsertMyCover2('false', accountIdList);
                        database.executebatch(prodBatch_false, 1);
                    }
                }
                
                // -New code - sf - 56//
            }
        //} //Test.isRunningTest scope shifted
        if(!isBatchExecuted){
            for(User anUser : Trigger.new){
                anUser.addError('No resources available.  Please try again later.');
            }
        }        
         //on user insert add a record in account contact junction object
        GardUtils.createAccountContactJuncRecord(Trigger.NewMap.keyset());
    }    
    if(Trigger.isAfter && Trigger.isUpdate && !GardUtils.flag /* && GardUtils.inUpdateContactRecordFirst && GardUtils.inUpdateUserFirst *** need to check */  ) //MYG-1336
    {   
        //GardUtils.updateContactRecord(Trigger.NewMap.keyset());
        if(System.isFuture()||System.isBatch()){
            GardUtils.updateContactRecord(Trigger.NewMap.keyset()); 
        }else{
            GardUtils.updateContactRecordFuture(Trigger.NewMap.keyset());   
        }        
        // for multicompany juction object          
        Set<Id> deactivContIdSet = new Set<Id>();
        //Set<Id> activContIdSet = new Set<Id>(); //not needed according to issue MYGP-217
        
        for(User usr: Trigger.NewMap.values())
        {
            if(usr.isActive == false)
            {                   
                deactivContIdSet.add(usr.ContactId);
            }
            //not needed according to issue MYGP-217
            /*if(usr.isActive == true && OldMap.get(usr.id).isActive == false)
            {
                activContIdSet.add(usr.ContactId);
            }*/
        }
        // method call deactivate all junction object record on user deactivation
        if(deactivContIdSet.size() >0)
        {
            if(System.isFuture()||System.isBatch())
            {
                GardUtils.updateMultiCompanyJuncObj(deactivContIdSet);
            }else
            {
                GardUtils.updateMultiCompanyJuncObjFuture(deactivContIdSet); 
            }   
        }
        //not needed according to issue MYGP-217        
        /* 
        // method call activate parent account junction object record on user activation
        if(activContIdSet.size() >0)
        {
            if(System.isFuture()||System.isBatch())
            {
                GardUtils.updateMultiCompany(activContIdSet);
            }else
            {
                GardUtils.updateMultiCompanyFuture(activContIdSet); 
            } 
        }  */              
    } 
    
    // ***************** START: MYG-1497
    if(trigger.isUpdate && trigger.isAfter)
    {
        if(System.isFuture()||System.isBatch())
        {
            GardContactUtil.updateGardContacts(trigger.newMap.keySet());
        }else
        {
            GardContactUtil.updateGardContactsFuture(trigger.newMap.keySet()); 
        }     
      
      
      /*List<Id> allGardContactIds = new List<Id>();
      List<Gard_Contacts__c> deactivatedUserContact = new List<Gard_Contacts__c>();
    
      for(User anUser : trigger.new){
        if(!anUser.isActive && anUser.ContactId__c!=null){
            System.Debug('*** Got inactive user');
            allGardContactIds.add(anUser.ContactId__c);
        }else if(anUser.isActive && anUser.ContactId__c!=null){
            System.Debug('*** Got Active user');
            allGardContactIds.add(anUser.ContactId__c);
        }
      }
    
       if(allGardContactIds.size() > 0){
         System.Debug('*** About the change');
         GardContactUtil.updateGardContacts(allGardContactIds); 
      }  */
    }   
    // ****************** END: MYG-1497
    
    //MYGP 190 - START
    Boolean showError = false;
    Boolean isGardStandardUser = false;
    String profileName=[Select Id,Name from Profile where Id = :Userinfo.getProfileId()].Name;
    System.debug('Profile Name : ' + profileName);
    if(profileName.equalsIgnoreCase('Gard Standard User (UWR)') || profileName.equalsIgnoreCase('Gard Standard User')){
        isGardStandardUser = true;
    }
    
    //Bulk handling not required since users are not uploaded so.
    
    //System.debug('1--old--' + Trigger.old[0].UserPreferencesHideS1BrowserUI);
    //System.debug('2--new--' + Trigger.new[0].UserPreferencesHideS1BrowserUI); 
    
    System.debug('SF-5163 ConsolidatedUserTrigger>beforeIF UserId - '+Userinfo.getUserId());
    System.debug('SF-5163 ConsolidatedUserTrigger>beforeIF UserProfileName - '+profileName);
    if(isGardStandardUser || Test.isRunningTest()){ 
        /* //previously added for user create SF Flow
        if(Trigger.isBefore && Trigger.isInsert && ([SELECT count() from Allow_user_Creation__c] == 0)){ //last part is added to bypass user creation flow
            showError = true;
        }*/
        System.debug('SF-5163 ConsolidatedUserTrigger>insideIF UserId - '+Userinfo.getUserId());
        System.debug('SF-5163 ConsolidatedUserTrigger>insideIF UserProfileName - '+profileName);
        if(Trigger.isBefore && Trigger.isUpdate){
            /*System.debug('Trigger.old[0].ProfileId-->' + Trigger.old[0].ProfileId);
            System.debug('Trigger.new[0].ProfileId-->' + Trigger.new[0].ProfileId);
            System.debug('Trigger.old[0].isActive-->' + Trigger.old[0].isActive);
            System.debug('Trigger.new[0].isActive-->' + Trigger.new[0].isActive);*/
            
            if(
                (Trigger.old[0].ProfileId != Trigger.new[0].ProfileId)  ||
                (Trigger.old[0].isActive == false && Trigger.new[0].isActive == true)    ||   // ---> MYGP-132
                (Trigger.old[0].username != Trigger.new[0].username) ||
                
                //(Trigger.old[0].CommunityNickname != Trigger.new[0].CommunityNickname) ||
                //(Trigger.old[0].title != Trigger.new[0].title) ||
                //(Trigger.old[0].CompanyName != Trigger.new[0].CompanyName) ||
                //(Trigger.old[0].department != Trigger.new[0].department) ||
                //(Trigger.old[0].alias != Trigger.new[0].alias) ||
                //(Trigger.old[0].email != Trigger.new[0].email) ||
                
                (Trigger.old[0].division != Trigger.new[0].division) ||
                (Trigger.old[0].UserPermissionsMarketingUser != Trigger.new[0].UserPermissionsMarketingUser) ||
                
                (Trigger.old[0].UserPermissionsChatterAnswersUser != Trigger.new[0].UserPermissionsChatterAnswersUser) ||
                (Trigger.old[0].UserPermissionsMobileUser != Trigger.new[0].UserPermissionsMobileUser) ||
                //(Trigger.old[0].UserPermissionsJigsawProspectingUser != Trigger.new[0].UserPermissionsJigsawProspectingUser) || //Commented out fo5r SF-5163 and SF-5165
                (Trigger.old[0].UserPermissionsSFContentUser != Trigger.new[0].UserPermissionsSFContentUser) ||
                
                //(Trigger.old[0].UserPreferencesHideS1BrowserUI != Trigger.new[0].UserPreferencesHideS1BrowserUI) ||
                
                
                
                (Trigger.old[0].ReceivesInfoEmails != Trigger.new[0].ReceivesInfoEmails) ||
                (Trigger.old[0].ForecastEnabled != Trigger.new[0].ForecastEnabled) ||
                (Trigger.old[0].extension != Trigger.new[0].extension) ||
                (Trigger.old[0].EmailEncodingKey != Trigger.new[0].EmailEncodingKey) ||
                (Trigger.old[0].isnolongeremployed__c != Trigger.new[0].isnolongeremployed__c) ||
                (Trigger.old[0].gic_id__c != Trigger.new[0].gic_id__c) ||
                (Trigger.old[0].paris_id__c != Trigger.new[0].paris_id__c) ||
                (Trigger.old[0].position__c != Trigger.new[0].position__c) ||
                (Trigger.old[0].pilot_user__c != Trigger.new[0].pilot_user__c) ||
                (Trigger.old[0].license_required__c != Trigger.new[0].license_required__c) ||
                (Trigger.old[0].ET_Username__c != Trigger.new[0].ET_Username__c) ||
                (Trigger.old[0].ET_password__c != Trigger.new[0].ET_password__c) ||
                (Trigger.old[0].contactid__c != Trigger.new[0].contactid__c) ||
                (Trigger.old[0].TimeZoneSidKey != Trigger.new[0].TimeZoneSidKey) ||
                (Trigger.old[0].LocaleSidKey != Trigger.new[0].LocaleSidKey) ||
                (Trigger.old[0].LanguageLocaleKey != Trigger.new[0].LanguageLocaleKey) ||
                
                (Trigger.old[0].CurrencyIsoCode != Trigger.new[0].CurrencyIsoCode) ||
                (Trigger.old[0].DelegatedApproverId != Trigger.new[0].DelegatedApproverId) ||
                (Trigger.old[0].ManagerId != Trigger.new[0].ManagerId) ||
                (Trigger.old[0].LanguageLocaleKey != Trigger.new[0].LanguageLocaleKey) ||
                (Trigger.old[0].ReceivesAdminInfoEmails != Trigger.new[0].ReceivesAdminInfoEmails)
                
              ) 
            {
                if(!trigger.new[0].username.equalsIgnoreCase(Userinfo.getusername())){
                    showError = true;
                }    
            }
        }
        if(showError && !Test.isRunningTest()){
            Trigger.new[0].addError(Label.StandardUserCanNotEditPartnerUsers);
        }
    }
    //MYGP 190 - END
}