trigger Event_BeforeDelete on Event (before delete) {
    /************************************************************************
     * Date: 23/07/2012
     * Author: Martin Gardner
     * Company: cDecisions Ltd.
     * Description: Delete trigger for Events
     * Issue00301
     * When an event is deleted, if there are published meeting minutes associated, 
     * the delete should be prevented. However, if the associated meeting minutes 
     * are not published, and there are attendees, the user should see a warning to 
     * remove the attendees before deleting. If there are no attendees, the event 
     * and associated (non-published) meeting minute record should be deleted.  
          
     * Modifications:
     * 14/05/2013 Steve O'Connell
     * Meeting minutes for the event are not deleted. Added MM delete. 
     *
     * Modifications:
     * SF- 3877 :
     * 21/08/2017 Shreyashi Sarkar
     ************************************************************************/  
    if (Trigger.isBefore) {
        if (Trigger.isDelete) {
    
            // In a before delete trigger, the trigger accesses the records that will be 
        
            // deleted with the Trigger.old list. 
            
                Set<Id> evtIds = new Set<Id>();
                /*
                List<Meeting_Minute__c> mmToDelete = new List<Meeting_Minute__c>();
                // added for SF - 3877
                List<Task> taskToDelete = new List<Task>();
                // end of SF-3877
                for(Event e : trigger.old){
                    evtIds.add(e.Id);
                }
                List <Meeting_Minute__c> theMinutes = [Select   m.Name, 
                                                                m.Event_Id__c, 
                                                                m.Id, 
                                                                m.Event_Subject__c, 
                                                                m.Event_StartDateTime__c, 
                                                                m.Event_Location__c, 
                                                                m.Event_EndDateTime__c,
                                                                m.Distributed__c,
                                                                m.Attendee_Count__c  
                                               From Meeting_Minute__c m 
                                               where event_id__c in : evtIds];
                Map<Id, Meeting_Minute__c> mapMinutes = new Map<Id, Meeting_Minute__c>();
                for(Meeting_Minute__c m : theMinutes){
                    mapMinutes.put(m.Event_Id__c, m);
                }
                                               
                for(Event evt: trigger.old){
                    //for each event determine if it has minutes and whether or not they are published
                    if(mapMinutes.containsKey(evt.Id)){
                        //meeting minutes exist for this event
                        //Check for the status of the minutes
                        Meeting_Minute__c thisMinute = mapMinutes.get(evt.Id);
                        if(thisMinute.Distributed__c){
                            //the meeting minutes have been published do not allow the
                            //event to be deleted
                            evt.addError('Published meeting minutes exist for this Event. You may not delete this record.');
                        }else{
                            //the meeting minutes have not been published
                            //check for attendees other than the assignee on the event 
                            //of attendees as a minimum
                            
                            List<EventAttendee> evtAtt = new List<EventAttendee>();
                            evtAtt = [SELECT AttendeeId FROM EventAttendee WHERE EventId = :evt.Id AND AttendeeId != :evt.OwnerId];
                            
                            if(evtAtt.size()>0){
                                //there are attendees
                                String errorMessage = 'There are attendees associated to the meeting minutes of this event.\nPlease remove the attendees before deleting the event.';
                                evt.addError(errorMessage);
                            }else{
                                //If there are no attendees, the event and associated (non-published) meeting minute record should be deleted.
                                //SOC 14/5/13 - Add MM to delete list
                                mmToDelete.Add(mapMinutes.get(evt.Id));
                            }
                        }
                    }else{
                        //meeting minutes do not exist for this event
                        //allow the delete to contintue
                    }
                } 
                
                if(mmToDelete.size() > 0)
                {
                    delete mmToDelete;
                } 
                // added for SF- 3877
                List <Task> theTask = [SELECT Id,Meeting_ID__c,Status,Subject,Type,WhatId,WhoId FROM Task where Meeting_ID__c in : evtIds];
                if(!theTask.isEmpty())
                {
                     for(Task t : theTask){
                        //if(t.Subject.containsIgnoreCase('Complete Meeting minutes')){
                          taskToDelete.Add(t);
                       // }
                      }
                }
                if(taskToDelete.size() > 0)
                {
                    delete taskToDelete;
                }   
                // end of SF - 3877    
                */                                         
            }
    }
}