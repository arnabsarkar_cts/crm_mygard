trigger ContactTrigger on Contact (before update,after update) {    
    if(Trigger.isUpdate && Trigger.isAfter){
        List<Contact> failedContacts = new List<Contact>();
        List<Id> contactIdsToSetNotifyAdminFalse = new List<Id>();
        List<Id> contactIdsToSetNotifyAdminTrue = new List<Id>();
        List<Id> NlEConIdList = new List<Id>();
        List<AccountContactRelation> accConRelList = new List<AccountContactRelation>();//SF-5016
        List<AccountContactRelation> DelAccConRelList = new List<AccountContactRelation>();//SF-5016
        List<AccountContactRelation> UpdateAccConRelList = new List<AccountContactRelation>();//SF-5016
        for(Contact aContact : Trigger.new){
            if(aContact.Synchronisation_Status__c == 'Synchronised' && aContact.Notify_Admin__c){
                //Update Contact notify admin flag to false
                contactIdsToSetNotifyAdminFalse.add(aContact.Id);
            }else if(aContact.Synchronisation_Status__c == 'Sync Failed' && !aContact.Notify_Admin__c){
                //Update Contact notify admin flag to true
                contactIdsToSetNotifyAdminTrue.add(aContact.Id);
                failedContacts.add(aContact);
            }
        }
        
        if(contactIdsToSetNotifyAdminFalse.size() > 0 && !test.isRunningTest() ){
            ContactHandler.updateContactRecordForNotifyAdmin(false, contactIdsToSetNotifyAdminFalse);
        }
        if(contactIdsToSetNotifyAdminTrue.size() > 0 && !test.isRunningTest() ){
            ContactHandler.updateContactRecordForNotifyAdmin(true, contactIdsToSetNotifyAdminTrue);
        }
        //START -- Code for Contact Sync Failed Notification - Send Email
        if(failedContacts.size() > 0){
            Sys_Admin_Email_Synch_Failed__c contactSyncFailedEmailNotification = Sys_Admin_Email_Synch_Failed__c.getValues('Contact Sync Failed');
            if(Test.isRunningTest()){
                MailUtil.sendContactSyncFailureMail(failedContacts);
            }
            else if(contactSyncFailedEmailNotification.Should_Send_Email__c){
                MailUtil.sendContactSyncFailureMail(failedContacts);
            }
        }
        //END -- Code for Contact Sync Failed Notification - Send Email
         //SF-3907 when a contact is set as NLE, all contact roles associated with it is deleted
        for(Contact aContact : Trigger.new){
            if(aContact.No_Longer_Employed_by_Company__c == true){
                NlEConIdList.add(aContact.Id);
            }
        }
        //SF-5016 Added by GEETHAM
        if(NlEConIdList!=null && NlEConIdList.size()>0)
        {
            accConRelList  = [Select id,ContactId,IsDirect,Roles,No_Longer_Employee__c FROM AccountContactRelation WHERE ContactId IN: NlEConIdList];
        }
        for(AccountContactRelation aCRel : accConRelList)
        {
            if(!aCRel.IsDirect)
            {
                DelAccConRelList.add(aCRel);
            }
            else
            {
                UpdateAccConRelList.add(aCRel);
                system.debug('aCRel--GG--'+aCRel+'--UpdateAccConRelList--'+UpdateAccConRelList.size());
            }
        }
        for(AccountContactRelation aCRel : UpdateAccConRelList)
        {
            aCRel.Roles = '';
            aCRel.No_Longer_Employee__c = true;
        }
        if(UpdateAccConRelList!=null && UpdateAccConRelList.size()>0)
        {
            update UpdateAccConRelList;
        }
        if(DelAccConRelList!=null && DelAccConRelList.size()>0)
        {
            delete DelAccConRelList;
        }
        //SF-5016 Added by GEETHAM
    }
    
                                //////////////////////////////////////
                                ///     Added in Phase2Dev       ///
                                ////////////////////////////////////

// CR No. MYG2A-3: Automatic deactivation of MyGard user when the corresponding contact is flagges as no longer employed.
// used to capture contact's ids whose noLongerEmployed field is true      
    if(trigger.isUpdate && trigger.isAfter)
    {        
        set<Id> updatedContactset = new set<Id>();
        for(Contact cnt: trigger.New)
        {
            if(cnt.No_Longer_Employed_by_Company__c == true)
                updatedContactset.add(cnt.id);                                      
        }        
        if(updatedContactset.size() >0)
        { 
            if(System.isFuture() || System.isBatch())
                GardUtils.deactivateUsr(updatedContactset);
            else
                GardUtils.deactivateUsrFuture(updatedContactset);
        }
        /*if(System.isFuture()||System.isBatch()){
          GardUtils.updateUserRecord(Trigger.NewMap.keyset());
        }else{
          GardUtils.updateUserRecordFuture(Trigger.NewMap.keyset());  
        } 
        */
    }    
// restrict account change if linked with multiple accounts     
    if(trigger.isUpdate && trigger.isBefore)
    {
        GardUtils.restrictAccountUpdate(trigger.OldMap,Trigger.NewMap);              
    }
    //MYGP-50
    
    if(trigger.isUpdate && trigger.isAfter){ 
     GardUtils.updateUserRecord(Trigger.NewMap.keyset());
      /*  if(System.isFuture()||System.isBatch()){
         if(GardUtils.inUpdateUserRecordFirst){
                  GardUtils.updateUserRecord(Trigger.NewMap.keyset());
         }else{
                  GardUtils.updateUserRecordFuture(Trigger.NewMap.keyset());  
         }
       }
       */ 
             
    } 
     
}