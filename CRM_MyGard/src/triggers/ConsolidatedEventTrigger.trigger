trigger ConsolidatedEventTrigger on Event (before delete,after insert, after update, after delete) {
    system.debug('Number of times it is called');
    Event e_new, e_old;
    List<FeedItem> feedItems = new List<FeedItem>();
    list<id> eventWhoIds = new list<id>();
    list<id> eventIds = new list<id>();
    List<Id> allRelationIds = new List<Id>();
    Set<ID> ownerIds = new Set<ID>();
    
    if(!Trigger.isInsert && !Trigger.old.isEmpty()) e_old = Trigger.old[0];
    if(trigger.isInsert || trigger.isUpdate)
    {
         e_new = Trigger.new[0];
        String UserId = UserInfo.getUserId(); 
        for(Event newEvent : Trigger.new){
        eventWhoIds.add(newEvent.whoId);
        eventIds.add(newEvent.id);
        ownerIds.add(newEvent.ownerId);
     }
    }
    
    
     Map<ID,User> userMap = new Map<ID,User>([SELECT ID, Name FROM User WHERE ID IN :ownerIds]); //This is our user map
     
     if(trigger.isAfter && (trigger.isInsert||trigger.isUpdate))
     {
        //EventRelationHelper.FetchEventRelationContact(Trigger.new);
        List<Contact> contactList = [ select Id,Name from Contact where id IN: eventWhoIds and No_Longer_Employed_by_Company__c=true];
        List<EventWhoRelation> allEventRelations = [SELECT ID, RelationId FROM EventWhoRelation WHERE EventId=:e_new.id];
        system.debug('allEventRelations****'+allEventRelations.size()+'****'+allEventRelations);
        for(Event newEvent : Trigger.new){
        if(contactList.size() >0){
            newEvent.addError('You cannot create an event with invitees who are no longer employed by company');
        }
        else{
            
            for(EventWhoRelation  anEventRelation : allEventRelations){
                allRelationIds.add(anEventRelation.RelationId);
                }
            
            }
        }
        if(allRelationIds.size() > 0){
            List<Contact> notEmployeedContactList = [SELECT ID, Name from Contact where id IN: allRelationIds and No_Longer_Employed_by_Company__c=true];
            for(Event newEvent : Trigger.new){
                for(contact con:notEmployeedContactList){
                    newEvent.addError('You cannot create an event with invitees who are no longer employed by company');
                }
            
            }
        }
     }
     
//---- Insert Starts-----
    if (Trigger.isAfter && trigger.isInsert){
      if(!Gard_RecursiveBlocker.blocker)
      {
         Gard_RecursiveBlocker.blocker = true;
         EventRelationHelper.HirerachyEvent(e_new);
      }
        for(Event e_new:trigger.new){
            if(e_new.WhatId != null && (e_new.Type!= null && e_new.Type.equalsignoreCase('meeting'))){
                //Create task to fill up meeting minutes
              if(!e_new.IsDuplicate__c)
              {
                    Task t = new Task();
                    t.whatId = e_new.WhatId;
                    t.meeting_id__c = e_new.id;
                    t.meeting_name__c = e_new.subject;
                    t.OwnerId = e_new.OwnerId;
                    t.Subject = 'Complete Meeting minutes - ' + e_new.subject;
                    t.Priority = 'Normal';                
                    t.ActivityDate = date.newinstance(e_new.EndDateTime.year(), e_new.EndDateTime.month(), e_new.EndDateTime.day()); //SF 3863
                    //t.ActivityDate = e_new.EndDateTime; //SF 3863
                    t.Status = 'Not Started';
                    // added for SF-3853
                    t.isComplete__c= true;
                    // end of SF-3853
                    //if(e_new.Minutes_of_Meeting__c != null && e_new.Minutes_of_Meeting__c != '')
                    if(e_new.Description != null && e_new.Description != '') 
                    {
                        t.Status = 'Completed';
                    } //This is to ensure to take scenario, where the minutes are filled while creating meetings
                    insert t;
                }
                //publish minutes of meeting in company chatter feed
                //commented for 3837 - Aritra
                //if(e_new.Minutes_of_Meeting__c != null && e_new.Minutes_of_Meeting__c != ''){
               
               //************Added this if condition for 4072.. Date - 19/01/2018************//         
                if(e_new.WhatId != null && (e_new.WhatId.getsObjectType() == Opportunity.sObjectType)){
                    if(e_new.Type != null && e_new.Type.equalsignoreCase('meeting')){
                        FeedItem fitem = new FeedItem(); 
                        //******If, event is linked with an oppty then it will link the chatter post to opportunity's Company******4072.. Date - 19/01/2018
                        Id accId = [select id,AccountId from Opportunity where id=:e_new.WhatId].AccountId;
                        fitem.type = 'LinkPost';
                        fitem.ParentId = accId;
                        fitem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e_new.id;
                        fitem.Title = 'View Meeting'; 
                        fitem.Body = 'New Meeting' + ((e_new.Location__c) != null ? ', ' + e_new.Location__c : '') 
                                + '\nStart:' + e_new.StartDateTime.format() + ',  End:' + e_new.EndDateTime.format()
                                + '\nSubject: ' + e_new.Subject;
                                
                        insert fitem;
                    }               
                }
            }
        }
        
         //Now loop though the new/updated tasks and create the feed posts
    for (Event e : Trigger.new) {
        if (e.WhatId != null && !e_new.Ultimate_Account_Parent__c) {//SF-4749
            if (e.Type == 'Meeting') {
                FeedItem fitem = new FeedItem();
                fitem.type = 'LinkPost';
                fitem.ParentId = e.WhatId;
                fitem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e.id;
                fitem.Title = 'View Meeting';
    
                //Get the user by checking the userMap we created earlier
                User assignedTo = userMap.get(e.ownerId);
    
                fitem.Body = 'New Meeting' + ((e.Location__c) != null ? ', ' + e.Location__c : '') 
                            + '\nStart:' + e.StartDateTime.format() + ',  End:' + e.EndDateTime.format()
                            + '\nSubject: ' + e.Subject;
                            
                feedItems.add(fitem);
            }
        }
    }

    //Save the FeedItems all at once.
    if (feedItems.size() > 0) {
        Database.insert(feedItems,false); //notice the false value. This will allow some to fail if Chatter isn't available on that object
    }
        
    }
//----insert ends----
//--- Update Starts -----
    if(Trigger.isUpdate){
       //List<FeedItem> feedIems1 = new List<FeedItem>();
            /* SF-3936-- Starts */
            List<Event> evntlst = new List<Event>();
            set<ID> EventWhatIDS = new set<ID>();
           if(e_new.WhatId != null)
            {
                
                String strTemp = String.valueOf(e_new.id).substring(0,15).trim();
               
                EventRelationHelper.FetchChildRecord(e_new,strTemp);
                system.debug('child events: '+evntlst);
              
             String str = String.valueOf(e_new.id).substring(0, 15);
            EventRelationHelper.UpdateChildRecordInsert(str);
            }
            //Added for SF - 4512 - GEETHAM
            if(e_new.whatid!=e_old.whatId)
            {
                List<Task> taskList= [SELECT id, subject,WhatId,status, activityDate from Task where Meeting_ID__c = :e_new.id];//limit 1 // AND status != 'Completed'
                system.debug('The e_new.id---->'+e_new.id);
                system.debug('taskList--->'+taskList.size());
                if(taskList.size()>0)
                {
                      if(e_new.IsDuplicate__c == false)
                      {
                          taskList[0].WhatId = e_new.WhatId;
                      }
                       List<FeedItem> FeedItemToBeDeleted = [select id,parentId from FeedItem where ParentId =:e_old.WhatId];
                       system.debug('FeedItemToBeDeleted--->'+FeedItemToBeDeleted.size());
                        if(FeedItemToBeDeleted.size()>0)
                         {
                             Delete FeedItemToBeDeleted;
                         }
                         FeedItem fitem = new FeedItem();
                         fitem.type = 'LinkPost';
                         fitem.ParentId = e_new.WhatId;
                         fitem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e_new.id;
                         fitem.Title = 'View Meeting';
    
                            //Get the user by checking the userMap we created earlier
                                User assignedTo = userMap.get(e_new.ownerId);
    
                                fitem.Body = 'New Meeting' + ((e_new.Location__c) != null ? ', ' + e_new.Location__c : '') 
                                + '\nStart:' + e_new.StartDateTime.format() + ',  End:' + e_new.EndDateTime.format()
                                + '\nSubject: ' + e_new.Subject;
                            
                                feedItems.add(fitem);
                 }
                 update taskList;
            }
            
            
            /* SF-3936-- Ends */
            //String str = String.valueOf(e_new.id).substring(0, 15);
           // EventRelationHelper.UpdateEventFeedTask(str,e_new,e_old);
            For(Event e_new:trigger.new){ 
            //commented for 3837 - Aritra
            //if(e_new.Type.equalsignoreCase('meeting') && e_new.Minutes_of_Meeting__c != e_old.Minutes_of_Meeting__c)      
            Boolean completeTask = false;
            List<Task> taskList;
            //aritra if(e_new.Type != null && e_new.Type != '' && e_old.Purpose__c != null && e_old.Purpose__c != ''){
            if(e_new.WhatId != null && (e_new.WhatId.getsObjectType() == Opportunity.sObjectType)){
               if(e_new.Type != null && e_new.Type.equalsignoreCase('meeting') && e_new.Description != null && e_new.Description != e_old.Description){            
                    //******SF - 4072: If, event is linked with an oppty then it will link the chatter post to opportunity's Company*****Date - 19/01/2018
                            Id accId = [select id,AccountId from Opportunity where id=:e_new.WhatId].AccountId;
                            List<FeedItem> fItemList = new List<FeedItem>();
                            FeedItem fitemAcc = new FeedItem();
                            //Opportunity's account feed
                            fitemAcc.type = 'LinkPost';
                            fitemAcc.ParentId = accId;
                           //SOC 27/3/13 - 2013R1 Changed URL to include base URL so that links on email Chatter notifications work
                            fitemAcc.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e_new.id;
                            fitemAcc.Title = 'View Meeting'; 
                            fitemAcc.Body = 'Meeting Update' + ((e_new.Location__c) != null ? ', ' + e_new.Location__c : '') 
                                    + '\nStart:' + e_new.StartDateTime.format() + ',  End:' + e_new.EndDateTime.format()
                                    + '\nSubject: ' + e_new.Subject;
                                    
                            fItemList.add(fitemAcc);
                            //Opportunity feed
                            FeedItem fitemOpp = new FeedItem();
                            //Opportunity's account feed
                            fitemOpp.type = 'LinkPost';
                            fitemOpp.ParentId = e_new.WhatId;
                           //SOC 27/3/13 - 2013R1 Changed URL to include base URL so that links on email Chatter notifications work
                            fitemOpp.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e_new.id;
                            fitemOpp.Title = 'View Meeting'; 
                            fitemOpp.Body = 'Meeting Update' + ((e_new.Location__c) != null ? ', ' + e_new.Location__c : '') 
                                    + '\nStart:' + e_new.StartDateTime.format() + ',  End:' + e_new.EndDateTime.format()
                                    + '\nSubject: ' + e_new.Subject;
                                    
                            fItemList.add(fitemOpp);
                            insert fItemList;
                    completeTask = true;
                    
                }
            }
            
            else{ //SF-4749
                    if(e_new.WhatId != null && (e_new.Type != null && e_new.Type.equalsignoreCase('meeting') && e_new.Description != null && e_new.Description != e_old.Description && !e_new.Ultimate_Account_Parent__c)){
                        FeedItem fitem = new FeedItem();                       
                            fitem.type = 'LinkPost';
                            fitem.ParentId = e_new.WhatId;
                           //SOC 27/3/13 - 2013R1 Changed URL to include base URL so that links on email Chatter notifications work
                            fitem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e_new.id;
                            fitem.Title = 'View Meeting'; 
                            fitem.Body = 'Meeting Update' + ((e_new.Location__c) != null ? ', ' + e_new.Location__c : '') 
                                    + '\nStart:' + e_new.StartDateTime.format() + ',  End:' + e_new.EndDateTime.format()
                                    + '\nSubject: ' + e_new.Subject;
                                    
                    insert fitem;
                    system.debug('updated---'+fitem.Body);
                    completeTask = true;
                   }
                
                
                }
                //if(completeTask || (e_new.EndDateTime != e_old.EndDateTime))
                
                if(completeTask || (e_new.EndDateTime != e_old.EndDateTime) || (e_new.WhatId!= e_old.WhatId)){
                    system.debug('This is called --->'+e_old.WhatId);
                    system.debug('This is called e_new.id--->'+e_new.id);
                    taskList= [SELECT id, subject,WhatId,status, activityDate from Task where Meeting_ID__c = :e_new.id AND status != 'Completed' limit 1];
                   system.debug('The taskList--->'+taskList.size()+'-------->'+taskList);
                    if(taskList != null && taskList.size() > 0){
                         // added for SF-3853
                        //isComplete__c = true -> denotes the task should not be shown in the activity timeline
                        taskList[0].isComplete__c= true;
                        // end of SF-3853
                        if(completeTask){
                            taskList[0].status = 'Completed';
                        }
                        if(e_new.EndDateTime != e_old.EndDateTime){
                            taskList[0].ActivityDate = Date.newinstance(e_new.EndDateTime.year(), e_new.EndDateTime.month(), e_new.EndDateTime.day()); //SF 3863
                        }
                        
                          /*  if(e_old.WhatId!=e_new.WhatId){
                                taskList[0].WhatId = e_new.WhatId;
                                List<FeedItem> FeedItemToBeDeleted = [select id,parentId from FeedItem where ParentId =:e_old.WhatId];
                                if(FeedItemToBeDeleted.size()>0)
                                {
                                    Delete FeedItemToBeDeleted;
                                }
                                FeedItem fitem = new FeedItem();
                                fitem.type = 'LinkPost';
                                fitem.ParentId = e_new.WhatId;
                                fitem.LinkUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/' + e_new.id;
                                fitem.Title = 'View Meeting';
    
                            //Get the user by checking the userMap we created earlier
                                User assignedTo = userMap.get(e_new.ownerId);
    
                                fitem.Body = 'New Meeting' + ((e_new.Location__c) != null ? ', ' + e_new.Location__c : '') 
                                + '\nStart:' + e_new.StartDateTime.format() + ',  End:' + e_new.EndDateTime.format()
                                + '\nSubject: ' + e_new.Subject;
                            
                                feedItems.add(fitem);
                            }*/
                        }
                }
              /*  if(feedItems.size()>0 && feedItems!=null)
                {
                    Database.insert(feedItems,false);
                }*/
                //added for 3899
                //if event's subject is changed it must do cascade change 
                //to task's subject and task's meeting name text field
                System.debug('e_new.subject--' + e_new.subject+'e_new.id->'+e_new.id);
                System.debug('e_old.subject--' + e_old.subject+'e_old.id->'+e_old.id);
                if(e_new.Type != null && e_new.Type.equalsignoreCase('meeting')){// added for SF-4007
                    if(e_new.subject != e_old.subject){
                        System.debug('I am here1');
                        Task t;
                        if(taskList != null && taskList.size() > 0){
                            Tasklist[0].Subject = 'Complete Meeting minutes - ' + e_new.subject;
                            Tasklist[0].meeting_name__c = e_new.subject;
                            Tasklist[0].WhatId = e_new.whatId;
                        }
                        else{
                            taskList= [SELECT id, subject from Task where Meeting_ID__c = :e_new.id limit 1];
                            if(taskList != null && taskList.size() > 0){// add for SF-4007 null check
                            System.debug('I am in Task Query on update');
                                Tasklist[0].Subject = 'Complete Meeting minutes - ' + e_new.subject;
                                Tasklist[0].meeting_name__c = e_new.subject;
                                Tasklist[0].WhatId = e_new.whatId;
                               
                            }// add for SF-4007 null check
                         // add new task on update if Task is not there SF-4007
                         else {
                         System.debug('I am in Task create on update');
                          if(!e_new.isDuplicate__c)
                          {
                             Task t_new = new Task();
                            if(e_new.WhatId != null){
                                t_new.whatId = e_new.WhatId;
                                t_new.meeting_id__c = e_new.id;
                                t_new.meeting_name__c = e_new.subject;
                                t_new.OwnerId = e_new.OwnerId;
                                t_new.Subject = 'Complete Meeting minutes - ' + e_new.subject;
                                t_new.Priority = 'Normal';
                                t_new.ActivityDate = date.newinstance(e_new.EndDateTime.year(), e_new.EndDateTime.month(), e_new.EndDateTime.day()); //SF 3863
                                //t.ActivityDate = e_new.EndDateTime; //SF 3863
                                t_new.Status = 'Not Started';
                                // added for SF-3853
                                t_new.isComplete__c= true;
                                // end of SF-3853
                                //if(e_new.Minutes_of_Meeting__c != null && e_new.Minutes_of_Meeting__c != '')
                                if(e_new.Description != null && e_new.Description != '') 
                                {
                                    t_new.Status = 'Completed';                        
                                } //This is to ensure to take scenario, where the minutes are filled while creating meetings
                                insert t_new;
                             }
                            }
                         }
                         // add new task on update if Task is not there SF-4007   
                        
                        }
                    }
                    //added 3899
                
                    //Finally update the task/tasklist
                    if(taskList != null && taskList.size() > 0){
                        System.debug('I am here2');
                        update taskList;
                    }
               
                }// added for SF-4007
            //aritra }
             
        }
        if(feedItems.size()>0 && feedItems!=null)
                {
                    Database.insert(feedItems,false);
                }
    }
//--- Update Ends ---
   // Added for SF-3936  - Geetham
    //SF-3936 -- starts
    if(Trigger.isDelete)
    {
       e_old = Trigger.old[0];
       String str = String.valueOf(e_old.id).substring(0,15);
       EventRelationHelper.DeleteChildRecord(str);
             
    }  
        //SF-3936 -- Ends
        
        
        
          if (Trigger.isBefore) {
        if (Trigger.isDelete) {
    
            // In a before delete trigger, the trigger accesses the records that will be 
        
            // deleted with the Trigger.old list. 
            
                Set<Id> evtIds = new Set<Id>();
                List<Meeting_Minute__c> mmToDelete = new List<Meeting_Minute__c>();
                // added for SF - 3877
                List<Task> taskToDelete = new List<Task>();
                // end of SF-3877
                for(Event e : trigger.old){
                    evtIds.add(e.Id);
                }
                List <Meeting_Minute__c> theMinutes = [Select   m.Name, 
                                                                m.Event_Id__c, 
                                                                m.Id, 
                                                                m.Event_Subject__c, 
                                                                m.Event_StartDateTime__c, 
                                                                m.Event_Location__c, 
                                                                m.Event_EndDateTime__c,
                                                                m.Distributed__c,
                                                                m.Attendee_Count__c  
                                               From Meeting_Minute__c m 
                                               where event_id__c in : evtIds];
                Map<Id, Meeting_Minute__c> mapMinutes = new Map<Id, Meeting_Minute__c>();
                for(Meeting_Minute__c m : theMinutes){
                    mapMinutes.put(m.Event_Id__c, m);
                }
                                               
                for(Event evt: trigger.old){
                    //for each event determine if it has minutes and whether or not they are published
                    if(mapMinutes.containsKey(evt.Id)){
                        //meeting minutes exist for this event
                        //Check for the status of the minutes
                        Meeting_Minute__c thisMinute = mapMinutes.get(evt.Id);
                        if(thisMinute.Distributed__c){
                            //the meeting minutes have been published do not allow the
                            //event to be deleted
                            evt.addError('Published meeting minutes exist for this Event. You may not delete this record.');
                        }else{
                            //the meeting minutes have not been published
                            //check for attendees other than the assignee on the event 
                            //of attendees as a minimum
                            
                            List<EventRelation> evtAtt = new List<EventRelation>();
                            evtAtt = [SELECT RelationId FROM EventRelation WHERE EventId = :evt.Id AND RelationId != :evt.OwnerId];
                            
                            if(evtAtt.size()>0){
                                //there are attendees
                                String errorMessage = 'There are attendees associated to the meeting minutes of this event.\nPlease remove the attendees before deleting the event.';
                                evt.addError(errorMessage);
                            }else{
                                //If there are no attendees, the event and associated (non-published) meeting minute record should be deleted.
                                //SOC 14/5/13 - Add MM to delete list
                                mmToDelete.Add(mapMinutes.get(evt.Id));
                            }
                        }
                    }else{
                        //meeting minutes do not exist for this event
                        //allow the delete to contintue
                    }
                } 
                
                if(mmToDelete.size() > 0)
                {
                    delete mmToDelete;
                } 
                // added for SF- 3877
                List <Task> theTask = [SELECT Id,Meeting_ID__c,Status,Subject,Type,WhatId,WhoId FROM Task where Meeting_ID__c in : evtIds];
                if(!theTask.isEmpty())
                {
                     for(Task t : theTask){
                        //if(t.Subject.containsIgnoreCase('Complete Meeting minutes')){
                          taskToDelete.Add(t);
                       // }
                      }
                }
                if(taskToDelete.size() > 0)
                {
                    delete taskToDelete;
                }   
                // end of SF - 3877                                             
            }
          }
        
}