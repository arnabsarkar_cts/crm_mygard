trigger updateOpptyAreaManagers on Account (after update) {
    boolean bPerformUpdate = false;
    boolean bPerformUpdateContry = false;
    String strQuery = 'SELECT Id, Name, Account_Area_Manager__c, Area_Manager__c '
                     +'FROM Opportunity '
                     +'WHERE (StageName = \'Risk Evaluation\' OR StageName = \'Renewable Opportunity\')';
    String strAccIds = '';
    for (Account acc: Trigger.new) {
        strAccIds+='\''+acc.Id+'\',';
        // only perform the update if the Area Manager field has been updated
        if (acc.Area_Manager__c != Trigger.oldMap.get(acc.Id).Area_Manager__c) {
            bPerformUpdate = true;
        }
        //SF-4398        
        if (acc.Country__c != Trigger.oldMap.get(acc.Id).Country__c) {
            bPerformUpdateContry = true;
        }
        
    }
    
    strAccIds = strAccIds.substring(0, strAccIds.length()-1);
    if (bPerformUpdate) {
        strQuery = strQuery + ' AND Account.Id IN ('+strAccIds+')';
        System.debug('### Query: '+strQuery);
        //call the async data processor to act on all the accounts
        AsyncDataProcessor.UpdateOpptyAreaManagers(strQuery);
    }
    
    //SF-4398
    if (bPerformUpdateContry) {         
         strQuery = 'SELECT Country_v2__c, Account.Country__c FROM Opportunity where Account.Id IN ('+strAccIds+')';
         System.debug('###updateCntry3 : ' + strQuery );
         AsyncDataProcessor.UpdateOpptyCountry(strQuery);
    }   
}