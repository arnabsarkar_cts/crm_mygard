trigger ClaimTrigger on Claim__c(before update, after insert, before insert, after update){
    
    Boolean isThisFromReportAClaim = false;
    
    if(trigger.isUpdate){
        for(Claim__c c : Trigger.old){
            if(c.status__c == 'Draft')
                isThisFromReportAClaim = true;
        }
    }
    
    if(trigger.isbefore && trigger.isInsert){
        //GardUtils.updateGUID(Trigger.new);
        ClaimHelper.updateGUID(Trigger.new);
        
        //SF 1866
        //GardUtils.populateClaimPicklistFieldsForCase(Trigger.isInsert, Trigger.new);
        ClaimHelper.populateClaimPicklistFieldsForCase(Trigger.isInsert, Trigger.new);
    }    
    
    if(!isThisFromReportAClaim){
        
        //sf-3511- start
        if(trigger.isAfter && ((trigger.isInsert) || (trigger.isUpdate))){
            List<Claim__c> lstCasestoUpdate = new List<Claim__c>();
            Map<Id,Claim__c> lstCase = Trigger.newMap;
            Map<Id,Claim__c> caseAccPA = new Map<Id,Claim__c>([ SELECT Id,Claims_handler__c,Claims_handler__r.IsActive,OwnerId FROM Claim__c WHERE Id IN :lstCase.keySet()]);
            if(caseAccPA != null){
                for(Claim__c claim : caseAccPA.values()){
                    if((claim.Claims_handler__c != null) && (claim.Claims_handler__c != claim.OwnerId)){//(c.RecordTypeId == strId) &&
                        if(claim.Claims_handler__r.IsActive){
                            claim.OwnerId  = claim.Claims_handler__c;
                            lstCasestoUpdate.add(claim);                    
                        }
                    }
                }
            }
            if(lstCasestoUpdate.size()>0){
                if(!test.isRunningTest()){
                    //GardUtils.updateClaimOwners(lstCasestoUpdate);
                    ClaimHelper.updateClaimOwners(lstCasestoUpdate);                    
                }
            }
        }
        //sf-3511 - end
        
        // Used by time based workflow for deleting cases which are in delete status
        if(trigger.isAfter || trigger.isBefore){
            Set<Id> setCaseId = new Set<Id>();
            Map<Id,Claim__c> updatedCase =Trigger.newMap;
            if(updatedCase != null){
                for(Claim__c caseObj : updatedCase.values()){
                    if(caseObj.status__c ==  'Delete')
                        setCaseId.add(caseObj.id);
                }
                
                if(setCaseId.size() >0){
                    //GardUtils.deleteCase(setCaseId);
                    ClaimHelper.deleteCase(setCaseId);
                }
            }
        }
    }
    
    if(trigger.isBefore && trigger.isUpdate){
        if(!test.isrunningtest()){
            //GardUtils.populateClaimPicklistFieldsForCase(Trigger.isInsert, Trigger.new);
            ClaimHelper.populateClaimPicklistFieldsForCase(Trigger.isInsert, Trigger.new);
        }
        //GardUtils.assignToMe_ServiceRequest(Trigger.oldMap,Trigger.newMap);//added for SF-5015
        ClaimHelper.assignToMe_ServiceRequest(Trigger.oldMap,Trigger.newMap);//added for SF-5015
    }
}