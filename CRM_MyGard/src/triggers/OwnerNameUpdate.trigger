trigger OwnerNameUpdate on Account (after update) {
    List<Customer_Feedback__c> lstcusFeed = new List<Customer_Feedback__c>();
    List<Customer_Feedback__c> updatedlstcusFeed = new List<Customer_Feedback__c>();
    List<Id> accIds = new List<Id>();
    if(trigger.isUpdate)
    {
        for (Account acc:Trigger.new)
        {
            if(acc.OwnerId != (trigger.oldMap.get(acc.Id).OwnerId) )
            {
                accIds.add(acc.id);
            }
        }  
    }
    if(!accIds.isEmpty()){
    lstcusFeed =[Select id,Responsible__c,From_Company__r.OwnerId,Source__c from Customer_Feedback__c where From_Company__c in: accIds];
    for(Customer_Feedback__c cf : lstcusFeed )
    {
        if(cf.Source__c == 'MyGard')
        {
            cf.Responsible__c = cf.From_Company__r.OwnerId;
            updatedlstcusFeed.add(cf);
        }
    }
    if(updatedlstcusFeed.size()>0)
    {
        update updatedlstcusFeed;
    }
    }
}