trigger idea_chatter_post on Idea (after update) 
{
  Set<Id> ideaIds = trigger.newMap.keySet();
  Id IdeaCreatorId=[SELECT createdbyId FROM idea WHERE id = :ideaIds LIMIT 1].createdbyId;
  string categories = [SELECT categories FROM idea WHERE id = :ideaIds LIMIT 1].categories;
  string IdeaTitle = [SELECT title FROM idea WHERE id = :ideaIds LIMIT 1].title;
  string ChatterMessage = 'Your Idea "'+ IdeaTitle + '" has been updated. Please see: ';
  String IdeaID = String.valueOf(ideaIds).mid(1,18); //Must remove brackets around ID to use it in the URL
  String IdeaLink = URL.getSalesforceBaseUrl().toExternalForm() + '/' + IdeaId;
//  IF (categories.contains('Salesforce')) //Possible to limit this to certain Categories
//  {
      ConnectApi.ChatterFeeds.postFeedElement(null, IdeaCreatorId, ConnectApi.feedElementType.FeedItem, ChatterMessage + IdeaLink);
//  }
}