trigger UpdateCheckListName on Opportunity_Checklist__c (before insert,before update) {
    List<string> parentOppIdList = new List<string>();
    public string opChcklstRecTyp;
    List<Opportunity_Checklist__c> oppChcklist = new List<Opportunity_Checklist__c>();
    for(Opportunity_Checklist__c single : Trigger.new){
        String name ='';
        if(single.Opportunity_Name__c != null){
            name = 'Checklist - ' + single.Opportunity_Name__c;
            parentOppIdList.add(single.Opportunity__c);
        }
        system.debug('parent opportunity -- >'+parentOppIdList);
        oppChcklist = [SELECT ID,Opportunity__r.RecordType.Name FROM Opportunity_Checklist__c WHERE Opportunity__c IN :parentOppIdList];
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Opportunity_Checklist__c.getRecordTypeInfosById();
        opChcklstRecTyp = rtMap.get(single.RecordTypeId).getName();
        system.debug(' --- parent opp list --- '+oppChcklist);
        system.debug(' --- current record type id --- '+opChcklstRecTyp+' -- Map -- '+rtMap);
        if((oppChcklist.Size() > 0) && (trigger.isInsert)){
            system.debug(' ---error found oppchcklist > 0--- ');
            if(oppChcklist[0].Opportunity__r.RecordType.Name == 'Marine' && oppChcklist.size()>0){
                system.debug(' ---Marine error found--- '+single.Opportunity__r.RecordType.Name);
                single.addError('Only one checklist is allowed for ' + oppChcklist[0].Opportunity__r.RecordType.Name + ' Opportunity.');
            }
            else if((oppChcklist[0].Opportunity__r.RecordType.Name == 'P&I') && (opChcklstRecTyp == 'Marine' || opChcklstRecTyp == 'Marine newbiz/upsell' || opChcklstRecTyp == 'Marine renewal' || opChcklstRecTyp == 'Old P&I' || opChcklstRecTyp == 'P&I')){
                system.debug(' ---P&I error found---'+opChcklstRecTyp );
                single.addError('Please select proper record type for creating checklist under P&I opportunity(Charterers/MOU/Owners). ');
            }
        }
        if(name.length()>80){
            name = name.SubString(0,80); 
        }
        /*
        if(single.Agreement_type__c != null){
            Boolean isRequiredToCheck = true;
            if(Trigger.isUpdate){
                Map<id,Opportunity_Checklist__c> oldCheckLists= trigger.oldmap;
                if(single.Agreement_type__c == oldCheckLists.get(single.Id).Agreement_type__c){
                    isRequiredToCheck =false;
                }
            }
            if(isRequiredToCheck){
                List<Opportunity_Checklist__c> allOppCheckList = [SELECT ID, Agreement_type__c FROM Opportunity_Checklist__c WHERE Opportunity__c =: single.Opportunity__c ];
                if(allOppCheckList.size()>0){
                    for(Opportunity_Checklist__c a : allOppCheckList){
                        if(a.Agreement_type__c == single.Agreement_type__c && a.Agreement_type__c != null){
                            single.addError('Only one checklist with record type ' + single.Agreement_type__c + ' is allowed');
                        }
                    }
                }
           */     
                /*
                //added for SF-3691
                String oppRecordType = [SELECT RecordType.Name FROM Opportunity WHERE Id =: single.Opportunity__c].RecordType.Name;
                if(oppRecordType != 'Marine'){
                    single.Name = name;
                }
                //end of SF-3691
                if(single.Checklist_Completed__c){
                    if(single.Agreement_type__c == 'Owners'){
                        single.Owners_P_I_Completed__c = true;
                    }
                    if(single.Agreement_type__c == 'MOU'){
                        single.MOU_Completed__c = true;
                    }
                    if(single.Agreement_type__c == 'Charterers'){
                        single.Charterers_Completed__c = true;
                    }
                }
                */
    }
}