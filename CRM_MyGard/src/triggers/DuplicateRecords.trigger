trigger DuplicateRecords on Account_Contact_Mapping__c (before insert, after insert,before update,after update, after delete) 
{
    String record_Id = '';
    String message = '';
    Set<string> account_id = new Set<string>();
    Set<string> contact_id = new Set<string>();
    Set<string> duplicateRecord_Acc = new Set<string>();
    Set<string> duplicateRecord_Con = new Set<string>();
    List<Account_Contact_Mapping__c> duplicateRecordList; 
    List<Account_Contact_Mapping__c> duplicateRecordlst;
    AdminUsers__c numberofusers = AdminUsers__c.getValues('Number of users');// 4376
    integer count = (integer)numberofusers.Value__c; // changes made for SF-4376
        
    // before insert or update check whether record exist with provided account and contact, if found restrict
    if((trigger.IsUpdate || trigger.IsInsert) && trigger.IsBefore)
    {
        for(Account_Contact_Mapping__c acm : Trigger.New)
        {
            account_id.add(acm.Account__c);
            contact_id.add(acm.Contact__c);
        }
        duplicateRecordList = [Select id, Account__c, Contact__c From Account_Contact_Mapping__c where Account__c IN :account_id AND Contact__c IN: contact_id];    
        if(duplicateRecordList.size() > 0)
        {
            message = 'Record already exist';
        }     
        for(Account_Contact_Mapping__c acm: duplicateRecordList)
        {
            duplicateRecord_Acc.add(acm.Account__c);
            duplicateRecord_Con.add(acm.Contact__c);               
        }    
        for(Account_Contact_Mapping__c acm : Trigger.new)
        {            
            if(trigger.IsInsert)
                if(acm.Account__c != null)
                {
                    System.debug('****acm -->'+acm);
                    if(duplicateRecord_Acc.contains(acm.Account__c) && duplicateRecord_Con.contains(acm.Contact__c))
                    {
                        acm.Account__c.addError(message);
                    }            
                }
            if(trigger.IsUpdate && Trigger.new[0].Account__c != null && Trigger.old[0].Account__c != null && (Trigger.new[0].Account__c != Trigger.old[0].Account__c)
                && Trigger.new[0].Contact__c != null && Trigger.old[0].Contact__c != null && (Trigger.new[0].Contact__c != Trigger.old[0].Contact__c))
            
            {
                if(duplicateRecord_Acc.contains(acm.Account__c) && duplicateRecord_Con.contains(acm.Contact__c))
                {
                    acm.Account__c.addError(message);
                }
            }
        } 
    }    
  // block multiple admin record with same accountId
    if(trigger.isUpdate && trigger.isBefore && Trigger.New[0].Active__c)
    {        
        Set<String> oldContactIds = new Set<String>();        
        account_id.clear();                
        for(Account_Contact_Mapping__c acm : Trigger.New)
        {            
            account_id.add(acm.Account__c); 
            oldContactIds.add(acm.Contact__c); 
        }       
        duplicateRecordList = [Select id, Account__c, Contact__c, Administrator__c, Active__c From Account_Contact_Mapping__c where 
                                Account__c IN :account_id AND Contact__c NOT IN: oldContactIds];    
        duplicateRecordlst = [Select id, Account__c, Administrator__c From Account_Contact_Mapping__c where 
                                Account__c IN :account_id AND Administrator__c='Admin' AND Active__c=true]; // changes made for SF-4376      
        for(Account_Contact_Mapping__c acm: duplicateRecordList)
        {
            if(acm.Administrator__c != null && acm.Active__c && acm.Administrator__c.contains('Admin') && Trigger.New[0].Administrator__c != null && Trigger.New[0].Administrator__c.contains('Admin') && Trigger.New[0].Account__c == acm.Account__c)
            {                
                for(Account_Contact_Mapping__c acmObj : Trigger.new)
                {    
                    if(duplicateRecordlst.size()>=count && ((Trigger.Old[0].Administrator__c==Trigger.New[0].Administrator__c && !Trigger.Old[0].Active__c && Trigger.New[0].Active__c)||Trigger.Old[0].Administrator__c!=Trigger.New[0].Administrator__c))// changes made for SF-4376 
                    {       
                    acmObj.Administrator__c.addError('Admin record already exist with this account');
                    break;
                    }
                }               
                break;
            }                       
        }         
    }
    if(trigger.isInsert && trigger.isBefore)
    {        
        Set<String> oldContactIds = new Set<String>();        
        account_id.clear();             
        for(Account_Contact_Mapping__c acm : Trigger.New)
        {            
            account_id.add(acm.Account__c); 
            oldContactIds.add(acm.Contact__c); 
        }        
        duplicateRecordList = [Select id, Account__c, Contact__c, Administrator__c, Active__c From Account_Contact_Mapping__c where Account__c IN :account_id];                                       
        duplicateRecordlst = [Select id, Account__c, Administrator__c From Account_Contact_Mapping__c where 
                                Account__c IN :account_id AND Administrator__c='Admin' AND Active__c=true]; // changes made for SF-4376
        for(Account_Contact_Mapping__c acm: duplicateRecordList)
        {
            if(acm.Administrator__c != null && acm.Administrator__c.contains('Admin') && acm.Active__c == true  && Trigger.New[0].Administrator__c != null  && Trigger.New[0].Administrator__c.contains('Admin'))
            {                   
                for(Account_Contact_Mapping__c acmObj : Trigger.new)
                {       
                    if(duplicateRecordlst.size()>=count) // changes made for SF-4376 
                    {    
                    acmObj.Administrator__c.addError('Admin record already exist with this account');
                    break;
                    }
                }               
                break;
            }                       
        }         
    }    
    // grant new admin user to peopleClaim access by default
    if((trigger.isInsert || trigger.isUpdate) && trigger.isAfter && (GardUtils.blockMultiCall == false))
    { 
        System.debug('grant new admin user to peopleClaim access by default');
        if(trigger.isUpdate && trigger.New[0].administrator__c == 'Admin' && trigger.Old[0].administrator__c != trigger.New[0].administrator__c)
            GardUtils.updatePeopleClaim(trigger.New);
        if(trigger.isInsert && trigger.New[0].administrator__c == 'Admin' && trigger.New[0].IsPeopleClaimUser__c != true)
            GardUtils.updatePeopleClaim(trigger.New);
    }    
    // set blank value of Administrator__c to 'Normal'         
    if((trigger.isInsert || trigger.isUpdate) && trigger.isAfter)
    {        
        GardUtils.setNormalToACJuncObject(Trigger.NewMap.keyset());
    }     
    // for deactivating user if no record is active specific to that contact in all records of in Acc-Cont-Junc object    
    if(trigger.isUpdate && trigger.isAfter && Trigger.New[0].Active__c == false && Trigger.Old[0].Active__c && !GardUtils.stopAccContTriggerCall)
    {                
        Set<Id> ContactId = new Set<Id>();
        ContactId.add(Trigger.New[0].contact__c);
        GardUtils.deactivate(ContactId);        
    }    
    // update MyGard_enabled__c field of account if this object has atleast a valid active record for that company.
    if((trigger.isInsert || trigger.isUpdate) && trigger.isAfter)
    {   
        system.debug('--Junction object trigger got fired--');
        //GardUtils.updateAccount(Trigger.NewMap);//commented for SF-5088
        GardUtils.updateAccountOnACMUpdate(Trigger.NewMap);//added for SF-5088
    }
    if(trigger.isDelete && !GardUtils.deactivateUserOnDel){
        //for user deactivation if there is no active record for that contact        
        Set<Id> ContactId = new Set<Id>();
        ContactId.add(Trigger.Old[0].contact__c);
        GardUtils.deactivate(ContactId);
        GardUtils.deactivateUserOnDel = true;
        //GardUtils.updateAccount(Trigger.oldMap);//commented for SF-5088
        GardUtils.updateAccountOnACMUpdate(Trigger.NewMap);//added for SF-5088
    }
    //update PI_access__c to true when IsPeopleClaimUser__c equals true CR - MYG-1829
    // also update IsPeopleClaimUser__c to false on PI_Access__c to false - MYG-2999
    if(trigger.isUpdate && trigger.isAfter && GardUtils.blockMultiCall == false)
    {
        GardUtils.updatePIAccess_PeopleClaimAccess(Trigger.NewMap, Trigger.oldMap);
    }
    // SF-263 & 271
    if(trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert) )
    {
        //sf-263
        User[] usr = [SELECT IsActive FROM User WHERE ContactId =: Trigger.New[0].contact__c];
        if(usr != null && usr.size() >0 && !usr[0].isActive && Trigger.New[0].Active__c) // added Active check for SF-3733
        {
            Trigger.New[0].Contact__c.addError('Partner user is not active');
        }
        else if(usr != null && usr.size() == 0 && !Test.isRunningTest())
        {
            Trigger.New[0].Contact__c.addError('Partner user is not created');
        }
        //sf-271
        Account[] acc = [SELECT isPartner FROM Account WHERE Id =: Trigger.New[0].account__c];
        if(acc != null && acc.size() >0 && !acc[0].isPartner && !Test.isRunningTest())
        {
            Trigger.New[0].Account__c.addError('Partner account is not enabled');
        }
    }
}