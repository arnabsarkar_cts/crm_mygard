trigger caseTrigger on Case (before update, after insert, before insert, after update) 
{    
    Case objectCase = new Case();
    Id profileId=userinfo.getProfileId();
    String profileName = [Select Id,Name from Profile where Id=:profileId].Name;
    system.debug('ProfileName'+profileName);
    
    // for contract review
    // type changed as per mail
    if(Trigger.isUpdate && Trigger.isAfter && Trigger.New[0].Type != null && Trigger.New[0].Type.equalsIgnoreCase('Crew Contract Review'))
    {        
        System.debug('update contract review object  &&&&&&&&&&&&&&&&&&&');
        // update contract review object
        if( Trigger.New[0].id != null)
            GardUtils.updateContractReview(Trigger.New[0]);            
        // Webservice call for owner change 
        if( Trigger.Old[0].ownerId != null && Trigger.New[0].ownerId != null && (Trigger.New[0].ownerId != Trigger.Old[0].ownerId) && Trigger.New[0].DM_Contract_Review_Case_ID__c != null)
        {
            // restrict for first update from ContractReviewLOCCtrl controller
            if(ContractReviewLOCCtrl.chkboolean){
                System.debug('Calling changeOwner_WS from trigger for owner change');
                ContractReviewLOCCtrl.changeOwner_WS(String.valueOf(Trigger.New[0].ownerId),Trigger.New[0].DM_Contract_Review_Case_ID__c);    
                
                // task creation on owner change
                //NOTE to reviewer : Explicit query is required since owner.name returns null in SF ;)
                System.debug('prev user name '+Trigger.Old[0].owner.Name);
                String prevUserName = [SELECT name from User where id = :Trigger.Old[0].ownerId limit 1].name; 
                
                ContractReviewLOCCtrl.createTaskOnOwnerChange(Trigger.New[0].Name_Of_Contract__c, prevUserName, Trigger.New[0].ownerId, Trigger.New[0].id);
            }
        }
        // Webservice call for Reviewer change
        if( Trigger.Old[0].Reviewer__c != null && Trigger.New[0].Reviewer__c != null && Trigger.New[0].Reviewer__c != Trigger.Old[0].Reviewer__c && Trigger.New[0].DM_Contract_Review_Case_ID__c != null)
        {
            // restrict for first update from ContractReviewLOCCtrl controller
            if(ContractReviewLOCCtrl.chkboolean){
                System.debug('Calling changeOwner_WS from trigger for reviewer change');
                // insert manual apex sharing record on reviewer change           
                ContractReviewLOCCtrl.changeOwner_WS(String.valueOf(Trigger.New[0].Reviewer__c),Trigger.New[0].DM_Contract_Review_Case_ID__c);
            }
        }        
    }    
    
    
    
    Boolean isThisFromReportAClaim = false;
    if(trigger.isUpdate){
        for(Case c : Trigger.old){
            if(c.status == 'Draft')
                isThisFromReportAClaim = true;
        }
    }
    if(trigger.isbefore && trigger.isInsert)
    {
        GardUtils.updateGUID(Trigger.new);
        
        //SF 1866
        GardUtils.populateClaimPicklistFieldsForCase(Trigger.isInsert, Trigger.new);
                
    }    
    
    if(!isThisFromReportAClaim){
        if(trigger.isBefore)
        {
            //Update_Status_Change_Date whenever the status is changed
            /*List<Case> oldValues=Trigger.Old;
            List<Case> newValues=Trigger.New;
            Map<Id,Case> mapOldCase=new Map<Id,Case>();
            mapOldCase.putAll(oldValues);
            for(Case aCase:newValues)
            {
                if(mapOldCase.containsKey(aCase.Id))
                {
                    Case oldCase =mapOldCase.get(aCase.Id);
                    if(oldCase.Status_Change__c!=aCase.Status_Change__c)
                    {
                      aCase.Status_Change_Date__c=DateTime.Now();   
                    }  
                }
            }*/
        }
        
        if(trigger.isAfter && trigger.isInsert)
        {    // change case owner for after insert on case
            system.debug('**************trigger.newMap  '+trigger.newMap);    
            GardUtils.changeCaseOwner(trigger.newMap);
            if(!test.isrunningtest()){
                GardUtils.insertContractReview(Trigger.new[0].id); 
            }
        }
        
        //sf-3511- start
        if(trigger.isAfter && ((trigger.isInsert) || (trigger.isUpdate)))
        {
            system.debug('test sf-3511');
            List<Case> lstCasestoUpdate = new List<Case>();
            List<RecordType> lstRecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Claim'];
            String strId = lstRecordType[0].Id;
            Map<Id,Case> lstCase =Trigger.newMap;
            map<Id,Case> caseAccPA = new map<Id,Case>([ Select Id, RecordTypeId, Claims_handler__c, Claims_handler__r.IsActive, OwnerId from Case where id In: lstCase.keySet()]);
            if(caseAccPA != null){
                for(Case c:caseAccPA.values())
                {
                    //system.debug('c.RecordTypeId:'+c.RecordTypeId);
                    //system.debug('strId:'+strId);
                    //system.debug('c.Claims_handler__c:'+c.Claims_handler__c);
                    //system.debug('c.OwnerId:'+c.OwnerId);
                    //system.debug('CH Active/Inactive:'+c.Claims_handler__r.IsActive);
                    if((c.RecordTypeId == strId) && (c.Claims_handler__c != null) && (c.Claims_handler__c != c.OwnerId))
                    {
                        if(c.Claims_handler__r.IsActive){
                            c.OwnerId  = c.Claims_handler__c;
                            lstCasestoUpdate.add(c);                    
                        }
                    }
                }
            }
            system.debug('lstCasestoUpdate size:'+lstCasestoUpdate.size());
            if(lstCasestoUpdate.size()>0)
            {
                if(!test.isRunningTest())
                    GardUtils.updateClaimOwners(lstCasestoUpdate);
            }
        }
        //sf-3511 - end
        // Used by time based workflow for deleting cases which are in delete status
        if(trigger.isAfter || trigger.isBefore)
        {
            Set<Id> setCaseId = new Set<Id>();
            Map<Id,Case> updatedCase =Trigger.newMap;
            if(updatedCase != null)
            {
                for(Case caseObj :updatedCase.values())
                {
                    if(caseObj.status ==  'Delete')
                        setCaseId.add(caseObj.id);
                }
                
                if(setCaseId.size() >0)
                    GardUtils.deleteCase(setCaseId); 
            }
        }
    }
    if(trigger.isAfter && trigger.isUpdate)
    {
        GardUtils.updateCaseStatusForCrewContractLOC(trigger.oldMap, trigger.newMap);
        
    }
    if(trigger.isBefore && trigger.isUpdate)
    {
        GardUtils.restrictUpdate(trigger.New[0]);
        //SF 3660
        System.debug('From trigger-->' + Trigger.new);
        if(!test.isrunningtest()){
            GardUtils.populateClaimPicklistFieldsForCase(Trigger.isInsert, Trigger.new);
        }
        
        GardUtils.newCaseUserAssignment(Trigger.oldMap,Trigger.newMap);//added for SF-5015, modified for SF-5114
    }
}