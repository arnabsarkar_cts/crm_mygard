trigger MDMSyncAccount on Account bulk (before insert,after insert,before update,after update,before delete,after delete) {
    System.Debug('*** Account Trigger is getting fired ***');
    MDM_Settings__c mdmSettings;
   if(!test.isRunningTest()){
        mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
   }
    else{
        mdmSettings = new MDM_Settings__c(AccountOnRisk_Trigger_Enabled__c = true, Account_Trigger_Enabled__c = true,
                                          Contact_Trigger_Enabled__c = true, Edit_Sync_Failure_Records__c = false,
                                          Triggers_Enabled__c = true , Web_Services_Enabled__c = true
                                          );  
       //insert MDMsettings ;
    }
    if(mdmSettings.Triggers_Enabled__c && mdmSettings.Account_Trigger_Enabled__c){
        System.Debug('*** Trigger is enabled');

        if (trigger.isBefore) {
            // MM: split out the multi-line street address into seperate fields
            if (trigger.isInsert || trigger.isUpdate) {
                System.Debug('*** Before update');
                AccountTriggerHelper.splitBillingAddress(trigger.new, trigger.oldMap);
                AccountTriggerHelper.splitShippingAddress(trigger.new, trigger.oldMap);
            }
            if (trigger.isInsert) {
                if(!test.isRunningTest())
                    MDMSyncAccountHandler.BeforeInsert(trigger.new, trigger.newMap);
            } else if (trigger.isUpdate) {
                System.Debug('*** Before update -> MDMSyncAccountHandler.BeforeUpdate');
                MDMSyncAccountHandler.BeforeUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
            } else if (trigger.isDelete) {
                MDMSyncAccountHandler.BeforeDelete(trigger.old, trigger.oldMap);
            }
        } else if (trigger.isAfter) {
            if (trigger.isInsert) {
                if(!test.isRunningTest())
                	MDMSyncAccountHandlerNew.AfterInsert(trigger.new, trigger.newMap);
            } else if (trigger.isUpdate) {
                System.Debug('*** After update -> MDMSyncAccountHandler.AfterUpdate');
                MDMSyncAccountHandlerNew.AfterUpdate(trigger.new, trigger.newMap, trigger.old, trigger.oldMap);
            } else if (trigger.isDelete) {
                MDMSyncAccountHandler.AfterDelete(trigger.old, trigger.oldMap);
            }
        }
    } else {
        if (trigger.isBefore) {
            if (trigger.isInsert || trigger.isUpdate) {
                //Set the role \ sub role text fields for use in formula fields
                MDMSyncAccountHandler.SetRoleText(trigger.new);
            }
        }
    }
}