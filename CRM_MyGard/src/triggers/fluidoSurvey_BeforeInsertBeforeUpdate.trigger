/*
*
* cDecisions Ltd 20/9/2013
*
* Can't do lookup to email templates, so Fluido just store Id
* We want to display a link, so this looks up and populates template
* name as Id is set
*
*/

trigger fluidoSurvey_BeforeInsertBeforeUpdate on fluidoconnect__Survey__c (before insert, before update) {

	boolean massEmailTemplateHasChanged = false;

	Map<Id, EmailTemplate> emailTemplates = new Map<Id, EmailTemplate>([SELECT Id, Name FROM EmailTemplate WHERE isActive=true]);

	for(fluidoconnect__Survey__c s : trigger.new)
	{
		massEmailTemplateHasChanged = false;
		if(trigger.isInsert)
		{
			if(s.fluidoconnect__TemplateId__c != null)
			{
				massEmailTemplateHasChanged = true;
			}
		}
		if(trigger.isUpdate)
		{
			if(s.fluidoconnect__TemplateId__c != trigger.oldMap.get(s.Id).fluidoconnect__TemplateId__c)
			{
				massEmailTemplateHasChanged = true;
			}
		}
		
		if(massEmailTemplateHasChanged && s.fluidoconnect__TemplateId__c != null)
		{
			s.Mass_Email_Template_Name__c = emailTemplates.get(s.fluidoconnect__TemplateId__c).Name;
		}
	}

}