<?xml version="1.0" encoding="UTF-8"?>
<ApprovalProcess xmlns="http://soap.sforce.com/2006/04/metadata">
    <active>false</active>
    <allowRecall>true</allowRecall>
    <allowedSubmitters>
        <submitter>GardAdministrators</submitter>
        <type>group</type>
    </allowedSubmitters>
    <allowedSubmitters>
        <type>owner</type>
    </allowedSubmitters>
    <approvalPageFields>
        <field>Account</field>
        <field>Area_Manager__c</field>
        <field>Business_Type__c</field>
        <field>Approval_Criteria__c</field>
        <field>P_I_Member_Approved__c</field>
        <field>Name</field>
        <field>Owner</field>
        <field>Sales_Channel__c</field>
        <field>Type</field>
        <field>CloseDate</field>
        <field>CurrencyIsoCode</field>
        <field>Budget_Amount__c</field>
        <field>Amount</field>
        <field>Variance_to_Budget__c</field>
        <field>Budget_Variance__c</field>
        <field>Terms_Conditions__c</field>
        <field>Description</field>
    </approvalPageFields>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Tech_UWR_Manager__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <booleanFilter>1 AND (2 OR 3)</booleanFilter>
            <criteriaItems>
                <field>Opportunity.Tech_UWR_Manager__c</field>
                <operation>notEqual</operation>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Business_Type__c</field>
                <operation>equals</operation>
                <value>MOUs</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Business_Type__c</field>
                <operation>equals</operation>
                <value>Ship Owners</value>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Technical UW</label>
        <name>Technical_UW</name>
    </approvalStep>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Area_Manager__c</name>
                <type>relatedUserField</type>
            </approver>
            <approver>
                <name>PM_Approver__c</name>
                <type>relatedUserField</type>
            </approver>
            <approver>
                <name>Senior_Approver__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>Unanimous</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <booleanFilter>(1 OR 2) AND (3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9) AND 10</booleanFilter>
            <criteriaItems>
                <field>Opportunity.Business_Type__c</field>
                <operation>equals</operation>
                <value>Ship Owners</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Business_Type__c</field>
                <operation>equals</operation>
                <value>Small Craft</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>RA &lt; 75%</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Deviation from Budget</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Multiple Year Deal</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Approval required in &lt; 24 hours</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Silent Approval</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Vessel &gt; 1500 GT</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Renewal Premium reduced by &gt; 20%</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Non Standard T&amp;Cs</value>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>AM and SVP and PM Approval</label>
        <name>AM_and_SVP_and_PM</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <assignedApprover>
            <approver>
                <name>PM_Approver__c</name>
                <type>relatedUserField</type>
            </approver>
            <approver>
                <name>Area_Manager__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>Unanimous</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <booleanFilter>(1 OR 2) AND (3 OR 4) AND 5</booleanFilter>
            <criteriaItems>
                <field>Opportunity.Business_Type__c</field>
                <operation>equals</operation>
                <value>Ship Owners</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Business_Type__c</field>
                <operation>equals</operation>
                <value>Small Craft</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>RA &lt; 90%</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Vessel &lt; 1500 GT &amp; Premium &lt; 85% rating</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Non Standard T&amp;Cs</value>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>AM and PM Approval</label>
        <name>AM_and_PM_Approval</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>false</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Area_Manager__c</name>
                <type>relatedUserField</type>
            </approver>
            <approver>
                <name>Senior_Approver__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>Unanimous</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7</booleanFilter>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>RA &lt; 75%</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Deviation from Budget</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Multiple Year Deal</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Approval required in &lt; 24 hours</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Silent Approval</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Vessel &gt; 1500 GT</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Renewal Premium reduced by &gt; 20%</value>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>AM and SVP Approval</label>
        <name>AM_and_SVP_Approval</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Owner</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <booleanFilter>1</booleanFilter>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>equals</operation>
                <value>Self Approval</value>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Opportunity Owner Approval</label>
        <name>Opportunity_Owner_Approval</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>Area_Manager__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <booleanFilter>(1 AND 2) OR (3 AND 4) OR (5 AND (6 OR 7 OR (11 AND 12))) OR (8 AND (9 OR 10 OR (12 AND 13)))</booleanFilter>
            <criteriaItems>
                <field>Opportunity.Business_Type__c</field>
                <operation>equals</operation>
                <value>Ship Owners</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>RA &lt; 90%</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Business_Type__c</field>
                <operation>equals</operation>
                <value>Small Craft</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>RA &lt; 90%</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Business_Type__c</field>
                <operation>equals</operation>
                <value>Charters and Traders</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>equals</operation>
                <value>Annual Renewal Premium &gt; $250000</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>equals</operation>
                <value>Non Standard T&amp;Cs</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Business_Type__c</field>
                <operation>equals</operation>
                <value>MOUs</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>equals</operation>
                <value>Deviation from Tariff</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>equals</operation>
                <value>Non Standard T&amp;Cs</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Annual Renewal Premium &gt; $250000</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Non Standard T&amp;Cs</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>includes</operation>
                <value>Deviation from Tariff</value>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>GotoNextStep</ifCriteriaNotMet>
        <label>Area Manager Approval</label>
        <name>Area_Manager_Approval</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <name>PM_Approver__c</name>
                <type>relatedUserField</type>
            </approver>
            <whenMultipleApprovers>FirstResponse</whenMultipleApprovers>
        </assignedApprover>
        <entryCriteria>
            <booleanFilter>(1 OR 2) AND 3</booleanFilter>
            <criteriaItems>
                <field>Opportunity.Business_Type__c</field>
                <operation>equals</operation>
                <value>Ship Owners</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Business_Type__c</field>
                <operation>equals</operation>
                <value>Small Craft</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.Approval_Criteria__c</field>
                <operation>equals</operation>
                <value>Non Standard T&amp;Cs</value>
            </criteriaItems>
        </entryCriteria>
        <label>Product Manager Approval</label>
        <name>Product_Manager_Approval</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <description>Implementing Approvals Mapping 2912v3d
20130120 No email when risk Small craft instead of MA Small craft (new org)</description>
    <emailTemplate>Approval_Templates/Oppty_Approval_Required</emailTemplate>
    <enableMobileDeviceAccess>true</enableMobileDeviceAccess>
    <entryCriteria>
        <booleanFilter>1 AND 2 AND 3 AND ((4 AND 5) OR 6)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.RecordType</field>
            <operation>equals</operation>
            <value>P&amp;I,Marine</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Risk_Evaluation_Status__c</field>
            <operation>equals</operation>
            <value>Ready for Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName_RO__c</field>
            <operation>equals</operation>
            <value>Risk Evaluation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>notEqual</operation>
            <value>Small Craft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Criteria__c</field>
            <operation>excludes</operation>
            <value>Self Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Budget_Status__c</field>
            <operation>notEqual</operation>
            <value>Budget Approved</value>
        </criteriaItems>
    </entryCriteria>
    <finalApprovalActions>
        <action>
            <name>P_I_Opportunity_Approval_Alert</name>
            <type>Alert</type>
        </action>
        <action>
            <name>Update_Stage_Quote</name>
            <type>FieldUpdate</type>
        </action>
    </finalApprovalActions>
    <finalApprovalRecordLock>false</finalApprovalRecordLock>
    <finalRejectionActions>
        <action>
            <name>P_I_Opportunity_Rejection_Alert</name>
            <type>Alert</type>
        </action>
        <action>
            <name>Update_Stage_Risk_Evaluation</name>
            <type>FieldUpdate</type>
        </action>
    </finalRejectionActions>
    <finalRejectionRecordLock>false</finalRejectionRecordLock>
    <initialSubmissionActions>
        <action>
            <name>Notify_UWR_Management</name>
            <type>Alert</type>
        </action>
        <action>
            <name>Update_Stage_Under_Approval</name>
            <type>FieldUpdate</type>
        </action>
        <action>
            <name>Update_Submitted_Date</name>
            <type>FieldUpdate</type>
        </action>
    </initialSubmissionActions>
    <label>P&amp;I and Marine Opp Approval 2012v3d</label>
    <recallActions>
        <action>
            <name>Opportunity_Recall_Alert</name>
            <type>Alert</type>
        </action>
        <action>
            <name>Update_Stage_Risk_Evaluation</name>
            <type>FieldUpdate</type>
        </action>
    </recallActions>
    <recordEditability>AdminOrCurrentApprover</recordEditability>
    <showApprovalHistory>true</showApprovalHistory>
</ApprovalProcess>
