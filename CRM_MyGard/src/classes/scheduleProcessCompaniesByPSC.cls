global class scheduleProcessCompaniesByPSC implements Schedulable {
    global void execute (SchedulableContext SC) {
        processCompaniesByPSC pc = new processCompaniesByPSC();
        
        pc.Query = 'SELECT Name, Id, P_I_Member_Flag__c, M_E_Client_Flag__c, PSC_Exists__c, RecordType.Name, Area_Manager__c, Market_Area__c, Market_Area_Code__c,'
                  +'On_Risk__c, Client_On_Risk_Flag__c, Broker_On_Risk_Flag__c, Company_Role__c, role_change_approved__c, (SELECT On_Risk__c, Source_System__c, Type__c, Company_Sub_Type__c from Admin_System_Companies__r) '
                  +'from Account';
        Database.executeBatch(pc, 20);
    }
    
    static testMethod void testSchedule() {
        Test.StartTest();
            scheduleProcessCompaniesByPSC testSched = new scheduleProcessCompaniesByPSC();
            String strCRON = '0 0 4 * * ?';
            System.schedule('scheduleProcessCompaniesByPSC Test', strCRON, testSched);
        Test.StopTest();
    }
}