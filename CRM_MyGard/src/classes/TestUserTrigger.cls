@isTest private class TestUserTrigger {
    private static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    private static UserRole salesforceRoleId = [SELECT Id FROM UserRole WHERE Name='Bergvall Marine AS Partner User' limit 1];
    public static string partnerLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community' and name='Partner Community Login User Custom' limit 1].id;
    private static Contact brokerContact,clientContact;
    private static Account brokerAcc,clientAcc;
    private static User salesforceLicUser,brokerUser,clientUser;
    private static List<User> userList = new List<User>();
    private static List<Account> accountList = new List<Account>(); //'Partner Community Login User Custom' 
    private static List<Contact> contactList = new List<Contact>();
    private static List<AccountToContactMap__c> acmCSs;
    private static List<Valid_Role_Combination__c> vrcList;
    private static Gard_Team__c gt;
    private static Market_Area__c Markt;
    private static Country__c country;
    private static Gard_Contacts__c grdobj;
    
    
    
    private static void createTestData(){
        if(acmCSs == null) acmCSs = new List<AccountToContactMap__c>();
        EmailWrapper emailWrap = new EmailWrapper();
        emailWrap.coverName = 'Defence Cover';
        emailWrap.clientName = 'DHT Management AS';
        
        User usr = [Select id from User where Id = :UserInfo.getUserId() AND UserRoleId != null];
        
        System.runAs(usr) {
            
            //Valid Role Combination----
            
            vrcList = new List<Valid_Role_Combination__c>();
            Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
            vrcList.add(vrcClient);
            Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
            vrcList.add(vrcBroker);
            Valid_Role_Combination__c vrcClinic = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
            vrcList.add(vrcClinic);
            insert vrcList;
            
            //CustomSetting added by Abhirup
            AdminUsers__c setting = new AdminUsers__c();
            setting.Name = 'Number of users';
            setting.Value__c = 3;
            insert setting;
            
            Markt = new Market_Area__c(Market_Area_Code__c = 'ab120',name='Charterers & Traders');
            insert Markt;  
            if(gt == null) gt = new Gard_Team__c(Active__c = true,Office__c = 'test',Name = 'GTeam',Region_code__c = 'TEST');
            insert gt;
            if(country == null) country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Gard_Team__c=gt.Id,Claims_Support_Team__c=gt.Id);
            insert country;
            grdobj  =    new  Gard_Contacts__c(
                FirstName__c = 'Testreqchng',
                LastName__c = 'Baann',
                //Company_Type__c = 'Grd', 
                Email__c = 'WR_gards.12@Test.com',
                //MailingCity__c = 'Arendal', 
                //MailingCountry__c = 'Norway', 
                // MailingPostalCode__c =  '487155',
                // MailingState__c = 'Oslo', 
                // MailingStreet__c = 'Rd Road', 
                //Mobile__c = '98323322', 
                MobilePhone__c = '548645', 
                Nick_Name__c  = 'NilsPeter', 
                Office_city__c = 'Arendal', 
                Phone__c = '5454454',
                Portal_Image__c ='<img alt="User-added image" src="https://c.cs8.content.force.com/servlet/rtaImage?eid=a1hL0000000kSyZ&amp;feoid=00NL0000003OySC&amp;refid=0EML00000008Zjs"></img>', 
                Title__c = 'Dr' 
            );
            insert grdobj ;
            System.debug('SOQL debug commonRecord() preUserRecords SOQL used - '+Limits.getQueries());            
            
            salesforceLicUser = new User(
                Alias = 'sflic' , 
                profileId = salesforceLicenseId ,
                Email='mail@mail.coo' ,
                EmailEncodingKey='UTF-8' ,
                CommunityNickname = 'cNNlic',
                LastName='sFLic' ,
                LanguageLocaleKey= 'en_US' ,
                LocaleSidKey= 'en_US' ,
                TimeZoneSidKey= 'America/Los_Angeles' ,
                UserName='someusername@mygard.coo',
                contactId__c = grdobj.id,
                City= 'Arendal'                    
            );
            insert salesforceLicUser;
            
            System.debug('SOQL debug commonRecord() preAccount SOQL used - '+Limits.getQueries());
            brokerAcc = new Account(   Name='testre',
                                    BillingCity = 'Southampton',
                                    BillingCountry =  'city' ,
                                    BillingPostalCode = 'BS2 AD!',
                                    BillingState = 'Avon LAng' ,
                                    BillingStreet = '1 Mangrove Road',
                                    recordTypeId = System.Label.Broker_Contact_Record_Type,
                                    Site = '_www.tcs.se',
                                    Type =  'Broker' ,
                                    company_Role__c =  'Broker' ,
                                    Sub_Roles__c = 'Broker - Reinsurance Broker', 
                                    //Company_ID__c = '64143',
                                    guid__c = '8ce8ad89-a6ed-1836-9e17',
                                    Market_Area__c = Markt.id,
                                    Product_Area_UWR_2__c= 'P&I' ,
                                    Product_Area_UWR_4__c= 'P&I' ,
                                    Product_Area_UWR_3__c= 'Marine' ,
                                    Claim_handler_Marine_2_lk__c = salesforceLicUser.id ,
                                    Claim_handler_Cargo_Liquid__c = salesforceLicUser.id ,
                                    Role_Broker__c = true,
                                    Claim_handler_Charterers__c = salesforceLicUser.id ,
                                    Claim_handler_Defence__c = salesforceLicUser.id ,
                                    Claim_handler_Energy__c = salesforceLicUser.id ,
                                    Claim_handler_Energy_2_lk__c= salesforceLicUser.id ,
                                    Claims_handler_Builders_Risk_2_lk__c= salesforceLicUser.id ,
                                    Claim_handler_Charterers_2_lk__c = salesforceLicUser.id ,
                                    Claim_handler_Cargo_Liquid_2_lk__c = salesforceLicUser.id ,
                                    Claims_handler_Builders_Risk__c = salesforceLicUser.id ,
                                    Company_Role_Text__c = 'Broker',
                                    //Claim_handler_Charterers__c = salesforceLicUser.id ,
                                    Area_Manager__c = salesforceLicUser.id ,
                                    X2nd_UWR__c=salesforceLicUser.id,
                                    X3rd_UWR__c=salesforceLicUser.id,
                                    Underwriter_4__c=salesforceLicUser.id,
                                    Underwriter_main_contact__c=salesforceLicUser.id,
                                    UW_Assistant_1__c=salesforceLicUser.id,
                                    U_W_Assistant_2__c=salesforceLicUser.id,
                                    Claim_handler_Crew__c = salesforceLicUser.id,
                                    Enable_share_data__c = true,
                                    Company_Status__c='Active',
                                    GIC_Office_ID__c = null,
                                    Key_Claims_Contact__c = salesforceLicUser.id,
                                    Claim_Adjuster_Marine_lk__c = salesforceLicUser.id,
                                    Claim_handler_Cargo_Dry__c = salesforceLicUser.id,
                                    Claim_handler_CEP__c = salesforceLicUser.id,
                                    Claim_handler_Marine__c = salesforceLicUser.id,
                                    Claim_handler_Cargo_Dry_2_lk__c = salesforceLicUser.id,
                                    Claim_handler_CEP_2_lk__c = salesforceLicUser.id,
                                    Claim_handler_Crew_2_lk__c =  salesforceLicUser.id,
                                    Claim_handler_Defence_2_lk__c = salesforceLicUser.id,
                                    Accounting_P_I__c = salesforceLicUser.id,
                                    Accounting__c = salesforceLicUser.id,
                                    
                                    Confirm_not_on_sanction_lists__c = true,
                                    License_description__c  = 'some desc',
                                    Description = 'some desc',
                                    Licensed__c = 'Pending',
                                    owner = usr,
                                    Active__c = true,
                                    ownerId = userinfo.getUserId(),
                                    country__c = country.Id
                                    //Owner = salesforceLicUser
                                    //Account_Sync_Status__c = 'Synchronised'
                                    //Ownerid=salesforceLicUser.id
                                    
                                   );
            AccountList.add(brokerAcc);        
            
            clientAcc = new Account(  Name = 'Testuu', 
                                    Site = '_www.test_1.s', 
                                    Type =  'Customer' , 
                                    owner = usr ,
                                    BillingStreet = 'Gatanfol 1',
                                    BillingCity = 'phuket', 
                                    BillingCountry =  'City'  , 
                                    BillingPostalCode =  '123456' ,
                                    Client_On_Risk_Flag__c =true,
                                    company_Role__c =  'Client' ,
                                    //Company_ID__c = '64144',
                                    recordTypeId=System.Label.Client_Contact_Record_Type,
                                    Market_Area__c = Markt.id,
                                    guid__c = '8ce8ad66-a6ec-1834-9e21',
                                    Product_Area_UWR_2__c= 'P&I' ,
                                    Product_Area_UWR_4__c= 'P&I' ,
                                    Product_Area_UWR_3__c= 'Marine' ,
                                    Claim_handler_Marine_2_lk__c = salesforceLicUser.id ,
                                    X2nd_UWR__c = salesforceLicUser.id ,
                                    X3rd_UWR__c = salesforceLicUser.id ,
                                    Role_Client__c = true,
                                    Underwriter_4__c = salesforceLicUser.id ,
                                    Claim_handler_Cargo_Liquid__c = salesforceLicUser.id ,
                                    Claim_handler_Charterers__c = salesforceLicUser.id ,
                                    Claim_handler_Defence__c = salesforceLicUser.id ,
                                    Claim_handler_Energy__c = salesforceLicUser.id ,
                                    Claim_handler_Energy_2_lk__c= salesforceLicUser.id ,
                                    Claims_handler_Builders_Risk_2_lk__c= salesforceLicUser.id ,
                                    Claim_handler_Charterers_2_lk__c = salesforceLicUser.id ,
                                    Claim_handler_Cargo_Liquid_2_lk__c = salesforceLicUser.id ,
                                    Claims_handler_Builders_Risk__c = salesforceLicUser.id ,
                                    Area_Manager__c = salesforceLicUser.id,
                                    Underwriter_main_contact__c=salesforceLicUser.id,
                                    UW_Assistant_1__c=salesforceLicUser.id,
                                    U_W_Assistant_2__c=salesforceLicUser.id    ,
                                    Claim_handler_Crew__c = salesforceLicUser.id,
                                    Company_Status__c='Active',
                                    P_I_Member_Flag__c = true,
                                    Key_Claims_Contact__c = salesforceLicUser.id,
                                    Claim_Adjuster_Marine_lk__c = salesforceLicUser.id,
                                    Claim_handler_Cargo_Dry__c = salesforceLicUser.id,
                                    Claim_handler_CEP__c = salesforceLicUser.id,
                                    Claim_handler_Marine__c = salesforceLicUser.id,
                                    Claim_handler_Cargo_Dry_2_lk__c = salesforceLicUser.id,
                                    Claim_handler_CEP_2_lk__c = salesforceLicUser.id,
                                    Claim_handler_Crew_2_lk__c =  salesforceLicUser.id,
                                    Claim_handler_Defence_2_lk__c = salesforceLicUser.id,
                                    Accounting_P_I__c = salesforceLicUser.id,
                                    Accounting__c = salesforceLicUser.id,
                                    PEME_Enrollment_Status__c = 'Enrolled',
                                    country__c = country.Id,
                                    ownerId = userinfo.getUserId()
                                   );                                    
            AccountList.add(clientAcc); 
            insert AccountList;
            user usrABCD = [select UserRoleId, id from user where id =: brokerAcc.ownerId];
            system.debug('--Brokeraccount--'+brokerAcc.ownerId+'----'+usrABCD.UserRoleId);
            
            System.assertequals(AccountList.size(),2,'Accounts inserted successfully');
            System.debug('SOQL debug commonRecord() preContact SOQL used - '+Limits.getQueries());
            brokerContact = new Contact( 
                FirstName='Yoo',
                LastName='Baooo',
                MailingCity = 'Kingsville',
                OtherPhone =  '9876543211' ,
                mobilephone =  '9879876541' ,
                MailingCountry =  'Oslo' ,
                MailingPostalCode = 'SE3 1AD',
                MailingState =  'State' ,
                MailingStreet = '1 Eastwood Road',
                AccountId = brokerAcc.Id,
                Email = 'test32@gmail.com',
                Synchronisation_Status__c ='Synchronised'
            );
            ContactList.add(brokerContact);
            
            clientContact= new Contact(   FirstName='sat_1',
                                       LastName='hak',
                                       MailingCity =  'State' ,
                                       OtherPhone =  '9876543212' ,
                                       mobilephone =  '9879876542' ,
                                       MailingCountry =  'Oslo' ,
                                       MailingPostalCode = 'SE1 1AE',
                                       MailingState =  'State' ,
                                       MailingStreet = '4 London Road',
                                       AccountId = clientAcc.Id,
                                       Email =  'ccontact@gard.coo' ,
                                       Primary_Contact__c = true,
                                       Synchronisation_Status__c  ='Synchronised',
                                       Phone = '+1 8009009123'
                                      );
            ContactList.add(clientContact);
            insert ContactList;
            System.assertequals(ContactList.size(),2,'Contacts inserted successfully'); 
            
            System.debug('SOQL debug commonRecord() prePortalUser SOQL used - '+Limits.getQueries());
            brokerUser = new User(  Alias = 'brAl' , 
                                  profileId = partnerLicenseId ,
                                  Email='Claim@gard.no',
                                  EmailEncodingKey='UTF-8',
                                  userRole = SalesforceRoleId ,
                                  LastName='tetst_005',
                                  LanguageLocaleKey='en_US' ,
                                  CommunityNickname = 'brokerUser',
                                  LocaleSidKey='en_US' ,  
                                  TimeZoneSidKey= 'America/Los_Angeles' ,
                                  UserName='mygardtestbrokerUser@testorg.com.mygard',
                                  ContactId = BrokerContact.Id,
                                  contactId__c = grdobj.id,
                                  IsActive = true,
                                  City= 'Arendal'
                                 );
            userList.add(brokerUser);                          
            clientUser = new User(Alias = 'clAl' ,
                                  CommunityNickname = 'clientUser',
                                  Email='client@gard.coo',
                                  //Email='test_11@testorg.com', 
                                  profileid = partnerLicenseId ,
                                  EmailEncodingKey='UTF-8',
                                  LastName= 'cLN' ,
                                  LanguageLocaleKey= 'en_US' ,
                                  userRole = SalesforceRoleId,
                                  LocaleSidKey='en_US' ,
                                  TimeZoneSidKey= 'America/Los_Angeles' ,
                                  UserName='testclientUser@testorg.commygard.mygard',
                                  ContactId = clientContact.id,
                                  contactId__c = grdobj.id,
                                  City= 'Arendal'
                                 );
            
            userList.add(clientUser) ;   //commented on 15th june. to be uncommented
            
            insert userList;
        }
    }
    
    @isTest private static void testPartnerUser(){
        createTestData();
        Test.startTest();
        update salesforceLicUser;
        //System.runAs(salesforceLicUser){
        	update clientUser;
        //}
        Test.stopTest();
    }
    
}