public class ReportClaimsCtrl{
    
   /* //Selected Information
    public Account selectedAccount{get;set;}
    public Object__c selectedObject{get;set;}
    public Asset selectedCover{get;set;}
    
    public String selectedAccountId{get;set;}
    public String selectedObjectId{get;set;}
    public String selectedCoverId{get;set;}
    
    
    public String selectedClaimType{get;set;}
    public String eventDate{get;set;}
    
    //Reusable Variables
    public Contact loggedInContact;
    public Account loggedInAccount;
    public List<Account> clientAccounts{get;set;}
    public List<Object__c> clientObjects{get;set;}
    public List<Asset> clientCovers{get;set;}
    
    //Select Option list for showing
    public List<SelectOption> clientList{get;set;}
    public List<SelectOption> objectList{get;set;}
    public List<SelectOption> coverList{get;set;}
    public List<SelectOption> claimTypeList{get;set;}
    public String userType{get;set;}
    public Attachment attachment{get;set;}
    
    public Attachment attachment1{get;set;}
    public Attachment attachment2{get;set;}
    public Attachment attachment3{get;set;}
    public Attachment attachment4{get;set;}
    
    public List<Attachment> attachmentList{get;set;}
    
    //Varibles for Sending Emails    
    public String place{get;set;}
    public String claim_description{get;set;}{claim_description = '';}
    public String claimReference{get;set;}
    public String crewName{get;set;}
    public String crewRank{get;set;}
    public String crewNationality{get;set;}
    public String crewClaimDetail{get;set;}
    public List<SelectOption> crewClaimDetailList{get;set;}
    public String portOfEmbarkation{get;set;}
    public String dateOfEmbarkation{get;set;}
    public String numberOfStowAways{get;set;}
     
    
    //Util method to convert a Set to a sorted List
    public List<String> convertToList(Set<String> stringSet){
        List<String> convertedList = new List<String>();
        if(stringSet != null){
            for(String s : stringSet){
                convertedList.add(s);
            }
        }
        convertedList.sort();
        return convertedList;
    }
    
    public boolean showAcknowledgement{get;set;}{showAcknowledgement = false;}
    public void displayAcknowledgement(){
        showAcknowledgement = true;
    }
    public void closeAcknowledgement(){
        showAcknowledgement = false;
    }
    
    //Constructors
    public ReportClaimsCtrl(){
        
        //initialization
        clientList = new List<SelectOption>();
        objectList = new List<SelectOption>();
        coverList = new List<SelectOption>();
        claimTypeList = new List<SelectOption>();
        crewClaimDetailList = new List<SelectOption>();
        
        attachment = new Attachment();
        attachment1 = new Attachment();
        attachment2 = new Attachment();
        attachment3 = new Attachment();
        attachment4 = new Attachment();
        
        loggedInContact = new Contact();
        loggedInAccount = new Account();
        userType = '';
        eventDate = '';
        selectedObjectId = '';
        
        //Populate useful Information in the Class variables
        String userID = UserInfo.getUserId();
        String loggedInContactId = [SELECT id,Contactid from User where id = :UserInfo.getUserId()].contactId;
        loggedInContact = [SELECT id, firstname, lastname, phone, mobilephone, email, account.name, accountId from Contact where id = :loggedInContactId ];
        loggedInAccount = [SELECT id, name, Company_Role__c , recordTypeId from Account where id = :loggedInContact.accountid];
        
        //Determine if account is Client or Broker
        Set<String> relatedUniqueAccountIDSet = new Set<String>();
        if(loggedInAccount.Company_Role__c.contains('Broker')){
            userType = 'Broker';
            List<Contract> relatedContractList = [SELECT id, client__c from Contract where Broker__c = :loggedInAccount.id];
            for(Contract contract : relatedContractList){
                 if(!relatedUniqueAccountIDSet.contains(contract.client__c)){
                     relatedUniqueAccountIDSet.add(contract.client__c);
                 }
            }
        }
        else{
            userType = 'Client';
            relatedUniqueAccountIDSet.add(loggedInAccount.id);
        }
        
        
        //Populate the Client SelectList Options
        List<Account> clientAccounts = [SELECT id, name from Account where id in :relatedUniqueAccountIDSet ORDER BY name];
        //clientList.add(new SelectOption('Select Client', 'Select Client'));
        selectedAccountId = clientAccounts[0].id;
        //WIP
        //clientList.add(new SelectOption('', ''));
        
        for(Account a : clientAccounts){
            clientList.add(new SelectOption(a.id, a.name));
        }
        
        //Populate the Claim Type SelectList Options
        List<String> claimTypeOrderedList = new List<String>();
        for(Claim_Types_Master_List__c claimtype : Claim_Types_Master_List__c.getAll().values()){
            claimTypeOrderedList.add(claimtype.value__c);
        }
        claimTypeOrderedList.sort();

        //claimTypeList.add(new SelectOption('Select Claim Type', 'Select Claim Type'));
        claimTypeList.add(new SelectOption('', ''));
        for(String claim_type : claimTypeOrderedList){
            claimTypeList.add(new SelectOption(claim_type, claim_type));
        }

        //PopulateClaimDetail List
        List<String> temp = new List<String>();
        for(Master_Claim_Detail_List__c claimdetailtype : Master_Claim_Detail_List__c.getAll().values()){
            temp.add(claimdetailtype.value__c);
        }

        //alphabetically sorted
        temp.sort();
        //crewClaimDetailList.add(new SelectOption('Select Claim Detail','Select Claim Detail'));
        crewClaimDetailList.add(new SelectOption('', ''));
        for(String claim_detail_type : temp){
            crewClaimDetailList.add(new SelectOption(claim_detail_type, claim_detail_type));
        }
        
        //Clean the previous values
        objectList.clear();
        //objectList.add(new SelectOption('Select Object','Select Object'));
        objectList.add(new SelectOption('',''));
        coverList.clear();
    }
    
    //Method to refresh the object list once the selected cover is changed
    public void refreshObjects(){
        System.debug('selectedCoverId -->' + selectedCoverId);
        objectList.clear();
        if(selectedCoverId != 'None'){
            Asset selectedAsset = [SELECT id, object__c, product_name__c from Asset where id = :selectedCoverId]; //query id to product_name__c of the selected cover
            String coverName = selectedAsset.product_name__c;                        //extract covername
            objectList.add(new SelectOption('', ''));      //add default value
            List<String> tempObjectIdList = new List<String>();                      //extract ids
            for(Asset a : asset_list){                                               //match with asset list's product name
                if(a.product_name__c.equalsIgnoreCase(coverName)){
                    tempObjectIdList.add(a.object__c);
                }
            }
            //Query and populate the object's SelectOption list for showing
            for(Object__c o : [SELECT id, name from Object__c where id in :tempObjectIdList order by name]){
                objectList.add(new SelectOption(o.id, o.name)); 
            }
        }        
        else{
            objectList.add(new SelectOption('None', 'None'));
        }        
    }
    
    //Method to invoke when uer clicks the Cancel button
    public Pagereference CancelReportClaim(){
        Pagereference P = new Pagereference('/ReportClaims'); //changed as per 1207
        return P;
    }
    
    
    public void setSelectedObjectIdFromAF(){
        String o_id = apexpages.currentpage().getparameters().get('param_selectedobject');
        System.debug('o_id -->' + o_id);
        selectedObjectId = o_id;
        if(selectedObjectId.equalsIgnoreCase('None')){
            coverList.clear();
        }
    }
    
    public void setSelectedCoverIdFromAF(){
        selectedCoverId   = apexpages.currentpage().getparameters().get('param_selected_coverid');
        selectedClaimType = apexpages.currentpage().getparameters().get('param_selected_claimtype');
        System.debug('setSelectedCoverIdFromAF-selectedCoverId-->' + selectedCoverId);
        System.debug('setSelectedCoverIdFromAF-selectedClaimType-->' + selectedClaimType);
    }
    
    public void temp(){
        eventDate = '';
        coverList.clear();
        objectList.clear();
        //return null;
    }
    
    //Method to invoke when account and event date are refreshed
    List<Asset> asset_list = new List<Asset>();
    public void refreshCovers(){
        asset_list.clear();
        coverList.clear();
        
        System.debug('Value is eventDate-->' + eventDate);
        
        if(eventDate != null ||  eventDate != ''){
        
            //List<SelectOption> tempList = new List<SelectOption>();
            Set<String> uniqueAssetSet = new Set<String>();
            //List<Asset> asset_list = new List<Asset>();
            
            //This part is same for Client and Broker
            asset_list.clear();
            List<String> contractlist = new List<String>();
           
            //For Broker
            if(userType == 'Broker'){
                List<Contract> conList = [SELECT id, broker__c, client__c from Contract where broker__c = :loggedInAccount.id  and client__c = :selectedAccountId];
                for(Contract con : conList){
                   contractlist.add(con.id);
                }  
            }
           
            //For Client
            if(userType == 'Client'){
                List<Contract> clientcontracts = [SELECT id from Contract where client__c = :selectedAccountId and broker__c = null];
                //List<String conlists = new List<String>();
                for(Contract c : clientcontracts ){
                    contractlist.add(c.id);
                }
                List<Contract> brokercontracts = [SELECT id, shared_with_client__c from Contract where client__c = :selectedAccountId AND broker__c != null];
                
                System.debug('critical contractlist1-->' + selectedAccountId);
                System.debug('critical contractlist2-->' + brokercontracts);
            
                for(Contract c: brokercontracts){
                    if(c.shared_with_client__c){
                        contractlist.add(c.id);
                    }  
                }
               
            }
            
            System.debug('critical contractlist-->' + contractlist);
            
            System.debug('eventDate-->' + eventDate);
            String[] myDateOnly = eventDate.replace('/', '_').split('_');
            Date ev_dt = Date.newInstance(Integer.valueOf(myDateOnly[2]),  Integer.valueOf(myDateOnly[1]), Integer.valueOf(myDateOnly[0]));
            System.debug('ev_dt Processed -->' + ev_dt);
            
            if(contractlist.size() > 0){
                asset_list = [SELECT id, name, object__c, object__r.name, Agreement__c, product_name__c from Asset 
                                where product_name__c != null
                                AND Agreement__c in :contractlist
                                AND product_name__c != null
                                AND object__c != null
                                AND Inception_date__c != null
                                AND Expiration_Date__c != null
                                AND Inception_date__c <= :ev_dt
                                AND Expiration_Date__c >= :ev_dt
                                ORDER BY product_name__c];
            }
           
            if(asset_list != null && asset_list.size() > 0){
                coverList.add(new SelectOption('', ''));
                for(Asset a : asset_list){
                    if(!uniqueAssetset.contains(a.product_name__c)){
                        uniqueAssetset.add(a.product_name__c);
                        coverList.add(new SelectOption(a.id, a.product_name__c));
                    }
                }
            }
              
            //Handling when no asset is present/product_name__c is not populated
            if(coverList.size() == 0){
                coverList.add(new SelectOption('No Cover', 'No Cover'));
            }
           
            System.debug('coverList-->' + coverList);
            
        }
    }
    
    
    
    public string someval{get;set;}
    
    //Method to invoke when a claim is finally reported from the extranet portal
    public Pagereference SaveCase() {
        Id idToBeReferred;
        System.debug('selectedCoverID--selectedClaimType-----' + selectedCoverID + '____' + selectedClaimType); 
        System.debug('selectedAccountId ->' + selectedAccountId);
        System.debug('selectedObjectId ->' + selectedObjectId);
        System.debug('selectedCoverId ->' + selectedCoverId);
        System.debug('selectedClaimType ->' + selectedClaimType);
        System.debug('Event date ->' + eventDate);
        System.debug('claimReference ->' + claimReference);
        System.debug('place->' + place);
        System.debug('attachment name->' + attachment.name);
        System.debug('Claim Description->' + claim_description);
        
        
        Case c = new Case();
        c.accountId = selectedAccountId;
        if(userType == 'Broker'){
            c.contract_for_review__c = [SELECT id from Contract where broker__c = :loggedInAccount.id and client__c = :selectedAccountId limit 1].id;
        }
        else if(userType == 'Client'){
            c.contract_for_review__c = [SELECT id, agreement__c from Asset where id = :selectedCoverId].agreement__c;
        }
        c.gard_area__c = 'offshore';
        c.origin = 'Community';
        if(claimReference == null){
            c.member_Reference__c = '';
        }
        else{
            c.member_Reference__c = claimReference;
        }
        if(selectedCoverId != '' && selectedCoverId != null && selectedCoverId != 'None'){
            c.risk_coverage__c = selectedCoverId;
        }
        c.Place_of_incident__c = place;
        c.description = claim_description;
        c.accountId = selectedAccountId;
        c.object__c = selectedObjectId;
        c.status = 'Pending';
        c.claim_Type__c = selectedClaimType;
        
        //Crew Details
        c.Crew__c = crewName;
        c.Crew_Rank__c = crewRank;
        c.Crew_Nationality__c = crewNationality;
        c.Claim_Detail__c = crewClaimDetail;
        
        //Stowaway Details
        c.Port_Of_Embarkation__c = portOfEmbarkation;
       // c.Date_Of_Embarkation__c = dateOfEmbarkation;
        c.Number_Of_Stowaways__c = numberOfStowAways;
        
            

        if(eventDate != null && eventDate != ''){
            String[] myDateOnly = eventDate.replace('/', '_').split('_');
            Integer myIntDate = Integer.valueOf(myDateOnly[0]);
            Integer myIntMonth = Integer.valueOf(myDateOnly[1]);
            Integer myIntYear = Integer.valueOf(myDateOnly[2]);
            Date ev_dt = Date.newInstance(myIntYear, myIntMonth, myIntDate);
            c.Event_Date__c = ev_dt;
        }   
        
        //Send Email to requestor and Claim Handler
        if(selectedCoverId != 'None'){
            System.debug('selectedCoverId **>' + selectedCoverId );
            System.debug('selectedAccountId **>' + selectedAccountId);
            String cover = [SELECT product_name__c from Asset where id = :selectedCoverId].product_name__c;
            System.debug('I am in with cover-->' + cover);
            String email2BeSentTo, email2BeSentTo_Name, email2BeSentTo_Id, businessarea;
            for(Business_Area_Coverage_Mapping__c bacmapping : Business_Area_Coverage_Mapping__c.getAll().values()){
                if(bacmapping.Coverage_Description__c == cover){
                    businessarea = bacmapping.Business_Area_Code__c;
                    break;
                }
            }
            System.debug('businessarea -->' + businessarea);
            System.debug('selectedClaimType-->' + selectedClaimType);
            
            Account selectedAccount = [SELECT id,name,
                                                Claim_handler_Cargo_Dry__r.ContactId__c,
                                                Claim_handler_Cargo_Liquid__r.ContactId__c,
                                                Claim_handler_Defence__r.ContactId__c,
                                                Claim_handler_Crew__r.ContactId__c,
                                                Claim_handler_CEP__r.ContactId__c,
                                                Claim_handler_Energy__r.ContactId__c,
                                                Claim_handler_Marine__r.ContactId__c,
                                                Claims_handler_Builders_Risk__r.ContactId__c
                                                from Account where id =:selectedAccountId];
           
             System.debug('I am in with selectedAccount-->' + selectedAccount);
             if(selectedClaimType == null){selectedClaimType = '';}
             if(businessarea.equalsIgnoreCase('PI')){
                if(selectedClaimType.equalsIgnoreCase('Cargo') || selectedClaimType.equalsIgnoreCase('Unrecoverable GA')){
                    if(selectedAccount.Claim_handler_Cargo_Dry__c != null){
                       email2BeSentTo_Id = selectedAccount.Claim_handler_Cargo_Dry__r.ContactId__c;
                       idToBeReferred = selectedAccount.Claim_handler_Cargo_Dry__c;
                    }
                    else if(selectedAccount.Claim_handler_Cargo_Liquid__r != null){
                       email2BeSentTo_Id = selectedAccount.Claim_handler_Cargo_Liquid__r.ContactId__c;
                       idToBeReferred = selectedAccount.Claim_handler_Cargo_Liquid__c;
                    }
                }

                else if(selectedClaimType.equalsIgnoreCase('Defence')){
                       email2BeSentTo_Id = selectedAccount.Claim_handler_Defence__r.ContactId__c;
                       idToBeReferred = selectedAccount.Claim_handler_Defence__c;
                }

                else if(selectedClaimType.equalsIgnoreCase('Crew') || selectedClaimType.equalsIgnoreCase('Passenger') || selectedClaimType.equalsIgnoreCase('Person(s) other than crew or passenger') || selectedClaimType.equalsIgnoreCase('Stowaways')){
                    email2BeSentTo_Id = selectedAccount.Claim_handler_Crew__r.ContactId__c;
                    idToBeReferred = selectedAccount.Claim_handler_Crew__c;
                }
                else{
                    email2BeSentTo_Id = selectedAccount.Claim_handler_CEP__r.ContactId__c;
                    idToBeReferred = selectedAccount.Claim_handler_CEP__c;
                }

            }

            else if(businessarea.equalsIgnoreCase('Energy')){
                email2BeSentTo_Id = selectedAccount.Claim_handler_Energy__r.ContactId__c;
                idToBeReferred = selectedAccount.Claim_handler_Energy__c;
            }

            else if(businessarea.equalsIgnoreCase('MA')){
                System.debug('Inside MA');
                email2BeSentTo_Id = selectedAccount.Claim_handler_Marine__r.ContactId__c;
                idToBeReferred = selectedAccount.Claim_handler_Marine__c;
                System.Debug('email2BeSentTo_Id: ' + email2BeSentTo_Id + ' idToBeReferred: ' + idToBeReferred);
            }

            else if(businessarea.equalsIgnoreCase('Builders')){
                email2BeSentTo_Id = selectedAccount.Claims_handler_Builders_Risk__r.ContactId__c;
                idToBeReferred = selectedAccount.Claims_handler_Builders_Risk__c;
            }
            //debug
            //email2BeSentTo_Id = null;
            if(email2BeSentTo_Id != null && email2BeSentTo_Id != '' && email2BeSentTo_Id != 'None'){
                System.debug('email2BeSentTo_Id -->' + email2BeSentTo_Id );
                Gard_Contacts__c con = [SELECT id, firstname__c, lastname__c,  email__c from Gard_Contacts__c where id = :email2BeSentTo_Id];
                if(con.firstname__c != null){
                    email2BeSentTo_Name = con.firstname__c;
                }
                if(con.lastname__c != null){
                    email2BeSentTo_Name = email2BeSentTo_Name + ' ' + con.lastname__c;
                }
                
                if(con.email__c != null && con.email__c != ''){
                    email2BeSentTo = con.email__c;
                }
                else{
                    email2BeSentTo = 'claim@gard.no';
                }
            }
            else{
                email2BeSentTo = 'claim@gard.no';
            }
            
            //To stamp the Claim Recipient value
            String claimHandlerUserId = '';
            if(email2BeSentTo == 'claim@gard.no')
            {
                list<User> lst = new List<User>([SELECT id from User where email = 'claim@gard.no' limit 1]);
                if(lst.size()>0)
                    claimHandlerUserId = lst[0].id; 
            }
            else
            {
                System.Debug('--idToBeReferred: ' + idToBeReferred);
                //list<User> lst = new List<User>([SELECT id from User where email = 'claim@gard.no' limit 1]);
                list<User> lst = new List<User>([SELECT id from User where ID = :idToBeReferred limit 1]);
                if(lst.size()>0)
                    claimHandlerUserId = lst[0].id;
                                
            }
            
            if(claimHandlerUserId != null && claimHandlerUserId != ''){
                c.Claims_handler__c = claimHandlerUserId;
            }
            
            
            insert c;
            System.debug('Case is created-c.id- : ' + c); 
            
            //Pass Parameters to Email Wrapper
            EmailInfoWrapper em = new EmailInfoWrapper();
            em.registerClaim_caseID =  c.id;
            //to be finalized
            //em.registerClaim_myClaimId = [SELECT id, SFDC_Claim_Ref_ID__c from Case where id=:c.id].SFDC_Claim_Ref_ID__c ;
            em.registerClaim_myClaimId = [SELECT id,Case_Autonumber__c from Case where id=:c.id].Case_Autonumber__c;
            em.registerClaim_userType = userType;
            em.registerClaim_clientName = selectedAccount.name;
            em.registerClaim_referencenumber = claimReference;
            em.registerClaim_objectName = [SELECT id, name from Object__c where id =: selectedObjectId].name;
            em.registerClaim_eventDate = eventDate;
            em.registerClaim_place = place;
            em.registerClaim_coverName = cover ;
            em.registerClaim_claimType = selectedClaimType;
            em.registerClaim_claimDescription = claim_description;
            
            //Crew Details
            em.registerClaim_name = crewName;
            em.registerClaim_rank = crewRank;
            em.registerClaim_nationality = crewNationality;
            em.registerClaim_claimDetail = crewClaimDetail ;
            
            //Stowaway Details
            em.registerClaim_portOfEmbarkation = portOfEmbarkation;
            em.registerClaim_dateOfEmbarkation = dateOfEmbarkation;
            em.registerClaim_numberofStowaways = numberOfStowAways;
            
            
            //Make below fields blank if they are null
            if(loggedInContact.firstName == null)loggedInContact.firstName = '';
            if(loggedInContact.lastName == null)loggedInContact.lastName = '';
            if(loggedInContact.phone == null)loggedInContact.phone = '';
            if(loggedInContact.mobilePhone == null)loggedInContact.mobilePhone = '';
            if(loggedInContact.email == null)loggedInContact.email = '';
                        
            //Requestor Details
            em.clientBrokerOldAdminName = loggedInContact.firstName + ' ' + loggedInContact.lastName;
            em.clientBrokerOrganization = loggedInContact.account.name;
            em.clientBrokerAdminPhone = loggedInContact.phone;
            em.clientBrokerAdminMobile = loggedInContact.mobilePhone;
            em.clientBrokerAdminEmail = loggedInContact.email;
            em.registerClaim_recipientEmail = loggedInContact.email;
            em.registerClaim_recipientName = loggedInContact.firstName + ' ' + loggedInContact.lastName;
            
            //Sending attachment
            System.debug('attachment  is-->' + attachment);
            System.debug('attachment1 is-->' + attachment1);
            System.debug('attachment2 is-->' + attachment2);
            System.debug('attachment3 is-->' + attachment3);
            System.debug('attachment4 is-->' + attachment4);
            
            
            List<attachment> atList = new List<attachment>();
            
            if(attachment != null && attachment.body != null && attachment.name != ''){
                attachment.ParentId = c.Id;
                atList.add(attachment);
            }
            if(attachment1 != null && attachment1.body != null && attachment1.name != ''){
                attachment1.ParentId = c.Id;
                atList.add(attachment1);
            }
            if(attachment2 != null && attachment2.body != null && attachment2.name != ''){
                attachment2.ParentId = c.Id;
                atList.add(attachment2);
            }
            if(attachment3 != null && attachment3.body != null && attachment3.name != ''){
                attachment3.ParentId = c.Id;
                atList.add(attachment3);
            }
            if(attachment4 != null && attachment4.body != null && attachment4.name != ''){
                attachment4.ParentId = c.Id;
                atList.add(attachment4);
            }
            
            if(atList.size() > 0){
                try{
                    insert atList;
                    em.attachmentlist = atList;
                }
                catch(Exception e){
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
                }
                finally{
                    //attachment = null;attachment1 = null;attachment2 = null;attachment3 = null;attachment4 = null;
                }
            }
            
            System.debug('Final attachmentlist - ' + em.attachmentlist);
            
            //For Requestor
            em.send('registerclaim_copytoRequestor');
            
            //For Claim Handler
            
            if(email2BeSentTo.equalsIgnoreCase('claim@gard.no')){
                  email2BeSentTo = Claim_Admin_Email__c.getInstance('1').email_address__c;
            }
            
            em.registerClaim_recipientEmail = email2BeSentTo;
            em.registerClaim_recipientName = email2BeSentTo_Name;
            System.debug('em.registerClaim_recipientEmail-->' + em.registerClaim_recipientEmail);
            em.send('registerclaim_copytoClaimHandler');
            showAcknowledgement = true;
        }
        
        PageReference p = new Pagereference('/ReportClaims');
        p.setRedirect(true);
        return p;
    }*/
}