public class GardSurveyClone {
    public static boolean memberCloningProcess=false; 
    public static boolean cloningProcess=false;
    ApexPages.StandardController s;
    
    final fluidoconnect__Survey__c survey;
    
    Map <Id, fluidoconnect__Question__c> clonedQuestions = new Map <Id, fluidoconnect__Question__c>();
    List <fluidoconnect__Question_Option__c> clonedOptions = new List <fluidoconnect__Question_Option__c>();
    public boolean cloneSurveyMembers{get;set;}
    public Id clonedId{get;set;}
    public GardSurveyClone(ApexPages.StandardController std){
        s = std;
        survey = (fluidoconnect__Survey__c)s.getRecord();
        cloneSurveyMembers=true;
    }
        
    public PageReference doClone(){
        cloningProcess=true;
        // Clone the survey object
        fluidoconnect__Survey__c clone = survey.clone(false, true);
        insert clone;
        clonedId=clone.Id;

        // Collect original questions and clone them
        for(fluidoconnect__Question__c q : [select Id, fluidoconnect__Type__c, fluidoconnect__Survey__c, fluidoconnect__Sort_Order__c, fluidoconnect__Question_Description__c, 
                                    Name, fluidoconnect__Question_Text_local__c, fluidoconnect__Subheading__c, fluidoconnect__Subheading_local__c,
                                    fluidoconnect__Not_displayed_to_Invitee__c, fluidoconnect__Mandatory__c,
                                (select Name, fluidoconnect__Option_Label_Local__c, fluidoconnect__Question__c, fluidoconnect__Positive__c from fluidoconnect__Question_Options__r)
                                from fluidoconnect__Question__c 
                                where fluidoconnect__Survey__c = :survey.Id]){
            fluidoconnect__Question__c qc = q.clone(false, true);
            qc.fluidoconnect__Survey__c = clone.Id;    
            clonedQuestions.put(q.Id, qc);
            for(fluidoconnect__Question_Option__c qo : q.fluidoconnect__Question_Options__r){
                fluidoconnect__Question_Option__c qoc = qo.clone(false, true);
                clonedOptions.add(qoc);
            }
        }
        
        insert clonedQuestions.values();
        
        List <fluidoconnect__Question_Option__c> clonedOptionsInsert = new List <fluidoconnect__Question_Option__c>();
        for(fluidoconnect__Question_Option__c qo : clonedOptions){
            qo.fluidoconnect__Question__c = clonedQuestions.get(qo.fluidoconnect__Question__c).Id;
            clonedOptionsInsert.add(qo);
        }
        
        insert clonedOptionsInsert;
        
        //START cloning members
        // IF THERE ISN'T A CLONE_EVENT_MEMBER APEX JOB IN QUEUE 
        list<AsyncApexJob> ListJobsQueue = [SELECT Status,ApexClass.Name
                   FROM AsyncApexJob
                    WHERE  (Status = 'Processing' OR Status = 'Queued' OR Status = 'Preparing' ) and ApexClass.Name='GardCloneEventMembersBatch' ];
        
        if (ListJobsQueue.size()>0){        
                    //String samp = System.Label.Sample;
                     ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'GardCloneEventMembersBatch add error message. Please try again.');
                     ApexPages.addMessage(msg);
        }else { 
                memberCloningProcess=true;
                if(cloneSurveyMembers){
                    //Call batch class to update organiser email
                    if (clone.id!=null && survey.Id!=null) Database.executeBatch(new GardCloneEventMembersBatch(clone.id,survey.Id));     
                }
        }
        // END cloning members
           
        PageReference p = Page.Gard_Survey_Clone_Page;
        return p;        
    } 
    
    public PageReference goToClone(){
        PageReference p = new PageReference('/'+clonedId);
        return p;      
    }
    
}