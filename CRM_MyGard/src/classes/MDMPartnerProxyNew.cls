/**************************************************************************
* Author  : Arpan Muhuri
* Company : Cognizant
* Date    : 05/03/2018
***************************************************************************/
    
/// <summary>
///  This proxy class provides methods to access the MDM Partner Service
/// </summary>
public without sharing class MDMPartnerProxyNew extends MDMProxy {

    private Set<String> getAccountFields(String requestName) {
        Set<String> fields = new Set<String>();
        for (MDM_Service_Fields__c field : MDM_Service_Fields__c.getAll().values()) {
            if (requestName.equalsIgnoreCase(field.Request_Name__c) && 'Account'.equalsIgnoreCase(field.Source_Object__c) && 'Field'.equalsIgnoreCase(field.Type__c)) {
                fields.add(field.Field_Name__c.toLowerCase());
            }
        }
        return fields;
    }
    @TestVisible private Account getAccountById(id accountId, String requestName) {
        return (Account)Database.query(buildSoqlRequest(getAccountFields(requestName), 'Account', accountId));
    }
    private List<Account> getAccountsByIds(List<id> accountIds, String requestName) {
        return (List<Account>)Database.query(buildSoqlRequest(getAccountFields(requestName), 'Account', accountIds));
    }
    //added for merge XML request
    private Static List<Account> getAccountsSyncByIds(Set<id> accountIds) {
        return (List<Account>)[SELECT Billing_Address_Sync_Status__c,Shipping_Address_Sync_Status__c,Synchronisation_Status__c FROM Account WHERE ID IN:accountIds];
    }
    //added for merge XML request
    private DOM.XMLNode MDMUpsertPartners(MDMUpsertPartnerRequest request) {
        system.debug('inside MDMUpsertPartners--> '+request);
        DOM.XMLNode requestXML = sendRequestMerge(request);
        system.debug('value of responseDoc --> '+requestXML);
        
        return requestXML;
    }
    
    public DOM.XMLNode MDMUpsertPartner(Map<id, Account> upsertAccounts) {
        MDMUpsertPartnerRequest request = new MDMUpsertPartnerRequest();
        Map<id, Account> accounts = new Map<id, Account>(getAccountsByIds(new List<id> (upsertAccounts.keySet()), request.getName()));
        request.accounts = accounts.values();
        DOM.XMLNode requestString = MDMUpsertPartners(request);
        system.debug('Request--> '+requestString);
        return requestString;
    }
            //requestString = requestString + MDMUpsertBillingAddressAsync(upsertAccounts.keySet())) + MDMUpsertShippingAddressAsync(upsertAccounts.keySet()));
            /* call other request */
            
            /*single call out */
    @future (callout = true)
    public static void singleCallOut(String requestString,set<id> accountIdSet,Boolean partnerChange,Boolean roleChange,Boolean billingAddChange,Boolean shippingAddChange,Boolean isApproved){
        Map<id, Account> accounts = new Map<id, Account>(getAccountsSyncByIds(accountIdSet)); // to be removed on monday
        system.debug('singleCallOut accounts param-->'+accounts);
        DOM.Document responseDoc = new DOM.Document();
            responseDoc = WebServiceUtil.processRequest(MDMConfig__c.getInstance().Endpoint__c, requestString, null);
            if(responseDoc!=null){
                System.Debug('Response: ' + responseDoc.toXMLString());
            }
            MDMUpsertPartnerResponse response = new MDMUpsertPartnerResponse();
            try {
                response.Deserialize(
                    responseDoc
                        .getRootElement()
                        .getChildElement('Body', MDMConfig__c.getInstance().SOAPNS__c)
                        .getChildElement('mdmChangeResponse', 'http://www.gard.no/mdm/v1_0/mdmpartner')
                );
            } catch (Exception ex) {
                throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMPartnerProxyNew.MDMUpsertPartnerResponse', ex);
            }
            try{
                for (MDMResponse mdmResponse : response.MDMResponses) {
                    System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
                    if (mdmResponse.Success) {
                    //added for updating account fields
                        if((partnerChange || roleChange) && !isApproved){
                            system.debug('Role successful..!! -acc Id- !!'+mdmResponse.Id);
                            accounts.get(mdmResponse.Id).Synchronisation_Status__c = 'Synchronised';
                        }
                        
                        //Added aritra
                        if(roleChange && !isApproved){
                            List<MDM_Role__c> syncinProgressroles = [SELECT id from MDM_Role__c 
                                                                        where Account__c = :mdmResponse.Id and Synchronisation_Status__c = 'Sync In Progress'];
                            if(syncinProgressroles != null && syncinProgressroles.size() > 0){
                                for (MDM_Role__c  m : syncinProgressroles ){
                                    m.Synchronisation_Status__c = 'Synchronised';
                                    system.debug('m.Synchronisation_Status__c-->'+m.Synchronisation_Status__c);
                                }
                                update syncinProgressroles;
                            }
                        }
                        //Added aritra
                        //Added by Arpan
                        if(roleChange && isApproved){
                            List<MDM_Role__c> syncinProgressroles = [SELECT id,Account__c  from MDM_Role__c where Id = :mdmResponse.Id];
                            accounts.get(syncinProgressroles[0].Account__c).Synchronisation_Status__c = 'Synchronised';
                            system.debug('account sync successful..!!'+accounts.get(mdmResponse.Id).Synchronisation_Status__c);
                        }
                        //Added by Arpan
                        if(billingAddChange){
                            system.debug('billing address successful..!!');
                            accounts.get(mdmResponse.Id).Billing_Address_Sync_Status__c = 'Synchronised';
                        }
                        if(shippingAddChange){
                            system.debug('shipping address successful..!!');
                            accounts.get(mdmResponse.Id).Shipping_Address_Sync_Status__c = 'Synchronised';
                        }
                    } else {
                        system.debug('Failure..!!');
                        if(partnerChange || roleChange){
                            system.debug('Role Sync Failed..!!');
                            accounts.get(mdmResponse.Id).Synchronisation_Status__c = 'Sync Failed';
                        }
                        if(billingAddChange){
                            system.debug('billing address Sync Failed..!!');
                            accounts.get(mdmResponse.Id).Billing_Address_Sync_Status__c = 'Sync Failed';
                        }
                        if(shippingAddChange){
                            system.debug('shipping address Sync Failed..!!');
                            accounts.get(mdmResponse.Id).Shipping_Address_Sync_Status__c = 'Sync Failed';
                        }
                    } 
                }
            }catch(Exception ex){
                System.debug('Exception from singleCallOut'+ex.getMessage());
            }
            system.debug('Finally updated..');
            update accounts.values(); 
            
    }
    
    public static DOM.XMLNode MDMUpsertPartnerAsync(list<id> accountIds) {
        MDMPartnerProxyNew proxy = new MDMPartnerProxyNew();
        MDMUpsertPartnerRequest request = new MDMUpsertPartnerRequest();
        Map<id, Account> accounts = new Map<id, Account>(proxy.getAccountsByIds(accountIds, request.getName()));
        system.debug('proxy.MDMUpsertPartner(accounts)--> '+accounts);
        DOM.XMLNode partnerRequest = proxy.MDMUpsertPartner(accounts);
        return partnerRequest;
    }
    
    //Called from an exception to set all sync status to failed
    @TestVisible private static void SetAccountsSyncFailed(List<account> accounts, String source) {
        try {
            if (accounts != null) {
                for (Account a : accounts) {
                    a.Synchronisation_Status__c = 'Sync Failed';
                }
                update accounts; 
            }
        } catch (Exception ex) {
            //Simple update, hopefully shouldn't get here.
            Logger.LogException(source, ex);
        }
    }
    
    
/// ----------------------------------------------------------------------------
///  -- Sub classes for the request and response messages called above
/// ----------------------------------------------------------------------------
    public virtual class MDMPartnerRequest extends MDMRequest {
        //TODO: move to custom setting...
        protected String MDMChangePartnersRequest_Label {
            get {
                return 'changePartnersRequest';
            }
        }
        protected String MDMChangePartnersRequest_Namespace {
            get {
                return 'http://www.gard.no/mdm/v1_0/mdmmessage';
            }
        }
        protected String MDMChangePartnersRequest_Prefix {
            get {
                return 'mdm';
            }
        }
        
        public virtual override String getName() { return 'changePartnersRequest'; }
        public List<Account> Accounts { get; set; }
                
        public virtual override DOM.Document getSoapBody() {
            return getSoapBody(this.getName(), MDMChangePartnersRequest_Namespace, MDMChangePartnersRequest_Prefix);
        }
        
        public DOM.Document getSoapBody(string method, String namespace, String prefix) {
            system.debug('*** method: '+method);
            DOM.Document body = new DOM.Document();
            DOM.XMLNode root = body.createRootElement(MDMChangePartnersRequest_Label, MDMChangePartnersRequest_Namespace, MDMChangePartnersRequest_Prefix);     

            //Get XML element list from Custom Setting
            List<MDM_Service_Fields__c> serviceFields = [
                SELECT Name, Request_Name__c, Sequence__c, Namespace_Prefix__c, Parent_Node__c, Type__c, XmlLabel__c, Source_Object__c, Field_Name__c, Address_Type__c, Omit_Node_If_Null__c, Strip_Special_Characters__c, Strip_Whitespaces__c
                FROM MDM_Service_Fields__c 
                WHERE Request_Name__c = :method
                ORDER BY Sequence__c];
            
            for(Account a : this.Accounts) {
                DOM.XmlNode partnerXml = root.addChildElement(method, namespace, prefix);
                appendSObjectXml(partnerXml, a, serviceFields);
            }
            return body;
        }
    }
    
    public class MDMUpsertPartnerRequest extends MDMPartnerRequest {
        public override String getName() { return 'upsertPartnerRequest'; }
    }
    
    public virtual class MDMChangeResponse { 
        public List<MDMResponse> mdmResponses { get; set; }
        
        public MDMChangeResponse() {
            mdmResponses = new List<MDMResponse>();
        }
        
        protected void Deserialize(Dom.XmlNode root, String parentNode) {
            try {
                for  (Dom.XmlNode node : root.getChildElements()) {
                    try {
                        XmlSerializer s = new XmlSerializer();
                        object o = s.Deserialize(node, 'MDMPartnerProxyNew.MDMResponse');
                        mdmResponses.add((MDMResponse)o);
                    } catch (Exception ex) {
                        //TODO: handle exceptions better...
                        //Don't let one failure fail the whole batch...?
                    }
                }
            } catch (Exception ex) {
                //TODO: handle exceptions better...
                throw ex;
            }
        }
    }
    
    public class MDMResponse implements XmlSerializable {
        public Boolean Success { get; set; }
        public String Id { get; set; }
        public String Error { get; set; }
        
        public object get(string fieldName) {
            Map<string, object> thisobjectmap = new Map<string, object> 
            { 
                'id' => this.Id, 
                'success' => this.Success,
                'error' => this.Error
            };
            return thisobjectmap.get(fieldname);
        }
        
        public boolean put(string fieldName, object value) {
            if (fieldName == 'id') {
                this.Id = String.valueOf(value);
            } else if (fieldName == 'success') {
                this.Success = Boolean.valueOf(value);  
            } else if (fieldName == 'error') {
                this.Error = String.valueOf(value);
            } else {
                return false;
            }
            return true;
        }
        
        public Set<string> getFields() {
            Set<string> fields = new Set<string> 
            { 
                'success',
                'id',
                'error'
            };
            
            return fields;
        }
    }
    
    public class MDMUpsertPartnerResponse extends MDMChangeResponse {
        public void Deserialize(Dom.XmlNode root) {
            Deserialize(root, 'MDMPartnerProxyNew.MDMChangeResponse');
        }
    }

}