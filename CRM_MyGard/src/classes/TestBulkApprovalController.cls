@isTest
Class TestBulkApprovalController
{    
    public static testmethod void cover()
    { 
         String salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
         
         Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
         insert Markt; 
          //correspondant contacts Custom Settings
        List<ContactSubscriptionFields__c> conFieldsList = new List<ContactSubscriptionFields__c>();
        ContactSubscriptionFields__c conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        ContactSubscriptionFields__c conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        ContactSubscriptionFields__c conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;
         Gard_Contacts__c grdobj = new  Gard_Contacts__c(
                                                FirstName__c = 'Testreqchng',
                                                LastName__c = 'Baann',
                                                //Company_Type__c = 'Grd', 
                                                Email__c = 'WR_gards.12@Test.com',
                                                //MailingCity__c = 'Arendal', 
                                                //MailingCountry__c = 'Norway', 
                                               // MailingPostalCode__c =  '487155',
                                               // MailingState__c = 'Oslo', 
                                               // MailingStreet__c = 'Rd Road', 
                                                //Mobile__c = '98323322', 
                                                MobilePhone__c = '548645', 
                                                Nick_Name__c  = 'NilsPeter', 
                                                Office_city__c = 'Arendal', 
                                                Phone__c = '5454454',
                                                Portal_Image__c ='<img alt="User-added image" src="https://c.cs8.content.force.com/servlet/rtaImage?eid=a1hL0000000kSyZ&amp;feoid=00NL0000003OySC&amp;refid=0EML00000008Zjs"></img>', 
                                                Title__c = 'test'
                                               );
            insert grdobj ;
        
           User salesforceLicUser = new User(
                                        Alias = 'standt', 
                                        profileId = salesforceLicenseId ,
                                        Email='standarduser@testorg.com',
                                        EmailEncodingKey='UTF-8',
                                        CommunityNickname = 'test13',
                                        LastName='Testing',
                                        LanguageLocaleKey='en_US',
                                        LocaleSidKey='en_US', 
                                        //contactID = brokercontact.id, 
                                        TimeZoneSidKey='America/Los_Angeles',
                                        UserName='mygardtest008@testorg.com.mygard',
                                        contactId__c = grdobj.id
                                   );
         insert salesforceLicUser;
         
         Account brokerAcc = new Account(   Name='testre',
                                    BillingCity = 'Southampton',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS2 AD!',
                                    BillingState = 'Avon LAng' ,
                                    BillingStreet = '1 Mangrove Road',
                                    recordTypeId=System.Label.Broker_Contact_Record_Type,
                                    Site = '_www.tcs.se',
                                    Type = 'Customer',
                                    company_Role__c = 'Customer',
                                    //Company_ID__c = '64143',
                                    guid__c = '8ce8ad89-a6ed-1836-9e17',
                                    Market_Area__c = Markt.id,
                                    Product_Area_UWR_2__c='P&I',
                                    Product_Area_UWR_4__c='P&I',
                                    Product_Area_UWR_3__c='Marine',
                                    Claim_handler_Marine_2_lk__c = salesforceLicUser.id ,
                                    Claim_handler_Cargo_Liquid__c = salesforceLicUser.id ,
                                    Role_Broker__c = true,
                                    Claim_handler_Charterers__c = salesforceLicUser.id ,
                                    Claim_handler_Defence__c = salesforceLicUser.id ,
                                    Claim_handler_Energy__c = salesforceLicUser.id ,
                                    Claim_handler_Energy_2_lk__c= salesforceLicUser.id ,
                                    Claims_handler_Builders_Risk_2_lk__c= salesforceLicUser.id ,
                                    Claim_handler_Charterers_2_lk__c = salesforceLicUser.id ,
                                    Claim_handler_Cargo_Liquid_2_lk__c = salesforceLicUser.id ,
                                    Claims_handler_Builders_Risk__c = salesforceLicUser.id ,
                                    //Claim_handler_Charterers__c = salesforceLicUser.id ,
                                    Area_Manager__c = salesforceLicUser.id ,
                                    X2nd_UWR__c=salesforceLicUser.id,
                                    X3rd_UWR__c=salesforceLicUser.id,
                                    Underwriter_4__c=salesforceLicUser.id,
                                    Underwriter_main_contact__c=salesforceLicUser.id,
                                    UW_Assistant_1__c=salesforceLicUser.id,
                                    U_W_Assistant_2__c=salesforceLicUser.id,
                                    Claim_handler_Crew__c = salesforceLicUser.id,
                                    Enable_share_data__c = true,
                                    Company_Status__c='Active',
                                    GIC_Office_ID__c = null,
                                    Key_Claims_Contact__c = salesforceLicUser.id,
                                    Claim_Adjuster_Marine_lk__c = salesforceLicUser.id,
                                    Claim_handler_Cargo_Dry__c = salesforceLicUser.id,
                                    Claim_handler_CEP__c = salesforceLicUser.id,
                                    Claim_handler_Marine__c = salesforceLicUser.id,
                                    Claim_handler_Cargo_Dry_2_lk__c = salesforceLicUser.id,
                                    Claim_handler_CEP_2_lk__c = salesforceLicUser.id,
                                    Claim_handler_Crew_2_lk__c =  salesforceLicUser.id,
                                    Claim_handler_Defence_2_lk__c = salesforceLicUser.id,
                                    Accounting_P_I__c = salesforceLicUser.id,
                                    Accounting__c = salesforceLicUser.id
                                    
                                    //Account_Sync_Status__c = 'Synchronised'
                                    //Ownerid=salesforceLicUser.id
                                                                                                      
                               );
          insert brokerAcc;
          
          Contact brokerContact = new Contact( 
                                        FirstName='Yoo',
                                        LastName='Baooo',
                                        MailingCity = 'Kingsville',
                                        OtherPhone = '1112223356',
                                        mobilephone = '1112223334',
                                        MailingCountry = 'United Kingdom',
                                        MailingPostalCode = 'SE3 1AD',
                                        MailingState = 'London',
                                        MailingStreet = '1 Eastwood Road',
                                        AccountId = brokerAcc.Id,
                                        Email = 'test32@gmail.com'
                                   );
           insert brokerContact;
           
           Opportunity opp1 = new Opportunity(Name='test opp 1',Budget_Status__c = 'Budget In Progress', StageName = 'Renewable Opportunity', CloseDate = System.today(), AccountId = brokerAcc.Id, Area_Manager__c = salesforceLicUser.id);
            Opportunity opp2 = new Opportunity(Name='test opp 2',Budget_Status__c = 'Budget In Progress', StageName = 'Renewable Opportunity', CloseDate = System.today(), AccountId = brokerAcc.Id, Area_Manager__c = salesforceLicUser.id);
            List<Opportunity> lopp = new List<Opportunity>();
            lopp.add(opp1);
            lopp.add(opp2);
            insert(lopp);
            
            //set up the test records which will trigger the exception (exclude record adjustment from one of the oppty records)
            Opportunity opp3 = new Opportunity(Name='test opp 3',Budget_Status__c = 'Budget In Progress', StageName = 'Under Approval - AM', CloseDate = System.today(), AccountId = brokerAcc.Id, Area_Manager__c = salesforceLicUser.id);
            Opportunity opp4 = new Opportunity(Name='test opp 4',Budget_Status__c = 'Budget In Progress', StageName = 'Under Approval - AM', CloseDate = System.today(), AccountId = brokerAcc.Id, Area_Manager__c = salesforceLicUser.id);
            List<Opportunity> lopp2 = new List<Opportunity>();
            lopp2.add(opp3);
            lopp2.add(opp4);
            insert(lopp2);
            
            PageReference pageRef = Page.BulkApproval;
            Test.setCurrentPage(pageRef);
            
            // controller for valid opportunities
            List<Opportunity> lopp_test = new List<Opportunity>();
            lopp_test = [Select Name,Budget_Status__c, StageName, CloseDate, AccountId from Opportunity where Id = :opp1.Id OR Id = :opp2.Id];
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lopp_test);
            BulkApprovalController bac = new BulkApprovalController(sc);
            
            // controller for opportunities which don't satisfy approval entry criteria
            List<Opportunity> lopp2_test = new List<Opportunity>();
            lopp2_test = [Select Name,Budget_Status__c, StageName, CloseDate, AccountId from Opportunity where Id = :opp3.Id OR Id = :opp3.Id];
            ApexPages.StandardSetController sc2 = new ApexPages.StandardSetController(lopp2_test);
            BulkApprovalController bac2 = new BulkApprovalController(sc2);
            List<Id> conId = new List<Id>();
            List<Contact> contactRec = [SELECT Id FROM Contact];
            for(Contact c: contactRec)
            {
            conId.add(c.Id);
            }
            //execute test case
            Test.StartTest();
            bac.submitAll();
            System.Assert(bac.GetRecordCOunt() == 2);
            bac.OppPage();
            bac.sendEmailNotification(conId);
            //System.Assert([Select Budget_Status__c from Opportunity where Id = :opp1.Id].Budget_Status__c == 'Under Approval - AM');  // validate that the approval process has changed the budget status
            
            //test with an opportunity which fails entry criteria, this ensures coverage of the exception code
            bac2.submitAll();
            BulkApprovalController bac2_b = new BulkApprovalController(sc2);
            //System.Assert([Select Budget_Status__c from Opportunity where Id = :opp3.Id].Budget_Status__c == 'Budget In Progress');  // validate that the status stays the same (i.e. thew approval process hasn't changed the status)
            
            Test.StopTest();
    }     
}