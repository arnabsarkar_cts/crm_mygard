@isTest(seeAllData=false)
Class TestRenewalUpdatesCtrl
{     
    public static testmethod void cover()
    {
        //List<RecordType> recordTypeList = new List<RecordType>();
        String recNewsID = [Select ID from RecordType where SobjectType =: 'PI_Renewal__c' AND Name =: 'News' limit 1].ID;
        String recRenUpdatesID = [Select ID from RecordType where SobjectType =: 'PI_Renewal__c' AND Name =: 'Renewal Updates' limit 1].ID;
        String recGenIncreaseID = [Select ID from RecordType where SobjectType =: 'PI_Renewal__c' AND Name =: 'General Increase' limit 1].ID;
        /*recordTypeList.add(recNews);
        recordTypeList.add(recRenUpdates);
        recordTypeList.add(recGenIncrease);   
        insert recordTypeList;*/
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
         list<MyGardDocumentAvailability__c> docAvailableList = new List<MyGardDocumentAvailability__c>();
                MyGardDocumentAvailability__c docAvailabilityPF = new MyGardDocumentAvailability__c(name='My Portfolio Document Tab',IsAvailable__c=true);
                docAvailableList.add(docAvailabilityPF);
                MyGardDocumentAvailability__c docAvailabilityOD = new MyGardDocumentAvailability__c(name='Object Details Document Tab',IsAvailable__c=true);
                docAvailableList.add(docAvailabilityOD);
                MyGardDocumentAvailability__c docAvailabilityBC = new MyGardDocumentAvailability__c(name='P&I Renewal Blue Card Tab',IsAvailable__c=true);
                docAvailableList.add(docAvailabilityBC);
                MyGardDocumentAvailability__c docAvailabilityRC = new MyGardDocumentAvailability__c(name='P&I Renewal Certificate Tab',IsAvailable__c=true);
                docAvailableList.add(docAvailabilityRC);
                insert docAvailableList;
         PIrenewalAvailability__c PRA = new PIrenewalAvailability__c(Name = 'value', Enable_Certificate__c = true, Enable_Claims_review__c = true, Enable_General_increase__c = true,
                                                                            Enable_Documentation__c = true, Enable_Portfolio_report__c = true, Enable_Renewal_terms__c = true,
                                                                            Enable_Renewal_Update__c = true, PIRenewalAvailability__c = true
                                                                            );
                insert PRA;
        List<PI_Renewal__c> piRenewalList = new List<PI_Renewal__c>();
        
        PI_Renewal__c piNews = new PI_Renewal__c(
                                                  Body__c = 'this is the news sections for testing',
                                                  Header__c = 'news',
                                                  Indentation__c= 'Main News',
                                                  Video_URL__c = 'https://player.vimeo.com/video/145645255',
                                                  Display_Video__c = true,
                                                  Attachment_Type__c = 'None',
                                                  RecordTypeId = recNewsID
                                                 );
        PI_Renewal__c piRenUpdates = new PI_Renewal__c(
                                                       Renewal_Updates__c = 'this is the renewal updates section for testing',
                                                       RecordTypeId = recRenUpdatesID
                                                       );
        PI_Renewal__c piGI = new PI_Renewal__c(
                                                About__c = 'this is the genral increase section for testing',
                                                RecordTypeId = recGenIncreaseID
                                               );
        piRenewalList.add(piNews);
        piRenewalList.add(piRenUpdates);
        piRenewalList.add(piGI); 
        insert piRenewalList;
        Account acc = TestDataGenerator.getClientAccount();  //sfedit pulkit
        //insert acc;
    Contact con = TestDataGenerator.createContactFor(acc);
        //insert con;
        AccountToContactMap__c testacmCS = new AccountToContactMap__c(Name = (''+con.id),AccountId__c = (''+acc.id));
        insert testacmCS;
        //User adminUsr = TestDataGenerator.getAdminUser();
        //adminUsr.ContactId
        //00eD0000001V9HO
        string partnerLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community' and name='Partner Community Login User Custom' limit 1].id;
        User adminUsr = new User(
      Username = 'testuser123456_test@example.com'
      ,LastName = 'Test-User_1'
      ,Email = 'testuser@example.com'
     , Alias = 'J. Doe'
      ,CommunityNickname = 'username'
      ,TimeZoneSidKey = 'Europe/Amsterdam'
      ,LocaleSidKey = 'en_US'
      ,EmailEncodingKey = 'ISO-8859-1'
      ,ProfileId =  partnerLicenseId
      ,ContactId = con.id
      ,LanguageLocaleKey = 'en_US');
      insert adminUsr;
      
      
        //update adminUsr;
        Account_Contact_Mapping__c testacm = new Account_Contact_Mapping__c(Account__c = acc.id,
                                                           Active__c = true,
                                                           Administrator__c =  'Admin' ,
                                                           Contact__c = con.id,
                                                           IsPeopleClaimUser__c = false,
                                                           Marine_access__c = true,
                                                           PI_Access__c = true
                                                           //Show_Claims__c = false,
                                                           //Show_Portfolio__c = false 
                                                           );
        insert testacm;
        System.runAs(adminUsr){
        Test.startTest();
        
        RenewalUpdatesCtrl rUpCtrl = new RenewalUpdatesCtrl();
        // added by ss
        
        rUpCtrl.fetchNews();
        System.assertEquals('None',piNews.Attachment_Type__c);
        rUpCtrl.fetchGeneralIncreaseAbout(); 
        
        Test.stopTest();       
        }
    }
}