@istest(seealldata = false)
public class TestMycoverlistctrl{
    public static string testStr =  'efgh';
    public static testmethod void MycoverList()
        { 
              Gardtestdata test_rec = new Gardtestdata();
              test_rec.Mycover_list_objectCtrl_testrecord();//testobj 
              
              Gardtestdata.coverlist1.guid__c= '8ce8ad79-a6ed-1837-9e11';
              //Gardtestdata.coverlist1.underwriter__c = Gardtestdata.broker_User.id;
              update Gardtestdata.coverlist1;
              
              Gardtestdata.testobj.guid__c = '8cc8ad79-a6cd-1937-9e11';
              update Gardtestdata.testobj;
    
    System.runAs(Gardtestdata.broker_User)
        {
                Test.startTest();
                Pagereference pge = page.MyCoverlist;
                pge.getparameters().put('id',Gardtestdata.coverlist1.guid__c);
                test.setcurrentpage(pge);
                insert new MyGardDocumentAvailability__c(IsAvailable__c = true,Name ='My Portfolio Document Tab');
                insert new MyGardDocumentAvailability__c(IsAvailable__c = false,Name ='My Portfolio Document Tab');
                MyCoverListCtrl test_broker= new MyCoverListCtrl();
                test_broker.jointSortingParam = 'DESC##Agreement__r.Business_Area__c';
                MyCoverListCtrl.coverwrapper temp = new MyCoverListCtrl.coverwrapper('abcd', testStr ,'ijkl', 'mnop','qrst','uvwx', 'yz', 'no',Date.valueof('2014-12-6'),'hret','ccc','cccd','wswsws','ghgj', testStr,'0123456789012345' );
                //test_broker.strParams = 'TestTing##nu&ll##null##cts##2014-08-23 00:00:00##2014##No##50##true';
                test_broker.strParams = Gardtestdata.coverlist1.guid__c;
                //PageReference mycoverlist_page = Page.MyCoverList;
                //mycoverlist_page.getParameters().put('underwriter','test_underwriter');
                //mycoverlist_page.getParameters().put('product','test_product');
                //mycoverlist_page.getParameters().put('productarea','test_prod_area');
                //mycoverlist_page.getParameters().put('broker','test_broker');
                //mycoverlist_page.getParameters().put('policyyear','2014');
               // mycoverlist_page.getParameters().put('claimslead','test_lead');
                //mycoverlist_page.getParameters().put('gardshare','test_share');
                //mycoverlist_page.getParameters().put('expirydate','2014-08-23 00:00:00');
                //mycoverlist_page.getParameters().put('Onrisk','true');
                //Test.setCurrentPage(mycoverlist_page);
                test_broker.lstObjectTypeOptions = new set<string>();
                test_broker.lstObjectTypeOptions.add('Barge');
                test_broker.lstObjectTypeOptions.add('Container Vessel');
                test_broker.SelectedObjectType = new List<string>();
                test_broker.SelectedObjectType.add('Barge');
                test_broker.SelectedObjectType.add('Container Vessel');
                test_broker.SelectedObjectType.add('Bulk Vessel');
                test_broker.underwrId=Gardtestdata.salesforceLicUser_1.id+'#'+Gardtestdata.salesforceLicUser_1.id;
                test_broker.underwriterPopup();
                //test_broker.viewObject();
                test_broker.ViewData();
                System.assertEquals(test_broker.blBrokerView, true);
                test_broker.getCovers();
                test_broker.exportToExcel();
                MyCoverListCtrl.getGenerateTimeStamp();
                MyCoverListCtrl.getGenerateTime();
                MyCoverListCtrl.getUserName();
                string str = test_broker.sortExpression;
                test_broker.setSortDirection(str);
                test_broker.getSortDirection();
                test_broker.getObjectTypeOptions();
                test_broker.lstRiskCover = new List<Asset>();
                test_broker.lstRiskCover.add(gardtestData.broker_Asset);
                test_broker.searchAssets();
                test_broker.blBrokerView=False;
                test_broker.searchAssets();
                test_broker.blBrokerView=True;
                test_broker.searchAssets(); 
                test_broker.setpageNumber();
                test_broker.hasNext=true;
                test_broker.hasPrevious=true;
                test_broker.pageNumber=100; 
                test_broker.navigate();
                test_broker.next();
                test_broker.previous();
                test_broker.last();
                test_broker.first();
                test_broker.clear();
                test_broker.print();
                test_broker.strParams = Gardtestdata.testobj.id;
                test_broker.viewObject();
          }
          
          
          system.RunAs(Gardtestdata.client_User)
          {
              Pagereference pge1 = page.MyCoverlist;
              pge1.getparameters().put('id',Gardtestdata.coverlist1.guid__c);
              test.setcurrentpage(pge1);
              MyCoverListCtrl test_client= new MyCoverListCtrl();
              

              test_client.jointSortingParam = 'DESC##Agreement__r.Business_Area__c';
              MyCoverListCtrl.coverwrapper temp = new MyCoverListCtrl.coverwrapper('abcd', testStr ,'ijkl', 'mnop','qrst','uvwx', 'yz', 'no',Date.valueof('2014-12-6'),'hret','ccc','cccd','wswsws','ghgj', testStr,'0123456789012345' );//parameter added by Abhirup
              //test_client.strParams = 'TestTing##null##null##cts##2014-08-23 00:00:00##2014##No##50##true';
             //apexpages.getParameters().put(test_client.strParams,Gardtestdata.testobj.guid__c);
               test_client.strParams = Gardtestdata.coverlist1.guid__c;
               system.debug('for testing of coverlist'+[select underwriter__c from MyCoverList__c]);
              //PageReference mycoverlist_page = Page.MyCoverList;
             // mycoverlist_page .getParameters().put('underwriter','test_underwriter');
              //mycoverlist_page .getParameters().put('product','test_product');
              //mycoverlist_page .getParameters().put('productarea','test_prod_area');
              //mycoverlist_page .getParameters().put('broker','test_broker');
              //mycoverlist_page .getParameters().put('policyyear','2014');
              //mycoverlist_page .getParameters().put('claimslead','test_lead');
              //mycoverlist_page .getParameters().put('gardshare','test_share');
              //mycoverlist_page .getParameters().put('expirydate','2014-08-23 00:00:00');
              //mycoverlist_page .getParameters().put('Onrisk','true');
              //Test.setCurrentPage(mycoverlist_page );
              test_client.SelectedObjectType = new List<string>();
              test_client.SelectedObjectType.add('Barge');
              test_client.SelectedObjectType.add('Container Vessel');
              //test_client.viewObject();
              test_client.ViewData();
              test_client.getCovers();
              test_client.exportToExcel();
              string str = test_client.sortExpression;
              test_client.getSortDirection();
              test_client.setSortDirection(str);
              test_client.getObjectTypeOptions();
              test_client.searchAssets();
              test_client.downloadVcardCover();
              /*Pagereference pge2 = page.MyCoverlist;
              pge2.getparameters().put('id',Gardtestdata.testobj.guid__c);
              test.setcurrentpage(pge2);
              MyCoverListCtrl test_client2= new MyCoverListCtrl();
              test_client2.viewObject();*/
              Test.stopTest();
          }
       }  
    }