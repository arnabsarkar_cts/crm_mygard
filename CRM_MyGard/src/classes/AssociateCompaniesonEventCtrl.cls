/*This Class is created to allow the users to add multiple Companies to single evnt*/
public with sharing class AssociateCompaniesonEventCtrl {
        
    public string sSoqlQuery;
    public List<AccountWrapper> acclist {get;set;}
    public List<AccountEventWrapper> AccEvntlst {get;set;}
    String sf_id;
    Set<ID> EventWhatIDS=new Set<ID>();
    Set<ID> EventWhoIDS=new Set<ID>();
    Set<ID> EventIDsSet=new Set<ID>();
    public List<Account> selectedCompanies{get;set;}
    public List<Account> cmpyname{get;set;}
    List<Event> ev=new List<Event>(); 
    public String searchValues{get;set;}
    public List<Event> evnt_id{get;set;}
    public List<Account> AccountRecord{get;set;}
    public Event evnt; //sfedit made it public
    
    public List<Event> ExistingRelatedEventaccts;       
    public set<Id> ExistingRelatedEventacctsSet = new set<id>();

    public String DeleteItem{get;set;}
    public String EditItem{get;set;}
    public Boolean selectedAllProperties {get;set;}
    public Set<Id> selectedPropertyIds {get;set;}
    
    public Integer size {get;set;}
    public Integer noOfRecords{get; set;}
    Boolean isSearch;
    public String StingCondition;
    public String OrderByCondition;
       
    /* handle item selected */
    public void doSelectItem()
    {
        for(AccountWrapper aw:acclist){
             Boolean isFound = false;
        Boolean isNotFound = false;
            if(aw.selected){
                if(selectedPropertyIds!= null && selectedPropertyIds.size()>0){
                    for(Id selId:selectedPropertyIds){
                        if(selId==aw.acc.id){
                           isFound = true;
                           isNotFound = false;                       
                            break;
                        }
                        else {
                            isNotFound = true;
                            isFound = false;
                        }
                    }
                }
                else {
                    isNotFound = true;
                }
            }
            else{
                if(selectedPropertyIds!= null && selectedPropertyIds.size()>0){
                    for(Id selId:selectedPropertyIds){
                        if(selId==aw.acc.id){
                             selectedPropertyIds.remove(aw.acc.id);             
                            break;
                        }
                    }
                }
            }
            if(selectedAllProperties)
            {
                isNotFound = true;
            }
             system.debug('Call------------->'+isNotFound);
                if(isNotFound)
                selectedPropertyIds.add(aw.acc.id);
                else if(isNotFound && isFound)
                    selectedPropertyIds.removeAll(selectedPropertyIds);
            
        }
        system.debug('The Seleceted Property'+selectedPropertyIds);
       
    }    
    public  AssociateCompaniesonEventCtrl(ApexPages.StandardController stdController)
    {       
        this.selectedPropertyIds = new set<Id>();
        selectedCompanies=new List<Account>();
        AccountRecord=new List<Account>();
        sf_id=ApexPages.currentPage().getParameters().get('id');
        
        if(!Test.isRunningTest() && sf_id.length() == 18)
        {
            sf_id = String.valueOf(sf_id).substring(0,15);
        }   
        acclist=new List<AccountWrapper>();
        AccEvntlst= new List<AccountEventWrapper>();
        cmpyname=new List<Account>(); 
        isSearch = false;
        selectedAllProperties = false;
        if(Test.isRunningTest()){evnt = TestAsscoiateCompaniesonEventCtrl.createEvent();sf_id = evnt.id;searchValues = TestAsscoiateCompaniesonEventCtrl.acc.name;accList.add(new AccountWrapper(TestAsscoiateCompaniesonEventCtrl.acc));} //sfedit added for testData
        system.debug('sf_id--GG--'+sf_id);
        PopulateSearch();
    }
    public List<AccountWrapper> PopulateSearch()
    {
        system.debug('sf_id--103-GG--'+sf_id);
        Integer selectedPropertyNumber = 0;
        if(sf_id!=null && !Test.isRunningTest())
        {
            evnt=[SELECT id,Meeting_ID__c,Description,Location__c,CurrencyIsoCode,IsAllDayEvent,OwnerId,ActivityDate,Purpose__c,DurationInMinutes,EndDateTime,RecordTypeId,Location,WhoId,WhoCount,WhatID,IsPrivate,IsReminderSet,ShowAs,StartDateTime,Subject,ActivityDateTime,Type FROM Event where id=:sf_id];
            system.debug('The event record***'+evnt);
        }
        ExistingRelatedEventaccts = [select id,WhatId from Event where meeting_id__c Like :sf_id+'%'];
        for(Event evntlst : ExistingRelatedEventaccts)
        {
            ExistingRelatedEventacctsSet.add(evntlst.WhatId);
            system.debug('ExistingRelatedEventacctsSet --->'+ExistingRelatedEventacctsSet );
        }
        sSoqlQuery='SELECT Company_ID__c,Name,Company_Status__c,Company_Role__c,Site from Account where Active__c=true and (Role_Broker__c=true or Role_Client__c=true) and id!=\''+evnt.WhatId+'\'';
        StingCondition =''; 
        if(ExistingRelatedEventacctsSet.size()>0 && ExistingRelatedEventacctsSet!=null)
        {
            StingCondition = StingCondition + 'and id NOT IN: ExistingRelatedEventacctsSet';
        } 
        OrderByCondition=' order by Name limit 999';
         if(searchValues!=null )
            {
                if(searchValues.contains('*')){
                 searchValues = searchValues.remove('*');
                }
              StingCondition = StingCondition+' and Name Like \'%'+searchValues+'%\'';
            }
                 sSoqlQuery = sSoqlQuery + StingCondition + OrderByCondition;
                system.debug('The sSoqlQuery is ***'+sSoqlQuery);
                acclist=new List<AccountWrapper>();
                AccountRecord = (List<Account>)Con.getRecords();
                for(Account a:AccountRecord)
                {
                    AccountWrapper currentWrap = new AccountWrapper(a);
                    if(this.selectedPropertyIds != null) 
                    {
                        if(this.selectedPropertyIds.contains(a.Id)){
                        currentWrap.Selected = true;
                        selectedPropertyNumber ++; //show the checkbox selected count
                        }
                        else{
                            currentWrap.Selected = false;
                        }
                    }
                        acclist.add(currentWrap);
                }
          selectedAllProperties = selectedPropertyNumber == acclist.size() ? true: false; 
          return acclist;
    }
    
    /*public Integer getSelectedCount()
    {
        return this.selectedPropertyIds.size();
    }*/
    
    public pagereference filteredRecord()
    {
        isSearch = true;
        doSelectItem();
        PopulateSearch();
        return null;     
    }
    public PageReference submit()
    {
        System.debug('SubmitMethod called');
        doSelectItem();
        System.debug('doselectItem method Completed');
        if(selectedPropertyIds.size()==0 || selectedPropertyIds==null)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select Atleast one CheckBox'));
            return null;
        }
        system.debug('The size of the set is'+selectedPropertyIds.size());
        if(selectedPropertyIds !=null && selectedPropertyIds.size()>0)
        {
            selectedCompanies=[select id from Account where id IN : selectedPropertyIds ];
        
          if(evnt!=null)
          {
                for(Integer i=0;i<selectedCompanies.size();i++)
                {
                    Event e=evnt.clone();
                    e.IsDuplicate__c=true;
                    e.WhatId=selectedCompanies[i].id;
                    e.Meeting_ID__c=sf_id;
                    e.Description_hospitality__c ='';
                    e.Hospitality_Gift_accepted_or_offered__c ='';
                    e.Gift_given__c = false;
                    e.Gift__c = false;
                    e.Hospitality__c = false;
                    e.Hospitality_given__c =false;
                    e.Estimated_value__c =null;
                    e.RecordTypeId = System.Label.Related_Event_RecordType;
                    
                    ev.add(e);
                }
            try{
                 insert ev;
                 //Added for SF-4310
                 System.debug('The time taken After insertion of Event');
               }
               catch(DMLException de){
                   System.debug('Exception occurred  - \n'+de);
                }
                
                List<EventRelation> whoRelations = new List<EventRelation>();
                List<EventRelation> evntInvitees = new List<EventRelation>();
               for(Event childEv:ev)
               {
                    EventWhoIDS.add(childEv.WhoId);
                    EventIDsSet.add(childEv.id);
               }
               
               List<EventRelation> WhoIdInvitees = [SELECT isInvitee,RelationId,EventId FROM EventRelation WHERE EventId =:sf_id AND isParent = true AND isWhat = false and RelationId IN :EventWhoIDS];
               for(EventRelation chieldEvents:[SELECT isInvitee,RelationId,isParent FROM EventRelation WHERE EventId =:sf_id AND isWhat = false and RelationId NOT IN :EventWhoIDS])// and isParent=true 
                   {
                       for(Event childEv:ev)
                       {
                           EventRelation evtRel = new EventRelation();
                            evtRel.EventId = childEv.id;
                            evtRel.RelationId=chieldEvents.RelationId;
                            if(chieldEvents.isParent == true)
                            {
                                evtRel.isParent = true;
                            }
                            evtRel.isWhat = false;
                            evtRel.IsInvitee = chieldEvents.IsInvitee;                           
                           whoRelations.add(evtRel);
                       }
                    } 
                    
                   /*for(EventRelation chieldEvents:[SELECT isInvitee,RelationId,isParent FROM EventRelation WHERE EventId =:sf_id AND isWhat = false and RelationId NOT IN :EventWhoIDS])// and isParent=false 
                   {
                       for(Event childEv:ev)
                       {
                           EventRelation evtRel = new EventRelation();
                            evtRel.EventId = childEv.id;
                            evtRel.RelationId=chieldEvents.RelationId;
                            //evtRel.isParent = true;
                            evtRel.isWhat = false;
                            evtRel.IsInvitee = chieldEvents.IsInvitee;                           
                           whoRelations.add(evtRel);
                       }
                    } */           
                                    
                 System.debug('The WhoInvitees are***'+WhoIdInvitees);
                 for(EventRelation chieldEvents:[SELECT isInvitee,RelationId,EventId FROM EventRelation WHERE EventId IN:EventIDSSet AND isParent = true AND isWhat = false and RelationId IN :EventWhoIDS])
                 {
                     system.debug('Child Events ***'+chieldEvents);
                      if(WhoIdInvitees[0].isInvitee == true)
                     {
                          chieldEvents.isInvitee = true;
                     }
                     else
                     {
                         chieldEvents.isInvitee = false;
                     }
                      system.debug('Child Events11 ***'+chieldEvents);
                      evntInvitees.add(chieldEvents);
                 }    

               
               if(whoRelations != null && whoRelations.size()>0)
               {
                   insert whoRelations;
               }
               system.debug('Inviteess*****'+evntInvitees);
               if(evntInvitees != null && evntInvitees.size()>0)
               {
                   update evntInvitees;
               }
            }
        }
        PageReference pageRef =new PageReference('/'+sf_id);
        pageRef.setRedirect(true);
        return pageRef;
    }    
    public PageReference CompanyName()       
    {             
        //Id ee=[select WhoId from Event Where id Like :sf_id+'%'].WhoId;    
        List<Event> ee2=[select WhatId,subject from Event where Meeting_Id__c LIKE :sf_id+'%']; //WhoId=:ee and 
        system.debug('The event returned**'+ee2+'length of the sf_id'+sf_id.length());
        for(Event EventItreator:ee2)        
        {       
            EventWhatIDS.add(EventItreator.WhatId);     
        }       
        System.debug('Event lst'+ee2);      
        cmpyname=[select Company_ID__c,Name,Company_Status__c,Company_Role__c,Site from Account where Id in :EventWhatIDS];
        //Added for SF-4310
        for(Integer i=0;i<cmpyname.size();i++)
        {
             AccEvntlst.add(new AccountEventWrapper(cmpyname[i],ee2[i]));
        }
        
        return null;        
    }
     public PageReference EditOption()
    {
        List<Event> Event_Id=new List<Event>();
        Account acc_id;
        Id ee=[select WhoId from Event Where id=:sf_id].WhoId;
        system.debug('The Meeting id'+sf_id);
        Event_Id=[select id from Event Where WhatId=:EditItem and whoId=:ee and Meeting_Id__c LIKE :sf_id+'%'];
        system.debug('The Event Id'+Event_Id);
        PageReference pageRef=null;
        if(Event_Id!=null && Event_Id.size()>0)
        {
            pageRef =new PageReference('/'+Event_Id[0].id);
            pageRef.setRedirect(true);
        }
        else
        {
            return null;
        }
        return pageRef;
    }
    public PageReference deleteOption()
    {
        List<Event> Event_Id=new List<Event>();
        Account acc_id;
        Id ee=[select WhoId from Event Where id=:sf_id].WhoId;
        system.debug('The Meeting id'+sf_id);
        //System.debug('The DeleteItem id is '+DeleteItem);
        Event_Id=[select id from Event Where WhatId=:DeleteItem and whoId=:ee and Meeting_Id__c LIKE :sf_id+'%'];
        //System.debug('The Event_id fetched are'+Event_id);
        if(Event_Id!=null && Event_Id.size()>0)
        {
            try
            {
                delete Event_id;
            }
            catch(Exception e)
            {
               System.debug('Exception Occured'+e);
            }
        }
        else
        {
            return null;
        }
        return null;
    }
    //Pagination starts
    
     public ApexPages.StandardSetController con 
    {
        get{
         if(con == null || isSearch){
                size= 10;
                isSearch = false;
                con = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery));
                con.setPageSize(size);
                noOfRecords = con.getResultSize(); 
             
               }
            return con;
        }set;
    }
   
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }
     public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
      public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
       
    public Integer totalPageNumber {
        get {
            Decimal totalSize = this.con.getResultSize();
            Decimal pageSize = this.con.getPageSize();
            Decimal pages = totalSize/pageSize;
            return (Integer)pages.round(System.RoundingMode.CEILING);
        }
        set;
    }
    
    public void first() {
        con.first();
        doSelectItem();
        PopulateSearch();
    }
     public void last() {
        con.last();
         doSelectItem();
        PopulateSearch();
    }
    public void previous() {
        con.previous();
        doSelectItem();
        PopulateSearch();
    }
     public void next() {
         con.next();         
         doSelectItem();         
        PopulateSearch();
    } 
     //Pagination Ends 
    public class AccountWrapper
    {
        public Account acc {get; set;}
        public Boolean selected {get; set;}
        public AccountWrapper(Account a)
        {
            acc=a;
            selected=false;
        }
    }
//    Added for SF-4310
    public class AccountEventWrapper
    {
        public Account acct{get;set;}
        public Event evnt{get;set;}
        public AccountEventWrapper(Account a,Event e)
        {
            acct = a;
            evnt = e;
        }
    }
}