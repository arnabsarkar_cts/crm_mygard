@isTest
private class TestPartnerUserTrigger {
/*
    static testMethod void testContactValuesWithPartnerUsers(){
        test.startTest();
        
        //Create Account
        Account anAccount = new Account(  
            Name = 'Test Account ',
            BillingCity = 'Bristol',
            BillingCountry = 'United Kingdom',
            BillingPostalCode = 'BS1 1AD',
            BillingState = 'Avon' ,
            BillingStreet = '1 Elmgrove Road',
            Company_Role__c = 'Client',
            Area_Manager__c = UserInfo.getUserId()
        );
        insert anAccount;
        
        //Fetch the account
        Account fetchedAccount = [SELECT ID,BillingCity from ACCOUNT WHERE Name = 'Test Account' LIMIT 1];
        System.assertEquals('Bristol',fetchedAccount.BillingCity);
        
        //build a list of record type ids
        List<Id> RecIds = new List<Id>();
        for(RecordType aRecType:ContactToolkit.ContactRecordTypes.values()){
            RecIds.add(aRecType.Id);
        }
        //Create contact
        Contact aContact = new Contact(  FirstName='Test',
                                       LastName = 'Contact ',
                                       RecordTypeId = RecIds[0],
                                       //If the record type is normal use the client type otherwise 
                                       //use the record type name which we expect will be Correspondent
                                       Type__c = ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name == 'Normal' ? 'Client' : ContactToolkit.ContactRecordTypes.get(RecIds[0]).Name,
                                       AccountId = fetchedAccount.Id);
        insert aContact;
        //Fetch the contact 
        Contact fetchedContact = [SELECT ID, FirstName from CONTACT where AccountId =: fetchedAccount.Id LIMIT 1];
        System.assertEquals('Test',fetchedContact.FirstName);
        
        
        //Create User
        Profile p = [SELECT Id FROM Profile WHERE Name='Partner Community Login User Custom'];
        String userNameToBeApplied = String.valueof(DateTime.now().getTime()) + 'testing@testorg.com.mygard';
        User u = new User(Alias = 'standt', 
                          Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', 
                          FirstName='TestFirstName',
                          LastName='Testing', 
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', 
                          ProfileId = p.Id, 
                          IsActive =true,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName=userNameToBeApplied,
                          ContactId = fetchedContact.Id
                         );
        insert u;
        
        test.stopTest();
        
        Contact fetchedContact1 = [SELECT ID, MyGard_user_type__c FROM CONTACT WHERE AccountId =: fetchedAccount.Id LIMIT 1];
        System.assertEquals('Admin', fetchedContact1.MyGard_user_type__c);
                
        update fetchedContact;
        
        fetchedContact1 = [SELECT ID, MyGard_user_type__c FROM CONTACT WHERE AccountId =: fetchedAccount.Id LIMIT 1];
        System.assertEquals('Normal', fetchedContact1.MyGard_user_type__c);
    }
    */
}