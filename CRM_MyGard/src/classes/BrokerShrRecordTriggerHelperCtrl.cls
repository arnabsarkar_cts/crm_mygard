/*
    Name of Company:     CTS
    
    Purpose of Class:    used as a helper controller for BrokerShrRecordTrigger, for every
                         context there is a dedicated method defined in this controller
*/
Public without sharing Class BrokerShrRecordTriggerHelperCtrl
{
    //get called on contract insertion and insert record in broker share object
    public static void AfterInsert(List<Contract> NewLst)
    {
        List<broker_share__c> BROKER_SHR_LST = new List<broker_share__c>();
        broker_share__c brokerShrObj;
        for(Contract obj : NewLst)
        {
                 brokerShrObj = new broker_share__c();
                 brokerShrObj.shared_to_client__c = obj.Shared_With_Client__c;
                 brokerShrObj.contract_external_id__c = obj.Agreement_ID__c;
                 brokerShrObj.Contract_id__c = obj.id;
                 BROKER_SHR_LST.add(brokerShrObj);
        }
        insert BROKER_SHR_LST;
    }
    //get called after update of contract and accepts old and new maps from trigger, update crossponding broker share records
    public static void AfterUpdate(Map<id,Contract> OldMap, Map<id,Contract> NewMap)
    {
        List<broker_share__c> BROKER_SHR_LST = new List<broker_share__c>();
        if(OldMap != null && OldMap.size() >0 && NewMap != null && NewMap.size() >0)
        {
            for(broker_share__c obj: [Select id,shared_to_client__c, Contract_id__c from broker_share__c where Contract_id__c IN: OldMap.keySet()])
            {
                if(NewMap.get(obj.Contract_id__c).Shared_With_Client__c != OldMap.get(obj.Contract_id__c).Shared_With_Client__c)
                {
                    obj.shared_to_client__c = NewMap.get(obj.Contract_id__c).Shared_With_Client__c;
                    BROKER_SHR_LST.add(obj);
                }
            }
            if(BROKER_SHR_LST != null && BROKER_SHR_LST.size() >0)
                update BROKER_SHR_LST;
        }
    }
    //get called on after deletion of contracts accept contract id in a set, deletes crossponding broker share records
    public static void AfterDelete(Set<Id> contractIdSet)
    {
        List<broker_share__c> BROKER_SHR_LST = new List<broker_share__c>();
        BROKER_SHR_LST = [Select id from broker_share__c where Contract_id__c IN: contractIdSet];
        if(BROKER_SHR_LST != null && BROKER_SHR_LST.size() >0)
            delete BROKER_SHR_LST;
    }
    public static void populateAgreementSetGuid(List<agreement_set__c> agreementSetLst)
    {
        for(agreement_set__c obj : agreementSetLst)
        {
            Blob b = Crypto.GenerateAESKey(128);
            String h = EncodingUtil.ConvertTohex(b);
            String guid = h.SubString(8,12)+ '-' + h.SubString(0,8) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20);
            obj.guid__c = guid;
            System.debug('Agreement set guid field *******'+guid);
        }
    }
}