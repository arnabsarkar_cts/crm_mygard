@isTest
private class TestAccountTriggerHelper {
    public static Account clientAcc;
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Market_Area__c Markt;
    public static User salesforceLicUser;
    
    private static void createUsers(){
        salesforceLicUser = new User(
            Alias = 'standt', 
            profileId = salesforceLicenseId ,
            Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8',
            CommunityNickname = 'test13',
            LastName='Testing',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',  
            TimeZoneSidKey='America/Los_Angeles',
            UserName='test008@testorg.com'
        );
        
        insert salesforceLicUser; 
    }
    private static void createAccount(){
        Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt; 
        createUsers();        
        
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrcClient);
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        vrcList.add(vrcBroker);
        Valid_Role_Combination__c vrcClinic = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
        vrcList.add(vrcClinic);
        insert vrcList;
        
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        
        clientAcc = new Account( Name = 'Test_1', 
                                Site = '_www.test_1.se', 
                                Type = 'Client', 
                                ShippingStreet = 'Test Shipping Street',
                                BillingStreet = 'Gatan 1',
                                BillingCity = 'Stockholm', 
                                BillingCountry = 'SWE', 
                                BillingPostalCode = 'BS1 1AD',
                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                Company_Role_Text__c='Client',
                                Market_Area__c = Markt.id,
                                Area_Manager__c = salesforceLicUser.id,
                                OwnerId = salesforceLicUser.id,
                                Sub_Roles__c = ''
                               );
        insert clientAcc;
    }
    
    static testMethod void testApprovedChangesAppliedForId(){
        Test.startTest();
        TestAccountTriggerHelper.createAccount();
        AccountTriggerHelper.approvedChangesAppliedForId(clientAcc.Id);
        Test.stopTest();
    }
    
    static testMethod void testSplitShippingStreetLines(){
        Test.startTest();
        TestAccountTriggerHelper.createAccount();
        AccountTriggerHelper.splitShippingStreetLines(clientAcc);
        Test.stopTest();
    }
    
    static testMethod void testSplitBillingStreetLines(){
        Test.startTest();
        TestAccountTriggerHelper.createAccount();
        AccountTriggerHelper.splitBillingStreetLines(clientAcc);
        Test.stopTest();
    }
    
    static testMethod void testSplitShippingAddressWithSomeOldAccount(){
        Test.startTest();
        TestAccountTriggerHelper.createAccount();
        Account newAccount = clientAcc.clone(true, true, true, true);
        //insert newAccount;
        Map<Id,Account> oldAccounts = new Map<Id, Account>();
        oldAccounts.put(clientAcc.Id, clientAcc);
        List<Account> accounts = new List<Account>();
        accounts.add(newAccount);
        AccountTriggerHelper.splitShippingAddress(accounts,oldAccounts);
        Test.stopTest();
    }
    
    static testMethod void testSplitShippingAddressWithNoOldAccount(){
        Test.startTest();
        TestAccountTriggerHelper.createAccount();
        List<Account> accounts = new List<Account>();
        accounts.add(clientAcc);
        AccountTriggerHelper.splitShippingAddress(accounts,null);
        Test.stopTest();
    }
    
    static testMethod void testSplitBillingAddressWithSomeOldAccount(){
        Test.startTest();
        TestAccountTriggerHelper.createAccount();
        Account newAccount = clientAcc.clone(true, true, true, true);
        //insert newAccount;
        Map<Id,Account> oldAccounts = new Map<Id, Account>();
        oldAccounts.put(clientAcc.Id, clientAcc);
        List<Account> accounts = new List<Account>();
        accounts.add(newAccount);
        AccountTriggerHelper.splitBillingAddress(accounts,oldAccounts);
        Test.stopTest();
    }
    
    static testMethod void testSplitBillingAddressWithNoOldAccount(){
        Test.startTest();
        TestAccountTriggerHelper.createAccount();
        List<Account> accounts = new List<Account>();
        accounts.add(clientAcc);
        AccountTriggerHelper.splitBillingAddress(accounts,null);
        Test.stopTest();
    }
    
    static testMethod void testCompanyRolesChanged(){
        
        Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt; 
        createUsers();
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        
        Account account = new Account( Name='Test2',
                                      BillingCity = 'Bristol',
                                      BillingCountry = 'United Kingdom',
                                      BillingPostalCode = 'BS1 1AD',
                                      BillingState = 'Avon' ,
                                      BillingStreet = '1 Elmgrove Road',
                                      recordTypeId=System.Label.Broker_Contact_Record_Type,
                                      Site = '_www.cts.se',
                                      Client_On_Risk_Flag__c = false,
                                      Broker_On_Risk_Flag__c = false,
                                      Deleted__c = true,
                                      Company_Status__c = 'Inactive',
                                      Company_Role__c = 'Broker',
                                      Sub_Roles_to_Remove__c = 'Broker',
                                      Type = 'Broker' ,
                                      Area_Manager__c = salesforceLicUser.id,      
                                      Market_Area__c = Markt.id,
                                      Sub_Roles__c = 'Broker - Reinsurance Broker'
                                     );
        List<Account> accounts = new List<Account>();
        accounts.add(account);
        AccountTriggerHelper.companyRolesChanged(accounts);
    }
    
    static testMethod void testCompanyRolesChangedWithBothOldAndNewCompany(){
        Test.startTest();
        TestAccountTriggerHelper.createAccount();
        /*
Account account = new Account( Name='Test2',
BillingCity = 'Bristol',
BillingCountry = 'United Kingdom',
BillingPostalCode = 'BS1 1AD',
BillingState = 'Avon' ,
BillingStreet = '1 Elmgrove Road',
recordTypeId=System.Label.Broker_Contact_Record_Type,
Site = '_www.cts.se',
Client_On_Risk_Flag__c = false,
Broker_On_Risk_Flag__c = false,
Deleted__c = true,
Company_Role__c = 'Broker',
Sub_Roles__c ='Broker',
Sub_Roles_to_Remove__c = 'Broker',
Type = 'Broker' ,
Area_Manager__c = salesforceLicUser.id,      
Market_Area__c = Markt.id                                                                 
);
insert account;
*/
        Account account = clientAcc.clone(true,true,true,true);
        map<ID, account> newAccounts = new map<ID,account>();
        map<ID,account> oldAccounts = new map<ID,account>();
        
        newAccounts.put(clientAcc.Id, clientAcc);
        oldAccounts.put(account.Id, account);
        
        AccountTriggerHelper.companyRolesChanged(newAccounts,oldAccounts);
        Test.stopTest();
    }
    
    static testMethod void testCRSettings(){
        CompanyRoleSettings__c aCompanyRoleSettings = AccountTriggerHelper.CRSettings;
    }
}