@isTest(seeAllData=false)
Public class TestUserManagement1
{
    Static testMethod void TestUserManagement()
    {
        UserRole ur = [Select PortalType From UserRole limit 1];
        String sfdcProfId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        string partnerLicenseId = [SELECT Id FROM profile WHERE Name= 'Partner Community Login User Custom' limit 1].id;
        User user;
        CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c ='Test123@gmail.com');
        insert cvar;
        //added by ss
        String typeBrStr =  'Broker';
        String typeSubBrStr = 'Broker - Reinsurance Broker'; //sfedit
         //Valid Role Combination----
            
            List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
            Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
            vrcList.add(vrcClient);
            Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
            vrcList.add(vrcBroker);
            insert vrcList;
        AccountToContactMap__c map_broker;
        Market_Area__c Markt = new Market_Area__c(
                                                    Market_Area_Code__c = 'test1ab120'
                                                 );
        insert Markt ;
        AdminUsers__c setting = new AdminUsers__c();
            setting.Name = 'Number of users';
            setting.Value__c = 3;
            insert setting;
        Gard_Team__c gt = new Gard_Team__c();
        gt.Active__c = true;
        gt.Office__c = 'test';
        gt.Name = 'GTeam';
        gt.Region_code__c = 'TEST';
        insert gt;
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Gard_Team__c=gt.Id,Claims_Support_Team__c=gt.Id);
        insert country;
        User salesforceLicUser = new User(
                                            Alias = 'standt', 
                                            profileId = sfdcProfId ,
                                            Email='standarduser@testorg.com',
                                            EmailEncodingKey='UTF-8',
                                            CommunityNickname = 'test13',
                                            LastName='Testing',
                                            LanguageLocaleKey='en_US',
                                            LocaleSidKey='en_US',  
                                            TimeZoneSidKey='America/Los_Angeles',
                                            UserName='yoo008@testorgo.com',
                                             City= 'Arendal'
                                           );
        insert salesforceLicUser ;
        system.assertequals(salesforceLicUser.UserName,'yoo008@testorgo.com','User inserted successfully');
        
        Account dummyAcc = new Account(  Name='testree',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    Site = '_www.cts.se',
                                    Type = 'Broker', 
                                    Market_Area__c = Markt.id,
                                    Confirm_not_on_sanction_lists__c = true,
                                    License_description__c  = 'some desc',
                                    Description = 'some desc',
                                    Licensed__c = 'Pending',
                                    Area_Manager__c = salesforceLicUser.id,
                                    company_Role__c =  typeBrStr ,
                                    Sub_Roles__c = typeSubBrStr,
                                    Country__c = country.id
                                 );
        insert dummyAcc ; 
        system.assertequals(dummyAcc.BillingCity,'Bristol','Dummy Account inserted successfully');
        
        
        Contact dummyContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             OtherPhone = '46344674',
                                             mobilephone= '90007665',
                                             phone = '4567896',
                                             AccountId = dummyAcc.Id,
                                             primary_contact__c=true,
                                             Email = 'jaredleto@gmail.com'
                                           );
                                         
        insert dummyContact; 
        system.assertequals(dummyContact.FirstName,'Raan','Dummy Contact inserted successfully');
        
        Account account = new Account(Name = 'Testt',
                                  Site = '_www.test.se',
                                  Type = 'Broker',
                                  BillingStreet = 'G,a,t,a,u,n,1',
                                  BillingCity = 'Stockholm',
                                  BillingCountry = 'SWE',
                                  ShippingStreet = 'j,k,l,m,n,o,',
                                  ShippingState = 'def',
                                  ShippingCountry = 'ghi',
                                  recordTypeId=System.Label.Broker_Contact_Record_Type,
                                  Phone_1__c = '7845125623',
                                  Market_Area__c = Markt.id,
                                  Confirm_not_on_sanction_lists__c = true,
                                  License_description__c  = 'some desc',
                                  Description = 'some desc',
                                  Licensed__c = 'Pending',
                                   company_Role__c =  typeBrStr ,
                                    Sub_Roles__c = typeSubBrStr,
                                    Country__c = country.id,
                                  Area_Manager__c = salesforceLicUser.id);
        Insert account;
        Account account_1 = new Account(Name = 'Testt_1',
                                  Site = '_www.test.se',
                                  Type = 'Broker',
                                  BillingStreet = 'G,a,t,a,u,n,1',
                                  BillingCity = 'Stockholm',
                                  BillingCountry = 'SWE',
                                  ShippingStreet = 'j,k,l,m,n,o,',
                                  ShippingState = 'def',
                                  ShippingCountry = 'ghi',
                                  recordTypeId=System.Label.Client_Contact_Record_Type,
                                  Phone_1__c = '784512562354',
                                  Market_Area__c = Markt.Id,
                                  Confirm_not_on_sanction_lists__c = true,
                                  License_description__c  = 'some desc',
                                  Description = 'some desc',
                                  Licensed__c = 'Pending',
                                  company_Role__c =  typeBrStr ,
                                    Sub_Roles__c = typeSubBrStr,
                                  Area_Manager__c = salesforceLicUser.id,
                                   Country__c = country.id);
        Insert account_1;
        system.assertequals(account_1.Name,'Testt_1','Account inserted successfully');
        system.assertequals(account.Name,'Testt','Account inserted successfully');
        
        
        Contact contact1;
        
        contact1 = new Contact(Accountid=account.id,
                              FirstName='sat_123',
                              LastName='haq',
                              MailingCity = 'London',
                              MailingCountry = 'United Kingdom',
                              MailingPostalCode = 'SE1 1AE',
                              MailingState = 'London',
                              MailingStreet = '4 London Road',
                              Email = 'tes321@gmail.com',
                              //name = dummyContact.id, 
                              //name= 'testuser', 
                              OtherPhone = '46345674',
                             //MobliePhone = 90007665,
                               phone = '4567896',
                               primary_contact__c=true,
                               No_Longer_Employed_by_Company__c=false,
                               MobilePhone = '123458',
                               Synchronisation_Status__c='Synchronised',
                               MyGard_user_type__c = 'Admin'
                               );
                               
        insert contact1 ;
        
        user = new User(
                        Alias = 'standard', 
                        profileId = partnerLicenseId,
                        Email='standarduser_675@testorg.com',
                        EmailEncodingKey='UTF-8',
                        CommunityNickname = 'test3',
                       // UserRole = ur[0],
                        LastName='Testing',
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US',  
                        TimeZoneSidKey='America/Los_Angeles',
                        UserName='testy@testorg.com.mygard',
                        IsActive = true,
                        Contactid=contact1.id
                        );
        insert user;
        system.assertequals(user.UserName,'testy@testorg.com.mygard','User inserted successfully');
        
         map_broker = new AccountToContactMap__c(AccountId__c=Account.id,Name=contact1.id,RolePreference__c='Broker');//RolePreference__c='Test'
        // map_client = new AccountToContactMap__c(AccountId__c= clientAcc.id,Name=clientContact.id);
         
         Insert map_broker;
        // Insert map_client; 
        
        Account_Contact_Mapping__c contact_map = new Account_Contact_Mapping__c( contact__c = contact1.id, account__c = account.id,
                                                                                 IsPeopleClaimUser__c = true, PI_Access__c = true,
                                                                                 marine_Access__c = true   );
        insert contact_map;
        
       Contract contract = New Contract(Accountid = account.id,
                                         Account = account,
                                         Status = 'Draft',
                                         Broker__c = account.id,
                                         Client__c = account_1.id,
                                         Accounting_Contacts__c = user.id,
                                         Contract_Reviewer__c = user.id,
                                         Contracting_Party__c = account.id,
                                         Expiration_Date__c = Date.valueof('2016-12-12'), 
                                         Inception_date__c = Date.valueof('2015-12-12'), 
                                         Agreement_ID__c = 'jhbjbajbj', 
                                         Agreement_Type_Code__c = 'kljnskhn',
                                         Shared_With_Client__c = true
                                        );        
        Insert contract;
        
        object__c testobj = new object__c(name = 'vesselka',
                                          Object_Sub_Type__c='Accommodation Ship',
                                         No_of_passenger__c=1000,
                                         No_of_crew__c=50,
                                         Gross_tonnage__c=1000,
                                         Length__c=20,
                                         Width__c=20,
                                         Object_Unique_ID__c = '9a42273d-4ba4-7e02-2527',
                                         Depth__c=30  );
        insert testobj;  
        
        Case test_Case = new Case(Accountid = account_1.id,
                                    ContactId = contact1.id,
                                    Origin = 'Community',
                                    Last_Name__c = 'meha',
                                    //Sub_Claim_Handler__c = brokerContact.id,
                                    //Registered_By__c = brokerContact.id, 
                                    Object__c = testobj.id,
                                    Status='Open',
                                    //Version_Key__c = 'kolka2342',
                                    Claim_Incurred_USD__c= 500,
                                    Claim_Reference_Number__c='kol23212',
                                    Contract_for_Review__c =  contract.id,
                                    Reserve__c = 350,
                                    Paid__c =50000,
                                    Total__c= 750,
                                    Description = 'abc',
                                     // Voyage_To__c = 'Voyage_To',
                                    Member_reference__c = 'ash1232',
                                   // Voyage_From__c = 'Voyage_From',
                                    Claim_Type__c = 'Cargo',
                                    Type = 'Add new user',
                                    Submitter_company__c = account_1.id
                                    //Risk_Coverage__c = asset_1st_broker.id                                                  
                              );
         insert test_Case;
                            
        /*Gard_Administrators__c gard_admin=new Gard_Administrators__c(Name='test1234',
                                                             name__c='testname',
                                                             email_address__c='aritram1@gmail.com');
        insert gard_admin;
        system.assertequals(gard_admin.name__c,'testname',true);*/
        
        Agreement_set__c agrSet = new Agreement_set__c();
         agrSet.agreement_set_type__c = 'Test data for broker share';
         agrSet.shared_by__c = account.ID;
         agrSet.shared_with__c = account_1.ID;
         agrSet.status__c = 'Active';
         insert agrSet;
         
         Broker_share__c brokerShr = new Broker_share__c();
         brokerShr.Active__c = false;
         brokerShr.Agreement_set_id__c = agrSet.ID;
         brokerShr.contract_external_id__c = String.valueOf(contract.ID);
         brokerShr.Contract_id__c = contract.ID;
         brokerShr.shared_to_client__c = true;
         insert brokerShr;
        /*Group testGroup = new Group(Name='MyGard_Gard_Administrator', Type='Queue');
        insert testGroup;*/
        System.runAs(user)
       {
            UserManagementController UserManagement=new UserManagementController();
            UserManagement.noPrimaryMsg = 'abc';
            UserManagement.emailAction = 'abc';
            //UserManagement.isClientContact = true;
            //QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            //insert testQueue;
           // UserManagement.loggedInAccountId = account.id; 
            test.starttest();
          // UserManagement.loggedInAccountId = account.id;  
            UserManagement.contractId=contract.id;
            UserManagement.contactId=contact1.id;
            UserManagement.notInContactList = true;
           UserManagement.sharedClientId = account_1.id;
            UserManagement.slectedContactforAddNewUser =contact1.id;
            //UserManagement.whichSectn ='GranteAccessToPeopleClaimsAck';
            String notifMailId='abc';
            UserManagement.whichSectn ='RevokeAccessToPeopleClaimsAck';
            //UserManagement.sendMails();
            UserManagement.closeNewUserPopUp();
            UserManagement.showPopup();
            UserManagement.closePopup();
            UserManagement.displaySharedClientData();
            UserManagement.jointSortingParam = 'ASC##account.name';
            UserManagement.displaySharedClientData();
            UserManagement.ChkAccessString = 'isPeople';
            UserManagement.togglePeopleLineUser();
            UserManagement.sendMails();
            UserManagement.contactId=contact1.id; //contact1.firstname+' '+contact1.lastname;
            UserManagement.removeAccess();
            UserManagement.startStopSharingAccounts();
            PageReference pageRef = page.UserManagement;
            Test.setCurrentPage(pageRef);
            UserManagement.makeAdmin();
            UserManagement.contactId=contact1.id;
            UserManagement.makePrimary();
           // UserManagement.createNewGardUser();
            //UserManagement.createRole();
            Id contid= contact1.id; 
            id accid = account.id; 
            UserManagementController.deactivateUsr(contid, accid);
            UserManagementController.wrapperClient wrpprCl = new UserManagementController.wrapperClient();
            
            test.stoptest();
            
       }
    }
}