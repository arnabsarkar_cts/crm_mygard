/*
*    Name of Company:     CTS    
*    Purpose of Class:   This controller is used to PEME Reports tab.
    
**    Modification Log
*    --------------------------------------------------------------------------------------------------------
*    Developer                 |               Date             |           Description
*    --------------------------------------------------------------------------------------------------------
*    Arpan Muhuri              |            22/01/2018          |           SF-4079
*/
public class PEMEReportsAnnualCtrl {

    public String header, footer, signature, header_ack ,link;
    private static final Integer SMARTQUERY_PAGE_SIZE   = 10;    
    public list<PEME_Report__c> lstReportsAnnual {get;set;}
    public list<PEME_Report__c> lstReportsMonthly {get;set;}
    public String selectedClinic {get;set;}   
    public set<String> lstYearOptions {get;set;} 
    public list<String> selectedYear {get;set;}
    public PEME_Report__c newReport {get;set;}
    public Attachment attachment{get; set;} 
    public String userName{get;set;}
    public String newReportType {get;set;}
    public String newMonth {get;set;}
    public String newYear {get;set;}
    public String reportName {get;set;}
    public String reportVersion {get;set;}
    public list<PEME_Report__c> lstReportPrint {get;set;}
    public string loggedInAccountName;
    //pagination variables
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public String sSoqlQuery;
    private String nameOfMethod;
    Public Integer intStartrecord { get ; set ;}
    Public Integer intEndrecord { get ; set ;}
    public Integer pageNum{get;set;}//{pageNum = 1;}
    public String jointSortingParam { get;set; }
  //  private String sortDirection = Ascending;
  //  private String sortExp = 'Report_Name__c';
    public Integer noOfData{get;set;}
    String strStatus;
    List<User> lstUser; 
    //pagination variables
    public String orderBy{get;set;}//{orderBy = '';}
    private String sortDirection = 'ASC';
    private String sortExp = ''; 
    public string loggedInContactId;
    public List<Contact> lstContact{get;set;} 
    private static String Ascending='ASC';     
    private final String USER_TYPE;
    private final String strBroker = 'Broker';
    private final String strClient = 'Client';
    private final String strClinic = 'Clinic';
    public String orderByNew
    {
     get
         {
            return sortExp;
         }
         set
         {
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExp)
             sortDirection = (sortDirection.contains('DESC')? Ascending : 'DESC');
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     }
     public String getSortDirection()
     {
        //if not column is selected 
        if (orderByNew == null || orderByNew == '')
          return Ascending;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value)
     {  
       sortDirection = value;
     }
    
    public ApexPages.StandardSetController submissionSetConOpen{
    get{ 
           return submissionSetConOpen;         
    }
    set; }
    
    public List<SelectOption> getReportTypeOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',''));        
        Schema.DescribeFieldResult fieldResult = PEME_Report__c.Report_Type__c.getDescribe();           
        for(Schema.PicklistEntry f : fieldResult.getPicklistValues()){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }         
        return options; 
    }
    
    public List<SelectOption> getMonthOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',''));        
        Schema.DescribeFieldResult fieldResult = PEME_Report__c.Month__c.getDescribe();         
        for(Schema.PicklistEntry f : fieldResult.getPicklistValues()){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }         
        return options; 
    }
    
    public List<SelectOption> getYearOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',''));        
        Schema.DescribeFieldResult fieldResult = PEME_Report__c.Year__c.getDescribe();          
        for(Schema.PicklistEntry f : fieldResult.getPicklistValues()){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }         
        return options; 
    }
    
    public List<SelectOption> getYearFilterOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstYearOptions!= null && lstYearOptions.size()>0){
            for(String strOp:lstYearOptions){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();            
        }
        return Options;
    }
    
    public String sortExpression{
         get{
            return sortExp;
         }
         set{          
           if (value == sortExp)
             sortDirection = (sortDirection == Ascending)? 'DESC' : Ascending;
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     }
    
  /*   public String getSortDirection(){        
        if (sortExpression == null || sortExpression == '')
          return Ascending;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value){  
       sortDirection = value;
     }
     */
    public PEMEReportsAnnualCtrl(){ 
        
        pageNum = 1;
        orderBy = '';
        MyGardHelperCtrl.CreateCommonData();
        USER_TYPE = MyGardHelperCtrl.USER_TYPE;
        loggedInAccountName = MyGardHelperCtrl.ACCOUNT_NAME;
        lstUser = new List<User>([select firstName,lastName,ContactId,Contact.Name,Contact.Account.Name,Contact.AccountId from User where id=:UserInfo.getUserId()]);
        if(lstUser!=null && lstUser.size()>0 && lstUser[0].Contactid!=null)
        {
            loggedInContactId = lstUser[0].Contactid;
            lstContact = new List<Contact>([select name, account.id,account.name,accountid, account.recordtypeid,account.company_Role__c, /*IsPeopleLineUser__c,*/account.BillingPostalcode__c,account.BillingCountry__c,account.BillingState__c,account.Billing_Street_Address_Line_1__c,account.Billing_Street_Address_Line_2__c,account.Billing_Street_Address_Line_3__c,account.Billing_Street_Address_Line_4__c 
                                          from Contact 
                                          where id=:loggedInContactId]);           
            if(lstContact!=null && lstContact.size()>0 && lstContact[0].accountId!=null)
            {
                //if(lstContact[0].account.company_Role__c.contains('External Service Provider')) //Added for MYG-2948
                if(USER_TYPE.containsIgnoreCase(strClinic)) //Added for MYG-2948 //SF-4516
                {
                    lstReportsAnnual = new list<PEME_Report__c>(); 
                    lstReportsMonthly = new list<PEME_Report__c>();       
                    lstYearOptions = new set<String>();       
                    selectedYear = new list<String>();
                    newReport = new PEME_Report__c();
                    attachment = new Attachment();
                    orderByNew ='Report_Name__c';
                  //  lstUser = new List<User>([select firstName,lastName,ContactId,Contact.Name,Contact.Account.Name,Contact.AccountId from User where id=:UserInfo.getUserId()]);
                    userName = lstUser[0].Contact.Name;
                    selectedClinic = lstUser[0].Contact.AccountId;
                    system.debug('selectedClinic: '+selectedClinic);
                    if(selectedClinic!=null && selectedClinic!=''){      
                        generateReports();    
                    }
                    header_ack = System.Label.header_Acknowledgement_mail;
                    header = System.Label.Header;
                    signature = System.Label.Signature;
                    footer = System.Label.Footer;
                    link = System.Label.InternalOrgURL;
                }
            }
        }
                 
    }
       
    public void generateReports(){
                
        pageNum = 1;
        lstReportsAnnual= new List<PEME_Report__c>();
        
      /*  if (jointSortingParam != null){
            system.debug('******jointSortingParam**************'+jointSortingParam);
            String[] arrStr = jointSortingParam .split('##');
            sortDirection = arrStr[0];
            sortExp = arrStr[1];            
        }
        */
        String strCondition ='';
                
        if(selectedYear!= null && selectedYear.size()>0){
            strCondition = ' AND Year__c IN:selectedYear';
        }
        
        string sortFullExp = sortExpression  + ' ' + sortDirection;
        
        strStatus = 'Annual';       
        sSoqlQuery = 'SELECT Clinic__c,CreatedDate,Id,Month__c,Report_Name__c,Report_Type__c,Version__c,Year__c FROM PEME_Report__c where'+
                     ' Report_Type__c=:strStatus AND Clinic__c=:selectedClinic'+strCondition;
        
      //  sSoqlQuery = sSoqlQuery+strCondition+' ORDER BY '+sortFullExp+' ';
        orderBy = ' ORDER BY ' + orderByNew + ' ' + sortDirection; //For new sorting          
        sSoqlQuery = sSoqlQuery+ ' ' + orderBy;
        submissionSetConOpen = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery + ' limit 10000')); 
        submissionSetConOpen.setPageSize(10);
        createOpenUserList();       
        // searchAssets();
        
        for(PEME_Report__c objReports:[SELECT Clinic__c,CreatedDate,Id,Month__c,Report_Name__c,Report_Type__c,Version__c,Year__c FROM PEME_Report__c where 
        Report_Type__c='Annual' AND Clinic__c=:selectedClinic order by Report_Name__c asc]){                           
            lstYearOptions.add(objReports.Year__c);                              
        }    
    }
    Public void createOpenUserList()
    {
        if(submissionSetConOpen!=null)
        {
            lstReportsAnnual.clear(); 
            for(PEME_Report__c inv : (List<PEME_Report__c>)submissionSetConOpen.getRecords())             
                lstReportsAnnual.add(inv);
                System.debug('********'+lstReportsAnnual.size());         
        } 
    }
    //Returns to the first page of records
    public void firstOpen() {
        submissionSetConOpen.first(); 
        pageNum = submissionSetConOpen.getPageNumber();
        
        createOpenUserList();
        
    }
    public Boolean hasPreviousOpen   
    {   
        get   
        {   
            return submissionSetConOpen.getHasPrevious();   
        }   
        set;   
    } 
     //Returns the previous page of records   
    public void previousOpen()   
    {   
        submissionSetConOpen.previous();
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();  
    } 
    //to go to a specific page number
    public void setpageNumberOpen()
    {
        submissionSetConOpen.setpageNumber(pageNum); 
        createOpenUserList();
    }
    public Boolean hasNextOpen   
    {   
        get   
        {   
            return submissionSetConOpen.getHasNext();   
        }   
        set;   
    } 
    //Returns the next page of records   
    public void nextOpen()   
    {   
        submissionSetConOpen.next(); 
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();
        
    }
     //Returns to the last page of records
    public void lastOpen(){
        submissionSetConOpen.last(); 
        pageNum = submissionSetConOpen.getPageNumber();
        
        createOpenUserList();
        
    }
 /*   public void searchAssets(){
        if(sSoqlQuery.length()>0){            
            System.debug('Soql Query--->'+sSoqlQuery);  
            setCon = null;            
            
            lstReportsAnnual=(List<PEME_Report__c>)setCon.getRecords();
            system.debug('****setCon.getResultSize()**********'+setCon.getResultSize());
            if(setCon.getPageNumber() == 1)
                intStartrecord = 1;
            else
                intStartrecord  = ((setCon.getPageNumber() - 1) * SMARTQUERY_PAGE_SIZE) + 1;
          
            intEndrecord = setCon.getPageNumber() * SMARTQUERY_PAGE_SIZE;
            if( intEndrecord > noOfRecords ){
                intEndrecord = noOfRecords ;
            }
        }
                                                              
    }
        
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                size = SMARTQUERY_PAGE_SIZE;
                System.debug('strStatus--->'+strStatus);
                System.debug('selectedClinic--->'+selectedClinic);
                System.debug('Soql Query--->'+sSoqlQuery);
                if(sSoqlQuery!=null && sSoqlQuery!=''){             
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery+' LIMIT 10000'));
                    setCon.setPageSize(size);
                    noOfRecords = setCon.getResultSize();
                    system.debug('****Total records****'+noOfRecords);
                    if (noOfRecords == null || noOfRecords == 0){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No search results found.'));
                    }else if (noOfRecords == 10000){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The search returned 10000 records (maximum allowable limit).'));
                    }
                }                
            }
            return setCon;
        }set;
    }  
    
    public void setpageNumber(){
        if(setCon!=null){
            setCon.setpageNumber(pageNum);
            searchAssets();
        }
    }
        
    public Boolean hasNext {
        get {
            if(setCon!=null)
            return setCon.getHasNext();
            else return false;
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            if(setCon!=null)
            return setCon.getHasPrevious();
            else return false;
        }
        set;
    }
    
    public Integer pageNumber {
        get {
            if(setCon!=null)
            return setCon.getPageNumber();
            else return 0;
        }
        set;
    }
       
    public void navigate(){
        if(nameOfMethod=='previous'){
             setCon.previous();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }else if(nameOfMethod=='last'){
             setCon.last();
             searchAssets();
        }else if(nameOfMethod=='next'){
             setCon.next();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }else if(nameOfMethod=='first'){
             setCon.first();
             searchAssets();
        }
    }
        
    public void first() {
        nameOfMethod='first';
        navigate();
    }
    
    public void next(){
        nameOfMethod='next';
        pageNum = setCon.getPageNumber();
        navigate();        
    }
   
    public void last(){
        nameOfMethod='last';
        navigate();
    }
  
    public void previous(){
        nameOfMethod='previous';
         pageNum = setCon.getPageNumber();
        navigate();
    } */ 
    
    public pageReference clearOptions(){       
        selectedYear.clear();
        generateReports();
        return null;
    }
    
    public PageReference createNewReport(){
        newReport.Clinic__c = selectedClinic;
        newReport.Report_Type__c = newReportType;
        if(newReportType == 'Quarterly')
        {
            newReport.Month__c = newMonth;
        }
        else
        {
            newReport.Month__c = null;
        }
        newReport.Year__c = newYear;
        newReport.Report_Name__c = reportName;
        newReport.Version__c = reportVersion;
        insert newReport;
        List<Id> id_attachment;
        if(attachment != null){
            attachment.ParentId = newReport.Id;
            //attachment.Body = attachmentBody;
            //attachment.Name = attachmentName;
            //if(!Test.isRunningTest())
                //insert attachment;
            id_attachment = new List<Id>();    //sf-4842
            id_attachment = MyGardHelperCtrl.insertContentFromAttachment(attachment);    //sf-4842
        }
        sendNotificationEmail();
        newReport = new PEME_Report__c();
        attachment = new Attachment();
        newReportType = null;
        newMonth = null;
        newYear = null;
        reportName = null;
        reportVersion = null;
        return null;
    }
    
    public void sendNotificationEmail(){
    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        Messaging.SingleEmailMessage[] massEmailMessage = new Messaging.SingleEmailMessage[]{};
        //added for SF-265
        List<User> lstToEmail = new List<User>();
        lstToEmail.addAll(PEMEReportsCtrl.fetchQueueMembers('Gard PEME Invoice Approval Queue'));
        List<String> lstToSendEmail = new List<String>();
        if(lstToEmail.size()>0)
        {
            for(User usr:lstToEmail)
            {
                lstToSendEmail.add(usr.Email);
            }
        }
        lstToSendEmail.add([SELECT Email FROM Group WHERE Name = 'Gard PEME Invoice Approval Queue' limit 1].Email);
        system.debug('**lstToSendEmail** : '+lstToSendEmail);
        //end of SF-265
        
        //String[] toAddress= new String[]{};
        String[] toAddress= new String[2];
      //  User gardHK = [Select name,email,Username from User where Username='gardhk@gard.com' LIMIT 1];
      //  toAddress[0]=gardHK.email;
        //toAddress[1]='amlan.cts@gmail.com';
        EmailTemplate et;
        if(newReport.Report_Type__c == 'Quarterly'){
            et=[Select id,Subject,Body,HtmlValue from EmailTemplate where DeveloperName=:'PEMEReportMonthly'];
        }else{
            et=[Select id,Subject,Body,HtmlValue from EmailTemplate where DeveloperName=:'PEMEReportAnnual'];
        }
        String subject = et.Subject + ' by '+loggedInAccountName;    //SF-4079
        String plainBody = et.Body;
        plainBody= plainBody.replace('[HEADER]',header_ack);
        plainBody= plainBody.replace('[SIGNATURE]',signature);
        plainBody= plainBody.replace('[FOOTER]',footer);
        plainBody=plainBody.replace('[Submitter]',lstUser[0].Contact.Name); 
        plainBody=plainBody.replace('[Submitter company]',lstUser[0].Contact.Account.Name);
        plainBody=plainBody.replace('[reportType]',newReportType);
        plainBody=plainBody.replace('[year]',newYear);
        plainBody=plainBody.replace('[reportName]',reportName);
        plainBody= plainBody.replace('[LINK]',link + newReport.id);
        if(reportVersion!=null)
        {
            plainBody=plainBody.replace('[version]',reportVersion);
        }
        else
        {
            plainBody=plainBody.replace('[version]',reportVersion);
        }
        if(newReport.Report_Type__c == 'Quarterly')
        plainBody=plainBody.replace('[month]',newMonth); 
        
        mail.setUseSignature(false);
        mail.setToAddresses(lstToSendEmail); 
        mail.setSubject(subject);       
        mail.saveAsActivity = false;  // MYGP-144
        //Attach mail to loggedin contact and related object               
        MyGardHelperCtrl.attachMailToObject(loggedInContactId, newReport.id, plainBody, subject);
        mail.setPlainTextBody(plainBody);
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'no-reply@gard.no'];
        if ( owea.size() > 0 ) 
            {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }   
        CommonVariables__c cvar = CommonVariables__c.getInstance('ReplyToMailId');
        mail.setReplyTo(cvar.value__c); 
        massEmailMessage.addAll(new Messaging.SingleEmailMessage[] {mail});
       
        try{
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(Exception ex){
                system.debug(ex);
             } 
             
        /* if(massEmailMessage.size() > 0){
            try{
                Messaging.SendEmailResult [] r = Messaging.sendEmail(massEmailMessage);
             }catch(Exception ex){
                system.debug(ex);
             } 
        } */     
    }
    
    public PageReference cancelReport(){
       // newReport = new PEME_Report__c();
      //  attachment = new Attachment();
        attachment = null;
        newReportType = null;
        newMonth = null;
        newYear = null;
        reportName = null;
        reportVersion = null;
        return null;
    }
    public PageReference print(){
        lstReportPrint = new List<PEME_Report__c>();
        Integer count = 1;
        noOfData = 0;
        for(PEME_Report__c mcl : Database.Query(sSoqlQuery+' LIMIT 10000')){               
            if(count<1000){
                lstReportPrint.add(mcl);                    
                count++;
            }else{                   
                count = 1;
            }   
            noOfData++;                 
        }
        return page.PrintForAnnualReports;
    }
        
     public PageReference exportToExcel(){
        lstReportPrint= new List<PEME_Report__c>();
        Integer count = 1;
        noOfData = 0;
        for(PEME_Report__c mcl : Database.Query(sSoqlQuery+' LIMIT 10000')){               
            if(count<1000){
                lstReportPrint.add(mcl);                    
                count++;
            }else{                   
                count = 1;
            }   
            noOfData++;                 
        }
        return page.GenerateExcelForAnnualReports;
    }
}