@isTest(seeAllData=false)
Public class TestUWRForEntryFormforCharterClientsCtrl
{
    public static GardTestData gtdInstance;
    public static Blob b;
    public static Attachment att,att1,att2;
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    
    private static void createTestData(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
                
        gtdInstance = new GardTestData();
        gtdInstance.commonrecord();
        gtdInstance.customsettings_rec();
        
        b = Blob.valueOf('Brokerssjk jabdkjasbdjk kasdjkasdjksha askbndjkasdjk asjkdhasjkdhdhasjkdhasjk'
                              +'dhjasdhjkashdjkashdjhdjhasjhdkasjhdjkashdjkhdahasjkhasjkhdjkasdhjkashdjkashdjk Data');  
        
        att = new Attachment();
        att.Name = '.pdf';  
        att.Body = b; 
                
        att1 = new Attachment();
        att1.Name = '.doc';  
        att1.Body = b; 
        
        att2 = new Attachment();
        att2.Name = '.ppt';  
        att2.Body = b; 
    }
    
    //Test Methods
    @isTest public static void testWithBroker(){
        createTestData();
        Test.startTest();
        System.runAs(GardTestData.brokerUser){
            UWRForEntryFormforCharterClientsCtrl test_broker=new UWRForEntryFormforCharterClientsCtrl();
            test_broker.renewalOther = true;
            test_broker.selectedCountryCode = 'IN - India';
            list<selectoption> country_code = test_broker.getCountrycodes();
            UWRForEntryFormforCharterClientsCtrl.InnerClass inn=new UWRForEntryFormforCharterClientsCtrl.InnerClass();
            test_broker.attachmentList=new List<Attachment>();
            test_broker.lstCon=new List<UWForm_Contact__c>();
            test_broker.selectedClient = GardTestData.clientAcc.id;
            test_broker.MailId=GardTestData.brokerCase.id;
            test_broker.getMemberDetails();
            test_broker.chboxaction();
            test_broker.getNames();
            test_broker.getPhones();
            test_broker.getEmails();
            test_broker.selectedGlobalClient = new list<string>();
            test_broker.selectedGlobalClient.add(GardTestData.clientAcc.id);
            List<SelectOption> clientOptions =  test_broker.getClientLst();
            //test_broker.getCName();
            test_broker.populateAddress();
            test_broker.addTextBox(); 
            test_broker.rowIndex=0;
            test_broker.removeTextBox();
            test_broker.clearRecord();
            test_broker.uwf.X3_instalments__c=true;
            test_broker.uwf.X1_instalment__c=true;
            test_broker.goNext();
            test_broker.sendToForm();
            test_broker.cancelRecord();
            test_broker.goPrev();
            test_broker.chkAccess();
            test_broker.sendAckMail();
            test_broker.policyPeriodTo = '05.02.2015';
            test_broker.policyPeriodFrom = '07.03.2015';
            test_broker.renewalDateOther = '07.05.2015' ;
            test_broker.primaryAssuredName = 'testname';
            test_broker.dateOutputForReview='05.02.2015';
            //test_broker.saveSubmit();
            //test_broker.sendNotiMail();
            test_broker.dateOutputForReview='05.02.2015';
            //test_broker.saveRecord();
            test_broker.uwf.id=GardTestData.brokerCase.id; 
            test_broker.lstClaim= new List<UWForm_Contact__c>();
            test_broker.lstClaim.add(GardTestData.uwfc);
            test_broker.attachment = att;
            test_broker.attachment1 = att1;
            test_broker.attachment2 = att2;
            test_broker.saveRecord();
            test_broker.goPrevForReview();
            test_broker.goNextForReview();
            test_broker.strSelected = GardTestData.clientAcc.id+';'+GardTestData.clientAcc.id;
            test_broker.fetchSelectedClients();
            test_broker.setGlobalClientIds = new List<String>();
            List<SelectOption> clientOptions1 =  test_broker.getClientLst();
            test_broker.isSelGlobalClient = true;
            test_broker.setGlobalClientIds = new List<String>();
            test_broker.setGlobalClientIds.clear();
            List<SelectOption> testoptsel = new List<SelectOption>();
            testoptsel = test_broker.getClientLst();
            test_broker.lstContact = new List<Contact> ();
            test_broker.lstContact.add(GardTestData.brokerContact);
            test_broker.selectedClient = '';
            test_broker.strGlobalClient = 'testing';
            Case updtUWF = [SELECT Primary_assured_s_address__c,Contact_person__c,E_mail_address__c,Co_assureds_capacity__c,
                                 IMO_not_known__c,Name_on_Premium_Invoice__c,Address_details__c,VAT_Name_AddressOf_Company__c,
                                 VAT_number__c,Email_Uwform__c ,Country_prefix__c,Other__c,Other_Info__c,Claims_invoice_sent_to__c,Phone__c,Phone_Uwform__c
                                 FROM Case WHERE Id =: GardTestData.brokerCase.Id];
            updtUWF.Primary_assured_s_address__c = 'text';
            updtUWF.Contact_person__c = 'text';
            updtUWF.E_mail_address__c = 'abc@test.com';
            updtUWF.Co_assureds_capacity__c = 'text';
            updtUWF.IMO_not_known__c = true;
            updtUWF.Name_on_Premium_Invoice__c = 'text';
            updtUWF.Address_details__c= 'text';
            updtUWF.VAT_Name_AddressOf_Company__c= 'text';
            updtUWF.VAT_number__c= 'text';
            updtUWF.Country_prefix__c= 'text';
            updtUWF.Other__c = true;
            updtUWF.Other_Info__c= 'text';
            updtUWF.Claims_invoice_sent_to__c = 'text';
            updtUWF.Phone_Uwform__c= '987484';
            updtUWF.Commission__c = 3.00;
            updtUWF.Email_Uwform__c = 'abc@testmail.com';
            update updtUWF;
            test_broker.selectedValue = 'abc@testmail.com';
            test_broker.chboxaction();
            //test_broker.getGardTeam();
            //test_broker.getGardEmployees();
            //test_broker.getGardTeam();
            test_broker.changeSelectedClientIdFormsCS();
        }
        Test.stopTest();
    }
    
    @isTest public static void testWithClient(){
        createTestData();
        Test.startTest();
        System.runAs(GardTestData.clientUser){
            UWRForEntryFormforCharterClientsCtrl entryFormClientInstance=new UWRForEntryFormforCharterClientsCtrl();
            UWRForEntryFormforCharterClientsCtrl.InnerClass inn = new UWRForEntryFormforCharterClientsCtrl.InnerClass();
            entryFormClientInstance.attachmentList = new List<Attachment>();
            entryFormClientInstance.lstCon = new List<UWForm_Contact__c>();
            entryFormClientInstance.selectedClient = GardTestData.clientAcc.id;
            entryFormClientInstance.MailId = GardTestData.clientCase.id;
            entryFormClientInstance.getMemberDetails();
            entryFormClientInstance.getNames();
            entryFormClientInstance.getPhones();
            entryFormClientInstance.getEmails();
            entryFormClientInstance.getCName();
            entryFormClientInstance.populateAddress();
            entryFormClientInstance.addTextBox(); 
            entryFormClientInstance.rowIndex=0;
            entryFormClientInstance.removeTextBox();
            entryFormClientInstance.clearRecord();
            entryFormClientInstance.uwf.X3_instalments__c=true;
            entryFormClientInstance.uwf.X1_instalment__c=true;
            entryFormClientInstance.goNext();
            entryFormClientInstance.sendToForm();
            entryFormClientInstance.cancelRecord();
            entryFormClientInstance.goPrev();
            entryFormClientInstance.chkAccess();
            entryFormClientInstance.sendAckMail();
            entryFormClientInstance.dateOutputForReview='05.02.2015';
            entryFormClientInstance.dateOutputForReview='05.02.2015';
            entryFormClientInstance.saveRecord();
            entryFormClientInstance.saveSubmit();
            entryFormClientInstance.uwf.id=GardTestData.clientCase.id;
            entryFormClientInstance.lstClaim= new List<UWForm_Contact__c>();
            entryFormClientInstance.lstClaim.add(GardTestData.uwfc);
            entryFormClientInstance.saveRecord(); 
            entryFormClientInstance.strSelected = GardTestData.clientAcc.id+';'+GardTestData.clientAcc.id;
            entryFormClientInstance.fetchSelectedClients();
        }
        Test.stopTest();
    }
    
    @isTest public static void testWithGuest(){
        createTestData();
        Test.startTest();
        GardTestData.acmCSs.get(1).rolePreference__c = 'GUEST';
        update GardTestData.acmCSs.get(1);
        System.runAs(GardTestData.clientUser){
            UWRForEntryFormforCharterClientsCtrl entryFormClientInstance=new UWRForEntryFormforCharterClientsCtrl();
            UWRForEntryFormforCharterClientsCtrl.InnerClass inn = new UWRForEntryFormforCharterClientsCtrl.InnerClass();
            entryFormClientInstance.attachmentList = new List<Attachment>();
            entryFormClientInstance.lstCon = new List<UWForm_Contact__c>();
            entryFormClientInstance.selectedClient = GardTestData.clientAcc.id;
            entryFormClientInstance.MailId = GardTestData.clientCase.id;
            entryFormClientInstance.getMemberDetails();
            entryFormClientInstance.getNames();
            entryFormClientInstance.getPhones();
            entryFormClientInstance.getEmails();
            entryFormClientInstance.getCName();
            entryFormClientInstance.populateAddress();
            entryFormClientInstance.addTextBox(); 
            entryFormClientInstance.rowIndex=0;
            entryFormClientInstance.removeTextBox();
            entryFormClientInstance.clearRecord();
            entryFormClientInstance.uwf.X3_instalments__c=true;
            entryFormClientInstance.uwf.X1_instalment__c=true;
            entryFormClientInstance.goNext();
            entryFormClientInstance.sendToForm();
            entryFormClientInstance.cancelRecord();
            entryFormClientInstance.goPrev();
            entryFormClientInstance.chkAccess();
            entryFormClientInstance.sendAckMail();
            entryFormClientInstance.dateOutputForReview='05.02.2015';
            entryFormClientInstance.dateOutputForReview='05.02.2015';
            entryFormClientInstance.submitterEmail = 'emailForGuestPForms@aaa.bbb';
            entryFormClientInstance.saveRecord();
            entryFormClientInstance.saveSubmit();
            entryFormClientInstance.uwf.id=GardTestData.clientCase.id;
            entryFormClientInstance.lstClaim= new List<UWForm_Contact__c>();
            entryFormClientInstance.lstClaim.add(GardTestData.uwfc2);
            entryFormClientInstance.saveRecord(); 
            entryFormClientInstance.strSelected = GardTestData.clientAcc.id+';'+GardTestData.clientAcc.id;
            entryFormClientInstance.fetchSelectedClients();
            entryFormClientInstance.clearRecord();
        }
        Test.stopTest();
    }
}