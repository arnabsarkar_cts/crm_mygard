public without sharing class ShareDataCtrl
{    
//vribale declaration 
    //constants
    private static final Integer SMARTQUERY_PAGE_SIZE = 10;
    
    //pagination variables
    Public Integer index {get; set;}
    public Boolean isPrevious {get; set;} 
    public Boolean isFirst {get; set;} 
    public Boolean isNext {get; set;} 
    public Boolean isLast {get; set;} 
    public Boolean isPageLoad {get; set;} 
    
    private String logedInUserType;
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public String sSoqlQuery;
    private String nameOfMethod;
    private Boolean isQueried = false;
    public Integer pageNum{get;set;}
    private String sortDirection;
    private String sortExp = '';  
    public string loggedInAccountId;
    public List<Account> lstAcc;
    public Boolean checkShareEnabled;
    private String str_soql;
    private String str_soqlpagination;
    public List<ShareDataWrapper> shareLst { get; set; }
    public set<Id> setAgreementIds;
    public String selectedSharedId {get;set;}
    public String strParams{get;set;}
    public String selectedGuid {get;set;}
    List<contact_share__c> lstContactShare;
    Map<Id, List<Contact_Share__c>> mapAgIdandContactShare;
    public Account_Contact_Mapping__c acm_JUNC_OBJ;
    public String strJunctionRole;
    Public string strCondition  {get; set;}
    public string sortFullExp  {get; set;}
    String strASC;
    //for export/print
    public List<List<ShareDataWrapper>> lstAllAgreementsForExcel{get;set;}
    public Integer noOfData{get;set;}
    public List<ShareDataWrapper> shareLstExport{get;set;}
    //for update/delete
    Agreement_Set__c AgSettoDelete;
    List<Broker_Share__c> lstBrokerShareIdstoDelete;
    List<Contact_Share__c> lstContactShareIdstoDelete;
    
    //populate all optionlist 
    public Set<String> lstClientOptions;
    public List<Share_data_Target_Company__c> shared_companies;
    public List<String> SelectedClient {get;set;}   
    public List<SelectOption> getClientOptions() {
    List<SelectOption> Options = new List<SelectOption>();
        for(String strOp: lstClientOptions){
            if(strOp != null )
            Options.add(new SelectOption(strOp,strOp));
        }
        Options.sort();
    return Options;
    }  
    
    //to populate Share Name Filter
    public Set<String> lstShareNameOptions;
    public List<String> SelectedShareName {get;set;}
    public List<SelectOption> getShareNameOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstShareNameOptions!= null && lstShareNameOptions.size()>0){
            for(String strOp:lstShareNameOptions){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();
        }
        return Options;
    } 
    
    //to populate Target Company Filter
    public Set<String> lstTargetCompanyOptions;
    public List<String> SelectedTargetCompany {get;set;}
    public List<SelectOption> getTargetCompanyOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstTargetCompanyOptions!= null && lstTargetCompanyOptions.size()>0){
            for(String strOp:lstTargetCompanyOptions){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();
        }
        return Options;
    } 
    
    //to populate Target Contact Filter
    public Set<String> lstTargetContactOptions;
    public List<String> SelectedTargetContact {get;set;}
    public List<SelectOption> getTargetContactOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstTargetContactOptions!= null && lstTargetContactOptions.size()>0){
            for(String strOp:lstTargetContactOptions){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();
        }
        return Options;
    } 
     public String sortExpression
       {
         get
         {
            return sortExp;
         }
         set
         {
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExp)
             sortDirection = (sortDirection == strASC)? 'DESC' : strASC;
           else
             sortDirection = strASC;
           sortExp = value;
         }
       }
    
    // to get Sort Direction (Ascending/Descending)while sorting on basis of column name
     public String getSortDirection()
     {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
          return strASC;
        else
         return sortDirection;
     }
    
    // to get Sort Direction while sorting on basis of column name
     public void setSortDirection(String value)
     {  
       sortDirection = value;
     }
    //contructor define 
    public ShareDataCtrl() 
    {
        lstClientOptions = new Set<String>();
        SelectedClient = new List<String>();
        lstShareNameOptions = new Set<String>();
        SelectedShareName = new List<String>();
        lstTargetCompanyOptions = new Set<String>();
        SelectedTargetCompany = new List<String>();
        lstTargetContactOptions = new Set<String>();
        SelectedTargetContact = new List<String>();
        MyGardHelperCtrl.CreateCommonData(); // call helper class to get all basic info
        logedInUserType = MyGardHelperCtrl.USER_TYPE;
        loggedInAccountId = MyGardHelperCtrl.LOGGED_IN_ACCOUNT_ID;
        shared_companies = MyGardHelperCtrl.SHARED_COMPANIES;
        acm_JUNC_OBJ = MyGardHelperCtrl.ACM_JUNC_OBJ;
        strJunctionRole = acm_JUNC_OBJ.Administrator__c;//check admin from junction object        
        lstAcc = MyGardHelperCtrl.lstAccount;
        checkShareEnabled = lstAcc[0].Enable_share_data__c;
        shareLst = new List<ShareDataWrapper>(); 
        lstContactShare = new List<contact_share__c>();
        sortExpression = 'broker_share_id__r.Agreement_set_id__r.agreement_set_type__c';
        index = 0;
        size = SMARTQUERY_PAGE_SIZE;
        isPrevious = true;
        isFirst = true;
        isNext = true;
        isLast = true;
        isPageLoad = true;
        pageNum = 1;        
        strASC = 'ASC';
        sortDirection = strASC;
        strParams = '';
        //sortExpression = 'agreement_set_type__c';
                
    }
    
    //to populate data on pageLoad
    public pageReference populateData()
    {       
        //System.debug('logedInUserType:'+ logedInUserType);
        //System.debug('Share Enabled:'+ checkShareEnabled);
        //System.debug('strJunctionRole:'+ strJunctionRole);       
        if((logedInUserType == 'Broker') && (checkShareEnabled) && (strJunctionRole == 'Admin') && (shared_companies.size() > 0))
        {   

            System.debug('Inside PopulateData Loop');                        
            getPaginationData();
            getFilterData();    
            //for pagination and standard set controller
            //str_soqlpagination ='SELECT id, agreement_set_type__c from Agreement_Set__c where Id In:setAgreementIds ';  
            check();          
            fillData();
            return null;
        }
        else {
             return page.HomePage;
        }
    }
    
    public void getFilterData()
    {
    System.debug('isPageLoad:'+isPageLoad);
        System.debug('SelectedClient :'+SelectedClient);    
            //lstShareNameOptions.clear();
            //if(lstShareNameOptions!= null && lstShareNameOptions.size() >0)
            //lstShareNameOptions.clear();
            if(lstTargetCompanyOptions!= null && lstTargetCompanyOptions.size() >0)
            lstTargetCompanyOptions.clear();
            if(lstTargetContactOptions!= null && lstTargetContactOptions.size() >0)
            lstTargetContactOptions.clear();
            if(lstClientOptions!= null && lstClientOptions.size() >0)
            lstClientOptions.clear();
            System.debug('lstTargetCompanyOptions:'+lstTargetCompanyOptions);
             //for binding data in wrapper class
            str_soql = 'SELECT id,broker_share_id__c,broker_share_id__r.Agreement_Set_Id__c,broker_share_id__r.Active__c,broker_share_id__r.Agreement_set_id__r.status__c,'+
            'broker_share_id__r.Agreement_set_id__r.agreement_set_type__c, broker_share_id__r.Agreement_set_id__r.GUID__c,broker_share_id__r.Agreement_set_id__r.shared_with__r.Name,'+
            'broker_share_id__r.Contract_id__r.client__r.name,'+
            'ContactId__c,ContactId__r.Name FROM contact_share__c  where broker_share_id__r.Agreement_set_id__r.shared_by__c =: loggedInAccountId';
            sortFullExp = sortExpression  + ' ' + sortDirection;
            strCondition ='';
            if( SelectedShareName != null && SelectedShareName.size()>0){
                strCondition = ' AND broker_share_id__r.Agreement_set_id__r.agreement_set_type__c IN : SelectedShareName ';
            }
            if( SelectedClient != null && SelectedClient.size()>0){
                  strCondition = strCondition +' AND broker_share_id__r.Contract_id__r.client__r.Name IN : SelectedClient ';                
            }            
            if( SelectedTargetCompany != null && SelectedTargetCompany.size()>0){
                  strCondition = strCondition +' AND broker_share_id__r.Agreement_set_id__r.shared_with__r.Name IN : SelectedTargetCompany ';                
            }
            if( SelectedTargetContact != null && SelectedTargetContact.size()>0){
                  strCondition = strCondition +' AND ContactId__r.Name IN : SelectedTargetContact ';                
            }  
         if(isPageLoad)
         {     
            sSoqlQuery = str_soql + strCondition + ' ORDER BY '+sortFullExp+' ';                    
            String sSoqlQueryforShare = str_soql + strCondition + ' ORDER BY '+sortFullExp;            
            system.debug('Soql Query in case of pageLoad--->'+sSoqlQuery);

            for (contact_share__c contactShareforAgShare : database.query(sSoqlQueryforShare))  {                
             if(contactShareforAgShare != null ){                                  
                 lstShareNameOptions.add(contactShareforAgShare.broker_share_id__r.Agreement_Set_Id__r.Agreement_Set_Type__c);  
                 lstClientOptions.add(contactShareforAgShare.broker_share_id__r.Contract_id__r.client__r.name);               
                 lstTargetCompanyOptions.add(contactShareforAgShare.broker_share_id__r.Agreement_set_id__r.shared_with__r.Name);                 
                 lstTargetContactOptions.add(contactShareforAgShare.ContactId__r.Name);                                
             }
            }
            //noOfRecords = setAgreementIds.size();
            System.debug('*******lstClientOptions:'+lstClientOptions);
            isPageLoad = false;        
         }
         else
         {
            sSoqlQuery = str_soql + strCondition + ' ORDER BY '+sortFullExp+' ';
            for (contact_share__c contactShareforAgShare : database.query(sSoqlQuery))  {                
             if(contactShareforAgShare != null ){                                  
                 //lstShareNameOptions.add(contactShareforAgShare.broker_share_id__r.Agreement_Set_Id__r.Agreement_Set_Type__c);  
                 lstClientOptions.add(contactShareforAgShare.broker_share_id__r.Contract_id__r.client__r.name);               
                 lstTargetCompanyOptions.add(contactShareforAgShare.broker_share_id__r.Agreement_set_id__r.shared_with__r.Name);                 
                 lstTargetContactOptions.add(contactShareforAgShare.ContactId__r.Name);                                
             }
            }
            //noOfRecords = setAgreementIds.size();
            System.debug('lstTargetCompanyOptions:'+lstTargetCompanyOptions);
            System.debug('*******lstClientOptions:'+lstClientOptions);
         }
    }
    
    
    public void fillData(){    
        System.debug('sSoqlQuery:'+ sSoqlQuery);
        if(sSoqlQuery.length()>0)
        {          
            lstContactShare = database.query(sSoqlQuery);
            system.debug('****lstContactShare **********'+lstContactShare );
            system.debug('****lstContactSharesize **********'+lstContactShare.size() );
        }        
        mapAgIdandContactShare = new Map<Id, List<Contact_Share__c>>();                
        for(Contact_Share__c cs : lstContactShare)
        {
            if(mapAgIdandContactShare.size() < size )
            {
            System.debug('AgreementSetIdin map'+cs.broker_share_id__r.Agreement_set_Id__c);
            if(mapAgIdandContactShare.containsKey(cs.broker_share_id__r.Agreement_set_Id__c))
            {
                mapAgIdandContactShare.get(cs.broker_share_id__r.Agreement_set_Id__c).add(cs);
            }
            else
            {
                mapAgIdandContactShare.put(cs.broker_share_id__r.Agreement_set_Id__c,new List<Contact_Share__c>{cs});
            }
            }
        }     
        system.debug('mapAgIdandContactShare.size:'+mapAgIdandContactShare.size());
        List<Contact_Share__c> contactShare = new List<Contact_Share__c>();        
        shareLst = new List<ShareDataWrapper>(); 
        for(Id agSetId: mapAgIdandContactShare.keySet())
        {
            Set<String> setTargetContacts = new Set<String>();
            contactShare = mapAgIdandContactShare.get(agSetId);
            for(Contact_Share__c cshareinMap: contactShare)
            {
                setTargetContacts.add(cshareinMap.ContactId__r.Name);                 
            }
                       
            shareLst.add(new ShareDataWrapper(contactShare[0].broker_share_id__r.Agreement_set_id__r.agreement_set_type__c,
                            contactShare[0].broker_share_id__r.Contract_id__r.client__r.name,
                            contactShare[0].broker_share_id__r.Agreement_set_Id__r.shared_with__r.Name,
                            string.join(new List<String>(setTargetContacts),','),
                            contactShare[0].broker_share_id__r.Agreement_set_id__r.status__c,
                            agSetId,
                            contactShare[0].broker_share_id__r.Agreement_set_id__r.GUID__c)); 
            System.debug('shareLst:'+shareLst);
            System.debug('shareLst size:'+shareLst.size());

        }               
    }
      public List<ShareDataWrapper> getShares() 
    {
        return shareLst;
    }
    
    //wrapper class to display data according to Agreement Set
    public class ShareDataWrapper
    {
      public String status{get;set;}
      public String shareName{get;set;}
      public String targetCompany{get;set;}
      public String targetContact{get;set;}
      public String clientName{get;set;}
      public String shraeId{get;set;}
      public String guid{get;set;}
      
      public ShareDataWrapper(String nm,String cname, String tComp, String cont,String stat,String sId, String sGuid)
      {
         status = stat;
         shareName = nm;
         targetCompany = tComp;
         targetContact = cont;
         clientName = cname;
         shraeId = sId;
         guid = sGuid;
      }
    }
    //check disable/enable pagination button
    public void check()
    {
       System.debug('in check method');
       if(pageNum == 1)
       { 
           index = 0;
           isFirst = false;
           isPrevious = false;
       }
       else
       {
          isFirst = true;
          isPrevious = true;
       }
       Integer pageLimit = 1;
       System.debug('noOfRecords:'+noOfRecords);
       Decimal checkLimit = size;
       checkLimit = (noOfRecords/checkLimit);
       System.debug('checkLimit:'+checkLimit);
       pageLimit = (checkLimit.round(System.RoundingMode.CEILING)).intvalue();
       System.debug('pageLimit:'+pageLimit);
       
       if(pageNum == pageLimit)
       {
           isNext = false;
           isLast = false;
       }
       else
       {
           isNext = true;
           isLast = true;
       }
    }
    
    //populate data when pagination buttons are clicked
    public void populateDataonPagination()
    {
        List<ShareDataWrapper> tempList = new List<ShareDataWrapper>();
        for(integer i = index; i < shareLst.size(); i++){
            if(tempList.size() == size)
            break;
            ShareDataWrapper ac = shareLst.get(i);
            tempList.add(ac);
        }
        shareLst = new List<ShareDataWrapper>();
        for(ShareDataWrapper a : tempList){
            shareLst.add(a);            
        }      
      check();
    }
    
    
    // for setting pageNumber in pagination
    public Pagereference setPageNumber()
    {
      System.debug('PageNum:'+ pageNum);    
      index = (pageNum - 1) * size;
      shareLst = getPaginationData(); 
      System.debug('index in setPageNumber:'+index);
      populateDataonPagination();      
      return null;
    }
    
    //for first record
    public Pagereference firstRecord()
    {
        pageNum = 1;
        check();
        populateData();
        return null;
    }
    
    //for first record
    public Pagereference lastRecord()
    {
       shareLst = getPaginationData(); 
       Decimal checkLimit = size;
       checkLimit = (noOfRecords/checkLimit);
       System.debug('checkLimit:'+checkLimit);
       pageNum = (checkLimit.round(System.RoundingMode.CEILING)).intvalue();
       setPageNumber();
       return null;        
    }
    
    //for next record
     public Pagereference nextRecord(){
         
         shareLst = getPaginationData(); 
         if(index < shareLst.size()){
            index+= size;
        }    
        pageNum = pageNum + 1;
        populateDataonPagination();
        return null;
    } 
    
    //for previous record
     public Pagereference previousRecord(){
         shareLst = getPaginationData(); 
         if(index < shareLst.size()){
            index-= size;
        } 
        pageNum = pageNum - 1;   
        populateDataonPagination();
        return null;
    } 
    
    //for pagination
    public List<ShareDataWrapper> getPaginationData()
    {
            //start - for filter            
            sortFullExp = sortExpression  + ' ' + sortDirection;
            strCondition ='';
            if( SelectedShareName != null && SelectedShareName.size()>0){
                strCondition = ' AND broker_share_id__r.Agreement_set_id__r.agreement_set_type__c IN : SelectedShareName ';
            }
            if( SelectedClient != null && SelectedClient.size()>0){
                  strCondition = strCondition +' AND broker_share_id__r.Agreement_set_id__r.shared_by__r.Name IN : SelectedClient ';                
            }            
            if( SelectedTargetCompany != null && SelectedTargetCompany.size()>0){
                  strCondition = strCondition +' AND broker_share_id__r.Agreement_set_id__r.shared_with__r.Name IN : SelectedTargetCompany ';                
            }
            if( SelectedTargetContact != null && SelectedTargetContact.size()>0){
                  strCondition = strCondition +' AND ContactId__r.Name IN : SelectedTargetContact ';                
            }
            
            //end - for filter
            str_soql = 'SELECT id,broker_share_id__c,broker_share_id__r.Agreement_Set_Id__c,broker_share_id__r.Active__c,broker_share_id__r.Agreement_set_id__r.status__c,'+
            'broker_share_id__r.Agreement_set_id__r.agreement_set_type__c, broker_share_id__r.Agreement_set_id__r.GUID__c,broker_share_id__r.Agreement_set_id__r.shared_with__r.Name,'+
            'broker_share_id__r.Contract_id__r.client__r.name,'+
            'ContactId__c,ContactId__r.Name FROM contact_share__c  where broker_share_id__r.Agreement_set_id__r.shared_by__c =: loggedInAccountId';
            sSoqlQuery = str_soql + strCondition + ' ORDER BY '+sortFullExp+' ';
            system.debug('Soql Query--->'+sSoqlQuery);
            setAgreementIds = new set<Id>();
            for (contact_share__c contactShareListAll : database.query(sSoqlQuery))  {
                setAgreementIds.add(contactShareListAll.broker_share_id__r.Agreement_set_id__c);             
            }
            
            if(sSoqlQuery.length()>0)
            {
                System.debug('Inside loop');
                lstContactShare = database.query(sSoqlQuery);
                system.debug('****lstContactShare **********'+lstContactShare );
                system.debug('****lstContactSharesize **********'+lstContactShare.size() );
            }
        
        mapAgIdandContactShare = new Map<Id, List<Contact_Share__c>>();        
        //Set<String> setClients = new Set<String>();
        //Set<String> setTargetCompany = new Set<String>();       
        for(Contact_Share__c cs : lstContactShare)
        {
            if(mapAgIdandContactShare.containsKey(cs.broker_share_id__r.Agreement_set_Id__c))
            {
                mapAgIdandContactShare.get(cs.broker_share_id__r.Agreement_set_Id__c).add(cs);
            }
            else
            {
                mapAgIdandContactShare.put(cs.broker_share_id__r.Agreement_set_Id__c,new List<Contact_Share__c>{cs});
            }                                   
        }     
        List<Contact_Share__c> contactShare = new List<Contact_Share__c>();
        shareLst = new List<ShareDataWrapper>(); 
        for(Id agSetId: mapAgIdandContactShare.keySet())
        {
            Set<String> setTargetContacts = new Set<String>();
            contactShare = mapAgIdandContactShare.get(agSetId);
            for(Contact_Share__c cshareinMap: contactShare)
            {
                setTargetContacts.add(cshareinMap.ContactId__r.Name);                 
            }
                       
            shareLst.add(new ShareDataWrapper(contactShare[0].broker_share_id__r.Agreement_set_id__r.agreement_set_type__c,
                            contactShare[0].broker_share_id__r.Contract_id__r.client__r.name,
                            contactShare[0].broker_share_id__r.Agreement_set_Id__r.shared_with__r.Name,
                            string.join(new List<String>(setTargetContacts),','),
                            contactShare[0].broker_share_id__r.Agreement_set_id__r.status__c,
                            agSetId,
                            contactShare[0].broker_share_id__r.Agreement_set_id__r.GUID__c)); 
         }
         noOfRecords = shareLst.size();
         return shareLst;                  
    }
    
    //get related data from Agreement Set Id
    public void getRelatedData(String agId)
    {
         AgSettoDelete = new Agreement_Set__c(Id = agId);
         //List<Agreement_Set__c> lstAgSet = [Select Id from Agreement_Set__c where Id =:selectedSharedId];
         lstBrokerShareIdstoDelete = new List<Broker_Share__c>();
         if(selectedSharedId != null && selectedSharedId != '')
         {             
             lstBrokerShareIdstoDelete = [Select Id from Broker_Share__c Where Agreement_set_id__c =: selectedSharedId];
         }
         lstContactShareIdstoDelete = new List<Contact_Share__c>();
         if(lstBrokerShareIdstoDelete!=null && lstBrokerShareIdstoDelete.size()>0)
         {
            lstContactShareIdstoDelete = [Select Id from Contact_Share__c Where broker_share_id__c IN :lstBrokerShareIdstoDelete];
         }
    }
    
    
     public PageReference deleteRecord()
     {
         system.debug('selectedSharedId--->'+selectedSharedId);//Agreement Set Id
         
         getRelatedData(selectedSharedId);     
                          
         //transaction start
         Savepoint sp = Database.setSavepoint();
         try{
         
         //Do not delete broker share, just mark it inactive, Delete Contact Share and Agreement Share
         for(Broker_Share__c brokerShare: lstBrokerShareIdstoDelete)
         {
             brokerShare.Active__c = False;
         }
             delete lstContactShareIdstoDelete;
             update lstBrokerShareIdstoDelete;
             delete AgSettoDelete;
             System.debug('Agreement Set deleted successfully!!!');
             populateData();
         }       
           Catch(Exception e){
             System.debug('Exception1 from ShareDataCtrl:'+e);
             Database.rollback(sp);             
         }         
         //transaction end         
         return null;

     }
    
    //stop sharing
    public PageReference stopsharing()
     {
         system.debug('stop sharing selectedSharedId--->'+selectedSharedId);//Agreement Set Id
         getRelatedData(selectedSharedId);
         
          //transaction start
         Savepoint sp = Database.setSavepoint();
         try{
             //updating Agreement Set
             AgSettoDelete.status__c = 'Suspended';
             
             //updating Broker Share
             for(Broker_Share__c brokerShare: lstBrokerShareIdstoDelete)
             {
                 brokerShare.Active__c = False;
             }
             
             update AgSettoDelete;
             update lstBrokerShareIdstoDelete;
             System.debug('Status Change to Suspended successfully!!!');
             populateData();
         }
         Catch(Exception e){
             System.debug('Exception2 from ShareDataCtrl:'+e);
             Database.rollback(sp);             
         }         
         //transaction end
         
         return null;
     }
    
    //start sharing
    public PageReference startsharing()
     {
         system.debug('start sharing selectedSharedId--->'+selectedSharedId);//Agreement Set Id
         getRelatedData(selectedSharedId);
         
          //transaction start
         Savepoint sp = Database.setSavepoint();
         try{
             //updating Agreement Set
             AgSettoDelete.status__c = 'Active';
             
             //updating Broker Share
             for(Broker_Share__c brokerShare: lstBrokerShareIdstoDelete)
             {
                 brokerShare.Active__c = True;
             }
             
             update AgSettoDelete;
             update lstBrokerShareIdstoDelete;
             System.debug('Status Change to Active successfully!!!');
             populateData(); 
         }
         Catch(Exception e){
             System.debug('Exception3 from ShareDataCtrl:'+e);
             Database.rollback(sp);             
         }
          
         //transaction end
         
         return null;
     }
     
    //for edit functionality, takes GUID and navigate to NewShare Page
    public pageReference editShare(){
    System.debug('*****Agreement Set Guid:'+strParams);
     try
        {
            if(strParams != null && strParams!= '')
            {
                Agreement_Set__c agSet = [SELECT ID, guid__c from Agreement_Set__c WHERE guid__c =: strParams];
                selectedGuid = agSet.guid__c;
                PageReference sharePage = new PageReference('/apex/NewShare?shareId='+selectedGuid);        
                sharePage.setRedirect(true);
                return sharePage;
            }
            else
            return null;
         }
       catch(Exception e)
       {
         System.debug('Exception4 from ShareDataCtrl:'+e);
         return null;
       }
    }
    
    //For clear button
    public pageReference clearOptions()
    {
        SelectedClient.clear();        
        SelectedShareName.clear();
        SelectedTargetCompany.clear();
        SelectedTargetContact.clear();
        pageReference pg = page.ShareData;
        pg.setRedirect(true);
        return pg;
    }
    
    public Static String getGenerateTimeStamp()
    {
        Datetime myDT = Datetime.now();
        String formatted = myDT.formatGMT('d/MMM/yy HH:mm a');
        return formatted ;
    }
    public Static String getGenerateTime()
    {
        Datetime myDT = Datetime.now();
        String formatted = myDT.formatGMT('EEE MMM d HH:mm:ss yyyy');
        return formatted ;
    }
    public Static String getUserName()
    {
        String name;
        User usr = [Select Id, contactID from User Where Id =: UserInfo.getUserId()]  ;      
        Contact myContact;
        if(usr!= null && usr.contactID != null){
            myContact = [Select AccountId,Name from Contact Where id=: usr.contactID];
            name=myContact.Name;
        }
        return name;
    }
    
    //export function
     public PageReference exportToExcelForShareData(){
        System.debug('Inside Print loop');
        lstAllAgreementsForExcel = new List<List<ShareDataWrapper>>();
        List<ShareDataWrapper> lst = new List<ShareDataWrapper>();
        Integer count = 1;
        noOfData = 0;
        shareLstExport  = new List<ShareDataWrapper>();
        for(ShareDataWrapper sharedData : shareLst)
        {
           System.debug('*****sharedData'+sharedData);
           if(count < 1000){shareLstExport.add(new ShareDataWrapper 
                                                (
                                                    string.valueof(sharedData.shareName),
                                                    string.valueof(sharedData.clientName),
                                                    string.valueof(sharedData.targetCompany),
                                                    string.valueof(sharedData.targetContact),
                                                    string.valueof(sharedData.status),
                                                    string.valueof(sharedData.shraeId),
                                                    string.valueof(sharedData.guid)
                                                ));
                }
            else
            {
                lstAllAgreementsForExcel.add(shareLstExport);
                shareLstExport = new List<ShareDataWrapper>();
                shareLstExport.add(new ShareDataWrapper 
                                                (
                                                    string.valueof(sharedData.shareName),
                                                    string.valueof(sharedData.clientName),
                                                    string.valueof(sharedData.targetCompany),
                                                    string.valueof(sharedData.targetContact),
                                                    string.valueof(sharedData.status),
                                                    string.valueof(sharedData.shraeId),
                                                    string.valueof(sharedData.guid)
                                                ));
                count = 1;                
            }
            noOfData++;
        }
        lstAllAgreementsForExcel.add(shareLstExport);
        return page.GenerateExcelForShareData;
    }
    
    //print function
    public PageReference print(){
        System.debug('Inside Print loop');
        lstAllAgreementsForExcel = new List<List<ShareDataWrapper>>();
        List<ShareDataWrapper> lst = new List<ShareDataWrapper>();
        Integer count = 1;
        noOfData = 0;
        shareLstExport  = new List<ShareDataWrapper>();
        for(ShareDataWrapper sharedData : shareLst)
        {
           System.debug('*****sharedData'+sharedData);
           if(count < 1000){shareLstExport.add(new ShareDataWrapper 
                                                (
                                                    string.valueof(sharedData.shareName),
                                                    string.valueof(sharedData.clientName),
                                                    string.valueof(sharedData.targetCompany),
                                                    string.valueof(sharedData.targetContact),
                                                    string.valueof(sharedData.status),
                                                    string.valueof(sharedData.shraeId),
                                                    string.valueof(sharedData.guid)
                                                ));
                }
            else
            {
                lstAllAgreementsForExcel.add(shareLstExport);
                shareLstExport = new List<ShareDataWrapper>();
                shareLstExport.add(new ShareDataWrapper 
                                                (
                                                    string.valueof(sharedData.shareName),
                                                    string.valueof(sharedData.clientName),
                                                    string.valueof(sharedData.targetCompany),
                                                    string.valueof(sharedData.targetContact),
                                                    string.valueof(sharedData.status),
                                                    string.valueof(sharedData.shraeId),
                                                    string.valueof(sharedData.guid)
                                                ));
                count = 1;                
            }
            noOfData++;
        }
        lstAllAgreementsForExcel.add(shareLstExport);
        return page.PrintForShareData;
    }     
}