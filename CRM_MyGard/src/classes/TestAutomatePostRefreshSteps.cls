@isTest
Public Class TestAutomatePostRefreshSteps{
    public static testmethod void TestRefresh(){ 
        Gardtestdata testData = new Gardtestdata();
        testData.commonRecord();
        testData.customsettings_rec();
        Claim_Admin_Email__c clAdmEmail = new Claim_Admin_Email__c(Name = 'test',Email_Address__c = 'abc@testMail.com');
        insert clAdmEmail ;
        
        GardContacts__c gc = new GardContacts__c(name = 'test',Email__c = 'abc@testMail.com',Office_City__c = 'test City', Phone__c = '99558');
        insert gc;
        Opportunity_Email_Recipients__c oer = new Opportunity_Email_Recipients__c (name = 'test', Default_Survey_To__c = 'abc@testMail.com',
                                                                            Fleet_Assessment__c = 'abc@testMail.com',
                                                                            Management_Audit__c='abc@testMail.com',
                                                                            Vessel_Survey__c = 'test' );
        insert oer ;
        Feedback_mail_id__c fmi = new Feedback_mail_id__c(Name = 'test',Email_Address__c = 'abc@testMail.com');
        insert fmi;
        MyGard_support_email_address__c mgsEmail = new MyGard_support_email_address__c(Name = 'test',Email_Address__c = 'abc@testMail.com');
        insert mgsEmail ;
        PortfolioReportsCtrl_for_Marine_endpoint__c prc = new PortfolioReportsCtrl_for_Marine_endpoint__c(Name = 'test',
                                                                           Endpoint__c = 'https://soa-test.gard.no');
        insert prc;
        Search_webservice_endpoint__c swe = new Search_webservice_endpoint__c(Name = 'test',
                                                                              Endpoint__c = 'https://soa-test.gard.no');
        insert swe; 
        MailtoUWR__c m = new MailtoUWR__c(Name = 'test',Email__c = 'abc@testMail.com');
        insert m;
        Claim_webservice_Endpoint__c cwe = new Claim_webservice_Endpoint__c(Name = 'test',
                                                                            Endpoint__c = 'https://soa-test.gard.no');
        insert cwe; 
        HomeFeedbackRecipient__c hfr = new HomeFeedbackRecipient__c(Name = 'test', Email__c = 'abc@testMail.com');
        insert hfr ;
        //Gard_Administrators__c ga = new Gard_Administrators__c(Name = 'test',Email_Address__c = 'abc@testMail.com',Name__c = 'test Name');
        //insert ga;
        EmailSettings__c es = new EmailSettings__c(name = ' test',Attachment_BCC_Service__c ='abc@testMail.com');
        insert es;
        COFR_Request_Email_CS__c cre = new COFR_Request_Email_CS__c(name = 'test', value__c ='Test124@gmail.com');
        insert cre;
        Customer_feedback_Email_Recipient__c cfdbckEml = new Customer_feedback_Email_Recipient__c(Name = 'test',Complaint__c = 'Test124@gmail.com');
        insert cfdbckEml ;
        MDMConfig__c mdm = new MDMConfig__c(Name = 'test',
                                            Endpoint__c = 'https://soa-test.gard.no');
        insert mdm; 
        MDM_Settings__c mdmSet = new MDM_Settings__c(name = 'test',Web_Services_Enabled__c = true);
        insert mdmSet;
        CommonVariables__c cvar2 = new CommonVariables__c(name = 'DefaultProfilePic', value__c ='Test123@gmail.com');
        insert cvar2;
        test.startTest();
        System.RunAs(Gardtestdata.brokerUser){
            AutomatePostRefreshSteps autoPstRefScripct = new AutomatePostRefreshSteps();
            AutomatePostRefreshSteps.updateContactEmails();
            AutomatePostRefreshSteps.updateAccountEmails();
            AutomatePostRefreshSteps.updateGardContactsEmails();
            AutomatePostRefreshSteps.updateCaseEmails();
            AutomatePostRefreshSteps.updateCustomSettings();
        }
        test.stopTest();
    }
}