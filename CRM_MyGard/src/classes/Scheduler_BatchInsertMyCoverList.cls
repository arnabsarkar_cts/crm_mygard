//Created by Pulkit, intended to replace the old Scheduler_BatchInsertMyCover after 18R3
//To schedule this, use the following line
//String cronIdAnonymous = Scheduler_BatchInsertMyCoverList.scheduleMe();
//System.debug('cronIdAnonymous - '+cronIdAnonymous);
global class Scheduler_BatchInsertMyCoverList implements Schedulable{
    //CRON Expression - Seconds Minutes Hours Day_of_month Month Day_of_week Optional_year
    public static String cronExp = '0 0 3,9,15,21 * * ?';  //4 times a day @ 3AM,9AM,3PM,9PM

    global static String scheduleMe(){
        String cronId;
        Scheduler_BatchInsertMyCoverList batchScheduler = new Scheduler_BatchInsertMyCoverList(); 
        
        if(Test.isRunningTest()) cronId = System.schedule('Test My CoverList Refresh Job', cronExp, batchScheduler);
        else cronId = System.schedule('My CoverList Refresh Job', cronExp, batchScheduler);
        
        System.debug('Scheduler_BatchInsertMyCoverList cronID - '+cronId);
        return cronId;
    }

    global void execute(SchedulableContext sc){    
        //if(!Test.isRunningTest()){
            Database.executeBatch(new BatchInsertMyCoverList(true));
			Database.executeBatch(new BatchInsertMyCoverList(false)); 
        //}
    }
}