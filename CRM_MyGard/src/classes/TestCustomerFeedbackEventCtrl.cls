@isTest(seealldata=false)
public class TestCustomerFeedbackEventCtrl 
{
     public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
     public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
     public static List<Customer_Feedback__c> cusFeedList = new list<Customer_Feedback__c>();
     public static  Customer_Feedback__c cusList1,cusList2,cusList3;
     public static testMethod void CustomerFeedback()
      {
          conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
          conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
          conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
          conFieldsList.add(conSubFields);
          conFieldsList.add(conSubFields2);
          conFieldsList.add(conSubFields3);
          insert conFieldsList;
          //cusList1=new Customer_Feedback__c(id=01ID0000000Rsm2,Name='test',status__c=Closed,Source__c=Phone,Responsible__c=GardTestData.brokerUser.id,Type_of_feedback__c=Suggestion,Response__c=Products,CreatedDate=)
          
          GardTestData gtd=new GardTestData();
          gtd.commonRecord();
          System.runAs(GardTestData.brokerUser)
          {  
          Test.startTest();
          Event E = new Event();
          E.Type = 'Email';
          E.Description = 'test'; 
          E.Subject='test';
          E.DurationInMinutes=30;
          E.ActivityDateTime=DateTime.newInstance(2006, 11, 19, 3, 3, 3);
          insert E;
          ApexPages.StandardController sc = new ApexPages.standardController(E);
          CustomerFeedbackEventCtrl feedback=new CustomerFeedbackEventCtrl(sc);
          feedback.eventName='test';
          Test.stopTest();

         }
      }
    
}