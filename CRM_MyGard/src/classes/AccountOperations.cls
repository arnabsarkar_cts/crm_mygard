public class AccountOperations{
	public void doInsertAccounts(List<Account> allAccounts){
		Database.insert(allAccounts);
	}
	
	public void doUpdateAccounts(List<Account> allAccounts){
		Database.update(allAccounts);
	}
	
	public void doDeleteAccounts(List<Account> allAccounts){
		Database.delete(allAccounts);
	}
	
	public void doUpsertAccounts(List<Account> allAccounts){
		Database.upsert(allAccounts);
	}
}