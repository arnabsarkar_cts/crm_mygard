@isTest public class TestMyClaimsCtrl{
    private Static GardTestData gtdInstance;
    
    //test data setup
    private Static void createTestData(){
        Map<String, Topics__c> TopicTotal = Topics__c.getAll();
        List<String> topicOptions = new List<String>();  
        GardUtils.inUpdateUserFirst = false; 
        /*Markt = new Market_Area__c(Market_Area_Code__c = 'abcd');
        Market_AreaList.add(Markt);
        insert Market_AreaList;  
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;
        */ 
        gtdInstance = new GardTestData();
        test.startTest();
        gtdInstance.commonRecord();  
        Test.stopTest();
        Reimbursement__c reimb = new Reimbursement__c();
        insert reimb;
        
        Reimbursement_Line_Item__c rli = new Reimbursement_Line_Item__c(
            Amount__c = 100,
            Reimbursement__c = reimb.ID
        );
        insert rli;
        
        Case_Reimbursement_Relation__c crr = new Case_Reimbursement_Relation__c(
            Related_claim__c = GardTestData.clientClaim.ID,
            Reimbursement__c = reimb.ID      
        );
        insert crr;
        Hide_Peme_Objects_in_Portal__c hidepemeobject = new Hide_Peme_Objects_in_Portal__c(name = 'Object Name',Hide_peme_objects__c='Pre-Empl');
        insert hidepemeobject;
    } 
    
    //Test Method for MyClaimsCtrl ............
    public static testmethod void myClaims(){
        //TestMyAcc_Claim_RepClaimsCtrl.data();
        createTestData();
        
        //test.startTest();
        system.RunAs(GardTestData.brokerUser){
            
            ApexPages.StandardController scontroller = new ApexPages.StandardController(GardTestData.brokerAcc);
            Test.setCurrentPage(page.AllClaims);
            Apexpages.currentpage().getparameters().put('shortformat',' DESC');
            MyClaimsCtrl myclaimctrl1 = new MyClaimsCtrl(scontroller);
            myclaimctrl1.deleteFavourites();
            ApexPages.currentPage().getParameters().put('favId',GardTestData.test_fav.id);//ApexPages.currentPage().getParameters().put('favId',exfav.id);
            MyClaimsCtrl myclaimctrl = new MyClaimsCtrl(scontroller);
            List<SelectOption> coverlist= myclaimctrl.getCoverLst();
            List<SelectOption> yearlist=myclaimctrl.getYearLst();
            List<SelectOption> objlist=myclaimctrl.getObjectName();
            List<SelectOption> claimlist=myclaimctrl.getClaimTypeLst();
            List<SelectOption> clientlist=myclaimctrl.getClientLst();
            List<SelectOption> opclientlist=myclaimctrl.getOpClientLst();
            List<SelectOption> opcoverlist=myclaimctrl.getOpCoverLst();
            List<SelectOption> opyearlist= myclaimctrl.getOpYearLst();
            List<SelectOption> op_objlist=myclaimctrl.getOpObjectName();
            List<SelectOption> OpClaimTypeList= myclaimctrl.getOpClaimTypeLst();
            string dir = 'ASC';
            myclaimctrl.getSortDirection();
            myclaimctrl.setSortDirection(dir);
            myclaimctrl.claimType = GardTestData.cargoStr ;
            myclaimctrl.objects ='James';
            myclaimctrl.year = GardTestData.policyYrStr ;
            GardTestData.clientClaim.Event_Date__c = Date.valueof('2014-12-16');
            myclaimctrl.filterRecords();
            myclaimctrl.filterRecordsForOpen();
            myclaimctrl.getOpenClaimDetails();
            myclaimctrl.createOpenuserList();
            myclaimctrl.getAllClaimDetails();
            myclaimctrl.createAlluserList();
            myclaimctrl.claimId = GardTestData.brokerClaim.id;
            myclaimctrl.showCaseDetails();
            myclaimctrl.filterRecords();
            myclaimctrl.filterRecordsForOpen();
            //myclaimctrl.getOpenClaimDetails();
            myclaimctrl.getCoverLst();
            myclaimctrl.getOpCoverLst();
            myclaimctrl.createOpenuserList();
            myclaimctrl.getAllClaimDetails();
            myclaimctrl.createAlluserList();
            myclaimctrl.selectedClaimType.add('Crew');
            myclaimctrl.selectedClaimType.add(GardTestData.cargoStr );
            myclaimctrl.selectedObj.add('test1');
            myclaimctrl.selectedObj.add('test2');
            myclaimctrl.selectedYear.add(GardTestData.policyYrStr );
            myclaimctrl.selectedYear.add('2012');
            myclaimctrl.selectedCover.add('abc');
            myclaimctrl.selectedCover.add('abcd');
            myclaimctrl.selectedClient.add('axzs');
            myclaimctrl.selectedClient.add('axz');
            myclaimctrl.getFilterCriteria();
            GardTestData.brokerClaim.Event_Date__c = Date.valueof('2014-12-16');
            myclaimctrl.pageNum=1;
            myclaimctrl.setpageNumber();
            //myclaimctrl.exportToExcel();
            //myclaimctrl.exportToExcelopenclaims();
            myclaimctrl.printData();
            myclaimctrl.printDataOpenClaims();
            myclaimctrl.newFilterName='test123';
            myclaimctrl.createFavourites();
            myclaimctrl.fetchFavourites();
            MyClaimsCtrl.getGenerateTimeStamp();
            MyClaimsCtrl.getGenerateTime();
            //MyClaimsCtrl.getUserName();
            myclaimctrl.strSelected='a;bc';
            //myclaimctrl.fetchSelectedClients();
            myclaimctrl.strSelected='abc';
            //myclaimctrl.fetchSelectedClients();
            myclaimctrl.setpageNumberOpen();
            myclaimctrl.pageNumberOpen = 1;
            myclaimctrl.hasNextOpen = true;
            myclaimctrl.hasPreviousOpen = true;
            myclaimctrl.pageNumber = 1;
            myclaimctrl.previousOpen();
            myclaimctrl.nextOpen();
            myclaimctrl.lastOpen();
            myclaimctrl.firstOpen();
            myclaimctrl.clearOptionsAllClaims();
            myclaimctrl.clearOptionsOpenClaims();
        } 
        //test.stopTest();
    }
    
    public static testmethod void myClaims2(){
        //TestMyAcc_Claim_RepClaimsCtrl.data();
        createTestData();
        //test.startTest();
        system.RunAs(GardTestData.clientUser){
            
            ApexPages.StandardController scontroller_1 = new ApexPages.StandardController(GardTestData.clientAcc);
            Apexpages.currentpage().getparameters().put('shortformat',' DESC');
            Test.setCurrentPage(page.OpenClaims);
            MyClaimsCtrl myclaimctrl = new MyClaimsCtrl(scontroller_1);
            myclaimctrl.strGlobalClient=GardTestData.clientAcc.id+';'+GardTestData.clientAcc.id;
            List<SelectOption> coverlist= myclaimctrl.getCoverLst();
            List<SelectOption> yearlist=myclaimctrl.getYearLst();
            List<SelectOption> objlist=myclaimctrl.getObjectName();
            List<SelectOption> claimlist=myclaimctrl.getClaimTypeLst();
            List<SelectOption> clientlist=myclaimctrl.getClientLst();
            List<SelectOption> opclientlist=myclaimctrl.getOpClientLst();
            List<SelectOption> opcoverlist=myclaimctrl.getOpCoverLst();
            List<SelectOption> opyearlist= myclaimctrl.getOpYearLst();
            List<SelectOption> op_objlist=myclaimctrl.getOpObjectName();
            List<SelectOption> OpClaimTypeList= myclaimctrl.getOpClaimTypeLst();
            myclaimctrl.claimId = GardTestData.clientClaim.id;
            myclaimctrl.filterRecords();
            myclaimctrl.filterRecordsForOpen();
            myclaimctrl.createOpenuserList();
            myclaimctrl.getAllClaimDetails();
            System.assertEquals(myclaimctrl.isFirstEntry , false);
            myclaimctrl.createAlluserList();
            myclaimctrl.filterRecords();
            myclaimctrl.filterRecordsForOpen();
            myclaimctrl.getAllClaimDetails();
            myclaimctrl.createAlluserList();
            Boolean val = myclaimctrl.hasNext; 
            val = myclaimctrl.hasPrevious;
            myclaimctrl.previous();
            myclaimctrl.next();
            myclaimctrl.last();
            myclaimctrl.first();
            myclaimctrl.exportToExcel();
            myclaimctrl.exportToExcelOpenClaims();
            myclaimctrl.getYearLst();
            myclaimctrl.getObjectName();
            myclaimctrl.getClaimTypeLst();
            myclaimctrl.getOpenClaimDetails();
            myclaimctrl.getOpYearLst();
            myclaimctrl.getOpObjectName();
            myclaimctrl.getOpClaimTypeLst();
            myclaimctrl.previousOpen(); //ps
            myclaimctrl.pageNum=1;
            myclaimctrl.setpageNumber();
            myclaimctrl.fetchSelectedClients();
            myclaimctrl.strSelected=GardTestData.clientAcc.id+';'+GardTestData.clientAcc.id;
            myclaimctrl.fetchSelectedClients();
            myclaimctrl.clearOptionsAllClaims();
            myclaimctrl.clearOptionsOpenClaims();
            myclaimctrl.lstClaimForExcel = new List<Claim__c>();
            myclaimctrl.lstRepClaim = new List<Claim__c>();
            myclaimctrl.tabName = 'nothing';
            myclaimctrl.claimsHandlerId = 'nothing';
            myclaimctrl.pointingId = 'nothing';
            myclaimctrl.mapClaimsHandlerDetail = new Map<String,String>{'I Am the ClaimHandler' => 'I Am His/Her Details'};
            myclaimctrl.url = 'bet365.com';
            myclaimctrl.lstContact = new List<Contact>();
            myclaimctrl.lstCont = new List<Gard_Contacts__c>();
            myclaimctrl.lstGardContact = new List<Gard_Contacts__c>();
        }
        //test.stopTest();
    }
    
}