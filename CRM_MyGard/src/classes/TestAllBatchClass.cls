@isTest
public class TestAllBatchClass
{
        Public static List<Account> AccountList=new List<Account>();
        Public static List<Contact> ContactList=new List<Contact>();
        Public static List<Contract> ContractList=new List<Contract>();
        Public static List<User> UserList=new List<User>();
        Public static List<Asset> AssetList=new List<Asset>();
        Public static List<Case> caseList =new List<Case>();
        Public static MyCoverList__c coverlist;
        Public static MyCoverList__c coverlist1;
        //Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community Login User Custom' limit 1].id;
        Public static string partnerLicenseId = [SELECT Id FROM Profile WHERE Name='Partner Community Login User Custom' limit 1].id;
        Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        public static Account clientAcc,brokerAcc;
        public static Contact clientContact,brokerContact;
        public static User clientUser,brokerUser;
        public static Contract clientContract;
        public static Object__c obj;
        public static Asset clientAsset;
        public static Case clientCase;
        public static Database.QueryLocator QueryLoc;
        public static Database.BatchableContext BatchCont;
        public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
        public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    public static void data()
    { 
       // List<Account> AccountList=new List<Account>();
       // List<Contact> ContactList=new List<Contact>();
       // List<Contract> ContractList=new List<Contract>();
       // List<User> UserList=new List<User>();
       // List<Asset> AssetList=new List<Asset>();
       // List<Case> caseList =new List<Case>();
       // string partnerLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community' limit 1].id;
       // string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        
       // Database.QueryLocator QueryLoc;
       // Database.BatchableContext BatchCont;
        
       //Accounts............
       clientAcc = new Account(Name = 'Test_1', 
                                        Site = '_www.test_1.se', 
                                        Type = 'Client', 
                                        BillingStreet = 'Gatan 1',
                                        BillingCity = 'Stockholm', 
                                        BillingCountry = 'SWE', 
                                        BillingPostalCode = 'BS1 1AD',
                                        recordTypeId=System.Label.Client_Contact_Record_Type
                                       );
                                    
        AccountList.add(clientAcc); 
          
        brokerAcc = new Account(Name = 'Test_1', 
                                        Site = '_www.test_1.se', 
                                        Company_Role__c = 'Broker',
                                        Type = 'Broker', 
                                        BillingStreet = 'Gatan 1',
                                        BillingCity = 'Stockholm', 
                                        BillingCountry = 'SWE', 
                                        BillingPostalCode = 'BS1 1AD',
                                        recordTypeId=System.Label.Broker_Contact_Record_Type
                                       );
                                    
        AccountList.add(brokerAcc);
                       
        insert AccountList ;
        
        //Contacts.................
        clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id
                                          );
        ContactList.add(clientContact);
      
        brokerContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              AccountId = brokerAcc.Id
                                          );
        ContactList.add(brokerContact);
        insert ContactList;                    

        //Users..................
        clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser1',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
                
         UserList.add(clientUser) ;
         
        brokerUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'brokerUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = brokerContact.id
                                   );
                
         UserList.add(brokerUser) ;
         insert UserList;             

    
         clientContract = New Contract(Accountid = clientAcc.id, 
                                        Account = clientAcc, 
                                        Status = 'Draft',
                                        CurrencyIsoCode = 'SEK', 
                                        StartDate = Date.today(),
                                        ContractTerm = 2, 
                                        Client__c = clientAcc.id,
                                        Expiration_Date__c = date.valueof('2015-01-01'),
                                        Inception_date__c = date.valueof('2012-01-01'),
                                        Agreement_ID__c = '123abc',
                                        Agreement_Type_Code__c = 'acegi',
                                        Shared_With_Client__c = true
                                        );
         
         ContractList.add(clientContract);
         insert ContractList ;        
          
         obj = New Object__c( Dead_Weight__c= 20, Object_Unique_ID__c = '321cba');
         insert obj ;
         
         
         clientAsset = new Asset(Name= 'Asset 1',
                                 accountid =clientAcc.id,
                                 contactid = clientContact.Id,  
                                 Agreement__c = clientContract.id, 
                                 CurrencyIsoCode='USD',
                                 product_name__c = 'War',
                                 Object__c = obj.id,
                                 Underwriter__c = clientUser.id,
                                 Expiration_Date__c = date.valueof('2015-01-01'),
                                 Inception_date__c = date.valueof('2012-01-01'),
                                 Risk_id2__c = '8547abcr',
                                 Risk_ID__c='abce'
                                 ); 
         AssetList.add(clientAsset);
         
         insert AssetList;
         
        clientCase = new Case(Accountid = clientAcc.id, 
                                   ContactId = clientContact.id,
                                   Claim_Incurred_USD__c= 40000, 
                                   Claim_Reference_Number__c='123ert',
                                   Object__c= obj.id,
                                   Risk_Coverage__c = clientAsset.id,
                                   //Sub_Claim_Handler__c = clientContact.id,
                                   Contract_for_Review__c = clientContract.id,
                                   Type = 'Add new user'
                                   //Claims_handler__c = clientContact.id
                                  );
        caseList.add(clientCase); 
        insert caseList;

        coverlist = new MyCoverList__c(broker_id__c=clientAcc.id,
                                                     broker__c  =clientAcc.Name,
                                                     //sclaimslead__c='Y',
                                                     client_id__c=clientAcc.id,
                                                     client__c=clientAcc.Name,
                                                     expirydate__c='2016-12-19',
                                                     gardshare__c=100,
                                                     onrisk__c=true,
                                                     param__c='abc',
                                                     policyyear__c=2016,
                                                     productarea__c='P&I',
                                                     product__c='P&I Cover',
                                                     UnderwriterId__c=salesforceLicenseId,
                                                     underwriter__c=salesforceLicenseId,
                                                     userId__c=clientUser.id,
                                                     claimslead__c='N',
                                                     viewobject__c=3
                                                     );
        insert coverlist;
        system.assertequals(coverlist.claimslead__c,'N','coverlist inserted successfully');  
    }  
        public static testmethod void DuplicateClaimDelete()
        {
                conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
                conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
                conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
                conFieldsList.add(conSubFields);
                conFieldsList.add(conSubFields2);
                conFieldsList.add(conSubFields3);
                insert conFieldsList; 
            TestAllBatchClass.data();
            
            system.RunAs(clientUser)
            {
                 BatchDuplicateClaimDelete batchduplicatedelete = new BatchDuplicateClaimDelete();
                 batchduplicatedelete.execute(BatchCont,caseList);
                 batchduplicatedelete.start(BatchCont);
                 batchduplicatedelete.finish(BatchCont);
            }
         }   
            public static testmethod void UpdateUnderwriterName()
            {
                    conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
                    conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
                    conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
                    conFieldsList.add(conSubFields);
                    conFieldsList.add(conSubFields2);
                    conFieldsList.add(conSubFields3);
                    insert conFieldsList; 
                    TestAllBatchClass.data();
                System.runAs(clientUser)
                 {
                     BatchUpdateUnderwriterName btchupdtundr = new BatchUpdateUnderwriterName();
                     btchupdtundr.execute(BatchCont,AssetList);
                     btchupdtundr.start(BatchCont);
                     btchupdtundr.finish(BatchCont);
                 }
            }
            
            public static testmethod void UpdateGardContacts()
            {
                	conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
                    conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
                    conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
                    conFieldsList.add(conSubFields);
                    conFieldsList.add(conSubFields2);
                    conFieldsList.add(conSubFields3);
                    insert conFieldsList; 
               TestAllBatchClass.data();
               system.RunAs(clientUser)
               {
                     BatchUpdateGardContacts updateGardContacts = new BatchUpdateGardContacts();
                     updateGardContacts.execute(BatchCont,UserList);
                     updateGardContacts.start(BatchCont);
                     updateGardContacts.finish(BatchCont);
               } 
            }
            public static testmethod void InsertMyCover()
            {
                    conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
                    conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
                    conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
                    conFieldsList.add(conSubFields);
                    conFieldsList.add(conSubFields2);
                    conFieldsList.add(conSubFields3);
                    insert conFieldsList; 
                TestAllBatchClass.data();
                system.RunAs(clientUser)
                {
                      //Commented out for SF 56 nov'16
                      //BatchInsertMyCover_bkp insrtmycover = new BatchInsertMyCover_bkp('true');
                      BatchInsertMyCover2 insrtmycover = new BatchInsertMyCover2('true', null);
                      insrtmycover.start(BatchCont);
                      insrtmycover.execute(BatchCont,AccountList);
                      insrtmycover.finish(BatchCont);
                }
                system.RunAs(brokerUser)
                {
                      //Commented out for SF 56 nov '16
                      //BatchInsertMyCover_bkp insrtmycover = new BatchInsertMyCover_bkp('true');
                      BatchInsertMyCover2 insrtmycover = new BatchInsertMyCover2('true', null);
                      insrtmycover.start(BatchCont);
                      insrtmycover.execute(BatchCont,AccountList);
                      insrtmycover.finish(BatchCont);
                }
               /* system.RunAs(brokerUser)
                {
                      BatchInsertMyCover insrtmycover = new BatchInsertMyCover('true');
                      insrtmycover.start(BatchCont);
                      insrtmycover.execute(BatchCont,UserList);
                      insrtmycover.finish(BatchCont);
                }*/
            }
            
           /* public static testmethod void InsertMyCover1()
            {
                TestAllBatchClass.data();
                system.RunAs(clientUser)
                {
                      BatchInsertMyCover insrtmycover = new BatchInsertMyCover('true');
                      insrtmycover.start(BatchCont);
                      insrtmycover.execute(BatchCont,UserList);
                      insrtmycover.finish(BatchCont);
                }
                system.RunAs(brokerUser)
                {
                      BatchInsertMyCover insrtmycover = new BatchInsertMyCover('true');
                      insrtmycover.start(BatchCont);
                      insrtmycover.execute(BatchCont,UserList);
                      insrtmycover.finish(BatchCont);
                }
            }*/
            
            public static testmethod void DeleteRejectedClaims()
            {
                    conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
                    conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
                    conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
                    conFieldsList.add(conSubFields);
                    conFieldsList.add(conSubFields2);
                    conFieldsList.add(conSubFields3);
                    insert conFieldsList; 
               TestAllBatchClass.data();
               system.RunAs(clientUser)
               {
                     BatchDeleteRejectedClaims delRejectedClaims= new BatchDeleteRejectedClaims();
                     delRejectedClaims.execute(BatchCont,caseList);
                     delRejectedClaims.start(BatchCont);
                     delRejectedClaims.finish(BatchCont);
               } 
            }
        }