/****************************************************************************************************************************
*    Deveployed By   :    Cognizant Technology Solution
*    Created Date    :    15/03/2018 
*    Created By      :    Geetham Gai Godavarthi
*    Descriptions    :    This class is used to Change the status of the case as closed as soon the approval is either Approved or Rejected
*********************************************************************************/

public class UpdateCaseStatusAfterApprovalProcess 
{  
    @InvocableMethod
    public static void UpdateCaseStatus(list<ID> AccountID) 
    {
        set<ID> CaseID =new Set<ID>();    
        List<Account> account=[select id,StoreCase_Id__c from Account where id=:AccountID];
        For(Account CaseIdsIterator :account )
        {
            CaseID.add(CaseIdsIterator.StoreCase_Id__c);
        }
        List<Case> case_status = new List<Case>();
        system.debug('----CaseID----'+CaseID);
        case_status=[select id,Status from Case where id IN :CaseID];
        if(case_status != null && case_status.size() > 0){
            case_status[0].Status='Closed';
            update case_status;
        }
    }
}