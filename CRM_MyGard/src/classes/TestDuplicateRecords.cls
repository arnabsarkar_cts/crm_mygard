@isTest(seealldata=false)
    public class TestDuplicateRecords{
    
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name= 'Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Account_Contact_Mapping__c acm1;
    //public static string partnerLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community' and name='Partner Community Login User Custom' limit 1].id;
    public static UserRole SalesforceRoleId = [SELECT Id FROM UserRole WHERE Name='Bergvall Marine AS Partner User' limit 1];
    Public static testmethod void DuplicateRecords(){
    Market_area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
    insert Markt;
    AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
    insert numberofusers;

     //correspondant contacts Custom Settings
        List<ContactSubscriptionFields__c> conFieldsList = new List<ContactSubscriptionFields__c>();
        ContactSubscriptionFields__c conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        ContactSubscriptionFields__c conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        ContactSubscriptionFields__c conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;

    Gard_Contacts__c grdobj  =    new  Gard_Contacts__c(
                                                FirstName__c = 'Testreqchng',
                                                LastName__c = 'Baann',
                                                //Company_Type__c = 'Grd', 
                                                Email__c = 'WR_gards.12@Test.com',
                                                //MailingCity__c = 'Arendal', 
                                                //MailingCountry__c = 'Norway', 
                                               // MailingPostalCode__c =  '487155',
                                               // MailingState__c = 'Oslo', 
                                               // MailingStreet__c = 'Rd Road', 
                                                //Mobile__c = '98323322', 
                                                MobilePhone__c = '548645', 
                                                Nick_Name__c  = 'NilsPeter', 
                                                Office_city__c = 'Arendal', 
                                                Phone__c = '5454454',
                                                Portal_Image__c ='<img alt="User-added image" src="https://c.cs8.content.force.com/servlet/rtaImage?eid=a1hL0000000kSyZ&amp;feoid=00NL0000003OySC&amp;refid=0EML00000008Zjs"></img>', 
                                                Title__c = 'test'
                                               );
            insert grdobj ;    
    User salesforceLicUser = new User(
                                        Alias = 'standt', 
                                        profileId = salesforceLicenseId ,
                                        Email='standarduser@testorg.com',
                                        EmailEncodingKey='UTF-8',
                                        CommunityNickname = 'test13',
                                        LastName='Testing',
                                        LanguageLocaleKey='en_US',
                                        LocaleSidKey='en_US', 
                                        //contactID = brokercontact.id, 
                                        TimeZoneSidKey='America/Los_Angeles',
                                        UserName='mygardtest008@testorg.com',
                                        contactId__c = grdobj.id
                                   );
         insert salesforceLicUser;
    Account brokerAcc = new Account(Name='testre',
                                BillingCity = 'Southampton',
                                BillingCountry = 'United Kingdom',
                                BillingPostalCode = 'BS2 AD!',
                                BillingState = 'Avon LAng' ,
                                BillingStreet = '1 Mangrove Road',
                                recordTypeId=System.Label.Broker_Contact_Record_Type,
                                Site = '_www.tcs.se',
                                Type = 'prospect',
                                company_Role__c = 'broker',
                                //Company_ID__c = '64143',
                                guid__c = '8ce8ad89-a6ed-1836-9e17',
                                Market_Area__c = Markt.id,
                                Product_Area_UWR_2__c='P&I',
                                Product_Area_UWR_4__c='P&I',
                                Product_Area_UWR_3__c='Marine',
                                Claim_handler_Marine_2_lk__c = salesforceLicUser.id ,
                                //X2nd_UWR__c = salesforceLicUser.id ,
                                //X3rd_UWR__c = salesforceLicUser.id ,
                                //Underwriter_4__c = salesforceLicUser.id ,
                                Claim_handler_Cargo_Liquid__c = salesforceLicUser.id ,
                                Role_Broker__c = true,
                                Claim_handler_Charterers__c = salesforceLicUser.id ,
                                Claim_handler_Defence__c = salesforceLicUser.id ,
                                Claim_handler_Energy__c = salesforceLicUser.id ,
                                Claim_handler_Energy_2_lk__c= salesforceLicUser.id ,
                                Claims_handler_Builders_Risk_2_lk__c= salesforceLicUser.id ,
                                Claim_handler_Charterers_2_lk__c = salesforceLicUser.id ,
                                Claim_handler_Cargo_Liquid_2_lk__c = salesforceLicUser.id ,
                                Claims_handler_Builders_Risk__c = salesforceLicUser.id ,
                                //Claim_handler_Charterers__c = salesforceLicUser.id ,
                                Area_Manager__c = salesforceLicUser.id ,
                                X2nd_UWR__c=salesforceLicUser.id,
                                X3rd_UWR__c=salesforceLicUser.id,
                                Underwriter_4__c=salesforceLicUser.id,
                                Underwriter_main_contact__c=salesforceLicUser.id                                                                      
                           );
    insert brokerAcc;
    Account clientAcc = new Account(  Name = 'Testuu', 
                                            Site = '_www.test_1.s', 
                                            Type = 'client', 
                                            BillingStreet = 'Gatanfol 1',
                                            BillingCity = 'phuket', 
                                            BillingCountry = 'SWE', 
                                            BillingPostalCode = 'BS1 1AD',
                                            company_Role__c = 'client',
                                            recordTypeId=System.Label.Client_Contact_Record_Type,
                                            Market_Area__c = Markt.id,
                                            guid__c = '8ce8ad66-a6ec-1834-9e21',
                                            Product_Area_UWR_2__c='P&I',
                                            Product_Area_UWR_4__c='P&I',
                                            Product_Area_UWR_3__c='Marine',
                                            Claim_handler_Marine_2_lk__c = salesforceLicUser.id ,
                                            X2nd_UWR__c = salesforceLicUser.id ,
                                            X3rd_UWR__c = salesforceLicUser.id ,
                                            Role_Client__c = true,
                                            Underwriter_4__c = salesforceLicUser.id ,
                                            Claim_handler_Cargo_Liquid__c = salesforceLicUser.id ,
                                            Claim_handler_Charterers__c = salesforceLicUser.id ,
                                            Claim_handler_Defence__c = salesforceLicUser.id ,
                                            Claim_handler_Energy__c = salesforceLicUser.id ,
                                            Claim_handler_Energy_2_lk__c= salesforceLicUser.id ,
                                            Claims_handler_Builders_Risk_2_lk__c= salesforceLicUser.id ,
                                            Claim_handler_Charterers_2_lk__c = salesforceLicUser.id ,
                                            Claim_handler_Cargo_Liquid_2_lk__c = salesforceLicUser.id ,
                                            Claims_handler_Builders_Risk__c = salesforceLicUser.id ,
                                            Area_Manager__c = salesforceLicUser.id,
                                            UW_Assistant_1__c=salesforceLicUser.id,
                                            U_W_Assistant_2__c=salesforceLicUser.id    ,
                                            Claim_handler_Crew__c = salesforceLicUser.id,
                                            Company_Status__c='Active',
                                            P_I_Member_Flag__c = false
                                            
                               );                                    
        insert clientAcc; 
        System.assertEquals(clientAcc.Name , 'Testuu');
    Contact brokerContact = new Contact( 
                                        FirstName='Yoo',
                                        LastName='Baooo',
                                        MailingCity = 'Kingsville',
                                        OtherPhone = '1112223356',
                                        mobilephone = '1112223334',
                                        MailingCountry = 'United Kingdom',
                                        MailingPostalCode = 'SE3 1AD',
                                        MailingState = 'London',
                                        MailingStreet = '1 Eastwood Road',
                                        AccountId = brokerAcc.Id,
                                        Email = 'test32@gmail.com'
                                   );
    insert brokerContact;
    Contact brokerContact_1 = new Contact( 
                                        FirstName='Yoo1',
                                        LastName='Baooo1',
                                        MailingCity = 'Kingsville1',
                                        OtherPhone = '11122233561',
                                        mobilephone = '11122233341',
                                        MailingCountry = 'United Kingdom1',
                                        MailingPostalCode = 'SE3 1AD1',
                                        MailingState = 'London1',
                                        MailingStreet = '1 Eastwood Road1',
                                        AccountId = brokerAcc.Id,
                                        Email = 'test312@gmail.com'
                                   );
    insert brokerContact_1;
    Contact clientContact= new Contact(   FirstName='sat_1',
                              LastName='hak',
                              MailingCity = 'London',
                              OtherPhone = '1112223356',
                              mobilephone = '1112223334',
                              MailingCountry = 'United Kingdom',
                              MailingPostalCode = 'SE1 1AE',
                              MailingState = 'London',
                              MailingStreet = '4 London Road',
                              AccountId = clientAcc.Id,
                              Email = 'test321@gmail.com',
                              Primary_Contact__c = true
                          );
    insert clientContact;
   
    
     Contact clientContact_1= new Contact(   FirstName='test client',
                              LastName='hak',
                              MailingCity = 'London',
                              OtherPhone = '1112223356',
                              mobilephone = '1112223334',
                              MailingCountry = 'United Kingdom',
                              MailingPostalCode = 'SE1 1AE',
                              MailingState = 'London',
                              MailingStreet = '4 London Road',
                              AccountId = clientAcc.Id,
                              Email = 'test321@gmail.com',
                              Primary_Contact__c = true
                          )
                          ;
    insert clientContact_1;
    
     Contact clientContact_2= new Contact(   FirstName='test client2',
                              LastName='hak2',
                              MailingCity = 'London',
                              OtherPhone = '1112223356',
                              mobilephone = '1112223334',
                              MailingCountry = 'United Kingdom',
                              MailingPostalCode = 'SE1 1AE',
                              MailingState = 'London',
                              MailingStreet = '4 London Road',
                              AccountId = clientAcc.Id,
                              Email = 'test321@gmail.com',
                              Primary_Contact__c = true
                          )
                          ;
    insert clientContact_2;
    
     System.assertEquals(clientContact_1.LastName ,  'hak');
     
      User usr = new User(  Alias = 'standt' , 
                                  profileId = partnerLicenseId ,
                                  Email='Claim@gard.no',
                                  EmailEncodingKey= 'UTF-8',
                                  userRole = SalesforceRoleId ,
                                  LastName='tetst_005',
                                  LanguageLocaleKey='en_US' ,
                                  CommunityNickname = 'brokerUser',
                                  LocaleSidKey='en_US' ,  
                                  TimeZoneSidKey= 'America/Los_Angeles'  ,
                                  UserName='mygardtestbrokerUser@testorg.com.mygard',
                                  ContactId = clientContact_2.Id,
                                  contactId__c = grdobj.id,
                                  IsActive = true,
                                  City= 'Arendal'
                                 );
    Test.startTest();
    GardUtils.blockMultiCall=false;
    Account_Contact_Mapping__c acm = new Account_Contact_Mapping__c();
    acm.Account__c = brokerAcc.id;
    acm.Active__c = true;
    acm.Contact__c = brokerContact.id;
    acm.IsPeopleClaimUser__c = false;
    acm.Marine_access__c = true;
    acm.PI_Access__c = true;
    //acm.Show_Claims__c = true;
    //acm.Show_Portfolio__c = false;
    acm.Administrator__c = 'Admin';
    insert acm;
    
    acm1 = new Account_Contact_Mapping__c();
    acm1.Account__c = brokerAcc.id;
    acm1.Active__c = true;
    acm1.Contact__c = brokerContact_1.id;
    acm1.IsPeopleClaimUser__c = false;
    acm.Marine_access__c = true;
    acm.PI_Access__c = true;
    //acm1.Show_Claims__c = true;
    //acm1.Show_Portfolio__c = false;
    acm1.Administrator__c = 'Admin';
    insert acm1;
        
    acm.Account__c = clientAcc.id;
    acm.Contact__c = clientContact.id;
    update acm;
    acm1.Active__c = false;
    update acm;
    acm.Administrator__c = 'Admin';
    update acm;
    //delete acm;
    acm1.Administrator__c = 'Admin';
    update acm1; 
    
    Account_Contact_Mapping__c acm2 = new Account_Contact_Mapping__c();
    acm2.Account__c = brokerAcc.id;
    acm2.Contact__c = brokerContact.id;
    acm2.Administrator__c = 'Admin';
    acm2.Active__c = true;
    insert acm2;
     
    Account_Contact_Mapping__c acm3 = new Account_Contact_Mapping__c();
    acm3.Account__c = clientAcc.id;
    acm3.Contact__c = brokerContact.id;
    acm3.Administrator__c = 'Admin';
    acm3.Active__c = true;
    insert acm3;
    
    Account_Contact_Mapping__c acm4 = new Account_Contact_Mapping__c();
    acm4.Account__c = brokerAcc.id;
    acm4.Contact__c = clientContact.id;
    acm4.Administrator__c = 'Normal';
    acm4.Active__c = true;
    insert acm4;
    
    acm4.Administrator__c = 'Admin';
    update acm4;
    
    acm1.Active__c = true;
    update acm;
    delete acm;
    
    //acm4.Contact__c=clientContact_2.id;
    //update acm4;
    
   
     
   /* Account_Contact_Mapping__c acm5 = new Account_Contact_Mapping__c();
    acm5.Account__c = clientAcc.id;
    acm5.Contact__c = brokerContact.id;
    acm5.Administrator__c = 'Admin';
    acm5.Active__c = true;
    insert acm5;*/
     Test.stopTest();
    
    
    
    }
}