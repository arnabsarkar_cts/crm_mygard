/**
 * Tests addOptyBroker trigger
 */
@isTest
private class testAddOptyBrokerTrigger {

    public static Opportunity[] testOpps;
    public static Contact brokerContact;
    public static String currencyISOCode = 'USD';
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;

    static void createData()
    {
        RecordType oppMarineRecType = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' AND Name='Marine'];
        RecordType oppEnergyRecType = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' AND Name='Energy'];
                
        testOpps = new Opportunity[]{
            new Opportunity (Name='Test1', RecordTypeId=oppMarineRecType.Id, StageName='Renewable Opportunity', CloseDate=Date.Today()+7, CurrencyISOCode = currencyISOCode),
            new Opportunity (Name='Test2', RecordTypeId=oppEnergyRecType.Id, StageName='Renewable Opportunity', CloseDate=Date.Today()+7, CurrencyISOCode = currencyISOCode)
        };
        insert testOpps;
        
        RecordType brokerAccRecType = [SELECT Id FROM RecordType WHERE SObjectType='Account' AND Name='Broker'];
        
        Account brokerAcc = new Account(Name='Test Broker', RecordTypeId=brokerAccRecType.Id, Role__c = 'Broker', Role_Broker__c = true);
        insert brokerAcc;
        
        brokerContact = new Contact(LastName='Test', AccountId=brokerAcc.Id);
        insert brokerContact;
        
        Partner testPartner = new Partner(OpportunityId = testOpps[0].Id, AccountToId = brokerAcc.Id, Role='Broker');
        insert testPartner;
        
    }

    static testMethod void myUnitTest() {
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        
        createData();
        
        //Add broker contact to each opp and save
        testOpps = [SELECT Id, Sales_Channel__c, Broker_Contact__c FROM Opportunity WHERE Id=:testOpps[0].Id OR Id=:testOpps[1].Id];
        
        testOpps[0].Sales_Channel__c = 'Broker';
        testOpps[0].Broker_Contact__c = brokerContact.Id;
        testOpps[1].Sales_Channel__c = 'Broker';
        testOpps[1].Broker_Contact__c = brokerContact.Id;
        
        update testOpps;

    }
}