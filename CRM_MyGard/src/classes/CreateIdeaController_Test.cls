@isTest
public with sharing class CreateIdeaController_Test {
    public static testMethod void insertNewIdea_UnitTest(){
        List<Community> communityList = [Select Id, Name From Community WHERE IsActive=true 
                                         Order By CreatedDate ASC LIMIT 1];
        Test.startTest();
        	CreateIdeaController.categoryPickList();
        	CreateIdeaController.CommunityForIdeas();
        	CreateIdeaController.statusPickList();
        	CreateIdeaController.postIdeaLightning('Quote name', '<p>Test Description</p>', 'DWH', communityList[0].Id, 'Not Yet Reviewed');
        Test.stopTest();
    }
}