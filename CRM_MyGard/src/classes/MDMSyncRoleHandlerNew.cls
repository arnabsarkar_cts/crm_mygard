/**************************************************************************
* Author  : Arpan Muhuri
* Company : Cognizant Technology Solutions
* Date    : 05/03/2018
***************************************************************************/
    
/// <summary>
///  Handler class for the MDM_Role__c.MDMSyncRole trigger
/// </summary>
public without sharing class MDMSyncRoleHandlerNew {
    static String SOAPNS { 
        get {
            if (Test.isRunningTest()) {
                return 'http://schemas.xmlsoap.org/soap/envelope/';
            } else {
                return MDMConfig__c.getInstance().SOAPNS__c;
            }
        }
    }
    static String SOAPNS_Prefix { 
        get {
            if (Test.isRunningTest()) {
                return 'soapenv';
            } else {
                return MDMConfig__c.getInstance().SOAPPrefix__c;
            }
        }
    }
  public static void InsertRoleSyncStatus (List<MDM_Role__c> newRoles, Boolean webServiceEnabled) {
    for(MDM_Role__c r : newRoles) {
      //TODO:consider moving these values to a custom setting... so they can be referenced else where...
      if (webServiceEnabled) {
        if (System.IsBatch() || System.IsFuture()) {
          r.Synchronisation_Status__c = 'Waiting To Sync';
          r.Sync_Date__c = Datetime.now().addMinutes(-59);
        } else {
          r.Synchronisation_Status__c = 'Sync In Progress';
        }
      } else {
        r.Synchronisation_Status__c = 'Synchronised';
      }
      r.Synchronisation_Action__c = 'create';
    }
  }
  
  public static void UpdateRoleSyncStatus(List<MDM_Role__c> newRoles, Map<id, MDM_Role__c> oldRolesMap,Boolean webServiceEnabled) {
    for(MDM_Role__c r : newRoles) {
      //TODO:consider moving these values to a custom setting... so they can be referenced else where...
      if (webServiceEnabled) {
        if (r.Synchronisation_Status__c == 'Synchronised' && r.Active__c != oldRolesMap.get(r.Id).active__c && oldRolesMap.get(r.Id).active__c) {
          if (System.IsBatch() || System.IsFuture()) {
            r.Synchronisation_Status__c = 'Waiting To Sync';
            r.Sync_Date__c = Datetime.now().addMinutes(-59);
          } else {
            r.Synchronisation_Status__c = 'Sync In Progress';
          }
          r.Synchronisation_Action__c = 'update';
        }
      } else {
        r.Synchronisation_Status__c = 'Synchronised';
        r.Synchronisation_Action__c = 'update';
      }
    }
  }
  
  public static DOM.XMLNode SyncRoles(Map<id, MDM_Role__c> newRolesMap, Boolean webServiceEnabled) {
    DOM.XMLNode requestRoles;
    if (webServiceEnabled) {
      List<id> roleIds = new List<id>();
      roleIds.addAll(newRolesMap.keyset());
      
      if (!System.IsBatch() && !System.IsFuture()) {
        requestRoles = MDMRoleProxyNew.MDMUpsertRoleAsync(roleIds);
      }
    }
    return requestRoles ;
  }
  
    public static DOM.XMLNode SyncUpsertRoles(List<MDM_Role__c> newRoles, Map<id, MDM_Role__c> oldRolesMap, Boolean webServiceEnabled) {
        DOM.XMLNode requestUpsertRoles;
        if (webServiceEnabled) {
            List<id> rolesToUpdate = new List<id>();
            for (MDM_Role__c r : newRoles) {
                system.debug('From SyncUpsertRoles() -- New Synchronisation_Status__c-> '+r.Synchronisation_Status__c+' --oldRolesMap.get(r.id).Synchronisation_Status__c-- '+oldRolesMap.get(r.id).Synchronisation_Status__c);
                //if (r.Synchronisation_Status__c == 'Sync In Progress' && r.Synchronisation_Status__c != oldRolesMap.get(r.id).Synchronisation_Status__c) {
                    if (r.Active__c && r.Role_Modified__c == true) {
                        rolesToUpdate.add(r.Id);
                    }
               // }
            }
            system.debug('rolesToUpdate from SyncUpsertRoles()-> '+rolesToUpdate);
            if (rolesToUpdate.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    requestUpsertRoles = MDMRoleProxyNew.MDMUpsertRoleAsync(rolesToUpdate);
                }
            }
        }
        system.debug('requestUpsertRoles from SyncUpsertRoles()-> '+requestUpsertRoles);
        return requestUpsertRoles;
    }
    public static DOM.XMLNode SyncDeleteRoles(List<MDM_Role__c> newRoles, Map<id, MDM_Role__c> oldRolesMap, Boolean webServiceEnabled) {
        DOM.XMLNode requestDeleteRoles;
        if (webServiceEnabled) {
            List<id> rolesToDelete = new List<id>();
            for (MDM_Role__c r : newRoles) {
                system.debug('From SyncDeleteRoles() -- New Synchronisation_Status__c-> '+r.Synchronisation_Status__c+' --oldRolesMap.get(r.id).Synchronisation_Status__c-- '+oldRolesMap.get(r.id).Synchronisation_Status__c);
                if (r.Role_Modified__c == true && r.Synchronisation_Status__c == 'Sync In Progress' && r.Synchronisation_Status__c == oldRolesMap.get(r.id).Synchronisation_Status__c) {
                    if (!r.Active__c) {
                        rolesToDelete.add(r.Id);
                    }
                }
            }
            system.debug('rolesToDelete from SyncUpsertRoles()-> '+rolesToDelete);
            if (rolesToDelete.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    requestDeleteRoles = MDMRoleProxyNew.MDMDeleteRoleAsync(rolesToDelete);
                }
            }
        }
        return requestDeleteRoles;
    }
  
  public static void InsertAccountSyncStatus(List<MDM_Role__c> newRoles, Map<id, MDM_Role__c> newRolesMap, Boolean webServiceEnabled) {
    set<id> accountIdsSet = new Set<id>();
    for (MDM_Role__c r : newRoles) {
      accountIdsSet.add(r.Account__c);
    }
    
    ////PS 15/04/2014 - Comment out as now calculated by rollups and workflow
    //SetAccountSyncStatus(accountIdsSet, newRolesMap, webServiceEnabled);
  }
  
  public static void UpdateAccountSyncStatus(List<MDM_Role__c> newRoles, Map<id, MDM_Role__c> newRolesMap, Map<id, MDM_Role__c> oldRolesMap, Boolean webServiceEnabled) {
    set<id> accountIdsSet = new Set<id>();
    for (MDM_Role__c r : newRoles) {
      if (!webServiceEnabled || r.Synchronisation_Status__c != oldRolesMap.get(r.id).Synchronisation_Status__c) {
        accountIdsSet.add(r.Account__c);
      }
    }
    
    ////PS 15/04/2014 - Comment out as now calculated by rollups and workflow
    //SetAccountSyncStatus(accountIdsSet, newRolesMap, webServiceEnabled);
  }
  /*
  private static void SetAccountSyncStatus(set<id> accountIdsSet, Map<id, MDM_Role__c> newRolesMap, Boolean webServiceEnabled) {
    if (webServiceEnabled) {
      List<MDM_Role__c> roles;
      List<id> rolesToSyncDelete = new List<id>();
      Map<id, string> accountIds = new Map<id, string>();
      
      if (accountIdsSet != null && accountIdsSet.size() > 0) {
        for (MDM_Role__c r : [SELECT id, Account__c, Synchronisation_Status__c FROM MDM_Role__c WHERE Account__c IN : accountIdsSet]) {
          if (newRolesMap.get(r.id) != null) {
            r.Synchronisation_Status__c = newRolesMap.get(r.id).Synchronisation_Status__c;
          }
          if (accountIds.get(r.Account__c) == null) {
            accountIds.put(r.Account__c, r.Synchronisation_Status__c);
          } else if (r.Synchronisation_Status__c == 'Sync Failed') {
            //Sync Failed always overwrites...
            accountIds.put(r.Account__c, r.Synchronisation_Status__c);
          } else if ((r.Synchronisation_Status__c == 'Sync In Progress' || r.Synchronisation_Status__c == 'Waiting To Sync') && (accountIds.get(r.Account__c) != 'Sync Failed')) {
            accountIds.put(r.Account__c, 'Sync In Progress');
          } 
        }
        
        List<Account> accounts = [SELECT id, Roles_Sync_Status__c FROM Account WHERE id IN : accountIds.KeySet()];
        for (Account a : accounts) {
          a.Roles_Sync_Status__c = accountIds.get(a.id);
        }
        update accounts;
      }
      
    } else {
      List<Account> accounts = [SELECT id, Roles_Sync_Status__c FROM Account WHERE id IN : accountIdsSet];
      for (Account a : accounts) {
        a.Roles_Sync_Status__c = 'Synchronised';
      }
      update accounts;
    }
  }
  */
  public static void BeforeInsert(List<MDM_Role__c> newRoles, Map<id, MDM_Role__c> newRolesMap) {
    try {
      MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
      InsertRoleSyncStatus(newRoles, mdmSettings.Web_Services_Enabled__c);
    } catch (Exception ex) {
            Logger.LogException('MDMSyncRoleHandlerNew.BeforeInsert', ex);
      throw ex;
        }
  }
  
  public static void AfterInsert(List<MDM_Role__c> newRoles, Map<id, MDM_Role__c> newRolesMap) {
    try {
      MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
      SyncRoles(newRolesMap, mdmSettings.Web_Services_Enabled__c);
      ////PS 15/04/2014 - Comment out as now calculated by rollups and workflow
      //InsertAccountSyncStatus(newRoles, newRolesMap, mdmSettings.Web_Services_Enabled__c);
    } catch (Exception ex) {
            Logger.LogException('MDMSyncRoleHandlerNew.AfterInsert', ex);
      throw ex;
        }
  }
  
  public static void BeforeUpdate(List<MDM_Role__c> newRoles, Map<id, MDM_Role__c> newRolesMap, List<MDM_Role__c> oldRoles, Map<id, MDM_Role__c> oldRolesMap) {
    try {
      MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
      UpdateRoleSyncStatus(newRoles, oldRolesMap, mdmSettings.Web_Services_Enabled__c);
    } catch (Exception ex) {
            Logger.LogException('MDMSyncRoleHandlerNew.BeforeUpdate', ex);
      throw ex;
        }
  }
  
  public static void AfterUpdate(List<MDM_Role__c> newRoles, Map<id, MDM_Role__c> newRolesMap, List<MDM_Role__c> oldRoles, Map<id, MDM_Role__c> oldRolesMap) {
    String request;
    Boolean partnerChange = false;
    Boolean roleChange = false;
    Boolean billingAddChange = false;
    Boolean shippingAddChange = false;
    Boolean isApproved = false;
    Set<Id> mdmRolesIdSet = new Set<Id>();
    mdmRolesIdSet.addAll(newRolesMap.keySet());
    //Set<Id> mdlRolesIdSet = new Set<Id>{newRoles[0].Account__c};
    try {
        Gard_RecursiveBlocker.blocker = true;
      MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
      ////PS 15/04/2014 - Comment out as now calculated by rollups and workflow
      //UpdateAccountSyncStatus(newRoles, newRolesMap, oldRolesMap, mdmSettings.Web_Services_Enabled__c);
        
        DOM.XMLNode roleUpsertRequestXML = SyncUpsertRoles(newRoles,oldRolesMap,mdmSettings.Web_Services_Enabled__c);
        DOM.XMLNode roleDeleteRequestXML = SyncDeleteRoles(newRoles,oldRolesMap,mdmSettings.Web_Services_Enabled__c);
        DOM.Document xmlDoc = new DOM.Document();
        DOM.XmlNode envelope = xmlDoc.createRootElement('Envelope', SOAPNS, SOAPNS_Prefix);
        for(MDM_Namespaces__c n : MDM_Namespaces__c.getAll().values()){
            envelope.setNamespace(n.name,n.namespace__c);
        }           
        DOM.XMLNode soapHeader = envelope.addChildElement('Header', SOAPNS, null);
        DOM.XMLNode soapBody = envelope.addChildElement('Body', SOAPNS, null);
        if(roleUpsertRequestXML !=null){
            XmlUtils.copyXmlNode(roleUpsertRequestXML,soapBody);
            partnerChange = true;
        }
        if(roleDeleteRequestXML !=null){
            XmlUtils.copyXmlNode(roleDeleteRequestXML,soapBody);
            partnerChange = true;
        }
        system.debug('roleUpsertRequestXML-->'+roleUpsertRequestXML);
        system.debug('roleDeleteRequestXML-->'+roleDeleteRequestXML);
        request = xmlDoc.toXMLString();
        request = request.replaceAll('<MyBody>','');
        request = request.replaceAll('</MyBody>','');
        system.debug('***final  soapBody : ' + request);
        if(roleDeleteRequestXML != null || roleUpsertRequestXML != null){
            MDMRoleProxyNew.roleMergeCallOut(request,mdmRolesIdSet);
            //MDMPartnerProxyNew.singleCallOut(request,mdmRolesIdSet ,partnerChange,roleChange,billingAddChange,shippingAddChange,isApproved);
        }
    }catch (Exception ex) {
           Logger.LogException('MDMSyncRoleHandlerNew.AfterUpdate', ex);
     throw ex;
        }
  }
  
  public static void BeforeDelete(List<MDM_Role__c> oldRoles, Map<id, MDM_Role__c> oldRolesMap) {
    //DO Nothing for now...
  }
  
  public static void AfterDelete(List<MDM_Role__c> oldRoles, Map<id, MDM_Role__c> oldRolesMap) {
    //DO Nothing for now...
    // Trigger will never get to this point. 
    // Before delete should cancel the delete process therefore, after delete is never reached.
  }


}