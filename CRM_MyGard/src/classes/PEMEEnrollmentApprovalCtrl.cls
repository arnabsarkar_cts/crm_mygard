public class PEMEEnrollmentApprovalCtrl{
    
    public Id enrollId;
    public string approvalComments {get;set;}
  //  public PEME_Debit_note_detail__c debitNote{get;set;}
    public List<PEME_Manning_Agent__c> ManAge{get;set;} 
    public List<PEME_Debit_note_detail__c> debitNote{get;set;}
    private static String Approved='Approved';
     //For Approval Process Start
    public List<ProcessInstance> lstItemsForApproval {get; set;} //{lstItemsForApproval  = new List<ProcessInstance>();}
    //For Approval Process End
    
    public PEMEEnrollmentApprovalCtrl(ApexPages.StandardController controller) {
        lstItemsForApproval  = new List<ProcessInstance>();
        enrollId = (ID)ApexPages.currentPage().getParameters().get('id');
        system.debug('enrollId**************'+enrollId);
        PEME_Enrollment_Form__c eform=[Select id,enrollment_status__c from PEME_Enrollment_Form__c where id=:enrollId ];
        if(!eform.enrollment_status__c.equalsIgnoreCase('Under Approval')){
        	 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.error, Label.ApprovalPageError);                                              
          ApexPages.addMessage(myMsg);
        }
        else{
        debitNote = new List<PEME_Debit_note_detail__c>();
        ManAge = new List<PEME_Manning_Agent__c>();
        for(PEME_Manning_Agent__c peme : [Select Status__c from PEME_Manning_Agent__c where PEME_Enrollment_Form__c =:enrollId])
        {
           if(peme.Status__c == 'Approved')
           {
              //system.debug('peme.Status__c---------' + peme.Status__c); 
               ManAge.add(peme); 
           }
        }
        system.debug('ManAge---------' + ManAge.size());
        for(PEME_Debit_note_detail__c debNot:[Select Status__c from PEME_Debit_note_detail__c where PEME_Enrollment_Form__c =:enrollId  AND Status__c =: Approved])
        {
            debitNote.add(debNot); //= [Select Status__c from PEME_Debit_note_detail__c where PEME_Enrollment_Form__c =:enrollId limit 1];
        }
        lstItemsForApproval.add(fetchLatestApprovals(enrollId)); 
        }
    }  
    public PEMEEnrollmentApprovalCtrl()
    {
        lstItemsForApproval = new  List<ProcessInstance >();
        enrollId = ApexPages.currentPage().getParameters().get('id'); 
        debitNote = new List<PEME_Debit_note_detail__c>();
        for(PEME_Debit_note_detail__c debNot:[Select Status__c from PEME_Debit_note_detail__c where PEME_Enrollment_Form__c =:enrollId AND Status__c =: Approved])
        {
            debitNote.add(debNot);
        }
        lstItemsForApproval.add(fetchLatestApprovals(enrollId)); 
    }
    //For Approval Process Start    
    public ProcessInstance fetchLatestApprovals(String pemeId){
    try{
        ProcessInstance process = [Select Id, TargetObjectId, isDeleted, Status,(Select Id, ProcessInstanceId, ActorId, Actor.Name, StepStatus, Comments 
        From StepsAndWorkItems Where StepStatus = 'Pending' and isDeleted = false Order By Createddate Desc Limit 1)From ProcessInstance 
        Where isDeleted = false and Status = 'Pending' and TargetObjectId=:pemeId Order By Createddate Desc LIMIT 1];       
        return process; 
    }catch(Exception e){
       ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Enrolment is approved/rejected');                                              
       ApexPages.addMessage(myMsg);
      } 
       return null;                        
    }
    //For Approval Process End
    public PageReference approve(){
     //  debitNote = [Select Status__c from PEME_Debit_note_detail__c where PEME_Enrollment_Form__c =:enrollId limit 1];
     system.debug('debitnote*********'+debitNote);
     system.debug('manage---------'+ManAge);
     //system.debug('manage size-----'+ManAge.size());
     if(debitNote.size()>0)
     {
           //system.debug('manage size-----'+ManAge.size());
           if(debitNote[0].Status__c == 'Approved' && (ManAge != null && ManAge.size() > 0)){
               performApprovals('Approve');
               approveRejectExams(Approved);
               PageReference pg= redirect();
               return pg;   
           } 
           else{
               if(ManAge.size() == 0){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'There should be atleast one approved manning agent');                                              
                    ApexPages.addMessage(myMsg);
                    return null;
               }
               else{
                   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'There should be atleast one approved debit note');                                              
                   ApexPages.addMessage(myMsg);
                   return null;
               }
           } 
       }
       else{
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please add Debit note details');                                              
           ApexPages.addMessage(myMsg);
           return null; 
       }
         
    }
    
    public PageReference reject(){
   //  debitNote = [Select Status__c from PEME_Debit_note_detail__c where PEME_Enrollment_Form__c =:enrollId limit 1];
           performApprovals('Reject');
           approveRejectExams('Rejected');
           PageReference pg= redirect();
           return pg;   
    }
    
  /*  public PageReference cancel(){
       PageReference pg= redirect();
       return pg;      
    }*/
    
    public PageReference redirect(){
        PageReference pg= new PageReference(System.Label.InternalOrgURL+enrollId);
        pg.setRedirect(true);
        return pg;  
    }
    
    //For Approval Process Start
    public void performApprovals(String action){
        Approval.ProcessWorkitemRequest req;
        Approval.ProcessResult result;      
        if(lstItemsForApproval!=null && lstItemsForApproval.size()>0){
            for(ProcessInstance objProcess:lstItemsForApproval){
                req = new Approval.ProcessWorkitemRequest();
                req.setNextApproverIds(null);
                if(objProcess !=null){
                if(objProcess.StepsAndWorkitems !=null && objProcess.StepsAndWorkitems.size() >0){
                req.setWorkitemId(objProcess.StepsAndWorkitems[0].Id);
                }
                req.setComments(approvalComments);
                req.setAction(action);
                result = Approval.process(req);  
                }                                           
            }   
        }   
    }
    //For Approval Process End
    public void approveRejectExams(String action){
        PEME_Enrollment_Form__c enroll = new  PEME_Enrollment_Form__c();
        enroll = [Select Enrollment_Status__c from PEME_Enrollment_Form__c where id =:enrollId];
        if(action == Approved)
        {
            enroll.Enrollment_Status__c = 'Enrolled';
        }
        else
        {
            enroll.Enrollment_Status__c = 'Not Enrolled';
        }
        update enroll;
    }
}