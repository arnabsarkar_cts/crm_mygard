public class TimeBarResponseWrapper {  
        public string claimId{get;set;}
        public string clientId{get;set;}
        public string eventName{get;set;}
        public string eventDate{get;set;}
        public string reminderDate{get;set;}
        public string sDescription{get;set;}
        public string eventType{get;set;}
        public string claimCaseNo{get;set;}
        public string eventGuid{get;set;}
        public string subscribers{get;set;}
        public string sstatus{get;set;}
        public string systemsource{get;set;}
        public string creator{get;set;}
        public Integer claiming{get;set;}
        public string resolution{get;set;}
        public string resolvedBy{get;set;}
        public string resolveDate{get;set;} 
   
}