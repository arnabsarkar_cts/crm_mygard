global class BatchDeleteRejectedClaims implements Database.Batchable<sObject>
{
    // Batch Constructor
    global BatchDeleteRejectedClaims()  {}
   
     // Start Method
     global Database.QueryLocator start(Database.BatchableContext BC)
     {
        string noOfDays = System.Label.NoOfDaysToDeleteRejectedClaims; 
        Integer daysInNumber = 0;
        if(noOfDays!=null)
            daysInNumber = Integer.valueOf(noOfDays);
        system.debug('******** daysInNumber : '+daysInNumber);
        Date dt = System.Today() - daysInNumber;
        system.debug('********** dt : '+dt);
        String query = 'select id from Case where origin = \'Community\' and status = \'Rejected\' and Claim_Rejection_Date__c = :dt';
        system.debug('scheduler_class : '+query);
        return Database.getQueryLocator(query);
     }
   
     // Execute Logic
    global void execute(Database.BatchableContext BC, List<Case> scope)
    {
        set<id> setCaseId = new set<id>();
        system.debug('********** scope: '+scope);
        for(Case cases:scope)
        {
            cases.status = 'Old Rejected';
          //  setCaseId.add(cases.id);
        }
        //List<Case> lst = new List<Case>([select id from Case where parentid =: setCaseId]);
        try
        {
            update scope;
            //delete scope;
            /*if(lst.size()>0)
            {
                delete lst;
            }*/
        }
        catch(Exception ex)
        {
            system.debug(ex.getMessage());
        }
    }
   
     global void finish(Database.BatchableContext BC)
     {
   
     }
   
 }