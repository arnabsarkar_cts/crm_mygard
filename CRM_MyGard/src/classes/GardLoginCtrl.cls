/****************************************************************************************************************************
*    Deveployed By   :    Cognizant Technology Solution
*    Created Date    :    6/03/2015 
*    Descriptions    :    MyGard portal login has been controll from this class.
*    Modification Log
*    --------------------------------------------------------------------------------------------------------
*    Developer                                Date                        Description
*    --------------------------------------------------------------------------------------------------------
*    Arindam Ganguly                          6/03/2015                   Custom login for MyGard(portal) user
*    Arindam Ganguly                          17/11/2016                  Empty space in last char not accepted in username SF-259 
*    Arpan Muhuri                             16/11/2017                  Implementation of Remember me feature
*****************************************************************************************************************************/
Global class GardLoginCtrl
{
    public boolean isRemember{get;set;}
    public String userName{get;set;}
    public String password{get;set;}
    public String topMessage{get;set;}
    public String detailMessage{get;set;}
    
    public String BrowsertopMessage{get;set;}
    public String BrowserDetailMessage{get;set;}
    public string rememberFlag; //SF-3897
    public GardLoginCtrl(){
        
        showMessage();
        showBrowserMessage();
        //Start SF-3897
        isRemember = false;
        rememberFlag = '';
        Cookie existingLogInCredDetails = ApexPages.currentPage().getCookies().get('GardLoginCredentials');
        if(existingLogInCredDetails != null)
        {
            String[] existingUsrId = existingLogInCredDetails.getValue().split('##');
            system.debug('user id  '+existingUsrId[0]+' -- Password : '+existingUsrId[1]);
            Blob UsrIdStr = EncodingUtil.base64Decode(existingUsrId[0].trim());
            userName = UsrIdStr.toString();
            if(existingUsrId[1] == 'true'){
                isRemember = true;
            }
            system.debug('username : '+userName);
            //login();
        }
        else{
            userName = '';
            password = '';
            isRemember = false;
        }
        
        //End SF-3897


    }       
    /*
    global PageReference forwardToCustomAuthPage() 
    {
        return new PageReference('/apex/HomePage');
    }
    */
    
 public PageReference forwardToCustomAuthPage() {
        if(UserInfo.getUserType() == 'Guest')
        {
            return new PageReference('/GardLogin');
        }
        else{
            return null;
        }
    }
    
        global PageReference forgotPassword()
    {
        PageReference page = new PageReference('/GardForgotPassword');
        page.setRedirect(true);
        return page;
    }
    global PageReference login() 
    {
        //Start SF-3897
        system.debug('remember me flag-- '+isRemember);
        if(isRemember == true && (username != null && username != '') && (password != null && password != ''))
        {
            Blob user_Id = Blob.valueOf(username.trim());
            String u_Id = EncodingUtil.base64Encode(user_Id);
            rememberFlag = 'true';
            system.debug('ecryption: user name : '+username+' -- encrypted user name : '+u_Id);
            Cookie logInCred = new Cookie('GardLoginCredentials',u_Id+'##'+rememberFlag,null,86400,true);
            ApexPages.currentPage().setCookies(new Cookie[]{logInCred});
            system.debug('cookie inserted--> '+logInCred);
        }
        //End SF-3897
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        system.debug('******** startURL : '+startURL);
        //SF-259
       if(username != null)
        username = username.trim();
        system.debug('******** username : '+username);
        PageReference page = Site.login(username, password, '/mygard/apex/MultipleCompany');
        system.debug('******** site : '+site.getErrorMessage());
        system.debug('******** page : '+page);
        return page;
    }
    Public void showMessage(){
        Map<String, LogInDownTime__c> allMessages = LogInDownTime__c.getAll();
         for(String singleKey : allMessages.keySet()){
             if(singleKey.contains('TopMessage')){
                  LogInDownTime__c singleMessageField = allMessages.get(singleKey);
                  topMessage = singleMessageField.Message__c; 
              }else if(singleKey.contains('DetailMessage')){
                  LogInDownTime__c singleMessageField = allMessages.get(singleKey);
                  detailMessage = singleMessageField.Message__c; 
              }
         }
     }
     Public void showBrowserMessage(){
        Map<String, Browser_Worning__c> allMessages = Browser_Worning__c.getAll();
         for(String singleKey : allMessages.keySet()){
             if(singleKey.contains('TopMessage')){
                  Browser_Worning__c singleMessageField = allMessages.get(singleKey);
                  BrowsertopMessage = singleMessageField.Message__c; 
              }else if(singleKey.contains('DetailMessage')){
                  Browser_Worning__c singleMessageField = allMessages.get(singleKey);
                  BrowserDetailMessage = singleMessageField.Message__c; 
              }
         }
   }
}