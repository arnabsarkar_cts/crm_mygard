@isTest
public class TestGardUtils1{
    //local use Variables
    private static GardTestData gtdInstance;
    private static Map<id,Case>csMap;
    private static Case caseForTestCover2;
    private static Case caseForTestCover3;
    private static Case caseForTestCover4;
    private static Case caseForTestCover5;
    private static List<Case> cases;
    
    //Create Test Data
    private static void createTestData(){
        Gardtestdata gtdInstance = new GardTestData();
        gtdInstance.customsettings_rec();
        gtdInstance.commonrecord();
        
        Test.startTest();
        cases = new List<Case>();
        caseForTestCover2 = new Case(
            Accountid = GardTestData.brokerAcc.id,
            Account = GardTestData.brokerAcc,
            Contact = GardTestData.brokerContact, 
            ContactId = GardTestData.brokerContact.id,
            Origin =  GardTestData.orignStr ,                        
            Object__c = GardTestData.test_object_1st.id,                          
            Claim_Incurred_USD__c= 500,
            Claim_Reference_Number__c='kol232322',
            Reserve__c = 350,
            //  recordTypeId =System.Label.Broker_Contact_Record_Type,
            Uwr_form_name__c = GardTestData.testStr,
            Paid__c =50000,
            Total__c= 750, 
            //email = 'a2z@gmail.com',                         
            Member_reference__c = 'ash12322',
            Contact_me_area__c = 'Claims',
            Last_name__c = 'yo12',
            guid__c = '8ce8ad66-a6ec-1834-9e22',
            Claim_Type__c = GardTestData.cargoStr ,
            Risk_Coverage__c = GardTestData.brokerAsset_1st.id,
            Status = 'Review Completed',
            Status_Change_Date__c = datetime.now(),
            Contract_for_Review__c = GardTestData.brokerContract_1st.id,
            Claims_handler__c = GardTestData.brokerUser.id,
            Assetid = GardTestData.brokerAsset_1st.id,
            OwnerId = GardTestData.brokerUser.id,
            LOC__c =  GardTestData.cntrctNmStr  ,
            Type = 'Request change mortgagee',
            Submitter_company__c = GardTestData.brokerAcc.id
            //   Name_Of_Contract__c =  cntrctNmStr  
            //Owner=brokerUser.id
        );
        //insert caseForTestCover2;
        cases.add(caseForTestCover2);
        
        caseForTestCover3 = new Case(
            Accountid = GardTestData.brokerAcc.id,
            Account = GardTestData.brokerAcc,
            Contact = GardTestData.brokerContact, 
            ContactId = GardTestData.brokerContact.id,
            Origin =  GardTestData.orignStr ,                        
            Object__c = GardTestData.test_object_1st.id,                          
            Claim_Incurred_USD__c= 500,
            Claim_Reference_Number__c='kol2323223',
            Reserve__c = 350,
            //  recordTypeId =System.Label.Broker_Contact_Record_Type,
            Uwr_form_name__c = GardTestData.testStr,
            Paid__c =50000,
            Total__c= 750, 
            //email = 'a2z@gmail.com',                         
            Member_reference__c = 'ash123223',
            Contact_me_area__c = 'Claims',
            Last_name__c = 'yo13',
            guid__c = '8ce8ad66-a6ec-1834-9e23',
            Claim_Type__c = GardTestData.cargoStr ,
            Risk_Coverage__c = GardTestData.brokerAsset_1st.id,
            Status = 'Review Completed',
            Status_Change_Date__c = datetime.now(),
            Contract_for_Review__c = GardTestData.brokerContract_1st.id,
            Claims_handler__c = GardTestData.brokerUser.id,
            Assetid = GardTestData.brokerAsset_1st.id,
            OwnerId = GardTestData.brokerUser.id,
            LOC__c =  GardTestData.cntrctNmStr  ,
            Type = 'Ship Owners',
            Submitter_company__c = GardTestData.brokerAcc.id
            //   Name_Of_Contract__c =  cntrctNmStr  
            //Owner=brokerUser.id
        );
        //insert caseForTestCover3;
        cases.add(caseForTestCover3);
        
        caseForTestCover4 = new Case(
            Accountid = GardTestData.brokerAcc.id,
            Account = GardTestData.brokerAcc,
            Contact = GardTestData.brokerContact, 
            ContactId = GardTestData.brokerContact.id,
            Origin =  GardTestData.orignStr ,                        
            Object__c = GardTestData.test_object_1st.id,                          
            Claim_Incurred_USD__c= 500,
            Claim_Reference_Number__c='kol2323224',
            Reserve__c = 350,
            //  recordTypeId =System.Label.Broker_Contact_Record_Type,
            Uwr_form_name__c = GardTestData.testStr,
            Paid__c =50000,
            Total__c= 750, 
            //email = 'a2z@gmail.com',                         
            Member_reference__c = 'ash123224',
            Contact_me_area__c = 'Claims',
            Last_name__c = 'yo14',
            guid__c = '8ce8ad66-a6ec-1834-9e24',
            Claim_Type__c = GardTestData.cargoStr ,
            Risk_Coverage__c = GardTestData.brokerAsset_1st.id,
            Status = 'Review Completed',
            Status_Change_Date__c = datetime.now(),
            Contract_for_Review__c = GardTestData.brokerContract_1st.id,
            Claims_handler__c = GardTestData.brokerUser.id,
            Assetid = GardTestData.brokerAsset_1st.id,
            OwnerId = GardTestData.brokerUser.id,
            LOC__c =  GardTestData.cntrctNmStr  ,
            Type = 'New Blue Card',
            Submitter_company__c = GardTestData.brokerAcc.id
            //   Name_Of_Contract__c =  cntrctNmStr  
            //Owner=brokerUser.id
        );
        //insert caseForTestCover4;
        cases.add(caseForTestCover4);
        
        caseForTestCover5 = new Case(
            Accountid = GardTestData.brokerAcc.id,
            Account = GardTestData.brokerAcc,
            Contact = GardTestData.brokerContact, 
            ContactId = GardTestData.brokerContact.id,
            Origin =  GardTestData.orignStr ,                        
            Object__c = GardTestData.test_object_1st.id,                          
            Claim_Incurred_USD__c= 500,
            Claim_Reference_Number__c='kol2323225',
            Reserve__c = 350,
            //  recordTypeId =System.Label.Broker_Contact_Record_Type,
            Uwr_form_name__c = GardTestData.testStr,
            Paid__c =50000,
            Total__c= 750, 
            //email = 'a2z@gmail.com',                         
            Member_reference__c = 'ash123225',
            Contact_me_area__c = 'Claims',
            Last_name__c = 'yo15',
            guid__c = '8ce8ad66-a6ec-1834-9e25',
            Claim_Type__c = GardTestData.cargoStr ,
            Risk_Coverage__c = GardTestData.brokerAsset_1st.id,
            Status = 'Review Completed',
            Status_Change_Date__c = datetime.now(),
            Contract_for_Review__c = GardTestData.brokerContract_1st.id,
            Claims_handler__c = GardTestData.brokerUser.id,
            Assetid = GardTestData.brokerAsset_1st.id,
            OwnerId = GardTestData.brokerUser.id,
            LOC__c =  GardTestData.cntrctNmStr  ,
            Type = 'Object Edit',
            Submitter_company__c = GardTestData.brokerAcc.id
            //   Name_Of_Contract__c =  cntrctNmStr  
            //Owner=brokerUser.id
        );
        //insert caseForTestCover5;
        cases.add(caseForTestCover5);
        
        insert cases;
        
        csMap = new Map<id,Case>();
        csMap.put(caseForTestCover2.ID,caseForTestCover2);    
    }
    
    @isTest public static void gardUtilTest(){
        createTestData();
        System.runAs(GardTestDAta.brokerUser){
            GardUtils.changeCaseOwner(csMap);
            GardUtils.displayDoccument(GardTestData.brokerAcc.id);
            GardUtils.displayDoc(GardTestData.brokerAcc.id);
            //GardUtils.updateGUID();
            //GardUtils.updateGUIDExam();
        }
        GardUtils.updateUserRecord(new Set<Id>{GardTestData.clientContact.id});
        Test.stopTest();
    }
    
    @isTest public static void gardUtilTest2(){
        createTestData();
        GardUtils.updateUser((GardTestData.clientContact.id+''),'some@Email.com');
        GardUtils.deleteCase(new Set<Id>{GardTestData.clientCase.id});
        Test.stopTest();
    }
}