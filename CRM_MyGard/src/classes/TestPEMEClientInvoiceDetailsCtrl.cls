@isTest(seeAllData=false)
public class TestPEMEClientInvoiceDetailsCtrl
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static List<Object__c> ObjectList=new List<Object__c>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    public static Gard_Team__c gt;
    public static Country__c country ;
    public static PEME_Invoice__c pi;
    public static PEME_Manning_Agent__c  pmg;
    public static PEME_Debit_note_detail__c pdnBroker=new PEME_Debit_note_detail__c(); 
    public static List<PEME_Debit_note_detail__c> pdnList=new List<PEME_Debit_note_detail__c>();
    public static PEME_Exam_Detail__c PExamDetail;
    public static List<AccountToContactMap__c> AccConMapList=new List<AccountToContactMap__c>(); 
    public static Extranet_Favourite__c ef;
    public static User user;
    public static Account_Contact_Mapping__c acm_UpdateFor_gardUtil = new Account_Contact_Mapping__c();
    public static User brokerUser , clientUser ;
    public static Contact clientContact , brokerContact ; 
    public static Account brokerAcc , clientAcc;
    public static Object__c Object1,Object2;
    public static PEME_Enrollment_Form__c pefBroker;
    public static AccountToContactMap__c AccConMapBroker,AccConMapClient;
    public Static void createTestData()
    {
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        gt = new Gard_Team__c(Active__c = true,Office__c = 'test',Name = 'GTeam',Region_code__c = 'TEST');
        insert gt;
        country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Gard_Team__c=gt.Id,Claims_Support_Team__c=gt.Id);
        insert country;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrcClient);
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        vrcList.add(vrcBroker);
        insert vrcList;
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c ='Test123@gmail.com');
        insert cvar;
        user = new User(
                            Alias = 'standt', 
                            profileId = salesforceLicenseId ,
                            Email='standarduser@testorg.com',
                            EmailEncodingKey='UTF-8',
                            CommunityNickname = 'test13',
                            LastName='Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',  
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='test008@testorg.com',
                            ContactId__c = gc.id
                             );
        insert user ;
        //added for gardutil coverage ---- start
        GardUtils.flag = false;
        user.CommunityNickname = 'testUpdate123';
        update user;
        //added for gardutil coverage ---- stop 
        User user1 =new User();
        user1 = [select id, contactid__c from user where id=:user.id];
        brokerAcc = new Account( Name='testre',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.Broker_Contact_Record_Type,
                                        Site = '_www.cts.se',
                                        Type = 'Broker' ,
                                        Area_Manager__c = user1.id,      
                                        Market_Area__c = Markt.id  ,
                                        Confirm_not_on_sanction_lists__c = true,
                                        License_description__c  = 'some desc',
                                        Description = 'some desc',
                                        Licensed__c = 'Pending',
                                        Sub_Roles__c = 'Broker - Reinsurance Broker',
		                                country__c = country.id
                                                                  
                                     );
        AccountList.add(brokerAcc);
        clientAcc= New Account();
        clientAcc.Name = 'Testt';
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.PEME_Enrollment_Status__c='Enrolled';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.country__c = country.Id;
        clientAcc.Area_Manager__c = user1.id;        
        clientAcc.Market_Area__c = Markt.id;
        clientAcc.Sub_Roles__c = '';
        clientAcc.country__c = country.id;
        
        AccountList.add(clientAcc);
        insert AccountList; 
        brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(brokerContact) ;
        clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        insert ContactList;
        //added for gardutil coverage ---- start
        //GardUtils.inUpdateUserFirst = true;
        clientContact.MailingCity = 'LondonCity';
        update clientContact;
        //added for gardutil coverage ---- stop 
        brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='test120@test.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id
                                   );
        UserList.add(brokerUser);
        clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
                
         UserList.add(clientUser) ;
         insert UserList;
         Object1=new Object__c(Name= 'TestObject1',
                                         //CurrencyIsoCode='NOK-Norwegian Krone',
                                         Object_Unique_ID__c='test11223'
                                         );
         ObjectList.add(Object1);
         Object2=new Object__c(
                                         Name= 'TestObject2',
                                        // CurrencyIsoCode='NOK-Norwegian Krone',
                                         Object_Unique_ID__c='test11224'
                                         );
         ObjectList.add(Object2);
         insert ObjectList;
         
            pefBroker=new PEME_Enrollment_Form__c();
            pefBroker.ClientName__c=brokerAcc.ID;
            pefBroker.Enrollment_Status__c='Draft';
            pefBroker.Submitter__c=brokerContact.Id;
            insert pefBroker;
            
             pmg=new PEME_Manning_Agent__c  ();
            pmg.Client__c=brokerAcc.ID;
            pmg.PEME_Enrollment_Form__c=pefBroker.ID;
            pmg.Company_Name__c='Test Company';
            pmg.Contact_Point__c=brokerContact.Id;
            pmg.Status__c='Approved';
            insert pmg;
            
           pdnBroker.PEME_Enrollment_Form__c=pefBroker.ID;
        //pdnBroker.Contact_person_Email__c='pdnc@test.com';
        pdnBroker.Contact_person_Name__c='conpdncPerson';
        pdnBroker.Requested_Contact_person_Email__c='pdnc@test1.com';
        pdnBroker.Requested_Contact_person_Name__c='conpdncPerson1';
        pdnBroker.Requested_City__c='testpdnccity1';
        pdnBroker.Requested_Country__c='testccountry1';
        pdnBroker.Requested_State__c='testcstate1';
        pdnBroker.Requested_Zip_Code__c='222755';
        pdnBroker.Requested_Address__c='testcaddr1';
        pdnBroker.Requested_Address_Line_2__c='testcaddr2';
        pdnBroker.Requested_Address_Line_3__c='testcaddr3';
        pdnBroker.Requested_Address_Line_4__c='testcaddr4';
        pdnBroker.Status__c='Approved';
        pdnBroker.Comments__c='testc comment';
        pdnBroker.RequestedComments__c='testc comment1';
        pdnList.add(pdnBroker);
        
        insert pdnList; 
         
         // PEMEClientInvoiceDetailsCtrl pcid=new PEMEClientInvoiceDetailsCtrl();
            pi=new PEME_Invoice__c ();
            pi.Client__c=clientAcc.id;
            pi.Clinic_Invoice_Number__c='qwerty123';
            pi.Clinic__c=clientAcc.id;
            pi.Crew_Examined__c=11223;
            pi.Invoice_Date__c=Date.valueOf('2016-01-05');
            pi.PEME_reference_No__c='aasd23';
            pi.Status__c='Approved';
            pi.ObjectList__c='qwerty for test purpose';
            pi.Total_Amount__c=112230;
            pi.Vessels__c= Object1.id;
            //pi.GUID__c='guid1';
            pi.Period_Covered_From__c=Date.valueOf('2016-01-05');
            pi.Period_Covered_To__c =Date.valueOf('2016-01-28');
            pi.PEME_Enrollment_Detail__c = pefBroker.id;
            insert pi;
            pi.guid__c = '1ghsdjhasgdjhasg';
            update pi;
            PExamDetail=new PEME_Exam_Detail__c ();
            PExamDetail.Age__c=30;
            PExamDetail.Amount__c=12000;
            PExamDetail.Date__c=Date.valueOf('2016-01-10');
            PExamDetail.Examination__c='Demo Exam';
            PExamDetail.Examinee__c='Demo employee';
           // PExamDetail.Id=;
            PExamDetail.Invoice__c=pi.id;
            PExamDetail.Object_Related__c=Object1.id;
            PExamDetail.Rank__c='Five';
            PExamDetail.Status__c='Approved';
            
            insert PExamDetail;
            
            AccConMapBroker=new AccountToContactMap__c();
            AccConMapBroker.Name=BrokerContact.id;
            AccConMapBroker.AccountId__c=brokerAcc.id;
            AccConMapBroker.RolePreference__c='Broker';
            AccConMapList.add(AccConMapBroker);
                        
            AccConMapClient=new AccountToContactMap__c();     
            AccConMapClient.Name=clientContact.id;
            AccConMapClient.AccountId__c=clientAcc.id;
            AccConMapClient.RolePreference__c='Client';
            AccConMapList.add(AccConMapClient);
            
            insert(AccConMapList);
             //https://developer.salesforce.com/forums/?id=906F00000008ye4IAA
             //https://developer.salesforce.com/forums/?id=906F0000000997LIAQ
             ef=new Extranet_Favourite__c();
          //   ef.ID='1122eeddffrrggt';
             insert ef;
        }
    Static testMethod void cover1(){   
        createTestData();  
        System.runAs(brokerUser)
            {
                
                test.startTest();
                System.currentPageReference().getParameters().put('invoices' ,'1ghsdjhasgdjhasg');
                System.currentPageReference().getParameters().put('favId' ,ef.ID);
                PEMEClientInvoiceDetailsCtrl PEMEClientInvoiceDetailsCntl = new PEMEClientInvoiceDetailsCtrl ();
                PEMEClientInvoiceDetailsCntl.loadData();
              //  PEMEClientInvoiceDetailsCntl.orderByNew = 'Date__c';
                PEMEClientInvoiceDetailsCntl.generateExams();
                PEMEClientInvoiceDetailsCntl.createOpenUserList();
                //PEMEClientInvoiceDetailsCntl.setSortDirection('ASC');
                PEMEClientInvoiceDetailsCntl.getSortDirection();
                PEMEClientInvoiceDetailsCntl.getExamOptions();
                PEMEClientInvoiceDetailsCntl.getObjectOptions();
                //PEMEClientInvoiceDetailsCntl.sortExpression='ASC';
                PEMEClientInvoiceDetailsCntl.firstOpen();
                PEMEClientInvoiceDetailsCntl.hasPreviousOpen=true;
                PEMEClientInvoiceDetailsCntl.previousOpen();
                PEMEClientInvoiceDetailsCntl.hasNextOpen=false;
                PEMEClientInvoiceDetailsCntl.nextOpen();
                PEMEClientInvoiceDetailsCntl.lastOpen();
                PEMEClientInvoiceDetailsCntl.setpageNumberOpen();
                PEMEClientInvoiceDetailsCntl.selectedExam=new list<String>();
                PEMEClientInvoiceDetailsCntl.selectedExam.add('checking');
                PEMEClientInvoiceDetailsCntl.selectedObject=new list<String>();
                PEMEClientInvoiceDetailsCntl.selectedObject .add('checking');
                PEMEClientInvoiceDetailsCntl.clearOptions();
                PEMEClientInvoiceDetailsCntl.print();
                PEMEClientInvoiceDetailsCntl.exportToExcel();
                PEMEClientInvoiceDetailsCntl.newFilterName='Demo Filter';
                PEMEClientInvoiceDetailsCntl.createFavourites();
                PEMEClientInvoiceDetailsCntl.fetchFavourites();
                PEMEClientInvoiceDetailsCntl.deleteFavourites();
                PEMEClientInvoiceDetailsCntl.selectedExam.add('test');
                PEMEClientInvoiceDetailsCntl.selectedObject.add('check');
                PEMEClientInvoiceDetailsCntl.generateExams();
            }
        test.stopTest();
        }
    Static testMethod void cover2(){ 
        createTestData(); 
        System.runAs(clientUser )
            {
                test.startTest();
                System.currentPageReference().getParameters().put('invoices', '1ghsdjhasgdjhasg');
                System.currentPageReference().getParameters().put('favId' ,ef.ID);
                PEMEClientInvoiceDetailsCtrl PEMEClientInvoiceDetailsCntl = new PEMEClientInvoiceDetailsCtrl ();
                PEMEClientInvoiceDetailsCntl.loadData();
                PEMEClientInvoiceDetailsCntl.generateExams();
                PEMEClientInvoiceDetailsCntl.createOpenUserList();
                //PEMEClientInvoiceDetailsCntl.setSortDirection('ASC');
                PEMEClientInvoiceDetailsCntl.getSortDirection();
                PEMEClientInvoiceDetailsCntl.getExamOptions();
                PEMEClientInvoiceDetailsCntl.getObjectOptions();
                //PEMEClientInvoiceDetailsCntl.sortExpression='ASC';
                PEMEClientInvoiceDetailsCntl.firstOpen();
                PEMEClientInvoiceDetailsCntl.hasPreviousOpen=true;
                PEMEClientInvoiceDetailsCntl.previousOpen();
                PEMEClientInvoiceDetailsCntl.hasNextOpen=false;
                PEMEClientInvoiceDetailsCntl.nextOpen();
                PEMEClientInvoiceDetailsCntl.lastOpen();
                PEMEClientInvoiceDetailsCntl.setpageNumberOpen();
                PEMEClientInvoiceDetailsCntl.selectedExam=new list<String>();
                PEMEClientInvoiceDetailsCntl.selectedExam.add('checking');
                PEMEClientInvoiceDetailsCntl.selectedObject=new list<String>();
                PEMEClientInvoiceDetailsCntl.selectedObject .add('checking');
                PEMEClientInvoiceDetailsCntl.clearOptions();
                PEMEClientInvoiceDetailsCntl.print();
                PEMEClientInvoiceDetailsCntl.exportToExcel();
                PEMEClientInvoiceDetailsCntl.newFilterName='Demo Filter';
                PEMEClientInvoiceDetailsCntl.createFavourites();
                PEMEClientInvoiceDetailsCntl.fetchFavourites();
                PEMEClientInvoiceDetailsCntl.deleteFavourites();
                PEMEClientInvoiceDetailsCntl.selectedExam.add('test');
                PEMEClientInvoiceDetailsCntl.selectedObject.add('check');
                PEMEClientInvoiceDetailsCntl.generateExams();                                                     
            }
          //updateTestForGardUtil();
          //added for gardutil coverage ---- start
          acm_UpdateFor_gardUtil.Account__c = brokerAcc.ID;
          acm_UpdateFor_gardUtil.Active__c = true;
          acm_UpdateFor_gardUtil.Contact__c = brokerContact.ID;
          acm_UpdateFor_gardUtil.IsPeopleClaimUser__c = true;
          acm_UpdateFor_gardUtil.Administrator__c = 'Normal';
          insert acm_UpdateFor_gardUtil;
          //GardUtils.blockMultiCall = false;
          acm_UpdateFor_gardUtil.Administrator__c = 'Admin';
          acm_UpdateFor_gardUtil.IsPeopleClaimUser__c = false;
          update acm_UpdateFor_gardUtil;
          //added for gardutil coverage ---- stop
          test.stopTest();
          PEME_Exam_Detail__c ped = new PEME_Exam_Detail__c();
          ped = [SELECT Id,Status__c FROM PEME_Exam_Detail__c WHERE Id =: PExamDetail.Id];
          ped.Status__c = 'Rejected-Closed';
          update ped;
          //ped.status__c = 'Approved';
          //update ped;
          //ped.status__c = 'Rejected-Need Input';
          //update ped;
          
        }
        
    }
    
   /* @future
   static void updateTestForGardUtil()
    {
       //added for gardutil coverage ---- start
      GardUtils.flag = false;
      user.isActive = false;
      //@future
      update user;
      //added for gardutil coverage ---- stop    
    }*/