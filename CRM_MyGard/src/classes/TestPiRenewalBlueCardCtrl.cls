@isTest(seeAllData=false)
Public class TestPiRenewalBlueCardCtrl 
{
            
    public static void createTestData(){
        //Create test data.................     
        //correspondant contacts Custom Settings
          
        GardTestData test_Rec = new GardTestData();
        test_Rec.customsettings_rec();
        test_Rec.commonrecord();
        test_Rec.ViewDoc_testrecord();
        
        gardtestdata.junc_object_broker.client__c = gardtestdata.clientAcc.id;
        update gardtestdata.junc_object_broker;
        
        gardtestdata.junc_object_client.client__c = gardtestdata.clientAcc.id;
        update gardtestdata.junc_object_client;
        
        GardTestData.brokerDoc.Document_Type_Description__c = '\'WRC Blue Card\'';
        update GardTestData.brokerDoc;
        
        GardTestData.clientDoc.Document_Type_Description__c = '\'Bunkers Blue Card\'';
        update GardTestData.clientDoc;
        
        List<PIRenew_BlueCard_Document_Type__c> pirDocTypeLst = new List<PIRenew_BlueCard_Document_Type__c>();
        PIRenew_BlueCard_Document_Type__c pirDocType1 = new PIRenew_BlueCard_Document_Type__c(
                                                                                                Name = 'Bunkers Blue Card',
                                                                                                Value__c = 'BBC'
                                                                                            );
        pirDocTypeLst.add(pirDocType1);
        
        PIRenew_BlueCard_Document_Type__c pirDocType2 = new PIRenew_BlueCard_Document_Type__c(
                                                                                                Name = 'CLC Blue Card 1969',
                                                                                                Value__c = 'CLC69'
                                                                                            );
        pirDocTypeLst.add(pirDocType2);
        
        PIRenew_BlueCard_Document_Type__c pirDocType3 = new PIRenew_BlueCard_Document_Type__c(
                                                                                                Name = 'CLC Blue Card 1992',
                                                                                                Value__c = 'CLC92'
                                                                                            );
        pirDocTypeLst.add(pirDocType3);
        
        PIRenew_BlueCard_Document_Type__c pirDocType4 = new PIRenew_BlueCard_Document_Type__c(
                                                                                                Name = 'PLR Blue Card',
                                                                                                Value__c = 'PLR'
                                                                                            );                                                                                                                                                                        
        pirDocTypeLst.add(pirDocType4);
        
        PIRenew_BlueCard_Document_Type__c pirDocType5 = new PIRenew_BlueCard_Document_Type__c(
                                                                                                Name = 'WRC Blue Card',
                                                                                                Value__c = 'WRC'
                                                                                            );                                                                                                                                                                        
        pirDocTypeLst.add(pirDocType5);
        
        insert pirDocTypeLst;
        
        Blue_Card_renewal_availability__c testbluCardAvail = new Blue_Card_renewal_availability__c(
                                                                                                      Name = 'value',
                                                                                                      BlueCardRenewalAvailability__c = false
                                                                                                  );
        insert testbluCardAvail;
        
        PIrenewalAvailability__c testPIRenAvail = new PIrenewalAvailability__c(
                                                                                    Name = 'value',
                                                                                    Enable_Certificate__c = true,
                                                                                    Enable_Claims_review__c = true,
                                                                                    Enable_General_increase__c = true,
                                                                                    Enable_Documentation__c = true,
                                                                                    Enable_Portfolio_report__c = true,
                                                                                    Enable_Renewal_terms__c = true,
                                                                                    Enable_Renewal_Update__c = true,
                                                                                    PIRenewalAvailability__c = true
                                                                                );
        insert testPIRenAvail;
        
        List<PI_Renewal_Document_Download_Tracking__c> testDocDownTrackLst = new List<PI_Renewal_Document_Download_Tracking__c>();
        PI_Renewal_Document_Download_Tracking__c testDocDownTrack1 = new PI_Renewal_Document_Download_Tracking__c(
                                                                                                                     CompanyId__c = gardTestData.brokerAcc.Id,
                                                                                                                     DownloadBy__c = gardTestData.brokerContact.Id,
                                                                                                                     Document_External_Id__c = '1122324',
                                                                                                                     Document_id__c = gardTestData.junc_object_broker.ID,
                                                                                                                     Document_Name__c = 'docBroker',
                                                                                                                     Download_At__c = Date.today()
                                                                                                                 );
        testDocDownTrackLst.add(testDocDownTrack1);
        
        PI_Renewal_Document_Download_Tracking__c testDocDownTrack2 = new PI_Renewal_Document_Download_Tracking__c(
                                                                                                                     CompanyId__c = gardTestData.clientAcc.Id,
                                                                                                                     DownloadBy__c = gardTestData.clientContact.Id,
                                                                                                                     Document_External_Id__c = '1122324',
                                                                                                                     Document_id__c = gardTestData.junc_object_client.ID,
                                                                                                                     Document_Name__c = 'docClient',
                                                                                                                     Download_At__c = Date.today()
                                                                                                                 );
        testDocDownTrackLst.add(testDocDownTrack2);
        
        insert testDocDownTrackLst;
        
        BlueCard_webservice_endpoint__c testBWebEPoint = new BlueCard_webservice_endpoint__c(
                                                                                                Name = 'BlueCard_WS_endpoint',
                                                                                                Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/Documents/DocumentFetcher/GetDocumentService_ep'
                                                                                            );
        insert testBWebEPoint;
        
        DM_SF_File_Externsion_mapping__c testFileExtMap = new DM_SF_File_Externsion_mapping__c(
                                                                                                  Name = 'ACROBAT',
                                                                                                  File_Extension__c = 'pdf',
                                                                                                  SF_Mime_Type__c = 'application/pdf'
                                                                                              );
        insert testFileExtMap;
        
        MLC_certificate__c MLCObj = new MLC_certificate__c(Client__c = GardTestData.ClientAcc.Id,
                                                           CaseId__c = GardTestData.clientCase.Id,
                                                           Comment__c = 'test Comments',
                                                           Email__c = true,
                                                           Phone__c = false,
                                                           Reporter_Contact__c = GardTestData.ClientContact.Id,
                                                           Your_faithfully__c = 'Test Name'
                                                           
                                                           );
        insert MLCObj;
        
        MLC_Certificate_availability__c MLC_Certificate_CS_Obj = new MLC_Certificate_availability__c(Name = 'MLC availability', value__c = true);
        insert MLC_Certificate_CS_Obj ;
    }
    Public static testmethod void CoverPiRBCCtrl1(){
        createTestData();
        System.runAs(GardTestData.brokerUser)
        {
            Test.startTest();
            System.currentPageReference().getParameters().put('favId' ,GardTestData.test_fav_for_doc.ID);
            PiRenewalBlueCardCtrl testBroker = new PiRenewalBlueCardCtrl();
            
            testBroker.isPageLoad1 = true;
            /*testBroker.setClients = new Set<String>();
            testBroker.setClients.add(GardTestData.junc_object_broker.Agreement__r.client__r.Name);
            testBroker.prodArea = new List<String>();
            testBroker.prodArea.add(gardtestdata.piStr);
            testBroker.prodArea.add(gardtestdata.marineStr);
            testBroker.own_contract_id_set = new Set<String>();
            testBroker.own_contract_id_set.add(gardTestData.brokercontract_1st.ID);*/
            testBroker.selectedObj = new List<String>();
            testBroker.selectedObj.add(gardtestdata.test_object_1st.Name);
            testBroker.selectedBlueCard = new List<String>();
            testBroker.selectedBlueCard.add(gardtestdata.brokerDoc.Document_Type_Description__c);
            testBroker.selectedYear = new List<String>();
            testBroker.selectedYear.add(GardTestData.junc_object_broker.Document_Metadata__r.Year__c);
            testBroker.selectedProductArea = new List<String>();
            testBroker.selectedProductArea.add(gardtestdata.pistr);
            testBroker.selectLocalClient = GardTestData.clientAcc.Name;
            testBroker.selectedGlobalClient = new List<String>();
            testBroker.selectedGlobalClient.add(GardTestData.clientAcc.Name);
            /*testBroker.selectedObj = new List<String>();
            testBroker.selectedObj.add(gardtestdata.test_object_1st.Name);
            testBroker.selectedBlueCard = new List<String>();
            testBroker.selectedBlueCard.add(gardtestdata.brokerDoc.Document_Type_Description__c);
            testBroker.selectedYear = new List<String>();
            testBroker.selectedYear.add('2011');
            testBroker.selectedProductArea = new List<String>();
            testBroker.selectedProductArea.add(gardtestdata.pistr);
            testBroker.selectLocalClient = GardTestData.clientAcc.Name;
            testBroker.selectedGlobalClient = new List<String>();
            testBroker.selectedGlobalClient.add(GardTestData.clientAcc.Name);*/
            testBroker.getFilterCriteria();
            testBroker.testStr = GardTestData.junc_object_broker.ID;
            testBroker.selectedYear = new List<String>();
            testBroker.selectedYear.add('2011');
            testBroker.selectLocalClient = GardTestData.clientAcc.Name;
            /*testBroker.accountIdSet = new Set<String>();
            testBroker.accountIdSet.add(gardTestData.junc_object_broker.Document_Metadata__r.Client__r.ID);*/
            //testBroker.selectedYear.add(GardTestData.junc_object_broker.Document_Metadata__r.Year__c); 
            testBroker.userType = 'broker';
            //testBroker.orderByNew = gardtestdata.test_object_1st.Name;
            //testBroker.sortDirection = 'ASC';
            testBroker.populateDocuments();
            testBroker.print();
            testBroker.exportToExcel();
            testBroker.selectedGlobalClient = new List<String>();
            testBroker.selectedGlobalClient.add(GardTestData.clientAcc.Name);
            testBroker.getClientLst();
            testBroker.getObjectName();
            testBroker.getBlueCardLst();
            testBroker.isPageLoad1 = true;
            testBroker.getYearLst();
            testBroker.getProductLst();
            //testBroker.getFilterList();
            testBroker.filterRecords();
            String str = testBroker.getSortDirection();
            Pagereference pgRef;
            pgRef = testBroker.viewDocuments();
            testBroker.getShares();
            testBroker.setpageNumber();
            testBroker.navigate();
            testBroker.first();
            testBroker.next();
            testBroker.last();
            testBroker.previous();
            testBroker.newFilterName = 'testFilter';
            testBroker.createFavourites();
            testBroker.fetchFavourites();
            testBroker.deleteFavourites();
            testBroker.currentDocumentId = GardTestData.junc_object_broker.External_ID__c;
            testBroker.isDownload = 'true';
            testBroker.strDocId = GardTestData.junc_object_broker.ID;
            pgRef = testBroker.displayDoc();
            PgRef = testBroker.print();
            testBroker.att = gardTestData.attach;
            testBroker.deleteDoc();
            testBroker.requestEdit();
            testBroker.requestRenew();
            pgRef = testBroker.clearOptions();
            //PgRef = testBroker.forceDownload();
            pgRef = testBroker.fetchSelectedClients();
            PgRef = testBroker.exportToExcel();
            PgRef = testBroker.print();
            String strTest1 = PiRenewalBlueCardCtrl.getGenerateTimeStamp();
            String strTest2 = PiRenewalBlueCardCtrl.getGenerateTime();
            String strTest3 = PiRenewalBlueCardCtrl.getUserName();
            testBroker.selectedGlobalClient = new List<String>();
            testBroker.selectedGlobalClient.add(GardTestData.clientAcc.Name);
            testBroker.selectedGlobalClient.add(GardTestData.brokerAcc.Name);
            testBroker.isPageLoad1 = true;
            testBroker.getClientLst();
            testBroker.hasNext = true;
            testBroker.hasPrevious = true;
            testBroker.pageNumber = 11;
            testBroker.prvUrl = 'testing';
            testBroker.selectedRequest = 'testingReq';
            //testBroker.currentFileId = 'fl112234';
            Test.stopTest();
        }  
    } 
    Public static testmethod void CoverPiRBCCtrl2(){
        
        createTestData();
        System.runAs(GardTestData.clientUser)
        {
            System.currentPageReference().getParameters().put('favId' ,GardTestData.test_fav_for_doc.ID);
            PiRenewalBlueCardCtrl testClient = new PiRenewalBlueCardCtrl();
            Test.startTest();
            /*testClient.prodArea = new List<String>();
            testClient.prodArea.add(gardtestdata.piStr);
            testClient.prodArea.add(gardtestdata.marineStr);
            testClient.own_contract_id_set = new Set<String>();
            testClient.own_contract_id_set.add(gardTestData.clientContract_1st.ID);*/
            testClient.testStr = GardTestData.junc_object_client.ID;
            testClient.accountIdSet = new Set<String>();
            testClient.accountIdSet.add(gardTestData.junc_object_client.Document_Metadata__r.Client__r.ID);
            testClient.selectedYear = new List<String>();
            testClient.selectedYear.add(GardTestData.clientDoc.Year__c);
            testClient.populateDocuments();
            testClient.getClientLst();
            testClient.getObjectName();
            testClient.getBlueCardLst();
            testClient.getYearLst();
            testClient.getProductLst();
            testClient.getFilterList();
            testClient.filterRecords();
            String str = testClient.getSortDirection();
            Pagereference pgRef;
            pgRef = testClient.viewDocuments();
            testClient.getShares();
            testClient.setpageNumber();
            testClient.navigate();
            testClient.first();
            testClient.next();
            testClient.last();
            testClient.previous();
            testClient.newFilterName = 'testFilter';
            testClient.createFavourites();
            //testClient.fetchFavourites();
            testClient.deleteFavourites();
            testClient.currentDocumentId = GardTestData.junc_object_client.External_ID__c;
            //testClient.isDisplay = 'true';
            testClient.isDownload = 'true';
            testClient.strDocId = GardTestData.junc_object_client.ID;
            //pgRef = testClient.displayDoc();
            testClient.att = gardTestData.attachmnt;
            testClient.deleteDoc();
            testClient.requestEdit();
            testClient.requestRenew();
            pgRef = testClient.clearOptions();
            //PgRef = testClient.forceDownload();
            //pgRef = testClient.fetchSelectedClients();
            //PgRef = testClient.exportToExcel();
           // PgRef = testClient.print();
            String strTest1 = PiRenewalBlueCardCtrl.getGenerateTimeStamp();
            String strTest2 = PiRenewalBlueCardCtrl.getGenerateTime();
            //String strTest3 = PiRenewalBlueCardCtrl.getUserName();
            Test.stopTest();
        }        
    }       
}