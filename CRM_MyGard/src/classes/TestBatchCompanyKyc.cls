@isTest
private class TestBatchCompanyKyc  {
    
    Static testMethod void  BatchCompanyKyctest ()
    {
        PEME_Enrollment_Form__c pef = new PEME_Enrollment_Form__c();
        pef.Enrollment_Status__c = 'Enrolled';
        insert pef;
        
        PEME_Manning_Agent__c pma = new PEME_Manning_Agent__c();
        pma.PEME_Enrollment_Form__c = pef.id;
        insert pma;
        
        PEME_Debit_note_detail__c pbnd = new PEME_Debit_note_detail__c();
        pbnd.PEME_Enrollment_Form__c = pef.id;
        insert pbnd;
        
        //Insert Account
        Account account = new Account();    
        account.Name = 'Customer Account';
        account.Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().id;
        account.Market_Area__c = TestDataGenerator.getMarketArea().Id;    
        account.Synchronisation_Status__c = 'Synchronised';
        account.Billing_Address_Sync_Status__c = 'Synchronised';
        account.Shipping_Address_Sync_Status__c = 'Synchronised';
        account.Roles_Sync_Status__c  = 'Synchronised'; 
        account.Company_Status__c  = 'Active';
        account.Company_Role__c = 'Broker;'; 
        account.Manning_Agent_or_Debit_Note_ID__c = pma.id; 
        insert account;
        
        List<Account> accList = new List<Account>();
        accList.add(account);
        
        Test.startTest();
            BatchCompanyKyc job = new BatchCompanyKyc();
            Database.QueryLocator ql = job.start(null);
            job.execute(null,accList);
            job.Finish(null);
        Test.StopTest();
        Account newAccount = new Account();    
        newAccount.Name = 'Customer Account';
        newAccount.Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().id;
        newAccount.Market_Area__c = TestDataGenerator.getMarketArea().Id;    
        newAccount.Synchronisation_Status__c = 'Synchronised';
        newAccount.Billing_Address_Sync_Status__c = 'Synchronised';
        newAccount.Shipping_Address_Sync_Status__c = 'Synchronised';
        newAccount.Roles_Sync_Status__c  = 'Synchronised'; 
        newAccount.Company_Status__c  = 'Active';
        newAccount.Company_Role__c = 'Client'; 
        newAccount.Manning_Agent_or_Debit_Note_ID__c = pma.id; 
        newAccount.P_I_Member_Flag__c = false;
        insert newAccount;
        newAccount.P_I_Member_Flag__c = true;
        update newAccount;
        
    }
}