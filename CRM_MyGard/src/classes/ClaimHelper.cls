//later change all variable NAMEs from case to claim, e.g. lstCases to lstClaim
//later, line commened with "field_to_be_created" is to be uncommented
public class ClaimHelper {
    
    public static void updateGUID(List<Claim__c> lstCases){
        for(Claim__c cas : lstCases){
            Blob b = Crypto.GenerateAESKey(128);
            String h = EncodingUtil.ConvertTohex(b);
            String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20);
            System.debug('********* guid : '+guid);
            cas.guid__c = guid;
        }
    }
    
    //START - SF 3660
    //SF 3660
    public static void populateClaimPicklistFieldsForCase(Boolean isInsert, List<Claim__c> claims){
        //System.debug('sfEdit GardUtils.populateClaimPicklistFieldsForCase STARTS - ');
        //System.debug('sfEdit GardUtils.populateClaimPicklistFieldsForCase initial claims - '+claims);
        Set<String> claimPicklistValues = new Set<String>();
        //sonarqube update 2017R2- adding null check - replacing OR (||) with AND (&&)
        for(Claim__c c : claims){
            if(c.Alleged_Cause__c!=null || c.Alleged_Cause__c!=''){claimPicklistValues.add(c.Alleged_Cause__c);}                                                                                        
            if(c.Cargo_Claims_Category__c!=null || c.Cargo_Claims_Category__c!=''){claimPicklistValues.add(c.Cargo_Claims_Category__c);}                                    
            if(c.Carriage_Method_Dry_Cargo__c!=null || c.Carriage_Method_Dry_Cargo__c!=''){claimPicklistValues.add(c.Carriage_Method_Dry_Cargo__c);}               
            if(c.Cause_of_Injury__c!=null || c.Cause_of_Injury__c!=''){claimPicklistValues.add(c.Cause_of_Injury__c);}                                                                     
            if(c.Claim_Amount_Currency__c!=null || c.Claim_Amount_Currency__c!=''){claimPicklistValues.add(c.Claim_Amount_Currency__c);}                                 
            if(c.Conseq_Entered_Ship__c!=null || c.Conseq_Entered_Ship__c!=''){claimPicklistValues.add(c.Conseq_Entered_Ship__c);}                                               
            if(c.Crew_Title__c!=null || c.Crew_Title__c!=''){claimPicklistValues.add(c.Crew_Title__c);}                                                                                                
            if(c.Damaged_Object_Object_Type__c!=null || c.Damaged_Object_Object_Type__c!=''){claimPicklistValues.add(c.Damaged_Object_Object_Type__c);}        
            if(c.Human_Claim_Type__c!=null || c.Human_Claim_Type__c!=''){claimPicklistValues.add(c.Human_Claim_Type__c);}                                                       
            if(c.Conseq_Other_Ship__c!=null || c.Conseq_Other_Ship__c!=''){claimPicklistValues.add(c.Conseq_Other_Ship__c);}                                                        
            if(c.Details_defence_Specification__c!=null || c.Details_defence_Specification__c!=''){claimPicklistValues.add(c.Details_defence_Specification__c);}              
            if(c.Light__c!=null || c.Light__c!=''){claimPicklistValues.add(c.Light__c);}                                                                                                                         
            if(c.Location__c!=null || c.Location__c!=''){claimPicklistValues.add(c.Location__c);}                                                                                                          
            if(c.Manouver__c!=null || c.Manouver__c!=''){claimPicklistValues.add(c.Manouver__c);}                                                                                                   
            if(c.Manouver_Other_Ship__c!=null || c.Manouver_Other_Ship__c!=''){claimPicklistValues.add(c.Manouver_Other_Ship__c);}                                              
            if(c.Position__c!=null || c.Position__c!=''){claimPicklistValues.add(c.Position__c);}                                                                                                            
            if(c.Position_Other_Ship__c!=null || c.Position_Other_Ship__c!=''){claimPicklistValues.add(c.Position_Other_Ship__c);}                                                       
            //if(c.Type__c_of_Cargo_Claim__c!=null || c.Type__c_of_Cargo_Claim__c!=''){claimPicklistValues.add(c.Type__c_of_Cargo_Claim__c);}//later, field_to_be_created
            //if(c.Type__c_of_Damage__c!=null || c.Type__c_of_Damage__c!=''){claimPicklistValues.add(c.Type__c_of_Damage__c);}//later, field_to_be_created
            if(c.Visibility__c!=null || c.Visibility__c!=''){claimPicklistValues.add(c.Visibility__c);}                                                                                                          
            if(c.Claim_Specification__c!=null || c.Claim_Specification__c!=''){claimPicklistValues.add(c.Claim_Specification__c);} 
        }  
        
        //Run the following ONLY if the list is not empty
        if(claimPicklistValues.size() > 0){
            Map<String, String> claimPickListMap = new Map<String, String>();
            for(Claims_picklist_value__c cpv : [SELECT id, full_description__c from Claims_picklist_value__c where id in :claimPickListValues]){
                claimPickListMap.put(cpv.id, cpv.full_description__c);
            }
            for(Claim__c c : claims){
                if(c.Alleged_Cause__c!=null || c.Alleged_Cause__c!=''){c.Alleged_Cause_Text__c=claimPickListMap.get(c.Alleged_Cause__c);}else{c.Alleged_Cause_Text__c='';}
                if(c.Cargo_Claims_Category__c!=null || c.Cargo_Claims_Category__c!=''){c.Cargo_Claims_Category_Text__c=claimPickListMap.get(c.Cargo_Claims_Category__c);}else{c.Cargo_Claims_Category_Text__c='';}
                if(c.Carriage_Method_Dry_Cargo__c!=null || c.Carriage_Method_Dry_Cargo__c!=''){c.Carriage_Method_Dry_Cargo_Text__c=claimPickListMap.get(c.Carriage_Method_Dry_Cargo__c);}else{c.Carriage_Method_Dry_Cargo_Text__c='';}
                if(c.Cause_of_Injury__c!=null || c.Cause_of_Injury__c!=''){c.Cause_of_Injury_Text__c=claimPickListMap.get(c.Cause_of_Injury__c);}else{c.Cause_of_Injury_Text__c='';}
                if(c.Claim_Amount_Currency__c!=null || c.Claim_Amount_Currency__c!=''){c.Claim_Amount_Currency_Text__c=claimPickListMap.get(c.Claim_Amount_Currency__c);}else{c.Claim_Amount_Currency_Text__c='';}
                if(c.Conseq_Entered_Ship__c!=null || c.Conseq_Entered_Ship__c!=''){c.Conseq_Entered_Ship_Text__c=claimPickListMap.get(c.Conseq_Entered_Ship__c);}else{c.Conseq_Entered_Ship_Text__c='';}
                if(c.Crew_Title__c!=null || c.Crew_Title__c!=''){c.Crew_Title_Text__c=claimPickListMap.get(c.Crew_Title__c);}else{c.Crew_Title_Text__c='';}
                if(c.Damaged_Object_Object_Type__c!=null || c.Damaged_Object_Object_Type__c!=''){c.Damaged_Object_Object_Type_Text__c=claimPickListMap.get(c.Damaged_Object_Object_Type__c);}else{c.Damaged_Object_Object_Type_Text__c='';}
                if(c.Human_Claim_Type__c!=null || c.Human_Claim_Type__c!=''){c.Human_Claim_Type_Text__c=claimPickListMap.get(c.Human_Claim_Type__c);}else{c.Human_Claim_Type_Text__c='';}
                if(c.Conseq_Other_Ship__c!=null || c.Conseq_Other_Ship__c!=''){c.Conseq_Other_Ship_Text__c=claimPickListMap.get(c.Conseq_Other_Ship__c);}else{c.Conseq_Other_Ship_Text__c='';}
                if(c.Details_defence_Specification__c!=null || c.Details_defence_Specification__c!=''){c.Details_defence_Specification_Text__c=claimPickListMap.get(c.Details_defence_Specification__c);}else{c.Details_defence_Specification_Text__c='';}
                if(c.Light__c!=null || c.Light__c!=''){c.Light_Text__c=claimPickListMap.get(c.Light__c);}else{c.Light_Text__c='';}
                if(c.Location__c!=null || c.Location__c!=''){c.Location_Text__c=claimPickListMap.get(c.Location__c);}else{c.Location_Text__c='';}
                if(c.Manouver__c!=null || c.Manouver__c!=''){c.Manouver_Text__c=claimPickListMap.get(c.Manouver__c);}else{c.Manouver_Text__c='';}
                if(c.Manouver_Other_Ship__c!=null || c.Manouver_Other_Ship__c!=''){c.Manouver_Other_Ship_Text__c=claimPickListMap.get(c.Manouver_Other_Ship__c);}else{c.Manouver_Other_Ship_Text__c='';}
                if(c.Position__c!=null || c.Position__c!=''){c.Position_Text__c=claimPickListMap.get(c.Position__c);}else{c.Position_Text__c='';}
                if(c.Position_Other_Ship__c!=null || c.Position_Other_Ship__c!=''){c.Position_Other_Ship_Text__c=claimPickListMap.get(c.Position_Other_Ship__c);}else{c.Position_Other_Ship_Text__c='';}
                //if(c.Type__c_of_Cargo_Claim__c!=null || c.Type__c_of_Cargo_Claim__c!=''){c.Type__c_of_Cargo_Claim_Text__c=claimPickListMap.get(c.Type__c_of_Cargo_Claim__c);}else{c.Type__c_of_Cargo_Claim_Text__c='';}//later, field_to_be_created
                //if(c.Type__c_of_Damage__c!=null || c.Type__c_of_Damage__c!=''){c.Type__c_of_Damage_Text__c=claimPickListMap.get(c.Type__c_of_Damage__c);}else{c.Type__c_of_Damage_Text__c='';}//later, field_to_be_created
                if(c.Visibility__c!=null || c.Visibility__c!=''){c.Visibility_Text__c=claimPickListMap.get(c.Visibility__c);}else{c.Visibility_Text__c='';}
                //if(c.Claim_Specification__c!=null || c.Claim_Specification__c!=''){c.Claim_Specification_Text__c=claimPickListMap.get(c.Claim_Specification__c);}else{c.Claim_Specification_Text__c='';}//later, field_to_be_created
            }
        }
        //System.debug('sfEdit GardUtils.populateClaimPicklistFieldsForCase final claims - '+claims);
    }
    //END - SF 3660
    
    ////sf-3511 - start
    public static void updateClaimOwners(List<Claim__c> lstCases){
        try{
            update lstCases;
        }catch(Exception e){
            System.debug('Exception caught ClaimHelper.updateClaimOwners() - '+e.getMessage());
        }
    }
    //sf-3511 - end
    
    //deletes Cases which are in delete status
    @future
    public Static void deleteCase(Set<Id> setCaseId){
        List<Claim__c> caseList = new List<Claim__c>();
        for(Claim__c caseObj :[select Id, status__c from Claim__c where Id IN :setCaseId]){
            System.debug('***********in for*******');
            if(caseObj.status__c == 'Delete'){
                caseList.add(caseObj);
            }
        }
        System.debug('************CaseList'+caseList.size());
        if(caseList.size()>0){
            try{
                delete CaseList;
            }
            catch(Exception e){
                System.debug(e.getMessage());
            }
        }
    }
    
    //Added for SF-5015
    //if the owner is changed in the 'New' or 'Open' status, the status is changed to 'In Progress'
    public static void assignToMe_ServiceRequest(Map<Id,Claim__c> oldCaseMap,Map<Id,Claim__c> newCaseMap){
        if(oldCaseMap == null || newCaseMap == null) return;
        
        Claim__c newCase,oldCase;
        for(Id caseId : newCaseMap.keySet()){
            newCase = newCaseMap.get(caseId);
            oldCase = oldCaseMap.get(caseId);
            if( oldCase != null
                && (newCase.Status__c.equalsIgnoreCase('new')) 
                && (newCase.OwnerId != oldCase.OwnerId)
            ){
                newCase.Status__c = 'In Progress';
            }
        }
    }    
}