@isTest
public class TestUWRCharterersRenewalQuestionnnaire{
    public static Case uw;
    public static string testStr;
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    public TestUWRCharterersRenewalQuestionnnaire(){
        uw = new Case();
        testStr = 'test';
    }
    public static testMethod void CharterersRenewalQuestionnnaire(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        GardTestData test_data=new GardTestData();
        test_data.commonRecord();
        test_data.customsettings_rec();
        Case uwfDel = [SELECT Id FROM Case WHERE Id =: GardTestData.brokerCase.Id];
        delete uwfDel;
        test.startTest();
        system.runAs(GardTestData.brokerUser){
            UWRCharterersRenewalQuestionnaireCtrl test_broker = new UWRCharterersRenewalQuestionnaireCtrl();
            test_broker.uwf.Name_of_the_Assured__c = testStr;
            test_broker.uwf.expected_changes_for_upcoming_yr__c = testStr;
            test_broker.uwf.current_time_no_of_vessels__c = testStr;
            test_broker.uwf.current_voyage_no_of_vessels__c = testStr;
            test_broker.uwf.upcoming_time_no_of_vessels__c = testStr;
            test_broker.uwf.upcoming_voyage_no_of_vessels__c = testStr;
            test_broker.uwf.current_time_duration_of_charters__c = testStr;
            test_broker.uwf.current_voyage_duration_of_charters__c = testStr;
            test_broker.uwf.upcoming_time_duration_of_charters__c = testStr;
            test_broker.uwf.upcoming_voyage_duration_of_charters__c = testStr;
            test_broker.uwf.current_time_avg_vessel_size__c = testStr;
            test_broker.uwf.current_voyage_avg_vessel_size__c = testStr;
            test_broker.uwf.upcoming_time_avg_vessel_size__c = testStr;
            test_broker.uwf.upcoming_voyage_avg_vessel_size__c = testStr;
            test_broker.uwf.current_time_avg_vessel_age__c = testStr;
            test_broker.uwf.current_voyage_avg_vessel_age__c = testStr;
            test_broker.uwf.upcoming_time_avg_vessel_age__c = testStr;
            test_broker.uwf.upcoming_voyage_avg_vessel_age__c = testStr;
            test_broker.uwf.current_time_types_of_charterparties__c = testStr;
            test_broker.uwf.current_voyage_types_of_charterparties__c = testStr;
            test_broker.uwf.upcoming_time_types_of_charterparties__c = testStr;
            test_broker.uwf.upcoming_voyage_types_of_charterparties__c = testStr;
            test_broker.uwf.Current_Ins_Yr_Geography__c = testStr;
            test_broker.uwf.Upcoming_Ins_Yr_Geography__c = testStr;
            test_broker.uwf.Current_Ins_Yr_Cargo_Typ_and_MT_Vol__c = testStr;
            test_broker.uwf.Upcoming_Ins_Yr_Cargo_Typ_and_MT_Vol__c = testStr;
            test_broker.tempSavePage1();
            test_broker.fetchGlobalClients();
            test_broker.setGlobalClientIds.clear();
            List<SelectOption> clientOptions =  test_broker.getClientLst();
            test_broker.fetchSelectedClients();
            //String strClient = test_broker.getCName();
            system.debug('UWFId : '+test_broker.uwf.Id+' guid : '+test_broker.uwf.guid__c );
            PageReference pageRef = Page.UWRCharterersRenewalQuestionnaire1;
            Test.setCurrentPage(pageRef);
            String guidStr = [SELECT Id,guid__c FROM Case WHERE Id =: test_broker.uwf.Id].guid__c;
            ApexPages.CurrentPage().getParameters().put('id',guidStr);
            UWRCharterersRenewalQuestionnaireCtrl test_broker_draft = new UWRCharterersRenewalQuestionnaireCtrl();
            test_broker_draft.sendToForm();
            test_broker_draft.chkAccess();
            List<SelectOption> booleanOptions1 = test_broker_draft.getBooleanOptions();
            List<SelectOption> booleanOptions2 = test_broker_draft.getBooleanOpt();
            test_broker_draft.goPrevForFirstPage();
            test_broker_draft.goNextSecondPage();
            test_broker_draft.goNextThirdPage();
            test_broker_draft.goNextReviewPage();
            test_broker_draft.goPrevForSecondPage();
            test_broker_draft.goPrevForThirdPage();
            test_broker_draft.enable4d();
            test_broker_draft.enable5a();
            test_broker_draft.enable5b();
            test_broker_draft.enable5c();
            //test_broker.tempSavePage1();
            test_broker_draft.doNotCancel();
            test_broker_draft.toggleCancel();
            test_broker_draft.hideSavePopUp();
            test_broker_draft.saveRecordForAddlInfo();
            test_broker_draft.saveRecordNSubmit();
         }
         test.stopTest();
    }
     public static testMethod void CharterersRenewalQuestionnnaireClient(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        GardTestData test_data_Client=new GardTestData();
        test_data_Client.commonRecord();
        test_data_Client.customsettings_rec();
        Case uwfDelClient = [SELECT Id FROM Case WHERE Id =: GardTestData.clientCase.Id];
        delete uwfDelClient;
        
        system.runAs(GardTestData.clientUser){
        test.startTest();
            UWRCharterersRenewalQuestionnaireCtrl test_client = new UWRCharterersRenewalQuestionnaireCtrl();
            test_client.uwf.Name_of_the_Assured__c = testStr;
            test_client.uwf.expected_changes_for_upcoming_yr__c = testStr;
            test_client.uwf.current_time_no_of_vessels__c = testStr;
            test_client.uwf.current_voyage_no_of_vessels__c = testStr;
            test_client.uwf.upcoming_time_no_of_vessels__c = testStr;
            test_client.uwf.upcoming_voyage_no_of_vessels__c = testStr;
            test_client.uwf.current_time_duration_of_charters__c = testStr;
            test_client.uwf.current_voyage_duration_of_charters__c = testStr;
            test_client.uwf.upcoming_time_duration_of_charters__c = testStr;
            test_client.uwf.upcoming_voyage_duration_of_charters__c = testStr;
            test_client.uwf.current_time_avg_vessel_size__c = testStr;
            test_client.uwf.current_voyage_avg_vessel_size__c = testStr;
            test_client.uwf.upcoming_time_avg_vessel_size__c = testStr;
            test_client.uwf.upcoming_voyage_avg_vessel_size__c = testStr;
            test_client.uwf.current_time_avg_vessel_age__c = testStr;
            test_client.uwf.current_voyage_avg_vessel_age__c = testStr;
            test_client.uwf.upcoming_time_avg_vessel_age__c = testStr;
            test_client.uwf.upcoming_voyage_avg_vessel_age__c = testStr;
            test_client.uwf.current_time_types_of_charterparties__c = testStr;
            test_client.uwf.current_voyage_types_of_charterparties__c = testStr;
            test_client.uwf.upcoming_time_types_of_charterparties__c = testStr;
            test_client.uwf.upcoming_voyage_types_of_charterparties__c = testStr;
            test_client.uwf.Current_Ins_Yr_Geography__c = testStr;
            test_client.uwf.Upcoming_Ins_Yr_Geography__c = testStr;
            test_client.uwf.Current_Ins_Yr_Cargo_Typ_and_MT_Vol__c = testStr;
            test_client.uwf.Upcoming_Ins_Yr_Cargo_Typ_and_MT_Vol__c = testStr;
            test_client.uwfc = test_client.uwf;
            //test_client.uwf.case_id__c = GardTestData.clientCase.id;
            //test.startTest();
            Database.upsert(test_client.uwf);
            
            test_client.generatePdfToAttachWithMail();
            //test_client.tempSavePage1();
            test_client.saveRecord();
            test_client.fetchGlobalClients();
            test_client.setGlobalClientIds.clear();
            List<SelectOption> clientOptions =  test_client.getClientLst();
           
            
            test_client.fetchSelectedClients();
            //String strClient = test_client.getCName();
            system.debug('UWFId : '+test_client.uwf.Id+' guid : '+test_client.uwf.guid__c );
            PageReference pageRef = Page.UWRCharterersRenewalQuestionnaire1;
            Test.setCurrentPage(pageRef);
            String guidStr1 = [SELECT Id,guid__c FROM Case WHERE Id =: test_client.uwf.Id].guid__c;
            ApexPages.CurrentPage().getParameters().put('id',guidStr1);
            UWRCharterersRenewalQuestionnaireCtrl test_client_draft = new UWRCharterersRenewalQuestionnaireCtrl();
            test_client_draft.sendToForm();
            test_client_draft.chkAccess();
            List<SelectOption> booleanOptions3 = test_client_draft.getBooleanOptions();
            List<SelectOption> booleanOptions4 = test_client_draft.getBooleanOpt();
            test_client_draft.goPrevForFirstPage();
            test_client_draft.goNextSecondPage();
            test_client_draft.goNextThirdPage();
            test_client_draft.goNextReviewPage();
            test_client_draft.goPrevForSecondPage();
            test_client_draft.goPrevForThirdPage();
            test_client_draft.enable4d();
            test_client_draft.enable5a();
            test_client_draft.enable5b();
            test_client_draft.enable5c();
            //test_client.tempSavePage1();
            test_client_draft.doNotCancel();
            test_client_draft.toggleCancel();
            test_client_draft.hideSavePopUp();
            test_client_draft.saveRecordForAddlInfo();
            test_client_draft.saveRecordNSubmit();
           
         }
       test.stopTest();
    }
}