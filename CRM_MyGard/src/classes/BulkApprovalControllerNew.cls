public with sharing class BulkApprovalControllerNew {

    public BulkApprovalControllerNew(ApexPages.StandardController controller) {
        userid = UserInfo.getUserId();
        selectedOpps = new List<opp>();
        selectedOppsCount=0;
        selectedOppsPageSize = 10;
        selectedOppsPageNum = 0;
        selectAllChecked = false;
        budgetYear='2013';
        populateOpps();
    }

    public boolean selectAllChecked {get;set;}
    public void selectAll() {
        for (opp o : selectedOpps) {
            if (selectAllChecked)
                o.checked = true;
            else
                o.checked = false;
        }
    }
    public void prev() {
        if (selectedOppsPageNum>0)
            selectedOppsPageNum -= 1;
    }
    public void next() {
        if (selectedOppsPageNum < selectedOppsPageCount)
            selectedOppsPageNum += 1;
    }
    public class opp {
        public boolean checked {get;set;}
        public string Id {get;set;}
        public string Name {get;set;}
        public string AccountName {get;set;}
        public string Gard_Market_Area {get;set;}
        public string Budget_Status {get;set;}
        public string RecordType {get;set;}
        public decimal Actual_Premium {get;set;}
        public decimal Budget_Difference {get;set;}
        public decimal Renewable_Premium {get;set;}
        public decimal Record_Adjustment {get;set;}
        public decimal Volume_Change {get;set;}
        public decimal Renewal_Probability {get;set;}
        public decimal Forecast_Budget {get;set;}
        public decimal Approved_Budget {get;set;}
        public decimal BudgetPremium {get;set;}
        public string Type {get;set;}
        public string CurrencyISOCode{get;set;}
    }
    public string userid {get;set;}
    public List<opp> selectedOpps {get;set;}
    public List<opp> selectedOppsPage {get;set;}
    public decimal   selectedOppsPageNum {get;set;}
    public decimal   selectedOppsPageSize {get;set;}
    public decimal   selectedOppsPageCount {get;set;}
    public decimal   selectedOppsCount {get;set;}
    public string budgetYear {
        get {
            return budgetYear;}
        set {
            budgetYear = value;
            if (budgetYear!=budgetYearPrevious) {
                populateOpps();
                budgetYearPrevious = budgetYear;
            }
        }
    }
    public string budgetYearPrevious {get;set;}
    public List<opp> getSelOppsPage() {
        if (selectedOppsPageNum+1 < selectedOppsPageCount) {
            selectedOppsPage = null;
            selectedOppsPage = new List<opp>();
            for (integer i=0; i<integer.valueOf(selectedOppsPageSize) && i<integer.valueOf(selectedOppsCount);i++) {
                selectedOppsPage.add(selectedOpps[i+integer.valueOf(selectedOppsPageNum*selectedOppsPageSize)]);
            }
            return selectedOppsPage;        
        }
        else {
            selectedOppsPage = null;
            selectedOppsPage = new List<opp>();
            if (selectedOppsCount>0) {
                for (integer i=0; i<integer.valueOf(selectedOppsCount-(selectedOppsPageSize*selectedOppsPageNum));i++) {
                    selectedOppsPage.add(selectedOpps[i+integer.valueOf(selectedOppsPageNum*selectedOppsPageSize)]);
                }
            }
            return selectedOppsPage;
        }
        return null;
    }

    
    public PageReference OppPage(){
        PageReference oppPage = new pageReference ('/apex/OppProducts');
        oppPage.setRedirect(true);
        return oppPage;
    }    
    public void populateOpps() {
        Set<String> oIds = new Set<String>();
        Set<ID>  stRecipients = new Set<ID>();
        List<ID> lstRecipients = new List<ID>();
        selectedOpps = new List<opp>();
        List<Opportunity> opps = [Select Id, Name 
                                    from Opportunity 
                                   where budget_status__c = 'Budget In Progress'
                                     and StageName = 'Renewable Opportunity'
                                     and Budget_Year__c = :budgetYear
                                     and OwnerId = :userId];

        for (Opportunity setopp : opps) {
            oIds.add(setopp.Id);
        }
        for (Opportunity o : [Select Id, Name, RecordType.Name, Account.Name, Gard_Market_Area__c, Type, Budget_Status__c, Actual_Premium__c, Renewable_Premium__c, Budget_Premium__c, Budget_Amount__c,
                                     Renewal_Premium__c, Renewal_Adjustment_Record__c, Area_Manager__c, Budget_Variance__c, Amount,
                                     Renewal_Adjustment_Volume_Change__c, Renewal_Adjustment_Probability__c, Forecast_Budget__c, CurrencyIsoCode
                                from Opportunity 
                               where Id in :oIds 
                            Order By Name ASC]) {
            opp tempopp = new opp();
            tempopp.checked = selectAllChecked;
            tempopp.id = o.id;
            tempopp.name = o.name;
            tempopp.AccountName = o.Account.Name;
            tempopp.Gard_Market_Area = o.Gard_Market_Area__c;
            tempopp.Budget_Status = o.Budget_Status__c;
            tempopp.RecordType = o.RecordType.Name;
            tempopp.Actual_Premium = o.Actual_Premium__c != null ? o.Actual_Premium__c.setScale(0): 0;
            tempopp.Renewable_Premium = o.Renewable_Premium__c != null ? o.Renewable_Premium__c.setScale(0): 0;
            tempopp.Record_Adjustment = o.Renewal_Adjustment_Record__c;
            tempopp.Volume_Change = o.Renewal_Adjustment_Volume_Change__c;
            tempopp.Renewal_Probability = o.Renewal_Adjustment_Probability__c;
            tempopp.Forecast_Budget = o.Forecast_Budget__c;
            tempopp.Approved_Budget = o.Budget_Amount__c;
            tempopp.Type = o.Type;
            tempopp.BudgetPremium = o.Budget_Premium__c != null ? o.Budget_Premium__c.setScale(0): 0;
            tempopp.CurrencyIsoCode = o.CurrencyIsoCode;
            if(o.Renewable_Premium__c>0){
                tempopp.Budget_Difference = (((o.Budget_Premium__c-o.Renewable_Premium__c)/o.Renewable_Premium__c)*100).setScale(2);
            }else{
                //if the amount is zero we can't find the difference ratio
                tempopp.Budget_Difference = 0;
            }
            selectedOpps.add(tempopp);
        }
        selectedOppsCount = selectedOpps.size();
        selectedOppsPageNum = 0;
        if (selectedOppsCount>0 && selectedOppsPageSize>0)
            selectedOppsPageCount = (selectedOppsCount/selectedOppsPageSize).round(system.roundingMode.CEILING);
        else
            selectedOppsPageCount=0;
    }
    public void submitOpps(List<opportunity> opps) {
        Set<String> oIds = new Set<String>();
        Set<ID>  stRecipients = new Set<ID>();
        List<ID> lstRecipients = new List<ID>();
        integer iSize = opps.size();
        if (iSize > 0) {
            for (Opportunity setopp : opps)
                oIds.add(setopp.Id);
            
            List<Opportunity> lstOpp = [Select Id, Name, Area_Manager__c, Budget_Status__c from Opportunity where Id in :oIds];
            try
            {
                list<Approval.ProcessSubmitRequest> reqList = new list<Approval.ProcessSubmitRequest>();
                for (Opportunity opp : lstOpp)
                {
                    approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                    if (opp.Budget_Status__c == 'Budget In Progress')    // Only submit opportunities which are set to Budget In Progress
                    {
                        req1.setObjectId(opp.id);
                        reqList.add(req1);
                        if (opp.Area_Manager__c != null)
                            stRecipients.Add(opp.Area_Manager__c);
                    }
                }
                system.debug('#### reslist: '+reqList);
                approval.ProcessResult[] result = Approval.process(reqList, True);
                
                // We'll only get to here if the submission was successful so send the notification email(s)
                for (String recip : stRecipients)        // Put the unique set of recipient IDs into a List 
                    lstRecipients.add(recip);
//                sendEmailNotification(lstRecipients);    // Pass the recipients list to the email method to send the notification emails
            }
            catch (System.DmlException e)
            {
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    //System.debug(e.getDmlMessage(i)); 
                    if (e.getDmlMessage(i) == 'No applicable approval process found.')
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'At least one of these records does not meet the entry criteria or initial submitters of any active approval processes. Please contact your administrator for assistance.'));
                    else
                        system.debug(e.getDmlMessage(i));
                }
            }
        }
    }
    public void submitAll() {
        Set<String> oIds = new Set<String>();
        Set<ID>  stRecipients = new Set<ID>();
        List<ID> lstRecipients = new List<ID>();
        //integer iSize = myController.getResultSize();
        //if (iSize > 0) {
            //myController.setPageSize(1000);
            //List<Opportunity> opps = (List<Opportunity>)myController.getRecords();
            //submitOpps(opps);
        //}
    }
    public void submitSelected() {
        Set<string> ids = new Set<string>();
        for (opp o : selectedOpps) {
            if (o.checked)
                ids.add(o.Id);
        }
        List<Opportunity> opps = [Select Id from Opportunity where Id in :ids];
        submitOpps(opps);
        populateOpps();
    }
    public void sendEmailNotification(List<ID> recips)
    {
        String sTemplateId = [Select Id from EmailTemplate where Name = 'Submit Opportunity Budget Notification' LIMIT 1].Id;
        for (ID recipid : recips)
        {
            //Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEMailMessage();
            mail.setTargetObjectId(recipid);
            mail.saveAsActivity = false;
            mail.setTemplateId(sTemplateId);
            //if (UserInfo.getUserId() == '00520000001Zd4jAAC' || UserInfo.getUserId() == '00520000001JnOyAAK')    // used for testing
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    //### TEST METHODS ###
    @IsTest(SeeAllData=true) public static void testBulkApprovalController2() {
        //set up test records
        Account acc = [Select Id from Account where Type = 'Customer' AND Area_Manager__c <> null LIMIT 1]; 
        Opportunity opp1 = new Opportunity(Name='test opp 1',Budget_Status__c = 'Budget In Progress', StageName = 'Renewable Opportunity', CloseDate = System.today(), AccountId = acc.Id);
        Opportunity opp2 = new Opportunity(Name='test opp 2',Budget_Status__c = 'Budget In Progress', StageName = 'Renewable Opportunity', CloseDate = System.today(), AccountId = acc.Id);
        List<Opportunity> lopp = new List<Opportunity>();
        lopp.add(opp1);
        lopp.add(opp2);
        insert(lopp);
        
        //set up the test records which will trigger the exception (exclude record adjustment from one of the oppty records)
        Opportunity opp3 = new Opportunity(Name='test opp 3',Budget_Status__c = 'Budget In Progress', StageName = 'Under Approval - AM', CloseDate = System.today(), AccountId = acc.Id);
        Opportunity opp4 = new Opportunity(Name='test opp 4',Budget_Status__c = 'Budget In Progress', StageName = 'Under Approval - AM', CloseDate = System.today(), AccountId = acc.Id);
        List<Opportunity> lopp2 = new List<Opportunity>();
        lopp2.add(opp3);
        lopp2.add(opp4);
        insert(lopp2);
        
        PageReference pageRef = Page.BulkApproval;
        Test.setCurrentPage(pageRef);
/*        
        // controller for valid opportunities
        List<Opportunity> lopp_test = new List<Opportunity>();
        lopp_test = [Select Name,Budget_Status__c, StageName, CloseDate, AccountId from Opportunity where Id = :opp1.Id OR Id = :opp2.Id];
        ApexPages.StandardController sc = new ApexPages.StandardController();
        BulkApprovalController2 bac = new BulkApprovalController2(sc);

        // controller for opportunities which don't satisfy approval entry criteria
        List<Opportunity> lopp2_test = new List<Opportunity>();
        lopp2_test = [Select Name,Budget_Status__c, StageName, CloseDate, AccountId from Opportunity where Id = :opp3.Id OR Id = :opp3.Id];
        ApexPages.StandardSetController sc2 = new ApexPages.StandardSetController(lopp2_test);
        BulkApprovalController2 bac2 = new BulkApprovalController2(sc2);
        
        //execute test case
        Test.StartTest();
            bac.submitAll();
            System.Assert([Select Budget_Status__c from Opportunity where Id = :opp1.Id].Budget_Status__c == 'Under Approval - AM');  // validate that the approval process has changed the budget status
    
            //test with an opportunity which fails entry criteria, this ensures coverage of the exception code
            bac2.submitAll();
            BulkApprovalController2 bac2_b = new BulkApprovalController2(sc2);
            System.Assert([Select Budget_Status__c from Opportunity where Id = :opp3.Id].Budget_Status__c == 'Budget In Progress');  // validate that the status stays the same (i.e. thew approval process hasn't changed the status)
            
        Test.StopTest();
*/        
    }
    
}