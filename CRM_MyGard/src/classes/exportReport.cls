public with sharing class exportReport {


    public String getFilename() {
        String encodedFilename = ApexPages.currentPage().getParameters().get('fileName');
        encodedFilename = encodedFilename.replaceAll('[\\:*?\"<>|\\,\\&\\$]', '');

             //System.debug('FileName---'+encodedFilename);            
       return EncodingUtil.urlDecode(encodedFilename, 'UTF-8');
       
     }

    public String getReportData() {
        Id reportId = (Id)ApexPages.currentPage().getParameters().get('id');
        String pemeId = (String)ApexPages.currentPage().getParameters().get('pv0');
        if(reportId != null && reportId.getSobjectType() == Report.SobjectType) {
            //PageReference ref = new PageReference('/'+reportId+'?pv0='+pemeId+'&excel=1');
             PageReference ref = new PageReference('/'+reportId+'?pv0='+pemeId+'&export=1&enc=UTF-8&xf=xls&isdtp=p1');
            //https://cs85.salesforce.com/00OD0000006tXOv?pv0=PI-0000001245&export=1&enc=UTF-8&xf=xls&fileName=TestAsdfQwerty.xls
            //System.debug(logginglevel.error,ref.geturl());
            //System.debug('response---->'+ref.getContent().toString());
            if(!test.isRunningTest()){
            return ref.getContent().toString();
            }
        }
        return null;
    }
}