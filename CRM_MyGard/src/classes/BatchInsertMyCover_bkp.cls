global class BatchInsertMyCover_bkp implements Database.Batchable<sObject> {
    
    //Declaring variables.
    global String strquery;
    global String  strTrueOrFalse;
   
    
    global BatchInsertMyCover_bkp (String truefalse){                  
        
        strTrueOrFalse = truefalse;
        
     
         
                    
                   
        strquery = 'SELECT id,Company_Role__c FROM Account where ispartner=true  ';                                                              
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){               
        return Database.getQueryLocator(strquery);        
    }
    
    /**
     * Implementation
     *
     * @return : null
     */
     global void execute(Database.BatchableContext BC, List<account> scope){  
     
      Boolean blBrokerView;
      List<MyCoverList__c> lstMyCover = new List<MyCoverList__c>();
    
      List<MyCoverList__c> delLstMyCover= new List<MyCoverList__c>();
      List<MyCoverList__c> delLstMyCoverAll = new List<MyCoverList__c>();
        try{
            
            
               //      system.debug('***************delLstMyCoverAll -->'+delLstMyCoverAll.size());
            //delete delLstMyCover;            
           // Database.delete( delLstMyCoverAll, false);
            /*List<Id> lstUserContactId= new List<Id>();
            for(User usr:scope){
                lstUserContactId.add(usr.contactID);
            }
            List<Contact> lstContact = new List<Contact>();
            if(lstuserId != null && lstuserId.size()>0)
                lstContact = [Select AccountId from Contact Where id IN : lstUserContactId];
                
            List<Account> lstAccount = new List<Account>();
            for(Contact con:lstContact){
                lstAccount.add(con.AccountId);
            }*/
            
            for(account acc:scope){
               Contact myContact;
                   /* if(usr!= null && usr.contactID != null){
                        myContact = [Select AccountId from Contact Where id=: usr.contactID];
                    } */
                   // Account acc;
                    List<Object__c> lstObject = new List<Object__c>();
                    List<Contract> lstContract = new List<Contract>();
                    List<Contract> lstContractClient = new List<Contract>();        
                    
                  /*  if(myContact != null && myContact.AccountId != null){
                        acc = [Select ID,Company_Role__c from Account Where id =: myContact.AccountId ]; */       
                        if(acc != null && acc.Company_Role__c != null &&  acc.Company_Role__c.contains('Broker')){
                            blBrokerView = true;
                           /* lstContract = [Select Id, Client__c from Contract Where Broker__c =: acc.id];
                            Set<ID> setClientID = new Set<ID>();
                            for(Contract contract: lstContract){
                                setClientID.add(contract.Client__c);
                            }*/
                          //  lstContractClient = [Select id from Contract Where Client__c IN : setClientID];
                           lstContractClient = [Select Id, Client__c from Contract Where Broker__c =: acc.id];
                            if(Boolean.valueof(strTrueOrFalse)==true){
                        delLstMyCover = [Select id from MyCoverList__c Where broker_id__c=:acc.id and onrisk__c=true];
                    }else{
                        delLstMyCover = [Select id from MyCoverList__c Where broker_id__c=:acc.id and onrisk__c=false];
                    }
                        }else{
                            blBrokerView = false;          
                            lstContractClient = [Select id from Contract Where Client__c =: acc.id AND broker__c=null ];
                           if(Boolean.valueof(strTrueOrFalse)==true){
                        delLstMyCover = [Select id from MyCoverList__c Where client_id__c=:acc.id and broker_id__c=null  and onrisk__c=true];
                    }else{
                        delLstMyCover = [Select id from MyCoverList__c Where client_id__c=:acc.id and broker_id__c=null and onrisk__c=false];
                    } 
                        }
                    //}
                    for(MyCoverList__c mcl : delLstMyCover){
                        delLstMyCoverAll.add(mcl);
                    }
                    
                    
                    Set<ID> setCoverID = new Set<ID>();
                    for(Contract obj : lstContractClient){
                        setCoverID.add(obj.ID);
                    }
                    system.debug('setCoverID'+setCoverID.size());
                    /*
                    List<Asset> lstRisk = new List<Asset>();
                    if(setCoverID.size()>0){
                        lstRisk = [Select id from Asset Where Agreement__c IN : setCoverID LIMIT 2000];
                    }
                    
                    setRiskID = new Set<ID>();
                    for(Asset rsk : lstRisk ){
                        setRiskID.add(rsk.ID);
                    }      
                    system.debug('*****setRiskID*****'+setRiskID);*/
            
                    //showMyCover =true;
                    string strforClient = '';
                    string strforClientGroupby = '';
                    
                   // if(blBrokerView == true){
                        //strforClient = 'Object__r.Client__r.Name clientname,';
                        //strforClientGroupby = 'Object__r.Client__r.Name,';
                        strforClient = 'Agreement__r.Client__r.Name clientname, Agreement__r.Client__r.Id clientId,';
                        strforClientGroupby = 'Agreement__r.Client__r.Name,Agreement__r.Client__r.Id,';
                  //  }
                    List<Boolean> lstblOnrisk = new List<Boolean>();
                    lstblOnrisk.add(Boolean.valueof(strTrueOrFalse));
                    String strCondition = ' AND On_risk_indicator__c IN : lstblOnrisk ';
                    
                    
                    
                    
                    
                    
                    
                    String sSoqlQuery = 'SELECT MAX(Expiration_Date__c) expdate,agreement__c AgId, '+strforClient+' UnderwriterNameTemp__c underwriter, UnderwriterId__c underwriterId,Product_Name__c prod,Agreement__r.Business_Area__c busarea,'+ 
                                        'Agreement__r.Broker__r.Name AggName,Agreement__r.Broker__r.Id AggId, Agreement__r.Policy_Year__c plcyyr, '+
                                        'Claims_Lead__c claim, Gard_Share__c grdshr, On_risk_indicator__c rskind,Agreement__r.Shared_With_Client__c sharedwithclient,  '+
                                        'Count(id) FROM Asset Where Agreement__c IN : setCoverID '+strCondition+
                                        'GROUP BY agreement__c,'+strforClientGroupby+' UnderwriterNameTemp__c, UnderwriterId__c, Product_Name__c, '+
                                                 'Agreement__r.Business_Area__c,'+
                                                 'Agreement__r.Broker__r.Name,Agreement__r.Broker__r.Id,'+
                                                 'Agreement__r.Policy_Year__c,'+
                                                 'Claims_Lead__c, Gard_Share__c, On_risk_indicator__c,Agreement__r.Shared_With_Client__c ';
                                                 
                    AggregateResult[] groupedResults = database.query(sSoqlQuery);
                    system.debug('**********groupedResults --> '+groupedResults);
                    
                  
                    
                   
                    
                    system.debug('**********groupedResults --> '+groupedResults); 
                    for (AggregateResult ar : groupedResults)  {
                            if(blBrokerView == false){
                                lstMyCover.add(new MyCoverList__c(
                                                     AgreementId__c = string.valueof(ar.get('AgId')),
                                                     client__c=string.valueof(ar.get('clientname')),
                                                     client_id__c=string.valueof(ar.get('clientId')),
                                                     underwriter__c=string.valueof(ar.get('underwriter')),
                                                     UnderwriterId__c=string.valueof(ar.get('underwriterId')),
                                                     product__c=string.valueof(ar.get('prod')),
                                                     productarea__c=string.valueof(ar.get('busarea')),
                                                     broker__c=string.valueof(ar.get('AggName')),
                                                     broker_id__c=string.valueof(ar.get('AggId')), 
                                                     expirydate__c=string.valueof(ar.get('expdate')),
                                                     policyyear__c=Integer.valueof(ar.get('plcyyr')),                                           
                                                     claimslead__c=string.valueof(ar.get('claim')),
                                                     gardshare__c=Double.valueof(ar.get('grdshr')),
                                                     onrisk__c=Boolean.valueof(ar.get('rskind')),
                                                     viewobject__c=Integer.valueOf(ar.get('expr0')),
                                                     Shared_With_Client__c=Boolean.valueof(ar.get('sharedwithclient'))
                                                   //  userId__c = usr.Id
                                                 ));
                            }else{
                                lstMyCover.add(new MyCoverList__c(
                                                                 AgreementId__c = string.valueof(ar.get('AgId')),
                                                                 client__c=string.valueof(ar.get('clientname')),
                                                                 client_id__c=string.valueof(ar.get('clientId')),
                                                                 underwriter__c=string.valueof(ar.get('underwriter')),
                                                                 UnderwriterId__c=string.valueof(ar.get('underwriterId')),
                                                                 product__c=string.valueof(ar.get('prod')),
                                                                 productarea__c=string.valueof(ar.get('busarea')),
                                                                 broker__c=string.valueof(ar.get('AggName')),
                                                                 broker_id__c=string.valueof(ar.get('AggId')),
                                                                 expirydate__c=string.valueof(ar.get('expdate')),
                                                                 policyyear__c=Integer.valueof(ar.get('plcyyr')),                                           
                                                                 claimslead__c=string.valueof(ar.get('claim')),
                                                                 gardshare__c=Double.valueof(ar.get('grdshr')),
                                                                 onrisk__c=Boolean.valueof(ar.get('rskind')),
                                                                 viewobject__c=Integer.valueOf(ar.get('expr0')),
                                                                 Shared_With_Client__c=Boolean.valueof(ar.get('sharedwithclient'))
                                                               //  userId__c = usr.Id
                                                               
                                                  ));
                            }
                        
                     }
                    
            
            
            
            
            }
            system.debug('***************lstMyCover -->'+lstMyCover.size());
               Database.delete( delLstMyCoverAll, false);
               
               
            //insert lstMyCover;
          Database.insert( lstMyCover, false);
          
            system.debug('***************lstMyCover_AfterInsert -->'+lstMyCover.size());
           // system.debug('***************delLstMyCoverAll_After_delete -->'+delLstMyCoverAll.size());         
                            
        }catch( Exception ex){ 
            system.debug('***********exception: ' +ex);           
        }
      
    } 
    
    global void finish(Database.BatchableContext BC){ 
    
    }
}