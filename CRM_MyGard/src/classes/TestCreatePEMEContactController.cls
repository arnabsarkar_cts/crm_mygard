@istest
public class TestCreatePEMEContactController {
    @istest static void test()
    {
        
        //GardTestData gdInstance = new GardTestData();
        //gdInstance.PEMEClinicSubmitInvoiceCtrl();
        string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        
        Gard_Contacts__c gc = new Gard_Contacts__c(FirstName__c ='test1',lastName__c = 'test1');
        insert gc;
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrcClient);
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        vrcList.add(vrcBroker);
        Valid_Role_Combination__c vrcClinic = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
        vrcList.add(vrcClinic);
        insert vrcList;
        
        User salesforceLicUser = new User(
            Alias = 'standt', 
            profileId = salesforceLicenseId ,
            Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8',
            CommunityNickname = 'test13',
            LastName='Testing',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',  
            TimeZoneSidKey='America/Los_Angeles',
            UserName='test008@testorg.com',
            isActive = true ,
            ContactId__c = gc.id,
            City= 'Arendal'
        );
        insert salesforceLicUser;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'abc120000');
        insert Markt;  
        
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA');
        insert country;
        
        Account clinicAcc = new Account( Name='testClinic',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.ESP_Contact_Record_Type,
                                        company_Role__c='External Service Provider',
                                        Sub_Roles__c = 'ESP - Agent',
                                        Site = '_www.ctsclinic.se',
                                        Type = 'Surveyor' ,
                                        Area_Manager__c = salesforceLicUser.id,      
                                        Market_Area__c = Markt.id,
                                        PEME_Enrollment_Status__c='Enrolled',
                                        Country__c = country.id                                                                  
                                       );
        insert clinicAcc;
        
        Account clientAcc= New Account( Name = 'Testt',
                                       recordTypeId = System.Label.Client_Contact_Record_Type,
                                       Site = '_www.test.se',
                                       Type = 'Customer',
                                       BillingStreet = 'Gatan 1',
                                       BillingCity = 'Stockholm',
                                       BillingCountry = 'SWE',
                                       Area_Manager__c = salesforceLicUser.id,        
                                       Market_Area__c = Markt.id, 
                                       PEME_Enrollment_Status__c='Enrolled',
                                       Company_Role__c = 'Client',        
                                       Sub_Roles__c = '',
                                       Country__c = country.id
                                      );
        insert clientAcc; 
        
        Contact clinicContact = new Contact( 
            FirstName='clinicContact',
            LastName='clinicContactLN',
            MailingCity = 'London',
            MailingCountry = 'United Kingdom',
            MailingPostalCode = 'SE1 1AD',
            MailingState = 'London',
            MailingStreet = '1 London Road',
            AccountId = clinicAcc.Id,
            Email = 'test5554@noSite.abc'
        );
        insert clinicContact;
        
        PEME_Enrollment_Form__c pef = new PEME_Enrollment_Form__c ();
        pef.ClientName__c=clientAcc.ID;
        pef.Comments__c='Report analyzed';
        pef.Enrollment_Status__c= 'Draft';
        pef.Gard_PEME_References__c='Test References,  ';
        pef.Last_Status_Change_Date__c=Date.valueOf('2016-01-27');
        pef.Submitter__c=clinicContact.ID;
        pef.DisEnrolled_Expire_Date__c=System.today().adddays(3);
        insert pef;
        
        PEME_Manning_Agent__c pmg=new PEME_Manning_Agent__c  ();
        pmg.Client__c=clinicAcc.ID;
        pmg.PEME_Enrollment_Form__c=pef.ID;
        pmg.Company_Name__c='Test Company';
        pmg.Contact_Point__c=clinicContact.Id;
        pmg.Status__c='Approved';
        insert pmg;
        System.debug('pmg--GG--'+pmg.id);
        Apexpages.currentPage().getParameters().put('00ND00000035lm3',pmg.id);
        system.debug('Apexpages.currentPage().getParamet--GG'+Apexpages.currentPage().getParameters().put('00ND00000035lm3',pmg.id));
        CreatePEMEContactController createPemeCont = new CreatePEMEContactController();
        createPemeCont.onLoad();
        Apexpages.currentPage().getParameters().put('00ND00000035llZ',pmg.id);
        CreatePEMECompanyController CreatePEMEComp = new CreatePEMECompanyController();
        CreatePEMEComp.onLoad();
    }
}