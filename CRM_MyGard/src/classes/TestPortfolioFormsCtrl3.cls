@isTest(seeAllData=false)
Public class TestPortfolioFormsCtrl3
{
      public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
      public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
      public static Blob b;
      public static Attachment attachment1 , att;
      public static void createTestData(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        att = new Attachment();
        gardtestdata test_rec= new gardtestdata();
        test_rec.commonrecord();
        test_rec.customsettings_rec();
        Forms_List__c Forms=new Forms_List__c(Type__c='Claim forms');
        insert Forms;
        SelectedClientId_Forms__c formId = new SelectedClientId_Forms__c(Name=gardtestdata.brokerContact.id,SelectedClientId__c=gardtestdata.brokerAcc.id);
        insert(formId);  
        
        b = Blob.valueOf('Brokers Data');  
        attachment1 = new Attachment();  
        attachment1.ParentId = Forms.Id;
        attachment1.Name = 'Application form - Charterers.pdf';  
        attachment1.Body = b;  
        insert(attachment1); 
    }
        //Blue_Card_renewal_availability__c CSBCAvailability = new Blue_Card_renewal_availability__c(name = 'value', BlueCardRenewalAvailability__c = true); 
        //insert CSBCAvailability; 
        //Invoking methods of Portfolioformsctrl for broker and client users.......
   static testmethod void PortfolioFormsCtrl1(){
        createTestData();
        System.runAs(gardtestdata.BrokerUser)         
        {
            Test.startTest();
            PortfolioFormsCtrl client_user = new PortfolioFormsCtrl();
            client_user.lstContact = new List<Contact>();
            client_user.caseId = gardtestdata.clientCase.id;
           // client_user.currentFileId = String.valueof(attachment.id);
            client_user.selectedGlobalClient.add(gardtestdata.clientAcc.Id);
            List<SelectOption> clientOptions =  client_user.getClientLst();
            //broker_user.strGlobalClient=brokerAcc.id+';'+brokerAcc.id;
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment1';
            Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body1');
            client_user.attachment.body=bodyBlob2;
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment11';
            Blob bodyBlob3=Blob.valueOf('Unit Test Attachment Body11');
            client_user.attachment.body=bodyBlob3;
            
            //client_user.uwFormType = 'uw-ship-owner-form-option';
            //client_user.uploadFileForAll();
            //client_user.uwFormType = 'uw-char-vesl-form-option';
            //client_user.uploadFileForAll();
            client_user.uwFormType = 'uw-mou-form-option';
            client_user.uploadFileForAll();
            client_user.displayDoc();
            
            
            
            
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment 12';
            Blob bodyBlob12a=Blob.valueOf('Unit Test Attachment Body 12');
            client_user.attachment.body=bodyBlob12a;
            client_user.uwFormType = 'uw-lou-loi-MOU-option';
            client_user.uploadFileForAll();


            
            
            
            
            
            System.assertEquals(client_user.isSuccess , true);
            client_user.strSelected=gardtestdata.clientAcc.id+';'+gardtestdata.clientAcc.id; 
           // client_user.fetchSelectedClients();
            client_user.sendToForm();
            PortfolioFormsCtrl.NewAttachmentWrapper newobj = new PortfolioFormsCtrl.NewAttachmentWrapper('originalName', 'base64Value', att);
            client_user.currentFileId = 'Application form - Charterers.pdf';
            client_user.mapIdForms.put('Application form - Charterers.pdf',attachment1.id);
           // client_user.forceDownload(); 
            Test.stopTest();
        }
    }
    static testmethod void PortfolioFormsCtrl3(){
        createTestData();
        System.runAs(gardtestdata.clientUser)         
        {
            Test.startTest();
            PortfolioFormsCtrl client_user = new PortfolioFormsCtrl();
            //added on 15th march
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment1';
            Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body1');
            client_user.attachment.body=bodyBlob2;
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment11';
            Blob bodyBlob3=Blob.valueOf('Unit Test Attachment Body11');
            client_user.attachment.body=bodyBlob3;
            //end
            //client_user.uwFormType = 'uw-ship-owner-form-option';
            //client_user.uploadFileForAll();
            client_user.uwFormType = 'uw-charter-client-form-option';
            //client_user.uploadFileForAll();
            client_user.uwFormType = 'uw-mou-form-option';
            client_user.uploadFileForAll();
            
       
            Test.stopTest();
        }
    }
    static testmethod void PortfolioFormsCtrl4(){
        createTestData();
        System.runAs(gardtestdata.clientUser)         
        {
            Test.startTest();
            PortfolioFormsCtrl client_user = new PortfolioFormsCtrl();
            //added on 15th march
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment1';
            Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body1');
            client_user.attachment.body=bodyBlob2;
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment11';
            Blob bodyBlob3=Blob.valueOf('Unit Test Attachment Body11');
            client_user.attachment.body=bodyBlob3;
            //end
            
            client_user.uwFormType = 'uw-lou-loi-option'; 
            //client_user.uploadFileForAll();
            client_user.uwFormType = 'uw-marine-layup-form-option'; 
            client_user.uploadFileForAll();
             //client_user.uwFormType = 'uw-lou-loi-option'; 
            //client_user.uploadFileForAll();
            
            Test.stopTest();
        }
    }
   
    static testmethod void PortfolioFormsCtrl2(){
        createTestData();
        System.runAs(gardtestdata.brokerUser)  
        {
            Test.startTest();
            PortfolioFormsCtrl client_user=new PortfolioFormsCtrl();   
            PortfolioFormsCtrl broker_user = new PortfolioFormsCtrl();
            //added on 15th march
            broker_user.attachment =new Attachment();
            broker_user.attachment.Name='Unit Test Attachment1';
            Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body1');
            broker_user.attachment.body=bodyBlob2;
            broker_user.attachment =new Attachment();
            broker_user.attachment.Name='Unit Test Attachment11';
            Blob bodyBlob3=Blob.valueOf('Unit Test Attachment Body11');
            broker_user.attachment.body=bodyBlob3;
            //end
            broker_user.checkCustomSetting();
            broker_user.uwFormType='uw-mlc-certificate';
            //broker_user.uploadFileForAll();
            broker_user.uwFormType = 'Renewal-Questionnaire-Form';
            client_user.uwFormType = 'uw-for-free-pass-option'; 
            client_user.selectedFromType='Application form ship owners';
            broker_user.uploadFileForAll();
            //broker_user.downloadForm();
            Test.stopTest();       
        } 
    }
    
    static testmethod void PortfolioFormsCtrl5(){
        createTestData();
        System.runAs(gardtestdata.clientUser)         
        {
            Test.startTest();
            PortfolioFormsCtrl client_user = new PortfolioFormsCtrl();
            //added on 15th march
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment1.pdf';
            Blob bodyBlob2=Blob.valueOf('Unit Test Attachment Body1');
            client_user.attachment.body=bodyBlob2;
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment11.pdf';
            Blob bodyBlob3=Blob.valueOf('Unit Test Attachment Body11');
            client_user.attachment.body=bodyBlob3;
            //end
            client_user.uwFormType = 'uw-for-free-pass-option'; 
            client_user.uploadFileForAll();
            client_user.selectedFromType='uw-charter-client-form-option';
            //client_user.uploadFileForAll();
            Test.stopTest();
        }
    }
    
}