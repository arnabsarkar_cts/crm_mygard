@isTest
public class TestFavouritesCtrl
{
        public static List<Extranet_Favourite__c> Fav_list=new List<Extranet_Favourite__c>();
        public static List<User> UserList=new List<User>();
        public static List<Account> AccountList=new List<Account>();
        public static List<Contact> ContactList=new List<Contact>();
        public static Extranet_Favourite__c test_fav;
        public static User brokerUser,clientUser, salesforceLicUser;
        public static Account clientAcc, brokerAcc;
        public static Contact clientContact, brokerContact;
        Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name= 'Partner Community Login User Custom' limit 1].id;
        Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        Public static object__c test_object;
        Public static List<object__c> ObjectList=new List<object__c>();
        Public static case brokerCase,clientCase;
        Public static List<case> caseList= new List<case>();
        Public static Extranet_Global_Client__c global_client;
        ///////*******Variables for removing common literals********************//////////////
        public static string enUsrStr = 'en_US';
        public static string mailngStateStr = 'London';
        /////**********************end**************************/////
         public static testmethod void MyFavourite()
         {
           
           Gardtestdata grdtest = new Gardtestdata();
             grdtest.commonRecord();
             grdtest.customsettings_rec();
          //correspondant contacts Custom Settings
        /*List<ContactSubscriptionFields__c> conFieldsList = new List<ContactSubscriptionFields__c>();
        ContactSubscriptionFields__c conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        ContactSubscriptionFields__c conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        ContactSubscriptionFields__c conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;
         salesforceLicUser = new User(
                                            Alias = 'standt', 
                                            profileId = salesforceLicenseId ,
                                            Email='standarduser@testorg.com',
                                            EmailEncodingKey='UTF-8',
                                            CommunityNickname = 'test13',
                                            LastName='Testing',
                                            LanguageLocaleKey= enUsrStr ,
                                            LocaleSidKey= enUsrStr ,  
                                            TimeZoneSidKey='America/Los_Angeles',
                                           // contactId__c=gard_contact.id,
                                            UserName='test008@testorg.com'
                                           );
        insert salesforceLicUser;
          
       //Accounts............
       brokerAcc = new Account(  Name='testre',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    recordTypeId=System.Label.Broker_Contact_Record_Type,
                                    Site = '_www.cts.se',
                                    Type = 'Broker',
                                    //Market_Area__c = Markt.id,
                                    Area_Manager__c = salesforceLicUser.id                                                                       
                                 );
       AccountList.add(brokerAcc);
    
      
       clientAcc = new Account( Name = 'Test_1', 
                                        Site = '_www.test_1.se', 
                                        Type = 'Client', 
                                        BillingStreet = 'Gatan 1',
                                        BillingCity = 'Stockholm', 
                                        BillingCountry = 'SWE', 
                                        BillingPostalCode = 'BS1 1AD',
                                        recordTypeId=System.Label.Client_Contact_Record_Type,
                                       // Market_Area__c = Markt.id,
                                        Area_Manager__c = salesforceLicUser.id 
                                       );
                                    
        AccountList.add(clientAcc); 
        insert AccountList; 
        //Contacts.................
        brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity =  mailngStateStr ,
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState =  mailngStateStr ,
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(brokerContact) ;

        clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity =  mailngStateStr ,
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState =  mailngStateStr ,
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        insert ContactList;
        
        //Users..................
        brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='mdjawedm@gmail.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing',
                                    LanguageLocaleKey= enUsrStr ,
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey= enUsrStr ,  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id
                                   );
        UserList.add(brokerUser);
                                   
        clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey= enUsrStr ,
                                    LocaleSidKey= enUsrStr ,
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
                
         UserList.add(clientUser) ;
         insert UserList;
         
         */
          AccountToContactMap__c   map_client = new AccountToContactMap__c(AccountId__c= gardtestdata.clientAcc.id,Name=gardtestdata.clientContact.id);
         
         
         Insert map_client; 
         
          test_object = New Object__c( Dead_Weight__c= 20,
                             Object_Unique_ID__c = '1234lkolk1',
                             Object_Type__c='Bulk Vessel',
                             Imo_Lloyds_No__c='435556',
                             Name='Curia' );
         ObjectList.add(test_object);
         insert ObjectList ;
         test.startTest();
         brokerCase = new Case(Accountid = gardtestdata.brokerAcc.id, 
                                    ContactId = gardtestdata.brokerContact.id, 
                                    Claim_Incurred_USD__c= 40000, 
                                    Claim_Reference_Number__c='123ert',                                    
                                    Object__c= test_object.id,
                                    Claim_Type__c='Crew',
                                    Event_Date__c=date.valueof('2015-02-03')
                                   // Risk_Coverage__c = brokerAsset.id,
                                    //Contract_for_Review__c = contract.id,
                                    //Claims_handler__c = brokerContact.id
                                   );
        caseList.add(brokerCase);
        
         clientCase = new Case(Accountid = gardtestdata.clientAcc.id, 
                                   ContactId = gardtestdata.clientContact.id,
                                   Claim_Incurred_USD__c= 40000, 
                                   Claim_Reference_Number__c='123ert',
                                   Object__c= test_object.id,
                                   //Risk_Coverage__c = clientAsset.id,
                                   Claim_Type__c='Cargo',
                                   Event_Date__c=date.valueof('2015-02-03')
                                   //Claims_handler__c = clientContact.id
                                  );
        caseList.add(clientCase); 
        insert caseList; 
          global_client=new Extranet_Global_Client__c(           
                                                   Selected_By__c=gardtestdata.clientUser.id,
                                                   Selected_Global_Clients__c=gardtestdata.clientAcc.id
                                                   );
                                                                               
          insert global_client;                                                                     
         
         test_fav=new Extranet_Favourite__c(Claim__c=gardtestdata.clientCase.id,
                                             Client__c=gardtestdata.clientAcc.id,
                                             Favourite_By__c=gardtestdata.clientUser.id,
                                             Filter_Claim_Type__c='crew',
                                             Filter_Claim_Year__c ='2014',  
                                             Filter_Client__c='Borealis Maritime Limited',
                                             Filter_Cover__c='P&I Cover',
                                             Filter_Document_Type__c='type 1',
                                             Filter_Global_Client_Id__c=gardtestdata.clientAcc.id,
                                             Filter_Global_Client__c='Golden Ocean Management As',
                                             Filter_Object__c=test_object.id,
                                             Filter_On_Risk__c='Y',
                                             Filter_Policy_Year__c='2014',
                                             Filter_Product_Area__c='P&I',
                                             Item_Id__c=clientCase.id,
                                             Item_Name__c='testGARD',
                                             Item_Type__c='Saved Search',
                                             Object__c=test_object.id, 
                                             Favourite_For_Broker_Client__c = gardtestdata.clientAcc.id, 
                                             Saved_Query__c=' SELECT Agreement__r.Client__r.Name,Object__r.Id, Object__r.Name, Object__r.Object_Type__c, UnderwriterNameTemp__c,UnderwriterId__c, Product_Name__c,Agreement__r.Business_Area__c, Agreement__r.Broker__r.Name , Expiration_Date__c , Agreement__r.Policy_Year__c , Claims_Lead__c , Gard_Share__c , On_risk_indicator__c,Object__r.Dummy_Object_flag__c FROM Asset Where Agreement__c IN : setCoverID AND On_risk_indicator__c IN : lstblOnrisk ORDER BY Object__r.Object_Type__c ASC limit 10000'
                                              );
         Fav_list.add(test_fav);
         insert Fav_list;
         
         System.runAs(Gardtestdata.clientUser)
         {
            //test.startTest();
            FavouritesCtrl test_user= new FavouritesCtrl();
            test_user.fetchFavourites();
            System.assert(Fav_list.size() > 0 , true);
            test_user.removeFavourites();
            string pageurl='005L0000002TxlmIAC';
            FavouritesCtrl.fetchPageName( pageurl);
            string pageName='abc';
            FavouritesCtrl.fetchExistingFavsList(pageName);
            string favId='005L0000002TxlmIAC'; 
            FavouritesCtrl.fetchFavsListById(favId);
            Extranet_Favourite__c objFav=new Extranet_Favourite__c(Filter_Client__c='Borealis Maritime Limited');
            List<Extranet_favourite__c> test_fav1=new list<Extranet_favourite__c>();
            test_fav1=[Select id,Filter_Client__c from Extranet_Favourite__c ];
            String Page_Name='search';
            String itemName='item';
            String itemType='type1';
            String currentUserId='005L0000002TxlmIAC';
            String query='SELECT Agreement__r.Client__r.Name,Object__r.Id, Object__r.Name, Object__r.Object_Type__c, UnderwriterNameTemp__c,UnderwriterId__c, Product_Name__c,Agreement__r.Business_Area__c, Agreement__r.Broker__r.Name , Expiration_Date__c , Agreement__r.Policy_Year__c , Claims_Lead__c , Gard_Share__c , On_risk_indicator__c,Object__r.Dummy_Object_flag__c FROM Asset Where Agreement__c IN : setCoverID AND On_risk_indicator__c IN : lstblOnrisk ORDER BY Object__r.Object_Type__c ASC limit 10000';
            FavouritesCtrl.createFavs(objFav,page_Name,itemName,itemType,currentUserId,query,test_fav1);
            List<String> listItems= new List<String>();
            listItems.add('abcd');
            FavouritesCtrl.generateFavFilters(listItems);
            string item='test1';
            FavouritesCtrl.fetchFavFilters(item);
            //test.stopTest();
         }
        test.stopTest();       
       }                                     
     }