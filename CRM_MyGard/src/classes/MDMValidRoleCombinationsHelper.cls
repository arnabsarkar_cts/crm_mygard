/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 22/10/2013
***************************************************************************/
    
/// <summary>
///  This helper class provides methods to determine whether account roles \ sub role combinations are valid and to create the DMD role records
/// </summary>
public without sharing class MDMValidRoleCombinationsHelper {

    private Map<String, Map<String, Valid_Role_Combination__c>> ValidRoleCombinations;
    private Map<String, String> ValidRoleCombinationsBySubRole;
    public Map<id, MDM_Role__c> MDMRolesOldMap = new Map<id, MDM_Role__c>();
    public Map<id, MDM_Role__c> MDMRolesNewMap = new Map<id, MDM_Role__c>();
    private void PopulateValidRoleCombinations() {
        if (ValidRoleCombinations == null) {
            ValidRoleCombinations = new Map<String, Map<String, Valid_Role_Combination__c>>();
            for (Valid_Role_Combination__c v : [SELECT id, Name, Role__c, Sub_Role__c FROM Valid_Role_Combination__c]) {
                if (ValidRoleCombinations.get(v.role__c) == null) {
                    ValidRoleCombinations.put(v.role__c, new Map<String, Valid_Role_Combination__c>());
                }
                String subRole = v.sub_Role__c;
                if(subRole == null || subRole == ''){
                    subRole = '';
                }
                ValidRoleCombinations.get(v.role__c).put(subRole, v);
            }
        }
    }
    
    private void PopulateValidRoleCombinationsBySubRole(){
        if(ValidRoleCombinationsBySubRole== null){
            PopulateValidRoleCombinations();
            ValidRoleCombinationsBySubRole = new Map<String, String>();
            for(String aRole: ValidRoleCombinations.keySet()){
                for(String aSubRole: ValidRoleCombinations.get(aRole).keySet()){
                    ValidRoleCombinationsBySubRole.put(aSubRole, aRole);                    
                }
            }   
        }       
    }
    
    private List<Valid_Role_Combination__c> FindValidRoleCombinations(Account account) {
        return FindValidRoleCombinations(
            account.Company_Role__c != null ? new Set<String>(account.Company_Role__c.split(';')) : new Set<String>() , 
            account.Sub_Roles__c != null ? new Set<String>(account.Sub_Roles__c.split(';')) : new Set<String>() );
    }
    
    private List<Valid_Role_Combination__c> FindValidRoleCombinations(Set<String> roles, Set<String> subroles) {
        List<Valid_Role_Combination__c> vRoles = new List<Valid_Role_Combination__c>();
        for (String role : roles) {
            //cDecisions MG 31/10/2013 first check for a lone role without any sub roles as a valid combinatio e.g. client
            if(ValidRoleCombinations.get(role) != null && ValidRoleCombinations.get(role).get('') != null){
                vRoles.add(ValidRoleCombinations.get(role).get(''));
            }

            for (String subrole : subroles) {
                if (ValidRoleCombinations.get(role) != null && ValidRoleCombinations.get(role).get(subrole) != null) {
                    vRoles.add(ValidRoleCombinations.get(role).get(subrole));
                }
            }
        }
        return vRoles;
    }
    
    
    public Map<String, String> FindInvalidRoleCombinations(Set<String> roles, Set<String> subroles) {
        PopulateValidRoleCombinationsBySubRole();
        Map<String, String> invalidRoles = new Map<String, String>();
        Set<String> uncheckedRoles = new Set<String>(roles);
        
        //run through all sub roles and check that the correct parent role exists
        for(String subrole : subroles){
            //CRM - 144
            //null checking added for CRM 144, since for rejection the role 
            //remains unapproved. So role and subrole = ''.
            if(subrole != ''){ 
                if(ValidRoleCombinationsBySubRole.get(subrole) != null){
                    if(!roles.contains(ValidRoleCombinationsBySubRole.get(subrole))){
                        //we have an invalid sub role
                        invalidRoles.put(subRole, 'Missing Parent Role');
                    } else {
                        uncheckedRoles.remove(ValidRoleCombinationsBySubRole.get(subrole));
                    }
                }
            }
        }
        
        if (uncheckedRoles.size() > 0) {
            //System.debug('uncheckedRoles-->' + uncheckedRoles);
            //System.debug('uncheckedRoles.size()-->' + uncheckedRoles.size());
            PopulateValidRoleCombinations();
            //System.debug('ValidRoleCombinations-->' + ValidRoleCombinations);
            
            for (String role : uncheckedRoles) {
                //CRM - 144
                //null checking added for CRM 144, since for rejection the role 
                //remains unapproved. So role and subrole = ''.
                if(role != ''){
                    if (ValidRoleCombinations.get(role) == null || ValidRoleCombinations.get(role).get('') == null) {
                        //System.debug('Role-->' + role);
                        //System.debug('ValidRoleCombinations.get(role)-->' + ValidRoleCombinations.get(role));
                        invalidRoles.put(role, 'Missing valid subrole');
                    }
                }
            }
        }
        
        return invalidRoles;
    }
    
    public void CheckInvalidRoleCombinations(List<Account> accounts){
        
        for(Account account : accounts){
            if(!Account.deleted__c && !Account.Inactivate_Company_Request__c){
                System.debug('*** Account Roles: ' + account.Company_Role__c + ', SubRoles : ' + account.Sub_Roles__c);
                Set<String> roles = account.Company_Role__c != null ? new Set<String>(account.Company_Role__c.split(';')) : new Set<String>();
                Set<String> unapprovedroles = account.Unapproved_Company_Roles__c != null ? new Set<String>(account.Unapproved_Company_Roles__c.split(';')) : new Set<String>();
                Set<String> subroles = account.Sub_Roles__c != null ? new Set<String>(account.Sub_Roles__c.split(';')) : new Set<String>();
                Set<String> unapprovedsubroles = account.Sub_Roles_Unapproved__c != null ? new Set<String>(account.Sub_Roles_Unapproved__c.split(';')) : new Set<String>();
                roles.addall(unapprovedroles);
                subroles.addall(unapprovedsubroles);
                System.debug('account.Awaiting_Approval__c-->' + account.Awaiting_Approval__c);
                    
                    if (account.createdDate == account.lastmodifiedDate){ //This means its creation process, ref:CRM 144 
                        if(roles.size() == 0){
                            account.addError(System.Label.MDM_Role_Required);        
                        }
                    }
                    
                Map<String, String> invalidRoles = new Map<String, String>();
                invalidRoles = FindInvalidRoleCombinations(roles,subroles);
                String errorMessage = '';
                for(String invalidRole : invalidRoles.keySet()){
                    errorMessage += invalidRole + ' - ' + invalidRoles.get(invalidRole) + '. ';
                }
                if(invalidRoles.keySet().size()>0){
                    account.addError(errorMessage);
                }
            }
        }
    }
    
    
    //public void SetValidRoleCombinations(List<Account> accounts) { //commented out due to SF-3803
    
    public List<MDM_Role__c> SetValidRoleCombinations(List<Account> accounts) { ////commented in due to SF-3803
        system.debug('SetValidRoleCombinations accounts--> '+accounts);
        PopulateValidRoleCombinations();
        
        Map<id, List<Valid_Role_Combination__c>> accountValidRoleCombinations = new Map<id, List<Valid_Role_Combination__c>>();
        
        for (Account a : accounts) {
            if(a.Company_Role__c != null){
                accountValidRoleCombinations.put(a.id, FindValidRoleCombinations(a));
            } else {
                accountValidRoleCombinations.put(a.id, new List<Valid_Role_Combination__c>());
            }
        }
        
        List<MDM_Role__c> mdmRoles = new List<MDM_Role__c>();
        
        Map<id, Map<id, MDM_Role__c>> existingMDMRoles = new Map<id, Map<id, MDM_Role__c>>();
        Map<id, MDM_Role__c> foundRoles = new Map<id, MDM_Role__c>();
        
        
        for (MDM_Role__c mdmRole : [SELECT id, Valid_Role_Combination__c, Account__c FROM MDM_Role__c WHERE Account__c IN : accountValidRoleCombinations.keySet() AND Active__c = true]) {
            if (existingMDMRoles.get(mdmRole.Account__c) == null) {
                existingMDMRoles.put(mdmRole.Account__c, new Map<id, MDM_Role__c>());
            }
            existingMDMRoles.get(mdmRole.Account__c).put(mdmRole.Valid_Role_Combination__c, mdmRole);
            foundRoles.put(mdmRole.id, mdmRole);
        }
        system.debug('existingMDMRoles-->'+existingMDMRoles);
        system.debug('foundRoles-->'+foundRoles);
        for (id accountId : accountValidRoleCombinations.keySet()) {
            for (Valid_Role_Combination__c pr : accountValidRoleCombinations.get(accountId)) {
                if (existingMDMRoles.get(accountId) == null || existingMDMRoles.get(accountId).get(pr.id) == null) {
                    mdmRoles.add(new MDM_Role__c ( Valid_Role_Combination__c = pr.id, Account__c = accountId));
                    system.debug('to be inserted-in if-'+mdmRoles);
                } else {
                    foundRoles.remove(existingMDMRoles.get(accountId).get(pr.id).id);
                    system.debug('foundRoles--else part--'+foundRoles);
                }
            }
        }
        this.MDMRolesOldMap = foundRoles;
        //cDecisions MG 30/10/2013 Update the found roles remaining to set them as inactive (a logical deletion)
        for(MDM_Role__c aRole: foundRoles.values()){
            aRole.Active__c = false;
            aRole.Role_Modified__c = false;
            aRole.Inactivated_Date__c = System.now();
            //MG Do we need to mark the record as On_Risk__c = false?
        }
        system.debug('to be inserted--'+mdmRoles);
        insert mdmRoles;
        system.debug('inserted--'+mdmRoles);
        //cDecisions MG 30/10/2013 changed from delete to update to apply logical deletion
        if(!foundRoles.isEmpty()) {
            update foundRoles.values();
        }
        this.MDMRolesNewMap = foundRoles;
        system.debug('foundRoles (update) -- >'+foundRoles.values());
        //added for role call out merge - Arpan
        List<String> roleIdList = new List<String>();
        for(MDM_Role__c roleId : mdmRoles){
            roleIdList.add(roleId.Id);
            system.debug('--inserted Ids--'+roleId.Id);
        }
        for(MDM_Role__c roleId : foundRoles.values()){
            roleIdList.add(roleId.Id);
            system.debug('--updated Ids--'+roleId.Id);
        }
        List<MDM_Role__c> rolesForCallOut = [SELECT Id,Role_Modified__c from MDM_Role__c WHERE Id IN: roleIdList];
        for(MDM_Role__c roleId : rolesForCallOut){
            roleId.Role_Modified__c = true;
        }
        system.debug('rolesForCallOut-->'+rolesForCallOut);
        //commented out
        if(rolesForCallOut != null && rolesForCallOut.size()>0) {
            update rolesForCallOut;
        }
        //added for role call out merge - Arpan
        return mdmRoles; //added for SF - 3803
        
    }
}