@isTest
public class MDMProxyTests extends MDMProxy {
    
    public static void setupMDMConfigSettings() {
        setMDMConfig();
        setMDMServiceFields();
        setMDMNamespaces();
        createCustomSettingsValue();
    }
    //for correspodant contact edit trigger
    public static void createCustomSettingsValue(){
        List<ContactSubscriptionFields__c> conFieldsList = new List<ContactSubscriptionFields__c>();
        ContactSubscriptionFields__c conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        ContactSubscriptionFields__c conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        ContactSubscriptionFields__c conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;
    }
    //--END--
    public static void setMDMConfig () {
        List<MDMConfig__c> mdmConfig = (List<MDMConfig__c>)Test.loadData(MDMConfig__c.sObjectType, 'MDMConfig');
    }
    
    public static void setMDMServiceFields() {
        List<MDM_Service_Fields__c> serviceFields = (List<MDM_Service_Fields__c>)Test.loadData(MDM_Service_Fields__c.sObjectType, 'MDMServiceFields');
    }
    
    public static void setMDMNamespaces() {
        List<MDM_Namespaces__c> serviceNamespaces = (List<MDM_Namespaces__c>)Test.loadData(MDM_Namespaces__c.sObjectType, 'MDMNamespaces');
    }
    
    private Dom.XmlNode generateMDMResponse(string objectId, boolean success, DOM.XMLNode mdmChangeResponse, String mdmNS, String mdmNSPrefix) {
        //<mdm1:mdmResponse>
        DOM.XMLNode mdmResponse = mdmChangeResponse.addChildElement('mdmResponse', mdmNS, null);
        
        //<mdm1:success>true</mdm1:success>
        DOM.XMLNode mdmSuccess = mdmResponse.addChildElement('success', mdmNS, null);
        mdmSuccess.addTextNode(String.valueOf(success)); 
        
        //<mdm1:id>003c000000Lke3hAAB</mdm1:id>
        DOM.XMLNode mdmId = mdmResponse.addChildElement('id', mdmNS, null);
        mdmId.addTextNode(String.valueOf(objectId));
        
        return mdmResponse;
    }
    
    public Dom.Document generateMDMServiceResponse(string objectId, boolean success) {
        System.debug('*** Setting Mock = ' + String.valueOf(success));
        return generateMDMServiceResponse(new Map<string, Boolean> { objectId => success });
    }
    
    public Dom.Document generateMDMServiceResponse(Map<string, boolean> successMap) {
        
        DOM.Document xmlDoc = new DOM.Document();

        //<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
        DOM.XmlNode envelope = xmlDoc.createRootElement('Envelope', SOAPNS, SOAPNS_Prefix); 
        //<soapenv:Header xmlns:par="http://www.gard.no/cdm/v1_0/partner" xmlns:con="http://www.gard.no/cdm/v1_0/contact" xmlns:sour="http://www.gard.no/cdm/v1_0/source" xmlns:role="http://www.gard.no/cdm/v1_0/role" xmlns:obj="http://www.gard.no/cdm/v1_0/object" xmlns:add="http://www.gard.no/cdm/v1_0/address" xmlns:mdm="http://www.gard.no/mdm/v1_0/mdmmessage"/>
        DOM.XMLNode soapHeader = envelope.addChildElement('Header', SOAPNS, null);
        //<soapenv:Body xmlns:par="http://www.gard.no/cdm/v1_0/partner" xmlns:con="http://www.gard.no/cdm/v1_0/contact" xmlns:sour="http://www.gard.no/cdm/v1_0/source" xmlns:role="http://www.gard.no/cdm/v1_0/role" xmlns:obj="http://www.gard.no/cdm/v1_0/object" xmlns:add="http://www.gard.no/cdm/v1_0/address" xmlns:mdm="http://www.gard.no/mdm/v1_0/mdmmessage">
        DOM.XMLNode soapBody = envelope.addChildElement('Body', SOAPNS, null);
        for(MDM_Namespaces__c n : MDM_Namespaces__c.getAll().values()){
            soapHeader.setNamespace(n.name,n.namespace__c);
            soapBody.setNamespace(n.name,n.namespace__c);
        }   
        
        //<mdm1:mdmChangeResponse xmlns:mdm1="http://www.gard.no/mdm/v1_0/mdmpartner">
        String mdmNS = 'http://www.gard.no/mdm/v1_0/mdmpartner';
        String mdmNSPrefix = 'mdm1';
        DOM.XMLNode mdmChangeResponse = soapBody.addChildElement('mdmChangeResponse', mdmNS, mdmNSPrefix);
        
        for (string objectId : successMap.keySet()) {
            generateMDMResponse(objectId, successMap.get(objectId), mdmChangeResponse, mdmNS, mdmNSPrefix);
        }
        
        return xmlDoc;
    }
}