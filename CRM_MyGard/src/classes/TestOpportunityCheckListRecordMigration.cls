@isTest
public class TestOpportunityCheckListRecordMigration{
    static testMethod void testOTMCharterersNew(){
        OpportunityCheckListRecordMigration otmOppChecklist = new OpportunityCheckListRecordMigration();
        otmOppChecklist.doOperation();
    }
    /* 
     public static Opportunity_Checklist__c [] testOppChecklists;
     public static User salesforceUser;
     public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
     public List<Opportunity> allOpp = [SELECT ID FROM Opportunity WHERE RecordTypeName__c = 'P_I'];
     public static String strAccId;
     public static String strOppId;
     public static Id oldPIRecId =[SELECT Id FROM RecordType WHERE Name='Old P&I' AND sObjectType='Opportunity_Checklist__c'].id;
     public static Id newPIRecId =[SELECT Id FROM RecordType WHERE Name='P&I' AND sObjectType='Opportunity_Checklist__c'].id;
     public static Id ownersRecId =[SELECT Id FROM RecordType WHERE Name='Owners' AND sObjectType='Opportunity_Checklist__c'].id;
     public static Id mouRecId =[SELECT Id FROM RecordType WHERE Name='MOU' AND sObjectType='Opportunity_Checklist__c'].id;
     public static Id charterersRecId =[SELECT Id FROM RecordType WHERE Name='Charterers' AND sObjectType='Opportunity_Checklist__c'].id; 
   // public Opportunity opp = new Opportunity();
     
     //static testMethod void testOneTimeMigrationOpportunityCheckListMethod()
     public static void createUser()
        {
            //List<Opportunity> allOpp = [SELECT ID FROM Opportunity WHERE RecordTypeName__c = 'P&I'];

            //Create a user
      salesforceUser = new User(
                                Alias = 'standt', 
                                profileId = salesforceLicenseId ,
                                Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8',
                                CommunityNickname = 'test13',
                                LastName='Testing',
                                LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US',  
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName='test008@testorg.com'
                                     );
        
        insert salesforceUser; 
        }
        //create an account record
        public static void createAccount(){
      
        Account acc = new Account(  Name='APEXTESTACC001',
                                  BillingCity = 'Bristol',
                                  BillingCountry = 'United Kingdom',
                                  BillingPostalCode = 'BS1 1AD',
                                  BillingState = 'Avon' ,
                                  BillingStreet = '1 Elmgrove Road',
                                  Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id, 
                                  Market_Area__c = TestDataGenerator.getMarketArea().Id,
                                  OwnerId = salesforceUser.id
                                );
        insert acc;
        strAccId = acc.Id; 
        }
         //create opportunity
        public static void createOpp(){
        Id strRecId =[SELECT Id FROM RecordType WHERE Name='P&I' AND sObjectType='Opportunity' ].id;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = strRecId ;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId;
        opp.StageName = 'Risk Evaluation';//Renewable Opportunity
        opp.Type = 'New Business';        
        opp.Business_Type__c = 'MOUs';
        opp.Approval_Criteria__c = 'Self Approval';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        opp.Confirm_not_on_sanction_list__c = true;
        opp.Amount = 100;
        insert opp;
        strOppId = opp.Id;
        System.debug('checking'+ strOppId );
        }
        //For Owners
     @isTest static void testOTMOwners(){                                                     
     //create Opportunity Checklist  
     testOpportunityCheckListRecordMigration.createOpp();
       
        testOppChecklists = new Opportunity_Checklist__c []{
            new Opportunity_Checklist__c (Opportunity__c = strOppId , Owners_P_I_Completed__c = true, Checklist_Completed__c = true,
                                          Capital_Provider_Updated_in_GIC__c = true, Premium_Installments__c = true,
                                          Gained_Lost_vsls_Comments__c = 'comments', Change_of_broker__c = true,
                                          No_change__c = true, Capital_Provider1__c = true, Capital_Provider_Comments__c='comments',
                                          Broker_details_including_contact__c='coments', Broker_Commission__c= true,
                                          Commission_Comments__c='test',Change_in_Premium__c= true, Change_in_Premium_Comments__c='test',
                                          Assureds1__c = true,Assureds_Comments__c= 'test', Coe_Text__c=true,
                                          Wordings_Comments__c='test',Additional_Covers1__c=true, Owners_P_I_Additional_Covers_Comments__c='test',
                                          Gained_Lost_vsls1__c = true,
                                          Deductibles__c=true,Deductibles_Comments__c= 'test', Other1__c=true,
                                          Comments__c='test',Owners_Checked_terms_conditions__c= true,Gained_Lost_vsls_Updated_in_GIC__c= true,
                                          Assistant_New_Premiums_in_GIC__c=true, Assistant_UW_Notes_Updated__c = true,
                                          Document_Addressees_Updated__c=true, Assistant_CoE_Sent__c= true,
                                          Assistant_Cover_Note_Sent__c='yes',Assistant_Debit_Note_Sent__c=true,RecordTypeId=oldPIRecId )
                                          
          // new Opportunity_Checklist__c (Opportunity__c = opp.id, Agreement_type__c = 'Charterers', Checklist_Completed__c = true,
          //                                 Capital_Provider_Updated_in_GIC__c = true, Premium_Installments__c = true,
          //                                 Gained_Lost_vsls_Comments__c = 'comments', Change_of_broker__c = true,
          //                                  Charterers_Deductibles__c = true,Chrters_Broker_details_including_contact__c ='comments' )  
        };
            insert testOppChecklists ;
            
            List<Opportunity_Checklist__c> OppCheckList = [SELECT ID, Agreement_type__c, Checklist_Completed__c, Opportunity_Name__c FROM Opportunity_Checklist__c WHERE Opportunity__c =: strOppId ];
            OpportunityCheckListRecordMigration otmOppChecklist = new OpportunityCheckListRecordMigration(); //creating instance of the class
            createUser();
            createAccount();
            Test.startTest();
            otmOppChecklist.createNew(testOppChecklists);
            otmOppChecklist.doOperation();
            System.assertEquals(OppCheckList[0].Checklist_Completed__c, true);
            Test.stopTest();
            
        }
                                                         
         static testMethod void testOTMOwnersNew(){
     //create Opportunity Checklist  
     testOpportunityCheckListRecordMigration.createOpp();
       
        testOppChecklists = new Opportunity_Checklist__c []{
            new Opportunity_Checklist__c (Opportunity__c = strOppId ,Owners_P_I_Completed__c = true, Checklist_Completed__c = false,
                                          Capital_Provider_Updated_in_GIC__c = true, Premium_Installments__c = true,
                                          Gained_Lost_vsls_Comments__c = 'comments', Change_of_broker__c = true,
                                          No_change__c = true, Capital_Provider1__c = true, Capital_Provider_Comments__c='comments',
                                          Broker_details_including_contact__c='coments', Broker_Commission__c= true,
                                          Commission_Comments__c='test',Change_in_Premium__c= true, Change_in_Premium_Comments__c='test',
                                          Assureds1__c = true,Assureds_Comments__c= 'test', Coe_Text__c=true,
                                          Wordings_Comments__c='test',Additional_Covers1__c=true, Owners_P_I_Additional_Covers_Comments__c='test',
                                          Gained_Lost_vsls1__c = true,
                                          Deductibles__c=true,Deductibles_Comments__c= 'test', Other1__c=true,
                                          Comments__c='test',Owners_Checked_terms_conditions__c= true,Gained_Lost_vsls_Updated_in_GIC__c= true,
                                          Assistant_New_Premiums_in_GIC__c=true, Assistant_UW_Notes_Updated__c = true,
                                          Document_Addressees_Updated__c=true, Assistant_CoE_Sent__c= true,
                                          Assistant_Cover_Note_Sent__c='yes',Assistant_Debit_Note_Sent__c=true,RecordTypeId=newPIRecId )
                                          
          // new Opportunity_Checklist__c (Opportunity__c = opp.id, Agreement_type__c = 'Charterers', Checklist_Completed__c = true,
            //                                 Capital_Provider_Updated_in_GIC__c = true, Premium_Installments__c = true,
            //                                Gained_Lost_vsls_Comments__c = 'comments', Change_of_broker__c = true,
            //                                Charterers_Deductibles__c = true,Chrters_Broker_details_including_contact__c ='comments' )  
        };
            insert testOppChecklists ;
            
            //List<Opportunity_Checklist__c> OppCheckList = [SELECT ID, Agreement_type__c, Checklist_Completed__c, Opportunity_Name__c FROM Opportunity_Checklist__c WHERE Opportunity__c =: strOppId ];
            OpportunityCheckListRecordMigration otmOppChecklist = new OpportunityCheckListRecordMigration(); //creating instance of the class
            createUser();
            createAccount();
            Test.startTest();
            otmOppChecklist.createNew(testOppChecklists);
            List<Opportunity_Checklist__c> OppCheckList = [SELECT ID, Agreement_type__c, Checklist_Completed__c, Opportunity_Name__c FROM Opportunity_Checklist__c WHERE Opportunity__c =: strOppId ];
            otmOppChecklist.doOperation();
            System.assertEquals(OppCheckList[0].Checklist_Completed__c, false);
            Test.stopTest();
            
        }
     //   For MOU
      static testMethod void testOTMMOU(){
         testOpportunityCheckListRecordMigration.createOpp();
       
        testOppChecklists = new Opportunity_Checklist__c []{
            new Opportunity_Checklist__c (MOU_Completed__c = true,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Premium__c=true,Change_in_Premium_Comments__c='test',
                                       Lay_up_Rate__c=true,Lay_up_Rate_Comments__c='test',
                                       Assureds1__c=false,Assureds_Comments__c='test',
                                       Coe_Text__c=true,Wordings_Comments__c='test',
                                       Additional_Covers1__c=true,Owners_P_I_Additional_Covers_Comments__c='test',
                                       Limits1__c=true ,MOU_Limits_Comments__c='test',
                                       Gained_Lost_vsls1__c=true,Gained_Lost_vsls_Comments__c='test',
                                       Renewal_Info_Sent__c=true,
                                       Deductibles__c= true,Deductibles_Comments__c='test',
                                       Other1__c=true,Comments__c='test',
                                       //GIC
                                       Capital_Provider_Updated_in_GIC__c=true,Owners_Checked_terms_conditions__c=true,
                                       Assistant_New_Premiums_in_GIC__c=true,Limits_Updated_in_GIC__c= true ,Gained_Lost_vsls_Updated_in_GIC__c=true,
                                       Commission_Updated_in_GIC__c=true,Assistant_UW_Notes_Updated__c= true,
                                       Change_in_Premium_Updated_in_GIC__c=true ,Document_Addressees_Updated__c=true,
                                       Assureds_Updated_in_GIC__c=true,Assistant_CoE_Sent__c=true,
                                       Assistant_Checked_T_Cs__c=false,Assistant_Cover_Note_Sent__c='yes',
                                       Wordings_Updated_in_GIC__c=true,Assistant_Debit_Note_Sent__c=true,
                                       Assistant_Comments__c='test',Lay_up_Rate_Updated_in_GIC__c=true,RecordTypeId=oldPIRecId )

};
insert testOppChecklists ;

List<Opportunity_Checklist__c> OppCheckList = [SELECT ID, Agreement_type__c, Checklist_Completed__c, Opportunity_Name__c FROM Opportunity_Checklist__c WHERE Opportunity__c =: strOppId ];
            OpportunityCheckListRecordMigration otmOppChecklist = new OpportunityCheckListRecordMigration(); //creating instance of the class
            createUser();
            createAccount();
            Test.startTest();
            otmOppChecklist.createNew(testOppChecklists);
            otmOppChecklist.doOperation();
            System.assertEquals(OppCheckList[0].Checklist_Completed__c, true);
            Test.stopTest();
            }
            
         static testMethod void testOTMMOUNew(){
         testOpportunityCheckListRecordMigration.createOpp();
       
         testOppChecklists = new Opportunity_Checklist__c []{
            new Opportunity_Checklist__c (MOU_Completed__c = true,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Premium__c=true,Change_in_Premium_Comments__c='test',
                                       Lay_up_Rate__c=true,Lay_up_Rate_Comments__c='test',
                                       Assureds1__c=false,Assureds_Comments__c='test',
                                       Coe_Text__c=true,Wordings_Comments__c='test',
                                       Additional_Covers1__c=true,Owners_P_I_Additional_Covers_Comments__c='test',
                                       Limits1__c=true ,MOU_Limits_Comments__c='test',
                                       Gained_Lost_vsls1__c=true,Gained_Lost_vsls_Comments__c='test',
                                       Renewal_Info_Sent__c=true,
                                       Deductibles__c= true,Deductibles_Comments__c='test',
                                       Other1__c=true,Comments__c='test',
                                       //GIC
                                       Capital_Provider_Updated_in_GIC__c=true,Owners_Checked_terms_conditions__c=true,
                                       Assistant_New_Premiums_in_GIC__c=true,Limits_Updated_in_GIC__c= true ,Gained_Lost_vsls_Updated_in_GIC__c=true,
                                       Commission_Updated_in_GIC__c=true,Assistant_UW_Notes_Updated__c= true,
                                       Change_in_Premium_Updated_in_GIC__c=true ,Document_Addressees_Updated__c=true,
                                       Assureds_Updated_in_GIC__c=true,Assistant_CoE_Sent__c=true,
                                       Assistant_Checked_T_Cs__c=false,Assistant_Cover_Note_Sent__c='yes',
                                       Wordings_Updated_in_GIC__c=true,Assistant_Debit_Note_Sent__c=true,
                                       Assistant_Comments__c='test',Lay_up_Rate_Updated_in_GIC__c=true,RecordTypeId=newPIRecId )

};
insert testOppChecklists ;

List<Opportunity_Checklist__c> OppCheckList = [SELECT ID, Agreement_type__c, Checklist_Completed__c, Opportunity_Name__c FROM Opportunity_Checklist__c WHERE Opportunity__c =: strOppId ];
            OpportunityCheckListRecordMigration otmOppChecklist = new OpportunityCheckListRecordMigration(); //creating instance of the class
            createUser();
            createAccount();
            Test.startTest();
            otmOppChecklist.createNew(testOppChecklists);
            otmOppChecklist.doOperation();
            System.assertEquals(OppCheckList[0].Checklist_Completed__c, true);
            Test.stopTest();
            }
            
            // For CHARTERERS
            static testMethod void testOTMCharterers(){
            testOpportunityCheckListRecordMigration.createOpp();
            testOppChecklists = new Opportunity_Checklist__c []{
            new Opportunity_Checklist__c (Charterers_Completed__c = true,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Charterers_Deductiblesv2__c= true, Charterers_Deductibles_Commentsv2__c='test',
                                       RecordTypeId=oldPIRecId )
                                       };
            insert testOppChecklists;      
            List<Opportunity_Checklist__c> OppCheckList = [SELECT ID, Agreement_type__c, Checklist_Completed__c, Opportunity_Name__c FROM Opportunity_Checklist__c WHERE Opportunity__c =: strOppId ];
            OpportunityCheckListRecordMigration otmOppChecklist = new OpportunityCheckListRecordMigration(); //creating instance of the class
            createUser();
            createAccount();
            Test.startTest();
            otmOppChecklist.createNew(testOppChecklists);
            otmOppChecklist.doOperation();
            System.assertEquals(OppCheckList[0].Checklist_Completed__c, true);
            Test.stopTest();                     
       }
        static testMethod void testOTMCharterersNew(){
            testOpportunityCheckListRecordMigration.createOpp();
            testOppChecklists = new Opportunity_Checklist__c []{
            new Opportunity_Checklist__c (Charterers_Completed__c = true,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Charterers_Deductiblesv2__c= true, Charterers_Deductibles_Commentsv2__c='test',
                                       RecordTypeId=newPIRecId )
                                       };
            insert testOppChecklists;      
            List<Opportunity_Checklist__c> OppCheckList = [SELECT ID, Agreement_type__c, Checklist_Completed__c, Opportunity_Name__c FROM Opportunity_Checklist__c WHERE Opportunity__c =: strOppId ];
            OpportunityCheckListRecordMigration otmOppChecklist = new OpportunityCheckListRecordMigration(); //creating instance of the class
            createUser();
            createAccount();
            Test.startTest();
            otmOppChecklist.createNew(testOppChecklists);
            otmOppChecklist.doOperation();
            System.assertEquals(OppCheckList[0].Checklist_Completed__c, true);
            Test.stopTest();                     
       }
       */
}