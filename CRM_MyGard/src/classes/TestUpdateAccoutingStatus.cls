@isTest
private class TestUpdateAccoutingStatus {
	public static Deferral_and_waiver_request__c deferralRequest;
    public static Account anAccount;
    
    private static void setUpAccount(){
       	anAccount = new Account(  
		            Name = 'Test Account',
		            BillingCity = 'Bristol',
		            BillingCountry = 'United Kingdom',
		            BillingPostalCode = 'BS1 1AD',
		            BillingState = 'Avon' ,
		            BillingStreet = '1 Elmgrove Road',
		            Company_Role__c = 'Client',
		            Area_Manager__c = UserInfo.getUserId()
        );
        insert anAccount;
	}
    
    private static void createDeferralRequest(){
        setUpAccount();
        deferralRequest = new Deferral_and_waiver_request__c(
        						status__c = 'Approved',
            					Request_type__c = 'Deferral',//Waiver
            					Company_Name__c = anAccount.Id
        					);
        insert deferralRequest;
    }
    
    static testMethod void TestBehaviour(){
        Test.startTest();
        createDeferralRequest();
        deferralRequest.Request_type__c = 'Waiver';
        update deferralRequest;
        Test.stopTest();
        
        Account fetchedAccount = [SELECT ID, Bankruptcy__c FROM ACCOUNT WHERE ID = :anAccount.Id];
        System.assertEquals('Written Off', fetchedAccount.Bankruptcy__c, 'Written Off');
    }
}