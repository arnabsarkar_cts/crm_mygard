@isTest(seealldata=false)
public class TestViewDocCtrl{

         //public static commonvariables__c com_var;
    Public static Object__c test_object;
        public static testmethod void ViewDocument(){
         AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonrecord();
        gtdInstance.customsettings_rec();
        system.debug('***********Accmap_1 '+GardTestData.Accmap_1 );
        system.debug('***********Accmap_2 '+GardTestData.Accmap_2 );
        gtdInstance.ViewDoc_testrecord(); 
         GardtestData.test_object_1st.guid__c = '8ce8ac89-a6fd-1836-9e07';
         update GardtestData.test_object_1st; 
         test_object = New Object__c(     Dead_Weight__c= 20,
                         Object_Unique_ID__c = '1234lkoko',
                         Name='hennessey',
                         Object_Type__c='Tanker',
                         Object_Sub_Type__c='Accommodation Ship',
                         No_of_passenger__c=100,
                         No_of_crew__c=70,
                         Gross_tonnage__c=1000,
                         Length__c=20,
                         Width__c=20,
                         Depth__c=30,
                         Port__c='Alabama',
                         Signal_Letters_Call_sign__c='hellooo',
                         Classification_society__c='CS 1',
                         Flag__c='Albania',
                         Rebuilt__c='2014-01-01',
                       //  Imo_Lloyds_No__c='4355555',
                        // Dummy_Object_flag__c = 0,
                         Year_built__c='2013-01-01',
                         Dummy_Object_flag__c=0);
                     insert test_object;
        //com_var = new commonvariables__c(Name = 'ReplyToMailId',value__c='no-reply@gard.no'); 
        //insert com_var;         
        //Invoking methods for ViewDocCtrl for broker and client users......
        
        GardTestData.junc_object_broker.Broker__c = GardTestData.brokerAcc.ID;
        //GardTestData.junc_object_broker.Client__c = GardTestData.brokerAcc.ID;
        update GardTestData.junc_object_broker;
        
        GardTestData.brokerDoc.Document_Metadata_External_ID__c = '2asas123';
        update GardTestData.brokerDoc;
        
        BlueCard_webservice_endpoint__c bcTest = new BlueCard_webservice_endpoint__c(
                                                                                         Name = 'BlueCard_WS_endpoint',
                                                                                         Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/Documents/DocumentFetcher/GetDocumentService_ep'
                                                                                     );
       
        insert bcTest;                                              
        
                
        System.runAs(GardTestData.brokerUser)        
        {
            
            Test.startTest();
            Pagereference pge = page.ViewDocument;             
            pge.getParameters().put('objid',GardtestData.test_object_1st.guid__c);
            test.setcurrentpage(pge);
            //ApexPages.currentPage().getParameters().put('favId',GardTestData.test_fav_for_doc.id); 
            ViewDocCtrl test_broker= new ViewDocCtrl();
            ApexPages.currentPage().getParameters().put('favId',GardTestData.test_fav_for_doc.id);   
            test_broker.prvUrl = 'ABC';
            test_broker.getSortDirection();
            test_broker.setSortDirection('ASC');
            
            test_broker.newstrSelected = GardTestData.brokerAcc.id;
            test_broker.strId=GardTestData.test_object_1st.id;        
            test_broker.newFilterName = 'abc';        
            //test_broker.getDocType();
            //test_broker.getRecentUpdatedVal(); 
            ApexPages.currentPage().getParameters().put('objid',test_broker.strId);
            //test_broker.sendMailforEditObject();           
            test_broker.selectedDocType.add(GardTestData.brokerDoc.Document_Type__c);            
            test_broker.selectedCover.add('P&I Cover');            
            test_broker.selectedProductArea.add('P&I');            
            test_broker.selectedPolicyYr.add('2014');            
            test_broker.selectedRisk.add('Y');            
            test_broker.selectedRisk.add('N');            
            test_broker.selectedObj.add('Curia');
            //test_broker.checkMode();
            apexpages.currentpage().getparameters().put('objid',GardTestData.test_object_1st.id);
            //testobj.checkMode();
            test_broker.strSelected = GardTestData.brokerAcc.id+';'+GardTestData.brokerAcc.id;
            //testobj.fetchSelectedClients(); 
            //system.assertequals(recent_update_list.size()>0,true,true);
            system.assertequals(test_broker.selectedDocType.size()>0,true,true);
            system.assertequals(test_broker.selectedCover.size()>0,true,true);
            system.assertequals(test_broker.selectedProductArea.size()>0,true,true);
            system.assertequals(test_broker.selectedRisk.size()>0,true,true);
            system.assertequals(test_broker.selectedObj.size()>0,true,true);
            test_broker.isQueried = true;
            test_broker.isfav = true;
           
            test_broker.createQuery();
            test_broker.sortData();
            test_broker.customQuerySearch();
            test_broker.reset();
            test_broker.getTotalPages();
            test_broker.pdf ='abc';
            test_broker.hasNext = true;
            test_broker.hasPrevious=true;
            test_broker.setpageNumber();
            test_broker.previous(); 
            test_broker.next();            
            PageReference pageRef = Page.ViewDocument;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('objid',GardTestData.test_object_1st.id);
                   
            test_broker.createFavourites();
            
            test_broker.obj=new object__c();
            test_broker.obj.id=GardTestData.test_object_1st.id;
            Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
            test_broker.caseTestId = singleCase.ID;
            test_broker.sendMailforEditObject();
            test_broker.closeconfirmPopUp();
            test_broker.displayEditPopUp();
           // test_broker.createFavouritesObject();
            test_broker.docJuncObjLst = new List<DocumentMetadata_Junction_object__c>();
            test_broker.docJuncObjLst.add(GardTestData.junc_object_broker);
            test_broker.exportToExcel();
            //test_broker.search();
            test_broker.deleteFavouritesObject();
            test_broker.fetchFavouritesObject();
            test_broker.deleteFavourites();
            ViewDocCtrl.getUserName();   
            // test_broker.createMasterDocIds();   
            test_broker.currentDocumentId = '2asas123'; 
            test_broker.displayDoc();
            test_broker.currentCompanyId = 'Abc';    
            test_broker.isObjectFav = true;
            test_broker.objMode = false;
            test_broker.createQuery();
            
            test_broker.populateDocuments();
            test_broker.fetchSelectedClients();
            apexpages.currentpage().getparameters().put('objid',test_object.id);
            test_broker.populateDocuments();
            test_broker.isAuthorized = true;
            test_broker.objMode = TRUE;
            test_broker.imoNo = null;
            test_broker.populateDocuments();
            Test.stopTest();
        }
            
   /* System.runAs(GardTestData.clientUser)             
    {
        Pagereference pge1 = page.ViewDocument;             
        pge1.getParameters().put('objid',GardtestData.test_object_1st.guid__c);
        test.setcurrentpage(pge1);
        ViewDocCtrl test_client= new ViewDocCtrl();
        test_client.selectedDocType.add(GardTestData.brokerDoc.Document_Type__c);            
        test_client.selectedCover.add('P&I Cover');            
        test_client.selectedProductArea.add('P&I');            
        test_client.selectedPolicyYr.add('2014');            
        test_client.selectedRisk.add('Y');            
        //test_client.selectedRisk.add('N');            
        test_client.selectedObj.add('Curia');
        system.assertequals(test_client.selectedDocType.size()>0,true,true);
        system.assertequals(test_client.selectedCover.size()>0,true,true);
        system.assertequals(test_client.selectedProductArea.size()>0,true,true);
        system.assertequals(test_client.selectedRisk.size()>0,true,true);
        system.assertequals(test_client.selectedObj.size()>0,true,true);
        test_client.first();
        test_client.last();
        //test_client.checkMode();
        apexpages.currentpage().getparameters().put('objid',GardTestData.test_object_1st.id);
       // test_client.populateDocuments();  
         
    
    } */

    //Invoking methods for ViewDocMyClaimsCtrl for broker and client users......
/*
     system.runAs(GardTestData.brokerUser)    
    {
        ViewDocMyClaimsCtrl test_broker= new ViewDocMyClaimsCtrl();
        test_broker.prvUrl='https://cs8.salesforce.com/01pL0000000CTi9/';        
        test_broker.CaseId=GardTestData.brokerCase.id;
        test_broker.populateClient(); 
        test_broker.getDocType();
        test_broker.getRecentUpdatedVal();
        test_broker.populateFilterData();
        test_broker.populateFilterDataCaseDtlPg();
        test_broker.caseDtl=false; 
        test_broker.defaultQuery(); 
        test_broker.caseDtl=true;
        test_broker.defaultQuery(); 
        test_broker.caseDtl=false;
        test_broker.createQuery();
        test_broker.caseDtl=true;
        test_broker.createQuery();
        test_broker.selectedDocType.add(GardTestData.brokerDoc.Document_Type__c);
        test_broker.createQuery();
        test_broker.selectedCover.add('P&I Cover');
        test_broker.createQuery();
        test_broker.selectedProductArea.add('P&I');
        test_broker.createQuery();
        test_broker.selectedPolicyYr.add('2014');
        test_broker.createQuery();
        test_broker.selectedClaimType.add('Alleged Cause');
        test_broker.createQuery();
        test_broker.selectedClaimStatus.add('Pending');
        test_broker.createQuery();
        test_broker.selectedObj.add('Curia');
        test_broker.createQuery();
        test_broker.search();
        test_broker.runQuery();
        test_broker.caseDtl=false; 
        test_broker.sortData();
        test_broker.caseDtl=true;
        test_broker.sortData();
        test_broker.reset();
        test_broker.getTotalPages();
        test_broker.setpageNumber();
        test_broker.previous(); 
        test_broker.next();
        test_broker.checkMode();
        PageReference pageRef = Page.ViewDocument;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('CaseId',GardTestData.brokerCase.id);
        test_broker.checkMode();      
        test_broker.caseDtl=false;
        test_broker.checkMode();   
        test_broker.hasNext=true;
        test_broker.hasPrevious=true;    
    }
    system.runAs(GardTestData.clientUser)    
    {
        ViewDocMyClaimsCtrl test_client= new ViewDocMyClaimsCtrl();
        test_client.prvUrl='https://cs8.salesforce.com/01pL0000000CTi9/';        
        test_client.populateClient(); 
        test_client.getDocType();
        test_client.getRecentUpdatedVal();
        test_client.populateFilterData();
        test_client.populateFilterDataCaseDtlPg();
        test_client.caseDtl=false; 
        test_client.defaultQuery(); 
        test_client.caseDtl=true;
        test_client.defaultQuery(); 
        test_client.search();
        
               
     
    }*/
  }
}