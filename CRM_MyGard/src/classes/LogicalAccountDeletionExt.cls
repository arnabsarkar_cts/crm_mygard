/*************************************************************************************************
    Author      : Rich Callear
    Company     : cDecisions
    Date Created: 
    Description : This class provides support to the Logical Account Deletion vf page
    Modified    : Martin Gardner  - Added the set Request flag method to direct the deletion via
                    an approval process. Also added the account record method to redirect back to the
                    account record.
***************************************************************************************************/
public with sharing class LogicalAccountDeletionExt {

    public account acc {get; set;}
    private ApexPages.StandardController stdCtrl {get; set;}
    public boolean DisplaySection{get;set;}
    public Boolean success{get;set;}//SF-4414 to give out errors
    public Boolean deletedFlagOriginalValue{get;set;} // SF-4414 used to save the older state for Deleted__c flag which would be chagned even if some error occurs when Updating, in some other trigger

    public logicalAccountDeletionExt(ApexPages.StandardController std) 
    {
      //acc = (account)std.getRecord();
      acc = [select id, Deleted__c, Broker_On_Risk__c, Client_On_Risk__c, Company_Status__c, Company_Role__c, Sub_Roles__c,MyGard_enabled__c from Account where Id = :std.getId()];   
      stdctrl = std;
      DisplaySection = false;
      success = false;
      deletedFlagOriginalValue = acc.deleted__c;
    }
    
    //Not In Use
    public pagereference setFlag()
    {
        if (!acc.Deleted__c){
            acc.deleted__C = true;
            stdctrl.save();
        }
        return null;
    }
    
    public pageReference setRequestFlag()
    {
        try{
            if (!acc.Deleted__c && !acc.Broker_On_Risk__c && !acc.Client_On_Risk__c && !acc.MyGard_enabled__c) {
        	DQ__c DQSettings = DataQualitySupport.GetSetting(UserInfo.getUserId());
            if(DQSettings.Enable_Role_Governance__c){ 
                //governance enabled
                
                    System.Debug('LogicalAccountDeletion*** Governance Enabled ****');
                    acc.Inactivate_Company_Request__c = true;
                    //acc.Company_Role__c = '';
                    //acc.Sub_Roles__c = '';
                    
                    
                    
                    //To be commented if post deactivation, role_change_approval_process is required to run
                    //START - 164
                    acc.Unapproved_Company_Roles_to_Remove__c = acc.Company_Role__c;
                    acc.Sub_Roles_to_Remove__c = acc.Sub_Roles__c;
                    //END - 164
                    
                    
                    //stdctrl.save();
                    update acc;
                    
                    //submit approval
                    //CRM 164
                    Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                    req1.setObjectId(acc.id);
                    Approval.ProcessResult result;
                    result = Approval.process(req1);
                    //CRM 164
                	success = true;
            }else{
                //governance disabled
                //if (!acc.Deleted__c && !acc.Broker_On_Risk__c && !acc.Client_On_Risk__c && !acc.MyGard_enabled__c) {
                    System.Debug('LogicalAccountDeletion*** Governance Disabled ****');
                    acc.ParentId = null; //SF-4414
                    acc.Company_Status__c = 'Inactive';
                    acc.Company_Role__c = '';
                    acc.Sub_Roles__c = '';
                    acc.Deleted__c = true;
                    update acc;
                    success = true;
                }
            }
        }Catch(Exception ex){
            success = false;
            System.debug('LogicalAccountDeletion Exception Occurred - '+ex.getMessage());
            //DisplaySection = true;
            //apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You do not have permissions to edit Correspondents.'));
            //apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()));
        }
        System.debug('LogicalAccountDeletion acc.Deleted__c - '+acc.Deleted__c);
        System.debug('LogicalAccountDeletion deletedFlagOriginalValue - '+deletedFlagOriginalValue);
        System.debug('LogicalAccountDeletion success - '+this.success);
        return null;
    }
    
    public pagereference accountsHome()
    {
            pagereference home = new pagereference('/001/o');
            return home;
    }
    public pagereference accountRecord()
    {
            pagereference act = new pagereference('/' + acc.Id);
            return act;
    }
        
}