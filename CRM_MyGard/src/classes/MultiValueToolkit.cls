/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 
    Description : This class provides support manipulating multivalue fields
    Modified    : 4/11/2013 streamlined the code  
***************************************************************************************************/
public with sharing class MultiValueToolkit {
    
    //Converts a semicolon separated string into a set
    public static Set<String> multiValueToSet(String mvFieldValue){        
        return mvFieldValue!=null ? new Set<String>(mvFieldValue.split(';')) : new Set<String>();
    }
}