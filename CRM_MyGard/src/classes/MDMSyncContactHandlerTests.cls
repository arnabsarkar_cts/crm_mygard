@isTest
private class MDMSyncContactHandlerTests {
	
	private static List<Valid_Role_Combination__c> validRoles;
	
	private static void setupValidRoles() {
		////For some reason this seems to be throwing an internal Salesforce error /
		////System.UnexpectedException: Salesforce System Error: 1583762872-13077 (1716107030) (1716107030)
		//validRoles = (List<Valid_Role_Combination__c>)Test.loadData(Valid_Role_Combination__c.sObjectType, 'MDMValidRoles');
		////END...
		validRoles = new List<Valid_Role_Combination__c>();
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 11'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 12'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 21'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 22'));
		validRoles.add(new Valid_Role_Combination__c(Role__c = 'Other', Sub_Role__c = 'Other Sub Role'));
		insert validRoles;
	}
	
	private static testMethod void InsertContactSyncStatus_WebServiceEnabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		
		///ACT....
		Test.startTest();
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test');
		MDMSyncContactHandler.InsertContactSyncStatus(new List<Contact> { c }, true);
		Test.stopTest();
		
		///ASSERT....
		System.assertEquals('Sync In Progress', c.Synchronisation_Status__c, 'Contact Sync Status');
	}
	
	private static testMethod void InsertContactSyncStatus_WebServiceDisabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		
		///ACT....
		Test.startTest();
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test');
		MDMSyncContactHandler.InsertContactSyncStatus(new List<Contact> { c }, false);
		Test.stopTest();
		
		///ASSERT....
		System.assertEquals('Synchronised', c.Synchronisation_Status__c, 'Contact Sync Status');
	}
	
	private static testMethod void UpdateContactSyncStatus_WebServiceEnabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test', Synchronisation_Status__c = 'Synchronised');
		insert c;
		
		Contact newC = c.clone(true, true, true, true);
		newC.FirstName = 'TestNew';
		
		///ACT....
		Test.startTest();
		MDMSyncContactHandler.UpdateContactSyncStatus(new List<Contact> { newC }, new map<id, Contact> { c.Id => c }, true);
		Test.stopTest();
		
		///ASSERT....
		System.assertEquals('Sync In Progress', newC.Synchronisation_Status__c, 'Contact Sync Status');
	}
	
	private static testMethod void UpdateContactSyncStatus_WebServiceDisabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test');
		insert c;
		
		Contact newC = c.clone(true, true, true, true);
		newC.FirstName = 'TestNew';
		
		///ACT....
		Test.startTest();
		MDMSyncContactHandler.UpdateContactSyncStatus(new List<Contact> { newC }, new map<id, Contact> { c.Id => c }, false);
		Test.stopTest();
		
		///ASSERT....
		System.assertEquals('Synchronised', newC.Synchronisation_Status__c, 'Contact Sync Status');
	}
	
	private static testMethod void DeleteContactSyncStatus_WebServiceEnabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test', Synchronisation_Status__c = 'Synchronised');
		insert c;
		
		Contact newC = c.clone(true, true, true, true);
		newC.Deleted__c = true;
		
		///ACT....
		Test.startTest();
		MDMSyncContactHandler.UpdateContactSyncStatus(new List<Contact> { newC }, new map<id, Contact> { c.Id => c }, true);
		Test.stopTest();
		
		///ASSERT....
		System.assertEquals('Sync In Progress', newC.Synchronisation_Status__c, 'Contact Sync Status');
	}
	
	private static testMethod void DeleteContactSyncStatus_WebServiceDisabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test');
		insert c;
		
		Contact newC = c.clone(true, true, true, true);
		newC.Deleted__c = true;
		
		///ACT....
		Test.startTest();
		MDMSyncContactHandler.UpdateContactSyncStatus(new List<Contact> { newC }, new map<id, Contact> { c.Id => c }, false);
		Test.stopTest();
		
		///ASSERT....
		System.assertEquals('Synchronised', newC.Synchronisation_Status__c, 'Contact Sync Status');
	}
	
	private static testMethod void SyncContactInsert_WebServiceEnabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test');
		insert c;
		
		///ACT....
		Test.startTest();
		MDMSyncContactHandler.SyncContact(new map<id, Contact> { c.Id => c }, true);
		Test.stopTest();
		
		///ASSERT....
		c = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :c.id];
		System.assertEquals('Sync Failed', c.Synchronisation_Status__c, 'Contact Sync Status');
	}
	
	private static testMethod void SyncContactInsert_WebServiceDisabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test', Synchronisation_Status__c = 'TEST');
		insert c;
		
		///ACT....
		Test.startTest();
		MDMSyncContactHandler.SyncContact(new map<id, Contact> { c.Id => c }, false);
		Test.stopTest();
		
		///ASSERT....
		c = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :c.id];
		System.assertEquals('TEST', c.Synchronisation_Status__c, 'Contact Sync Status');
	}
	
	private static testMethod void SyncContactUpdate_WebServiceEnabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test', Synchronisation_Status__c = 'Synchronised');
		insert c;
		
		Contact c2 = c.clone(true, true, true, true);
		c2.LastName = 'Test 2';
		c2.Synchronisation_Status__c = 'Sync In Progress';
		
		///ACT....
		Test.startTest();
		MDMSyncContactHandler.SyncContact(new List<Contact> { c2 }, new map<id, Contact> { c2.Id => c2 }, new map<id, Contact> { c.Id => c }, true);
		Test.stopTest();
		
		///ASSERT....
		c = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :c.id];
		System.assertEquals('Sync Failed', c.Synchronisation_Status__c, 'Contact Sync Status');
	}
	
	private static testMethod void SyncContactUpdate_WebServiceDisabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test', Synchronisation_Status__c = 'TEST');
		insert c;
		
		Contact c2 = c.clone(true, true, true, true);
		c2.LastName = 'Test 2';
		c2.Synchronisation_Status__c = 'Sync In Progress';
		
		///ACT....
		Test.startTest();
		MDMSyncContactHandler.SyncContact(new List<Contact> { c2 }, new map<id, Contact> { c2.Id => c2 }, new map<id, Contact> { c.Id => c }, false);
		Test.stopTest();
		
		///ASSERT....
		c = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :c.id];
		System.assertEquals('TEST', c.Synchronisation_Status__c, 'Contact Sync Status');
	}
	
	private static testMethod void SyncContactDelete_WebServiceEnabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test', Synchronisation_Status__c = 'Synchronised');
		insert c;
		
		Contact c2 = c.clone(true, true, true, true);
		c2.Deleted__c = true;
		c2.Synchronisation_Status__c = 'Sync In Progress';
		
		///ACT....
		Test.startTest();
		MDMSyncContactHandler.SyncContact(new List<Contact> { c2 }, new map<id, Contact> { c2.Id => c2 }, new map<id, Contact> { c.Id => c }, true);
		Test.stopTest();
		
		///ASSERT....
		c = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :c.id];
		System.assertEquals('Sync Failed', c.Synchronisation_Status__c, 'Contact Sync Status');
	}
	
	private static testMethod void SyncContactDelete_WebServiceDisabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test', Synchronisation_Status__c = 'TEST');
		insert c;
		
		Contact c2 = c.clone(true, true, true, true);
		c2.Deleted__c = true;
		c2.Synchronisation_Status__c = 'Sync In Progress';
		
		///ACT....
		Test.startTest();
		MDMSyncContactHandler.SyncContact(new List<Contact> { c2 }, new map<id, Contact> { c2.Id => c2 }, new map<id, Contact> { c.Id => c }, false);
		Test.stopTest();
		
		///ASSERT....
		c = [SELECT id, Synchronisation_Status__c FROM Contact WHERE id = :c.id];
		System.assertEquals('TEST', c.Synchronisation_Status__c, 'Contact Sync Status');
	}
	
	private static testMethod void MDMSyncContactHandler_Coverage() {
		MDMProxyTests.setupMDMConfigSettings();
		setupValidRoles();
		
		Account a = new Account (Name = 'Unit Test Account', Company_Role__c = validRoles[0].Role__c, sub_Roles__c = validRoles[0].Sub_Role__c);
		insert a;
		Contact c = new Contact( AccountId = a.id,  firstname = 'Test', lastName = 'Test', Synchronisation_Status__c = 'TEST');
		insert c;
		
		Contact c2 = c.clone(true, true, true, true);
		c2.FirstName = 'Test 2';
		c2.Synchronisation_Status__c = 'Sync In Progress';
		
		List<Contact> newContacts = new List<Contact> { c2 };
		Map<id, Contact> newContactsMap = new Map<Id, Contact> (newContacts);
		List<Contact> oldContacts = new List<Contact> { c };
		Map<id, Contact> oldContactsMap = new Map<Id, Contact> (oldContacts);
		
		MDMSyncContactHandler.beforeInsert(newContacts, newContactsMap);
		MDMSyncContactHandler.afterInsert(newContacts, newContactsMap);
		MDMSyncContactHandler.beforeUpdate(newContacts, newContactsMap, oldContacts, oldContactsMap);
		MDMSyncContactHandler.afterUpdate(newContacts, newContactsMap, oldContacts, oldContactsMap);
		MDMSyncContactHandler.beforeDelete(oldContacts, oldContactsMap);
		MDMSyncContactHandler.afterDelete(oldContacts, oldContactsMap);
	}
}