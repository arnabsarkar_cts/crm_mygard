/*
    Name of Developer:   Debosmeeta Paul
    Name of Company:     CTS 
    
    Purpose of Class:    This class is used for Broker-Client login Clinic Invoice Details
*/
public class PEMEClientInvoiceDetailsCtrl{

    private static final Integer SMARTQUERY_PAGE_SIZE   = 10;
    public set<String> lstExaminations {get;set;}
    public set<String> lstObjects {get;set;}    
    public PEME_Invoice__c objInvoice {get;set;}
    public list<PEME_Exam_Detail__c> lstExamDetails {get;set;}
    public list<PEME_Exam_Detail__c> lstExamsPrint {get;set;}
    public String selectedInvoice {get;set;}
    public list<String> selectedObject {get;set;}
    public list<String> selectedExam {get;set;}
    public Contact userContact{get;set;}        
    List<User> lstUser;     
    public String loggedInAccountId;        
    public String loggedInContactId;        
    public Boolean isPeopleClaim{get;set;}
    private final String strBroker = 'Broker';
    private final String strClient = 'Client';
    private final String strClinic = 'Clinic';
    //pagination variables
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public String sSoqlQuery;
    private String nameOfMethod;
    Public Integer intStartrecord { get ; set ;}
    Public Integer intEndrecord { get ; set ;}
    public Integer pageNum{get;set;}//{pageNum = 1;}
    public String jointSortingParam { get;set; }
 //   private String sortDirection = Ascending;
 //   private String sortExp = 'Date__c';
    public Integer noOfData{get;set;}
    //pagination variables
    public String userName{get;set;}
    //Added For Favourites - Start
    String favouriteId;
    private List<Account_Contact_Mapping__c> acmLst;// added by Arindam
    public boolean isFav{get;set;}
    public String newFilterName{get;set;}
    public String strSubmitted{get;set;} //{strSubmitted=  'Approved';  }
    //Added For Favourites - End
    public String orderBy{get;set;}//{orderBy = '';}
    private String sortDirection = 'ASC';
    private String sortExp = '';  
    private static String Ascending='ASC';
    private static String limit10000=' limit 10000';
    AccountToContactMap__c acmCS;
    public String orderByNew
    {
     get
         {
            return sortExp;
         }
         set
         {
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExp)
             sortDirection = (sortDirection.contains('DESC')? Ascending : 'DESC');
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     }
     public String getSortDirection()
     {
        //if not column is selected 
        if (orderByNew == null || orderByNew == '')
          return Ascending;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value)
     {  
       sortDirection = value;
     }
    public ApexPages.StandardSetController submissionSetConOpen{
    get{ 
           return submissionSetConOpen;         
    }
    set; }
    
    public List<SelectOption> getExamOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstExaminations!= null && lstExaminations.size()>0){
            for(String strOp:lstExaminations){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();            
        }
        return Options;
    }
    
    public List<SelectOption> getObjectOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstObjects!= null && lstObjects.size()>0){
            for(String strOp:lstObjects){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();            
        }
        return Options;
    }
    
  /*  public String sortExpression{
         get{
            return sortExp;
         }
         set{          
           if (value == sortExp)
             sortDirection = (sortDirection == Ascending)? 'DESC' : Ascending;
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     } */
    
  /*   public String getSortDirection(){        
        if (sortExpression == null || sortExpression == '')
          return Ascending;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value){  
       sortDirection = value;
     } */
     
    public PEMEClientInvoiceDetailsCtrl(){ 
        pageNum = 1;
        strSubmitted=  'Approved';
        orderBy = '';
        lstUser = new List<User>([select firstName,lastName,ContactId,Contact.Name,Contact.Phone,Contact.MobilePhone,Contact.Email,Contact.Account.Name,Phone,Email from User where id=:UserInfo.getUserId()]);
        if(lstUser[0].ContactId!=null){
            loggedInContactId = lstUser[0].Contactid;
        }
        userContact = [Select FirstName,LastName,Id,Name,AccountId,Account.Name,Account.RecordTypeId,Email,Phone,MobilePhone,/*IsPeopleLineUser__c,*/ 
           Account.Company_Role__c from Contact where Id=:lstUser[0].ContactId];
       userName = userContact.Name;
        acmCS = AccountToContactMap__c.getValues(loggedInContactId);
        if(acmCS!=null && acmCS.name!=null && acmCS.name!='')
            {
                loggedInAccountId = acmCS.AccountId__c;
            }
        //loggedInAccountId = userContact.AccountId;
         acmLst = new List<Account_Contact_Mapping__c>([select id,IsPeopleClaimUser__c,PI_Access__c,Account__r.PEME_Enabled__c, account__r.company_Role__c 
                                                                                                from Account_Contact_Mapping__c where contact__c =:loggedInContactId 
                                                                                                and account__c=:loggedInAccountId]);
           if(acmLst.size()>0)
                {
                    isPeopleClaim = acmLst[0].IsPeopleClaimUser__c;
                }
    }
     public pageReference loadData()
     {
        //Account_Contact_Mapping__c accCont= [select PI_Access__c,Account__r.PEME_Enabled__c from Account_Contact_Mapping__c where contact__c =:loggedInContactId AND Account__c =:loggedInAccountId]; 
        if(acmLst != null && acmLst.size()>0 && (acmLst[0].PI_Access__c == false || acmLst[0].Account__r.PEME_Enabled__c == false))        
        {
            PageReference pg = new PageReference('/apex/HomePage');          
            return pg;
        }
        else
        {
            if(userContact.AccountId!=null){
                   //if(userContact.Account.Company_Role__c!=null && userContact.Account.Company_Role__c.contains('Client')){
                   if(acmCS.RolePreference__c.containsIgnoreCase(strClient)){ //SF-4516
                        lstExaminations = new set<String>(); 
                        lstObjects = new set<String>();        
                        lstExamDetails = new list<PEME_Exam_Detail__c>(); 
                        
                        List<User> lstUser = new List<User>([select firstName,lastName,ContactId,Contact.Name,Contact.Account.Name from User where id=:UserInfo.getUserId()]);
                        userName = lstUser[0].Contact.Name;
                        
                        //Added For Favourites - Start
                        favouriteId = ApexPages.currentPage().getParameters().get('favId');
                        if(favouriteId!=null && favouriteId!=''){
                            if((FavouritesCtrl.fetchFavsListById(favouriteId)).size()>0){
                                isFav = true;
                                selectedObject = new List<String>();
                                selectedExam = new List<String>();                   
                                fetchFavourites();
                            }else{
                                isFav = false;  
                            } 
                        }else{
                            isFav = false;
                        }                
                        //Added For Favourites - End
                        
                        if(selectedInvoice==null)    
                        selectedInvoice = ApexPages.currentPage().getParameters().get('invoices');
                        if(selectedInvoice!=null && selectedInvoice!=''){      
                            
                            objInvoice = [SELECT Client__c,Client__r.Name,Clinic_Invoice_Number__c,Clinic__c,Clinic__r.Name,Crew_Examined__c,Invoice_Date__c,PEME_reference_No__c,Status__c,ObjectList__c,
                            Total_Amount__c,Manning_agent__r.Client__r.Name,Vessels__c,GUID__c,Vessels__r.Name,Period_Covered_From__c,Period_Covered_To__c FROM PEME_Invoice__c where GUID__c=:selectedInvoice and Client__c =:loggedInAccountId];   
                            
                            generateExams();        
                        }
                        system.debug('lstExaminations.size() '+lstExaminations.size()); 
                        system.debug('lstObjects.size() '+lstObjects.size());        
                        system.debug('lstExamDetails.size() '+lstExamDetails.size());  
                    }
                    else
                    {
                        PageReference pg = new PageReference('/apex/HomePage');          
                        return pg;
                    }
                }
                return null;
            }
        }        
    public void generateExams(){
                
        pageNum = 1;
        lstExamDetails= new List<PEME_Exam_Detail__c>();
        orderByNew = 'Date__c';
      /*  if (jointSortingParam != null){
            system.debug('******jointSortingParam**************'+jointSortingParam);
            String[] arrStr = jointSortingParam .split('##');
            sortDirection = arrStr[0];
            sortExp = arrStr[1];
        }else{
            jointSortingParam = 'ASC##Date__c';           
        } */
        
        String strCondition ='';
        if(selectedExam!= null && selectedExam.size()>0){
            strCondition = ' AND Examination__c IN:selectedExam ';
        }       
        if(selectedObject!= null && selectedObject.size()>0){
            strCondition = strCondition +' AND Object_Related__r.Name IN : selectedObject ';                
        }
        
       // string sortFullExp = sortExpression  + ' ' + sortDirection;
        String strSubmitted =  'Approved';           
        sSoqlQuery ='SELECT Age__c,Amount__c,Date__c,Invoice__r.GUID__c,Examination__c,Examinee__c,Id,Invoice__c,Object_Related__c,Object_Related__r.Name,Rank__c,Status__c FROM PEME_Exam_Detail__c'+
                    ' where Invoice__r.GUID__c=:selectedInvoice and Status__c =:strSubmitted'+strCondition;
        orderBy = ' ORDER BY ' + orderByNew + ' ' + sortDirection; //For new sorting 
        sSoqlQuery = sSoqlQuery+ ' ' + orderBy;     
       // searchAssets();
        submissionSetConOpen = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery + limit10000)); 
        submissionSetConOpen.setPageSize(10);
        createOpenUserList();   
                             
        for(PEME_Exam_Detail__c objExam:[SELECT Age__c,Amount__c,Invoice__r.GUID__c,Date__c,Examination__c,Examinee__c,Id,Invoice__c,Object_Related__c,Object_Related__r.Name,Rank__c,Status__c FROM PEME_Exam_Detail__c
        where Invoice__r.GUID__c=:selectedInvoice and Status__c = 'Approved']){            
            lstExaminations.add(objExam.Examination__c);
            lstObjects.add(objExam.Object_Related__r.Name);              
        } 
    }
    Public void createOpenUserList()
    {
        if(submissionSetConOpen!=null)
        {
            lstExamDetails.clear(); 
            for(PEME_Exam_Detail__c exa : (List<PEME_Exam_Detail__c>)submissionSetConOpen.getRecords())             
                lstExamDetails.add(exa);
             //   System.debug('********'+lstExamDetails.size());         
        } 
    }
    //Returns to the first page of records
    public void firstOpen() {
        submissionSetConOpen.first(); 
        pageNum = submissionSetConOpen.getPageNumber();
        
        createOpenUserList();
        
    }
    public Boolean hasPreviousOpen   
    {   
        get   
        {   
            return submissionSetConOpen.getHasPrevious();   
        }   
        set;   
    } 
     //Returns the previous page of records   
    public void previousOpen()   
    {   
        submissionSetConOpen.previous();
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();  
    } 
    //to go to a specific page number
    public void setpageNumberOpen()
    {
        submissionSetConOpen.setpageNumber(pageNum); 
        createOpenUserList();
    }
    public Boolean hasNextOpen   
    {   
        get   
        {   
            return submissionSetConOpen.getHasNext();   
        }   
        set;   
    } 
    //Returns the next page of records   
    public void nextOpen()   
    {   
        submissionSetConOpen.next(); 
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();
        
    }
     //Returns to the last page of records
    public void lastOpen(){
        submissionSetConOpen.last(); 
        pageNum = submissionSetConOpen.getPageNumber();
        
        createOpenUserList();
        
    }
   /* public void searchAssets(){
        if(sSoqlQuery.length()>0){           
            System.debug('selectedExam--->'+selectedExam);            
            System.debug('selectedObject--->'+selectedObject);
            System.debug('Soql Query--->'+sSoqlQuery);  
            setCon = null;            
            
            lstExamDetails=(List<PEME_Exam_Detail__c>)setCon.getRecords();
            system.debug('****setCon.getResultSize()**********'+setCon.getResultSize());
            if(setCon.getPageNumber() == 1)
                intStartrecord = 1;
            else
                intStartrecord  = ((setCon.getPageNumber() - 1) * SMARTQUERY_PAGE_SIZE) + 1;
          
            intEndrecord = setCon.getPageNumber() * SMARTQUERY_PAGE_SIZE;
            if( intEndrecord > noOfRecords ){
                intEndrecord = noOfRecords ;
            }
        }
                                                              
    }
        
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                size = SMARTQUERY_PAGE_SIZE;
                System.debug('Soql Query--->'+sSoqlQuery);              
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery+limit10000));
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
                system.debug('****Total records****'+noOfRecords);
                if (noOfRecords == null || noOfRecords == 0){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No search results found.'));
                }else if (noOfRecords == 10000){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The search returned 10000 records (maximum allowable limit).'));
                }                
            }
            return setCon;
        }set;
    }  
    
    public void setpageNumber(){
        setCon.setpageNumber(pageNum);
        searchAssets();
    }
        
    public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }
    
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }
       
    public void navigate(){
        if(nameOfMethod=='previous'){
             setCon.previous();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }else if(nameOfMethod=='last'){
             setCon.last();
             searchAssets();
        }else if(nameOfMethod=='next'){
             setCon.next();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }else if(nameOfMethod=='first'){
             setCon.first();
             searchAssets();
        }
    }
        
    public void first() {
        nameOfMethod='first';
        navigate();
    }
    
    public void next(){
        nameOfMethod='next';
        pageNum = setCon.getPageNumber();
        navigate();        
    }
   
    public void last(){
        nameOfMethod='last';
        navigate();
    }
  
    public void previous(){
        nameOfMethod='previous';
         pageNum = setCon.getPageNumber();
        navigate();
    } */
    
    public pageReference clearOptions(){
        selectedExam.clear();
        selectedObject.clear();
        generateExams();
        return null;
    }
    
    public PageReference print(){
        lstExamsPrint = new List<PEME_Exam_Detail__c>();
        Integer count = 1;
        noOfData = 0;
      //  System.debug('******************'+sSoqlQuery);
        for(PEME_Exam_Detail__c mcl : Database.Query(sSoqlQuery+limit10000)){               
            if(count<1000){
                lstExamsPrint.add(mcl);                    
                count++;
            }else{                   
                count = 1;
            }   
            noOfData++;                 
        }
        return page.PrintforClientExamDetails;
    }
        
     public PageReference exportToExcel(){
        lstExamsPrint = new List<PEME_Exam_Detail__c>();
        Integer count = 1;
        noOfData = 0;
        for(PEME_Exam_Detail__c mcl : Database.Query(sSoqlQuery+limit10000)){               
            if(count<1000){
                lstExamsPrint.add(mcl);                    
                count++;
            }else{                   
                count = 1;
            }   
            noOfData++;                 
        }
        return page.GenerateExcelForClientExamDetails;
    }
    
    //Added For Favourites - Start                  
        public void createFavourites(){
         //   system.debug('createFavourites');
         //   system.debug('newFilterName: '+newFilterName);
            String currentUserId = UserInfo.getUserId();
            List<Extranet_Favourite__c> listExisting = new List<Extranet_Favourite__c>();
            
            String pageName = FavouritesCtrl.fetchPageName(ApexPages.CurrentPage().getUrl());                       
            listExisting.addAll(FavouritesCtrl.fetchExistingFavsList(pageName));
            String itemType = 'Saved Search';
            String query = sSoqlQuery + limit10000;
            
            if(newFilterName!=null && newFilterName!=''){               
                Extranet_Favourite__c objFav = new Extranet_Favourite__c();                
                objFav = FavouritesCtrl.createFavs(objFav, pageName, newFilterName,itemType, currentUserId, query, listExisting);
                objFav.Filter_Client__c = selectedInvoice;
                objFav.Filter_Object__c = FavouritesCtrl.generateFavFilters(selectedObject);
                objFav.Filter_Product_Area__c = FavouritesCtrl.generateFavFilters(selectedExam);                                     
                insert objFav;
                favouriteId = objFav.Id; 
            }   
        }  
        
        public void deleteFavourites(){
          //  system.debug('deleteFavourites');
            String currentUserId = UserInfo.getUserId();
            List<Extranet_Favourite__c> listFav = new List<Extranet_Favourite__c>();                        
            listFav.addAll(FavouritesCtrl.fetchFavsListById(favouriteId));
            if(listFav.size()>0){
                delete listFav;
            }
        }
        
        public void fetchFavourites(){
         //   system.debug('fetchFavourites');
         //   system.debug('favouriteId: '+favouriteId);
            String currentUserId = UserInfo.getUserId();
            List<Extranet_Favourite__c> listobj = new List<Extranet_Favourite__c>();
            listobj.addAll(FavouritesCtrl.fetchFavsListById(favouriteId));
            if(listobj.size()>0){
                selectedInvoice = listobj[0].Filter_Client__c;
                selectedObject.addAll(FavouritesCtrl.fetchFavFilters(listobj[0].Filter_Object__c));
                selectedExam.addAll(FavouritesCtrl.fetchFavFilters(listobj[0].Filter_Product_Area__c));                  
            }       
        }
        //Added For Favourites - End
        
}