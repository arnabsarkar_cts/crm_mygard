@isTest(seealldata = false)
Class TestBrokerClientControllerClinic
{
        
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    Public static AccountToContactMap__c map_clinic;
    Public static Account_Contact_Mapping__c Acmap1;
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    Public static accounttocontactmap__c map_broker, map_client;
    
    public Static testMethod void cover()
    {
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
        User user = new User(
                        Alias = 'standt', 
                        profileId = salesforceLicenseId ,
                        Email='standarduser@testorg.com',
                        EmailEncodingKey='UTF-8',
                        CommunityNickname = 'test134',
                        LastName='Testing',
                        LanguageLocaleKey='en_US',
                        LocaleSidKey='en_US',  
                        TimeZoneSidKey='America/Los_Angeles',
                        UserName='test008@testorg.com',
                        ContactId__c = gc.id
                         );
        insert user ;
        User user1 =new User();
        user1 = [select id, contactid__c from user where id=:user.id];
        
        Account clinicAcc = new Account( Name='testClinic',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    recordTypeId=System.Label.ESP_Contact_Record_Type,
                                    Site = '_www.ctsclinic.se',
                                    Type = 'Surveyor' ,
                                    Area_Manager__c = user1.id,      
                                    Market_Area__c = Markt.id                                                                    
                                 );
        AccountList.add(clinicAcc);
        insert AccountList; 
        Contact clinicContact = new Contact( 
                                         FirstName='medicalTest',
                                         LastName='medical',
                                         MailingCity = 'London',
                                         MailingCountry = 'United Kingdom',
                                         MailingPostalCode = 'SE1 1AD',
                                         MailingState = 'London',
                                         MailingStreet = '1 London Road',
                                         AccountId = clinicAcc.Id,
                                         Email = 'test321@gmail.com'
                                       );
        ContactList.add(clinicContact) ;
        
        insert ContactList;
        
        User clinicUser = new User(
                                Alias = 'Clinic', 
                                profileId = partnerLicenseId,
                                Email='testClinic120@test.com',
                                EmailEncodingKey='UTF-8',
                                LastName='ClinicTesting',
                                LanguageLocaleKey='en_US',
                                CommunityNickname = 'clinicUser',
                                LocaleSidKey='en_US',  
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName='testClinicUser@testorg.com.mygard',
                                ContactId = clinicContact.Id
                               );
        UserList.add(clinicUser);
        insert UserList;
        map_clinic = new AccountToContactMap__c(AccountId__c= clinicAcc.id,Name=clinicContact.id,RolePreference__c='Clinic');
        insert map_clinic; 
        
        ClinicAddressBook__c addBookCS2 = new ClinicAddressBook__c();
        addBookCS2.UserId__c = clinicUser.id;
        addBookCS2.Name = 'PEME Claims executive,Claims';
        insert addBookCS2;
        
        Acmap1 = new Account_Contact_Mapping__c(Account__c = clinicAcc.id,
                                                   Active__c = true,
                                                   Administrator__c = 'Admin',
                                                   Contact__c = clinicContact.id,
                                                   IsPeopleClaimUser__c = false,
                                                   Marine_access__c = true,
                                                   PI_Access__c = true
                                                   //Show_Claims__c = false,
                                                   //Show_Portfolio__c = false 
                                                 );
        insert Acmap1;
        System.runAs(clinicUser) 
        {
            system.debug('------>user contactID'+[select id,Contactid from User where id=:UserInfo.getUserId()]+'---->clinicContactID---->'+clinicContact.id+'------>AccountToContactMap__c.getValues(loggedInContactId)--->'+AccountToContactMap__c.getValues(clinicContact.id));
            BrokerClientController brokerClientCnt1 = new BrokerClientController();
            brokerClientCnt1.setClientId.clear();
            
            //System.debug('***setClientId '+brokerClientCnt1.setClientId);
            brokerClientCnt1.strGlobalClient = clinicAcc.id;
            brokerClientCnt1.selectedGlobalClient = new list<string>();
            brokerClientCnt1.selectedGlobalClient.add(clinicAcc.Name);
            System.assert(brokerClientCnt1.selectedGlobalClient.size() > 0 , true);
            //brokerClientCnt1.getItems();
            //brokerClientCnt1.getImageUrl('');
            brokerClientCnt1.setClientId.add(clinicAcc.id);
            brokerClientCnt1.addAddressList();
            brokerClientCnt1.refreshList();
            //brokerClientCnt1.orderByUnderwriter();
            //brokerClientCnt1.orderByUnderwriter();
            //brokerClientCnt1.orderByClaimsHandler();
            ////brokerClientCnt1.orderByClaimsHandler();
            //brokerClientCnt1.orderByAccContact();
            //brokerClientCnt1.orderByAccContact();
            brokerClientCnt1.option = clinicAcc.Id;
            brokerClientCnt1.section = 'UWR';
            brokerClientCnt1.printData();
            brokerClientCnt1.section = 'Claim';
            brokerClientCnt1.printData();
            brokerClientCnt1.section = 'AccCont';
            brokerClientCnt1.printData();
            brokerClientCnt1.strselected = clinicAcc.id+';'+clinicAcc.id;
            pageReference pg = brokerClientCnt1.fetchSelectedClients();
            addBookCS2.Name = 'PEME Manning Director';
            update addBookCS2;
            brokerClientCnt1.addAddressList();
            addBookCS2.Name = 'PEME Vice President';
            update addBookCS2;
            brokerClientCnt1.addAddressList();
            addBookCS2.Name = 'Peme Claims executive, People claims';
            update addBookCS2;
            brokerClientCnt1.addAddressList();  
            addBookCS2.Name = 'Claims executive, People claims';
            update addBookCS2;
            brokerClientCnt1.addAddressList();  
         }
    }
}