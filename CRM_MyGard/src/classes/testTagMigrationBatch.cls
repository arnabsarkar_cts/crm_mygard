@isTest(seeAllData = false)
public class testTagMigrationBatch{
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id; 
    public static Account newAcc;
    public static Country__c country;
    public static Market_Area__c Markt;
    public static user salesforceLicUser;
    
         
        //GardTestData test_data = new GardTestData();
        //test_data.commonRecord();
    public static void createTestData(){
        country = new Country__c();
        Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
        salesforceLicUser = new User(
                                        Alias = 'aliasT', 
                                        profileId = salesforceLicenseId ,
                                        Email= 'test@test.com',
                                        EmailEncodingKey='UTF-8',
                                        CommunityNickname = 'testing tests',
                                        LastName='test LastName',
                                        LanguageLocaleKey= 'en_US',
                                        LocaleSidKey= 'en_US', 
                                        //contactID = brokercontact.id, 
                                        TimeZoneSidKey= 'America/Los_Angeles' ,
                                        UserName='mygardtest008@testorg.com.mygard'
                                   );
         insert salesforceLicUser;
        newAcc = new Account(Name = 'Test Account' , 
                                    BillingStreet = 'Test Street',
                                    BillingCity = 'phuket', 
                                    BillingCountry =  'test country'  , 
                                    BillingPostalCode =  'test code' ,
                                    BillingPostalCode__c= 'test',
                                    BillingCity__c= 'test',
                                    Country__c=country.id,
                                    Market_Area__c = Markt.id,
                                    Area_Manager__c = salesforceLicUser.id
                                    );
        }
    @isTest static void testTagMigration1(){
        createTestData();
        test.startTest();
        Database.BatchableContext BC;
        Database.QueryLocator QL;
        tagMigrationBatch tmb = new tagMigrationBatch();
        ID batchprocessid = Database.executeBatch(tmb);
        
        //QL = tmb.start(BC);
        //tmb.finish(BC);
        test.stopTest();
    }
    @isTest static void testTagMigration2(){
        createTestData();
        List<Account> accList = [SELECT id FROM Account WHERE Id =: newAcc.Id ];
        Database.BatchableContext BC;
        tagMigrationBatch tmb = new tagMigrationBatch();
        tmb.execute(BC,accList);
    }
}