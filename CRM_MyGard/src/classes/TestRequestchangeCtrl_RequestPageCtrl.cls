//Test method for RequestChangeCtrl .
@istest(seealldata = false)
public class TestRequestchangeCtrl_RequestPageCtrl{    
    //Constant Variables
    private static final string expDateStr =  '03.01.2014';
    private static final string piStr =   'P&I';
    
    //Local use variables
    private static GardTestData gtdInstance;
    
    //createTestData
    private static void createTestData(){
        System.debug('SOQL debug before GardTestData() SOQL used - '+Limits.getQueries());
        gtdInstance = new GardTestData();
        //gtdInstance.customsettings_rec(); 
        System.debug('SOQL debug before commonRecord() SOQL used - '+Limits.getQueries());
        gtdInstance.commonrecord();
        System.debug('SOQL debug after commonRecord() SOQL used - '+Limits.getQueries()); 
        Test.startTest();      
        gtdInstance.Requestchange_testrecord(); 
        System.debug('SOQL debug after reqChangeCtrl testRecord() SOQL used - '+Limits.getQueries());
    }
    
    //Test Method
    //@isTest public static void TestRequestchangeCtrl_RequestPageCtrl1(){
    @isTest public static void testWithReqType1(){
        createTestData();
        System.runAs(GardTestData.brokerUser){
            RequestChangeCtrl rccInstance = new RequestChangeCtrl();
            System.debug('SOQL debug after constructor SOQL used - '+Limits.getQueries());
            rccInstance.selectedCover = GardTestData.brokerAsset_1st.product_name__c;    
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.clientName = GardTestData.clientAcc.Name;
            rccInstance.effctvDate= expDateStr ;//date.valueof('2014-01-03');
            rccInstance.tonnage = 1000;
            rccInstance.typeOfObj = 'test';
            rccInstance.selectedYrBuilt='2014';
            rccInstance.selectedObj.add(GardTestData.test_object_1st.id);        
            rccInstance.selectedObj.add(GardTestData.test_object_3rd.id); 
            rccInstance.reqType=1;       
            System.debug('SOQL debug after LIMIT refresh SOQL used - '+Limits.getQueries());
            rccInstance.populatefromObject();
            rccInstance.btnClicked = 'New'; 
            System.assertEquals( rccInstance.tonnage , 1000);
            // for type-1
            rccInstance.selectedReqType = 'Request Change to terms';
            //rccInstance.sendType();
            rccInstance.userType = 'Broker';
            rccInstance.populateObject();
            rccInstance.UWRId = GardTestData.salesforcelicuser.id;
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.effctvDate= expDateStr ;//date.valueof('2014-01-03');
            rccInstance.selectedprodArea= piStr ;
            rccInstance.SelectedObhName=GardTestData.test_object_1st.name;
            rccInstance.objMode = true;
            rccInstance.onSubmit();
            rccInstance.popUpClose();
            rccInstance.attachMailtoCaseopp(rccInstance.reqType);
            // for type-1 objMode on                
            rccInstance.prvUrl='MyObjectDetails?objid='+GardTestData.test_object_1st.id;
            rccInstance.selectedReqType='change in assureds';
            rccInstance.reqType=1;
            rccInstance.sendType();
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.effctvDate= expDateStr ;//date.valueof('2014-01-03');
            rccInstance.selectedprodArea= piStr ;
            rccInstance.SelectedObhName=GardTestData.test_object_1st.name;
            //  rccInstance.onSubmit();
            rccInstance.btnClicked = 'New';
            rccInstance.popUpClose();
            /*
            // for type-2            
            rccInstance.SelectedObhName = null; 
            rccInstance.reqType=2;       
            rccInstance.selectedReqType='additional covers for existing objects';
            rccInstance.sendType();
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.effctvDate= expDateStr ;//date.valueof('2014-01-03');
            rccInstance.selectedprodArea= piStr ;
            rccInstance.SelectedObhName=GardTestData.test_object_1st.name;
            rccInstance.onSubmit();
            rccInstance.btnClicked = 'Save';
            rccInstance.popUpClose();
            */
            /*
            // for type-3
            rccInstance.prvUrl = '';    
            rccInstance.selectedMultiCover.add(GardTestData.brokerAsset_1st.product_name__c);
            rccInstance.selectedReqType='covers for new objects';
            rccInstance.sendType();
            rccInstance.phn = true;
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.effctvDate= expDateStr ;//date.valueof('2014-01-03');
            rccInstance.selectedprodArea= piStr ;
            rccInstance.SelectedObhName=GardTestData.test_object_1st.name;
            Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
            rccInstance.caseTestId = singleCase.ID;
            System.debug('SOQL debug GardTestData.brokerAcc.Type '+GardTestData.brokerAcc.Type);
            rccInstance.onSubmitNew();
            rccInstance.btnClicked = 'New';
            // rccInstance.oppObj.RecordTypeId = [SELECT Id FROM RecordType where name ='P&I' and SobjectType = 'opportunity'].id;
            //rccInstance.createOpportunity();
            //rccInstance.popUpClose();
            //testb.onSubmitNew();
            rccInstance.onCancel();
            */
            Test.stopTest();
        } 
    }
    
    @isTest public static void testWithReqType2(){
        createTestData();
        System.runAs(GardTestData.brokerUser){
            RequestChangeCtrl rccInstance = new RequestChangeCtrl();
            //System.debug('SOQL debug after constructor SOQL used - '+Limits.getQueries());
            rccInstance.selectedCover = GardTestData.brokerAsset_1st.product_name__c;    
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.clientName = GardTestData.clientAcc.Name;
            rccInstance.effctvDate= expDateStr ;//date.valueof('2014-01-03');
            rccInstance.tonnage = 1000;
            rccInstance.typeOfObj = 'test';
            rccInstance.selectedYrBuilt='2014';
            rccInstance.selectedObj.add(GardTestData.test_object_1st.id);        
            rccInstance.selectedObj.add(GardTestData.test_object_3rd.id);  
            //System.debug('SOQL debug after LIMIT refresh SOQL used - '+Limits.getQueries());
            rccInstance.populatefromObject();
            rccInstance.btnClicked = 'New'; 
            System.assertEquals( rccInstance.tonnage , 1000);
            // for type-2            
            rccInstance.SelectedObhName = null; 
            rccInstance.reqType=2;       
            rccInstance.selectedReqType='additional covers for existing objects';
            rccInstance.sendType();
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.effctvDate= expDateStr ;//date.valueof('2014-01-03');
            rccInstance.selectedprodArea= piStr ;
            rccInstance.SelectedObhName=GardTestData.test_object_1st.name;
            rccInstance.onSubmit();
            rccInstance.btnClicked = 'Save';
            rccInstance.popUpClose();
            Test.stopTest();
        }
    }
    
    @isTest public static void testWithReqType3(){
        createTestData();
        System.runAs(GardTestData.brokerUser){
            RequestChangeCtrl rccInstance = new RequestChangeCtrl();
            //System.debug('SOQL debug after constructor SOQL used - '+Limits.getQueries());
            rccInstance.selectedCover = GardTestData.brokerAsset_1st.product_name__c;    
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.clientName = GardTestData.clientAcc.Name;
            rccInstance.effctvDate= expDateStr ;//date.valueof('2014-01-03');
            rccInstance.tonnage = 1000;
            rccInstance.typeOfObj = 'test';
            rccInstance.selectedYrBuilt='2014';
            rccInstance.selectedObj.add(GardTestData.test_object_1st.id);        
            rccInstance.selectedObj.add(GardTestData.test_object_3rd.id);  
            //System.debug('SOQL debug after LIMIT refresh SOQL used - '+Limits.getQueries());
            rccInstance.populatefromObject();
            rccInstance.btnClicked = 'New'; 
            System.assertEquals( rccInstance.tonnage , 1000);
            rccInstance.prvUrl = '';    
            rccInstance.selectedMultiCover.add(GardTestData.brokerAsset_1st.product_name__c);
            rccInstance.selectedReqType='covers for new objects';
            rccInstance.sendType();
            rccInstance.phn = true;
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.effctvDate= expDateStr ;//date.valueof('2014-01-03');
            rccInstance.selectedprodArea= piStr ;
            rccInstance.SelectedObhName=GardTestData.test_object_1st.name;
            Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
            rccInstance.caseTestId = singleCase.ID;
            System.debug('SOQL debug GardTestData.brokerAcc.Type '+GardTestData.brokerAcc.Type);
            rccInstance.onSubmitNew();
            rccInstance.btnClicked = 'New';
            // rccInstance.oppObj.RecordTypeId = [SELECT Id FROM RecordType where name ='P&I' and SobjectType = 'opportunity'].id;
            //rccInstance.createOpportunity();
            //rccInstance.popUpClose();
            //testb.onSubmitNew();
            rccInstance.onCancel();
            Test.stopTest();
        }
    }
}