@isTest
private class TestUpdateCompanyDetails {
//public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
	public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    private static void setUpdata(){   
   // User usr = new User(
    //                            Alias = 'standt', 
    //                            profileId = salesforceLicenseId ,
    //                            Email='standarduser@testorg.com',
    //                            EmailEncodingKey='UTF-8',
    //                            CommunityNickname = 'test13',
    //                            LastName='Testing',
    //                            LanguageLocaleKey='en_US',
    //                            LocaleSidKey='en_US',  
    //                            TimeZoneSidKey='America/Los_Angeles',
   //                             UserName='test008007@testorg.com'
   //                                  );
        
   //     insert usr; 
    
   //     Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
   //     insert Markt; 
         
    
       //create an account record
        Account acc = new Account(  Name='APEXTESTACC001',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id,
                                    Market_Area__c = TestDataGenerator.getMarketArea().Id
                                   // Market_Area__c = Markt.id,
                                   // Area_Manager__c = usr.id
                                );
       insert acc;
       String strAccId = acc.Id;
     
     //create Contact 
        system.assertNotEquals(null, acc.Id);
        Contact con = new Contact();
        con.LastName='Sutherland';
        con.AccountId = strAccId;
        insert (con);
        String strConId = con.Id;
      
    Event evt = new Event(  Subject='TESTCOVERAGE', 
                                StartDateTime=datetime.now().addDays(1), 
                                EndDateTime=datetime.now().addDays(2),
                                WhoId = strConId,
                                WhatId = strAccId);
        insert evt;
    
     }
      
    static testMethod void testMM(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
      setUpdata();
        
      Event e = [select id from Event where subject='TESTCOVERAGE' order by CreatedDate  desc limit 1]; 
      Meeting_Minute__c a = new Meeting_Minute__c(event_id__c = e.Id);
      insert(a);
      String strmmId = a.Id;
        
        
         Meeting_Minute__c mm = [SELECT ID, Event_Id__c FROM Meeting_Minute__c WHERE ID =: strmmId ];
         Event evt = [SELECT ID, WhatId FROM EVENT WHERE ID =: mm.Event_Id__c];
         
         Customer_Feedback__c feedback = new Customer_Feedback__c(Title__c='TESTFEEDBACK', 
                                From_Meeting_Minute__c = a.Id, 
        //                        From_Company__c=evt.WhatId,
                                Type_of_feedback__c='Positive feedback',
                                Source__c = 'Meeting');
         insert feedback;
         String feedbackId = feedback.Id;
    
        Customer_Feedback__c fback= [SELECT ID, From_Company__c,Source__c FROM Customer_Feedback__c WHERE ID =: feedbackId];
        System.assertEquals(fback.From_Company__c, evt.WhatId);
        //System.assertEquals(fback.Source__c, 'Meeting');
        
    }
}