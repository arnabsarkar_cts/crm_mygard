@istest
Public Class TestMultipleCompanyCtrl{
    
    public static testmethod void multicompctrl(){
        gardtestdata test_rec = new gardtestdata();
        test_rec.multicmpctrl();
        System.runAs(test_rec.brokeruser_multi){
            PageReference pg = page.MultipleCompany;
            test.setcurrentpage(page.MultipleCompany);
            MultipleCompanyCtrl testobj = new MultipleCompanyCtrl();
            //Page pg = page.MultipleCompany;
            //test.setcurrentpage(page.MultipleCompany);
            //Apexpages.currentPage().getparameters().put('id', testobj.url); 
            //testobj.url =url_id;
            //MultipleCompanyCtrl testobj1 = new MultipleCompanyCtrl(); 
            //Test.setCurrentPageReference(new PageReference('Page.MultipleCompany'));
            //System.currentPageReference().getParameters().put(testobj.url, 'id');
            testobj.checkIfMultiCompany();
            //testobj.activeRecordSize = 0 ;
            //testobj.checkIfMultiCompany();
            // testobj.selectedAccContId = test_rec.Accmap_1st_multi.id;
            // testobj.mapCompany.put(test_rec.Accmap_1st_multi.id,test_rec.Accmap_1st_multi);
            //if( testobj.isMultiCompanyUser){
            testobj.selectedAccContId = test_rec.Accmap_1st_multi.id;
            testobj.selectedACMWrapperId = 0;
            testobj.setCompanyForMultiple();
            System.assertEquals(testobj.redirect , true);
            testobj.setCompanyForMultiple();
            //}
            testobj.redirect = true;
            testobj.cancel();  
            testobj.redirect = false;
            testobj.cancel();
        }
        Test.startTest();
        System.runAs(test_rec.clientuser_multi){
            
            //PageReference pg1 = page.MultipleCompany;
            Id preservedAccountId = test_rec.Accmap_2nd_multi.id;
            test.setcurrentpage(page.MultipleCompany);
            Apexpages.currentPage().getparameters().put('id', 'pg=r');
            MultipleCompanyCtrl testobj2 = new MultipleCompanyCtrl();
            testobj2.checkIfMultiCompany();
            testobj2.selectedAccContId = test_rec.Accmap_2nd_multi.id;
            testobj2.selectedACMWrapperId = 0;
            //testobj2.mapCompany.put(test_rec.Accmap_2nd_multi.id,test_rec.Accmap_2nd_multi);
            //if( testobj2.isMultiCompanyUser){
            testobj2.setCompanyForMultiple();
            //delete test_rec.Accmap_4th_multi;
            //testobj2.setCompanyForMultiple();
            //}
        }
        /*
        system.runAs(test_rec.clientUser_multi1){
            test.setcurrentpage(page.MultipleCompany);
            Apexpages.currentPage().getparameters().put('id', 'pg=r');
            MultipleCompanyCtrl testobj1 = new MultipleCompanyCtrl(); 
            testobj1.checkIfMultiCompany();
            testobj1.selectedAccContId = test_rec.Accmap_1st_multi.id;
            testobj1.selectedACMWrapperId = 0;
            //testobj1.selectedAccContId = preservedAccountId;
            //System.debug('sfEditTestClass selectedACMWrapperId - '+testobj1.selectedACMWrapperId+' - '+test_rec.Accmap_2nd_multi.id+'/'+preservedAccountId);
            //testobj1.mapCompany.put(test_rec.Accmap_2nd_multi.id,test_rec.Accmap_2nd_multi);
            //if( testobj1.isMultiCompanyUser){
            testobj1.setCompanyForMultiple();
            //}
        }
		*/
        Integer acmCount = 0;
        for(Account_Contact_Mapping__c acm : [SELECT id,account__c,contact__c FROM Account_Contact_Mapping__c Order BY contact__c]){
            acmCount++;
            System.debug('sfEditTestClass acm - '+acm);
        }
        System.debug('sfEditTestClass acm count - '+acmCount);
        Test.stopTest();
    }
}