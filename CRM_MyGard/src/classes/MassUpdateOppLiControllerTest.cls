@isTest(seeAllData=true)
public class MassUpdateOppLiControllerTest
{ 

    //### TEST METHODS ###
    public static testMethod void testMassUpdateOppLiController() {
        //set up test records
        //GardTestData gtd = new GardTestData();
        //gtd.commonRecord();
        String sfdcProfId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        User salesforceLicUser = new User(
                                        Alias = 'standt', 
                                        profileId = sfdcProfId ,
                                        Email='standarduser@testorg.com',
                                        EmailEncodingKey='UTF-8',
                                        CommunityNickname = 'test13',
                                        LastName='Testing',
                                        LanguageLocaleKey='en_US',
                                        LocaleSidKey='en_US',  
                                        TimeZoneSidKey='America/Los_Angeles',
                                        UserName='yoo008@testorgo.com',
                                        City= 'Arendal'
                                        );
        
        insert salesforceLicUser ;
        Test.StartTest();
        system.runAs(salesforceLicUser  )
        {
        date dToday = date.Today();
        PriceBookEntry pbi = [Select Id, Product2Id, CurrencyISOCode from PriceBookEntry WHERE IsActive = true LIMIT 1];
        Product2 prod = [Select Name, Id from Product2 where Id = :pbi.Product2Id LIMIT 1];
        String budgetYear = '2014';//PrognosisConfigSupport.GetSetting(UserInfo.getUserId()).Prognosis_Year__c;
        
        String stageName = 'Renewable Opportunity';
        String oppType = 'Renewal';
        System.debug('*** Budget Year = ' + budgetYear);
        Opportunity opp1 = new Opportunity(Name='1TEST CLASS OPP 1', OwnerId=userInfo.getUserId(), Type =oppType,budget_status__c='Budget In Progress', Budget_Year__c=budgetYear,CurrencyIsoCode=pbi.CurrencyIsoCode,closeDate=dToday,StageName=stageName);
        Opportunity opp2 = new Opportunity(Name='2TEST CLASS OPP 2', OwnerId=userInfo.getUserId(), Type =oppType,budget_status__c='Budget In Progress', Budget_Year__c=budgetYear,CurrencyIsoCode=pbi.CurrencyIsoCode,closeDate=dToday,StageName=stageName);
        Opportunity opp3 = new Opportunity(Name='3TEST CLASS OPP 3', OwnerId=userInfo.getUserId(), Type =oppType,budget_status__c='Budget In Progress', Budget_Year__c=budgetYear,CurrencyIsoCode=pbi.CurrencyIsoCode,closeDate=dToday,StageName=stageName);
        Insert opp1;Insert opp2;Insert opp3;
        System.debug('*** Opp 1 = ' + opp1);
        
        OpportunityLineItem oli1_1 = new OpportunityLineItem(OpportunityId=opp1.Id, PricebookEntryId = pbi.Id, Quantity=1, UnitPrice=100, Renewable_Probability__c=100, Line_Size__c = 1.0);
        OpportunityLineItem oli1_2 = new OpportunityLineItem(OpportunityId=opp1.Id, PricebookEntryId = pbi.Id, Quantity=1, UnitPrice=100, Renewable_Probability__c=100, Line_Size__c = 1.0);
        OpportunityLineItem oli2_1 = new OpportunityLineItem(OpportunityId=opp2.Id, PricebookEntryId = pbi.Id, Quantity=1, UnitPrice=100, Renewable_Probability__c=100, Line_Size__c = 1.0);
        OpportunityLineItem oli2_2 = new OpportunityLineItem(OpportunityId=opp2.Id, PricebookEntryId = pbi.Id, Quantity=1, UnitPrice=100, Renewable_Probability__c=100, Line_Size__c = 1.0);
        OpportunityLineItem oli3_1 = new OpportunityLineItem(OpportunityId=opp3.Id, PricebookEntryId = pbi.Id, Quantity=1, UnitPrice=100, Renewable_Probability__c=100, Line_Size__c = 1.0);
        OpportunityLineItem oli3_2 = new OpportunityLineItem(OpportunityId=opp3.Id, PricebookEntryId = pbi.Id, Quantity=1, UnitPrice=100, Renewable_Probability__c=100, Line_Size__c = 1.0);
        Insert oli1_1;Insert oli1_2;Insert oli2_1;Insert oli2_2;Insert oli3_1;Insert oli3_2;
        List<OpportunityLineItem> testOli = new List<OpportunityLineItem>{oli1_1,oli1_2,oli2_1,oli2_2,oli3_1,oli3_2};
        system.debug('*** OLI = ' + testOli);
        // controller for valid opportunities
        MassUpdateOppLiController oli = new MassUpdateOppLiController();
        oli.budgetYear = budgetYear;
        oli.PopulateOpportunityLineItems();
        //execute test case

        //List<OpportunityLineItem> opps = oli.getOpportunityLineItems();
            
           // system.debug('*** Opps = ' + opps);
           // system.assert(opps.size()>0);
            oli.pageSize = 2;//system.assert(oli.currPage == 0);
            oli.pageNext();//system.assert(oli.currPage == 2);
            oli.pagePrev();//system.assert(oli.currPage == 0);
            oli.pageNext();//system.assert(oli.currPage == 2);
            oli.pageFirst();//system.assert(oli.currPage == 0);
            //oli.OpportunityLineItems[0].UnitPrice = 200;
            oli.saveAll();
            //This won't work now as there is a workflow updating UnitPrice to be EPI for 2014 onwards
            //system.assert([Select UnitPrice from OpportunityLineItem where Id = :oli.OpportunityLineItems[0].Id].UnitPrice == 200);
            oli.filterText='2TEST';
            oli.setFilterText();
            //opps = oli.getOpportunityLineItems();
            for (OpportunityLineItem li : oli.OpportunityLineItems) {
                //system.assert(li.Opportunity.Name.startsWith('2TEST')); 
            }
             PageReference pageRef = Page.BulkApprovalNew;
            Test.setCurrentPage(pageRef);
            oli.OppPage();
            oli.budgetYearList.add(new SelectOption('2011','2011'));
            oli.productFamilyList.add(new SelectOption('Test','Test'));
            oli.productFamily = 'Show all';
            //oli.getOpportunityLineItems();
        }
          Test.StopTest(); 
    }
}