public with sharing class WebServiceUtil {
    /*public static String getSoapHeader() 
    {
        String soapMessage = '';    
        soapMessage+='<soapenv:Header />';
        return soapMessage;
    }*/
    
    public static Dom.Document processRequest(String ep, String msg, String actionURL) 
    {
        Dom.Document doc;
        System.debug('EndPoint:'+ep);
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        if (actionURL!=null && actionURL!='') 
            req.setHeader('SOAPAction', actionURL);
        else 
            req.setHeader('SOAPAction', '""');
        req.setHeader('Content-Type', 'text/xml;charset=utf-8');
        req.setEndpoint(ep);
        req.setBody(msg);       
        req.setTimeout(20000);

        HttpResponse res;
        if (!Test.isRunningTest()) {
            System.debug('Web Service Call request: '+String.valueOf(req));
            res = h.send(req);   
            System.debug('Web Service Call response: '+String.valueOf(res));
            doc = res.getBodyDocument();
        } else {
            // Test version of response for Apex Unit Tests
            System.debug('*** MOCK Web Service Call request: ' + String.valueOf(req));
            WebServiceUtilTests.Instance.MockWebServiceRequest = req;
            if (WebServiceUtilTests.Instance.MockWebServiceResponse != null && WebServiceUtilTests.Instance.MockWebServiceResponse instanceOf Dom.Document) {
                doc = (Dom.Document)WebServiceUtilTests.Instance.MockWebServiceResponse;
                System.debug('*** MOCK Web Service Call response: ' + doc.toXmlString());
            }
        }
        try {
            Logger.LogCallout(msg, doc.toXmlString());
        } catch (Exception e) {
            System.debug('Exception ******'+e);
            //Don't let logging cause an exception...
        }
        return doc;
    } 
    
    ///Obselete code, used for connecting to Enterprise WSDL in initial service development...
    /*
    public static String LoginToEnterpriseWSDL(String username, String password) {
        DOM.Document xmlDoc = new DOM.Document();
      
        String soapNS = MDMConfig__c.getInstance().SOAPNS__c; //String xsi = 'http://www.w3.org/2001/XMLSchema-instance';
        
        String prefix = 'urn';
        String serviceNS = 'urn:enterprise.soap.sforce.com';

        DOM.XmlNode envelope = xmlDoc.createRootElement('Envelope', soapNS, 'soapenv');
        envelope.setNamespace(prefix, serviceNS);
        
        DOM.XMLNode body = envelope.addChildElement('Body', soapNS, null);
        DOM.XMLNode login = body.addChildElement('login', serviceNS, prefix);
        login.addChildElement('username', serviceNS, prefix).addTextNode(username);
        login.addChildElement('password', serviceNS, prefix).addTextNode(password);
        

        system.debug('Request: ' + xmlDoc.toXMLString());
        
        DOM.Document loginResponse = processRequest('https://test.salesforce.com/services/Soap/c/28.0/0DFc00000004CZ1', xmlDoc.toXMLString(), null);
        
        system.debug('Response: ' + loginResponse.toXMLString());
        
        try {
            system.debug('Session ID: ' + loginResponse.getRootElement().getChildElement('Body', soapNS).getChildElement('loginResponse', serviceNS).getChildElement('result', serviceNS).getChildElement('sessionId', serviceNS).getText());
            return loginResponse.getRootElement().getChildElement('Body', soapNS).getChildElement('loginResponse', serviceNS).getChildElement('result', serviceNS).getChildElement('sessionId', serviceNS).getText();
        } catch (Exception ex) {
            return null;
        }

    }
    
    public static String getMDMTestSession() {
        return LoginToEnterpriseWSDL('mdmtest@gard.no.mdmdev','6gyhsdHrJmI3UcURXYDswRQ4SIIxOMPfYDLT3ehxI71vYHTXWZD8I7HzpIx5');
    }
    */
}