@istest(seealldata = false)

public class TestRequestchangeCtrl_RequestPageCtrl2
{    
    private static string testStr = 'test'; 
    private static string standtStr = 'standt';
    private static string utfStr = 'UTF-8';
    private static string test13Str = 'test13';
    private static string testingStr = 'Testing';
    private static string enUsrStr = 'en_US';
    private static string tymZnStr =  'America/Los_Angeles' ;
    private static string bilCntryStr =  'United Kingdom' ;
    private static string bilCntryStr2 =  'SWE'  ;
    private static string typeClStr = 'client';
    private static string typeBrStr =  'Broker';
    private static string typeSubBrStr = 'Broker - Reinsurance Broker'; //sfedit
    private static string piStr =  'P&I';
    private static string marineStr =  'Marine';
    private static string postlCdStr =  'BS1 1AD';
    private static string otherPhnStr =  '1112223356' ;
    private static string mblPhnStr =   '1112223334';
    private static string mailngStateStr =  'London' ;
    private static string mailStr =  'test321@gmail.com';
    private static string isoCodeStr =  'SEK';
    private static string expDateStr =  '2015-01-01';
    private static string agrmntTypCdStr =  'Agreement123';
    private static string polYrStr =  '2015';
    private static string cntrctNmStr =  'ABC';
    private static string objSubTypStr =  'Accommodation Ship' ;
    private static string csStr =  'CS 1' ;
    private static string flagStr =  'Albania';
    private static string rebuiltStr =  '2014-01-01';
    private static string imoStr = '4355555';
    private static string yrBuiltStr =  '2013-01-01';
    private static string cntryISOStr =  'USD';
    private static string orignStr =  'Community';
    private static Country__c country;
    private static string stdUsrMailStr = 'standarduser@testorg.com';
    private static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    private static string partnerLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Partner Community' and name='Partner Community Login User Custom' limit 1].id;
    private static string draftStr =  'Draft';
    private static string incptnDateStr =  '2012-01-01';
    
    private static GardTestData gtdInstance;
    //Data creation to be moved to GardTestData
    /*
    private static Gard_Contacts__c grdobj;
    private static User salesforceLicUser;
    private static Account clientAcc;
    private static Account cAcc;
    private static Account brokerAcc ;
    private static Contact brokerContact;
    private static User brokerUser;
    private static Object__c test_object_1st;
    private static Object__c test_object_2nd;
    private static Object__c test_object_3rd;
    private static Contract brokercontract_1st;
    private static Asset brokerAsset_1st;
    private static Case brokerCase;
    */
    private static void createTestData(){
        gtdInstance = new GardTestData();
        gtdInstance.commonRecord();
        /*
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting; 
        //Valid Role Combination----
        
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrcClient);
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        vrcList.add(vrcBroker);
        insert vrcList;
        Gard_Team__c gt = new Gard_Team__c();
        gt.Active__c = true;
        gt.Office__c = testStr;
        gt.Name = 'GTeam';
        gt.Region_code__c = testStr;
        insert gt;
        country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Gard_Team__c=gt.Id,Claims_Support_Team__c=gt.Id);
        insert country;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;  
        grdobj  =    new  Gard_Contacts__c(
            FirstName__c = 'Testreqchng',
            LastName__c = 'Baann',
            Email__c = 'WR_gards.12@Test.com',
            MobilePhone__c = '548645', 
            Nick_Name__c  = 'NilsPeter', 
            Office_city__c = 'Arendal', 
            Phone__c = '5454454',
            Portal_Image__c ='<img alt="User-added image" src="https://c.cs8.content.force.com/servlet/rtaImage?eid=a1hL0000000kSyZ&amp;feoid=00NL0000003OySC&amp;refid=0EML00000008Zjs"></img>', 
            Title__c = testStr 
        );
        insert grdobj ;
        salesforceLicUser = new User(
            Alias = standtStr , 
            profileId = salesforceLicenseId ,
            Email=stdUsrMailStr ,
            EmailEncodingKey=utfStr ,
            CommunityNickname = test13Str ,
            LastName=testingStr ,
            LanguageLocaleKey= enUsrStr ,
            LocaleSidKey= enUsrStr , 
            TimeZoneSidKey= tymZnStr ,
            UserName='mygardtest008@testorg.com.mygard',
            contactId__c = grdobj.id,
            City= 'Arendal'
            
        );
        insert salesforceLicUser;
        
        clientAcc = new Account(  Name = 'Testuu', 
                                Site = '_www.test_1.s', 
                                Type =  typeClStr , 
                                BillingStreet = 'Gatanfol 1',
                                BillingCity = 'phuket', 
                                BillingCountry =  bilCntryStr2  , 
                                BillingPostalCode =  postlCdStr ,
                                company_Role__c =  typeClStr ,
                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                Market_Area__c = Markt.id,
                                guid__c = '8ce8ad66-a6ec-1834-9e21',
                                Product_Area_UWR_2__c= piStr ,
                                Product_Area_UWR_4__c= piStr ,
                                Product_Area_UWR_3__c= marineStr ,
                                Claim_handler_Marine_2_lk__c = salesforceLicUser.id ,
                                X2nd_UWR__c = salesforceLicUser.id ,
                                X3rd_UWR__c = salesforceLicUser.id ,
                                Role_Client__c = true,
                                Underwriter_4__c = salesforceLicUser.id ,
                                Claim_handler_Cargo_Liquid__c = salesforceLicUser.id ,
                                Claim_handler_Charterers__c = salesforceLicUser.id ,
                                Claim_handler_Defence__c = salesforceLicUser.id ,
                                Claim_handler_Energy__c = salesforceLicUser.id ,
                                Claim_handler_Energy_2_lk__c= salesforceLicUser.id ,
                                Claims_handler_Builders_Risk_2_lk__c= salesforceLicUser.id ,
                                Claim_handler_Charterers_2_lk__c = salesforceLicUser.id ,
                                Claim_handler_Cargo_Liquid_2_lk__c = salesforceLicUser.id ,
                                Claims_handler_Builders_Risk__c = salesforceLicUser.id ,
                                Area_Manager__c = salesforceLicUser.id,
                                Underwriter_main_contact__c=salesforceLicUser.id,
                                UW_Assistant_1__c=salesforceLicUser.id,
                                U_W_Assistant_2__c=salesforceLicUser.id    ,
                                Claim_handler_Crew__c = salesforceLicUser.id,
                                Company_Status__c='Active',
                                P_I_Member_Flag__c = true,
                                Key_Claims_Contact__c = salesforceLicUser.id,
                                Claim_Adjuster_Marine_lk__c = salesforceLicUser.id,
                                Claim_handler_Cargo_Dry__c = salesforceLicUser.id,
                                Claim_handler_CEP__c = salesforceLicUser.id,
                                Claim_handler_Marine__c = salesforceLicUser.id,
                                Claim_handler_Cargo_Dry_2_lk__c = salesforceLicUser.id,
                                Claim_handler_CEP_2_lk__c = salesforceLicUser.id,
                                Claim_handler_Crew_2_lk__c =  salesforceLicUser.id,
                                Claim_handler_Defence_2_lk__c = salesforceLicUser.id,
                                Accounting_P_I__c = salesforceLicUser.id,
                                Accounting__c = salesforceLicUser.id,
                                PEME_Enrollment_Status__c = 'Enrolled',
                                Country__c = country.Id
                                
                               ); 
        insert clientAcc;
        cAcc = new Account();
        cAcc = [select id,Type from Account where id =: clientAcc.id];
        cAcc.Type = 'Customer';
        update cAcc;
        
        brokerAcc = new Account(   Name='testre',
                                BillingCity = 'Southampton',
                                BillingCountry =  bilCntryStr ,
                                BillingPostalCode = 'BS2 AD!',
                                BillingState = 'Avon LAng' ,
                                BillingStreet = '1 Mangrove Road',
                                recordTypeId=System.Label.Broker_Contact_Record_Type,
                                Site = '_www.tcs.se',
                                Type =  typeBrStr ,
                                company_Role__c =  typeBrStr ,
                                Sub_Roles__c = typeSubBrStr, //sfedit
                                guid__c = '8ce8ad89-a6ed-1836-9e17',
                                Market_Area__c = Markt.id,
                                Product_Area_UWR_2__c= piStr ,
                                Product_Area_UWR_4__c= piStr ,
                                Product_Area_UWR_3__c= marineStr ,
                                Claim_handler_Marine_2_lk__c = salesforceLicUser.id ,
                                Claim_handler_Cargo_Liquid__c = salesforceLicUser.id ,
                                Role_Broker__c = true,
                                Claim_handler_Charterers__c = salesforceLicUser.id ,
                                Claim_handler_Defence__c = salesforceLicUser.id ,
                                Claim_handler_Energy__c = salesforceLicUser.id ,
                                Claim_handler_Energy_2_lk__c= salesforceLicUser.id ,
                                Claims_handler_Builders_Risk_2_lk__c= salesforceLicUser.id ,
                                Claim_handler_Charterers_2_lk__c = salesforceLicUser.id ,
                                Claim_handler_Cargo_Liquid_2_lk__c = salesforceLicUser.id ,
                                Claims_handler_Builders_Risk__c = salesforceLicUser.id ,
                                Company_Role_Text__c = 'Broker',
                                Area_Manager__c = salesforceLicUser.id ,
                                X2nd_UWR__c=salesforceLicUser.id,
                                X3rd_UWR__c=salesforceLicUser.id,
                                Underwriter_4__c=salesforceLicUser.id,
                                Underwriter_main_contact__c=salesforceLicUser.id,
                                UW_Assistant_1__c=salesforceLicUser.id,
                                U_W_Assistant_2__c=salesforceLicUser.id,
                                Claim_handler_Crew__c = salesforceLicUser.id,
                                Enable_share_data__c = true,
                                Company_Status__c='Active',
                                GIC_Office_ID__c = null,
                                Key_Claims_Contact__c = salesforceLicUser.id,
                                Claim_Adjuster_Marine_lk__c = salesforceLicUser.id,
                                Claim_handler_Cargo_Dry__c = salesforceLicUser.id,
                                Claim_handler_CEP__c = salesforceLicUser.id,
                                Claim_handler_Marine__c = salesforceLicUser.id,
                                Claim_handler_Cargo_Dry_2_lk__c = salesforceLicUser.id,
                                Claim_handler_CEP_2_lk__c = salesforceLicUser.id,
                                Claim_handler_Crew_2_lk__c =  salesforceLicUser.id,
                                Claim_handler_Defence_2_lk__c = salesforceLicUser.id,
                                Accounting_P_I__c = salesforceLicUser.id,
                                Accounting__c = salesforceLicUser.id,
                                
                                Confirm_not_on_sanction_lists__c = true,
                                License_description__c  = 'some desc',
                                Description = 'some desc',
                                Licensed__c = 'Pending',
                                Active__c = true,
                                Country__c = country.Id
                                
                               );
        insert brokerAcc; 
        brokerContact = new Contact( 
            FirstName='Yoo',
            LastName='Baooo',
            MailingCity = 'Kingsville',
            OtherPhone =  otherPhnStr ,
            mobilephone =  mblPhnStr ,
            MailingCountry =  bilCntryStr ,
            MailingPostalCode = 'SE3 1AD',
            MailingState =  mailngStateStr ,
            MailingStreet = '1 Eastwood Road',
            AccountId = brokerAcc.Id,
            Email = 'test32@gmail.com',
            Synchronisation_Status__c ='Synchronised'
        );
        insert brokerContact;
        
        brokerUser = new User(  Alias = standtStr , 
                              profileId = partnerLicenseId ,
                              Email='Claim@gard.no',
                              EmailEncodingKey=utfStr,
                              LastName='tetst_005',
                              LanguageLocaleKey=enUsrStr ,
                              CommunityNickname = 'brokerUser',
                              LocaleSidKey=enUsrStr ,  
                              TimeZoneSidKey= tymZnStr ,
                              UserName='mygardrccInstancerokerUser@testorg.com.mygard',
                              ContactId = BrokerContact.Id,
                              contactId__c = grdobj.id,
                              IsActive = true,
                              City= 'Arendal'
                             );
        insert brokerUser;
        test_object_1st = New Object__c(     Dead_Weight__c= 20,
                                        Object_Unique_ID__c = '1234lkolko',
                                        Name='hennessey',
                                        Object_Type__c='Tanker',
                                        Object_Sub_Type__c= objSubTypStr ,
                                        No_of_passenger__c=100,
                                        No_of_crew__c=70,
                                        Gross_tonnage__c=1000,
                                        Length__c=20,
                                        Width__c=20,
                                        Depth__c=30,
                                        Port__c='Alabama',
                                        Signal_Letters_Call_sign__c='hellooo',
                                        Classification_society__c= csStr  ,
                                        Flag__c= flagStr ,
                                        Rebuilt__c= rebuiltStr ,
                                        Imo_Lloyds_No__c= imoStr ,
                                        Year_built__c= yrBuiltStr ,
                                        Dummy_Object_flag__c=0);
        insert test_object_1st;
        test_object_2nd = New Object__c(Dead_Weight__c= 20,
                                        Object_Unique_ID__c = '1934lkolko',
                                        Name='hennessey all',
                                        Object_Type__c='Cruise Vessel',
                                        Object_Sub_Type__c= objSubTypStr ,
                                        No_of_passenger__c=100,
                                        No_of_crew__c=70,
                                        Gross_tonnage__c=1000,
                                        Length__c=20,
                                        Width__c=20,
                                        Depth__c=30,
                                        Port__c='Alabama',
                                        Signal_Letters_Call_sign__c='hellooo',
                                        Classification_society__c= csStr  ,
                                        Flag__c= flagStr ,
                                        Rebuilt__c= rebuiltStr ,
                                        Imo_Lloyds_No__c= imoStr ,
                                        Year_built__c= yrBuiltStr ,
                                        Dummy_Object_flag__c=0);
        insert test_object_2nd;
        test_object_3rd = New Object__c(     Dead_Weight__c= 20,
                                        Object_Unique_ID__c = '1236lkolko',
                                        Name='Sparrow',                 
                                        Object_Sub_Type__c= objSubTypStr ,
                                        No_of_passenger__c=1000,
                                        No_of_crew__c=50,
                                        Gross_tonnage__c=1000,
                                        Length__c=20,
                                        Width__c=20,
                                        Depth__c=30,
                                        Port__c='Hampshire',
                                        Signal_Letters_Call_sign__c='hellooo',
                                        Classification_society__c= csStr  ,
                                        Flag__c= flagStr ,
                                        Rebuilt__c= rebuiltStr ,
                                        Imo_Lloyds_No__c= imoStr ,
                                        Year_built__c= yrBuiltStr ,
                                        Dummy_Object_flag__c=0);
        insert test_object_3rd;
        
        brokercontract_1st= New Contract(Accountid = brokerAcc.id, 
                                         Account = brokerAcc, 
                                         Status =  draftStr   ,
                                         CurrencyIsoCode =  isoCodeStr , 
                                         StartDate = Date.today(),
                                         ContractTerm = 2,
                                         Agreement_Type__c = 'Charterers', 
                                         Broker__c = brokerAcc.id,
                                         Client__c = clientAcc.id,
                                         Broker_Name__c = brokerAcc.id,
                                         Expiration_Date__c =  date.valueof( expDateStr  ),
                                         Inception_date__c =   date.valueof( incptnDateStr )  ,
                                         Agreement_Type_Code__c =  agrmntTypCdStr ,
                                         Agreement_ID__c = 'Agreement_ID_879',
                                         Business_Area__c= piStr ,
                                         Accounting_Contacts__c = brokerUser.id,
                                         Contract_Reviewer__c = brokerUser.id,
                                         Contracting_Party__c = brokerAcc.id,
                                         Policy_Year__c= polYrStr  ,
                                         Name_of_Contract__c =  cntrctNmStr                                                              
                                        );
        
        insert brokercontract_1st;
        
        brokerAsset_1st = new Asset(Name= 'Defence Cover',
                                    accountid =brokerAcc.id,
                                    contactid = BrokerContact.Id,                                 
                                    Agreement__c = brokerContract_1st.id, 
                                    Cover_Group__c =  piStr ,
                                    Cover_Group_Code__c = 'CR',
                                    CurrencyIsoCode= cntryISOStr  ,
                                    product_name__c =  piStr ,
                                    Object__c = test_object_1st.id,
                                    Underwriter__c = brokerUser.id,
                                    Risk_ID__c = '123456ris',
                                    On_risk_indicator__c=true,
                                    Expiration_Date__c = date.today().adddays(50),
                                    Inception_date__c = date.today()
                                   ); 
        insert brokerAsset_1st;
        
        brokerCase = new Case(
            Accountid = brokerAcc.id,
            Account = brokerAcc,
            Contact = brokerContact, 
            ContactId = brokerContact.id,
            Origin =  orignStr ,                        
            Object__c = test_object_1st.id,                          
            Claim_Incurred_USD__c= 500,
            Claim_Reference_Number__c='kol23232',
            Reserve__c = 350,
            Uwr_form_name__c = testStr,
            Paid__c =50000,
            Total__c= 750,                       
            Member_reference__c = 'ash1232',
            Contact_me_area__c = 'Claim',
            Last_name__c = 'yo1',
            guid__c = '8ce8ad66-a6ec-1834-9e21',
            
            Risk_Coverage__c = brokerAsset_1st.id,
            Status = 'Closed',
            Status_Change_Date__c = datetime.now(),
            Contract_for_Review__c = brokerContract_1st.id,
            Claims_handler__c = brokerUser.id,
            Assetid = brokerAsset_1st.id,
            OwnerId = brokerUser.id,
            LOC__c =  cntrctNmStr  ,
            Type = 'Add new user',
            Submitter_company__c = brokerAcc.id
        );
        insert brokerCase;
		*/
    }
    public static testmethod void TestRequestchangeCtrl_RequestPageCtrl2(){
        createTestData();
        Test.startTest();
        System.runAs(GardTestData.brokerUser)    
        {       
            RequestChangeCtrl rccInstance = new RequestChangeCtrl();
            System.debug('rccInstance.loggedInContactId - '+rccInstance.loggedInContactId);
            System.debug('rccInstance.loggedInContactId brokerContactId - '+GardTestData.brokerContact.id);
            rccInstance.selectedCover = GardTestData.brokerAsset_1st.product_name__c;    
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.clientName = GardTestData.clientAcc.Name;
            rccInstance.effctvDate='03.01.2014';//date.valueof('2014-01-03');
            rccInstance.tonnage = 1000;
            rccInstance.typeOfObj = testStr;
            rccInstance.selectedYrBuilt='2014';
            rccInstance.selectedObj.add(GardTestData.test_object_1st.id);        
            rccInstance.selectedObj.add(GardTestData.test_object_3rd.id); 
            rccInstance.reqType=1;
            rccInstance.btnClicked = 'New'; 
            // for type-1
            rccInstance.selectedReqType = 'Request Change to terms';
            rccInstance.userType = 'Broker';
            rccInstance.UWRId = GardTestData.salesforceLicUser.id;
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.effctvDate='03.01.2014';//date.valueof('2014-01-03');
            rccInstance.selectedprodArea='P&I';
            rccInstance.SelectedObhName=GardTestData.test_object_1st.name;
            // for type-1 objMode on                
            rccInstance.prvUrl='MyObjectDetails?objid='+GardTestData.test_object_1st.id;
            rccInstance.selectedReqType='change in assureds';
            rccInstance.reqType=2;
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.effctvDate='03.01.2014';//date.valueof('2014-01-03');
            rccInstance.selectedprodArea='P&I';
            rccInstance.SelectedObhName=GardTestData.test_object_1st.name;
            rccInstance.btnClicked = 'New';        
            // for type-2            
            rccInstance.SelectedObhName = null; 
            rccInstance.reqType=2;       
            rccInstance.selectedReqType='additional covers for existing objects';
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.effctvDate='03.01.2014';//date.valueof('2014-01-03');
            rccInstance.selectedprodArea='P&I';
            rccInstance.SelectedObhName=GardTestData.test_object_1st.name;
            rccInstance.btnClicked = 'Save';
            // for type-3
            rccInstance.prvUrl = '';    
            rccInstance.selectedMultiCover.add(GardTestData.brokerAsset_1st.product_name__c);
            rccInstance.selectedReqType='covers for new objects';
            rccInstance.phn = true;
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.effctvDate='03.01.2014';//date.valueof('2014-01-03');
            rccInstance.selectedprodArea='P&I';
            rccInstance.SelectedObhName=GardTestData.test_object_1st.name;
            Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
            rccInstance.caseTestId = singleCase.ID;
            rccInstance.btnClicked = 'New';
            
            //extra call
            rccInstance.reqType=2;
            
            rccInstance.selectedObj.add(GardTestData.test_object_1st.id);
            rccInstance.objMode = false;
            
            rccInstance.selectedCover=GardTestData.brokerAsset_1st.product_name__c; 
            rccInstance.reqType=2;
            rccInstance.selectedClient= GardTestData.clientAcc.id;
            rccInstance.userType = 'Broker';
            rccInstance.reqType=2;
            rccInstance.SelectedObhName = null;
            rccInstance.btnClicked = 'new';
            rccInstance.reqType=3;
            rccInstance.SelectedObhName=GardTestData.test_object_1st.name;
            
            integer reqtype;
            reqtype = 2;
            rccInstance.attachMailtoCaseopp(reqType );
            reqtype = 3;
            rccInstance.attachMailtoCaseopp(reqType );
            rccInstance.selectedprodArea='P&I';
            rccInstance.oppObj.RecordTypeId = [SELECT Id FROM RecordType where name ='P&I' and SobjectType = 'opportunity'].id;
            rccInstance.effctvDate='03.01.2014';//date.valueof('2014-01-03');
            rccInstance.UWRId = GardTestData.salesforceLicUser.id;
            rccInstance.comment=testStr;
            rccInstance.reqType=2;
            rccInstance.selectedObj.add(GardTestData.test_object_2nd.id);
            rccInstance.selectedClient = GardTestData.clientAcc.id;
            rccInstance.createOpportunity();
            rccInstance.clearTyp2Cover();
            Test.stopTest();
        } 
    }
    
}