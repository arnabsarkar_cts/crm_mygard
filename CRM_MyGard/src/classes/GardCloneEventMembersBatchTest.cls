/**
 * This class contains unit tests for validating the behavior of Apex class GardCloneEventMembersBatch
 */
@isTest
private class GardCloneEventMembersBatchTest {

    static testMethod void myUnitTest() {
         
        Test.StartTest();
        fluidoconnect__Survey__c s1 = new  fluidoconnect__Survey__c(name='Kokeilukysely1', 
                    fluidoconnect__Event_Owner__c=UserInfo.getUserId(),
                    fluidoconnect__Event_Organiser__c=UserInfo.getUserId());
        
        fluidoconnect__Survey__c s2=new  fluidoconnect__Survey__c(name='Kokeilukysely2',
                     fluidoconnect__Event_Owner__c=UserInfo.getUserId(),
                     fluidoconnect__Event_Organiser__c=UserInfo.getUserId());
                     
        list< fluidoconnect__Survey__c> survs=new List< fluidoconnect__Survey__c>{s1,s2};
        insert survs;
          
        List< fluidoconnect__Invitation__c> invs = new List< fluidoconnect__Invitation__c>();
        
        for(integer i=0;i<1;i++){
            invs.add(new fluidoconnect__Invitation__c( fluidoconnect__Survey__c=survs[0].id));
        }
        insert invs;
          
        GardCloneEventMembersBatch c= new GardCloneEventMembersBatch(survs[1].id,survs[0].id);
        ID batchprocessid = Database.executeBatch(c, 1);
   
        Test.StopTest();
 
        
    }
}