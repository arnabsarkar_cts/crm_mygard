@isTest(seeAllData=false)
public class TestUWRITOPFNewMemberDtlsCtrlClient
{           public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
            public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
            Public static testmethod void UWRITOPFNewMemberDtlsCtrlClient(){
            
            GardTestData test_rec=new GardTestData();
            test_rec.customsettings_rec();
            test_rec.commonrecord();
            
            //Invoking methods of UWRITOPFNewMemberDtlsCtrl for client users...
            
            System.runAs(GardTestData.clientUser)  
            {
                test.startTest();
                UWRITOPFNewMemberDtlsCtrl test_client= new UWRITOPFNewMemberDtlsCtrl();
                test_client.lstObject=new List<Object__c>();
                test_client.lstAsset=new List<Asset>();
                test_client.ast=GardTestData.clientAsset_1st;
                test_client.confirmationCheckbox=true;
                test_client.IMONotKnown=true;
                test_client.selectedObj=GardTestData.test_object_1st.id;
                List<SelectOption> clientOptions =  test_client.getClientLst();
                List<SelectOption> flag=test_client.getFlagList();
                List<SelectOption> port=test_client.getPortList();
               // List<SelectOption> classification=test_client.getClassificationList();
                system.assertequals(clientOptions.size()>0,true,true);
                system.assertequals(flag.size()>0,true,true);
                system.assertequals(port.size()>0,true,true);
                test_client.selectedClient = GardTestData.clientAcc.id;
                test_client.assetFinal = null;
                test_client.selectedClient = GardTestData.clientAcc.id;
                test_client.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c); 
                test_client.FindBy='imo';          
                test_client.searchByImoObjectName();
                test_client.selectedClient = GardTestData.clientAcc.id;
                test_client.assetFinal = null;
                test_client.ObjName = string.valueof(GardTestData.test_object_1st.name); 
                test_client.FindBy='objname';
                test_client.accountIdSet = new set<String>();
                test_client.accountIdSet.add(GardTestData.brokerAcc.id);
                test_client.contractIdSet = new set<String>();
                test_client.contractIdSet.add(GardTestData.brokerContract_1st.id);
                test_client.ObjName = string.valueof(GardTestData.test_object_1st.name);
                test_client.searchByImoObjectName();
                test_client.dateOutput='05.02.2015';
                test_client.dateOutputForReview='05.02.2015';
                test_client.setObjectSelection();
                test_client.selectedClient = GardTestData.clientAcc.id;
                test_client.chkAccess();
                test_client.brokersClientDtls();
                test_client.selectedObj=GardTestData.test_object_1st.id;
                test_client.saveRecord();
                test_client.clearRecord();
                test_client.goNext();
                test_client.goPrev();
               // test_client.saveRecordNSubmit();
                test_client.getCName();
                test_client.sendToForm();
                test_client.sendAckMail();
             //   test_client.sendNotiMail();
                test_client.uwf.id = GardTestData.clientCase.id;
                //test_client.saveRecord();
               // test_client.saveRecordNSubmit();
               test.stopTest();
            }
       }
  }