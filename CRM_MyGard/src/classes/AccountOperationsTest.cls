@isTest
public class AccountOperationsTest{
	static testMethod void testDoInsertAccounts(){
	
		Account parentAccount = new Account(name='Test Account', ShippingStreet='1 Main St.', ShippingState='VA', ShippingPostalCode='12345', ShippingCountry='USA', ShippingCity='Anytown', Description='This is a test account', BillingStreet='1 Main St.', BillingState='VA', BillingPostalCode='12345', BillingCountry='USA', BillingCity='Anytown', AnnualRevenue=10000);
		List<Account> allAccounts = new List<Account>();
		allAccounts.add(parentAccount);
		new AccountOperations().doInsertAccounts(allAccounts);
	}
	
	static testMethod void testDoUpdateAccounts(){
		Account parentAccount = new Account(name='Test Account', ShippingStreet='1 Main St.', ShippingState='VA', ShippingPostalCode='12345', ShippingCountry='USA', ShippingCity='Anytown', Description='This is a test account', BillingStreet='1 Main St.', BillingState='VA', BillingPostalCode='12345', BillingCountry='USA', BillingCity='Anytown', AnnualRevenue=10000);
		List<Account> allAccounts = new List<Account>();
		allAccounts.add(parentAccount);
		AccountOperations accountOperation = new AccountOperations();
		accountOperation.doInsertAccounts(allAccounts);
		accountOperation.doUpdateAccounts(allAccounts);
	}
	
	static testMethod void testDoDeleteAccounts(){
		Account parentAccount = new Account(name='Test Account', ShippingStreet='1 Main St.', ShippingState='VA', ShippingPostalCode='12345', ShippingCountry='USA', ShippingCity='Anytown', Description='This is a test account', BillingStreet='1 Main St.', BillingState='VA', BillingPostalCode='12345', BillingCountry='USA', BillingCity='Anytown', AnnualRevenue=10000);
		List<Account> allAccounts = new List<Account>();
		allAccounts.add(parentAccount);
		AccountOperations accountOperation = new AccountOperations();
		accountOperation.doInsertAccounts(allAccounts);
		accountOperation.doDeleteAccounts(allAccounts);
	}
	
	static testMethod void testDoUpsertAccounts(){
		Account parentAccount = new Account(name='Test Account', ShippingStreet='1 Main St.', ShippingState='VA', ShippingPostalCode='12345', ShippingCountry='USA', ShippingCity='Anytown', Description='This is a test account', BillingStreet='1 Main St.', BillingState='VA', BillingPostalCode='12345', BillingCountry='USA', BillingCity='Anytown', AnnualRevenue=10000);
		List<Account> allAccounts = new List<Account>();
		allAccounts.add(parentAccount);
		AccountOperations accountOperation = new AccountOperations();
		accountOperation.doInsertAccounts(allAccounts);
		accountOperation.doUpsertAccounts(allAccounts);
	}
}