//batch class used to assign claim handlers as case (claim type) owner
global class CaseClaimHandler_Batch implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        List<RecordType> lstRecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Claim'];
        if(lstRecordType != null && lstRecordType.size() == 1)
        {
            String strId = lstRecordType[0].Id;
            return Database.getQueryLocator('Select Id, Claims_handler__c, Claims_handler__r.IsActive, OwnerId  from Case where RecordTypeId =:strId');
        }  
        else 
            return null;
    }
    
    global void execute(Database.BatchableContext bc, List<Case> scope){
        
        List<Case> newlstCases;       
        if(scope != null && scope.size()>0)
        {
            newlstCases = new List<Case>();
            for(Case c: scope)
            {   
                if((c.Claims_handler__c != null) && (c.Claims_handler__c != c.OwnerId))
                    if(c.Claims_handler__r.IsActive){
                        c.OwnerId  = c.Claims_handler__c;
                        newlstCases.add(c);					
                    }
            }
        }
        Database.update(newlstCases,false);
    }   
    
    global void finish(Database.BatchableContext bc){
    }
}