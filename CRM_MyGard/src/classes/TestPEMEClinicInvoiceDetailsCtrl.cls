@isTest(seeAllData=false)
public class TestPEMEClinicInvoiceDetailsCtrl
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    public static PEME_Invoice__c pi=new PEME_Invoice__c ();
    public static PEME_Exam_Detail__c PExamDetail = new PEME_Exam_Detail__c();
    public static PEME_Enrollment_Form__c pef;
    public static PEME_Manning_Agent__c  pmg; 
    Public static List<Object__c> ObjectList=new List<Object__c>();
    Public static String typeBrStr =  'Broker';
    Public static String typeSubBrStr = 'Broker - Reinsurance Broker';
    Public static String typeEspStr =  'External Service Provider';
    Public static String typeSubEspStr = 'ESP - Agent';
    Public static Country__c country;
    private static final String strClinic = 'Clinic';
    Static testMethod void cover()
    {
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        Gard_Team__c gardTeam=new Gard_Team__c(P_I_email__c='test@test.com',Region_code__c='CASM', Office__c='Arendal');
        
        insert gardTeam;
        
        Country__c country=new Country__c(Claims_Support_Team__c=gardTeam.id);
        insert country;
        Valid_Role_Combination__c vrcESP = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
        insert vrcESP;
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab12000');
        insert Markt;
        
        User user = new User(
            Alias = 'standt', 
            profileId = salesforceLicenseId ,
            Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8',
            CommunityNickname = 'test13',
            LastName='Testing',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',  
            TimeZoneSidKey='America/Los_Angeles',
            UserName='test008@testorg.com',
            ContactId__c = gc.id
        );
        insert user ;
        User user1 =new User();
        user1 = [select id, contactid__c from user where id=:user.id];
        /*Account brokerAcc = new Account( Name='testre',
        BillingCity = 'Bristol',
        BillingCountry = 'United Kingdom',
        BillingPostalCode = 'BS1 1AD',
        BillingState = 'Avon' ,
        BillingStreet = '1 Elmgrove Road',
        recordTypeId=System.Label.Broker_Contact_Record_Type,
        Site = '_www.cts.se',
        Type = 'Broker' ,
        Area_Manager__c = user1.id,      
        Market_Area__c = Markt.id                                                                    
        );
        AccountList.add(brokerAcc);
        Account clientAcc= New Account();
        clientAcc.Name = 'Testt';
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.Area_Manager__c = user1.id;        
        clientAcc.Market_Area__c = Markt.id;    
        AccountList.add(clientAcc);
        insert AccountList; */
        
        Account clinicAcc = new Account( Name='testClinic',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.ESP_Contact_Record_Type,
                                        Site = '_www.ctsclinic.se',
                                        Type = 'Surveyor' ,
                                        Area_Manager__c = user1.id,
                                        company_Role__c =  typeEspStr ,
                                        Sub_Roles__c = typeSubEspStr,
                                        Market_Area__c = Markt.id ,
                                        country__c = country.Id                                                                   
                                       );
        AccountList.add(clinicAcc);
        insert AccountList; 
        Blob b = Crypto.GenerateAESKey(128);            
        String h = EncodingUtil.ConvertTohex(b);            
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20);
        clinicAcc .GUID__c=guid;
        update clinicAcc ;
        /* Contact brokerContact = new Contact( 
        FirstName='Raan',
        LastName='Baan',
        MailingCity = 'London',
        MailingCountry = 'United Kingdom',
        MailingPostalCode = 'SE1 1AD',
        MailingState = 'London',
        MailingStreet = '1 London Road',
        AccountId = brokerAcc.Id,
        Email = 'test321@gmail.com'
        );
        ContactList.add(brokerContact) ;
        Contact clientContact= new Contact( FirstName='tsat_1',
        LastName='chak',
        MailingCity = 'London',
        MailingCountry = 'United Kingdom',
        MailingPostalCode = 'SE1 1AE',
        MailingState = 'London',
        MailingStreet = '4 London Road',
        AccountId = clientAcc.Id,
        Email = 'test321@gmail.com'
        );
        ContactList.add(clientContact);
        insert ContactList;*/
        Contact clinicContact = new Contact( 
            FirstName='medicalTest',
            LastName='medical',
            MailingCity = 'London',
            MailingCountry = 'United Kingdom',
            MailingPostalCode = 'SE1 1AD',
            MailingState = 'London',
            MailingStreet = '1 London Road',
            AccountId = clinicAcc.Id,
            Email = 'test321@gmail.com'
        );
        ContactList.add(clinicContact) ;
        
        insert ContactList;
        /*User brokerUser = new User(
        Alias = 'standt', 
        profileId = partnerLicenseId,
        Email='test120@test.com',
        EmailEncodingKey='UTF-8',
        LastName='estTing',
        LanguageLocaleKey='en_US',
        CommunityNickname = 'brokerUser',
        LocaleSidKey='en_US',  
        TimeZoneSidKey='America/Los_Angeles',
        UserName='testbrokerUser@testorg.com.mygard',
        ContactId = BrokerContact.Id
        );
        UserList.add(brokerUser);
        User clientUser = new User(Alias = 'alias_1',
        CommunityNickname = 'clientUser',
        Email='test_11@testorg.com', 
        profileid = partnerLicenseId, 
        EmailEncodingKey='UTF-8',
        LastName='tetst_006',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Los_Angeles',
        UserName='testclientUser@testorg.com.mygard',
        ContactId = clientContact.id
        );
        
        UserList.add(clientUser) ;
        insert UserList;*/
        User clinicUser = new User(
            Alias = 'Clinic', 
            profileId = partnerLicenseId,
            Email='testClinic120@test.com',
            EmailEncodingKey='UTF-8',
            LastName='ClinicTesting',
            LanguageLocaleKey='en_US',
            CommunityNickname = 'clinicUser',
            LocaleSidKey='en_US',  
            TimeZoneSidKey='America/Los_Angeles',
            UserName='testClinicUser@testorg.com.mygard',
            ContactId = clinicContact.Id
        );
        UserList.add(clinicUser);
        insert UserList;
        Object__c Object1=new Object__c(Name= 'TestObject1',
                                        //CurrencyIsoCode='NOK-Norwegian Krone',
                                        Object_Unique_ID__c='test11223'
                                       );
        ObjectList.add(Object1);
        Object__c Object2=new Object__c(
            Name= 'TestObject2',
            // CurrencyIsoCode='NOK-Norwegian Krone',
            Object_Unique_ID__c='test11224'
        );
        ObjectList.add(Object2);
        insert ObjectList;
        pef=new PEME_Enrollment_Form__c ();
        pef.ClientName__c=clinicAcc.ID;
        pef.Comments__c='Report analyzed';
        pef.Enrollment_Status__c='Draft';
        pef.Gard_PEME_References__c='Test References';
        pef.Last_Status_Change_Date__c=Date.valueOf('2016-01-27');
        //  pef.Person_in_charge_Email__c='test@mail.com';
        //  pef.Person_in_charge_Name__c='test Person';
        pef.Submitter__c=clinicContact.ID;
        insert pef;
        
        pmg=new PEME_Manning_Agent__c  ();
        pmg.Client__c=clinicAcc.ID;
        pmg.PEME_Enrollment_Form__c=pef.ID;
        pmg.Company_Name__c='Test Company';
        //   pmg.Contact_Person__c='Test Person';
        pmg.Contact_Point__c=clinicContact.Id;
        pmg.Status__c='Approved';
        insert pmg;
        // pi.Client__c=clientAcc.id;
        
        // PEMEClientInvoiceDetailsCtrl pcid=new PEMEClientInvoiceDetailsCtrl();
        
        pi.Clinic_Invoice_Number__c='qwerty123';
        pi.Clinic__c=clinicAcc.id;
        pi.Crew_Examined__c=11223;
        pi.Invoice_Date__c=Date.valueOf('2016-01-05');
        pi.PEME_reference_No__c='aasd23';
        pi.Status__c='Approved';
        pi.ObjectList__c='qwerty for test purpose';
        pi.Total_Amount__c=112230;
        pi.Vessels__c= Object1.id;
        //pi.GUID__c='guid1';
        pi.Period_Covered_From__c=Date.valueOf('2016-01-05');
        pi.Period_Covered_To__c =Date.valueOf('2016-01-28');
        pi.PEME_Enrollment_Detail__c = pef.id;
        insert pi;
        pi.guid__c = '1ghsdjhasgdjhasg'; 
        update pi;
        //  PExamDetail=new PEME_Exam_Detail__c ();
        PExamDetail.Age__c=30;
        PExamDetail.Amount__c=12000;
        PExamDetail.Date__c=Date.valueOf('2016-01-10');
        PExamDetail.Examination__c='Demo Exam';
        PExamDetail.Examinee__c='Demo employee';
        PExamDetail.Invoice__c=pi.id;
        PExamDetail.Object_Related__c=Object1.id;
        PExamDetail.Rank__c='Five';
        PExamDetail.Status__c='Approved';
        
        insert PExamDetail;
        AccountToContactMap__c acmCS = new AccountToContactMap__c(name = (clinicContact.id+''),AccountId__c = (clinicAcc.id+''),RolePreference__c = strClinic);
        insert acmCS;
        Account_Contact_Mapping__c acm = new Account_Contact_Mapping__c(
            contact__c = (clinicContact.id+''),Account__c = (clinicAcc.id+''),Active__c = true,
            Administrator__c = 'true', PI_Renewal_Access__c = true, IsPeopleClaimUser__c = true, PI_access__c = true, Marine_access__c = true, Read_Only__c = false
        );
        insert acm;
        
        Test.startTest();
        System.runAs(clinicUser )
        {
            //  pi.ownerid = clinicUser.id;
            //    update pi;
            System.currentPageReference().getParameters().put('invoices' ,'1ghsdjhasgdjhasg'); 
            PEMEClinicInvoiceDetailsCtrl PEMEClinicInvoiceDetailsCntl=new PEMEClinicInvoiceDetailsCtrl();
            PEMEClinicInvoiceDetailsCntl.loadData();
            PEMEClinicInvoiceDetailsCntl.generateExams();
            PEMEClinicInvoiceDetailsCntl.createOpenUserList();
            PEMEClinicInvoiceDetailsCntl.getSortDirection();
            PEMEClinicInvoiceDetailsCntl.setSortDirection('ASC');
            PEMEClinicInvoiceDetailsCntl.getExamOptions();
            PEMEClinicInvoiceDetailsCntl.getObjectOptions();
            PEMEClinicInvoiceDetailsCntl.getStatusOptions();
            PEMEClinicInvoiceDetailsCntl.firstOpen();
            PEMEClinicInvoiceDetailsCntl.hasPreviousOpen=true;
            PEMEClinicInvoiceDetailsCntl.previousOpen();
            PEMEClinicInvoiceDetailsCntl.setpageNumberOpen();
            PEMEClinicInvoiceDetailsCntl.hasNextOpen=false;
            PEMEClinicInvoiceDetailsCntl.nextOpen();
            PEMEClinicInvoiceDetailsCntl.lastOpen();
            PEMEClinicInvoiceDetailsCntl.selectedObject=new list<String>();
            PEMEClinicInvoiceDetailsCntl.selectedExam=new list<String>();
            PEMEClinicInvoiceDetailsCntl.selectedStatus=new list<String>();
            PEMEClinicInvoiceDetailsCntl.selectedExam.add('testExam');
            PEMEClinicInvoiceDetailsCntl.selectedObject.add('testObject');
            PEMEClinicInvoiceDetailsCntl.selectedStatus.add('testStatus');
            PEMEClinicInvoiceDetailsCntl.generateExams();
            PEMEClinicInvoiceDetailsCntl.clearOptions();
            PEMEClinicInvoiceDetailsCntl.print();
            PEMEClinicInvoiceDetailsCntl.exportToExcel();
            PEMEClinicInvoiceDetailsCntl.editExamId=PExamDetail.ID;
            PEMEClinicInvoiceDetailsCntl.fetchExamLineItem();
            PEMEClinicInvoiceDetailsCntl.editableExamRecord = new PEME_Exam_Detail__c();
            PEMEClinicInvoiceDetailsCntl.editableExamRecord.id = PExamDetail.ID;
            PEMEClinicInvoiceDetailsCntl.updateExamDetails();
            PEMEClinicInvoiceDetailsCntl.closeEditPopup();
            PEMEClinicInvoiceDetailsCntl.deleteExamId=PExamDetail.ID;
            //PEMEClinicInvoiceDetailsCntl.deleteRecord();
            PEMEClinicInvoiceDetailsCntl.noOfRecords=10;
            PEMEClinicInvoiceDetailsCntl.size=10;
            PEMEClinicInvoiceDetailsCntl.intStartrecord=1;
            PEMEClinicInvoiceDetailsCntl.intEndrecord=8;
            PEMEClinicInvoiceDetailsCntl.jointSortingParam='test';      
        }
        Test.stopTest();
        /* System.runAs(clientUser )
        {
        PExamDetail=new PEME_Exam_Detail__c ();
        PExamDetail.Age__c=30;
        PExamDetail.Amount__c=12000;
        PExamDetail.Date__c=Date.valueOf('2016-01-10');
        PExamDetail.Examination__c='Demo Exam';
        PExamDetail.Examinee__c='Demo employee';
        // PExamDetail.Id=;
        PExamDetail.Invoice__c=pi.id;
        PExamDetail.Object_Related__c=Object1.id;
        PExamDetail.Rank__c='Five';
        PExamDetail.Status__c='Approved';
        insert PExamDetail;
        System.currentPageReference().getParameters().put('invoices' ,'1ghsdjhasgdjhasg');
        PEMEClinicInvoiceDetailsCtrl PEMEClinicInvoiceDetailsCntl=new PEMEClinicInvoiceDetailsCtrl();
        PEMEClinicInvoiceDetailsCntl.generateExams();
        PEMEClinicInvoiceDetailsCntl.createOpenUserList();
        PEMEClinicInvoiceDetailsCntl.getSortDirection();
        PEMEClinicInvoiceDetailsCntl.setSortDirection('ASC');
        PEMEClinicInvoiceDetailsCntl.getExamOptions();
        PEMEClinicInvoiceDetailsCntl.getObjectOptions();
        PEMEClinicInvoiceDetailsCntl.getStatusOptions();
        PEMEClinicInvoiceDetailsCntl.firstOpen();
        PEMEClinicInvoiceDetailsCntl.hasPreviousOpen=true;
        PEMEClinicInvoiceDetailsCntl.previousOpen();
        PEMEClinicInvoiceDetailsCntl.setpageNumberOpen();
        PEMEClinicInvoiceDetailsCntl.hasNextOpen=false;
        PEMEClinicInvoiceDetailsCntl.nextOpen();
        PEMEClinicInvoiceDetailsCntl.lastOpen();
        PEMEClinicInvoiceDetailsCntl.selectedExam.add('testExam');
        PEMEClinicInvoiceDetailsCntl.selectedObject.add('testObject');
        PEMEClinicInvoiceDetailsCntl.selectedStatus.add('testStatus');
        PEMEClinicInvoiceDetailsCntl.clearOptions();
        PEMEClinicInvoiceDetailsCntl.print();
        PEMEClinicInvoiceDetailsCntl.exportToExcel();
        PEMEClinicInvoiceDetailsCntl.editExamId=PExamDetail.ID;
        PEMEClinicInvoiceDetailsCntl.fetchExamLineItem();
        PEMEClinicInvoiceDetailsCntl.updateExamDetails();
        PEMEClinicInvoiceDetailsCntl.closeEditPopup();
        PEMEClinicInvoiceDetailsCntl.deleteExamId=PExamDetail.ID;
        PEMEClinicInvoiceDetailsCntl.deleteRecord();
        }*/
    }
}