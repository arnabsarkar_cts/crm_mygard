@isTest(SeeAllData=true)
public class TestDataGenerator {
    
    private static String ADMIN_USERNAME = 'test.admin.user@gard.no.unittest';  
    
    private static String ADMIN_PROFILEID = '00e20000001O7Hl'; // System Administrator
    
    private static String UNDERWRITER_USERNAME = 'Test.underwriter.user@gard.no.unittest';
    
    private static String UNDERWRITER_PROFILEID = '00e20000001US4e'; // Gard Standard User
    
    private static String MARKET_AREA_MANAGER_PROFILEID = UNDERWRITER_PROFILEID;
    
    private static String MARKET_AREA_MANAGER_USERNAME = 'test.market.area.manager@gard.no.unittest';
    //public static AdminUsers__c setting;
    //public static List<Valid_Role_Combination__c> vrcList;
   // public static Valid_Role_Combination__c vrcClient,vrcBroker;
    public static Country__c createCountry(){
        Gard_Team__c gt = new Gard_Team__c();
        gt.Active__c = true;
        gt.Office__c = 'test';
        gt.Name = 'GTeam';
        gt.Region_code__c = 'TEST';
        insert gt;
        /*
        vrcList = new List<Valid_Role_Combination__c>();
            Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
            vrcList.add(vrcClient);
            Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
            vrcList.add(vrcBroker);
            insert vrcList;
        */
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRAEL',Gard_Team__c=gt.Id,Claims_Support_Team__c=gt.Id);
        insert country;
         //CustomSetting added by Abhirup
          /*  setting = new AdminUsers__c();
            setting.Name = 'Number of users';
            setting.Value__c = 3;
            insert setting;*/
        return country;
    }
    
    public static User getAdminUser() {
        
        return getOrCreateUser(ADMIN_USERNAME, ADMIN_PROFILEID);
    }
    
    
    public static User getUnderwriterUser() {
        
        return getOrCreateUser(UNDERWRITER_USERNAME, UNDERWRITER_PROFILEID);
    }
    
    
    public static User getMarketAreaManagerUser() {
        
        return getOrCreateUser(MARKET_AREA_MANAGER_USERNAME, MARKET_AREA_MANAGER_PROFILEID);
    }
    
    
    public static Account getApprovedMemberAccount() {
        
        Country__c country = createCountry();
        Account account = new Account();
        account.Membership_Status__c = 'Approved';
        account.Name = 'Approved Member Test Account';
        account.Area_Manager__c = getMarketAreaManagerUser().Id;
        account.Market_Area__c = getMarketArea().Id;
        account.Type = 'Customer';
        account.Synchronisation_Status__c = 'Synchronized';
        account.Billing_Address_Sync_Status__c = 'Synchronised';
        account.Shipping_Address_Sync_Status__c = 'Synchronised';
        account.Roles_Sync_Status__c  = 'Synchronised'; 
        account.country__c = country.Id;
        
        insert account;
        return account;
    }
    
    
    public static Account getClientAccount() {
        
        Country__c country = createCountry();
        Account account = new Account();        
        account.Name = 'Customer Test Account';
        account.Area_Manager__c = getMarketAreaManagerUser().id;
        account.Market_Area__c = getMarketArea().Id;        
        account.Synchronisation_Status__c = 'Synchronised';
        account.Billing_Address_Sync_Status__c = 'Synchronised';
        account.Shipping_Address_Sync_Status__c = 'Synchronised';
        account.Roles_Sync_Status__c  = 'Synchronised'; 
        account.country__c = country.Id;
        
        insert account;
        return account; 
    }
    
    
    public static Contact createContactFor(Account account) {
        
        Contact contact = new Contact(LastName = 'Test-Contact');       
        contact.AccountId = account.Id;
        
        insert contact;
        
        contact = [select Id, LastName, MailingStreet, MailingCity, MailingPostalCode, MailingState, MailingCountry from contact where Id = :contact.id limit 1];   
        
        return contact;
    }
    
    public static Market_Area__c getMarketArea() {
        
        Market_Area__c marketArea = new Market_Area__c();
        insert marketArea;
        return marketArea;
    }
    
    
    private static RecordType getOpportunityRecordType(String recordTypeName) {
        
        List < RecordType > recordTypes = [select Id from RecordType where Name=:recordTypeName and SObjectType = 'opportunity'];
        System.assert(recordTypes.size() == 1, 'Expected 1 record type [' + recordTypeName + ']. Found ' + recordTypes.size() + '.');
        return recordTypes.get(0);
    }
    
    
    public static Opportunity getOpportunity(String recordTypeName, String stageName, String riskName) {
        
        Account account = getApprovedMemberAccount();
        RecordType recordType = getOpportunityRecordType(recordTypeName);
        
        Opportunity opportunity = new Opportunity();
        opportunity.RecordTypeId = recordType.Id;
        opportunity.Type = 'New Business';
        opportunity.Name = 'Test opportunity';
        opportunity.AccountId = account.Id;
        opportunity.StageName = stageName;
        opportunity.CloseDate = Date.today();
        opportunity.Business_Type__c = riskName;
        opportunity.Amount = 150000;
        opportunity.Agreement_Market_Area__c = getMarketArea().Id;
        opportunity.Area_Manager__c = getMarketAreaManagerUser().Id;
        opportunity.Approval_Criteria__c = '';
        opportunity.Senior_Approver__c = null;      
        
        insert opportunity;
        return opportunity;
    }

    
    public static Opportunity changeApprovalCriterion(Opportunity opportunity, String approvalCriterion) {
        
        opportunity.Approval_Criteria__c = approvalCriterion;
        update opportunity;
        return refreshOpportunity(opportunity);
    }
    
    
    public static Opportunity changeEpi(Opportunity opportunity, Integer epi) {
        
        opportunity.Amount = epi;
        update opportunity;
        return opportunity;
    }
    
        
    public static Opportunity refreshOpportunity(Opportunity opportunity) {
            
        Opportunity reloadedOpportunity = [ 
        select 
            Id, 
            RecordTypeId, 
            Name, 
            AccountId, 
            StageName, 
            CloseDate,  
            Business_Type__c, 
            Budget_Status__c, 
            Amount, 
            Risk_Evaluation_Status__c,
            Area_Manager__c,
            Senior_Approver__c,
            PM_Approver__c,
            Approval_Criteria__c,
            Approved_For_Cancellation__c,
            Approval_Criteria_Count__c,
            Approval_Process_ID__c
        from Opportunity where 
            Id = :opportunity.Id
        ];
        
        return reloadedOpportunity;
    }
    
    
    public static OpportunityLineItem getOpportunityLineItem(Opportunity opportunity) {
        
        Product2 product = getProduct();
        Pricebook2 priceBook = getStandardPriceBook();
        
        OpportunityLineItem opportunityLineItem = new opportunityLineItem();
        opportunityLineItem.OpportunityId = opportunity.Id;                         
        opportunityLineItem.PricebookEntryId = getPriceBookEntry(product, priceBook).Id;
        opportunityLineItem.Line_Size__c = 100;
        
        insert opportunityLineItem;
        return opportunityLineItem;
    }
    
    
    public static Product2 getProduct() {
        
        List < Product2 > products = [select Id from Product2 where Name = 'Test product'];

        if (products.isEmpty()) {
            Product2 product = new Product2();
            product.Name = 'Test product';      
        
            insert product;
            PriceBook2 standardPriceBook = getStandardPriceBook();              
            getPriceBookEntry(product, standardPriceBook);                  
            return product;
        } else {
            return products.get(0);
        }
    }
    
    
    public static PriceBook2 getStandardPriceBook() {

        List < PriceBook2 > priceBooks = [select Id, IsStandard from PriceBook2];
        System.assert(priceBooks.size() == 1, 'Standard priceBook not found');
        return priceBooks.get(0);           
    }
    
    
    public static PriceBookEntry getPriceBookEntry(Product2 product, PriceBook2 priceBook) {
        
        List < PricebookEntry > entries = [select Id from PricebookEntry where Pricebook2Id = :priceBook.id and Product2Id = :product.Id];
        
        if (entries.isEmpty()) {
            PriceBookEntry priceBookEntry = new priceBookEntry();
            priceBookEntry.UnitPrice = 1000;        
            priceBookEntry.Product2Id = product.Id;
            priceBookEntry.Pricebook2Id = priceBook.Id;     
            priceBookEntry.IsActive = true;
        
            insert priceBookEntry;
            return priceBookEntry;
        } else {
            return entries.get(0);
        }
    }
    
    public static Country__c getCountry(String countryName) {
        Country__c country = new Country__c(name= countryName);
        insert country;
        return country;
    }
    
    private static User getOrCreateUser(String username, String profileId) {
        
        List < User > users = [select Id from User where Username=:username];
        User user = null;
        
        if (users.isEmpty()) {
            System.debug('Creating new test user');
            user = new User();
            user.Username = username;
            user.LastName = 'Test-User';
            user.Email = username;
            user.Alias = 'J. Doe';
            user.CommunityNickname = username.substring(0, 15);
            user.TimeZoneSidKey = 'Europe/Amsterdam';
            user.LocaleSidKey = 'en_US';
            user.EmailEncodingKey = 'ISO-8859-1';
            user.ProfileId = profileId;
            user.LanguageLocaleKey = 'en_US';
            user.City = 'Arendal';
            insert user;
        } else if (users.size() == 1) {
            System.debug('Existing test user found');
            user = users.get(0);
        } else {
            System.assert(false, 'Multiple users found, zero or one expected.');
        }
        
        return user;
    }
    
    
    private static testMethod void getAdminUserSmoketest() {
        
        User adminUser = getAdminUser();
        System.assert(adminUser != null, 'Unable to retrieve or create admin user');
    }
    
    
    private static testMethod void getUnderwriterUserSmoketest() {
        
        User underwriterUser = getUnderwriterUser();
        System.assert(underwriterUser != null, 'Unable to retrieve or create underwriter user');
    }
    
    
    private static testMethod void getMarketAreaManagerUserSmoketest() {
        
        User marketAreaManagerUser = getMarketAreaManagerUser();
        System.assert(marketAreaManagerUser != null, 'Unable to retrieve or create market area manager user');
    }
    
    
    private static testMethod void getMarketAreaSmoketest() {
        
        Market_Area__c marketArea = getMarketArea();
        System.assert(marketArea != null, 'Unable to create market area');
    }
    
    
    private static testMethod void getOpportunitySmoketest() {
        
        Opportunity opportunity = getOpportunity('P&I', 'Risk Evaluation', 'Small Craft');
        System.assert(opportunity != null, 'Unable to create opportunity');
    }
    
    
    private static testMethod void getOpportunityLineItemSmoketest() {
        
        Opportunity opportunity = getOpportunity('P&I', 'Risk Evaluation', 'Small Craft');
        OpportunityLineItem opportunityLineItem = getOpportunityLineItem(opportunity);
        System.assert(opportunityLineItem != null, 'Unable to create OpportunityLineItem');
    }
    
    
    private static testMethod void getProductSmoketest() {
        
        Product2 product = getProduct();
        System.assert(product != null, 'Unable to create product');
    }
    
    
    private static testMethod void getStandardPriceBookSmoketest() {
        
        PriceBook2 standardPriceBook = getStandardPriceBook();
        System.assert(standardPriceBook != null, 'Unable to create price book');
    }
    
    
    private static testMethod void getApprovedMemberAccountSmoketest() {
        
        Account account = getApprovedMemberAccount();
        System.assert(account != null, 'Unable to create approved member account');
    }
    
    
    private static testMethod void changeApprovalCriterionSmoketest() {
        
        Opportunity opportunity = getOpportunity('P&I', 'Risk Evaluation', 'Small Craft');
        opportunity = changeApprovalCriterion(opportunity, 'Self Approval');
        System.assert(opportunity.Approval_Criteria__c == 'Self Approval', 'Unable to change approval criteria');       
    }
    
    private static testMethod void createContactForSmoketest() {
        
        Account account = getClientAccount();
        Contact contact = createContactFor(account);
        System.assert(contact != null, 'Unable to create contact');
    }
}