@isTest
private class TestScheduler_BatchInsertMyCover {
	public static TestMethod void testScheduler_BatchInsertMyCover() {
        
        test.StartTest();
        
        // schedule a new job
        String jobId = Scheduler_BatchInsertMyCover.scheduleMe();
        /*
        String jobId = System.schedule('testScheduler_class',
            '0 0 0 3 9 ? 2022', 
            new Scheduler_class());
        */
        // get the newly created trigger record
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, 
            NextFireTime
            FROM CronTrigger WHERE id = :jobId];
        
        // assert that the values in the job are correct
        System.assertEquals(0, ct.TimesTriggered);
        
        test.StopTest();
    }
}