@isTest
private class CompanyRibbonExtensionTests {
	private static Account account;
    private static List<String> roles = new List<String>{'Client','Broker','ESP','Other'};
    private static List<String> subRoles = new List<String>{'ClientSubRole','BrokerSubRole','ESPSubRole','OtherSubRole'};
	private static List<Valid_Role_Combination__c> validRoleSubRoleCombination;
    
    //Test Data Setup Methods
	private static void setupTestData(){
        setupValidRoles();
		account = new Account (Name = 'Unit Test Account', Company_Role__c = String.join(roles,';'), sub_Roles__c = String.join(subRoles,';'));
		insert account;
	}
	
	private static void setupValidRoles(){
		////For some reason this seems to be throwing an internal Salesforce error /
		////System.UnexpectedException: Salesforce System Error: 1583762872-13077 (1716107030) (1716107030)
		//validRoleSubRoleCombination = (List<Valid_Role_Combination__c>)Test.loadData(Valid_Role_Combination__c.sObjectType, 'MDMValidRoles');
		////END...
		validRoleSubRoleCombination = new List<Valid_Role_Combination__c>();
        for(Integer index = 0 ; index < roles.size() ; index++){
            validRoleSubRoleCombination.add(new Valid_Role_Combination__c(Role__c = roles[index], Sub_Role__c = subRoles[index]));
        }
		insert validRoleSubRoleCombination;
	}
	
    //Test Methods
	@isTest static void CompanyRibbonExtension_Coverage(){
		setupTestData();
		Apexpages.currentPage().getParameters().put('id',account.Id);
		ApexPages.StandardController sc = new ApexPages.standardController(account);
        CompanyRibbonExtension companyRibbon = new CompanyRibbonExtension(sc);
		List<String> otherRoles = companyRibbon.OtherRoles;
        //companyRibbon.useRoleRecords=true;
        for(String otherRole : otherRoles){
            companyRibbon.RecordType = otherRole;
            companyRibbon.applyRecordTypeChange();
        }
        companyRibbon.reloadRibbon();
        /*
		companyRibbon.RecordType = otherRoles[0];
		companyRibbon.applyRecordTypeChange();
		companyRibbon.RecordType = otherRoles[1];
		companyRibbon.applyRecordTypeChange();
		*/
	}
}