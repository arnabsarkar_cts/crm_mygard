@isTest(seeAllData=false)
public class TestPEMEClinicSubmitInvoiceCtrl_1
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    public static PEME_Invoice__c pi=new PEME_Invoice__c ();
    public static PEME_Exam_Detail__c PExamDetail = new PEME_Exam_Detail__c();
    Public static List<Object__c> ObjectList=new List<Object__c>();
    
    Static testMethod void cover()
    {
        CommonVariables__c cvar = new CommonVariables__c(Name='ReplyToMailId',Value__c='test@example.com');
        insert cvar;
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab12000');
        insert Markt;
        Gard_Team__c gardTeam=new Gard_Team__c(P_I_email__c='test@test.com',Region_code__c='CASM', Office__c='Arendal');
        insert gardTeam;
        AdminUsers__c setting = new AdminUsers__c();
            setting.Name = 'Number of users';
            setting.Value__c = 3;
            insert setting;
        Country__c country=new Country__c(Claims_Support_Team__c=gardTeam.id);
        insert country;
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrcClient);
        Valid_Role_Combination__c vrcESP = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
        vrcList.add(vrcESP);
        insert vrcList;
        User user = new User(
                            Alias = 'standt', 
                            profileId = salesforceLicenseId ,
                            Email='standarduser@testorg.com',
                            EmailEncodingKey='UTF-8',
                            CommunityNickname = 'test13',
                            LastName='Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',  
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='test008@testorg.com',
                            ContactId__c = gc.id
                             );
        insert user ;
        User user1 =new User();
        user1 = [select id, contactid__c from user where id=:user.id];
        Blob b1 = Crypto.GenerateAESKey(128);            
        String h1 = EncodingUtil.ConvertTohex(b1);            
        String guid1 = h1.SubString(0,8)+ '-' + h1.SubString(8,12) + '-' + h1.SubString(12,16) + '-' + h1.SubString(16,20);
        
        Account clientAcc= New Account();
        clientAcc.Name = 'Testt';
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.Area_Manager__c = user1.id;        
        clientAcc.Market_Area__c = Markt.id; 
        clientAcc.company_Role__c = 'Client';
        clientAcc.GUID__c=guid1;
        clientAcc.PEME_Enrollment_Status__c='Enrolled'; 
        clientAcc.country__c = country.Id;  
        AccountList.add(clientAcc);
        //insert AccountList; 
        Account clinicAcc = new Account( Name='testClinic',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.ESP_Contact_Record_Type,
                                        company_Role__c='External Service Provider',
                                        Site = '_www.ctsclinic.se',
                                        Type = 'Surveyor' ,
                                        Area_Manager__c = user1.id,      
                                        Market_Area__c = Markt.id,
                                        PEME_Enrollment_Status__c='Enrolled',
                                        Sub_Roles__c = 'ESP - Agent',
                                        country__c = country.Id
                                     );
        AccountList.add(clinicAcc);
        insert AccountList;
        /*
        Blob b1 = Crypto.GenerateAESKey(128);            
        String h1 = EncodingUtil.ConvertTohex(b1);            
        String guid1 = h1.SubString(0,8)+ '-' + h1.SubString(8,12) + '-' + h1.SubString(12,16) + '-' + h1.SubString(16,20);
        clientAcc.GUID__c=guid1;
        clientAcc.company_Role__c = 'Client';
        update clientAcc; 
        */
        Blob b = Crypto.GenerateAESKey(128);            
        String h = EncodingUtil.ConvertTohex(b);            
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20);
     //   clinicAcc .GUID__c=guid;
        //update clinicAcc ;
        /* Contact brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(brokerContact) ;*/
        Contact clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        //insert ContactList;
        Contact clinicContact = new Contact( 
                                             FirstName='medicalTest',
                                             LastName='medical',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = clinicAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(clinicContact) ;
        
        insert ContactList;
        /*User brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='test120@test.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id
                                   );
        UserList.add(brokerUser);*/
        User clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
                
         UserList.add(clientUser) ;
         //insert UserList;
         User clinicUser = new User(
                                    Alias = 'Clinic', 
                                    profileId = partnerLicenseId,
                                    Email='testClinic120@test.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='ClinicTesting',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'clinicUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testClinicUser@testorg.com.mygard',
                                    ContactId = clinicContact.Id
                                   );
        UserList.add(clinicUser);
        insert UserList;
         Object__c Object1=new Object__c(Name= 'TestObject1',
                                         //CurrencyIsoCode='NOK-Norwegian Krone',
                                         Object_Unique_ID__c='test11223'
                                         );
         ObjectList.add(Object1);
         Object__c Object2=new Object__c(
                                         Name= 'TestObject2',
                                        // CurrencyIsoCode='NOK-Norwegian Krone',
                                         Object_Unique_ID__c='test11224'
                                         );
         ObjectList.add(Object2);
         insert ObjectList;
        
        
        Account clinicAcc1 = new Account( Name='TestPEMEClinicSubmitInvoice',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.ESP_Contact_Record_Type,
                                        company_Role__c='External Service Provider',
                                        Site = '_www.TestPEMEClinicSubmitInvoice.se',
                                        Type = 'Surveyor' ,
                                        Area_Manager__c = user1.id,      
                                        Market_Area__c = Markt.id,
                                        PEME_Enrollment_Status__c='Draft',
                                        Sub_Roles__c = 'ESP - Agent',
                                        country__c = country.Id
                                     );
        //insert clinicAcc1;
        
         
        PEME_Enrollment_Form__c pefBroker=new PEME_Enrollment_Form__c();
        pefBroker.ClientName__c=clientAcc.ID;
            pefBroker.Enrollment_Status__c='Draft';
        pefBroker.Submitter__c=clinicContact.Id;
        insert pefBroker;
        //System.assertEquals(pefBroker.Enrollment_Status__c , 'Under Approval'); 
         // PEMEClientInvoiceDetailsCtrl pcid=new PEMEClientInvoiceDetailsCtrl();
        pi.Client__c=clinicAcc.id;
        pi.Clinic_Invoice_Number__c='qwerty123';
        pi.Clinic__c=clinicAcc.id;
        pi.Crew_Examined__c=11223;
        pi.Invoice_Date__c=Date.valueOf('2016-01-05');
        pi.PEME_reference_No__c='aasd23';
        pi.Status__c='Draft';
        pi.ObjectList__c='qwerty for test purpose';
        pi.Total_Amount__c=112230;
        pi.Vessels__c= Object1.id;
        pi.PEME_Enrollment_Detail__c = pefBroker.id;
        pi.Period_Covered_From__c=Date.valueOf('2016-01-05');
        pi.Period_Covered_To__c =Date.valueOf('2016-01-28');
        pi.guid__c = '8fd0180b-acf5-8c8c-7519';
        insert pi;        
      //  PExamDetail=new PEME_Exam_Detail__c ();
        PExamDetail.Age__c=30;
        PExamDetail.Amount__c=12000;
        PExamDetail.Date__c=Date.valueOf('2016-01-10');
        PExamDetail.Examination__c='Demo Exam';
        PExamDetail.Examinee__c='Demo employee';
        PExamDetail.Invoice__c=pi.id;
        PExamDetail.Object_Related__c=Object1.id;
        PExamDetail.Rank__c='Five';
        PExamDetail.Status__c='Approved';
        
        insert PExamDetail;
        //System.assertEquals(PExamDetail.Rank__c , 'Five');
        System.runAs(clinicUser)
        {
            test.startTest();
            PEMEClinicSubmitInvoiceCtrl PEMEClinicSubmitInvoiceCntl1=new PEMEClinicSubmitInvoiceCtrl();
            PEMEClinicSubmitInvoiceCntl1.invoice.id=pi.id;
            PEMEClinicSubmitInvoiceCntl1.loggedInAccountId = clinicAcc.id; 
            PEMEClinicSubmitInvoiceCntl1.selectedClient= clinicAcc.id;
                       system.debug('PI'+pi);
           // system.debug('ONe more checkkkkkk'+[select guid__c from PEME_Invoice__c where ID =:pi.ID].GUID__c);
            //System.currentPageReference().getParameters().put('id' ,pi.guid__c);
            //System.currentPageReference().getParameters().put('id' ,pi.guid__c);
            /*PageReference pageRef_1 = Page.PEMEClinicSubmitInvoice1; // Add your VF page Name here
            pageRef_1.getParameters().put('id', pi.guid__c);
            Test.setCurrentPage(pageRef_1);*/
            System.currentPageReference().getParameters().put('id' ,pi.guid__c);
            PEMEClinicSubmitInvoiceCtrl PEMEClinicSubmitInvoiceCntl=new PEMEClinicSubmitInvoiceCtrl();
            
            

            
            
            /*PEMEClinicSubmitInvoiceCntl.mapClient=new map<String,Id>();
            PEMEClinicSubmitInvoiceCntl.mapClient.put('testClinic',(pef.ClientName__c +'').substring(0,15));
            System.debug('mapClient-------'+PEMEClinicSubmitInvoiceCntl.mapClient);*/
            /*for(Account Acctlist:AccountList)
            {
                PEMEClinicSubmitInvoiceCntl.selectedclient=Acclist.id;  
            }*/
            
            //  PEMEClinicSubmitInvoiceCntl.selectedclient=clinicAcc1.ID;
            //}
            
            PEMEClinicSubmitInvoiceCntl.goBankDetailNext();
            PEMEClinicSubmitInvoiceCntl.invoice=new PEME_Invoice__c();
            PEMEClinicSubmitInvoiceCntl.invoice.ID=pi.ID;
            String toDate = String.valueof(pi.Period_Covered_From__c);
            String[] periodToDate = toDate.split('-');
            PEMEClinicSubmitInvoiceCntl.periodFrom = periodToDate[2]+'.'+periodToDate[1]+'.'+periodToDate[0];
            String fromDate = String.valueof(pi.Period_Covered_To__c);
            String[] periodFromDate = fromDate.split('-');
            PEMEClinicSubmitInvoiceCntl.periodTo = periodFromDate[2]+'.'+periodFromDate[1]+'.'+periodFromDate[0];
            PEMEClinicSubmitInvoiceCntl.submit();
            PEMEClinicSubmitInvoiceCntl.sendInvoiceEmails();
             PEMEClinicSubmitInvoiceCntl.goExamPrev();
            PEMEClinicSubmitInvoiceCntl.getAge();
            PEMEClinicSubmitInvoiceCntl.getExamination();
            PEMEClinicSubmitInvoiceCntl.getObjectLst();
            PEMEClinicSubmitInvoiceCntl.getRefNo();
            PEMEClinicSubmitInvoiceCntl.getSelectObj();
            System.debug(LoggingLevel.INFO, '-->' + PEMEClinicSubmitInvoiceCntl.selectedclient);
            PEMEClinicSubmitInvoiceCntl.populateRefNo();
            PEMEClinicSubmitInvoiceCntl.populateObject();
            PEMEClinicSubmitInvoiceCntl.removeExam();
            PEMEClinicSubmitInvoiceCntl.goExamdetailsNext();
            PEMEClinicSubmitInvoiceCntl.goBankPrev();
            PEMEClinicSubmitInvoiceCntl.goYes();
            PEMEClinicSubmitInvoiceCntl.goNo();
            PEMEClinicSubmitInvoiceCntl.cancel();
            PEMEClinicSubmitInvoiceCntl.goReviewNext();
            PEMEClinicSubmitInvoiceCntl.goInvoicePrev();
            PEMEClinicSubmitInvoiceCntl.deleteRecord();
            PEMEClinicSubmitInvoiceCntl.addMore();
            PEMEClinicSubmitInvoiceCntl.getCName();
            PEMEClinicSubmitInvoiceCntl.checkExistedInvoice();
            PEMEClinicSubmitInvoiceCntl.getAgentLst();
            
            //PEMEClinicSubmitInvoiceCntl.getClientLst();
            try{
                PEMEClinicSubmitInvoiceCntl1.getClientLst();
            }
            catch(NullpointerException e){
                System.debug(e);
            }
            test.stopTest();  
            
        }
    }
}