@isTest(seeAllData=false)
public class TestPEMEClinicClientsCtrl
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    public static PEME_Enrollment_Form__c pef;
    
    Static testMethod void cover()
    {
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
         AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        Gard_Team__c gardTeam=new Gard_Team__c(P_I_email__c='test@test.com',Region_code__c='CASM', Office__c='Arendal');
        Country__c country=new Country__c(Claims_Support_Team__c=gardTeam.id);
        insert country;
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcESP = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
        vrcList.add(vrcESP);
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        vrcList.add(vrcBroker);
        insert vrcList;
        User user = new User(
                            Alias = 'standt', 
                            profileId = salesforceLicenseId ,
                            Email='standarduser@testorg.com',
                            EmailEncodingKey='UTF-8',
                            CommunityNickname = 'test13',
                            LastName='Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',  
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='test008@testorg.com',
                            ContactId__c = gc.id
                             );
        insert user ;
        User user1 =new User();
        user1 = [select id, contactid__c from user where id=:user.id];
        Account brokerAcc = new Account( Name='testre',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.Broker_Contact_Record_Type,
                                        Site = '_www.cts.se',
                                        Type = 'Broker' ,
                                        Area_Manager__c = user1.id,      
                                        Market_Area__c = Markt.id ,
                                        Confirm_not_on_sanction_lists__c = true,
                                        License_description__c  = 'some desc',
                                        Description = 'some desc',
                                        Licensed__c = 'Pending',
                                        country__c = country.id,
                                        company_Role__c =  'Broker',
                                        Sub_Roles__c = 'Broker - Reinsurance Broker'
                                     );
        AccountList.add(brokerAcc);
        Account clientAcc= New Account();
        clientAcc.Name = 'Testt';
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.Area_Manager__c = user1.id;        
        clientAcc.Market_Area__c = Markt.id; 
        clientAcc.company_Role__c = 'External Service Provider';
        clientAcc.Sub_Roles__c = 'ESP - Agent';
        clientAcc.country__c = country.Id;
        AccountList.add(clientAcc);
        Account clinicAcc = new Account( Name='testClinic',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.ESP_Contact_Record_Type,
                                        Site = '_www.ctsclinic.se',
                                        Type = 'Surveyor' ,
                                        Area_Manager__c = user1.id,      
                                        Market_Area__c = Markt.id,
                                        company_Role__c = 'External Service Provider',
                                        Sub_Roles__c = 'ESP - Agent',
                                        country__c = country.Id    
                                                                                                          
                                     );
        AccountList.add(clinicAcc);
        insert AccountList;
        Blob b = Crypto.GenerateAESKey(128);            
        String h = EncodingUtil.ConvertTohex(b);            
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20);
        brokerAcc.GUID__c=guid;
        update brokerAcc;
        Blob b1 = Crypto.GenerateAESKey(128);            
        String h1 = EncodingUtil.ConvertTohex(b1);            
        String guid1 = h1.SubString(0,8)+ '-' + h1.SubString(8,12) + '-' + h1.SubString(12,16) + '-' + h1.SubString(16,20);
        clientAcc.GUID__c=guid1;
        update clientAcc;
        Contact brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(brokerContact) ;
        Contact clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        Contact clinicContact = new Contact( 
                                             FirstName='medicalTest',
                                             LastName='medical',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = clinicAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(clinicContact) ;
        insert ContactList;
         User brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='test120@test.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id
                                   );
        UserList.add(brokerUser);
        User clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
                
        UserList.add(clientUser) ;
        User clinicUser = new User(
                                    Alias = 'Clinic', 
                                    profileId = partnerLicenseId,
                                    Email='testClinic120@test.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='ClinicTesting',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'clinicUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testClinicUser@testorg.com.mygard',
                                    ContactId = clinicContact.Id
                                   );
        UserList.add(clinicUser);
        insert UserList;
        
        pef=new PEME_Enrollment_Form__c ();
        pef.ClientName__c=clientAcc.ID;
        pef.Comments__c='Report analyzed';
        pef.Enrollment_Status__c='Draft';
        pef.Gard_PEME_References__c='Test References';
        pef.Last_Status_Change_Date__c=Date.valueOf('2016-01-27');
        //pef.Person_in_charge_Email__c='test@mail.com';
        //pef.Person_in_charge_Name__c='test Person';
        pef.Submitter__c=clientContact.ID;
        insert pef;
        /*
        pef.Enrollment_Status__c='Enrolled';
        update pef; //commented on 7/12/2016
        */
        System.runAs(brokerUser)
        {
            test.startTest();
            PEMEClinicClientsCtrl PEMEClinicClientsCntl=new PEMEClinicClientsCtrl();
            PEMEClinicClientsCntl.selectedClient.add(clientAcc.Name);
            PEMEClinicClientsCntl.generateClients();
            PEMEClinicClientsCntl.getSortDirection();
            PEMEClinicClientsCntl.setSortDirection('ASC');
            PEMEClinicClientsCntl.getClientOptions();
            PEMEClinicClientsCntl.createOpenUserList(); 
            //PEMEClinicClientsCntl.firstOpen();
            PEMEClinicClientsCntl.hasPreviousOpen=true;
            //PEMEClinicClientsCntl.previousOpen();
            //PEMEClinicClientsCntl.setpageNumberOpen();
            PEMEClinicClientsCntl.hasNextOpen=false;
            //PEMEClinicClientsCntl.nextOpen();
            //PEMEClinicClientsCntl.lastOpen();
            PEMEClinicClientsCntl.clearOptions();
            //PEMEClinicClientsCntl.print();
            //PEMEClinicClientsCntl.exportToExcel();
            PEMEClinicClientsCntl.noOfRecords=10;
            PEMEClinicClientsCntl.size=10;
            PEMEClinicClientsCntl.intStartrecord=1;
            PEMEClinicClientsCntl.intEndrecord=8;
            PEMEClinicClientsCntl.jointSortingParam='test';
            PEMEClinicClientsCntl.sSoqlQuery='Select Id,ClientName__c, ClientName__r.GUID__c,GUID__c, ClientName__r.Name, Enrollment_Status__c,LastModifiedDate,Last_Status_Change_Date__c from PEME_Enrollment_Form__c';
            PEMEClinicClientsCntl.print();
            PEMEClinicClientsCntl.exportToExcel();
        }
        
        System.runAs(clientUser)
        {
            PEMEClinicClientsCtrl PEMEClinicClientsCntl=new PEMEClinicClientsCtrl();
            PEMEClinicClientsCntl.selectedClient.add(clientAcc.Name);
            PEMEClinicClientsCntl.generateClients();
            PEMEClinicClientsCntl.getSortDirection();
            PEMEClinicClientsCntl.setSortDirection('ASC');
            PEMEClinicClientsCntl.getClientOptions();
            PEMEClinicClientsCntl.createOpenUserList();
            //PEMEClinicClientsCntl.firstOpen();
            PEMEClinicClientsCntl.hasPreviousOpen=true;
            //PEMEClinicClientsCntl.previousOpen();
            //PEMEClinicClientsCntl.setpageNumberOpen();
            PEMEClinicClientsCntl.hasNextOpen=false;
            //PEMEClinicClientsCntl.nextOpen();
            //PEMEClinicClientsCntl.lastOpen();
            PEMEClinicClientsCntl.clearOptions();
            //PEMEClinicClientsCntl.print();
            //PEMEClinicClientsCntl.exportToExcel();
            PEMEClinicClientsCntl.noOfRecords=10;
            PEMEClinicClientsCntl.size=10;
            PEMEClinicClientsCntl.intStartrecord=1;
            PEMEClinicClientsCntl.intEndrecord=8;
            PEMEClinicClientsCntl.jointSortingParam='test';
        }
        
        System.runAs(clinicUser)
        {
            PEMEClinicClientsCtrl PEMEClinicClientsCntl=new PEMEClinicClientsCtrl();
            PEMEClinicClientsCntl.selectedClient.add(clinicAcc.Name);
            //PEMEClinicClientsCntl.generateClients();
            PEMEClinicClientsCntl.getSortDirection();
            PEMEClinicClientsCntl.setSortDirection('ASC');
            PEMEClinicClientsCntl.getClientOptions();
            PEMEClinicClientsCntl.createOpenUserList(); 
            PEMEClinicClientsCntl.firstOpen();
            PEMEClinicClientsCntl.hasPreviousOpen=true;
            PEMEClinicClientsCntl.previousOpen();
            PEMEClinicClientsCntl.setpageNumberOpen();
            PEMEClinicClientsCntl.hasNextOpen=false;
            PEMEClinicClientsCntl.nextOpen();
            PEMEClinicClientsCntl.lastOpen();  
            /* PEMEClinicClientsCntl.print();
            PEMEClinicClientsCntl.exportToExcel(); */  
            PEMEClinicClientsCntl.sSoqlQuery='SELECT Object__r.Id,Object__r.Name,Name,Agreement__r.Client__r.GUID__c,Expiration_Date__c,Agreement__r.Policy_Year__c,Object__r.Imo_Lloyds_No__c,On_risk_indicator__c FROM Asset';
            PEMEClinicClientsCntl.print();
            PEMEClinicClientsCntl.exportToExcel();
            test.stopTest();
        }
    }
}