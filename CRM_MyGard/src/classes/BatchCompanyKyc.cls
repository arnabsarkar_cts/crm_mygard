Global class BatchCompanyKyc implements Database.Batchable<sObject>{
    
    public String query;
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        System.debug(':::::::: start batch class ::::::::::');
        query = 'SELECT id,name, Company_Role__c  FROM Account WHERE (not name like \'%test%\')';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Account> scope){
        System.debug('------ execute batch class ------');
        List<account> accToBeUpdated = new List<account>();
        for(account acc:scope){
            //acc.Account_Edit_3866__c = true;
            accToBeUpdated.add(acc);
            system.debug('found account for KYC-> '+acc.Id+' -- '+acc.name);
        }
        if(accToBeUpdated.size() > 0){
           update accToBeUpdated;
        }
    }
      
   global void finish(Database.BatchableContext BC){
   }

}