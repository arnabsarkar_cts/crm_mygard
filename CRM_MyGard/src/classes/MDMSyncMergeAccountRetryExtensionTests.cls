@isTest
private class MDMSyncMergeAccountRetryExtensionTests {
	
	static testMethod void MDMSyncMergeAccountRetryExtension_Coverage() {
		///ARRANGE...
		Account a1 = new Account (Name = 'Unit Test Account1');
		Account a2 = new Account (Name = 'Unit Test Account2');
		
		insert a1;
		insert a2;
		
		MDM_Account_Merge__c m = new MDM_Account_Merge__c(from_account__c = a1.Id, to_account__c = a2.Id);
		m.Synchronisation_Status__c = 'Sync Failed';
		insert m;
		
		Apexpages.currentPage().getParameters().put('id',m.Id);     
		ApexPages.StandardController sc = new ApexPages.standardController(m);
        MDMSyncMergeAccountRetryExtension retryExt = new MDMSyncMergeAccountRetryExtension(sc);
		
		///ACT...
		Test.startTest();
		boolean b = retryExt.IsAdmin;
		string s = retryExt.redirectUrl;
		boolean b2 = retryExt.shouldRedirect;
		retryExt.RetryMergeAccountSync();
		Test.stopTest();
		
		///ASSERT...
		m = [SELECT id, Synchronisation_Status__c FROM MDM_Account_Merge__c WHERE id = :m.id];
		System.assertEquals('Sync In Progress', m.Synchronisation_Status__c, 'MDM_Account_Merge__c sync status should be sync in progress');
	}
}