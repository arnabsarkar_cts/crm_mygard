global class processCompanyGardTeam implements Database.Batchable<sObject> {

    global String query;
    // build list of users in memory to save repeated DB querying
    public List<User> lstUsr;

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        system.debug('The scope ****'+scope);
        if (scope.size() > 0) {
            //SOC 24/4/13 PCI-000104 - Added first name/last name to this query
            
            //lstUsr = [Select Id, Username, FirstName, LastName, GIC_ID__c, PARIS_ID__c from User where ];
            //the above is commented out by aritra for SF-4505 optimization            
            lstUsr = [Select Id, Username, FirstName, LastName, GIC_ID__c, PARIS_ID__c from User where GIC_ID__c != null and PARIS_ID__c != null];
            
            //4505 List<Account> lstAcc = new List<Account>();
            Map<String, Account> mapAcc = new Map<String, Account>(); //4505 
            Map<String, Boolean> handlerSet = null;
            Account acc;
            boolean bExistsGICpsc;
            boolean bExistsGICOnRiskpsc;
            boolean bExistsPARISpsc;
            boolean bExistsPARISOnRiskpsc;
            
            for (sObject listacc: scope) {
                bExistsPARISpsc = false;
                bExistsGICpsc = false;
                
                //Reset all handler statuses to indicate not set in this run
                handlerSet = new Map<String, Boolean>();
                handlerSet.put('CargoDry1', false);
                handlerSet.put('CargoLiquid1', false);
                handlerSet.put('CEP1', false);
                handlerSet.put('Charterers1', false);
                handlerSet.put('Crew1', false);
                handlerSet.put('Defence1', false);
                handlerSet.put('Energy1', false);
                handlerSet.put('Marine1', false);
                handlerSet.put('BuildersRisk1', false);
                handlerSet.put('CargoDry2', false);
                handlerSet.put('CargoLiquid2', false);
                handlerSet.put('CEP2', false);
                handlerSet.put('Charterers2', false);
                handlerSet.put('Crew2', false);
                handlerSet.put('Defence2', false);
                handlerSet.put('Energy2', false);
                handlerSet.put('Marine2', false);
                handlerSet.put('BuildersRisk2', false);
                //Non-claims handler fields
                handlerSet.put('AccountingParis', false);
                handlerSet.put('AccountingGIC', false);
                handlerSet.put('UWAssistant1', false);
                handlerSet.put('UWAssistant2', false);
                
                //SF 4368
                handlerSet.put('GardteamGIC', true); //SF 4505 default set to true to bypass the loop. Can be brought back in future.
                handlerSet.put('GardteamPARIS', true); //SF 4505 - same as above
                
                //CRM-111
                handlerSet.put('AssistantEnergy', false);
                //SOC 29/08/13 PCI-000138 - Adding claims adjuster marine
                handlerSet.put('ClaimAdjusterMarine', false);
                                
                acc = (Account)listacc;
               
                // Loop through the related PSCs. Ordered by priority in schedulable class. 
                system.debug('Looping through PSCs for: ' + acc.Name);
                for (Admin_System_Company__c psc: acc.Admin_System_Companies__r) 
                {
                    //All PSCs should be on risk because of schedulable class query
                    if (psc.On_Risk__c == true && psc.Source_System__c == 'Paris')
                    {
                        system.debug('Updating from Paris PSC: ' + psc.Name);
                       
                        checkPARISHandlers(acc, psc, handlerSet);
                    }
                    
                    else if (psc.On_Risk__c == true && psc.Source_System__c == 'GIC')
                    {   
                        system.debug('Updating from GIC PSC: ' + psc.Name);
                                       
                        checkGICHandlers(acc, psc, handlerSet);
                    }
                       
                }
                
                mapacc.put(acc.id, acc);
                System.debug('mapacc-->' + mapAcc);
            }
            
            //START - Added for 4505 - This is just for CT team assignment, this is in
            //separate loop since it follows a different priority for PSCs for source
            List<Account> gt_accounts = [SELECT id, gard_team__c, 
                                            (SELECT id, gard_team__c from Admin_System_Companies__r 
                                            where Company_Status__c = 'Active' AND On_Risk__c = true 
                                            order by Source_System__c desc, GIC_Priority__c, Mapper_Number__c DESC, Company_ID__c DESC)
                                         from Account where id in :mapAcc.keyset()];
            
            for (Account a : gt_accounts){
                Account loopAcc = mapAcc.get(a.id);
                Boolean found = false;
                for (Admin_System_Company__c psc: a.Admin_System_Companies__r){
                    if(psc.gard_team__c != null){
                        loopAcc.gard_team__c = getCustomerTeamId(psc.gard_team__c);
                        found = true;
                        break;
                    }    
                }
                if (!found){   
                    loopAcc.gard_team__c = null;  
                }
                mapAcc.put(a.id, loopAcc);
            }    
            //FINISH
            
            if(mapAcc.Size() > 0){
                update(mapAcc.values());
            }
            
            /*
            if (lstAcc.Size() > 0)
                //System.debug('lstAcc--->' + lstAcc);
                update(lstAcc); 
            */
        } 
    }

    global void finish(Database.BatchableContext BC) {
        // do nothing
    }
    
    private void checkPARISHandlers(Account acc, Admin_System_Company__c psc, Map<String, Boolean> handlerSet)
    {
        Map<String,String> secClaimHandlerDetails = null;
        //These are the handlers defined as coming from PARIS
        //Energy, Marine, Builders Risk
        //Also includes fields: Accounting, UW Assistant 2
        if(handlerSet.get('Energy1')==false)
        {
            acc.Claim_handler_Energy__c         = getUserId(psc.Claim_handler_Energy__c);
            if(acc.Claim_handler_Energy__c != null)
                handlerSet.put('Energy1', true);
        }
        
        if(handlerSet.get('Energy2')==false)
        {
            //secClaimHandlerDetails              = getSecClaimHandlerDetails(psc.Claim_handler_Energy_2__c); //MYGP-23
            //acc.Claim_handler_Energy_2_lk__c  = secClaimHandlerDetails.get('Id');  //MYGP-23
            acc.Claim_handler_Energy_2_lk__c     = getUserId(psc.Claim_handler_Energy_2__c);
            //acc.Claim_handler_Energy_2_Id__c    = secClaimHandlerDetails.get('Id');  //MYGP-23
            //acc.Claim_handler_Energy_2_Name__c  = secClaimHandlerDetails.get('FullName'); //MYGP-23
            //if(acc.Claim_handler_Energy_2_Id__c != null) //MYGP-23
            if(acc.Claim_handler_Energy_2_lk__c != null)
                handlerSet.put('Energy2', true);
        }
        
        if(handlerSet.get('Marine1')==false)
        {
            acc.Claim_handler_Marine__c         = getUserId(psc.Claim_handler_Marine__c);
            if(acc.Claim_handler_Marine__c != null)
                handlerSet.put('Marine1', true);
        }
        
        if(handlerSet.get('Marine2')==false)
        {
            //secClaimHandlerDetails              = getSecClaimHandlerDetails(psc.Claim_handler_Marine_2__c); //MYGP-23
            //acc.Claim_handler_Marine_2_lk__c  = secClaimHandlerDetails.get('Id'); //MYGP-23
            acc.Claim_handler_Marine_2_lk__c  = getUserId(psc.Claim_handler_Marine_2__c);
            //acc.Claim_handler_Marine_2_Id__c    = secClaimHandlerDetails.get('Id');  //MYGP-23
            //acc.Claim_handler_Marine_2_Name__c  = secClaimHandlerDetails.get('FullName');  //MYGP-23
            //if(acc.Claim_handler_Marine_2_Id__c != null)  //MYGP-23
            if(acc.Claim_handler_Marine_2_lk__c != null)
                handlerSet.put('Marine2', true);
        }
        
        if(handlerSet.get('BuildersRisk1')==false)
        {
            system.debug('psc.Claims_handler_Builders_Risk__c---->'+psc.Claims_handler_Builders_Risk__c);
            acc.Claims_handler_Builders_Risk__c = getUserId(psc.Claims_handler_Builders_Risk__c);
            if(acc.Claims_handler_Builders_Risk__c != null)
                handlerSet.put('BuildersRisk1', true);
        }
        
        if(handlerSet.get('BuildersRisk2')==false)
        {
            //secClaimHandlerDetails              = getSecClaimHandlerDetails(psc.Claims_handler_Builders_Risk_2__c); //MYGP-23
            //acc.Claims_handler_Builders_Risk_2_lk__c  = secClaimHandlerDetails.get('Id');  //MYGP-23
            system.debug('psc.Claims_handler_Builders_Risk_2__c---->'+psc.Claims_handler_Builders_Risk_2__c);
            acc.Claims_handler_Builders_Risk_2_lk__c  = getUserId(psc.Claims_handler_Builders_Risk_2__c);
            //acc.Claims_handler_Builders_Risk_2_Id__c    = secClaimHandlerDetails.get('Id');  //MYGP-23
            //acc.Claims_handler_Builders_Risk_2_Name__c  = secClaimHandlerDetails.get('FullName'); //MYGP-23                       
            //if(acc.Claims_handler_Builders_Risk_2_Id__c != null) //MYGP-23
            if(acc.Claims_handler_Builders_Risk_2_lk__c != null)
                handlerSet.put('BuildersRisk2', true);
        }
        
        if(handlerSet.get('AccountingParis')==false)
        {
            acc.Accounting__c = getUserId(psc.Accounting__c);
            if(acc.Accounting__c != null)
                handlerSet.put('AccountingParis', true);
        }
        
        if(handlerSet.get('UWAssistant2')==false)
        {
            if (getUserId(psc.U_W_Assistant_1__c) != null)
            {
                //Try first PSC UW assistant
                acc.U_W_Assistant_2__c = getUserId(psc.U_W_Assistant_1__c);
                handlerSet.put('UWAssistant2', true);
            }
            else if (getUserId(psc.U_W_Assistant_2__c) != null)
            {
                //Try second UW assistant
                acc.U_W_Assistant_2__c = getUserId(psc.U_W_Assistant_2__c);
                handlerSet.put('UWAssistant2', true);
            }
            else
                acc.U_W_Assistant_2__c = null;
        }
        
        //SOC 29/08/13 PCI-000138 - Adding claims adjuster marine
        if(handlerSet.get('ClaimAdjusterMarine')==false)
        {       
            //secClaimHandlerDetails  = getSecClaimHandlerDetails(psc.Claim_adjuster_Marine__c);//SF-3812
            acc.Claim_Adjuster_Marine_lk__c = getUserId(psc.Claim_adjuster_Marine__c);//SF-3812
            //acc.Claim_adjuster_Marine_Id__c     = secClaimHandlerDetails.get('Id');//SF-3812
            //acc.Claim_adjuster_Marine_Name__c   = secClaimHandlerDetails.get('FullName');    //SF-3812                    
            //if(acc.Claim_adjuster_Marine_Id__c != null)//SF-3812
            if(acc.Claim_Adjuster_Marine_lk__c != null)//SF-3812
                handlerSet.put('ClaimAdjusterMarine', true);
        }
        
        //CRM-111
        if(handlerSet.get('AssistantEnergy')==false)
        {
            if (getUserId(psc.U_W_Assistant_Energy__c) != null)
            {
                acc.UW_Assistant_Energy__c = getUserId(psc.U_W_Assistant_Energy__c);
                handlerSet.put('AssistantEnergy', true);
                //handlerSet.put('UWAssistant1', true);
            }            
        }
        
        /*
        //SF 4368 start
        if(handlerSet.get('GardteamPARIS')==false){
            if (psc.Gard_Team__c != null)
                acc.Gard_Team__c = getCustomerTeamId(psc.Gard_Team__c);
            else
                acc.Gard_Team__c = null;
            
            handlerSet.put('GardteamPARIS', true);            
        }
        //4368 end*/
        
        return;
        
    }
    
    private void checkGICHandlers(Account acc, Admin_System_Company__c psc, Map<String, Boolean> handlerSet)
    {
        System.debug('psc--->'+psc);
        Map<String,String> secClaimHandlerDetails = null;
        //These are the handlers defined as coming from GIC
        //Cargo Dry, Cargo Liquid, CEP, Charterers, Crew, Defence
        //Also includes UW Assistant 1
        
        if(handlerSet.get('CargoDry1')==false)
        {
            //system.debug('psc.Claim_handler_Cargo_Dry__c--->'+psc.Claim_handler_Cargo_Dry__c);
            acc.Claim_handler_Cargo_Dry__c          = getUserId(psc.Claim_handler_Cargo_Dry__c);
            if(acc.Claim_handler_Cargo_Dry__c != null)
                handlerSet.put('CargoDry1', true);
        }
        
        if(handlerSet.get('CargoDry2')==false)
        {
            //secClaimHandlerDetails                  = getSecClaimHandlerDetails(psc.Claim_handler_Cargo_Dry_2__c);
            //acc.Claim_handler_Cargo_Dry_2_lk__c     = secClaimHandlerDetails.get('Id');    // MYGP-23
            acc.Claim_handler_Cargo_Dry_2_lk__c       = getUserId(psc.Claim_handler_Cargo_Dry_2__c);
            //acc.Claim_handler_Cargo_Dry_2_Id__c     = secClaimHandlerDetails.get('Id');  // MYGP-23
            //acc.Claim_handler_Cargo_Dry_2_Name__c   = secClaimHandlerDetails.get('FullName');  // MYGP-23
            //if(acc.Claim_handler_Cargo_Dry_2_Id__c != null)  //MYGP-23
            if(acc.Claim_handler_Cargo_Dry_2_lk__c != null)  
                handlerSet.put('CargoDry2', true);
        }
        
        if(handlerSet.get('CargoLiquid1')==false)
        {
            acc.Claim_handler_Cargo_Liquid__c       = getUserId(psc.Claim_handler_Cargo_Liquid__c);
            if(acc.Claim_handler_Cargo_Liquid__c != null)
                handlerSet.put('CargoLiquid1', true);
        }
        
        if(handlerSet.get('CargoLiquid2')==false)
        {
            //secClaimHandlerDetails                  = getSecClaimHandlerDetails(psc.Claim_handler_Cargo_Liquid_2__c);
            //acc.Claim_handler_Cargo_Liquid_2_lk__c  =secClaimHandlerDetails.get('Id');  //MYGP-23
            acc.Claim_handler_Cargo_Liquid_2_lk__c  = getUserId(psc.Claim_handler_Cargo_Liquid_2__c);
            //acc.Claim_handler_Cargo_Liquid_2_Id__c  = secClaimHandlerDetails.get('Id'); //MYGP-23
            //acc.Claim_handler_Cargo_Liquid_2_Name__c    = secClaimHandlerDetails.get('FullName'); //MYGP-23
            //if(acc.Claim_handler_Cargo_Liquid_2_Id__c != null)  //MYGP-23
            if(acc.Claim_handler_Cargo_Liquid_2_lk__c != null)
                handlerSet.put('CargoLiquid2', true);
        }
        
        if(handlerSet.get('CEP1')==false)
        {
            acc.Claim_handler_CEP__c                = getUserId(psc.Claim_handler_CEP__c);
            if(acc.Claim_handler_CEP__c != null)
                handlerSet.put('CEP1', true);
        }
        
        if(handlerSet.get('CEP2')==false)
        {
            //secClaimHandlerDetails                  = getSecClaimHandlerDetails(psc.Claim_handler_CEP_2__c);
            //acc.Claim_handler_CEP_2_lk__c      = secClaimHandlerDetails.get('Id');  //MYGP-23
            acc.Claim_handler_CEP_2_lk__c      = getUserId(psc.Claim_handler_CEP_2__c);
            //acc.Claim_handler_CEP_2_Id__c           = secClaimHandlerDetails.get('Id');  //MYGP-23
            //acc.Claim_handler_CEP_2_Name__c         = secClaimHandlerDetails.get('FullName'); //MYGP-23
            //if(acc.Claim_handler_CEP_2_Id__c != null)  //MYGP-23
            if(acc.Claim_handler_CEP_2_lk__c != null)
                handlerSet.put('CEP2', true);
        }
        
        if(handlerSet.get('Charterers1')==false)
        {
            system.debug('psc.Claim_handler_Charterers__c--->'+psc.Claim_handler_Charterers__c);
            acc.Claim_handler_Charterers__c         = getUserId(psc.Claim_handler_Charterers__c);
            if(acc.Claim_handler_Charterers__c != null)
                handlerSet.put('Charterers1', true);
        }
        
        if(handlerSet.get('Charterers2')==false)
        {
            //secClaimHandlerDetails                  = getSecClaimHandlerDetails(psc.Claim_handler_Charterers_2__c);
            //acc.Claim_handler_Charterers_2_lk__c  = secClaimHandlerDetails.get('Id'); //MYGP-23
            acc.Claim_handler_Charterers_2_lk__c  = getUserId(psc.Claim_handler_Charterers_2__c);
            //acc.Claim_handler_Charterers_2_Id__c    = secClaimHandlerDetails.get('Id'); //MYGP-23
            //acc.Claim_handler_Charterers_2_Name__c  = secClaimHandlerDetails.get('FullName'); //MYGP-23
            //if(acc.Claim_handler_Charterers_2_Id__c != null)  //MYGP-23
            if(acc.Claim_handler_Charterers_2_lk__c != null)
                handlerSet.put('Charterers2', true);
        }
        
        if(handlerSet.get('Crew1')==false)
        {
            acc.Claim_handler_Crew__c               = getUserId(psc.Claim_handler_Crew__c);
            if(acc.Claim_handler_Crew__c != null)
                handlerSet.put('Crew1', true);
        }
        
        if(handlerSet.get('Crew2')==false)
        {
            //secClaimHandlerDetails                  = getSecClaimHandlerDetails(psc.Claim_handler_Crew_2__c);
            //acc.Claim_handler_Crew_2_lk__c      = secClaimHandlerDetails.get('Id');  //MYGP-23
            acc.Claim_handler_Crew_2_lk__c         = getUserId(psc.Claim_handler_Crew_2__c);
            //acc.Claim_handler_Crew_2_Id__c          = secClaimHandlerDetails.get('Id');  //MYGP-23
            //acc.Claim_handler_Crew_2_Name__c        = secClaimHandlerDetails.get('FullName');  //MYGP-23
            //if(acc.Claim_handler_Crew_2_Id__c != null)  //MYGP-23
            if(acc.Claim_handler_Crew_2_lk__c != null)
                handlerSet.put('Crew2', true);
        }
        
        if(handlerSet.get('Defence1')==false)
        {
            acc.Claim_handler_Defence__c            = getUserId(psc.Claim_handler_Defence__c);
            if(acc.Claim_handler_Defence__c != null)
                handlerSet.put('Defence1', true);
        }
        
        if(handlerSet.get('Defence2')==false)
        {
            //secClaimHandlerDetails                  = getSecClaimHandlerDetails(psc.Claim_handler_Defence_2__c);
            //acc.Claim_handler_Defence_2_lk__c    = secClaimHandlerDetails.get('Id');  //MYGP-23
            acc.Claim_handler_Defence_2_lk__c    =  getUserId(psc.Claim_handler_Defence_2__c);
            //acc.Claim_handler_Defence_2_Id__c       = secClaimHandlerDetails.get('Id');  //MYGP-23
            //acc.Claim_handler_Defence_2_Name__c     = secClaimHandlerDetails.get('FullName');  //MYGP-23
            //if(acc.Claim_handler_Defence_2_Id__c != null)  //MYGP-23
            if(acc.Claim_handler_Defence_2_lk__c != null)
                handlerSet.put('Defence2', true);
        }
        
        //System.debug('handlerSet--1'+handlerSet);
        
        if(handlerSet.get('AccountingGIC')==false)
        {
            //System.debug('handlerSet--'+handlerSet);
            acc.Accounting_P_I__c = getUserId(psc.Accounting__c);
            if(acc.Accounting_P_I__c != null)
                handlerSet.put('AccountingGIC', true);
        }
        
        //System.debug('handlerSet--2'+handlerSet);
        
        if(handlerSet.get('UWAssistant1')==false)
        {
            //System.debug('psc.U_W_Assistant_1__c--' + psc.U_W_Assistant_1__c);
            if (getUserId(psc.U_W_Assistant_1__c) != null)
            {
                acc.UW_Assistant_1__c          = getUserId(psc.U_W_Assistant_1__c);
                handlerSet.put('UWAssistant1', true);
            }
            else if (getUserId(psc.U_W_Assistant_2__c) != null)
            { 
                acc.UW_Assistant_1__c          = getUserId(psc.U_W_Assistant_2__c);
                handlerSet.put('UWAssistant1', true);
            }
            else
                acc.UW_Assistant_1__c          = null;
            //acc.UW_Assistant_1__c             = null;
            
        }
        
        //CRM-111
        if(handlerSet.get('AssistantEnergy')==false)
        {
            if (getUserId(psc.U_W_Assistant_Energy__c) != null)
            {
                acc.UW_Assistant_Energy__c          = getUserId(psc.U_W_Assistant_Energy__c);
                handlerSet.put('AssistantEnergy', true);
                //handlerSet.put('UWAssistant1', true);
            }            
        }
        
        /*
        //SF 4368 start
        if(handlerSet.get('GardteamGIC')==false){
            if (psc.Gard_Team__c != null)
                acc.Gard_Team__c = getCustomerTeamId(psc.Gard_Team__c);
            else
                acc.Gard_Team__c = null;
            handlerSet.put('GardteamGIC', true);            
        }
        //4368 end
        */
        
        return; 
                          
    }
    
    //4368 start
    //This method will get the team SF id from it's region code
    public string getCustomerTeamId(string regionCode){
        List<Gard_Team__c> gardteam = [SELECT id from Gard_Team__c where region_code__c = :regionCode limit 1];
        String gardteamid = null; //changed from '' to null to solve SF-4505
        if(gardteam != null && gardteam.size() > 0){
            gardteamid = gardteam[0].id;
        }
        return gardteamid ;
    }
    //4368 end
    
    private string getUserId(string sUsername) {
        string sRetVal = null;
        if (sUsername != '' && sUsername != null) {
            for (User usr: lstUsr) {
                system.debug('sUsername--->'+sUsername);
                //system.debug('usr.GIC_ID__c--->'+usr.GIC_ID__c);
                //system.debug('usr.PARIS_ID__c--->'+usr.PARIS_ID__c);
                if (usr.GIC_ID__c == sUsername) {sRetVal = usr.Id;}
                if ((usr.GIC_ID__c == '' || usr.GIC_ID__c == null) && usr.PARIS_ID__c == sUsername) {sRetVal = usr.Id;}
            }
        }
        System.Debug(LoggingLevel.INFO,'UserID Found: ' + sRetVal);
        return sRetVal;
    }
    
    private Map<String, String> getSecClaimHandlerDetails(string sUsername) {
        Map<String,String> sRetVal = new Map<String, String>();
        if (sUsername != '' && sUsername != null) {
            for (User usr: lstUsr) {
                if (usr.GIC_ID__c == sUsername) 
                {
                    sRetVal.put('Id',usr.Id);
                    sRetVal.put('FullName',usr.FirstName + ' ' + usr.LastName);
                }
                if ((usr.GIC_ID__c == '' || usr.GIC_ID__c == null) && usr.PARIS_ID__c == sUsername) 
                {
                    sRetVal.put('Id',usr.Id);
                    sRetVal.put('FullName',usr.FirstName + ' ' + usr.LastName);
                }
            }
        }
        system.debug('Secondary claims handler details returned: ' + sRetVal.get('FullName'));
        return sRetVal;
    }
}