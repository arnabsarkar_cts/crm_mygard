global class Batch_AccountContactRelation implements Database.Batchable<sObject>
{   
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('the start executed');
        String query = 'SELECT AccountId,ContactId,Contact.Name,Id,IsDeleted,IsPrimary,Role FROM AccountContactRole order by ContactId,accountId';
        return Database.getQueryLocator(query);
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        System.debug('---The AccountContactRelation starts--');
        Map<Id,AccountContactRole> idToAccountContactRoleMap = new Map<Id,AccountContactRole>([SELECT AccountId,ContactId,Contact.Name,Id,IsDeleted,IsPrimary,Role FROM AccountContactRole order by ContactId,accountId]);
        Map<Id,AccountContactRelation> idToAccountContactRelationMap = new Map<Id,AccountContactRelation>([SELECT AccountId,ContactId,Contact.Name,Id,IsDeleted,Roles FROM AccountContactRelation order by contactId]);
        Map<String,List<String>> idsToRolesMap = new Map<String,List<String>>();
        Map<String,AccountContactRelation> idsToRelsMap = new Map<String,AccountContactRelation>();
        List<AccountContactRelation> aCRelsToBeUpserted = new List<AccountContactRelation>();
        
        String compoundedId;
        AccountContactRole thisACRole;
        AccountContactRelation thisACRel;
        List<String> thisRoles;
        
        for(Id aCRoleId : idToAccountContactRoleMap.keySet()){
            thisACRole = idToAccountContactRoleMap.get(aCRoleId);
            compoundedId = thisACRole.contactId+'-'+thisACRole.accountId;
            thisRoles = idsToRolesMap.get(compoundedId);
            if(thisRoles == null){
                thisRoles = new List<String>{thisACRole.role};
                    }else{
                        thisRoles.add(thisACRole.role);
                    }
            idsToRolesMap.put(compoundedId,thisRoles);
        }
        
        for(Id aCRel : idToAccountContactRelationMap.keySet()){
            thisACRel = idToAccountContactRelationMap.get(aCRel);
            compoundedId = thisACRel.contactId+'-'+thisACRel.accountId;
            idsToRelsMap.put(compoundedId,thisACRel);
        }
        List<String> ids;
        for(String compoundedIdKey : idsToRolesMap.keySet()){
            thisRoles = idsToRolesMap.get(compoundedIdKey);
            thisACRel = idsToRelsMap.get(compoundedIdKey);
            
            if(thisACRel == null){
                ids = compoundedIdKey.split('-');
                thisACRel = new AccountContactRelation(
                    contactId = ids[0],
                    accountId = ids[1]
                );
            }
            thisACRel.roles = String.join(thisRoles,';');
            aCRelsToBeUpserted.add(thisACRel);
        }
        List<Database.UpsertResult> dbURs = Database.upsert(aCRelsToBeUpserted,true);
        Integer successCount = 0;
        for(Database.UpsertResult dbUR : dbURs){
            if(dbUR.isSuccess()) successCount++;
        }
        System.debug('AccountContactRelation Creation Complete. '+successCount+' records passed out of '+dbURs.size());
    }
    global void finish(Database.BatchableContext BC)
    {
        
    }
    
}