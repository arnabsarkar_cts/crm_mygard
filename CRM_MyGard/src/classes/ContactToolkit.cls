/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 
    Description : This class provides support checking whether a contact record can be created
    Modified    : Modified to use the Type__c picklist field instead of record types.  
***************************************************************************************************/
public with sharing class ContactToolkit {
    
    public static boolean UseContactTypes = true;
    
    public static boolean EnableContactValidation = DataQualitySupport.GetSetting(UserInfo.getUserId()).Enable_Contact_Type_Validation__c;
    
    public static Map<Id, RecordType> ContactRecordTypes{
        get{
            if(ContactRecordTypes==null){
                ContactRecordTypes = new Map<Id, RecordType>();
                List<RecordType> lstConRecs = new List<RecordType>();
                lstConRecs = [Select Id, Name, DeveloperName FROM RecordType WHERE SobjectType = 'Contact' and IsActive = true];
                for(RecordType aRec:lstConRecs){
                    ContactRecordTypes.put(aRec.Id, aRec);
                }   
            }
            return ContactRecordTypes;
        }
        set;
    }
    
    public static List<Schema.PicklistEntry> ContactTypes{
        get{
            if(ContactTypes==null){
                Schema.DescribeFieldResult f = Schema.sObjectType.Contact.fields.Type__c;
                ContactTypes = f.getPicklistValues();
            }
            return ContactTypes;
        }
        set;
    }
    
   
    public static boolean checkForAccountRecordType (Contact contact) {
        if (Contact.AccountId == null || !EnableContactValidation) {
            return true;
        }
        Account acc = [Select Id, Company_Role__c FROM Account WHERE Id = :Contact.AccountId];
        Set<String> accRoles = MultiValueToolkit.multiValueToSet(acc.Company_Role__c);
        
        String theRecordTypeName = UseContactTypes ? contact.Type__c : ContactToolKit.ContactRecordTypes.get(contact.RecordTypeId).Name;
        
        if(theRecordTypeName != 'Document Addressee' && !accRoles.contains(theRecordTypeName)){
            return false;
        }
        
        return true;
    }
    
    public static List<Contact> checkForAccountRecordType(List<Contact> lstCon){
        
        if(lstCon!=null && EnableContactValidation){
            //Get a list of all the account ids to check
            //convert that list into a map keyed on account id mapping to a set that
            //contains the list of roles
            Set<Id> setAccIds = new Set<Id>();
            for(Contact aCon:lstCon){
                setAccIds.add(aCon.AccountId);
            }
            
            List<Account> lstAcc = new List<Account>();
            lstAcc = [Select Id, Company_Role__c FROM Account WHERE Id in : setAccIds];
            
            Map<Id, Set<String>> mapAccRoles = new Map<Id, Set<String>>();
            for(Account anAcc:lstAcc){
                mapAccRoles.put(anAcc.Id, MultiValueToolkit.multiValueToSet(anAcc.Company_Role__c));
            }
            
            
            //run through each contact and lookup the contact's account in the map.
            //in the set of roles check to see if the role corresponding to the
            //contact's record type exists
            //The Document Addressee contact type doesn't need to be in the list.
            
            for(Contact aCon:lstCon){
                String theRecordTypeName = UseContactTypes ? aCon.Type__c : ContactToolKit.ContactRecordTypes.get(aCon.RecordTypeId).Name;
                System.Debug('*** Contact Type = ' + theRecordTypeName);
                System.Debug('*** Account Company Roles = ' + mapAccRoles.get(aCon.AccountId));
                if(theRecordTypeName != 'Document Addressee' && !mapAccRoles.get(aCon.AccountId).contains(theRecordTypeName)){
                    aCon.addError(System.Label.Contact_Record_Type_Mis_match);
                }
            }
            
            //we are going to have to store the mapping between contact record type and role 
            //in a custom setting
        }
        
        
        
        return lstCon;
    }  
      
    //RCallear - added 21 01 14 to stop accounts having multiple primary contacts
    public static List<Contact> checkPrimaryContacts(LIST<Contact> contacts){
        //PSpenceley - 17/03/2014 - Too many SOQL queries: 101 - Exception being generated...
        //Changed the following...
        /*
        for(Contact c:Contacts){
            if(c.Primary_Contact__c){
                LIST<Contact> primaryContacts;
                if(c.Id == null){
                    primaryContacts = [select Id from Contact where accountid = :c.accountid and primary_contact__c = true];
                }else{
                    primaryContacts = [select Id from Contact where accountid = :c.accountid and primary_contact__c = true and Id <> :c.Id];
                }
                if(primaryContacts.size() >=1){
                    c.addError(System.Label.Contact_One_Primary);   
                }
            }
        }
        */
        
        //For this...
        Map<id, Set<id>> primaryContactIds = new Map<id, Set<id>>();
        Map<id, list<contact>> primaryContacts = new Map<id, list<contact>>();
        
        for (Contact c : contacts) {
            if(c.Primary_Contact__c){
                
                if (c.id != null) {
                    //If ISUPDATE
                    if (primaryContactIds.get(c.AccountId) == null) {
                        primaryContactIds.put(c.AccountId, new set<id>());
                    }
                    primaryContactIds.get(c.AccountId).add(c.id);
                } else {
                    //ELSE ISINSERT
                    if (primaryContacts.get(c.AccountId) == null) {
                        primaryContacts.put(c.AccountId, new list<contact>());
                    }
                    primaryContacts.get(c.AccountId).add(c);
                }
                
            }
        }
        
        List<Contact> currentPrimaryContacts;
        if (primaryContactIds.size() > 0) {
            currentPrimaryContacts = [SELECT AccountId, id FROM Contact WHERE AccountId IN :primaryContactIds.keySet() AND Primary_Contact__c = true];
        } else {
            currentPrimaryContacts = [SELECT AccountId, id FROM Contact WHERE AccountId IN :primaryContacts.keySet() AND Primary_Contact__c = true];
        }
        
        for (Contact c : currentPrimaryContacts) {
            if (primaryContactIds.size() > 0) {
                if (primaryContactIds.get(c.AccountId) == null) {
                    primaryContactIds.put(c.AccountId, new set<id>());
                }
                primaryContactIds.get(c.AccountId).add(c.id);
            } else {
                if (primaryContacts.get(c.AccountId) == null) {
                    primaryContacts.put(c.AccountId, new list<contact>());
                }
                primaryContacts.get(c.AccountId).add(c);
            }
        }
        
        
        if (primaryContactIds.size() > 0) {
            //IF ISUPDATE primaryContactIds will be populated...
            Map<id, Contact> contactsMap = new Map<id, Contact>(contacts);
            for (id accountId : primaryContactIds.keySet()) {
                if (primaryContactIds.get(accountId) != null && primaryContactIds.get(accountId).size() > 1) {
                    if (primaryContactIds.get(accountId) != null) {
                        for (id contactId : primaryContactIds.get(accountId)) {
                            if (contactsMap.get(contactId) != null) {
                                //contactsMap.get(contactId).addError(System.Label.Contact_One_Primary);  
                            }
                        }
                    }
                }
            }
        } else {
            //ELSE ISINSERT primaryContacts will be populated
            for (id accountId : primaryContacts.keySet()) {
                if (primaryContacts.get(accountId) != null && primaryContacts.get(accountId).size() > 1) {
                    if (primaryContacts.get(accountId) != null) {
                        for (contact c : primaryContacts.get(accountId)) {
                            if (c.id == null) {
                                c.addError(System.Label.Contact_One_Primary);   
                            }
                        }
                    }
                }
            }
        }
        
        
        //PSpenceley - 17/03/2014 - END...
        
        return contacts;
    }
      
    
}