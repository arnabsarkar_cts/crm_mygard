global class scheduleDeleteReconciliations implements Schedulable {
    global void execute (SchedulableContext SC) {
        deleteReconciliations rs = new deleteReconciliations();
        Database.executeBatch(rs, 2000);
    }
    
    static testMethod void testSchedule() {
        Test.StartTest();
            scheduleDeleteReconciliations testSched = new scheduleDeleteReconciliations();
            String strCRON = '0 0 4 * * ?';
            System.schedule('deleteReconciliations Test', strCRON, testSched);
        Test.StopTest();
    }
}