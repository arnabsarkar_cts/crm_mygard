global class BatchUpdateAddress implements Database.Batchable<sObject>{
    public String query;
    public boolean exclBillingAddress = false;
    public boolean exclShippingAddress = false;
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        // Build a list of Ids so we can requery, saving having to include all fields in the call to the batch job
        Set<Id> accIds = new Set<Id>();
        for (sObject so: scope) {
            Account acc = (Account)so;
            accIds.add(acc.Id);
        }
        // Retrieve all relevant accounts, with all fields we need to read/update
        List<Account> lstAcc = [SELECT Id, Name, 
                                BillingStreet, Billing_Street_Address_Line_1__c, Billing_Street_Address_Line_2__c, Billing_Street_Address_Line_3__c, Billing_Street_Address_Line_4__c, 
                                BillingCity, BillingState, BillingPostalCode, BillingCountry, BillingCity__c, BillingState__c, BillingPostalCode__c, BillingCountry__c,
                                ShippingStreet, Shipping_Street_Address_Line_1__c, Shipping_Street_Address_Line_2__c, Shipping_Street_Address_Line_3__c, Shipping_Street_Address_Line_4__c,
                                ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, ShippingCity__c, ShippingState__c, ShippingPostalCode__c, ShippingCountry__c
                                FROM Account WHERE Id IN :accIds];
        for(Account acc : lstAcc) {
            if(!exclBillingAddress) {
                if(acc.BillingStreet!=null && acc.BillingStreet.trim().length()>0) {
                    string[] splitFields = splitMultiLine(acc.BillingStreet);
                    if(splitFields.size()>0 && splitFields[0]!=null) acc.Billing_Street_Address_Line_1__c = splitFields[0].trim().length()>40 ? splitFields[0].trim().subString(0,40) : splitFields[0].trim();
                    if(splitFields.size()>1 && splitFields[1]!=null) acc.Billing_Street_Address_Line_2__c = splitFields[1].trim().length()>40 ? splitFields[1].trim().subString(0,40) : splitFields[1].trim();
                    if(splitFields.size()>2 && splitFields[2]!=null) acc.Billing_Street_Address_Line_3__c = splitFields[2].trim().length()>40 ? splitFields[2].trim().subString(0,40) : splitFields[2].trim();
                    if(splitFields.size()>3 && splitFields[3]!=null) acc.Billing_Street_Address_Line_4__c = splitFields[3].trim().length()>40 ? splitFields[3].trim().subString(0,40) : splitFields[3].trim();
                    if(splitFields.size()==0) {acc.Billing_Street_Address_Line_4__c=''; acc.Billing_Street_Address_Line_3__c=''; acc.Billing_Street_Address_Line_2__c=''; acc.Billing_Street_Address_Line_1__c='';}
                    if(splitFields.size()==1) {acc.Billing_Street_Address_Line_4__c=''; acc.Billing_Street_Address_Line_3__c=''; acc.Billing_Street_Address_Line_2__c='';}
                    if(splitFields.size()==2) {acc.Billing_Street_Address_Line_4__c=''; acc.Billing_Street_Address_Line_3__c='';}
                    if(splitFields.size()==3) {acc.Billing_Street_Address_Line_4__c='';}
                    acc.BillingCity__c       = acc.BillingCity;
                    acc.BillingState__c      = acc.BillingState;
                    acc.BillingPostalCode__c = acc.BillingPostalCode;
                    acc.BillingCountry__c    = acc.BillingCountry;
                }
            }
            if(!exclShippingAddress) {
                if(acc.ShippingStreet!=null && acc.ShippingStreet.trim().length()>0) {
                    string[] splitFields = splitMultiLine(acc.ShippingStreet);
                    if(splitFields.size()>0 && splitFields[0]!=null) acc.Shipping_Street_Address_Line_1__c = splitFields[0].trim().length()>40 ? splitFields[0].trim().subString(0,40) : splitFields[0].trim();
                    if(splitFields.size()>1 && splitFields[1]!=null) acc.Shipping_Street_Address_Line_2__c = splitFields[1].trim().length()>40 ? splitFields[1].trim().subString(1,40) : splitFields[1].trim();
                    if(splitFields.size()>2 && splitFields[2]!=null) acc.Shipping_Street_Address_Line_3__c = splitFields[2].trim().length()>40 ? splitFields[2].trim().subString(2,40) : splitFields[2].trim();
                    if(splitFields.size()>3 && splitFields[3]!=null) acc.Shipping_Street_Address_Line_4__c = splitFields[3].trim().length()>40 ? splitFields[3].trim().subString(3,40) : splitFields[3].trim();
                    if(splitFields.size()==0) {acc.Shipping_Street_Address_Line_4__c=''; acc.Shipping_Street_Address_Line_3__c=''; acc.Shipping_Street_Address_Line_2__c=''; acc.Shipping_Street_Address_Line_1__c='';}
                    if(splitFields.size()==1) {acc.Shipping_Street_Address_Line_4__c=''; acc.Shipping_Street_Address_Line_3__c=''; acc.Shipping_Street_Address_Line_2__c='';}
                    if(splitFields.size()==2) {acc.Shipping_Street_Address_Line_4__c=''; acc.Shipping_Street_Address_Line_3__c='';}
                    if(splitFields.size()==3) {acc.Shipping_Street_Address_Line_4__c='';}
                    acc.ShippingCity__c       = acc.ShippingCity;
                    acc.ShippingState__c      = acc.ShippingState;
                    acc.ShippingPostalCode__c = acc.ShippingPostalCode;
                    acc.ShippingCountry__c    = acc.ShippingCountry;
                }
            }
        }
        if (lstAcc.size()>0) update lstAcc;
    }
    
    global void finish(Database.BatchableContext BC) {
        //ASSERT: do something here
    }
    
    public static string[] splitMultiLine(string inVal) {
        // Standardise the newline characters to \n then split based on \n
        string[] outVal;
        inVal = inVal.replace('\r\n','\n');
        inVal = inVal.replace('\n\r','\n');
        inVal = inVal.replace('\r','\n');
        outVal = inVal.split('\n');
        return outVal;
    }
}