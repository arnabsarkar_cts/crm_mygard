/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 06/11/2013
***************************************************************************/
    
/// <summary>
///  Handler class for the Contact.MDMSyncContact trigger
/// </summary>
public without sharing class MDMSyncContactHandler {

	public static void InsertContactSyncStatus(List<Contact> newContacts, Boolean webServicesEnabled) {
		for(contact c : newContacts) {
			//TODO:consider moving these values to a custom setting... so they can be referenced else where...
			if (webServicesEnabled) {
				if (System.IsBatch() || System.IsFuture()) {
					c.Synchronisation_Status__c = 'Waiting To Sync';
					c.Sync_Date__c = Datetime.now().addMinutes(-59);
				} else {
					c.Synchronisation_Status__c = 'Sync In Progress';
				}
			} else {
				c.Synchronisation_Status__c = 'Synchronised';
			}
			c.Synchronisation_Action__c = 'create';
		}
	}
	
	public static void UpdateContactSyncStatus(List<Contact> newContacts, Map<id, Contact> oldContactMap, Boolean webServicesEnabled) {
		List<MDM_Service_Fields__c> fields = new List<MDM_Service_Fields__c>();
		for (MDM_Service_Fields__c field : MDM_Service_Fields__c.getAll().values()) {
			if ('Contact'.equalsIgnoreCase(field.Source_Object__c) && 'Field'.equalsIgnoreCase(field.Type__c) && field.Trigger_Synchronisation__c) {
				fields.add(field);
			}
		}
		
		system.debug('*** before update fieldset - ' + fields);
		for(contact c : newContacts) {
			//Loop through each field in the fieldSet, checking if the value is different between
			//new and old record. If one of these values has changed, we want to sync.
			if (webServicesEnabled) {
				if (c.Synchronisation_Status__c == 'Synchronised') {
					Contact oldContact = oldContactMap.get(c.ID);
					if (!oldContact.Deleted__c && c.Deleted__c) {
						if (System.IsBatch() || System.IsFuture()) {
							c.Synchronisation_Status__c = 'Waiting To Sync';
							c.Sync_Date__c = Datetime.now().addMinutes(-59);
						} else {
							c.Synchronisation_Status__c = 'Sync In Progress';
						}
						c.Synchronisation_Action__c = 'update';
					} else if (!c.Deleted__c) {
						for(MDM_Service_Fields__c field : fields) {
							if (c.get(field.Field_Name__c) != oldContact.get(field.Field_Name__c)) {
								if (System.IsBatch() || System.IsFuture()) {
									c.Synchronisation_Status__c = 'Waiting To Sync';
									c.Sync_Date__c = Datetime.now().addMinutes(-59);
								} else {
									c.Synchronisation_Status__c = 'Sync In Progress';
								}
								c.Synchronisation_Action__c = 'update';
								break;
							}
						}
					}
				}
			} else {
				c.Synchronisation_Status__c = 'Synchronised';
				c.Synchronisation_Action__c = 'update';
			}
		}
	}
	
	public static void SyncContact(Map<id, Contact> newContactsMap, Boolean webServicesEnabled) {
		List<id> ContactIds = new List<id>();
		ContactIds.addAll(newContactsMap.keyset());
		
		if (webServicesEnabled) {
			if (!System.IsBatch() && !System.IsFuture()) {
				MDMContactProxy.MDMUpsertContactAsync(ContactIds);
			}
		}
	}
	
	public static void SyncContact(List<Contact> newContacts, Map<id, Contact> newContactsMap, Map<id, Contact> oldContactsMap, Boolean webServicesEnabled) {
		if (webServicesEnabled) {
			List<id> UpsertedContactIds = new List<id>();
			List<id> DeleteContactIds = new List<id>();
			for(Contact c : newContacts) {
				Contact oldContact = oldContactsMap.get(c.ID);
				system.debug('*** After Update c.syncstatus = '+c.synchronisation_Status__c);
				system.debug('*** After Update oldContact.syncstatus = '+oldContact.synchronisation_Status__c);					
				
				if (c.synchronisation_Status__c == 'Sync In Progress' && oldContact.Synchronisation_Status__c != 'Sync In Progress'){
					
					if (c.Deleted__c) {
						system.debug('*** After Update (DELETE) calling sync for Contact : '+c.id);	
						DeleteContactIds.add(c.id);	
					} else {
						system.debug('*** After Update (UPSERT) calling sync for Contact : '+c.id);	
						UpsertedContactIds.add(c.id);	
					}
				}
			}
		
		
			if (UpsertedContactIds.size() > 0) {
				if (!System.IsBatch() && !System.IsFuture()) {
					MDMContactProxy.MDMUpsertContactAsync(UpsertedContactIds);
				}
			}
			if (DeleteContactIds.size() > 0) {
				if (!System.IsBatch() && !System.IsFuture()) {
					MDMContactProxy.MDMDeleteContactAsync(DeleteContactIds);
				}
			}
		}
	}
	
	public static void BeforeInsert(List<Contact> newContacts, Map<id, Contact> newContactsMap) {
		try {
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			ContactToolKit.checkForAccountRecordType(newContacts);
			ContactToolKit.checkPrimaryContacts(newContacts);
			InsertContactSyncStatus(newContacts, mdmSettings.Web_Services_Enabled__c);
		} catch (Exception ex) {
            Logger.LogException('MDMSyncContactHandler.BeforeInsert', ex);
			throw ex;
        }
	}
	
	public static void AfterInsert(List<Contact> newContacts, Map<id, Contact> newContactsMap) {
		try {
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			SyncContact(newContactsMap, mdmSettings.Web_Services_Enabled__c);
		} catch (Exception ex) {
            Logger.LogException('MDMSyncContactHandler.AfterInsert', ex);
			throw ex;
        }
	}
	
	public static void BeforeUpdate(List<Contact> newContacts, Map<id, Contact> newContactsMap, List<Contact> oldContacts, Map<id, Contact> oldContactsMap) {
		try {
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			ContactToolKit.checkForAccountRecordType(newContacts);
			ContactToolKit.checkPrimaryContacts(newContacts);
			UpdateContactSyncStatus(newContacts, oldContactsMap, mdmSettings.Web_Services_Enabled__c);
		} catch (Exception ex) {
            Logger.LogException('MDMSyncContactHandler.BeforeUpdate', ex);
			throw ex;
        }
	}
	
	public static void AfterUpdate(List<Contact> newContacts, Map<id, Contact> newContactsMap, List<Contact> oldContacts, Map<id, Contact> oldContactsMap) {
		try {
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			SyncContact(newContacts, newContactsMap, oldContactsMap, mdmSettings.Web_Services_Enabled__c);
		} catch (Exception ex) {
            Logger.LogException('MDMSyncContactHandler.AfterUpdate', ex);
			throw ex;
        }
	}
	
	public static void BeforeDelete(List<Contact> oldContacts, Map<id, Contact> oldContactsMap) {
		//DO Nothing for now...
	}
	
	public static void AfterDelete(List<Contact> oldContacts, Map<id, Contact> oldContactsMap) {
		//DO Nothing for now...
		// Trigger will never get to this point. 
		// Before delete should cancel the delete process therefore, after delete is never reached.
	}

}