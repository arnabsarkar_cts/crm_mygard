@isTest

Public Class TestViewPdf
{
    public static testMethod void TestViewPdfCtrl()
    {
        BlueCard_webservice_endpoint__c bweTest = new BlueCard_webservice_endpoint__c();
        bweTest.Name = 'BlueCard_WS_endpoint';
        bweTest.Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/Documents/DocumentFetcher/GetDocumentService_ep';
        insert bweTest;
        Test.startTest();
        ViewPdf vpTest = new ViewPdf();
        vpTest.pdf = 'testFile.pdf';
        vpTest.mimetype = 'testMimeType';
        Pagereference prTest = vpTest.fetchDoc();
        Test.stopTest();
    }
}