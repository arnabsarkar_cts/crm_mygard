/*
    Name of Developer:   Debosmeeta Paul
    Name of Company:     CTS
    
    Purpose of Class:    This class is used for submission of enrollment of client for PEME.
    SF-3833 : Activity feed - added by Shreyashi Sarkar
*/
public class PEMEEnrollmentCtrl
{
    public string loggedInContactId;
    public List<Contact> lstContact{get;set;}
    public List<selectOption> lstCompany{get;set;}//{lstCompany = new List<selectOption>();}
    public List<selectOption> lstCon{get;set;}//{lstCon = new List<selectOption>();}
    public map<String, Id> mapClient;
    public map<String, Id> mapContact;
    public map<String, Id> mapastect;
    public map<Id,String> mapIdastect;
    public map<Id, String> mapIdClient;
    public map<Id, String> mapIdContact;
    public String userType{get;set;}
    public String selectedClient{get;set;}//{selectedClient = null;}
    public List<String> selectedContact {get; set;}//{selectedContact = new List<String>();}
    public String clientName{get; set;}
   // public String ContactName{get; set;}
    public string loggedInAccountId;
    public PEME_Enrollment_Form__c pef{get;set;}
    public List<ManningAgent> addAgent{get;set;}
    public List<DebitDetails> debitDetailsLst{get;set;}
    public Integer debitDetailsLstPos;
    //Make-shift Components-block STARTS
    //SF-1902 created these because they were being referred to by either PEMEEnroll1,2 or 3; need to remove this block After 2018R2 release
    public Integer rowIndex{get;set;}
    public Integer rowIndex1{get;set;}
    public void removeTextBox(){}
    public PageReference addTextBox(){return null;}
    //Make-shift components ENDS
    public Integer rowIndexManning{get;set;}
    public Integer rowIndexDebit{get;set;}
    public Integer counter{get;set;}
    public List<PEME_Manning_Agent__c> lstAgent{get;set;}
    public List<PEME_Contact__c> lstCont{get;set;}
    public List<PEME_Debit_note_detail__c> lstDebit{get;set;}
    public Boolean isSuccess{get; set;}
    public boolean debitAddress{get;set;} //flag for radio button
    public String companyAddress{get;set;}
    public PEME_Debit_note_detail__c debit{get;set;}
    public String Add1{get;set;}
    public String Add2{get;set;}
    public String Add3{get;set;}
    public String Add4{get;set;}
    public String city{get;set;}
    public String state{get;set;}
    public String zip{get;set;}
    public String country{get;set;}
    public String strSelected{get;set;}//added For Global Client
    public String strGlobalClient{get;set;}//added For Global Client
    public List<String> selectedGlobalClient {get;set;}//added For Global Client
    public set<String> setGlobalClientIds;//added For Global Client
    Set<String> contractIdSet = new Set<String>();
    Set<String> accountIdSet;
    private static String homepage='/apex/HomePage';   
    // added for SF-3833
    String newEnrollment = 'PEME new enrollment';
    String newManningAgentStr;
    
    // end of SF-3833
    public List<SelectOption> getClientLst()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.clear();
        options.add(new SelectOption('',''));
        List<string> lstClients = new list<String>();
         //added For Global Client - start
        if(selectedGlobalClient!=null && selectedGlobalClient.size()==1){
            for(string globalClient:selectedGlobalClient){
                if(mapClient.containsKey(globalClient)){
                    lstClients.add(globalClient);
                    selectedClient = mapClient.get(globalClient); 
                  //  System.debug('selectedClient----'+selectedClient);
                }
            }       
        }
        else if(selectedGlobalClient!=null && selectedGlobalClient.size()>1){
            for(string globalClient:selectedGlobalClient){
                if(mapClient.containsKey(globalClient)){
                    lstClients.add(globalClient);
                }
            }        
        }else{
            lstClients = new List<String>(mapClient.keyset());
        } 
        //added For Global Client - end
        //lstClients = new List<String>(mapClient.keyset()); 
        lstClients.sort(); 
        for(String client: lstClients) 
        {                     
            options.add(new SelectOption(''+mapClient.get(client),client));
        }
     //   system.debug('********** client : '+options.size());
        return options;
    }
     public List<SelectOption> getastectLst()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.clear();
        List<string> lstastects = new list<String>();
        lstastects = new List<String>(mapastect.keyset()); 
        lstastects.sort(); 
        for(String astects: lstastects) 
        {  
           // options.add(new SelectOption('',''));                     
            //options.add(new SelectOption(''+mapastect.get(astects),astects));
            options.add(new SelectOption(astects,astects));
        }
     //   system.debug('********** astECT: '+options.size());
        return options;
    }
    public List<SelectOption> getManningLst()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',''));
        ManningAgent ma= new ManningAgent();
        lstAgent = new List<PEME_Manning_Agent__c>();
        if(addAgent != null && addAgent.size()>0)
        {
            for(ManningAgent man:addAgent)
            {
                options.add(new SelectOption(man.companyName,man.companyName));
            }
        }
        return options;
    }
    public PEMEEnrollmentCtrl()
    {
        lstCompany = new List<selectOption>();
        lstCon = new List<selectOption>();
        selectedClient = null;
        counter=0;  
        isSuccess = false;  
        debitAddress = false;  
        mapastect =  new map<String,Id>();  
        addAgent = new List<ManningAgent>();
        addAgent.add(new ManningAgent());
        debitDetailsLst = new List<DebitDetails>();
        debitDetailsLst.add(new DebitDetails());
        counter++; 
        pef = new PEME_Enrollment_Form__c();
        debit = new PEME_Debit_note_detail__c();
        selectedContact = new List<String>();
        MyGardHelperCtrl.CreateCommonData();
        loggedInContactId = MyGardHelperCtrl.LOGGED_IN_CONTACT_ID;
        loggedInAccountId = MyGardHelperCtrl.LOGGED_IN_ACCOUNT_ID;
        userType = MyGardHelperCtrl.USER_TYPE;
        contractIdSet = MyGardHelperCtrl.OWN_CONTRACT_ID_SET;
        accountIdSet = MyGardHelperCtrl.ACCOUNT_ID_SET;
        newManningAgentStr = '';
    }
    public pageReference loadData()
    {
      /*  List<User> lstUser = new List<User>([select id,Contactid,name,Email from User where id=:UserInfo.getUserId()]);
        if(lstUser!=null && lstUser.size()>0 && lstUser[0].Contactid!=null)
        {
            loggedInContactId = lstUser[0].Contactid;
            lstContact = new List<Contact>([select name, account.id,account.name,accountid, account.recordtypeid,account.company_Role__c, /*IsPeopleLineUser__c,*//*account.BillingPostalcode__c,account.BillingCountry__c,account.BillingState__c,account.Billing_Street_Address_Line_1__c,
            account.Billing_Street_Address_Line_2__c,account.Billing_Street_Address_Line_3__c,account.Billing_Street_Address_Line_4__c 
                                          from Contact 
                                          where id=:loggedInContactId]);*/
            mapClient =  new map<String,Id>();
            mapIdClient = new map<Id, String>(); 
            mapIdastect = new map<Id, String>();
        	lstCompany.clear();
        	lstCompany.add(new selectOption('',''));
          //  loggedInAccountId = lstContact[0].accountId;
            Account_Contact_Mapping__c accCont= [select PI_Access__c,Account__r.PEME_Enabled__c from Account_Contact_Mapping__c where contact__c =:loggedInContactId AND Account__c =:loggedInAccountId]; 
            if(accCont != null && (accCont.PI_Access__c == false || accCont.Account__r.PEME_Enabled__c == false))
            {
                PageReference pg = new PageReference(homepage);          
                return pg;
            }
            else if(userType == 'Broker')
            {
                  System.debug('selectedGlobalClient----'+selectedGlobalClient); 
                  /*  for (AggregateResult ast : [SELECT client__c,client__r.name,client__r.PEME_Enabled__c,client__r.peme_enrollment_Status__c,client__r.BillingCity__c,client__r.BillingCountry__c FROM Contract where broker__c In:accountIdSet and id in:contractIdSet and client__r.PEME_Enabled__c = true and (client__r.peme_enrollment_Status__c=null or client__r.peme_enrollment_Status__c = 'Not Enrolled') group by client__c,client__r.name,client__r.BillingCity__c,client__r.BillingCountry__c,client__r.peme_enrollment_Status__c,client__r.PEME_Enabled__c order by client__r.name])  */
                  for (AggregateResult ast : [SELECT client__c,client__r.name,client__r.PEME_Enabled__c,client__r.peme_enrollment_Status__c,client__r.BillingCity__c,client__r.BillingCountry__c FROM Contract where (broker__c In:accountIdSet AND Revoke_Broker_Access__c = false) and id in:contractIdSet and client__r.PEME_Enabled__c = true and (client__r.peme_enrollment_Status__c=null or client__r.peme_enrollment_Status__c = 'Not Enrolled') group by client__c,client__r.name,client__r.BillingCity__c,client__r.BillingCountry__c,client__r.peme_enrollment_Status__c,client__r.PEME_Enabled__c order by client__r.name]) 
                    {
                           mapClient.put(String.valueOf(ast.get('Name')),Id.valueOf(String.valueOf(ast.get('client__c')).substring(0,15)));
                           mapIdClient.put(Id.valueOf(String.valueOf(ast.get('client__c')).substring(0,15)),String.valueOf(ast.get('Name')));
                           lstCompany.add(new selectOption(String.valueOf(ast.get('client__c')).substring(0,15),String.valueOf(ast.get('Name'))+ ';' + String.valueOf(ast.get('BillingCity__c')) + ',' + String.valueOf(ast.get('BillingCountry__c')) ));
                    }  
                     
                      fetchGlobalClients();//added For Global Client  
                      System.debug('selectedGlobalClient----'+selectedGlobalClient);
                      if(selectedGlobalClient!=null && selectedGlobalClient.size()==1){
                            for(string globalClient:selectedGlobalClient){
                                if(mapClient.containsKey(globalClient)){
                                    selectedClient = mapClient.get(globalClient); 
                                    System.debug('selectedClient----1'+selectedClient);
                                    populateContact();
                                }
                            }       
                        } 
                    }
                else if(userType == 'Client')
                {
                  //  userType = 'Client';
                   // selectedClient = lstContact[0].accountId;
                    system.debug('***********selectedClient '+selectedClient );
                  //  clientName = lstContact[0].account.name;
                  //  mapIdClient.put((lstContact[0].account.id +'').substring(0,15),lstContact[0].account.name);
                    boolean hasAsset = false;
                    for(Contract ast:[SELECT client__r.name,client__r.PEME_Enrollment_Status__c FROM Contract where client__c=:loggedInAccountId and client__r.PEME_Enabled__c = true])
                    {
                        if(accCont != null && accCont.PI_Access__c == true)
                        {
                            hasAsset = true;
                        }
                        
                            system.debug('***********ast.client__r.PEME_Enrollment_Status__c'+ast.client__r.PEME_Enrollment_Status__c);
                            if(ast.client__r.PEME_Enrollment_Status__c != 'Enrolled' && ast.client__r.PEME_Enrollment_Status__c != 'Under Approval')
                            {
                                mapIdClient.put((ast.client__c +'').substring(0,15),ast.client__r.Name);
                                clientName = ast.client__r.Name;
                                selectedClient = ast.client__c;
                                system.debug('***********clientName'+clientName);
                            }
                            else{
                                PageReference pg = new PageReference(homepage);          
                                return pg;
                            }
                        
                        
                    }                   
                    if(!hasAsset)
                    {
                        PageReference pg = new PageReference(homepage);          
                        return pg;
                    }
                    populateContact();//get contact list for Client
                  //  ContactName = MyGardHelperCtrl.usr.Contact.Name;                   
                }
                else if(userType == 'External Service Provider')
                    {
                        PageReference pg = new PageReference(homepage);          
                        return pg;
                    } 
        showAddressAll();
        return null;
    }
    public void populateContact()
    {
     //   system.debug('selectedClient---'+selectedClient);
        lstCon.clear();
         mapIdContact = new map<Id, String>();  
        //lstCon.add(new selectOption('',''));
       // selectedContact = new List<String>();
        for(Contact con: [select name,id,Email, account.id,account.name,accountid, account.recordtypeid,account.company_Role__c,No_Longer_Employed_by_Company__c
                                          from Contact 
                                          where accountid =: selectedClient AND (NOT name  like '%test%') AND (NOT name  like '%support%') AND No_Longer_Employed_by_Company__c = false order by name])
                                      {
                                          //mapContact.put(con.name,(con.name +'').substring(0,15));
                                          mapIdContact.put(Id.valueOf(String.valueOf(con.id).substring(0,15)),String.valueOf(con.name));
                                          lstCon.add(new selectOption(String.valueOf(con.id).substring(0,15),String.valueOf(con.name) + ';' +(con.Email == null?'':con.Email)));
                                      }
    //  system.debug('lstContact size--->'+lstCon.size());
      //showaddress();
    }
   /* public void populateObject()
    {
        mapObject.clear();
        for(Asset ast:[SELECT object__c,object__r.name,client__r.name FROM Asset where broker__c=:lstContact[0].accountId and client__c=:selectedClient])
        {
            mapObject.put(ast.object__r.name,(ast.object__c +'').substring(0,15));
            mapIdObject.put((ast.object__c +'').substring(0,15),ast.object__r.name);
          //  system.debug('********** ast.object__r.name: '+ast.object__r.name);
        }
    } */ 
    public String getConName()
    {
        string allContact='';
        if(selectedContact!=null && selectedContact.size() > 0 )
        {
            
            for(String conName : selectedContact) {
               allContact = allContact + mapIdContact.get(conName) + ',';
            }
            if(allContact.contains(','))
            {
                allContact = allContact.substring(0,allContact.length()-1);
            }
            return allContact;
        }
        return null;
    }     
    public String getCName()
    {
        if(selectedClient!=null && mapIdClient.containsKey(selectedClient))
        {
            return mapIdClient.get(selectedClient);
        }
        return null;
    } 
    public pageReference cancel()
    {
        if(userType == 'broker')
        {
            pageReference pg = page.PEMEEnrollmentDone;
            return pg;
        }
        else
        {
            pageReference pg = page.PEMELandingPage;
            return pg;
        }
    }
    public class ManningAgent
    {
        public String companyName{set;get;}
        public String contactPerson{set;get;}
        public String phone{set;get;}
        public String email{set;get;}
        public String address1{set;get;}
        public String address2{set;get;}
        public String address3{set;get;}
        public String address4{set;get;}
        public String city{set;get;}
        public String zip{set;get;}
        public String state{set;get;}
        public String country{set;get;}
        public ManningAgent()
        {
            country='Philippines';
        }
    }
    public PageReference addManning()   
    {  
        try  
            {              
                addAgent.add(new ManningAgent());      
            }  
        catch(Exception e)  
            {  
                ApexPages.addMessages(e);  
            }
         return null;
    }
    
       
    public void removeManning()
    {	   
           rowIndexManning = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowIndexManningParam'));
           if(rowIndexManning<=addAgent.size()-1){
               addAgent.remove(rowIndexManning);
           }
    } 
    public pageReference goBankDetailsNext()
    {
        pageReference pg = page.PEMEEnrollPage2;
        pg.setRedirect(false);
        return pg;
    } 
    
    public class DebitDetails{        
        public String company{get;set;} // SF-1902
        public String contactName{get;set;}
        public String contactEmail{get;set;}
        public String address1{set;get;}
        public String address2{set;get;}
        public String address3{set;get;}
        public String address4{set;get;}
        public String city{get;set;}
        public String zipCode{get;set;}
        public String state{get;set;}
        public String country{get;set;}
        public String agent{get;set;}
        public String astects{get;set;} 
        public String comments{get;set;} //SF-1902
        public boolean debitAddress{get;set;}//SF-1902
    }
    
    public PageReference addDebit()   
    {  
        try  
            {              
                debitDetailsLst.add(new DebitDetails());
            }  
        catch(Exception e)  
            {  
                ApexPages.addMessages(e);  
            }  
         return null;
    } 
    
    public PageReference removeDebit()
    {     
           rowIndexDebit = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowIndexDebitParam'));
           if(rowIndexDebit<=debitDetailsLst.size()-1){
               debitDetailsLst.remove(rowIndexDebit);
           }
        return null;
    } 
    
    //To Be Removed after 2018R2
    public pageReference goPrevMannning()
    {
        pageReference pg = page.PEMEEnrollPage1;
        return pg;
    }
    
    public pageReference goPrevManning()
    {
        pageReference pg = page.PEMEEnrollPage1;
        return pg;
    }
    
    public pageReference goReviewNext()
    {
        pageReference pg = page.PEMEEnrollPage3;
        return pg;
    }
    
    public pageReference goprevBank()
    {
        pageReference pg = page.PEMEEnrollPage2;
        return pg;
    }
    
    public pageReference saveSubmit()
    {
        lstAgent = new List<PEME_Manning_Agent__c>();
        lstDebit = new List<PEME_Debit_note_detail__c>();
        lstCont = new List<PEME_Contact__c>();
        
        // added for SF-3833
        ClientActivityEvents__c newEnrollmentEvents = ClientActivityEvents__c.getValues(newEnrollment);
        String eventType ='';
        String newEnrollmentDescription = '';
        System.debug('new Enrollment Events ::::::: '+ newEnrollmentEvents);
        // end of SF-3833
        
        //creating PEME_Enrollment_Form__c record for current join Programme Request
        pef.Enrollment_Status__c = 'Under Approval';
        pef.ClientName__c = selectedClient;
        pef.Submitter__c = loggedInContactId;
        //Added aritra
        List<Group> queue_list = [select Id from Group where Name = 'PEME GARD ARENDAL QUEUE' and Type = 'Queue' limit 1];
        if(queue_list != null){
            pef.ownerId = queue_list[0].id;
        }
        System.debug('sfEdit pef to be update - '+pef);
        if(!Test.isRunningTest())
            insert pef;
        
        //creating a list on PEME_Manning_Agent__c records from ManningAgent Wrapper
        if(addAgent != null && addAgent.size()>0)
        {
            for(ManningAgent man:addAgent)
            {  
                if(man.companyName != '' && man.city != '')
                {      
                    PEME_Manning_Agent__c peme = new PEME_Manning_Agent__c(
                                                                            Company_Name__c = man.companyName,
                                                                            Requested_Contact_Person__c = man.contactPerson,
                                                                            Requested_Address__c = man.address1,
                                                                            Requested_Address_Line_2__c = man.address2,
                                                                            Requested_Address_Line_3__c = man.address3,
                                                                            Requested_Address_Line_4__c = man.address4,
                                                                            Requested_City__c = man.city,
                                                                            Requested_Zip_Code__c = man.zip,
                                                                            Requested_State__c = man.state,
                                                                            Requested_Country__c = man.country,
                                                                            Requested_Email__c = man.email,
                                                                            Requested_Phone__c =man.phone
                                                                           );
                    peme.PEME_Enrollment_Form__c = pef.id;
                	peme.Status__c = 'Under Approval';//'Under Approval''Approved';
                    lstAgent.add(peme);
                }
            }
        }
        
        //creating list of PEME_Debit_note_detail__c records from DebitDetails Wrapper
        if(debitDetailsLst != null && debitDetailsLst.size()>0)
        {
            for(DebitDetails dd:debitDetailsLst)
            {
                PEME_Debit_note_detail__c debit =new PEME_Debit_note_detail__c(
                                                                                Draft_Company_Name__c = dd.company,
                                                                                Contact_person_Name__c = dd.contactName,
                                                                                Contact_person_Email__c = dd.contactEmail,
                                                                                Requested_Address__c = dd.address1,
                                                                                Requested_Address_Line_2__c = dd.address2,
                                                                                Requested_Address_Line_3__c = dd.address3,
                                                                                Requested_Address_Line_4__c = dd.address4,
                                                                                Requested_City__c = dd.city,
                                                                                Requested_Zip_code__c = dd.zipCode,
                                                                                Requested_State__c = dd.state,
                                                                                Requested_Country__c = dd.country,
                    															Comments__c = dd.Comments
                													  );
                if(dd.debitAddress != null){
                    if(dd.debitAddress){
                        debit.Status__c = 'Approved';
                    }else{
                        debit.Status__c = 'Under Approval';
                    }
                }
                debit.PEME_Enrollment_Form__c = pef.id;
                lstDebit.add(debit);
            }
        }
        
        if(!Test.isRunningTest() && lstDebit.size() > 0){
            System.debug('about to insert debit note');
            for(PEME_Debit_note_detail__c pdn : lstDebit){
                System.debug('pdn - '+pdn);
            }
           insert lstDebit;//sf-1902
        }
        
        if(!Test.isRunningTest() && lstAgent.size() > 0){
            System.debug('about to insert manning agent');
            for(PEME_Manning_Agent__c pma : lstAgent){
                System.debug('pdn - '+pma);
            }
            insert lstAgent;
        }
        
        if(selectedContact.size()>0)
        {
            for(String contacts: selectedContact) 
            { 
               PEME_Contact__c peme = new PEME_Contact__c(Contact__c = contacts);
               lstCont.add(peme);
            }
        }
        
        if(lstCont.size()>0)
        {
            for(PEME_Contact__c pemeCon:lstCont)
            {
                pemeCon.PEME_Enrollment_Detail__c = pef.id;
            }
            if(!Test.isRunningTest())
                insert lstCont;
        }

       /* if(lstDebit.size()>0)
        {
            for(PEME_Debit_note_detail__c note:lstDebit)
            {
                note.PEME_Enrollment_Form__c = pef.id;
            }
            insert lstDebit;
        } */
        //pageReference pg = page.PEMEEnrollPage1;
      //  system.debug('isSuccess: '+isSuccess);
      
      //added for SF-3833
        for(PEME_Manning_Agent__c newManningAgent : lstAgent)
        {
            newManningAgentStr = newManningAgentStr + newManningAgent.Requested_Contact_Person__c + ',';
            system.debug('############ newManningAgentStr ###############'+newManningAgentStr);
        }
        
        newManningAgentStr = newManningAgentStr.removeEnd(',');
        system.debug('############ after remove end ###############'+newManningAgentStr);
        
      if(newEnrollmentEvents != null)
        {
          eventType = eventType + newEnrollmentEvents.Event_Type__c; 
          System.debug('new enrollment ::::::::::: event ::::::: '+eventType);
        }
        System.debug(':::::::: with in newManningAgent if block :::::::::::::');
      if(userType == 'Broker')                
        {   
            Account acc = [select id,name from account where id =:selectedClient limit 1];
            
            newEnrollmentDescription = 'Newly enrolled company: '+' '+acc.name+';'+' '+'New requested manning agent/agents: '+' '+newManningAgentStr+' '+'and debit note is created';
            System.debug(':::::::: with in userType == Broker if block :::::::::::::');            
            MyGardHelperCtrl.createClientEvents(UserInfo.getUserId(), selectedClient, eventType, null , null , null, null, newEnrollmentDescription);                    
        }               
        else if(userType == 'Client')               
        {   
            Account acc = [select id,name from account where id =:loggedInAccountId limit 1];
            newEnrollmentDescription = 'Newly enrolled company: '+' '+acc.name+';'+' '+'New requested manning agent/agents: '+newManningAgentStr+' '+'and debit note is created';
            System.debug(':::::::: with in userType == Client if block :::::::::::::');            
            MyGardHelperCtrl.createClientEvents(UserInfo.getUserId(), loggedInAccountId, eventType, null , null , null, null, newEnrollmentDescription);                 
        }
        
      // end of SF-3833
      
        isSuccess = true; //used for success Reporting to VF page
        return null;
        
    }
    
    public void showAddress()
    {	
                
        debitDetailsLstPos = Integer.valueOf(ApexPages.currentPage().getParameters().get('debitDetailsLstPosParam'));
        debitAddress = Boolean.valueOf(ApexPages.currentPage().getParameters().get('debitAddressParam'));
        
        if(debitDetailsLstPos == null) return;       
                
        DebitDetails dd = debitDetailsLst[debitDetailsLstPos];
        dd.debitAddress = debitAddress;
        
        List<Account> lstAcc = new List<Account>([select Name,BillingCity__c,BillingPostalcode__c,BillingCountry__c,BillingState__c,Billing_Street_Address_Line_1__c,Billing_Street_Address_Line_2__c,Billing_Street_Address_Line_3__c,Billing_Street_Address_Line_4__c 
                                                  from Account
                                                  where id =: selectedClient]);
        
        if(debitAddress && lstAcc.size() != 0) 
        {   
            Account con = lstAcc[0];
            dd.company = con.Name;
            dd.address1 = con.Billing_Street_Address_Line_1__c;
            dd.address2  = con.Billing_Street_Address_Line_2__c;
            dd.address3 = con.Billing_Street_Address_Line_3__c;
            dd.address4 = con.Billing_Street_Address_Line_4__c;
            dd.city = con.BillingCity__c;
            dd.state = con.BillingState__c;
            dd.country = con.BillingCountry__c;
            dd.zipCode = con.BillingPostalcode__c;
        }else{
            dd.company = '';
            dd.address1 = '';
            dd.address2 = '';
            dd.address3 = '';
            dd.address4 = '';
            dd.city = '';
            dd.zipCode = '';
            dd.state = '';
            dd.country = '';
        }
        debitDetailsLstPos = null;
        debitAddress = null;
  }
    
  //used if the selected client is changed
  public void showAddressAll()
    {	        
        List<Account> lstAcc = new List<Account>([select Name,BillingCity__c,BillingPostalcode__c,BillingCountry__c,BillingState__c,Billing_Street_Address_Line_1__c,Billing_Street_Address_Line_2__c,Billing_Street_Address_Line_3__c,Billing_Street_Address_Line_4__c 
                                                  from Account
                                                  where id =: selectedClient]);
        
        if(lstAcc.size() != 0) 
        {   
            Account con = lstAcc[0];
            for(DebitDetails dd : debitDetailsLst){   
                     if(dd.debitAddress!=null && dd.debitAddress){  
                        //dd.company = con.Name;
                        dd.address1 = con.Billing_Street_Address_Line_1__c;
                        dd.address2  = con.Billing_Street_Address_Line_2__c;
                        dd.address3 = con.Billing_Street_Address_Line_3__c;
                        dd.address4 = con.Billing_Street_Address_Line_4__c;
                        dd.city = con.BillingCity__c;
                        dd.state = con.BillingState__c;
                        dd.country = con.BillingCountry__c;
                        dd.zipCode = con.BillingPostalcode__c;
                    }
       		}	
    	}
  } 
    //added For Global Client - Start
        public void fetchGlobalClients(){
            String currentUserId = UserInfo.getUserId();
          //  system.debug('currentUserId: '+currentUserId);
            for(Extranet_Global_Client__c ast:[select Id,Name,Selected_Global_Clients__c,Selected_By__c,Selected_For_Client__c from Extranet_Global_Client__c where Selected_By__c=:currentUserId and Selected_For_Client__c=:loggedInAccountId LIMIT 1]){
                strGlobalClient = ast.Selected_Global_Clients__c;               
            }
          //  system.debug('strGlobalClient: '+strGlobalClient);
            if(strGlobalClient!=null && strGlobalClient!=''){
                selectedGlobalClient = new List<String>();//added For Global Client
                setGlobalClientIds = new set<String>();//added For Global Client         
                if(strGlobalClient.contains(';')){
                    setGlobalClientIds.addAll(strGlobalClient.split(';'));//added For Global Client 
                }else{
                    setGlobalClientIds.add(strGlobalClient);//added For Global Client   
                }
                if(setGlobalClientIds.size()>0){                                        
                    for(Account acc:[Select Id,Name from Account where Id IN:setGlobalClientIds]){
                        selectedGlobalClient.add(acc.Name);                                                 
                    }   
                }       
            }               
        }
         public PageReference refreshPage(){
            PageReference pg = Page.PEMEEnrollPage1;
            pg.setRedirect(true);
            return pg;
        }
         public void fetchSelectedClients(){
         //   system.debug('fetchSelectedClients');
         //   system.debug('strSelected: '+strSelected);
            set<String> setClientIds = new set<String>();
            selectedGlobalClient = new List<String>();//added For Global Client
            if(strSelected!=null && strSelected!=''){
                if(strSelected.contains(';')){
                    setClientIds.addAll(strSelected.split(';'));  
                }else{
                    setClientIds.add(strSelected);    
                }
                if(setClientIds.size()>0){
                    for(Account acc:[Select Id,Name from Account where Id IN:setClientIds]){
                        selectedGlobalClient.add(acc.Name);//added For Global Client    
                    }   
                }                  
            }            
        }              
        
}