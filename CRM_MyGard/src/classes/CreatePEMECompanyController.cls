public with sharing class CreatePEMECompanyController {
    private String manningAgentId{get;set;}
    private String retURL{get;set;}
    private String RecordType{get;set;}
    private String name_lastacc2{get;set;}
    private String RequestedAddress{get;set;}
    private String RequestedAddress1{get;set;}
    private String RequestedAddress2{get;set;}
    private String RequestedAddress3{get;set;}
    private String RequestedCity{get;set;}
    private String RequestedState{get;set;}private String RequestedPhone{get;set;}
    private String RequestedZipCode{get;set;}
    private String RequestedCountry{get;set;}
    private String RequestedEmail{get;set;}
    private String con12{get;set;}
    private String saveURL{get;set;}
    
    public pageReference onLoad(){
        if(String.isNotBlank(manningAgentId)){
            ManningAgentID__c manningAgent=ManningAgentID__c.getInstance(userInfo.getUserId());
            system.debug('manningAgent*****'+manningAgent);
            if(manningAgent.ManningAgentID__c!=null){
                delete manningAgent;
            }
            manningAgent=new ManningAgentID__c();
            manningAgent.SetupOwnerId=userInfo.getUserId();
            manningAgent.ManningAgentID__c=String.valueOf(manningAgentId);
            upsert manningAgent;
        }
        return new PageReference('/001/e?retURL='+retURL+'&RecordType='+RecordType+'&ent=Account&acc2='+name_lastacc2+'&00ND0000005Fx5v='+RequestedAddress+'&00ND0000005Fx5w='+RequestedAddress1+'&00ND0000005Fx5x='+RequestedAddress2+'&00ND0000005Fx5y='+RequestedAddress3+'&acc23='+RequestedCity+'&00ND0000005Fx5u='+RequestedState+'&00ND0000005Fx5t='+RequestedZipCode+'&CF00ND0000002g4Xj='+RequestedCountry+/*'&00ND0000002fsrD='+RequestedEmail+'&acc10='+RequestedPhone+*/'&Chosen=43&saveURL='+saveURL);
    }
    
    public CreatePEMECompanyController(){
        manningAgentId=apexpages.currentPage().getParameters().get('00ND00000035llZ');
        retURL=apexpages.currentPage().getParameters().get('retURL');
        RecordType=apexpages.currentPage().getParameters().get('RecordType');
        name_lastacc2=apexpages.currentPage().getParameters().get('acc2');
        RequestedAddress =apexpages.currentPage().getParameters().get('00ND0000005Fx5v'); 
        RequestedAddress1 = apexpages.currentPage().getParameters().get('00ND0000005Fx5w');
        RequestedAddress2 = apexpages.currentPage().getParameters().get('00ND0000005Fx5x');
        RequestedAddress3 = apexpages.currentPage().getParameters().get('00ND0000005Fx5y');
        RequestedCity = apexpages.currentPage().getParameters().get('acc23');
        RequestedState = apexpages.currentPage().getParameters().get('00ND0000005Fx5u');
        RequestedZipCode = apexpages.currentPage().getParameters().get('00ND0000005Fx5t');
        RequestedCountry = apexpages.currentPage().getParameters().get('CF00ND0000002g4Xj');
        //RequestedEmail = apexpages.currentPage().getParameters().get('00ND0000002fsrD');
        //RequestedPhone = apexpages.currentPage().getParameters().get('acc10');
        saveURL=apexpages.currentPage().getParameters().get('saveURL');
        system.debug('manningAgentId******'+manningAgentId);
        
        
    }
    
}