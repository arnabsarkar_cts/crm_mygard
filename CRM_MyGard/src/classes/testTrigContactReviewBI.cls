/**
* Contract Review Trigger Test Class
* When a new contract review is created
* - If the contract reviewer is populated it should remain as is
* - If it's blank it should be set to the current logged in user
*/
@isTest
private class testTrigContactReviewBI {

    static testMethod void myUnitTest() {
       	
       	UserRole ur = [SELECT Id FROM UserRole WHERE Name='CEO' LIMIT 1];
       	
       	User testUser = new User(LastName='Test', 
   								Alias='test',
   								Email='test@gard.no.test',
   								Username='test@gard.no.test',
   								CommunityNickname='test123',
   								UserRole=ur,
   								ProfileId=UserInfo.getProfileId(),
   								TimezoneSidKey='Europe/Amsterdam',
   								LocaleSidKey='no_NO',
   								EmailEncodingKey='ISO-8859-1',
   								LanguageLocaleKey='en_US'
   								);
   								
   		insert testUser;
   		
       	test.startTest();
       	
       	List<Contract_Review__c> crs = new List<Contract_Review__c>();
       	crs.add(new Contract_Review__c(Contract_Name__c='Test CR 1', Contract_Reviewer__c=testUser.Id));
       	crs.add(new Contract_Review__c(Contract_Name__c='Test CR 2'));
       	insert crs;
       	
       	test.stopTest();
       	
       	Contract_Review__c testCR1 = [SELECT Id, Contract_Reviewer__c FROM Contract_Review__c WHERE Id=:crs[0].Id];
       	Contract_Review__c testCR2 = [SELECT Id, Contract_Reviewer__c FROM Contract_Review__c WHERE Id=:crs[1].Id];
       	
       	system.assertEquals(testCR1.Contract_Reviewer__c, testUser.Id);
       	system.assertEquals(testCR2.Contract_Reviewer__c, UserInfo.getUserId());
    }
}