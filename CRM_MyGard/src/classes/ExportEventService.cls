//Created for SF-5063
public class ExportEventService {
	
    @AuraEnabled
    public static Event getEvent(String eventId){
        List<Event> events = Database.query('SELECT id,StartDateTime,EndDateTime,Subject,Description,Location FROM Event WHERE Id= :eventId');
        if(events.size()>0) return events.get(0);
        return null;
    }
    
    @AuraEnabled
    public static String getBaseURL(){
        return System.URL.getOrgDomainUrl().toExternalForm()+'/apex/ExportEvent';
    }
}