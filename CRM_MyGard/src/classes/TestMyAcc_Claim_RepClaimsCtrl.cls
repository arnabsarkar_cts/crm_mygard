//This test class is for MyAccountCtrl , ReportedClaimsCtrl , MyClaimsCtrl .

@isTest
public class TestMyAcc_Claim_RepClaimsCtrl{
      
    public static testmethod void myAccount(){
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        GardTestData test_rec = new GardTestData();
        test_rec.commonRecord();
        test_rec.customsettings_rec();
        System.assert(GardTestData.grdobj!= null , true);
        GardTestData.test_object_2nd.guid__c = '8ce8ac89-a6fd-1836-9e07';
        update GardTestData.test_object_2nd;
        System.assertEquals(GardTestData.test_object_2nd.guid__c , '8ce8ac89-a6fd-1836-9e07');
        
        System.RunAs(GardTestData.brokerUser){
            //MyAccountCtrl myacountctrl = new MyAccountCtrl();            
            Test.startTest();
             MyAccountCtrl myacountctrl = new MyAccountCtrl();
            myacountctrl.textInput = 'email:tony@george.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChange();
            myacountctrl.textInput = 'strt:10th,Park Street,House number 15,#city:Oslo#state:Oslo#ctry:Norway#pcode:0157#tcodec:india#tncomp:123456#email:abc@abc.com#companyWebsite:Gard.com#vstrt:11,Turnpike Lane,London,#vcity:New York#vstate:New York#vctry:USA#vpcode:USA#fax:123456#srt:oslo#city:abc#state:arendal#country:Norway#code:123#fax:123456#24hourNumber:18003334444';
            myacountctrl.showCompanyPopup();
            myacountctrl.textInput = 'email:test32@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChange();
            myacountctrl.textInput = 'email:3211@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Yoo#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChange();
            myacountctrl.textInput = 'email:test32@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Yoo#lName:Baooo#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChange();
            myacountctrl.textInput = 'strt:10th,Park Street,House number 15,#city:Oslo#state:Oslo#ctry:Norway#pcode:0157#tcodec:india#tncomp:123456#email:abc@abc.com#companyWebsite:gard.com#vstrt:11,Turnpike Lane,London,#vcity:New York#vstate:New York#vctry:USA#vpcode:USA#fax:123456#srt:oslo#city:abc#state:arendal#country:Norway#code:123#fax:123456#24hourNumber:18003334444';
            myacountctrl.confirmChange();
            myacountctrl.closePopup() ;
            myacountctrl.showPopup();
            myacountctrl.closeConfirmPopUp();
            Test.stopTest();
            /*
            MyAccountCtrl.SubscriptionsWrapper testSW= new MyAccountCtrl.SubscriptionsWrapper(true,false,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,true); //SF-4518 one parameter added for GardRules flad by Abhirup
            myacountctrl.confirmChangeToCompany();
            myacountctrl.onSubmit();
            
            myacountctrl.callAlert();
            PageReference pageRef = page.MyAccount;
            myacountctrl.closeAlert(); 
            myacountctrl.forceSave();
            myacountctrl.addNewContact();
            myacountctrl.getPickValues();
            myacountctrl.isNew=false;
            myacountctrl.currentContactId= GardTestData.brokerContact.id;
            myacountctrl.textInput1 = 'email:tony@george.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.updateContact();
            myacountctrl.textInput1 = 'email:test32@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.updateContact();
            myacountctrl.textInput1 = 'email:3211@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Yoo#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.updateContact();
            myacountctrl.textInput1 = 'email:test32@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Yoo#lName:Baooo#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.updateContact();
            myacountctrl.createCaseForNewUser();
            myacountctrl.callAlert1();
            //myacountctrl.hasNext();
            //myacountctrl.hasPrevious();
            //myacountctrl.next();
            //myacountctrl.previous();
            //myacountctrl.currentContactId= GardTestData.brokerContact.id;
            myacountctrl.populateEditableData();
            //System.assertEquals(myacountctrl.isShowPopup,false);
            */
        }
    }
    
    @isTest private static void myAccount2(){
        GardTestData test_rec = new GardTestData();
        test_rec.commonRecord();
        test_rec.customsettings_rec();
        System.assert(GardTestData.grdobj!= null , true);
        GardTestData.test_object_2nd.guid__c = '8ce8ac89-a6fd-1836-9e07';
        update GardTestData.test_object_2nd;
        System.assertEquals(GardTestData.test_object_2nd.guid__c , '8ce8ac89-a6fd-1836-9e07');
        
        
        System.RunAs(GardTestData.clientUser){
           
            Test.startTest();
            MyAccountCtrl myacountctrl = new MyAccountCtrl();
            MyAccountCtrl.SubscriptionsWrapper testSW= new MyAccountCtrl.SubscriptionsWrapper(true,false,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,true); //SF-4518 one parameter added for GardRules flad by Abhirup
            //myacountctrl.confirmChangeToCompany();
            myacountctrl.onSubmit();
            myacountctrl.callAlert();
            PageReference pageRef = page.MyAccount;
            myacountctrl.closeAlert(); 
            myacountctrl.forceSave();
            myacountctrl.addNewContact();
            myacountctrl.getPickValues();
            myacountctrl.isNew=false;
            myacountctrl.currentContactId= GardTestData.brokerContact.id;
            myacountctrl.textInput1 = 'email:tony@george.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.updateContact();
            myacountctrl.textInput1 = 'email:test32@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.updateContact();
            myacountctrl.textInput1 = 'email:3211@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Yoo#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.updateContact();
            myacountctrl.textInput1 = 'email:test32@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Yoo#lName:Baooo#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.updateContact();
            myacountctrl.confirmChangeToCompany();
            myacountctrl.createCaseForNewUser();
            myacountctrl.callAlert1();        
            Test.stopTest();
        }
    }
    
    @isTest private static void myAccount3(){
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        GardTestData test_rec = new GardTestData();
        test_rec.commonRecord();
        test_rec.customsettings_rec();
        System.assert(GardTestData.grdobj!= null , true);
        GardTestData.test_object_2nd.guid__c = '8ce8ac89-a6fd-1836-9e07';
        update GardTestData.test_object_2nd;
        System.assertEquals(GardTestData.test_object_2nd.guid__c , '8ce8ac89-a6fd-1836-9e07');
        
        System.RunAs(GardTestData.clientUser){
           
            Test.startTest();
            MyAccountCtrl myacountctrl = new MyAccountCtrl();
            MyAccountCtrl.SubscriptionsWrapper testSW= new MyAccountCtrl.SubscriptionsWrapper(true,false,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,false,true,true); //SF-4518 one parameter added for GardRules flad by Abhirup
            myacountctrl.textInput1 = 'email:tony@george.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChangeToCompany();
            myacountctrl.textInput1 = 'email:test32@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChangeToCompany();
            myacountctrl.textInput1 = 'email:3211@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Yoo#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChangeToCompany();
            myacountctrl.textInput1 = 'email:test32@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Yoo#lName:Baooo#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChangeToCompany();
            myacountctrl.onSubmit();
            myacountctrl.callAlert();
            PageReference pageRef = page.MyAccount;
            myacountctrl.closeAlert(); 
            myacountctrl.forceSave();
            myacountctrl.addNewContact();
            myacountctrl.getPickValues();
            myacountctrl.isNew=false;
            myacountctrl.currentContactId= GardTestData.brokerContact.id;
            myacountctrl.textInput1 = 'email:tony@george.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.updateContact();
            myacountctrl.textInput1 = 'email:test32@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.updateContact();
            myacountctrl.textInput1 = 'email:3211@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Yoo#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.updateContact();
            myacountctrl.textInput1 = 'email:test32@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Yoo#lName:Baooo#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.updateContact();
            myacountctrl.createCaseForNewUser();
            myacountctrl.callAlert1();
            //myacountctrl.hasNext();
            //myacountctrl.hasPrevious();
            //myacountctrl.next();
            //myacountctrl.previous();
            //myacountctrl.currentContactId= GardTestData.brokerContact.id;
            myacountctrl.populateEditableData();
            //System.assertEquals(myacountctrl.isShowPopup,false);
            Test.stopTest();
        }
    }
    
    public static testmethod void myAccount4(){
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        GardTestData test_rec = new GardTestData();
        test_rec.commonRecord();
        test_rec.customsettings_rec();
        System.assert(GardTestData.grdobj!= null , true);
        GardTestData.test_object_2nd.guid__c = '8ce8ac89-a6fd-1836-9e07';
        update GardTestData.test_object_2nd;
        System.assertEquals(GardTestData.test_object_2nd.guid__c , '8ce8ac89-a6fd-1836-9e07');
        System.RunAs(GardTestData.clientUser){   
            Test.startTest();
            MyAccountCtrl myacountctrl = new MyAccountCtrl();
            //System.assertEquals(myacountctrl.hasNext,false);
            //System.assertEquals(myacountctrl.hasPrevious,false);
            //myacountctrl.next();
            //myacountctrl.previous();  
            myacountctrl.currentContactId=GardTestData.clientContact.Id;
            myacountctrl.populateEditableData();
            myacountctrl.updateContact();
            myacountctrl.textInput = 'email:tony@george.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChange();
            myacountctrl.textInput = 'email:3211@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChange();
            myacountctrl.textInput = 'email:32111@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:tsat_22#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChange();
            myacountctrl.textInput = 'email:32111@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:tsat12_22#lName:Greg1#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChange();
            myacountctrl.textInput = 'email:32112@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:tsat12_22#lName:Greg12#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChange();
            myacountctrl.textInput = 'email:32112@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:tsat12_22#lName:Greg12#occp:Farmer#dept:Legal#strt:32,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561';
            myacountctrl.confirmChange();
            myacountctrl.textInput = 'strt:10th,Park Street,House number 15,#city:Oslo#state:Oslo#ctry:Norway#pcode:0157#tcodec:india#tncomp:123456#email:abc@abc.com#companyWebsite:gard.com#vstrt:11,Turnpike Lane,London,#vcity:New York#vstate:New York#vctry:USA#vpcode:USA#fax:123456#srt:oslo#city:abc#state:arendal#country:Norway#code:123#fax:123456#24hourNumber:18003334444';
            myacountctrl.confirmChange();
            
            /*myacountctrl.showCompanyPopup();
            myacountctrl.closePopup() ;
            myacountctrl.showPopup();
            myacountctrl.closeConfirmPopUp();
            System.assertEquals(myacountctrl.displayconfirmPopUpInfo,false);
            myacountctrl.confirmChangeToCompany();
            myacountctrl.callAlert();
            PageReference pageRef = page.MyAccount;
            Test.setCurrentPage(pageRef);
            myacountctrl.closeAlert();
            myacountctrl.getPickValues();
            myacountctrl.forceSave();
            myacountctrl.addNewContact();
            myacountctrl.callAlert1();
            //myacountctrl.createCaseForNewUser();
            //myacountctrl.closeEditPopup();
            */
            Test.stopTest();
        }
    }
    public static testmethod void myAccount5(){
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        GardTestData test_rec = new GardTestData();
        test_rec.commonRecord();
        test_rec.customsettings_rec();
        System.assert(GardTestData.grdobj!= null , true);
        GardTestData.test_object_2nd.guid__c = '8ce8ac89-a6fd-1836-9e07';
        update GardTestData.test_object_2nd;
        System.assertEquals(GardTestData.test_object_2nd.guid__c , '8ce8ac89-a6fd-1836-9e07');
        System.RunAs(GardTestData.clientUser){   
            Test.startTest();
            MyAccountCtrl myacountctrl = new MyAccountCtrl();
            myacountctrl.pCode ='';
            myacountctrl.country='';
            myacountctrl.department='';
            myacountctrl.occupation='';
            myacountctrl.lName='';
            myacountctrl.fName='';
            myacountctrl.tNumber='';
            //myacountctrl.previous();  
            myacountctrl.currentContactId=GardTestData.clientContact.Id;
            myacountctrl.textInput = 'email:tony@george.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561#one#two#three#four#five#123456#123';
            myacountctrl.confirmChangeToCompany();
            myacountctrl.textInput = 'email:3211@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:Tony#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561#one#two#three#four#five#123456#123';
            myacountctrl.confirmChangeToCompany();
            myacountctrl.textInput = 'email:32111@gmail.com#company:GEORG DUNCKER#companyType:PRIVATE#fName:tsat_22#lName:Greg#occp:Farmer#dept:Legal#strt:31,Kittelsbuktveien,test1,test#city:Arendal#state:Arendal#ctry:Norway#pcode:72401#tcode:123#tn:+47 224 54355#mob:8811445561#one#two#three#four#five#123456#123';
            myacountctrl.confirmChangeToCompany();
            myacountctrl.textInput = 'strt:10th,Park Street,House number 15,#city:Oslo#state:Oslo#ctry:Norway#pcode:0157#tcodec:india#tncomp:123456#email:abc@abc.com#companyWebsite:gard.com#vstrt:11,Turnpike Lane,London,#vcity:New York#vstate:New York#vctry:USA#vpcode:USA#fax:123456#srt:oslo#city:abc#state:arendal#country:Norway#code:123#fax:123456#24hourNumber:18003334444#one#two#three#four#five#123456#123';
            myacountctrl.confirmChangeToCompany();
            myacountctrl.populateAllContact();
            /*myacountctrl.showCompanyPopup();
            myacountctrl.closePopup() ;
            myacountctrl.showPopup();
            myacountctrl.closeConfirmPopUp();
            System.assertEquals(myacountctrl.displayconfirmPopUpInfo,false);
            myacountctrl.confirmChangeToCompany();
            myacountctrl.callAlert();
            PageReference pageRef = page.MyAccount;
            Test.setCurrentPage(pageRef);
            myacountctrl.closeAlert();
            myacountctrl.getPickValues();
            myacountctrl.forceSave();
            myacountctrl.addNewContact();
            myacountctrl.callAlert1();
            //myacountctrl.createCaseForNewUser();
            //myacountctrl.closeEditPopup();
            */
            Test.stopTest();
        }
    }
}