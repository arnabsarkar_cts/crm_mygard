//Generated by wsdl2apex

public class OBIEEReportResponse {
    public class ReportResponseBase {
        public String Report;
        private String[] Report_type_info = new String[]{'Report','http://messages.gard.no/v1_0/ReportResponse',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://messages.gard.no/v1_0/ReportResponse','true','false'};
        private String[] field_order_type_info = new String[]{'Report'};
    }
}