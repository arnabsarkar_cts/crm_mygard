//SF-4355 && SF-4612, removes the Examinee__c field's value from all the PEME_Exam_Detail__c records whose 
//ApprovalRejectionDate__c is atleast 30(Label - NoOfDaysToDeleteApprovedExaminee) days earlier than the batch run date
global class BatchDeleteApprovedExaminee implements Database.Batchable<sObject>{
    global BatchDeleteApprovedExaminee(){}
    
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){
        string noOfDays = System.Label.NoOfDaysToDeleteApprovedExaminee;
        Integer daysInNumber = 0;
        if(noOfDays!=null){
            daysInNumber = Integer.valueOf(noOfDays);
        }
        Date dt = System.Today() - daysInNumber;
        if(Test.isRunningtest()){
            dt = System.Today();
        }
        //String Query = 'select id,Examinee__c,Status__c from PEME_Exam_Detail__c where (Status__c= \'Approved\' or Status__c = \'Rejected-Need Input\') and ApprovedDate__c = :dt';//commented for SF-4612
        String Query = 'select id,Examinee__c,Status__c from PEME_Exam_Detail__c where (Status__c= \'Approved\' or Status__c = \'Rejected-Need Input\') and ApprovalRejectionDate__c <= :dt';//added for SF-4612
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<PEME_Exam_Detail__c> scope){
        for(PEME_Exam_Detail__c pemeExam:scope){
            if(pemeExam.Examinee__c !=null || pemeExam.Examinee__c !=''){
                pemeExam.Examinee__c = '';
            }
        }
        try{
            update scope;     
        }catch(Exception e){
            system.debug('Exception occurred in BatchDeleteApprovedExaminee - '+e.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC){}
}