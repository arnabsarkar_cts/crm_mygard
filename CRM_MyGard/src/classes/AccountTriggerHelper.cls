public with sharing class AccountTriggerHelper 
{
    
    private static Map<id, boolean> ApprovedChangesApplied = new Map<id, Boolean>();
    public static boolean approvedChangesAppliedForId(id accountId) {
        if (ApprovedChangesApplied.get(accountId) == null) {
            ApprovedChangesApplied.put(accountId, false);
        }
        return ApprovedChangesApplied.get(accountId);
    }

    public static CompanyRoleSettings__c CRSettings{
        get{
            if(CRSettings==null){
                CRSettings = CompanyRoleSettings__c.getOrgDefaults();
            }
            return CRSettings;
        }
        set;
    }
    
    //Used on insert
    //PS. When run on before insert no id is available for the accounts. This is why we have a second call to submit for approval in the after trigger.
    public static list<account> companyRolesChanged(list<account> newAccounts){
        DQ__c DQSettings;
        if(Test.isRunningTest()){
            DQSettings = new DQ__c();
            DQSettings.Enable_Role_Governance__c = true;
        }else{
            DQSettings = DataQualitySupport.GetSetting(UserInfo.getUserId());
        }
        if(DQSettings.Enable_Role_Governance__c){
            Set<String> approvalAcctIds = new Set<String>();
            for(account newA:newAccounts){
                newA.Unapproved_Company_Roles__c = newA.company_role__c;
                newA.Unapproved_Company_Roles_to_Add__c = newA.company_role__c;
                newA.Unapproved_Company_Roles_to_Remove__c = null;
                newA.Company_Role__c = '';
                
                newA.Sub_Roles_Unapproved__c = newA.Sub_Roles__c;               
                newA.Sub_Roles_to_Add__c = newA.Sub_Roles__c;
                newA.Sub_Roles__c = '';
                
                if (newA.Id != null) {
                    approvalAcctIds.add(newA.Id);
                }
                System.Debug('companyRolesChanged setting newA.Company_Role__c to ' + newA.Company_Role__c);
                
            }
            if(approvalAcctIds.size()>0){
                accountTriggerHelper.submitApproval(approvalAcctIds);
            }
        }
        /*
        if(!DQSettings.Enable_Role_Governance__c){
            for(account newA:newAccounts){
                newA.Company_Status__c = 'Active';
            }
        }
        */
        
        return newAccounts;
    }
    
    public static map<ID, account> companyRolesChanged(map<ID, account> newAccounts, map<ID,account> oldAccounts) {
        DQ__c DQSettings;
        if(Test.isRunningTest()){
            DQSettings = new DQ__c();
            DQSettings.Enable_Role_Governance__c = true;
        }else{
            DQSettings = DataQualitySupport.GetSetting(UserInfo.getUserId());
        }
        //Allow us to dynamically disable the role governance if we need to.
        System.Debug('DQ Settings ->'  + DQSettings);
        System.Debug('DQSettings.Enable_Role_Governance__c =' + DQSettings.Enable_Role_Governance__c);
        if(DQSettings.Enable_Role_Governance__c){
            Set<String> approvalAcctIds = new Set<String>();
    
            //for each account, check if company role has changed
            for(account newA:newAccounts.values())
            {
                account oldA = oldAccounts.get(newA.id);
                //If this account has unapproved role changes
                System.Debug('*** companyRolesChanged newA.role_change_approved__c =' + newA.role_change_approved__c);
                System.Debug('*** newA.company_role__c = ' + newA.company_role__c);
                System.Debug('*** oldA.company_role__c = ' + oldA.company_role__c);
                System.Debug('*** newA.Sub_Roles__c = ' + newA.Sub_Roles__c);
                System.Debug('*** oldA.Sub_Roles__c = ' + oldA.Sub_Roles__c);
                System.Debug('*** companyRolesChanged approvedChangesAppliedForId =' + approvedChangesAppliedForId(newA.id));
    
                
                if( newA.role_change_approved__c != true && 
                    (newA.company_role__c != oldA.company_role__c || newA.Sub_Roles__c != oldA.Sub_Roles__c)  && 
                    !approvedChangesAppliedForId(newA.id))
                {
                    //work out which roles have been added and which have been removed.
                    Set<String> setOldRoles = new Set<String>();
                    Set<String> setNewRoles = new Set<String>();
                    Set<String> setOldSubRoles = new Set<String>();
                    Set<String> setNewSubRoles = new Set<String>();
                    
                    Set<String> setAdded    = new Set<String>();
                    Set<String> setRemoved  = new Set<String>();
                    
                    Set<String> setSubRoleAdded = new Set<String>();
                    Set<String> setSubRoleRemoved = new Set<String>();
                    
                    List<String> lstAdded   = new List<String>();
                    List<String> lstRemoved = new List<String>();
                    
                    List<String> lstSubRoleAdded = new List<String>();
                    List<String> lstSubRoleRemoved = new List<String>();
                    
                    
                    setOldRoles = MultiValueToolkit.multiValueToSet(oldA.company_role__c);
                    setRemoved  = setOldRoles != null ? setOldRoles.clone() : null;
                    setNewRoles = MultiValueToolkit.multiValueToSet(newA.company_role__c);
                    setAdded    = setNewRoles != null ? setNewRoles.clone() : null;
                    
                    setOldSubRoles = MultiValueToolkit.multiValueToSet(oldA.Sub_Roles__c);
                    setSubRoleRemoved = setOldSubRoles!= null ? setOldSubRoles.clone() : null;
                    setNewSubRoles = MultiValueToolkit.multiValueToSet(newA.Sub_Roles__c);
                    setSubRoleAdded = setNewSubRoles!= null ? setNewSubRoles.clone() : null;
                    
                    //The set of added is new - old
                    if(setAdded!=null && setOldRoles!= null){setAdded.removeAll(setOldRoles);}
                    if(setSubRoleAdded!= null && setOldSubRoles != null){setSubRoleAdded.removeAll(setOldSubRoles);}
                    //The set of removed is old - new
                    if(setRemoved!= null && setNewRoles != null){setRemoved.removeAll(setNewRoles);}
                    if(setSubRoleRemoved!= null && setNewSubRoles!= null){setSubRoleRemoved.removeAll(setNewSubRoles);}
                    
                    
                    //Convert sets to lists
                    if(setAdded!=null){lstAdded.addAll(setAdded);}
                    if(setSubRoleAdded!=null){lstSubRoleAdded.addAll(setSubRoleAdded);}
                    if(setRemoved!=null){lstRemoved.addAll(setRemoved);}
                    if(setSubRoleRemoved!=null){lstSubRoleRemoved.addAll(setSubRoleRemoved);}
                    
                    //Reset the company roles until after approval
                    System.Debug(' companyRolesChanged - Resetting company roles until after approval');
                    newA.Unapproved_Company_Roles__c = newA.company_role__c;
                    newA.Company_Role__c = oldA.company_role__c;
                    newA.Unapproved_Company_Roles_to_Add__c = lstAdded!=null ? String.join(lstAdded, ';') : null;
                    newA.Unapproved_Company_Roles_to_Remove__c = lstRemoved!=null ? String.join(lstRemoved, ';') : null;
                    
                    //Reset the sub roles until after approval
                    System.Debug(' companyRolesChanged - Resetting company sub roles until after approval');
                    newA.Sub_Roles_Unapproved__c = newA.Sub_Roles__c;
                    newA.Sub_Roles__c = oldA.Sub_Roles__c;
                    newA.Sub_Roles_to_Add__c = lstSubRoleAdded != null ? String.join(lstSubRoleAdded, ';') : null;
                    newA.Sub_Roles_to_Remove__c = lstSubRoleRemoved != null ? String.join(lstSubRoleRemoved, ';') : null;
                    
                    newA.Approval_Process_Submitter__c = System.Userinfo.getUserId();//set the submitter to the current user.
                    
                    System.Debug('*** newA.company_role__c = ' + newA.company_role__c);
                    System.Debug('*** newA.Unapproved_Company_Roles__c = ' + newA.Unapproved_Company_Roles__c);
                    System.Debug('*** newA.Sub_Roles__c = ' + newA.Sub_Roles__c);
                    System.Debug('*** newA.Sub_Roles_Unapproved__c = ' + newA.Sub_Roles_Unapproved__c);
                                    
                    System.Debug('** Account changed now looks like : ' + newA);
                    approvalAcctIds.add(newA.Id);
                    System.Debug('companyRolesChanged setting newA.Company_Role__c to ' + newA.Company_Role__c);
                    
                }else{
                    System.Debug('*** companyRolesChanged - No change made');
                }
            }
            //Invoke the approval process asyncrhonously
            //Now call an approval process to get the role changes approved
            System.Debug('*** companyRolesChanged : Submitting async approval process for ids ' + approvalAcctIds);
            if(approvalAcctIds.size()>0){
                accountTriggerHelper.submitApproval(approvalAcctIds);
            }
        }     
        //CRM-31
        if(!DQSettings.Enable_Role_Governance__c){
            for(account newA:newAccounts.values()){
                account oldA = oldAccounts.get(newA.id);
                if(oldA.Company_Status__c == 'Inactive'){
                    //Check if there is any role changed
                    if(newA.company_role__c != oldA.company_role__c){
                        newA.Company_Status__c = 'Active';
                    }
                }
            }
        }  
        System.Debug('*** companyRolesChanged : newAccounts -> ' + newAccounts);
        
        return newAccounts;
    }
    
    //If the role was supposed to be added but that has been rejected remove it from the toAdd set and also from the Unapproved set.
    //If the role was supposed to be removed but that has been rejected remove it from the toRemove set but also add it back into the Unapproved set.
    //This method is used when the approval process can result in only some of the changes being approved.
    private static void checkRoleStatusAndAmend(String roleStatus, String role, Set<String> setToAdd, Set<String> setToRemove, Set<String> setUnapproved){
        if(roleStatus == 'Rejected'){
                if(setToAdd.contains(role)){
                    setToAdd.remove(role);
                    if(setUnapproved.contains(role)){
                        setUnapproved.remove(role);
                    }
                }
                if(setToRemove.contains(role)){
                    setToRemove.remove(role);
                    setUnapproved.add(role);                
                }
                
            }
    }

    //PS. 28/11/2013 Added DQ settings to enable \ disable this thorugh custom settings
    public static map<ID, account> approvedRolesChanged(map<ID, account> newAccounts) {

        DQ__c DQSettings = DataQualitySupport.GetSetting(UserInfo.getUserId());
        //Allow us to dynamically disable the role governance if we need to.
        System.Debug('DQ Settings ->'  + DQSettings);
        System.Debug('DQSettings.Enable_Role_Governance__c =' + DQSettings.Enable_Role_Governance__c);
        if(DQSettings.Enable_Role_Governance__c){
            
            for(account newA:newAccounts.values())
            {
                //Check if there are any approved Company Role Changes
                System.Debug('*** approvedRolesChanged newA.role_change_approved__c =' + newA.role_change_approved__c);
                System.Debug('*** approvedRolesChanged approvedChangesAppliedForId =' + approvedChangesAppliedForId(newA.id));
                System.Debug('*** approvedRolesChanged account = ' + newA);
                System.Debug('*** approvedRolesChanged account.Unapproved_Company_Roles__c = ' + newA.Unapproved_Company_Roles__c);
                System.Debug('*** approvedRolesChanged account.Sub_Roles_Unapproved__c = ' + newA.Sub_Roles_Unapproved__c);
                if(newA.Role_Change_Approved__c && !approvedChangesAppliedForId(newA.id))
                {
                    //Upsert role records for the additions and removals
                    Set<String> setToAdd = MultiValueToolkit.multiValueToSet(newA.Unapproved_Company_Roles_to_Add__c);
                    Set<String> setToRemove = MultiValueToolkit.multiValueToSet(newA.Unapproved_Company_Roles_to_Remove__c);
                    Set<String> setApproved = MultiValueToolkit.multiValueToSet(newA.Unapproved_Company_Roles__c);
                    
                    //Sub_Roles_Unapproved__c Sub_Roles_to_Add__c Sub_Roles_to_Remove__c
                    Set<String> setSubRolesToAdd = MultiValueToolkit.multiValueToSet(newA.Sub_Roles_to_Add__c);
                    Set<String> setSubRolesToRemove = MultiValueToolkit.multiValueToSet(newA.Sub_Roles_to_Remove__c);
                    Set<String> setSubRolesApproved = MultiValueToolkit.multiValueToSet(newA.Sub_Roles_Unapproved__c);
                    
                    System.Debug('*** Initial newA.unapproved_company_roles__c + ' + newA.unapproved_company_roles__c);
                    System.Debug('*** Initial setUnapproved = ' + setApproved);                 
                    //If a role change was rejected remove it from the list that it is in.      
                    
                    /* cDecisions MG 31/10/2013 Now we assume that one person approves all changes so we don't need to refer to individual approval status values */
                    /*
                    accountTriggerHelper.checkRoleStatusAndAmend(newA.Role_Status_Correspondent__c,             accountTriggerHelper.CRSettings.Correspondent__c,               setToAdd, setToRemove, setUnapproved);      
                    accountTriggerHelper.checkRoleStatusAndAmend(newA.Role_Status_Client__c,                    accountTriggerHelper.CRSettings.Client__c,                      setToAdd, setToRemove, setUnapproved);      
                    accountTriggerHelper.checkRoleStatusAndAmend(newA.Role_Status_Broker__c,                    accountTriggerHelper.CRSettings.Broker__c,                      setToAdd, setToRemove, setUnapproved);      
                    accountTriggerHelper.checkRoleStatusAndAmend(newA.Role_Status_Insurance_Company__c,         accountTriggerHelper.CRSettings.Insurance_Company__c,           setToAdd, setToRemove, setUnapproved);      
                    accountTriggerHelper.checkRoleStatusAndAmend(newA.Role_Status_External_Service_Provider__c, accountTriggerHelper.CRSettings.External_Service_Provider__c,   setToAdd, setToRemove, setUnapproved);      
                    accountTriggerHelper.checkRoleStatusAndAmend(newA.Role_Status_Other__c,                     accountTriggerHelper.CRSettings.Other__c,                       setToAdd, setToRemove, setUnapproved);      
                    accountTriggerHelper.checkRoleStatusAndAmend(newA.Role_Status_Bank__c,                      accountTriggerHelper.CRSettings.Bank__c,                        setToAdd, setToRemove, setUnapproved);      
                    accountTriggerHelper.checkRoleStatusAndAmend(newA.Role_Status_Investments__c,               accountTriggerHelper.CRSettings.Investments__c,                 setToAdd, setToRemove, setUnapproved);      
                    */
                    //MGAR cDecisions 30/10/2013 use MDM Roles instead of Roles__c
                    //upsertRoles(setToAdd, setToRemove, newA.id);      
                    
                    
                    //Build a new value for unapproved company roles from the approved values.
                    List<String> lstApproved = new List<String>();
                    lstApproved.addAll(setApproved);          
                    //PS 15/11/13 - removing all roles should be allowed... changing...
                        //newA.Company_Role__c = lstApproved.size()>0 ? String.join(lstApproved, ';') : newA.Company_Role__c;   
                    //TO
                    newA.Company_Role__c = lstApproved.size()>0 ? String.join(lstApproved, ';') : null; 
                    System.Debug('approvedRolesChanged setting newA.Company_Role__c to ' + newA.Company_Role__c);
                    System.Debug('** setSubRolesApproved ' + setSubRolesApproved);
                    //Build a new value for unapproved company sub roles from the approved values.
                    List<String> lstApprovedSubRoles = new List<String>();
                    lstApprovedSubRoles.addAll(setSubRolesApproved);     
                    System.Debug('** lstApprovedSubRoles ' + lstApprovedSubRoles); 
                    newA.Sub_Roles__c = lstApprovedSubRoles.size()>0 ? String.join(lstApprovedSubRoles, ';') : null;                    
                    System.Debug('** newA.Sub_Roles__c = ' + newA.Sub_Roles__c);
                    //a subset of the roles control visibiltiy and we have flags on the account that reflect that which have to be maintained.
                    
                    System.Debug('** setToAdd ' + setToAdd);
                    //Set role flags on the account based on the approved ones.
                    if(setToAdd != null && setToAdd.size()>0){
                        for(String aRole: setToAdd){                    
                            if(aRole==accountTriggerHelper.CRSettings.Correspondent__c){newA.Role_Correspondent__c = true;}
                            if(aRole==accountTriggerHelper.CRSettings.Client__c){newA.Role_Client__c = true;}
                            if(aRole==accountTriggerHelper.CRSettings.Investments__c){newA.Role_Investments__c = true;} 
                        }
                    }
                    //unset role flags on any roles that are being removed.
                    if(setToRemove != null && setToRemove.size()>0){
                        for(String aRole: setToRemove){
                            if(aRole==accountTriggerHelper.CRSettings.Correspondent__c){newA.Role_Correspondent__c = false;}
                            if(aRole==accountTriggerHelper.CRSettings.Client__c){newA.Role_Client__c = false;}
                            if(aRole==accountTriggerHelper.CRSettings.Investments__c){newA.Role_Investments__c = false;}                                        
                        }
                    }
                    
                    
                    //Reset transient fields
                    newA.Unapproved_Company_Roles__c = null;
                    newA.Unapproved_Company_Roles_to_Add__c = null;
                    newA.Unapproved_Company_Roles_to_Remove__c = null;
                    //Sub_Roles_Unapproved__c Sub_Roles_to_Add__c Sub_Roles_to_Remove__c
                    newA.Sub_Roles_to_Add__c = null;
                    newA.Sub_Roles_to_Remove__c = null;
                    newA.Sub_Roles_Unapproved__c = null;                
                    
                    newA.Role_Change_Approved__c = false;
                    
                    newA.Role_Status_Correspondent__c = null;
                    newA.Role_Status_Client__c = null;
                    newA.Role_Status_Broker__c = null;
                    newA.Role_Status_Insurance_Company__c = null;
                    newA.Role_Status_External_Service_Provider__c = null;
                    newA.Role_Status_Other__c = null;
                    newA.Role_Status_Bank__c = null;
                    newA.Role_Status_Investments__c = null;
                    approvedChangesApplied.put(newA.id, true);            
                    //newA.Approval_Process_Submitter__c =null;   //Commented out for CRM 144. This field is used to send email for 'Role Change Approval Process v0.29'
                    //Reset transient fields
                    
                    System.Debug('*** approvedRolesChanged - Setting approvedChangesAppliedForId =' + approvedChangesAppliedForId(newA.id));
                    System.Debug('*** approvedRolesChanged - Setting newA.Role_Change_Approved__c =' + newA.Role_Change_Approved__c);
                    System.Debug('*** approvedRolesChanged account.Unapproved_Company_Roles__c = ' + newA.Unapproved_Company_Roles__c);
                    System.Debug('*** approvedRolesChanged account.Sub_Roles_Unapproved__c = ' + newA.Sub_Roles_Unapproved__c);             
                }else{
                    System.Debug('*** approvedRolesChanged - No Change Made - newA.Role_Change_Approved__c =' + newA.Role_Change_Approved__c);
                }
            }
        }
        
        return newAccounts;
    }
    //MGAR cDecisions 30/10/2013 use MDM Roles instead of Roles__c
    /*@future
    static void upsertRoles(Set<String> setToAdd, Set<String> setToRemove, String AcctId){
        //Create a list of roles to allow us to make upserts
        List<Role__c> lstRoles = new List<Role__c>();
        for(String s: setToAdd){
            lstRoles.add(new Role__c(Role_Key__c = AcctId + '-' +s, Role__c = s, IsActive__c = true, Activated__c = System.now(), Account__c = AcctId));
        }
        for(String s: setToRemove){
            lstRoles.add(new Role__c(Role_Key__c = AcctId + '-' +s, Role__c = s, IsActive__c = false, Inactivated__c = System.now(), Account__c = AcctId));
        }
        if(lstRoles.size()>0){upsert lstRoles Role_Key__c;}
    }
    */

    
    //PSpenceley - 18/03/2014 - Made the following code bulk.
    //Previously, loop was in the method - each account id was calling @FUTURE
    //Now, send set<id> to @FUTURE
    public static void submitApproval(Set<String> acctIds){     
        if(System.isBatch() || System.isFuture() || System.isScheduled()){
            if(acctIds!=null){
                for(String id : acctIds){
                    System.Debug('*** submitApproval : About to submit approval for id : ' + id);
                    Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                    req1.setObjectId(id);
                    System.Debug('submitApproval - In Batch, Future or Scheduled so invoking process synchronously.');                                      
                    Approval.ProcessResult result= Approval.process(req1);
                    System.Debug('*** submitApproval : Approval Results : ' + result);
                }
            }
        } 
        //CRM 144 - Commented out since the code already exists in after insert trigger
        /*else {
            System.Debug('submitApproval - Not in Batch, Future or Scheduled so invoking process asynchronously.');
            submitApprovalFuture(acctIds);
        }*/
        //CRM 144
    }
    
    @future
    public static void submitApprovalFuture(String objectId){
        System.Debug('*** submitApprovalFuture : for object id ' + objectId );
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setObjectId(objectId);
        Approval.ProcessResult result = Approval.process(req1);
        System.Debug('*** submitApprovalFuture : Approval Results : ' + result);
    }
    
    @future
    public static void submitApprovalFuture(Set<String> acctIds){
        if(acctIds!=null){
            for(String objectId : acctIds){
                System.Debug('*** submitApprovalFuture : for object id ' + objectId );
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setObjectId(objectId);
                Approval.ProcessResult result = Approval.process(req1);
                System.Debug('*** submitApprovalFuture : Approval Results : ' + result);
            }
        }
    }
    
    public static void splitBillingAddress(List<Account> accounts, Map<Id,Account> oldAccounts) {
        for (Account acc : accounts) {
            Account oldAcc = oldAccounts!=null ? oldAccounts.get(acc.Id) : null;
            // If SF BillingStreet field has changed split lines out into separate fields
            if((oldAcc==null && !String.isEmpty(acc.BillingStreet)) || (oldAcc!=null && acc.BillingStreet!=oldAcc.BillingStreet)) {
                splitBillingStreetLines(acc);
            }
            // Else if split street address lines have been updated update the SF street address field
            else if(oldAcc==null ||
                    acc.Billing_Street_Address_Line_1__c!=oldAcc.Billing_Street_Address_Line_1__c ||
                    acc.Billing_Street_Address_Line_2__c!=oldAcc.Billing_Street_Address_Line_2__c ||
                    acc.Billing_Street_Address_Line_3__c!=oldAcc.Billing_Street_Address_Line_3__c ||
                    acc.Billing_Street_Address_Line_4__c!=oldAcc.Billing_Street_Address_Line_4__c) {
                acc.BillingStreet =  !String.isEmpty(acc.Billing_Street_Address_Line_1__c) ? acc.Billing_Street_Address_Line_1__c : '';
                acc.BillingStreet += !String.isEmpty(acc.Billing_Street_Address_Line_2__c) ? '\r\n'+acc.Billing_Street_Address_Line_2__c : '';
                acc.BillingStreet += !String.isEmpty(acc.Billing_Street_Address_Line_3__c) ? '\r\n'+acc.Billing_Street_Address_Line_3__c : '';
                acc.BillingStreet += !String.isEmpty(acc.Billing_Street_Address_Line_4__c) ? '\r\n'+acc.Billing_Street_Address_Line_4__c : '';
            }
            
            // If any of the non street address SF fields have been updated sync with the split out address fields
            if (oldAcc==null) {
                if(!String.isEmpty(acc.BillingCity) || !String.isEmpty(acc.BillingState) || !String.isEmpty(acc.BillingPostalCode) || !String.isEmpty(acc.BillingCountry)) {
                    acc.BillingCity__c       = acc.BillingCity;
                    acc.BillingState__c      = acc.BillingState;
                    acc.BillingPostalCode__c = acc.BillingPostalCode;
                    acc.BillingCountry__c    = acc.BillingCountry;
                } else {
                    acc.BillingCity       = acc.BillingCity__c;
                    acc.BillingState      = acc.BillingState__c;
                    acc.BillingPostalCode = acc.BillingPostalCode__c;
                    acc.BillingCountry    = acc.BillingCountry__c;
                }
            } else {
                if(acc.BillingCity !=oldAcc.BillingCity || acc.BillingState !=oldAcc.BillingState || acc.BillingPostalCode !=oldAcc.BillingPostalCode || acc.BillingCountry !=oldAcc.BillingCountry) {
                    acc.BillingCity__c       = acc.BillingCity;
                    acc.BillingState__c      = acc.BillingState;
                    acc.BillingPostalCode__c = acc.BillingPostalCode;
                    acc.BillingCountry__c    = acc.BillingCountry;   
                } else {
                    acc.BillingCity       = acc.BillingCity__c;
                    acc.BillingState      = acc.BillingState__c;
                    acc.BillingPostalCode = acc.BillingPostalCode__c;
                    acc.BillingCountry    = acc.BillingCountry__c;
                }
            }
        }
    }
    
    public static void splitShippingAddress(List<Account> accounts, Map<Id,Account> oldAccounts) {
        for (Account acc : accounts) {
            Account oldAcc = oldAccounts!=null ? oldAccounts.get(acc.Id) : null;
            // If SF ShippingStreet field has changed split lines out into separate fields
            if((oldAcc==null && !String.isEmpty(acc.ShippingStreet)) || (oldAcc!=null && acc.ShippingStreet!=oldAcc.ShippingStreet)) {  
                splitShippingStreetLines(acc);
            }
            // Else if split street address lines have been updated update the SF street address field
            else if(oldAcc==null ||
                    acc.Shipping_Street_Address_Line_1__c!=oldAcc.Shipping_Street_Address_Line_1__c ||
                    acc.Shipping_Street_Address_Line_2__c!=oldAcc.Shipping_Street_Address_Line_2__c ||
                    acc.Shipping_Street_Address_Line_3__c!=oldAcc.Shipping_Street_Address_Line_3__c ||
                    acc.Shipping_Street_Address_Line_4__c!=oldAcc.Shipping_Street_Address_Line_4__c) {
                acc.ShippingStreet =  !String.isEmpty(acc.Shipping_Street_Address_Line_1__c) ? acc.Shipping_Street_Address_Line_1__c : '';
                acc.ShippingStreet += !String.isEmpty(acc.Shipping_Street_Address_Line_2__c) ? '\r\n'+acc.Shipping_Street_Address_Line_2__c : '';
                acc.ShippingStreet += !String.isEmpty(acc.Shipping_Street_Address_Line_3__c) ? '\r\n'+acc.Shipping_Street_Address_Line_3__c : '';
                acc.ShippingStreet += !String.isEmpty(acc.Shipping_Street_Address_Line_4__c) ? '\r\n'+acc.Shipping_Street_Address_Line_4__c : '';
            }
            
            // If any of the non street address SF fields have been updated sync with the split out address fields
            if (oldAcc==null) {
                if(!String.isEmpty(acc.ShippingCity) || !String.isEmpty(acc.ShippingState) || !String.isEmpty(acc.ShippingPostalCode) || !String.isEmpty(acc.ShippingCountry)) {
                    acc.ShippingCity__c       = acc.ShippingCity;
                    acc.ShippingState__c      = acc.ShippingState;
                    acc.ShippingPostalCode__c = acc.ShippingPostalCode;
                    acc.ShippingCountry__c    = acc.ShippingCountry;
                } else {
                    acc.ShippingCity       = acc.ShippingCity__c;
                    acc.ShippingState      = acc.ShippingState__c;
                    acc.ShippingPostalCode = acc.ShippingPostalCode__c;
                    acc.ShippingCountry    = acc.ShippingCountry__c;
                }
            } else {
                if(acc.ShippingCity !=oldAcc.ShippingCity || acc.ShippingState !=oldAcc.ShippingState || acc.ShippingPostalCode !=oldAcc.ShippingPostalCode || acc.ShippingCountry !=oldAcc.ShippingCountry) {
                    acc.ShippingCity__c       = acc.ShippingCity;
                    acc.ShippingState__c      = acc.ShippingState;
                    acc.ShippingPostalCode__c = acc.ShippingPostalCode;
                    acc.ShippingCountry__c    = acc.ShippingCountry;
                } else {
                    acc.ShippingCity       = acc.ShippingCity__c;
                    acc.ShippingState      = acc.ShippingState__c;
                    acc.ShippingPostalCode = acc.ShippingPostalCode__c;
                    acc.ShippingCountry    = acc.ShippingCountry__c;
                }
            }
        }
    }
    
    // MM: Splits out the billing street address into seperate address lines fields
    public static void splitBillingStreetLines(Account acc) {
        if(acc.BillingStreet!=null && acc.BillingStreet.trim().length()>0) {
            string[] splitFields = splitMultiLine(acc.BillingStreet);
            if(splitFields.size()>0 && splitFields[0]!=null) acc.Billing_Street_Address_Line_1__c = splitFields[0].trim();
            if(splitFields.size()>1 && splitFields[1]!=null) acc.Billing_Street_Address_Line_2__c = splitFields[1].trim();
            if(splitFields.size()>2 && splitFields[2]!=null) acc.Billing_Street_Address_Line_3__c = splitFields[2].trim();
            if(splitFields.size()>3 && splitFields[3]!=null) acc.Billing_Street_Address_Line_4__c = splitFields[3].trim();
            if(splitFields.size()==0) {acc.Billing_Street_Address_Line_4__c=''; acc.Billing_Street_Address_Line_3__c=''; acc.Billing_Street_Address_Line_2__c=''; acc.Billing_Street_Address_Line_1__c='';}
            if(splitFields.size()==1) {acc.Billing_Street_Address_Line_4__c=''; acc.Billing_Street_Address_Line_3__c=''; acc.Billing_Street_Address_Line_2__c='';}
            if(splitFields.size()==2) {acc.Billing_Street_Address_Line_4__c=''; acc.Billing_Street_Address_Line_3__c='';}
            if(splitFields.size()==3) {acc.Billing_Street_Address_Line_4__c='';}
        }
    }
    
    // MM: Splits out the shipping street address into seperate address lines fields
    public static void splitShippingStreetLines(Account acc) {
        if(acc.ShippingStreet!=null && acc.ShippingStreet.trim().length()>0) {
            string[] splitFields = splitMultiLine(acc.ShippingStreet);
            if(splitFields.size()>0 && splitFields[0]!=null) acc.Shipping_Street_Address_Line_1__c = splitFields[0].trim();
            if(splitFields.size()>1 && splitFields[1]!=null) acc.Shipping_Street_Address_Line_2__c = splitFields[1].trim();
            if(splitFields.size()>2 && splitFields[2]!=null) acc.Shipping_Street_Address_Line_3__c = splitFields[2].trim();
            if(splitFields.size()>3 && splitFields[3]!=null) acc.Shipping_Street_Address_Line_4__c = splitFields[3].trim();
            if(splitFields.size()==0) {acc.Shipping_Street_Address_Line_4__c=''; acc.Shipping_Street_Address_Line_3__c=''; acc.Shipping_Street_Address_Line_2__c=''; acc.Shipping_Street_Address_Line_1__c='';}
            if(splitFields.size()==1) {acc.Shipping_Street_Address_Line_4__c=''; acc.Shipping_Street_Address_Line_3__c=''; acc.Shipping_Street_Address_Line_2__c='';}
            if(splitFields.size()==2) {acc.Shipping_Street_Address_Line_4__c=''; acc.Shipping_Street_Address_Line_3__c='';}
            if(splitFields.size()==3) {acc.Shipping_Street_Address_Line_4__c='';}
        }
    }
    
    public static string[] splitMultiLine(string inVal) {
        // Standardise the newline characters to \n then split based on \n
        string[] outVal;
        inVal = inVal.replace('\r\n','\n');
        inVal = inVal.replace('\n\r','\n');
        inVal = inVal.replace('\r','\n');
        outVal = inVal.split('\n');
        return outVal;
    }
}