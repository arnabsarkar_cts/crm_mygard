global class processCompaniesByPSC implements Database.Batchable<sObject> {

    public class processCompaniesByPSCException extends Exception{}
    
    global String query;
    
    private static DQ__c OriginalDQSettings{
        get{
            if(OriginalDQSettings==null){
                OriginalDQSettings = DataQualitySupport.GetSetting(UserInfo.getUserId());
                if(OriginalDQSettings.Id ==null){insert OriginalDQSettings;}
                return OriginalDQSettings;
            }else{
                return OriginalDQSettings;
            }
        }
    }
    
    private static DQ__c DQSettings{
        get{
            if(DQSettings==null){
                DQSettings = OriginalDQSettings.clone(true, true, true, true);
                return DQSettings;
            }else{
                return DQSettings;
            }
        }
        set;
    }
    
    private static MDM_Settings__c MDMSettings {
        get{
            if(MDMSettings==null){
                MDMSettings = OriginalMDMSettings.clone(true, true, true, true);
                return MDMSettings;
            }else{
                return MDMSettings;
            }
        }
        set;
    }
    
    private static MDM_Settings__c OriginalMDMSettings{
        get{
            if(OriginalMDMSettings==null){
                OriginalMDMSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
                if(OriginalMDMSettings.Id == null){insert OriginalMDMSettings;}
                return OriginalMDMSettings;
            }else{
                return OriginalMDMSettings;
            }
        }
        set;
    }    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    //MG cDecisions 28/11/2013 split out the main process into a separate method
    //to make it easier to invoke.
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        processAccounts(scope);
    }
    
    global void processAccounts(List<sObject> scope){
        try {
            if (scope.size() > 0) {
                // these variables control whether the flag on the parent account should be set or not
                boolean bPIFlagSet;
                boolean bMEFlagSet;
                boolean bOnRiskSet;
                boolean bUpdateAcc;
                boolean bPSCExists;
                //MG 28/11/2013 Issue01028 Adding Client and Broker On Risk flags
                boolean bClientOnRiskSet;
                boolean bBrokerOnRiskSet;
                boolean bBroker;
                
                // Accounts to update with P&I PSC
                List<Account> lstAcc = new List<Account>();
                Account acc;
                
                for (sObject listacc: scope) {
                    acc = (Account)listacc;
                    // set PI and ME variables to false
                    bPIFlagSet = false;
                    bMEFlagSet = false;
                    bOnRiskSet = false;
                    bUpdateAcc = false;
                    bPSCExists = false;
                    bBroker = false;
                    bClientOnRiskSet = false;
                    bBrokerOnRiskSet = false;
                                    
                    for (Admin_System_Company__c psc: acc.Admin_System_Companies__r) {
                        bPSCExists = true;    // set the PSC Exists flag as a PSC exists against this Salesforce account
                        if (psc.Type__c == 'Client' && psc.On_Risk__c == true && psc.Source_System__c == 'GIC')   { bPIFlagSet = true; }  // if GIC then set P&I flag on parent company
                        if (psc.Type__c == 'Client' && psc.On_Risk__c == true && psc.Source_System__c == 'Paris') { bMEFlagSet = true; }  // if Paris then set M&E flag on parent company
                        if (psc.On_Risk__c == true) { bOnRiskSet = true; }  // if PSC On Risk then set On Risk flag on parent company 
                        if (psc.Type__c == 'Client' && psc.On_Risk__c) { bClientOnRiskSet = true;}
                        if (psc.Type__c == 'Broker' && psc.On_Risk__c) { bBrokerOnRiskSet = true;}
                        if (psc.Type__c == 'Broker') bBroker=true;
                        // the following is useful if you need to debug or test...
                        System.debug('###### - Acc Id:'+acc.Id);
                        System.debug('###### - PI Flag:'+bPIFlagSet); 
                        System.debug('###### - ME Flag:'+bMEFlagSet);
                        System.debug('###### - On Risk Flag:'+bOnRiskSet);
                        System.debug('###### - Client On Risk Flag:'+bClientOnRiskSet);
                        System.debug('###### - Broker On Risk Flag:'+bBrokerOnRiskSet);
                        //MG we don't want to reflect the PSC types onto the account roles
                        //bUpdateAcc = setAccRoleFields(acc, psc);
                    }
                    // set relevant Salesforce company flags if not already checked
                    System.debug('##### - pre acc.P_I_Member_Flag__c:'+acc.P_I_Member_Flag__c);
                    System.debug('##### - bPIFlagSet:'+bPIFlagSet);
                    System.debug('##### - pre acc.M_E_Client_Flag__c:'+acc.M_E_Client_Flag__c);
                    System.debug('##### - bMEFlagSet:'+bMEFlagSet);
                    if (bPIFlagSet) { if (acc.P_I_Member_Flag__c == false || acc.P_I_Member_Flag__c == null) { acc.P_I_Member_Flag__c = true; bUpdateAcc = true; }}
                    if (bMEFlagSet) { if (acc.M_E_Client_Flag__c == false || acc.M_E_Client_Flag__c == null) { acc.M_E_Client_Flag__c = true; bUpdateAcc = true; }}
                    System.debug('##### - post acc.P_I_Member_Flag__c:'+acc.P_I_Member_Flag__c);
                    System.debug('##### - post acc.M_E_Client_Flag__c:'+acc.M_E_Client_Flag__c);
                    //MG cDecisions don't set the on risk flag anymore use client or broker on risk flags instead.
                    //if (bOnRiskSet) { if (acc.On_Risk__c == false || acc.On_Risk__c == null)                 { acc.On_Risk__c = true; bUpdateAcc = true; }}
                    if (bClientOnRiskSet){ if (acc.Client_On_Risk_Flag__c == false || acc.Client_On_Risk_Flag__c == null) { acc.Client_On_Risk_Flag__c = true;bUpdateAcc = true;}}
                    if (bBrokerOnRiskSet){ if (acc.Broker_On_Risk_Flag__c == false || acc.Broker_On_Risk_Flag__c == null) { acc.Broker_On_Risk_Flag__c = true;bUpdateAcc = true;}}
                    // MM cDecisions: ensure P&I and M&E flags are not set for Brokers
                    if (bBroker && !bPIFlagSet && !bMEFlagSet && 
                        (acc.P_I_Member_Flag__c == true || acc.M_E_Client_Flag__c == true)) {
                        acc.P_I_Member_Flag__c = false; acc.M_E_Client_Flag__c = false; bUpdateAcc = true;
                    }
                    
                    
                    // if not GIC/Paris and company flag currently set then clear it
                    if (bPIFlagSet == false) { if (acc.P_I_Member_Flag__c == true) { acc.P_I_Member_Flag__c = false; bUpdateAcc = true; }}
                    if (bMEFlagSet == false) { if (acc.M_E_Client_Flag__c == true) { acc.M_E_Client_Flag__c = false; bUpdateAcc = true; }}
                    //MG cDecisions don't set the on risk flag anymore use client or broker on risk flags instead.
                    //if (bOnRiskSet == false) { if (acc.On_Risk__c == true)         { acc.On_Risk__c = false; bUpdateAcc = true; }}
                    System.Debug('##### acc = ' + acc);
                    if (bClientOnRiskSet == false){ if (acc.Client_On_Risk_Flag__c == true) { acc.Client_On_Risk_Flag__c = false;bUpdateAcc = true;}}
                    if (bBrokerOnRiskSet == false){ if (acc.Broker_On_Risk_Flag__c == true) { acc.Broker_On_Risk_Flag__c = false;bUpdateAcc = true;}}
                    
                    // if the account is a Client then only update the PSC Exists flag if the area manager and market area code are not empty (as they're required but not always populated in the data)
                    //MM cDecisions 02/10/14 added check for non-null Company_Role__c
                    if (bPSCExists == true && !String.isEmpty(acc.Company_Role__c)){
                    //MG cDecisions 28/11/2013 changed from looking at record type to Company_Role__c field
                        Set<String> corpRoles = new Set<String>();
                        corpRoles.addAll( new List<String>(acc.Company_Role__c.split(';')));
                         
                        if ((corpRoles != null && corpRoles.contains('Client') && acc.Area_Manager__c != null && acc.Market_Area__c != null) || (corpRoles != null && !corpRoles.contains('Client')))
                            acc.PSC_Exists__c = true; bUpdateAcc = true;
                    }                
                    
                    
                    if (bUpdateAcc)
                        lstAcc.add(acc);
                }
                if (lstAcc.Size() > 0){             
                    updateAccounts(lstAcc);
                } 
            }   
        } catch (Exception ex) {
            throw ex;
        } finally {
            update OriginalMDMSettings;
            setValidation(OriginalDQSettings.Enable_Validation__c); 
        }
    }


    //Returns true if the account was updated false otherwise. the account is modified by reference.
    public Boolean setAccRoleFields(Account acc, Admin_System_Company__c psc){
        //MG cDecisions 4/12/2013 amending PSC/ESP functionality for MDM
        //Create a set from the company roles and then add esp to that set
        //convert the set back into a string
        //mapping roles based on CLAIMS-PI-FDD- 5699079 - SRS Document 19th September.docx
        boolean updateMade= false;
        String newRole = getRole(psc.Type__c);
        if(acc.Company_Role__c!=null && newRole!=null){
            Set <String> corpRoles = new Set<String>(new List<String>(acc.Company_Role__c.split(';')));         
            updateMade = corpRoles.add(newRole) || updateMade;
            acc.Company_Role__c = String.join(new List<String>(corpRoles),';');
        }else if(acc.Company_Role__c == null && newRole!=null){
            acc.Company_Role__c = newRole;
            updateMade = true || updateMade;
        }
        System.Debug('*** Account Roles for Id ' + acc.Id +' set to : ' + acc.Company_Role__c);
        //Create a set from the existing sub roles and add the new role to it
        //convert the set of sub roles back into a string.
        String newSubRole = getSubRole(psc.Company_Sub_Type__c, getPrefix(newRole));
        
        if(acc.Sub_Roles__c != null && newSubRole!=null){
            Set <String> subRoles = new Set<String>(new List<String>(acc.Sub_Roles__c.split(';')));
            //find the right sub role for the given ESP role            
            updateMade = subRoles.add(newSubRole) || updateMade;
            acc.Sub_Roles__c    = String.join(new List<String>(subRoles),';');
        }else if(acc.Sub_Roles__c==null && newSubRole!=null){
            acc.Sub_Roles__c    = newSubRole;
            updateMade = true || updateMade;
        }
        System.Debug('*** Account Sub Roles for Id ' + acc.Id +' set to : ' + acc.Sub_Roles__c);
        //Auto approving this change
       // if(updateMade){
        //  acc.role_change_approved__c =true;
            
        //}
        return updateMade;      
    }    
    public static string getRole(String corpRole){
        if(corpRole!=null){
            if(corpRole.equalsIgnoreCase('Broker')){
                return 'Broker';
            }else if(corpRole.equalsIgnoreCase('Client')){
                return 'Client';
            }else if(corpRole.equalsIgnoreCase('Correspondent')){
                return 'Correspondent';
            }else if(corpRole.equalsIgnoreCase('ESP')){
                return 'External Service Provider';
            }else if(corpRole.equalsIgnoreCase('Other')){
                return 'Other';
            }else if(corpRole.equalsIgnoreCase('Prospect')){
                return 'Client';
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    
    public static string getPrefix(String corpRole){
        if(corpRole!=null){
            if(corpRole.equalsIgnoreCase('Broker')){
                return 'Broker - ';
            }else if(corpRole.equalsIgnoreCase('Client')){
                return '';
            }else if(corpRole.equalsIgnoreCase('Correspondent')){
                return '';
            }else if(corpRole.equalsIgnoreCase('External Service Provider')){
                return 'ESP - ';
            }else if(corpRole.equalsIgnoreCase('Other')){
                return 'Other - ';
            }else if(corpRole.equalsIgnoreCase('Prospect')){
                return '';
            }else if(corpRole.equalsIgnoreCase('Bank')){
                return 'Bank - ';
            }else if(corpRole.equalsIgnoreCase('Insurance Company')){
                return 'Insurance Company - ';
            }else if(corpRole.equalsIgnoreCase('Investments')){
                return 'Investments - ';
            }else{
                return null;
            }
        }else{
            return null;
        }       
    }
    
    public static string getSubRole(String espRole, String prefix){
        //based on CLAIMS-PI-FDD- 5699079 - SRS Document 19th September.docx return the correct
        //sub role string
        if(espRole!=null){
            if(espRole.equalsIgnoreCase('Surveyor')){
                return prefix  + 'P&I Surveyor';
            }else if(espRole.equalsIgnoreCase('Broker')){
                return 'Broker - Broker';
            }else{
                return prefix + espRole;
            }
        }else{
            return null;
        }
    }
    
    private static void setValidation(boolean enable){
        //disable validation during the execution of this operation 
        //only if we are allowed
        System.Debug('setValidation DQSettings.Enable_Validation_for_Processing_PSC__c = ' + DQSettings.Enable_Validation_for_Processing_PSC__c);
        if(!DQSettings.Enable_Validation_for_Processing_PSC__c){    
            DQSettings.Enable_Validation__c = enable;       
            System.Debug('setValidation - updating settings');
            update DQSettings;
        }
    }     
        
    private void updateAccounts(List<Account> lstAcc){
        if (lstAcc.Size() > 0){
            setValidation(false);
            //Temporarily disable MDM Triggers
            boolean MDMSettingsChanged = false;
            if(MDMSettings.Account_Trigger_Enabled__c){
                MDMSettings.Account_Trigger_Enabled__c = false;
                update MDMSettings;
                MDMSettingsChanged=true;
            }           
            System.debug('Accounts to update ' + lstAcc);
            Database.SaveResult[] srList = Database.update(lstAcc, false);
            
            System.debug('Updated accounts. ' + srList);
            
            //Re-enable MDM Triggers                
            if(MDMSettingsChanged){
                MDMSettings=OriginalMDMSettings;
                update MDMSettings;
            }   
            setValidation(OriginalDQSettings.Enable_Validation__c);     
            // Iterate through each returned result
            //Use an iterator i so we know which source id caused the problem
            //We assume that the results are in the same order as the input list.
            for(Integer i=0;i<srList.size();i++){
                Database.SaveResult sr = srList[i];
                Account acc = new Account();
                if(i<lstAcc.size()){ acc= lstAcc[i];}
                
            //for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully updated account. Account ID: ' + sr.getId());
                }
                else {
                    System.debug('Error occured for account. Account ID: ' + acc.Id);                   
                    // Operation failed, so get all errors 
                           
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug(' Account fields that affected this error: ' + err.getFields());
                        processCompaniesByPSCException ex = new processCompaniesByPSCException('Exception for Account Id ' + acc.Id + ' : '+ err.getStatusCode() + ': ' + err.getMessage() + ' Account fields that affected this error: ' + err.getFields());
                        Logger.LogException('processCompaniesByPSC.processAccounts', ex);
                    }
                }
            }
        }       
    }   

    global void finish(Database.BatchableContext BC) {
        // do nothing
    }
    
    public static testmethod void runTest() {
        setValidation(false);
        Market_Area__c marketarea = new Market_Area__c(Name='testmarketarea',Market_Area_Code__c='testmktcode');
        insert(marketarea);
        Account accTest = new Account(Name='Test1',P_I_Member_Flag__c = true, M_E_Client_Flag__c = false, Client_On_Risk_Flag__c= false, Broker_On_Risk_Flag__c = false, Area_Manager__c=UserInfo.getUserId(),Market_Area__c=marketarea.ID,RecordTypeId=[Select Id from RecordType where Name = 'Client' LIMIT 1].Id);
        insert(accTest);
        string idAcc = accTest.Id;
        system.Assert(idAcc <> null);
        
        Admin_System_Company__c pscTest1 = new Admin_System_Company__c(Salesforce_Company__c=idAcc,On_Risk__c = true,Type__c='Client', Source_System__c = 'GIC');
        Admin_System_Company__c pscTest2 = new Admin_System_Company__c(Salesforce_Company__c=idAcc,On_Risk__c = true,Type__c='Client', Source_System__c = 'Paris');
        Admin_System_Company__c pscTest3 = new Admin_System_Company__c(Salesforce_Company__c=idAcc,On_Risk__c = true,Type__c='Broker', Source_System__c = 'Paris');
        insert(pscTest1);
        insert(pscTest2);
        insert(pscTest3);
        system.Assert(pscTest1.Id <> null);
        system.Assert(pscTest2.Id <> null);
        setValidation(true);
        Test.startTest();
            processCompaniesByPSC pc = new processCompaniesByPSC();
            pc.Query = 'SELECT Name, Id, P_I_Member_Flag__c, M_E_Client_Flag__c, PSC_Exists__c, RecordType.Name, Area_Manager__c, Market_Area__c, Market_Area_Code__c,'
                  +'On_Risk__c, Client_On_Risk_Flag__c, Broker_On_Risk_Flag__c, Company_Role__c, (SELECT On_Risk__c, Source_System__c, Type__c from Admin_System_Companies__r) '
                  +'from Account where Id = \'' + idAcc + '\'';
            system.Debug('######'+pc.Query);

            Database.executeBatch(pc,20);
        Test.stopTest();

       // system.Assert([select M_E_Client_Flag__c from Account where Id = :idAcc LIMIT 1].M_E_Client_Flag__c == true);
        system.Assert([select P_I_Member_Flag__c from Account where Id = :idAcc LIMIT 1].P_I_Member_Flag__c == true);
        //system.Assert([select On_Risk__c from Account where Id = :idAcc LIMIT 1].On_Risk__c == true);
        //system.assert([Select PSC_Exists__c from Account where Id = :idAcc LIMIT 1].PSC_Exists__c == true);
        
        //system.assert([Select Client_On_Risk_Flag__c from Account where Id = :idAcc LIMIT 1].Client_On_Risk_Flag__c == true);
       // system.assert([Select Broker_On_Risk_Flag__c from Account where Id = :idAcc LIMIT 1].Broker_On_Risk_Flag__c == true);

    }
    

    public static testmethod void runNegativeTest() {
        setValidation(false);
        Market_Area__c marketarea = new Market_Area__c(Name='testmarketarea',Market_Area_Code__c='testmktcode');
        insert(marketarea);
        Account accTest = new Account(Name='Test1',P_I_Member_Flag__c = true, M_E_Client_Flag__c = true, Client_On_Risk_Flag__c= true, Broker_On_Risk_Flag__c = false, Area_Manager__c=UserInfo.getUserId(),Market_Area__c=marketarea.ID,RecordTypeId=[Select Id from RecordType where Name = 'Client' LIMIT 1].Id);
        insert(accTest);
        string idAcc = accTest.Id;
        system.Assert(idAcc <> null);
        
        Admin_System_Company__c pscTest1 = new Admin_System_Company__c(Salesforce_Company__c=idAcc,On_Risk__c = false, Type__c = 'Client', Source_System__c = 'GIC');
        Admin_System_Company__c pscTest2 = new Admin_System_Company__c(Salesforce_Company__c=idAcc,On_Risk__c = false, Type__c = 'Client', Source_System__c = 'Paris');
        Admin_System_Company__c pscTest3 = new Admin_System_Company__c(Salesforce_Company__c=idAcc,On_Risk__c = false, Type__c = 'Broker', Source_System__c = 'Paris');
        insert(pscTest1);
        insert(pscTest2);
        insert(pscTest3);
        system.Assert(pscTest1.Id <> null);
        system.Assert(pscTest2.Id <> null);
        system.Assert(pscTest3.Id <> null);
        setValidation(true);
        Test.startTest();
            processCompaniesByPSC pc = new processCompaniesByPSC();
            pc.Query = 'SELECT Name, Id, P_I_Member_Flag__c, M_E_Client_Flag__c, PSC_Exists__c, RecordType.Name, Area_Manager__c, Market_Area__c, Market_Area_Code__c,'
                  +'On_Risk__c, Client_On_Risk_Flag__c, Broker_On_Risk_Flag__c, Company_Role__c, (SELECT On_Risk__c, Source_System__c, Type__c from Admin_System_Companies__r) '
                  +'from Account where Id = \'' + idAcc + '\'';
            system.Debug('######'+pc.Query);

            Database.executeBatch(pc,20);
        Test.stopTest();
        Account testAcc = [select Id,  M_E_Client_Flag__c, P_I_Member_Flag__c, On_Risk__c, PSC_Exists__c, Client_On_Risk_Flag__c, Broker_On_Risk_Flag__c from Account where Id = :idAcc LIMIT 1];
        System.Debug('###### testAcc ' + testAcc);
        system.Assert([select M_E_Client_Flag__c from Account where Id = :idAcc LIMIT 1].M_E_Client_Flag__c == false);
        system.Assert([select P_I_Member_Flag__c from Account where Id = :idAcc LIMIT 1].P_I_Member_Flag__c == false);
        system.Assert([select On_Risk__c from Account where Id = :idAcc LIMIT 1].On_Risk__c == false);
        system.assert([Select PSC_Exists__c from Account where Id = :idAcc LIMIT 1].PSC_Exists__c == true);
        
        system.assert([Select Client_On_Risk_Flag__c from Account where Id = :idAcc LIMIT 1].Client_On_Risk_Flag__c == false);
        system.assert([Select Broker_On_Risk_Flag__c from Account where Id = :idAcc LIMIT 1].Broker_On_Risk_Flag__c == false);

    }

}