public without sharing Class AutomatePostRefreshSteps
{   
    public static void updateContactEmails()
    {
        List<Contact> contactLst1stBatch = new List<Contact>();
        List<Contact> contactLst2ndBatch = new List<Contact>();
        List<Contact> contactLst3rdBatch = new List<Contact>();
        List<Contact> contactLst4thBatch = new List<Contact>();
        Integer i = 0;
        for(Contact contactIns : [Select id, Email,Email_1__c,Email_2__c,Email_3__c,Email_4__c,Email_5__c FROM Contact])
        {
            if(contactIns.Email != null || contactIns.Email != '')
                contactIns.Email = 'example'+contactIns.Email;
            if(contactIns.Email_1__c != null || contactIns.Email_1__c != '')
                contactIns.Email_1__c = 'example'+contactIns.Email_1__c;
            if(contactIns.Email_2__c != null || contactIns.Email_2__c != '')
                contactIns.Email_2__c = 'example'+contactIns.Email_2__c;
            if(contactIns.Email_3__c != null || contactIns.Email_3__c != '')
                contactIns.Email_3__c = 'example'+contactIns.Email_3__c;
            if(contactIns.Email_4__c != null || contactIns.Email_4__c != '')
                contactIns.Email_4__c = 'example'+contactIns.Email_4__c;
            if(contactIns.Email_5__c != null || contactIns.Email_5__c != '')
                contactIns.Email_5__c = 'example'+contactIns.Email_5__c;
            if(i<10000)
                contactLst1stBatch.add(contactIns);
            if(i> = 10000 && i<20000)
                contactLst2ndBatch.add(contactIns);
            if(i> = 20000 && i<30000)
                contactLst3rdBatch.add(contactIns);
            if(i> = 30000 && i<40000)
                contactLst4thBatch.add(contactIns);
        }
        if(!Test.isRunningTest())
        {
            if(contactLst1stBatch.size() > 0)
                update contactLst1stBatch;
            if(contactLst2ndBatch.size() > 0)
                update contactLst2ndBatch;
            if(contactLst3rdBatch.size() > 0)
                update contactLst3rdBatch;
            if(contactLst4thBatch.size() > 0)
                update contactLst4thBatch;
        }
        contactLst1stBatch.clear();
        contactLst2ndBatch.clear();
        contactLst3rdBatch.clear();
        contactLst4thBatch.clear();
    }
    
    public static void updateAccountEmails()
    {
        List<Account> accountLst1stBatch = new List<Account>();
        List<Account> accountLst2ndBatch = new List<Account>();
        List<Account> accountLst3rdBatch = new List<Account>();
        Integer i = 0;
        for(Account accountIns : [Select id, Email_1__c,Email_Address_2__c FROM Account])
        {
            if(accountIns.Email_Address_2__c != null || accountIns.Email_Address_2__c != '')
                accountIns.Email_Address_2__c = 'example'+accountIns.Email_Address_2__c;
            if(accountIns.Email_1__c != null || accountIns.Email_1__c != '')
                accountIns.Email_1__c = 'example'+accountIns.Email_1__c;
            
            if(i<10000)
                accountLst1stBatch.add(accountIns);
            if(i> = 10000 && i<20000)
                accountLst2ndBatch.add(accountIns);
            if(i> = 20000 && i<30000)
                accountLst3rdBatch.add(accountIns);
        }
        if(!Test.isRunningTest())
        {
            if(accountLst1stBatch.size() > 0)
                update accountLst1stBatch;
            if(accountLst2ndBatch.size() > 0)
                update accountLst2ndBatch;
            if(accountLst3rdBatch.size() > 0)
                update accountLst3rdBatch;   
        }
        accountLst1stBatch.clear();
        accountLst2ndBatch.clear();
        accountLst3rdBatch.clear();
    }
    public static void updateGardContactsEmails()
    {
        List<Gard_Contacts__c> grdContactLst1stBatch = new List<Gard_Contacts__c>();
        Integer i = 0;
        for(Gard_Contacts__c grdContactIns : [SELECT Email__c FROM Gard_Contacts__c])
        {
            if(grdContactIns.Email__c != null || grdContactIns.Email__c != '')
                grdContactIns.Email__c = 'example'+grdContactIns.Email__c;
            if(i<10000)
                grdContactLst1stBatch.add(grdContactIns);
        }
        if(!Test.isRunningTest() && grdContactLst1stBatch.size() > 0)
            update grdContactLst1stBatch;
        grdContactLst1stBatch.clear();
    }
    
    // update all case emails which don't have claim record type
    public static void updateCaseEmails()
    {
        List<RecordType> recordTypeLst;
        List<Case> caseLst1stBatch = new List<Case>();
        String claimRecordTypeId;
        Integer i = 0;
        
        recordTypeLst = [SELECT Id FROM RecordType WHERE Name = 'claim'];
        if(recordTypeLst != null && recordTypeLst.size() >0)
            claimRecordTypeId = recordTypeLst[0].id;
        
        for(Case caseIns : [SELECT suppliedEmail,Claim_Submitter_Email__c FROM Case WHERE RecordTypeId !=: claimRecordTypeId])
        {
            if(caseIns.suppliedEmail != null || caseIns.suppliedEmail != '')
                caseIns.suppliedEmail = 'example'+caseIns.suppliedEmail;
            if(caseIns.Claim_Submitter_Email__c != null || caseIns.Claim_Submitter_Email__c != '')
                caseIns.Claim_Submitter_Email__c = 'example'+caseIns.Claim_Submitter_Email__c;
            if(i<10000)
                caseLst1stBatch.add(caseIns);
        }
        if(!Test.isRunningTest() && caseLst1stBatch.size() > 0)
            update caseLst1stBatch;
            
        caseLst1stBatch.clear();
    }
    
    public static void updateCustomSettings()
    {
        //Claim_Admin_Email__c claimAdminEmail = Claim_Admin_Email__c.getValues('1');
        List<Claim_Admin_Email__c> Claim_Admin_Email_Lst = new List<Claim_Admin_Email__c>();
        List<BlueCardTeam__c> blueCardTmLst = new List<BlueCardTeam__c>();
        List<GardContacts__c> gardContactLst = new List<GardContacts__c>();
        List<Opportunity_Email_Recipients__c>  oppEmailRecLst = new List<Opportunity_Email_Recipients__c>();
        list<Feedback_mail_id__c> Feedback_mail_id_Lst = new List<Feedback_mail_id__c>();
        List<MyGard_support_email_address__c> MyGrdSupportMailLst = new List<MyGard_support_email_address__c>();
        List<Contract_Review_Default_Claim_Email__c> contractRevwDefaultClaimMailLst = new List<Contract_Review_Default_Claim_Email__c>();
        List<BlueCard_webservice_endpoint__c> blueCrdViewDocWSEndPointLst = new List<BlueCard_webservice_endpoint__c>();
        List<Claim_webservice_Endpoint__c> claimWSEndPointLst = new List<Claim_webservice_Endpoint__c>();
        List<ContractReviewWebServices__c> ContractReviewWebServicesLst = new List<ContractReviewWebServices__c>();
        List<PortfolioReportsCtrl_for_Marine_endpoint__c> portFolioRepMarineLst = new List<PortfolioReportsCtrl_for_Marine_endpoint__c>();
        List<Search_webservice_endpoint__c> searchWsLst = new List<Search_webservice_endpoint__c>();
        List<MailtoUWR__c> mailToLst = new List<MailtoUWR__c>();
        List<HomeFeedbackRecipient__c> homeFeedBkRecLst = new List<HomeFeedbackRecipient__c>();
        //List<Gard_Administrators__c> gardAdminLst = new List<Gard_Administrators__c>();
        List<EmailSettings__c> emailSettingLst = new List<EmailSettings__c>();
        List<COFR_Request_Email_CS__c> COFR_Request_Email_CS_Lst = new List<COFR_Request_Email_CS__c>();
        List<Customer_feedback_Email_Recipient__c> customerFeedBkRecLst = new List<Customer_feedback_Email_Recipient__c>();
        List<CommonVariables__c> CommonVarLst = new List<CommonVariables__c>();
        List<Claim_Admin_Email__c> claimAdminLst = new List<Claim_Admin_Email__c>();
        List<MDMConfig__c> mdmConfigEndPointLst = new List<MDMConfig__c>();
        List<MDM_Settings__c> mdmSettingLst = new List<MDM_Settings__c>();
        
        for(Claim_Admin_Email__c obj :[SELECT Email_Address__c,Id,Name FROM Claim_Admin_Email__c])
        {
            obj.Email_Address__c = 'ownergard@gmail.com';
            Claim_Admin_Email_Lst.add(obj);
        }
        for(BlueCardTeam__c blueCardTm: [SELECT Id,Name,Value__c FROM BlueCardTeam__c])//= BlueCardTeam__c.getValues('BlueCardTeam');
        {
            blueCardTm.Value__c = 'ownergard@gmail.com';
            blueCardTmLst.add(blueCardTm);
        }
        for(GardContacts__c gardContact: [SELECT Email__c,Id,Name,Office_City__c,Phone__c FROM GardContacts__c])// GardContacts__c.getValues('DeactivatedUsers');
        {
            gardContact.Email__c = 'ownergard@gmail.com';
            gardContactLst.add(gardContact);
        }
        for(Opportunity_Email_Recipients__c oppEmailRec : [SELECT Default_Survey_To__c,Fleet_Assessment__c,Id,Management_Audit__c FROM Opportunity_Email_Recipients__c]) //= Opportunity_Email_Recipients__c.getInstance();
        {
            oppEmailRec.Fleet_Assessment__c = 'ownergard@gmail.com,gardoffshore@gmail.com';
            oppEmailRec.Management_Audit__c = 'ownergard@gmail.com';
            oppEmailRecLst.add(oppEmailRec);
        }
        for(Feedback_mail_id__c obj: [SELECT Email_address__c,Id,Name FROM Feedback_mail_id__c])
        {
            obj.Email_address__c = 'ownergard@gmail.com';
            Feedback_mail_id_Lst.add(obj);
        }
        /*Feedback_mail_id__c feedBkNotifMailCC = Feedback_mail_id__c.getValues('Feedback notification CC');
        Feedback_mail_id__c feedBkNoMailMyGrdBug = Feedback_mail_id__c.getValues('Feedback notification MyGard bugs');
        feedBkNotifMailCC.Email_address__c = 'ownergard@gmail.com';
        feedBkNoMailMyGrdBug.Email_address__c = 'ownergard@gmail.com';*/
        
        for(MyGard_support_email_address__c MyGrdSupportMail:[SELECT Email_address__c,Id,Name FROM MyGard_support_email_address__c]) //= MyGard_support_email_address__c.getValues('Support email address');
        {
            MyGrdSupportMail.Email_address__c = 'ownergard@gmail.com';
            MyGrdSupportMailLst.add(MyGrdSupportMail);
        }
        for(Contract_Review_Default_Claim_Email__c contractRevwDefaultClaimMail: [SELECT Email__c,Id,Name FROM Contract_Review_Default_Claim_Email__c]) //= Contract_Review_Default_Claim_Email__c.getValues('Claim@gard.no');
            contractRevwDefaultClaimMail.Email__c = 'ownergard@gmail.com';
        
        for(BlueCard_webservice_endpoint__c blueCrdViewDocWSEndPoint: [SELECT Endpoint__c,Id,Name FROM BlueCard_webservice_endpoint__c])// = BlueCard_webservice_endpoint__c.getValues('BlueCard_WS_endpoint');
        {
            blueCrdViewDocWSEndPoint.Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/Documents/DocumentFetcher/GetDocumentService_ep';
            blueCrdViewDocWSEndPointLst.add(blueCrdViewDocWSEndPoint);
        }
        for(Claim_webservice_Endpoint__c claimWSEndPoint:[SELECT Endpoint__c,Id,Name FROM Claim_webservice_Endpoint__c])// = Claim_webservice_Endpoint__c.getValues('Claims_SOA_Endpoint');
        {
            claimWSEndPoint.Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/ClaimHandling/ClaimsReceiver/ClaimReceiverMediator_ep';
            claimWSEndPointLst.add(claimWSEndPoint);
        }
        
        for(ContractReviewWebServices__c obj: [SELECT Endpoint__c,Id,Name FROM ContractReviewWebServices__c])
        {
            obj.Endpoint__c = obj.Endpoint__c + ' TestString';
            ContractReviewWebServicesLst.add(obj);
        }
        /*ContractReviewWebServices__c contractRevwWS = ContractReviewWebServices__c.getValues('CR_submission_from_MyGard');
        ContractReviewWebServices__c contractRevwWSOwnerChng = ContractReviewWebServices__c.getValues('Owner Change');
        contractRevwWS.Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/ContractReview/ContractReviewReceiverExternal/ContractReviewReceiver';
        contractRevwWSOwnerChng.Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/ContractReview/ContractReassignHandler/ReassignContract_ep';
        */
        for(PortfolioReportsCtrl_for_Marine_endpoint__c portFolioRepMarine: [SELECT Endpoint__c,Id,Name FROM PortfolioReportsCtrl_for_Marine_endpoint__c])// = PortfolioReportsCtrl_for_Marine_endpoint__c.getValues('endpoint');
        {
            portFolioRepMarine.Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/Reports/ReportsFetcher/ReportsService';
            portFolioRepMarineLst.add(portFolioRepMarine);
        }
        for(Search_webservice_endpoint__c searchWS: [SELECT Endpoint__c,Id,Name FROM Search_webservice_endpoint__c])// = Search_webservice_endpoint__c.getValues('Search_SOA_Endpoint');
        {
            searchWS.Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/Common/SearchGardNo/SearchRequest';
            searchWsLst.add(searchWS);
        }
        for(MailtoUWR__c mailTo: [SELECT Email__c,Id,Name FROM MailtoUWR__c])// = MailtoUWR__c.getValues('UWR');
        {
            mailTo.Email__c = 'ownergard@gmail.com';
            mailToLst.add(mailTo);
        }
        
        /*
        for(Organization_Wide_Email__c obj : [SELECT Id,Name,value__c FROM Organization_Wide_Email__c])
        {
            obj.value__c = '';
        }
        Organization_Wide_Email__c orgWideEmail = Organization_Wide_Email__c.getValues('claim@gard.no');
        Organization_Wide_Email__c orgWideEmailNoReply = Organization_Wide_Email__c.getValues('no-reply@gard.no');
        orgWideEmail.value__c = '';
        orgWideEmailNoReply.value__c = '';
        */
        for(HomeFeedbackRecipient__c homeFeedBkRec :[SELECT Email__c,Id,Name FROM HomeFeedbackRecipient__c])//= HomeFeedbackRecipient__c.getValues('Feedback DL');
        {
            homeFeedBkRec.Email__c = 'ownergard@gmail.com';
            homeFeedBkRecLst.add(homeFeedBkRec);
        }
        /*Sf-4015: start 
        for(Gard_Administrators__c gardAdmin:[SELECT Email_Address__c,Id,Name,Name__c FROM Gard_Administrators__c])// = Gard_Administrators__c.getvalues('1');
        {
            gardAdmin.Email_Address__c = 'ownergard@gmail.com';
            gardAdminLst.add(gardAdmin);
        }
		Sf-4015: end*/
        for(EmailSettings__c emailSetting:[SELECT Attachment_BCC_Service__c,Id,Name FROM EmailSettings__c])// = EmailSettings__c.getValues('Quote');
        {
            emailSetting.Attachment_BCC_Service__c = 'ownergard@gmail.com';
            emailSettingLst.add(emailSetting);
        }
        for(COFR_Request_Email_CS__c COFRReq:[SELECT Id,Name,Value__c FROM COFR_Request_Email_CS__c]) // = COFR_Request_Email_CS__c.getValues('Notification_Email_ID');
        {
            COFRReq.Value__c = 'ownergard@gmail.com';
            COFR_Request_Email_CS_Lst.add(COFRReq);
        }
        for(Customer_feedback_Email_Recipient__c customerFeedBkRec: [SELECT Complaint__c,Id,Name FROM Customer_feedback_Email_Recipient__c])// = Customer_feedback_Email_Recipient__c.getInstance();
        {
            customerFeedBkRec.Complaint__c = 'ownergard@gmail.com';
            customerFeedBkRecLst.add(customerFeedBkRec);
        }
        
        for(CommonVariables__c CommonVar : [SELECT Id,Name,Value__c FROM CommonVariables__c])// = CommonVariables__c.getValues('DefaultProfilePic');
        {
            //CommonVariables__c CommonVar = CommonVariables__c.getValues('ReplyToMailId');
            if(CommonVar.Name == 'DefaultProfilePic')
                CommonVar.Value__c = '/mygard/profilephoto/005/T';
            if(CommonVar.Name == 'ReplyToMailId')
                CommonVar.Value__c = 'no-reply@gard.no';
            CommonVarLst.add(CommonVar);
        }
        //CommonVarDefaultPic.Value__c = '/mygard/profilephoto/005/T';
        //CommonVar.Value__c = 'no-reply@gard.no';
        
        for(Claim_Admin_Email__c claimAdmin: [SELECT Email_Address__c,Id,Name FROM Claim_Admin_Email__c])// = Claim_Admin_Email__c.getValues('1');
        {
            claimAdmin.Email_Address__c = 'claimtest@gard.no';
            claimAdminLst.add(claimAdmin);
        }
        for(MDMConfig__c mdmConfigEndPoint:[SELECT Endpoint__c,Id,Name FROM MDMConfig__c])// = MDMConfig__c.getInstance();
        {
            mdmConfigEndPoint.Endpoint__c = 'https://soa-test.gard.no/MDM/Notification';
            mdmConfigEndPointLst.add(mdmConfigEndPoint);
        }
        for(MDM_Settings__c mdmSettings: [SELECT Id,Name,Web_Services_Enabled__c FROM MDM_Settings__c])// = MDM_Settings__c.getInstance();
        {
            mdmSettings.Web_Services_Enabled__c = false;
            mdmSettingLst.add(mdmSettings);
        }
       /* List<QueueSobject> queLst = new List<QueueSobject>();
        for(QueueSobject que :[SELECT id, QueueId, queue.email FROM QueueSobject]) // where queue.name like 'MyGard_Gard_Administrator' and SobjectType = 'Case'
        {
            if(que.queue.email != null || que.queue.email != '')
            {
                que.queue.email = 'ownergard@gmail.com';
                queLst.add(que);
            }
        }*/
        
        System.debug('Claim_Admin_Email_Lst '+Claim_Admin_Email_Lst);
        System.debug('blueCardTmLst '+blueCardTmLst);
        System.debug('gardContactLst '+gardContactLst);
        System.debug('oppEmailRecLst '+oppEmailRecLst);
        System.debug('Feedback_mail_id_Lst '+Feedback_mail_id_Lst);
        System.debug('MyGrdSupportMailLst '+MyGrdSupportMailLst);
        System.debug('contractRevwDefaultClaimMailLst '+contractRevwDefaultClaimMailLst);
        System.debug('blueCrdViewDocWSEndPointLst '+blueCrdViewDocWSEndPointLst);
        System.debug('claimWSEndPointLst '+claimWSEndPointLst);
        System.debug('ContractReviewWebServicesLst '+ContractReviewWebServicesLst);
        System.debug('portFolioRepMarineLst '+portFolioRepMarineLst);
        System.debug('searchWsLst '+searchWsLst);
        System.debug('mailToLst '+mailToLst);
        System.debug('homeFeedBkRecLst '+homeFeedBkRecLst);
        //System.debug('gardAdminLst '+gardAdminLst);
        System.debug('emailSettingLst '+emailSettingLst);
        System.debug('COFR_Request_Email_CS_Lst '+COFR_Request_Email_CS_Lst);
        System.debug('customerFeedBkRecLst '+customerFeedBkRecLst);
        System.debug('CommonVarLst '+CommonVarLst);
        System.debug('claimAdminLst '+claimAdminLst);
        System.debug('mdmConfigEndPointLst '+mdmConfigEndPointLst);
        System.debug('mdmSettingLst '+mdmSettingLst);
        
        
        if(!Test.isRunningTest())
        {
            update Claim_Admin_Email_Lst;
            update blueCardTmLst;
            update gardContactLst;
            update oppEmailRecLst;
            update Feedback_mail_id_Lst;
            //update feedBkNoMailMyGrdBug;
            update MyGrdSupportMailLst;
            update contractRevwDefaultClaimMailLst;
            update blueCrdViewDocWSEndPointLst;
            update claimWSEndPointLst;
            update ContractReviewWebServicesLst;
            update portFolioRepMarineLst;
            update searchWsLst;
            update mailToLst;
            update homeFeedBkRecLst;
            //update gardAdminLst;
            update emailSettingLst;
            update COFR_Request_Email_CS_Lst;
            update customerFeedBkRecLst;
            update CommonVarLst;
            update claimAdminLst;
            update mdmConfigEndPointLst;
            update mdmSettingLst;
            
            System.debug('Claim_Admin_Email_Lst '+Claim_Admin_Email_Lst);
            System.debug('blueCardTmLst '+blueCardTmLst);
            System.debug('gardContactLst '+gardContactLst);
            System.debug('oppEmailRecLst '+oppEmailRecLst);
            System.debug('Feedback_mail_id_Lst '+Feedback_mail_id_Lst);
            System.debug('MyGrdSupportMailLst '+MyGrdSupportMailLst);
            System.debug('contractRevwDefaultClaimMailLst '+contractRevwDefaultClaimMailLst);
            System.debug('blueCrdViewDocWSEndPointLst '+blueCrdViewDocWSEndPointLst);
            System.debug('claimWSEndPointLst '+claimWSEndPointLst);
            System.debug('ContractReviewWebServicesLst '+ContractReviewWebServicesLst);
            System.debug('portFolioRepMarineLst '+portFolioRepMarineLst);
            System.debug('searchWsLst '+searchWsLst);
            System.debug('mailToLst '+mailToLst);
            System.debug('homeFeedBkRecLst '+homeFeedBkRecLst);
            //System.debug('gardAdminLst '+gardAdminLst);
            System.debug('emailSettingLst '+emailSettingLst);
            System.debug('COFR_Request_Email_CS_Lst '+COFR_Request_Email_CS_Lst);
            System.debug('customerFeedBkRecLst '+customerFeedBkRecLst);
            System.debug('CommonVarLst '+CommonVarLst);
            System.debug('claimAdminLst '+claimAdminLst);
            System.debug('mdmConfigEndPointLst '+mdmConfigEndPointLst);
            System.debug('mdmSettingLst '+mdmSettingLst);
            //if(queLst.size() >0)
                //update queLst;
        }
    }
}