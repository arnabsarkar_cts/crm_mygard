public with sharing class MDMSettingSupport {
	private static MDM_Settings__c testconfig = null;

	public static MDM_Settings__c GetSetting(String UserId){
		if(testconfig != null ){
			return testconfig;   
		}else{
			MDM_Settings__c theobject = MDM_Settings__c.getInstance(UserId);
			if(theobject==null){ theobject = new MDM_Settings__c();}
			if(Test.isRunningTest()){
				
				theobject.Triggers_Enabled__c = false;
				theobject.Web_Services_Enabled__c = false;
				theobject.Retry_Buttons_Enabled__c = false;
				theobject.Edit_Sync_Failure_Records__c = false;
				theobject.MDM_Account_Merge_Trigger_Enabled__c = false;
				theobject.MDM_Role_Trigger_Enabled__c = false;
				theobject.Contact_Trigger_Enabled__c = false;
				theobject.Account_Trigger_Enabled__c = false;
			}
			return theobject;
		}
 	}

	public static MDM_Settings__c GetSetting(){
	  if(testconfig != null ){
			return testconfig;   
		}else{
			MDM_Settings__c theobject = MDM_Settings__c.getInstance();

			if(theobject==null){ theobject = new MDM_Settings__c();}
			if(Test.isRunningTest()){
				
				theobject.Triggers_Enabled__c = false;
				theobject.Web_Services_Enabled__c = false;
				theobject.Retry_Buttons_Enabled__c = false;
				theobject.Edit_Sync_Failure_Records__c = false;
				theobject.MDM_Account_Merge_Trigger_Enabled__c = false;
				theobject.MDM_Role_Trigger_Enabled__c = false;
				theobject.Contact_Trigger_Enabled__c = false;
				theobject.Account_Trigger_Enabled__c = false;
			}
			return theobject;
		}
 	}
	
}