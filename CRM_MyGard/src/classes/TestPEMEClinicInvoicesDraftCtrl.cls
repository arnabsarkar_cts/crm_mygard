@isTest(seeAllData=false)
public class TestPEMEClinicInvoicesDraftCtrl
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    public static PEME_Invoice__c pi=new PEME_Invoice__c ();
    Public static List<Object__c> ObjectList=new List<Object__c>();
    static testMethod void cover()
    {
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        Gard_Team__c gardTeam=new Gard_Team__c(P_I_email__c='test@test.com',Region_code__c='CASM', Office__c='Arendal');
        insert gardTeam;
        Country__c country=new Country__c(Claims_Support_Team__c=gardTeam.id);
        insert country;
         Valid_Role_Combination__c vrcESP = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
        insert vrcESP;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
        User user = new User(
                            Alias = 'standt', 
                            profileId = salesforceLicenseId ,
                            Email='standarduser@testorg.com',
                            EmailEncodingKey='UTF-8',
                            CommunityNickname = 'test13',
                            LastName='Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',  
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='test008@testorg.com',
                            ContactId__c = gc.id
                             );
        insert user ;
        User user1 =new User();
        user1 = [select id, contactid__c from user where id=:user.id];
        Account clinicAcc = new Account( Name='testClinic',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    recordTypeId=System.Label.ESP_Contact_Record_Type,
                                    Site = '_www.ctsclinic.se',
                                    Type = 'Surveyor' ,
                                    Area_Manager__c = user1.id,      
                                    Market_Area__c = Markt.id,
                                    country__c = country.Id,
                                    company_Role__c = 'External Service Provider',
                                    Sub_Roles__c = 'ESP - Agent'
                                 );
        AccountList.add(clinicAcc);
        insert AccountList; 
        Blob b = Crypto.GenerateAESKey(128);            
        String h = EncodingUtil.ConvertTohex(b);            
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20);
        clinicAcc .GUID__c=guid;
        update clinicAcc ;
        Contact clinicContact = new Contact( 
                                         FirstName='medicalTest',
                                         LastName='medical',
                                         MailingCity = 'London',
                                         MailingCountry = 'United Kingdom',
                                         MailingPostalCode = 'SE1 1AD',
                                         MailingState = 'London',
                                         MailingStreet = '1 London Road',
                                         AccountId = clinicAcc.Id,
                                         Email = 'test321@gmail.com'
                                       );
        ContactList.add(clinicContact) ;
        insert ContactList;
        
        User clinicUser = new User(
                                Alias = 'Clinic', 
                                profileId = partnerLicenseId,
                                Email='testClinic120@test.com',
                                EmailEncodingKey='UTF-8',
                                LastName='ClinicTesting',
                                LanguageLocaleKey='en_US',
                                CommunityNickname = 'clinicUser',
                                LocaleSidKey='en_US',  
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName='testClinicUser@testorg.com.mygard',
                                ContactId = clinicContact.Id
                               );
        UserList.add(clinicUser);
        insert UserList;
        Object__c Object1=new Object__c(Name= 'TestObject1',
                                     //CurrencyIsoCode='NOK-Norwegian Krone',
                                     Object_Unique_ID__c='test11223'
                                     );
        ObjectList.add(Object1);
        Object__c Object2=new Object__c(
                                     Name= 'TestObject2',
                                    // CurrencyIsoCode='NOK-Norwegian Krone',
                                     Object_Unique_ID__c='test11224'
                                     );
        ObjectList.add(Object2);
        insert ObjectList;
        
        PEME_Enrollment_Form__c pefBroker=new PEME_Enrollment_Form__c();
        pefBroker.ClientName__c=clinicAcc.ID;
        pefBroker.Enrollment_Status__c='Draft';
        pefBroker.Submitter__c=clinicContact.Id;
        insert pefBroker;
        
        //pi.Client__c=clinicAcc.id;
        pi.Clinic_Invoice_Number__c='qwerty123';
        pi.Clinic__c=clinicAcc.id;
        pi.Crew_Examined__c=11223;
        pi.Invoice_Date__c=Date.valueOf('2016-01-05');
        pi.PEME_reference_No__c='aasd23';
        pi.Status__c='Draft';
        pi.ObjectList__c='qwerty for test purpose';
        pi.Total_Amount__c=112230;
        pi.Vessels__c= Object1.id;
        pi.Date_of_Final_Verification__c=Date.valueOf('2016-01-20');
        pi.Period_Covered_From__c=Date.valueOf('2016-01-05');
        pi.Period_Covered_To__c =Date.valueOf('2016-01-28');
        pi.PEME_Enrollment_Detail__c = pefBroker.id;
        insert pi;
        
        System.runAs(clinicUser)
        {
            PEMEClinicInvoicesDraftCtrl PEMEClinicInvoicesDraftCntl=new PEMEClinicInvoicesDraftCtrl();
            //PEMEClinicInvoicesDraftCntl.jointSortingParam='t';
            PEMEClinicInvoicesDraftCntl.generateInvoices();
            PEMEClinicInvoicesDraftCntl.getSortDirection();
            PEMEClinicInvoicesDraftCntl.setSortDirection('ASC');
            PEMEClinicInvoicesDraftCntl.getClientOptionsDraft();
            PEMEClinicInvoicesDraftCntl.createOpenUserList();
            PEMEClinicInvoicesDraftCntl.firstOpen();
            PEMEClinicInvoicesDraftCntl.hasPreviousOpen=true;
            PEMEClinicInvoicesDraftCntl.previousOpen();
            PEMEClinicInvoicesDraftCntl.setpageNumberOpen();
            PEMEClinicInvoicesDraftCntl.hasNextOpen=false;
            PEMEClinicInvoicesDraftCntl.nextOpen();
            PEMEClinicInvoicesDraftCntl.lastOpen();
            PEMEClinicInvoicesDraftCntl.print();
            PEMEClinicInvoicesDraftCntl.exportToExcel();
            PEMEClinicInvoicesDraftCntl.selectedClientsDraft.add('testDraft');
            PEMEClinicInvoicesDraftCntl.clearOptions();
            PEMEClinicInvoicesDraftCntl.editId=String.valueOf(pi.id);
            PEMEClinicInvoicesDraftCntl.goToEditInvoice();
            PEMEClinicInvoicesDraftCntl.deleteId=pi.id;
            PEMEClinicInvoicesDraftCntl.deleteRecord();
            PEMEClinicInvoicesDraftCntl.print();
            PEMEClinicInvoicesDraftCntl.exportToExcel();
            PEMEClinicInvoicesDraftCntl.noOfRecords=10;
            PEMEClinicInvoicesDraftCntl.size=10;
            PEMEClinicInvoicesDraftCntl.intStartrecord=1;
            PEMEClinicInvoicesDraftCntl.intEndrecord=8;
            PEMEClinicInvoicesDraftCntl.jointSortingParam='ASC##Clinic_Invoice_Number__c';
            PEMEClinicInvoicesDraftCntl.generateInvoices();
        }
    }
}