//public with sharing class MeetingMinutesSetupAttendees {
//SOC 24/5/13 - Sync attendees returning different results depending
//on user, removed "with sharing"
public class MeetingMinutesSetupAttendees {
    
    private ApexPages.StandardController controller ;
    public MeetingMinutesSetupAttendees(ApexPages.StandardController con){
        controller = con;
    }    
    public class MeetingMinutesException Extends Exception{}
 
    public pagereference setupAttendees(){
        
        //Called on page load to synch custom attendee object with values from standard attendee object
        Map <Id,Meeting_Attendee__c> attMap = new Map <Id,Meeting_Attendee__c>();
        Meeting_Minute__c mm = new Meeting_Minute__c();
        String eventId = ApexPages.currentPage().getParameters().get('eventId');
        if(eventId == null || eventId == ''){
            // this code is used when the overridden Edit button is clicked on the meeting minute detail page
            mm = (Meeting_Minute__c) controller.getRecord();
            eventId = mm.Event_Id__c;
        }else{
            // get or create the meeting minute object based on Event Id
            List<Meeting_Minute__c> mmList = [select Id, 
                                           Event_Id__c, 
                                           Distributed__c
                                    from Meeting_Minute__c 
                                    where Event_id__c = : eventId];
            if (mmList.size() != 0 ){
                mm=mmlist[0];
            }
            else
            {
                //Steve O'Connell cDecisions 27/08/2013
                //Invitees were being shown an error because the event id doesn't return the MM Id
                //Invitee events are a separate event with isChild=true
                //However there is no link between the two. Make best efforts by finding one with matching details
                List<Event> inviteeEvent = [SELECT WhatId, StartDateTime, OwnerId, isChild  FROM Event WHERE Id=:eventId LIMIT 1];
                if(inviteeEvent.size() == 1)
                {
                    //Check we have child event
                    if(inviteeEvent[0].isChild == true)
                    {
                        //We have child event, find parent event
                        List<EventAttendee> parentEvent = [SELECT EventId FROM EventAttendee WHERE AttendeeId=:inviteeEvent[0].OwnerId AND Event.StartDateTime=:inviteeEvent[0].StartDateTime AND Event.WhatId = :inviteeEvent[0].WhatId];
                    
                        if(parentEvent.size() == 1)
                        {
                            //We have one parent event
                            string mmEventId = parentEvent[0].EventId;
                            mmList = [select Id, 
                                           Event_Id__c, 
                                           Distributed__c
                                    from Meeting_Minute__c 
                                    where Event_id__c = : mmEventId];
                            if (mmList.size() != 0 ){
                                mm=mmlist[0];
                                //Update eventId so that rest of logic returns correct details
                                eventId = mm.Event_Id__c;
                            }                            
                        }                    
                    }
                }
            }
            //Martin Gardner cDecisions 27/07/2012 
            // I think this is causing meeting minute records to be created incorrectly 
            // so I am disabling it and throwing an exception instead
            if (mm.Id == null){
                throw new MeetingMinutesException('Meeting Minute record could not be found. Please create one before navigating to this page.');
            
            //mm = new Meeting_Minute__c(Event_Id__c=eventId);insert (mm);
                /*get the event and create the Meeting Minute record properly
                Event evt = [SELECT Id, Subject, EndDateTime, StartDateTime, Location, OwnerId FROM Event WHERE Id = :eventId LIMIT 1];
                mm.Event_EndDateTime__c = evt.EndDateTime;
                mm.Event_Subject__c = evt.Subject;
                mm.Event_StartDateTime__c = evt.StartDateTime;
                mm.Event_Location__c = evt.Location;
                mm.Event_Id__c = evt.id; 
                mm.OwnerId = evt.OwnerId;
                string newName = evt.StartDateTime.format() + ' - ' + evt.subject;
                if(newName.length() > 80){
                    newName = newName.substring(0,79);
                }
                mm.name= newName;
                */
            }
        }
        if (mm.Distributed__c == true){
            return new apexpages.Pagereference('/apex/Meeting_Minutes_ro?Id=' + mm.Id + '&eventId=' + eventId + '&id=' + mm.Id);
        } else {
            ApexPages.currentPage().getParameters().put('Id', mm.Id);
            ApexPages.currentPage().getParameters().put('id', mm.Id);
            
            // get the list of standard attendees
            List<EventAttendee> stdAtt = [Select e.Status, 
                                              e.Id, 
                                              e.EventId, 
                                              e.AttendeeId 
                                              From EventAttendee e
                                              Where e.EventId = : eventId 
                                              ALL ROWS];
          //create a map of the above to allow setup of attendee type
            Map<Id,EventAttendee> stdAttMap = new Map<Id,EventAttendee>();
            for(EventAttendee ea : stdAtt){ stdAttMap.put (ea.AttendeeId,ea);   
            }
            Map<Id,String> userTypeMap = new Map<Id, String>();
            
            List<Contact> attContact = [Select Id from Contact where id in : stdAttMap.keySet()];
            for (Contact c : attContact){userTypeMap.put(c.id,'Contact');
            }
            List<User> attUser = [Select Id from User where id in : stdAttMap.keySet()];
            for (User u : attUser){userTypeMap.put(u.id,'User');
            }
            userTypeMap.put(userinfo.getuserid(),'User');
            List<Lead> attLead = [Select Id from Lead where id in : stdAttMap.keySet()];
            for (Lead l : attLead){userTypeMap.put(l.id,'Lead');
            }          

            Event ev = [Select  e.OwnerId, e.WhoId, e.WhatId, e.StartDateTime From Event e where id = : eventId ALL ROWS];
            // MG cDecisions PCI-000096 and create a map at the same time keyed on the Attendee Id            
            boolean flag = ev.StartDateTime > Datetime.now();
            if(ev.StartDateTime > Datetime.now()){
                // look for standard attendees that haven't been added to the custom list
                // get the current list of custom attendees
                List<Meeting_Attendee__c> mAtt = [Select m.Name, 
                                                         m.Meeting_Role__c, 
                                                         m.Meeting_Attendee__c, 
                                                         m.Id, 
                                                         m.Event_Id__c,
                                                         m.Type__c 
                                                         From Meeting_Attendee__c m
                                                         Where m.Event_Id__c = : eventId];
                         
                for (Meeting_Attendee__c ma : mAtt){
                    attMap.put(ma.Meeting_Attendee__c, ma);
                }
                Map <Id , Meeting_Attendee__c> maToAdd = new Map<Id, Meeting_Attendee__c>();   
                //Convert the Standard attendee list in to MyEventAttendee          
                Map<Id, MyEventAttendee> myAttMap = new Map<Id, MyEventAttendee>();   
                for(EventAttendee ea: stdAtt){myAttMap.put(ea.AttendeeId,new MyEventAttendee(ea));}
                maToAdd = findMissingAttendees(myAttMap.values(), attMap, userTypeMap, mm.Id);
                if(!attMap.containsKey(ev.OwnerId)){
                    Meeting_Attendee__c uma = new Meeting_Attendee__c(User__c=ev.OwnerId, Type__c='User', Meeting_Minute__c=mm.Id, Meeting_Attendee__c = ev.OwnerId, Event_Id__c=eventId);
                    maToAdd.put(ev.OwnerId, uma); 
                }
                       
                String whoId = ev.WhoId;
                List <Contact> whoContacts = [Select Id from Contact where id = : whoId];
                if (whoContacts.size() > 0 ){
                    if(!attMap.containsKey(whoId)){
                        Meeting_Attendee__c ma = new Meeting_Attendee__c(Contact__c=whoContacts[0].Id, Type__c='Contact', Meeting_Minute__c=mm.Id, Meeting_Attendee__c = whoContacts[0].Id, Event_Id__c=eventId);
                        maToAdd.put(whoContacts[0].Id, ma);                   
                    }
                }
                // and insert them
                if (maToAdd.size() > 0 ){insert(maToAdd.values());
                }
                // MG cDecisions PCI-000096 find the attendees that have been removed from the event and remove them from the meeting minutes
                List<Meeting_Attendee__c> attToDel = new List<Meeting_Attendee__c>();
                attToDel = findDeltedAttendees(myAttMap, attMap, ev.OwnerId);
                 // For CRM-47
                //if(attToDel.size()>0){delete(attToDel);}       
            }     
            return new apexpages.Pagereference('/apex/Meeting_Minutes?Id=' + mm.Id + '&eventId=' + eventId + '&id=' + mm.Id);
        }
    }
    
    public static void setupAttendeesNonUI(Id eventId, Id MeetingMinuteId, Boolean distributed){
        //Martin Gardner cDecisions 
        //Called from non-ui contexts to synch custom attendee object with values from standard attendee object
        Map <Id,Meeting_Attendee__c> attMap = new Map <Id,Meeting_Attendee__c>();
        if (distributed != true){
            // get the list of standard attendees
            List<EventAttendee> stdAtt = [Select e.Status, 
                                              e.Id, 
                                              e.EventId, 
                                              e.AttendeeId 
                                              From EventAttendee e
                                              Where e.EventId = : eventId 
                                              ALL ROWS];
            
            //create a map of the above to allow setup of attendee type
            Map<Id,EventAttendee> stdAttMap = new Map<Id,EventAttendee>();
            for(EventAttendee ea : stdAtt){stdAttMap.put (ea.AttendeeId,ea);
            }
            Map<Id,String> userTypeMap = new Map<Id, String>();
            
            List<Contact> attContact = [Select Id from Contact where id in : stdAttMap.keySet()];
            for (Contact c : attContact){userTypeMap.put(c.id,'Contact');
            }
            List<User> attUser = [Select Id from User where id in : stdAttMap.keySet()];
            for (User u : attUser){userTypeMap.put(u.id,'User');
            }
            userTypeMap.put(userinfo.getuserid(),'User');
            List<Lead> attLead = [Select Id from Lead where id in : stdAttMap.keySet()];
            for (Lead l : attLead){userTypeMap.put(l.id,'Lead');
            }

            Event ev = [Select  e.OwnerId, e.WhoId, e.WhatId, e.StartDateTime From Event e where id = : eventId ALL ROWS];
            // MG cDecisions PCI-000096 and create a map at the same time keyed on the Attendee Id            
            
            if(ev.StartDateTime > Datetime.now()){            
                // get the current list of custom attendees
                List<Meeting_Attendee__c> mAtt = [Select m.Name, 
                                                         m.Meeting_Role__c, 
                                                         m.Meeting_Attendee__c, 
                                                         m.Id, 
                                                         m.Event_Id__c,
                                                         m.Type__c 
                                                         From Meeting_Attendee__c m
                                                         Where m.Event_Id__c = : eventId];
                for (Meeting_Attendee__c ma : mAtt){System.debug('\n\n ma.Meeting_Attendee__c: ' + ma.Meeting_Attendee__c);attMap.put(ma.Meeting_Attendee__c, ma);
                }
                // look for standard attendees that haven't been added to the custom list            
                Map <Id , Meeting_Attendee__c> maToAdd = new Map<Id, Meeting_Attendee__c>();
                //Convert the Standard attendee list in to MyEventAttendee
                // MG cDecisions PCI-000096 and create a map at the same time keyed on the Attendee Id
                Map<Id, MyEventAttendee> myAttMap = new Map<Id, MyEventAttendee>();            
                for(EventAttendee ea: stdAtt){myAttMap.put(ea.AttendeeId,new MyEventAttendee(ea));}
                maToAdd = findMissingAttendees(myAttMap.values(), attMap, userTypeMap, MeetingMinuteId);
                if(!attMap.containsKey(ev.OwnerId)){
                    Meeting_Attendee__c uma = new Meeting_Attendee__c(User__c=ev.OwnerId, Type__c='User', Meeting_Minute__c=MeetingMinuteId, Meeting_Attendee__c = ev.OwnerId, Event_Id__c=eventId);
                    maToAdd.put(ev.OwnerId, uma); 
                }
                           
                String whoId = ev.WhoId;
                List <Contact> whoContacts = [Select Id from Contact where id = : whoId];
                if (whoContacts.size() > 0 ){
                    if(!attMap.containsKey(whoId)){
                        Meeting_Attendee__c ma = new Meeting_Attendee__c(Contact__c=whoContacts[0].Id, Type__c='Contact', Meeting_Minute__c=MeetingMinuteId, Meeting_Attendee__c = whoContacts[0].Id, Event_Id__c=eventId);
                        maToAdd.put(whoContacts[0].Id, ma);                   
                    }
                }     
                // and insert them
                if (maToAdd.size() > 0 ){
                    insert(maToAdd.values());
                }
                 // MG cDecisions PCI-000096 find the attendees that have been removed from the event and remove them from the meeting minutes
                List<Meeting_Attendee__c> attToDel = new List<Meeting_Attendee__c>();
                attToDel = findDeltedAttendees(myAttMap, attMap, ev.OwnerId);
                if(attToDel.size()>0){delete(attToDel);}           
            }
        }       
    }  
    
    // **************************** TEST *************************************
    
    static testMethod void testNonUI(){
        Event evt = new Event(Subject='TESTCOVERAGE', StartDateTime=datetime.now().addDays(1), EndDateTime=datetime.now().addDays(2));
        insert evt;
        Event e = [select id from Event where subject='TESTCOVERAGE' limit 1];

        system.assertNotEquals(null, e.Id);
        Meeting_Minute__c a = new Meeting_Minute__c(event_id__c = e.Id);
        a.Distributed__c = false;
        insert(a);
        system.assertNotEquals(null, a.Id);
        
        MeetingMinutesSetupAttendees.setupAttendeesNonUI(e.Id, a.Id, a.Distributed__c);
    }  
    
    public static List<Meeting_Attendee__c> swapMeetingAttendeeUser(List<Meeting_Attendee__c> attList, Id oldUserId, Id newUserId){
        
        List<Meeting_Attendee__c> swapList = new List<Meeting_Attendee__c>();
        
        for(Meeting_Attendee__c att : attList){
            if(att.User__c == oldUserId){
                att.User__c = newUserId;
                att.Meeting_Attendee__c = newUserId;
                swapList.add(att);
            }
        }
        return swapList;
    }       

    public class MyEventAttendee {
        String Status {get;set;}
        Id Id {get;set;}
        Id EventId {get;set;}
        Id AttendeeId {get;set;}
        
        MyEventAttendee(EventAttendee ea){
            this.Status = ea.Status;
            this.Id = ea.Id;
            this.EventId = ea.EventId;
            this.AttendeeId = ea.AttendeeId;
        }
        
        MyEventAttendee(String status, Id event, Id attendee){
            this.Status = status;
            this.EventId = event;
            this.AttendeeId = attendee;
        }       
    }    
    static TestMethod void testMyEventAttendee() {
        EventAttendee ea = [Select Id, Status, EventId, AttendeeId FROM EventAttendee LIMIT 1];
        MyEventAttendee mea = new MyEventAttendee(ea);
    }
    
    // MG cDecisions PCI-000096 
    public static List<Meeting_Attendee__c> findDeltedAttendees(  Map <Id,MyEventAttendee> stdAtt, 
                                    Map <Id,Meeting_Attendee__c> attMap,
                                    Id OwnerId){
      List<Meeting_Attendee__c> maToDel = new List<Meeting_Attendee__c>();
      system.debug('\nfindDeletedAttendees\nstdAtt ' + stdAtt + '\nattMap ' + attMap + '\nOnwnerId ' + OwnerId + '\n');
    // look for meeting attendees that have been deleted from the standard list
        for(Meeting_Attendee__c ea : attMap.values()){
            if(!stdAtt.containsKey(ea.Meeting_Attendee__c) && ea.Meeting_Attendee__c <> OwnerId){
                maToDel.add(ea);
            }
        }                  
      return maToDel;                                        
    }    
    
    
    public static Map <Id , Meeting_Attendee__c> findMissingAttendees(  List<MyEventAttendee> stdAtt, 
                                                                        Map <Id,Meeting_Attendee__c> attMap,
                                                                        Map<Id,String> userTypeMap,
                                                                        ID mmId) {
        Map <Id , Meeting_Attendee__c> maToAdd = new Map<Id, Meeting_Attendee__c>();
        // look for standard attendees that haven't been added to the custom list
        for(MyEventAttendee ea : stdAtt){
            if(!attMap.containsKey(ea.AttendeeId)){
                string maType = userTypeMap.get(ea.attendeeId);
                if (maType == 'User'){
                    Meeting_Attendee__c ma = new Meeting_Attendee__c(User__c=ea.attendeeId, Type__c=maType, Meeting_Minute__c=mmId, Meeting_Attendee__c = ea.AttendeeId, Event_Id__c=ea.EventId);
                    maToAdd.put(ea.attendeeId, ma);
                }else if (maType == 'Contact'){
                    Meeting_Attendee__c ma = new Meeting_Attendee__c(Contact__c=ea.attendeeId, Type__c=maType, Meeting_Minute__c=mmId, Meeting_Attendee__c = ea.AttendeeId, Event_Id__c=ea.EventId);
                    maToAdd.put(ea.attendeeId, ma);
                }else if (maType == 'Lead'){
                    Meeting_Attendee__c ma = new Meeting_Attendee__c(Lead__c=ea.attendeeId, Type__c=maType, Meeting_Minute__c=mmId, Meeting_Attendee__c = ea.AttendeeId, Event_Id__c=ea.EventId);
                    maToAdd.put(ea.attendeeId, ma);
                } else{
                    Meeting_Attendee__c ma = new Meeting_Attendee__c(Type__c=maType, Meeting_Minute__c=mmId, Meeting_Attendee__c = ea.AttendeeId, Event_Id__c=ea.EventId);
                    maToAdd.put(ea.attendeeId, ma);
                }
            }
        }
        return  maToAdd; 
    } 
    
    static testMethod void testfindMissingAttendees(){
        
        // MG cDecisions PCI-000096 Test delted as well 
        Map <Id , Meeting_Attendee__c> maToAdd = new Map<Id, Meeting_Attendee__c>();
        Map <Id,Meeting_Attendee__c> attMap = new Map <Id,Meeting_Attendee__c>();
        List<Meeting_Attendee__c> attToDel = new List<Meeting_Attendee__c>();        
        Map<Id,String> userTypeMap = new Map<Id, String>();

        Profile p = [Select Id, Name from Profile LIMIT 1];
        User tempusr = [SELECT TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, LanguageLocaleKey from User where IsActive=true LIMIT 1];
        User usr1 = new User(UserName='EOAtest1@gard.no', Alias='test1', LastName='test1', ProfileId=p.Id, Email='test1@gard.no', TimeZoneSidKey=tempusr.TimeZoneSidKey, LocaleSidKey=tempusr.LocaleSidKey, EmailEncodingKey=tempusr.EmailEncodingKey, LanguageLocaleKey=tempusr.LanguageLocaleKey);
        User usr2 = new User(UserName='EOAtest2@gard.no', Alias='test2', LastName='test2', ProfileId=p.Id, Email='test2@gard.no', TimeZoneSidKey=tempusr.TimeZoneSidKey, LocaleSidKey=tempusr.LocaleSidKey, EmailEncodingKey=tempusr.EmailEncodingKey, LanguageLocaleKey=tempusr.LanguageLocaleKey);
        insert usr1;
        insert usr2;
        Event evt = new Event(Subject='TESTCOVERAGE', StartDateTime=datetime.now().addDays(1), EndDateTime=datetime.now().addDays(2));
        insert evt;
        Event e = [select id, OwnerId from Event where subject='TESTCOVERAGE' limit 1];      
        system.assertNotEquals(null, e.Id);
        Lead ld = new Lead(LastName='Test Lead', Company='Test Corp');
        insert ld;
        Account acc = new Account(  Name='APEXTESTACC001',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    objectives__c = 'Test Objective',
                                    plan__c = 'Test Plan',
                                    Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id,
                                    Market_Area__c = TestDataGenerator.getMarketArea().Id
                                );
        insert acc;
        String strAccId = acc.Id;                               
        Contact con = new Contact(LastName='Test Contact', AccountId = acc.Id);
        Contact con2 = new Contact(LastName='Test Contact2', AccountId = acc.Id);
        insert con;
        insert con2;
        
        userTypeMap.put(ld.Id, 'Lead');
        userTypeMap.put(usr1.Id, 'User');
        userTypeMap.put(con.Id, 'Contact');
        userTypeMap.put(con2.Id, 'Other');
        
        
        // get the list of standard attendees
        List<MyEventAttendee> stdAtt = new List<MyEventAttendee>();
        stdAtt.add(new MyEventAttendee('Test', e.Id, ld.Id));
        stdAtt.add(new MyEventAttendee('Test', e.Id, usr1.Id));
        stdAtt.add(new MyEventAttendee('Test', e.Id, con.Id));
        stdAtt.add(new MyEventAttendee('Test', e.Id, con2.Id));
                
        Meeting_Minute__c mm = new Meeting_Minute__c(event_id__c = e.Id, Distributed__c=false);
        insert(mm);
            
        maToAdd = findMissingAttendees(stdAtt, attMap, userTypeMap, mm.Id);
        // MG cDecisions PCI-000096 modify the list of std attendees
        Map <Id, MyEventAttendee> stdAttMap = new Map<Id, MyEventAttendee>();
        for(MyEventAttendee ea : stdAtt){
          //Ignore one of the attendees
          if(ea.Id != con.Id){
            stdAttMap.put(ea.AttendeeId, ea);
          }
        }
        attToDel = findDeltedAttendees(stdAttMap, attMap, e.OwnerId);
                 
        //Also test swap meeting attendees here.
        MeetingMinutesSetupAttendees.setupAttendeesNonUI(e.Id, mm.Id, mm.Distributed__c);
        List<Meeting_Attendee__c> attList = new List<Meeting_Attendee__c>();
        List<Meeting_Attendee__c> swapList = new List<Meeting_Attendee__c>();
        Meeting_Attendee__c ma = new Meeting_Attendee__c(User__c=usr1.Id, Type__c='User', Meeting_Minute__c=mm.Id, Meeting_Attendee__c = usr1.Id, Event_Id__c=e.Id);
        attList.add(ma);        
        swapList = swapMeetingAttendeeUser(attList, usr1.Id, usr2.Id);
        system.assert(swapList.size()>0);
        system.assertEquals(swapList[0].User__c, usr2.Id);
    }
    
    static testMethod void test(){
        Event evt = new Event(Subject='TESTCOVERAGE', StartDateTime=datetime.now().addDays(1), EndDateTime=datetime.now().addDays(2));
        insert evt;
        Event e = [select id from Event where subject='TESTCOVERAGE' limit 1];
        //Contact c = new Contact();
        //c.LastName='Sutherland';
        //insert (c);
        //Event e = new Event(Subject = 'test');
        //e.StartDateTime = datetime.now().addDays(1);
        //e.EndDateTime = datetime.now().adddays(2);
        //insert(e);
        system.assertNotEquals(null, e.Id);
        // Not possible to create EventAttendees through the API so can't get full test coverage...
        EventAttendee ea = new EventAttendee();
        //ea.EventId = e.id;
        //ea.AttendeeId = c.Id;
        //ea.Status = 'New';
        //insert(ea);
        Meeting_Minute__c a = new Meeting_Minute__c(event_id__c = e.Id);
        a.Distributed__c = false;
        insert(a);
        system.assertNotEquals(null, a.Id);
        
        ApexPages.currentPage().getParameters().put('eventId',e.id);    
        ApexPages.StandardController sc = new ApexPages.Standardcontroller(a);
        MeetingMinutesSetupAttendees mme = new MeetingMinutesSetupAttendees(sc);
        mme.setupAttendees();
        


        // Instantiate second controller with no eventid parameter to run through seperate branch of code
        ApexPages.currentPage().getParameters().put('eventId','');    
        ApexPages.StandardController sc2 = new ApexPages.Standardcontroller(a);
        MeetingMinutesSetupAttendees mme2 = new MeetingMinutesSetupAttendees(sc2);
        mme2.setupAttendees();
        
        a.Distributed__c = true;
        update(a);
                
        ApexPages.currentPage().getParameters().put('eventId',e.id);    
        ApexPages.StandardController sc3 = new ApexPages.Standardcontroller(a);
        MeetingMinutesSetupAttendees mme3 = new MeetingMinutesSetupAttendees(sc3);
        mme3.setupAttendees(); 
        
        //negative testing
         ApexPages.currentPage().getParameters().put('eventId',null);    
        ApexPages.StandardController sc4 = new ApexPages.Standardcontroller(a);
        MeetingMinutesSetupAttendees mme4 = new MeetingMinutesSetupAttendees(sc4);
        mme4.setupAttendees();      
    }
    
    
}