//Created again by pulkit, intended to replace the old BatchInsertMyCover after 18R3
//TO Run this Batch, run following:
//Database.executeBatch(new BatchInsertMyCoverList(true));
//Database.executeBatch(new BatchInsertMyCoverList(false)); 
//UPDATE : Created records seemed alright, TO BE DEPLOYED TO PROD
global class BatchInsertMyCoverList implements Database.Batchable<sObject> {
    
    //Declaring variables.
    global List<Id> accountIds;
    global String strQuery;
    global Boolean risk;
    global List<Boolean> onRiskList;
    
    //Constructors STARTS
    global BatchInsertMyCoverList (Boolean risk,List<Id> accountIds){
        this.accountIds = accountIds;
        this.risk = risk;
        this.onRiskList = new List<Boolean>();
        onRiskList.add(risk);
        strQuery = 'SELECT id FROM Account WHERE id IN :accountIds';
    }
    
    global BatchInsertMyCoverList (Boolean risk){
        this.risk = risk;
        this.onRiskList = new List<Boolean>();
        onRiskList.add(risk);
        strQuery = 'SELECT id FROM Account';
    }
    //Constructors ENDS
    
    //Batch Methods START
    //Overriding the start method
    global Database.QueryLocator start(Database.BatchableContext BC){               
        return Database.getQueryLocator(strQuery);        
    }
    
    //Override the execute method
    global void execute(Database.BatchableContext BC, List<account> accounts){
        List<Id> contractIds = new List<Id>();
        List<Id> coverListIds = new List<Id>();
        List<MyCoverList__c> newMyCoverLists = new List<MyCoverList__c>();//to be created
        List<MyCoverList__c> oldMyCoverLists = new List<MyCoverList__c>();//to be deleted
		String strCondition;
        String assetQuery;
        
        strCondition = ' AND On_risk_indicator__c IN :onRiskList ';
        
        if(accountIds == null){
            accountIds = new List<Id>();
            for(Account account : accounts){
                accountIds.add(account.Id);
            }
        }
        
        for(Contract contract : Database.query('SELECT id FROM Contract WHERE AccountId IN :accountIds')){
            contractIds.add(contract.Id);
        }
        
        for(MyCoverList__c myCoverList : Database.query('SELECT id FROM MyCoverList__c WHERE onrisk__c IN :onRiskList AND AgreementId__c IN :contractIds')){
            oldMyCoverLists.add(myCoverList);
        }
        
        //String strforClient = '';
        //strforClient = 'Agreement__r.Client__r.Name clientname, Agreement__r.Client__r.Id clientId,';
        
        //String strforClientGroupby = '';
        //strforClientGroupby = 'Agreement__r.Client__r.Name,Agreement__r.Client__r.Id,';
        
        //Run the aggregate query to retrieve asset info grouped by parameters
        assetQuery = 'SELECT MAX(Expiration_Date__c) expdate,agreement__c AgId, Agreement__r.Client__r.Name clientname, Agreement__r.Client__r.Id clientId, UnderwriterNameTemp__c underwriter, UnderwriterId__c underwriterId,Product_Name__c prod,Agreement__r.Business_Area__c busarea,'+ 
            'Agreement__r.Broker__r.Name AggName,Agreement__r.Broker__r.Id AggId, Agreement__r.Policy_Year__c plcyyr, '+
            'Claims_Lead__c claim, Gard_Share__c grdshr, On_risk_indicator__c rskind,Agreement__r.Shared_With_Client__c sharedwithclient,  '+
            'Count(id) FROM Asset Where Agreement__c IN :contractIds'+strCondition+
            'GROUP BY agreement__c,Agreement__r.Client__r.Name,Agreement__r.Client__r.Id, UnderwriterNameTemp__c, UnderwriterId__c, Product_Name__c, '+
            'Agreement__r.Business_Area__c,'+
            'Agreement__r.Broker__r.Name,Agreement__r.Broker__r.Id,'+
            'Agreement__r.Policy_Year__c,'+
            'Claims_Lead__c, Gard_Share__c, On_risk_indicator__c,Agreement__r.Shared_With_Client__c ';
        
        AggregateResult[] groupedResults = database.query(assetQuery);
        
        //Looping the aggregateresult to generate mycover list records
        for (AggregateResult ar : groupedResults){
                newMyCoverLists.add(new MyCoverList__c(
                                        AgreementId__c = string.valueof(ar.get('AgId')),
                                        client__c=string.valueof(ar.get('clientname')),
                                        client_id__c=string.valueof(ar.get('clientId')),
                                        underwriter__c=string.valueof(ar.get('underwriter')),
                                        UnderwriterId__c=string.valueof(ar.get('underwriterId')),
                                        product__c=string.valueof(ar.get('prod')),
                                        productarea__c=string.valueof(ar.get('busarea')),
                                        broker__c=string.valueof(ar.get('AggName')),
                                        broker_id__c=string.valueof(ar.get('AggId')), 
                                        expirydate__c=string.valueof(ar.get('expdate')),
                                        policyyear__c=Integer.valueof(ar.get('plcyyr')),                                           
                                        claimslead__c=string.valueof(ar.get('claim')),
                                        gardshare__c=Double.valueof(ar.get('grdshr')),
                                        onrisk__c=Boolean.valueof(ar.get('rskind')),
                                        viewobject__c=Integer.valueOf(ar.get('expr0')),
                                        Shared_With_Client__c=Boolean.valueof(ar.get('sharedwithclient'))
                					));
        }
        
        //Delete existing records
        Database.delete( oldMyCoverLists, false);
        
        //insert lstMyCover;
        List<Database.SaveResult> dbSRs = Database.insert( newMyCoverLists, false);
        
        //if(allSuccess(dbSRs)){
            //Delete the old records
        	
       //}
       //
       accountIds = null;
    }
    
    //Placeholder for overriding the finish method
    global void finish(Database.BatchableContext BC){}
    //Batch Methods END
}