global class ContactRoleWrapper implements Comparable{
    public Contact allContact{get;set;}
    public Boolean isRole{get;set;}
    public String allRole{get;set;}
    public AccountContactRelation aCRel{get;set;}//added for SF-4420
    public static Map<Id,ContactRoleWrapper> cRWMap;
    public static Map<String,String> aCRelToHiddenRoles = new Map<String,String>();//SF-4890
    
    public ContactRoleWrapper(Contact con,String roles,Boolean isDirect){
        allContact = con;
        allRole = roles;
        isRole = !isDirect;
    }
    
    //Constructor added for SF-4420
    public ContactRoleWrapper(Contact con,AccountContactRelation aCRel,String roles,Boolean isDirect){
        if(cRWMap == null) cRWMap = new Map<Id,ContactRoleWrapper>();
        
        allContact = con;
        
        if(roles!=null)allRole = roles;
        else allRole='';
        
        isRole = !isDirect;
        this.aCRel = aCRel;
        
        cRWMap.put(con.id,this);
    }
    
    global Integer compareTo(Object objToCompare){
        ContactRoleWrapper that = (ContactRoleWrapper) objToCompare;
        String thisFirstName,thisLastName,thatFirstName,thatLastName,thisFullName,thatFullName;
        
        thisFirstName = this.allContact.firstName != null ? this.allContact.firstName : '';
        thisLastName = this.allContact.lastName != null ? this.allContact.lastName : '';
        thisFullName = (thisLastName+' '+thisFirstName);
        
        thatFirstName = that.allContact.firstName != null ? that.allContact.firstName : '';
        thatLastName = that.allContact.lastName != null ? that.allContact.lastName : '';
        thatFullName = (thatLastName+' '+thatFirstName);
        
        return thisFullName.compareTo(thatFullName);
    }
    
    //Added for SF-4420
    public static List<ContactRoleWrapper> showAllContact(String selectedAccountId){
        List<ContactRoleWrapper> lstRoleWrapper = new List<ContactRoleWrapper>();
        Set<Id> contactsetId = new Set<Id>();
        List<AccountContactRelation> AccountContactRel = [select id,AccountId,Account.Name,ContactId,Contact.firstName,Contact.lastName,Contact.Name,roles,isDirect,Contact.Department,Contact.Occupation__c,Contact.Title,Contact.OtherPhone,Contact.MobilePhone,Contact.primary_contact__c,Contact.phone,Contact.Account.Email__c,Contact.Email,Contact.OtherStreet,Contact.OtherCity,Contact.OtherState, Contact.OtherPostalCode,Contact.OtherCountry,Contact.No_Longer_Employed_by_Company__c  from AccountContactRelation   where Accountid=:selectedAccountId and Contact.No_Longer_Employed_by_Company__c=false and (NOT Contact.Name  like '%test%') AND (NOT Contact.Name  like '%support%') ORDER BY Contact.Account.name];
        //SF-4890 Added by Geetham
        Map<String,Contact_roles_to_hide_in_MyGard__c> contRoleToHide = Contact_roles_to_hide_in_MyGard__c.getAll();
        String HiddenRoles,accConId;
        for(AccountContactRelation acontRel : AccountContactRel){
            //Added By Geetham For SF-4890
            for(String contRole : contRoleToHide.keyset())
            {
                if(acontRel.Roles!=null && acontRel.Roles.contains(';'+contRole))
                {
                    accConId = acontRel.AccountId+'-'+acontRel.ContactId;
                    HiddenRoles = aCRelToHiddenRoles.get(accConId);
                    if(HiddenRoles==null)
                    {
                        HiddenRoles = contRole;
                    }else
                    {
                        HiddenRoles += ';'+contRole;
                    }
                    aCRelToHiddenRoles.put(accConId,HiddenRoles);
                    acontRel.Roles = acontRel.Roles.remove(';'+contRole);
                }
                else if(acontRel.Roles!=null && acontRel.Roles.contains(contRole+';'))
                {
                    accConId = acontRel.AccountId+'-'+acontRel.ContactId;
                    HiddenRoles = aCRelToHiddenRoles.get(accConId);
                    if(HiddenRoles==null)
                    {
                        HiddenRoles = contRole;
                    }else
                    {
                        HiddenRoles += ';'+contRole;
                    }
                    aCRelToHiddenRoles.put(accConId,HiddenRoles);
                    acontRel.Roles = acontRel.Roles.remove(contRole+';');
                }
                else if((acontRel.Roles!=null && acontRel.Roles == contRole))
                {
                    accConId = acontRel.AccountId+'-'+acontRel.ContactId;
                    HiddenRoles = aCRelToHiddenRoles.get(accConId);
                    if(HiddenRoles==null)
                    {
                        HiddenRoles = contRole;
                    }else
                    {
                        HiddenRoles += ';'+contRole;
                    }
                    aCRelToHiddenRoles.put(accConId,HiddenRoles);
                    acontRel.Roles = acontRel.Roles.remove(contRole);
                }
            }
            Contact cont = new Contact(
                AccountId = acontRel.AccountId,
                id = acontRel.ContactId,
                firstName = acontRel.Contact.firstName,
                lastName = acontRel.Contact.lastName,
                Department = acontRel.Contact.Department,
                Occupation__c = acontRel.Contact.Occupation__c,
                primary_contact__c = acontRel.Contact.primary_contact__c,
                phone = acontRel.Contact.phone,
                OtherPhone = acontRel.Contact.OtherPhone,
                MobilePhone = acontRel.Contact.MobilePhone,
                Email = acontRel.Contact.Email,
                title = acontRel.Contact.title,
                OtherStreet = acontRel.Contact.OtherStreet,
                OtherCity = acontRel.Contact.OtherCity,
                OtherState = acontRel.Contact.OtherState,
                OtherPostalCode = acontRel.Contact.OtherPostalCode,
                OtherCountry = acontRel.Contact.OtherCountry,
                No_Longer_Employed_by_Company__c = acontRel.Contact.No_Longer_Employed_by_Company__c
            );
            lstRoleWrapper.add(new ContactRoleWrapper(cont,acontRel,acontRel.Roles,acontRel.IsDirect)); 
        }
        return lstRoleWrapper;
    }
    
   /* //Added for SF-4920
    public static List<Contact> getAllContacts(){
        List<Contact> retContacts = new List<Contact>();
        retContacts.addAll(getDirectContacts());
        retContacts.addAll(getIndirectContacts());
        return retContacts;
    }
    public static List<Contact> getDirectContacts(){
        List<Contact> retContacts = new List<Contact>();
        for(ContactRoleWrapper cRW : cRWMap.values()){
            if(!cRW.isRole)retContacts.add(cRW.allContact);
        }
        return retContacts;
    }
    public static List<Contact> getIndirectContacts(){
        List<Contact> retContacts = new List<Contact>();
        for(ContactRoleWrapper cRW : cRWMap.values()){
            if(cRW.isRole)retContacts.add(cRW.allContact);
        }
        return retContacts;
    }*/
}