public class MeetingMinutesAction {
  //Define the Event Object
  Event theEvent = new Event();
  String theEventID;
  String theURL;
  
   // Constructor - this only really matters if the autoRun function doesn't work right     
    public MeetingMinutesAction(ApexPages.StandardController stdController) {        
        this.theEvent = (Event)stdController.getRecord();     
    } 
    
    // Code invoked on page load.      
    public PageReference autoRun()
    {           
        String thePageEventId = ApexPages.currentPage().getParameters().get('id'); 
        if (thePageEventId == null) 
        {             
            // Display the Visualforce page's error message if no Id is passed over             
            return null;         
        }       
      
    for (Event theEvent:[select Id from Event where id =:thePageEventId Limit 1]) 
    { 
        theEventID = theEvent.Id; 
        TheURL = 'https://'+ApexPages.currentPage().getHeaders().get('Host')+ '/apex/MeetingMinutesInitialisation?eventId=' + theEventID; 
    }  
      
    // Redirect the user to teh Meeting Minute.
    PageReference pageRef = new PageReference(TheURL);         
    pageRef.setRedirect(true);    
    return pageRef;    
    }
  
  
}