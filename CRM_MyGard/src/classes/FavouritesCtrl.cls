public without sharing class FavouritesCtrl{
    
    public List<Extranet_Favourite__c> listClients {get;set;}
    public List<Extranet_Favourite__c> listObjects {get;set;}
    public List<Extranet_Favourite__c> listClaims {get;set;}
    public List<Extranet_Favourite__c> listSavedSearches {get;set;}
    public String favouriteId{get;set;}
    public static Integer homePageVisit{get;set;}
    
    public FavouritesCtrl(){
        listClients = new List<Extranet_Favourite__c>();
        listObjects = new List<Extranet_Favourite__c>();
        listClaims = new List<Extranet_Favourite__c>();
        listSavedSearches = new List<Extranet_Favourite__c>();
        fetchFavourites();  
    }
    
    
    public void fetchFavourites(){        
        String currentUserId = UserInfo.getUserId(); 
        User usr = [Select Id, contactID, Name from User Where Id =: UserInfo.getUserId()];//Added For Favourites(Multiple Client)
        AccountToContactMap__c acmCS = AccountToContactMap__c.getValues(usr.contactID);//Added For Favourites(Multiple Client)       
        for(Extranet_Favourite__c obj:[select Id,Name,Item_Id__c,Item_Type__c,Item_Name__c,Favourite_By__c,Object__c,Object__r.Imo_Lloyds_No__c,Object__r.Name,Object__r.guid__c,Claim__c,
                                      Claim__r.Claim_Reference_Number__c,Claim__r.Claim_Type__c,Claim__r.Event_Date__c,Claim__r.Guid__c,Saved_Query__c, 
                                      Related_claim__r.Claim_Reference_Number__c,Related_claim__r.Claim_Type__c,Related_claim__r.Event_Date__c,Related_claim__r.Guid__c
                                      from Extranet_Favourite__c where Favourite_By__c=:currentUserId
          and Favourite_For_Broker_Client__c =:acmCS.AccountId__c order by Item_Name__c asc]){//Added For Favourites(Multiple Client)
            if(obj.Item_Type__c=='Client'){
                listClients.add(obj);        
            }else if(obj.Item_Type__c=='Object'){
                listObjects.add(obj);    
            }else if(obj.Item_Type__c=='Claim'){
                listClaims.add(obj);    
            }else if(obj.Item_Type__c=='Saved Search'){
                listSavedSearches.add(obj);    
            }  
        }
    }
    
    public void removeFavourites(){
        system.debug('removeFavourites');           
        system.debug('favouriteId: '+favouriteId);
        List<Extranet_Favourite__c> listFav = new List<Extranet_Favourite__c>();                        
        listFav = [select Id,Item_Id__c,Favourite_By__c from Extranet_Favourite__c where Id=:favouriteId LIMIT 1];
        if(listFav.size()>0){
            delete listFav;
        }
    }
    
    //Util Methods To Be Called from other Classes - Start
    public static String fetchPageName(String pageURL){
        String pageName = '';
        if(pageURL!=null && pageURL!=''){
            pageName = pageURL;            
            pageName = pageName.replaceFirst('/apex/','');                  
            pageName = EncodingUtil.urlEncode(pageName, 'UTF-8');           
            String[] pageNameExtra = pageName.split('%3F',0);             
            pageName = pageNameExtra[0]; 
            pageName = String.escapeSingleQuotes(pageName);
            system.debug('pageName-->'+pageName);
        }
        return pageName; 
    }
    
    public static List<Extranet_Favourite__c> fetchExistingFavsList(String pageName){
        List<Extranet_Favourite__c> lstFavs = new List<Extranet_Favourite__c>();
        lstFavs = [select Id,Item_Id__c,Item_Name__c,Item_Type__c from Extranet_Favourite__c where Item_Id__c=:pageName AND Item_Type__c = 'Saved Search' ORDER BY Item_Name__c DESC LIMIT 1];
        return lstFavs; 
    }
    
    public static List<Extranet_Favourite__c> fetchFavsListById(String favId){
        List<Extranet_Favourite__c> lstFavs = new List<Extranet_Favourite__c>();
        lstFavs = [select Id,Item_Id__c,Favourite_By__c,Saved_Query__c,Filter_Client__c,Filter_Product_Area__c,Filter_Policy_Year__c,Filter_On_Risk__c,Filter_Cover__c,
                   Filter_Claim_Type__c,Filter_Object__c,Filter_Claim_Year__c,Filter_Document_Type__c,Filter_Global_Client__c,Filter_Global_Client_Id__c 
                   from Extranet_Favourite__c where Id=:favId LIMIT 1];
        return lstFavs; 
    }
    
    public static Extranet_Favourite__c createFavs(Extranet_Favourite__c objFav,String pageName,String itemName,String itemType,String currentUserId,String query,List<Extranet_Favourite__c> listExisting){        
        system.debug('pageName:' +pageName);
        system.debug('itemName:' +itemName);
        system.debug('listExisting:' +listExisting);
        objFav.Item_Id__c = pageName;
        objFav.Item_Type__c = itemType;
        objFav.Favourite_By__c = currentUserId;        
        objFav.Item_Name__c = itemName;
        objFav.Saved_Query__c = query;
        User usr = [Select Id, contactID, Name from User Where Id =: UserInfo.getUserId()];//Added For Favourites(Multiple Client)
        AccountToContactMap__c acmCS = AccountToContactMap__c.getValues(usr.contactID);//Added For Favourites(Multiple Client)
        objFav.Favourite_For_Broker_Client__c = acmCS.AccountId__c;//Added For Favourites(Multiple Client)
        return objFav;  
    }
    
    public static String generateFavFilters(List<String> listItems){    
        String filterField='';          
        if(listItems!=null && listItems.size()>0){
            Integer counter = 0;                    
            for(String item:listItems){
                if(counter == 0){
                    filterField = item;
                }else{
                    filterField = filterField + '#' + item;
                }
                counter = counter + 1;  
            }
        }       
        return filterField; 
    }
    
    public static List<String> fetchFavFilters(String item){    
        List<String> listItems=new List<String>();  
        if(item!=null){
            if(item.contains('#')){
                listItems.addAll(item.split('#'));      
            }else{
                listItems.add(item);    
            }   
        }
        return listItems;   
    }
    //Util Methods To Be Called from other Classes - End

}