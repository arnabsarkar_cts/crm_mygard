public class RedirectUserCtrl
{
public string currentUrl{get;set;}
public boolean isPortal{get;set;}{isPortal = false;}
    public void RedirectUserToHome()
    {
        User u = [select id, IsPortalEnabled from User where id=:userInfo.getUserId()];
        //ApexPages.CurrentPage().getParameters().put('X-Frame-Options','Allow');
        if(u.IsPortalEnabled)
        {
            currentUrl = ApexPages.currentPage().getHeaders().get('Referer');
            system.debug('**********currentUrl: '+currentUrl );
            isPortal=true;
          //  return page.HomePage;
        }
        //return null;
    }
}