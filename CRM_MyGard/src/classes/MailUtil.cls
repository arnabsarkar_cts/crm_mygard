public class MailUtil {
	public static Messaging.SendEmailResult[] sendAccountSyncFailureMail(List<Account> failedAccounts){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
    	List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>(); 
    	list<String> sendToList= new list<String>();
    	list<String> idOfAdmin= new list<String>();
    	
    	String bodyContent='<html><body>Outbound sync has failed for Account(s) with record Id(s)<br/>';
    	Group g = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name = 'Gard Administrators'];
       
    	for (GroupMember gm : g.groupMembers){
    		idOfAdmin.add(gm.userOrGroupId);
   	 	}
      
    	User[] usr = [SELECT email FROM user WHERE id IN :idOfAdmin];
    
    	for(User u : usr) {
    		sendToList.add(u.email);
    	}
    	for(Account aFailedAccount : failedAccounts){
        	String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + aFailedAccount.Id;
        	bodyContent+='<a href="'+fullRecordURL+'">'+aFailedAccount.Id+'</a><br/>';
        } 
        
        bodyContent+='<br/>Synchronisation Status is Sync Failed<br/></body>';
        mail.setToAddresses(sendToList);
    	mail.setSubject('Account outbound sync failure notification'); 
    	mail.setHtmlBody(bodyContent);
    	mails.add(mail);
    	Messaging.SendEmailResult[] emailResult = Messaging.sendEmail(mails);
    	if(test.isRunningTest()){
    		return emailResult;
    	}else{
    		return null;
    	}
	}
	
	public static Messaging.SendEmailResult[] sendContactSyncFailureMail(List<Contact> failedContacts){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
    	List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>(); 
    	list<String> sendToList= new list<String>();
    	list<String> idOfAdmin= new list<String>();
    	
    	String bodyContent='<html><body>Outbound sync has failed for Contact(s) with record Id(s)<br/>';
    	Group g = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name = 'Gard Administrators'];
       
    	for (GroupMember gm : g.groupMembers){
    		idOfAdmin.add(gm.userOrGroupId);
   	 	}
      
    	User[] usr = [SELECT email FROM user WHERE id IN :idOfAdmin];
    
    	for(User u : usr) {
    		sendToList.add(u.email);
    	}
    	for(Contact aFailedContact : failedContacts){
        	String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + aFailedContact.Id;
        	bodyContent+='<a href="'+fullRecordURL+'">'+aFailedContact.Id+'</a><br/>';
        } 
        bodyContent+='<br/>Synchronisation Status is Sync Failed<br/></body>';
        
        mail.setToAddresses(sendToList);
    	mail.setSubject('Outbound Sync Failure - Contact'); 
    	mail.setHtmlBody(bodyContent);
    	mails.add(mail);
    	Messaging.SendEmailResult[] emailResult = Messaging.sendEmail(mails);
    	if(test.isRunningTest()){
    		return emailResult;
    	}else{
    		return null;
    	}
	}
}