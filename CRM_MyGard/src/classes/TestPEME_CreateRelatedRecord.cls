@isTest 
public class TestPEME_CreateRelatedRecord{
    //Constants
    private static String strAccount = 'Account';
    private static String strContact = 'Contact';
    //Variables
    private static GardTestData gtd;
    
    //Test Setup Methods
    private static void createTestData(){
        gtd = new GardTestData();
        gtd.PEMEClinicSubmitInvoiceCtrl();
    }
    
    //Test Methods
    @isTest public static void testWithDebitAccount(){
        createTestData();
        PEME_CreateRelatedRecord.getParentRecord(GardTestData.pdn.id,strAccount);
    }
    @isTest public static void testWithManningAccount(){
        createTestData();
        PEME_CreateRelatedRecord.getParentRecord(GardTestData.pmg.id,strAccount);
    }
    @isTest public static void testWithManningContact(){
        createTestData();
        PEME_CreateRelatedRecord.getParentRecord(GardTestData.pmg.id,strContact);
    }
}