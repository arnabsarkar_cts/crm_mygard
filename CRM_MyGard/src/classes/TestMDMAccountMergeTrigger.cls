@isTest
private class TestMDMAccountMergeTrigger {
    private static void setUpAccount(){
        Account anAccount = new Account(  
                    Name = 'Test Account',
                    BillingCity = 'Bristol',
                    BillingCountry = 'United Kingdom',
                    BillingPostalCode = 'BS1 1AD',
                    BillingState = 'Avon' ,
                    BillingStreet = '1 Elmgrove Road',
                    Company_Role__c = 'Client',
                    Area_Manager__c = UserInfo.getUserId()
        );
        insert anAccount;
    }
    
    static testMethod void testMerge(){
        setUpAccount();
        Account anAccount = [SELECT ID,Company_ID__c FROM ACCOUNT WHERE Name = 'Test Account'];
        MDM_Account_Merge__c mm = new MDM_Account_Merge__c(From_Account__c=anAccount.Id,To_Account__c = anAccount.Id);
        insert mm;
        
        MDM_Account_Merge__c fetchedMM = [SELECT ID, To_Company_ID__c from MDM_Account_Merge__c WHERE To_Account__c = :anAccount.Id];
        
        System.assertEquals(fetchedMM.To_Company_ID__c,anAccount.Company_ID__c);
    }
}