@isTest(seealldata=false)
public class TestContractReviewLOCCtrl1{
    private static string testStr =  'test'; 
    private static GardTestData testRec;
    private static Attachment attach;
    
    //Create test data
    private static void createTestData(){
        testRec = new GardTestData();
        testRec.customsettings_rec();
        testRec.commonrecord();
        testRec.ViewDoc_testrecord();
        GardtestData.test_object_1st.guid__c = '8ce8ac89-a6fd-1836-9e07';
        update GardtestData.test_object_1st; 
        attach=new Attachment();
        attach.name='Unit Test Attachment';
        attach.body=Blob.valueOf('Unit Test Attachment Body');
        attach.parentId= GardTestData.brokerCase.id;
        attach.ContentType = 'application/pdf';
        insert attach;
        
        External_Email_Comments__c ExtEmailCom = new External_Email_Comments__c(
            Case__c = GardTestData.brokerCase.id,
            Comment__c = 'text'
        );
        insert ExtEmailCom;
    }
    
    //Test methods
    @isTest public static void contractReviewTest1(){
        createTestData();
        System.runAs(GardTestData.brokerUser){
            Test.startTest();
            ContractReviewLOCCtrl cr = new ContractReviewLOCCtrl();
            cr.imoNo = 12;
            cr.noImoNo = true;
            cr.lstObject =new List<Object__c>();
            cr.contractName =  testStr ;
            cr.isConfidential = true;
            cr.isLocReqd = true;
            cr.comments =  testStr ;
            cr.lOCType =  testStr ;
            cr.objMode = true;
            cr.prvUrl =  testStr ;
            cr.isFav = true;
            cr.newFilterName =  testStr ;
            ApexPages.StandardController scontroller = new ApexPages.StandardController(GardTestData.brokerCase);
            ContractReviewLOCCtrl MyObject=new ContractReviewLOCCtrl(scontroller);
            cr.selectedCover = GardTestData.brokerAsset_2nd.id;
            cr.selectedClient = GardTestData.clientAcc.id;
            cr.strSelected = 'ABc';
            cr.isPhone = true;
            cr.caseIns.OwnerId = GardTestData.brokerUser.id;
            cr.caseIns.accountId = GardTestData.clientAcc.id;
            cr.caseIns.Type = 'Crew Contract Review';
            cr.caseIns.Name_Of_Contract__c =  testStr ;
            cr.caseIns.Status = 'DM Synchronising';
            List<SelectOption> Countrycode= cr.getCountrycodes();
            List<SelectOption> ClientOp= cr.getClientLst();
            List<SelectOption> CoverOp= cr.getCoverLst();
            /*
ApexPages.CurrentPage().getParameters().put('id',GardTestData.brokerCase.id);
cr.ws_ConfirmationRequest();
ApexPages.CurrentPage().getParameters().put('id',GardTestData.clientCase.id);
cr.previewDocWS();
List<SelectOption> TypeOfLoc= cr.getTypeOfLocLst();
ApexPages.CurrentPage().getParameters().put('id',GardTestData.brokerCase.id);
cr.reDirectToCase();
cr.relatedLstDocs = new List<gardNoDm.DocumentBase>();
cr.displayDoc();
cr.retrieveCoverLst();
cr.selectedCover = GardTestData.brokerAsset_1st.id;
cr.retrieveAssetLst();
List<SelectOption> ObjectOp= cr.getObjectLst();
cr.selectedMultiObj.add(GardTestData.test_object_1st.id);
cr.attRowNum = 1;
cr.caseIns.id= GardTestData.brokerCase.id;
cr.addDocuments();
cr.lstAttachments.add(GardTestData.attachmnt);
cr.previewDocWS();
case c = [select id,type,contact_preference__c from case where id =: GardTestData.brokerCase.id];
c.type = 'Crew Contract Review';
c.contact_preference__c = 'Email;Phone';
update c;
ContractReviewLOCCtrl.ContractReviewRequestWS(GardTestData.brokerCase.id);
cr.LOCSendEmailRequestWS();
cr.fetchSelectedClients();
cr.sendMails();
System.assertEquals(cr.isPhone , true);
cr.fetchSelectedClients();
ContractReviewLOCCtrl.changeOwner_WS(GardTestData.brokerUser.id,'Abc');
ContractReviewLOCCtrl.ShowDocs sd = new ContractReviewLOCCtrl.ShowDocs();
ContractReviewLOCCtrl.startReview(GardTestData.brokerCase.id);
ContractReviewLOCCtrl.completeReview(GardTestData.brokerCase.id);
*/
            Test.stopTest();
        }
        System.runAs(GardTestData.clientUser)        
        {
            //  ContractReviewLOCCtrl cr = new ContractReviewLOCCtrl();
        }
    }
    
    @isTest private static void contractReviewTest2(){
        createTestData();
        System.runAs(GardTestData.brokerUser){
            Test.startTest();
            ContractReviewLOCCtrl cr = new ContractReviewLOCCtrl();
            cr.imoNo = 12;
            cr.noImoNo = true;
            cr.lstObject =new List<Object__c>();
            cr.contractName =  testStr ;
            cr.isConfidential = true;
            cr.isLocReqd = true;
            cr.comments =  testStr ;
            cr.lOCType =  testStr ;
            cr.objMode = true;
            cr.prvUrl =  testStr ;
            cr.isFav = true;
            cr.newFilterName =  testStr ;
            ApexPages.StandardController scontroller = new ApexPages.StandardController(GardTestData.brokerCase);
            ContractReviewLOCCtrl MyObject=new ContractReviewLOCCtrl(scontroller);
            cr.selectedCover = GardTestData.brokerAsset_2nd.id;
            cr.selectedClient = GardTestData.clientAcc.id;
            cr.strSelected = 'ABc';
            cr.isPhone = true;
            cr.caseIns.OwnerId = GardTestData.clientUser.id;
            cr.caseIns.accountId = GardTestData.clientAcc.id;
            cr.caseIns.Type = 'Crew Contract Review';
            cr.caseIns.Name_Of_Contract__c =  testStr ;
            cr.caseIns.Status = 'DM Synchronising';
            ApexPages.CurrentPage().getParameters().put('id',GardTestData.brokerCase.id);
            cr.ws_ConfirmationRequest();
            ApexPages.CurrentPage().getParameters().put('id',GardTestData.clientCase.id);
            //ApexPages.CurrentPage().getParameters().put('id',GardTestData.brokerCase.id);
            cr.previewDocWS();
            List<SelectOption> TypeOfLoc= cr.getTypeOfLocLst();
            ApexPages.CurrentPage().getParameters().put('id',GardTestData.brokerCase.id);
            cr.reDirectToCase();
            cr.relatedLstDocs = new List<gardNoDm.DocumentBase>();
            cr.displayDoc();
            cr.retrieveCoverLst();
            cr.selectedCover = GardTestData.brokerAsset_1st.id;
            cr.retrieveAssetLst();
            List<SelectOption> ObjectOp= cr.getObjectLst();
            cr.selectedMultiObj.add(GardTestData.test_object_1st.id);
            cr.attRowNum = 1;
            cr.caseIns.id= GardTestData.brokerCase.id;
            cr.addDocuments();
            cr.lstAttachments.add(GardTestData.attachmnt);
           
            //cr.previewDocWS();
            //Test.stopTest();
            case c = [select id,type,contact_preference__c from case where id =: GardTestData.brokerCase.id];
            c.type = 'Crew Contract Review';
            c.contact_preference__c = 'Email;Phone';
            update c;
            ContractReviewLOCCtrl.ContractReviewRequestWS(GardTestData.brokerCase.id);
            cr.LOCSendEmailRequestWS();
            cr.fetchSelectedClients();
            cr.sendMails();
            System.assertEquals(cr.isPhone , true);
            cr.fetchSelectedClients();
            ContractReviewLOCCtrl.changeOwner_WS(GardTestData.brokerUser.id,'Abc');
            ContractReviewLOCCtrl.ShowDocs sd = new ContractReviewLOCCtrl.ShowDocs();
            ContractReviewLOCCtrl.startReview(GardTestData.brokerCase.id);
            ContractReviewLOCCtrl.completeReview(GardTestData.brokerCase.id);
            Test.stopTest();
        }
    }
}