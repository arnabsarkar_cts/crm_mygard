//Generated by wsdl2apex

public class messagesGardNoV10Locsendemailinput {
    public class LOCSendEmailInputBase {
        public String EmailTo;
        public String EmailFrom;
        public String CaseNo;
        public String ClaimHandlerUserId;
        public String LocDocNumber;
        public messagesGardNoV10Locsendemailinput.ClientType Client;
        public String ReferenceNumber;
        public messagesGardNoV10Locsendemailinput.CoverType Cover;
        public messagesGardNoV10Object.ObjectBase[] Object_x;
        public Date ContractDate;
        public Date EndDate;
        public Boolean Confidential;
        public String Comments;
        public messagesGardNoV10Locsendemailinput.ContactPreference_element ContactPreference;
        public String ContractSubmitterName;
        public String ContractSubmitterEmail;
        public String ContractSubmitterPhone;
        public String ContractSubmitterMobile;
        public messagesGardNoV10Locsendemailinput.CommentType[] EmailComment;
        public String NameOfContract;
        public messagesGardNoV10Locsendemailinput.GardContact_element GardContact;
        private String[] EmailTo_type_info = new String[]{'EmailTo','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] EmailFrom_type_info = new String[]{'EmailFrom','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] CaseNo_type_info = new String[]{'CaseNo','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] ClaimHandlerUserId_type_info = new String[]{'ClaimHandlerUserId','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] LocDocNumber_type_info = new String[]{'LocDocNumber','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] Client_type_info = new String[]{'Client','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] ReferenceNumber_type_info = new String[]{'ReferenceNumber','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] Cover_type_info = new String[]{'Cover','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] Object_x_type_info = new String[]{'Object','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','-1','false'};
        private String[] ContractDate_type_info = new String[]{'ContractDate','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] EndDate_type_info = new String[]{'EndDate','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] Confidential_type_info = new String[]{'Confidential','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] Comments_type_info = new String[]{'Comments','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] ContactPreference_type_info = new String[]{'ContactPreference','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] ContractSubmitterName_type_info = new String[]{'ContractSubmitterName','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] ContractSubmitterEmail_type_info = new String[]{'ContractSubmitterEmail','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] ContractSubmitterPhone_type_info = new String[]{'ContractSubmitterPhone','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] ContractSubmitterMobile_type_info = new String[]{'ContractSubmitterMobile','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] EmailComment_type_info = new String[]{'EmailComment','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','-1','false'};
        private String[] NameOfContract_type_info = new String[]{'NameOfContract','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] GardContact_type_info = new String[]{'GardContact','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://messages.gard.no/v1_0/LOCSendEmailInput','true','false'};
        private String[] field_order_type_info = new String[]{'EmailTo','EmailFrom','CaseNo','ClaimHandlerUserId','LocDocNumber','Client','ReferenceNumber','Cover','Object_x','ContractDate','EndDate','Confidential','Comments','ContactPreference','ContractSubmitterName','ContractSubmitterEmail','ContractSubmitterPhone','ContractSubmitterMobile','EmailComment','NameOfContract','GardContact'};
    }
    public class ContactPreference_element {
        public Boolean email;
        public Boolean telephone;
        private String[] email_type_info = new String[]{'email','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] telephone_type_info = new String[]{'telephone','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://messages.gard.no/v1_0/LOCSendEmailInput','true','false'};
        private String[] field_order_type_info = new String[]{'email','telephone'};
    }
    public class ClientType {
        public String Client;
        public String ClientId;
        private String[] Client_type_info = new String[]{'Client','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] ClientId_att_info = new String[]{'ClientId'};
        private String[] apex_schema_type_info = new String[]{'http://messages.gard.no/v1_0/LOCSendEmailInput','true','false'};
        private String[] field_order_type_info = new String[]{'Client'};
    }
    public class CommentType {
        public String CommentName;
        public String CommentText;
        private String[] CommentName_type_info = new String[]{'CommentName','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] CommentText_type_info = new String[]{'CommentText','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://messages.gard.no/v1_0/LOCSendEmailInput','true','false'};
        private String[] field_order_type_info = new String[]{'CommentName','CommentText'};
    }
    public class CoverType {
        public String Cover;
        public String CoverId;
        private String[] Cover_type_info = new String[]{'Cover','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] CoverId_att_info = new String[]{'CoverId'};
        private String[] apex_schema_type_info = new String[]{'http://messages.gard.no/v1_0/LOCSendEmailInput','true','false'};
        private String[] field_order_type_info = new String[]{'Cover'};
    }
    public class GardContact_element {
        public String Name;
        public String Title;
        public String Department;
        public String Company;
        public String Phone;
        public String Mobile;
        public String SwitchBoardNumber;
        private String[] Name_type_info = new String[]{'Name','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] Title_type_info = new String[]{'Title','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] Department_type_info = new String[]{'Department','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] Company_type_info = new String[]{'Company','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'1','1','false'};
        private String[] Phone_type_info = new String[]{'Phone','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] Mobile_type_info = new String[]{'Mobile','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] SwitchBoardNumber_type_info = new String[]{'SwitchBoardNumber','http://messages.gard.no/v1_0/LOCSendEmailInput',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://messages.gard.no/v1_0/LOCSendEmailInput','true','false'};
        private String[] field_order_type_info = new String[]{'Name','Title','Department','Company','Phone','Mobile','SwitchBoardNumber'};
    }
}