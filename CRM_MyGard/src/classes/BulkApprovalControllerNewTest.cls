@isTest
public class BulkApprovalControllerNewTest {

    //TODO: this is just a mock test to touch as much code as possible.... 
        public static Opportunity opptest;
        Public static String strRecId = [SELECT Id FROM RecordType WHERE Name='P&I' AND sObjectType='Opportunity' LIMIT 1].Id;
        Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    static testMethod void myUnitTest() {
        User userTest = TestDataGenerator.getAdminUser() ;
        Account acc = TestDataGenerator.getApprovedMemberAccount();
        opptest = new Opportunity();
        opptest.RecordTypeId = strRecId;
        opptest.Name = 'APEXTESTOPP001';
        opptest.AccountId = acc.Id;
        opptest.StageName = 'Renewable Opportunity';
        opptest.Type = 'New Business';
        opptest.Sales_Channel__c = 'Direct';
        opptest.CloseDate = Date.Today();
        opptest.Budget_Status__c = 'Budget In Progress';
        opptest.Budget_Year__c = '2013';
        opptest.OwnerId = userTest.id ;
        insert opptest;
    system.runAs(userTest){
        
        
        Opportunity theOpp = TestDataGenerator.getOpportunity('Marine', 'Risk Evaluation', 'Hull & Machinery');
        theOpp.Budget_Status__c = 'Budget In Progress';
        BulkApprovalControllerNew ctrl = new BulkApprovalControllerNew(new ApexPages.StandardController(opptest));
        
        ctrl.selectAllChecked = true;
        //selectedOpps = new List<>
        ctrl.selectAll();
        ctrl.selectAllChecked = false;
        ctrl.selectAll();
        ctrl.selectedOppsPageCount = 5.0;
        ctrl.selectedOppsPageNum = 2.0;
        ctrl.prev();
        ctrl.next();
        ctrl.selectedOppsPageNum = 2.0;
        ctrl.selectedOppsPageCount = 4.0;
        ctrl.getSelOppsPage();
        
        BulkApprovalControllerNew.opp opp = new BulkApprovalControllerNew.opp();
        boolean b = opp.checked;
        string s = opp.id;
        s = opp.AccountName;
        s = opp.Gard_Market_Area;
        s = opp.Budget_Status;
        s = opp.RecordType;
        s = opp.Name;
        decimal d = opp.Actual_Premium;
        d = opp.Budget_Difference;
        d = opp.Renewable_Premium;
        d = opp.Record_Adjustment;
        d = opp.Volume_Change;
        d = opp.Renewal_Probability;
        d = opp.Forecast_Budget;
        d = opp.APproved_Budget;
        d = opp.BudgetPremium;
        s = opp.type;
        s = opp.currencyISOCode;
        
        s = ctrl.budgetYear;
        ctrl.budgetYear = '2001';
        
        ctrl.selectAllChecked = true;
        ctrl.selectedOpps = new List<BulkApprovalControllerNew.opp>();
        ctrl.selectedOpps.add(opp);
        ctrl.selectAll();
        ctrl.selectAllChecked = false;
        ctrl.selectedOpps = new List<BulkApprovalControllerNew.opp>();
        ctrl.selectedOpps.add(opp);
        ctrl.selectAll();
        
        ctrl.getSelOppsPage();
        ctrl.userid = userTest.id;
        system.debug('just for testing -->>'+opptest);
        system.debug('just for testing12345 -->>'+[Select Id, Name, budget_status__c, StageName, Budget_Year__c, OwnerId from Opportunity where name = 'APEXTESTOPP001']+userTest.id);
        ctrl.populateOpps();
        
        ctrl.submitAll();
        ctrl.submitSelected();
        
        List<Opportunity> opps = new List<Opportunity>();
        opps.add(theOpp);
        ctrl.submitOpps(opps);
        ctrl.OppPage();
        
        List<ID> recipients = new List<ID>();
        recipients.add('005D0000003kHEQIA2');
        ctrl.sendEmailNotification(recipients);
        }
    }
}