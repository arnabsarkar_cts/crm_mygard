@isTest
private class TestPreventContactTrigger {
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Market_Area__c Markt;
    public static User salesforceLicUser;
    public static Account clientAcc;
    public static Contact employedContact;
    public static Contact nonEmployedContact;
    public static Event eventWithEmployedContact;
    public static Event eventWithNonEmployedContact; 
    public static Event testEvent; 
    public static string mailngStateStr = 'London';
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    private static void createUsers(){
        salesforceLicUser = new User(
            Alias = 'standt', 
            profileId = salesforceLicenseId ,
            Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8',
            CommunityNickname = 'test13',
            LastName='Testing',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',  
            TimeZoneSidKey='America/Los_Angeles',
            UserName='test008@testorg.com'
        );
        
        insert salesforceLicUser; 
    }
    
    private static void createClientCompany(){
        Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt; 
        createUsers();        
        
        clientAcc = new Account( Name = 'Test_1', 
                                Site = '_www.test_1.se', 
                                Type = 'Client', 
                                BillingStreet = 'Gatan 1',
                                BillingCity = 'Stockholm', 
                                BillingCountry = 'SWE', 
                                BillingPostalCode = 'BS1 1AD',
                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                Company_Role_Text__c='Client',
                                Market_Area__c = Markt.id,
                                Area_Manager__c = salesforceLicUser.id,
                                OwnerId = salesforceLicUser.id
                               );
        insert clientAcc;
    }
    
    private static void createContacts(Account createdAccount){
        List<Contact> allContacts = new List<Contact>();
        
        employedContact= new Contact( FirstName='primary_1',
                                     LastName='chak',
                                     MailingCity =  mailngStateStr ,
                                     MailingCountry = 'United Kingdom',
                                     MailingPostalCode = 'SE1 1AE',
                                     MailingState =  mailngStateStr ,
                                     MailingStreet = '4 London Road',
                                     AccountId = createdAccount.Id,
                                     Email = 'test321@gmail.com',
                                     Primary_Contact__c = true,
                                     Publication_Member_circulars__c=false,
                                     Publications_Gard_Rules__c=false,
                                     Publication_Guidance_to_Master__c=false,
                                     No_Longer_Employed_by_Company__c=false
                                    );
        allContacts.add(employedContact);                       
        nonEmployedContact= new Contact( FirstName='nonPrimary_1',
                                        LastName='chak',
                                        MailingCity =  mailngStateStr ,
                                        MailingCountry = 'United Kingdom',
                                        MailingPostalCode = 'SE1 1AE',
                                        MailingState =  mailngStateStr ,
                                        MailingStreet = '4 London Road',
                                        AccountId = createdAccount.Id,
                                        Email = 'test156@gmail.com',
                                        Publication_Member_circulars__c=false,
                                        Publications_Gard_Rules__c=false,
                                        Publication_Guidance_to_Master__c=false,
                                        No_Longer_Employed_by_Company__c=true
                                       );
        allContacts.add(nonEmployedContact);
        
        insert allContacts;
    }
    
    
    private static void createClientCompanyWithContacts(){
        createClientCompany();
        createContacts(clientAcc);
    }
    
    static testMethod void testEventOnContactEmployedAndNonEmployed(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        TestPreventContactTrigger.createClientCompanyWithContacts();
        Account fetchedAccount = [SELECT ID,Name FROM ACCOUNT WHERE Name = 'Test_1' LIMIT 1];
        Contact fetchedEmployedContact = [SELECT ID, No_Longer_Employed_by_Company__c  FROM CONTACT
                                          WHERE AccountId =: fetchedAccount.Id AND FirstName='primary_1' LIMIT 1];
        Contact fetchedNonEmployedContact = [SELECT ID, No_Longer_Employed_by_Company__c FROM CONTACT
                                             WHERE AccountId =: fetchedAccount.Id AND FirstName='nonPrimary_1' LIMIT 1];
                                             
        eventWithEmployedContact = new Event( Subject='TEST EVENT WITH EMPLOYED CONTACT', 
                                             StartDateTime=datetime.now(), 
                                             EndDateTime=datetime.now().addDays(1),
                                             WhoId = fetchedEmployedContact.id,
                                             WhatId = fetchedAccount.id);
        insert eventWithEmployedContact;
        Event fetchedEvent = [SELECT ID, WhoId, WhatId FROM EVENT WHERE Subject = 'TEST EVENT WITH EMPLOYED CONTACT'];
        System.assertEquals(fetchedEmployedContact.id, fetchedEvent.WhoId);
        System.assertEquals(fetchedAccount.id, fetchedEvent.WhatId);
        try{
            eventWithNonEmployedContact = new Event( Subject='TEST EVENT WITH NON EMPLOYED CONTACT', 
                                                    StartDateTime=datetime.now(), 
                                                    EndDateTime=datetime.now().addDays(1),
                                                    WhoId = fetchedNonEmployedContact.id,
                                                    WhatId = fetchedAccount.id);
            insert eventWithNonEmployedContact;
            testEvent = new Event( Subject='TEST EVENT WITH NON EMPLOYED CONTACT', 
                                                    StartDateTime=datetime.now(), 
                                                    EndDateTime=datetime.now().addDays(1),
                                                    WhoId = fetchedNonEmployedContact.id,
                                                    type = 'survey',
                                                    WhatId = fetchedAccount.id);
            insert testEvent;  //---------------------------------------------
            testEvent.EndDateTime = datetime.now().addDays(2);
            update testEvent;
        } catch (Exception e) { 
            Boolean expectedExceptionThrown =  e.getMessage().contains('You cannot create an event with invitees who are no longer employed by company')? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        
    } 
    
    static testMethod void testEventOnContactEmployedWithEventRelation(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        TestPreventContactTrigger.createClientCompanyWithContacts();
        Account fetchedAccount = [SELECT ID,Name FROM ACCOUNT WHERE Name = 'Test_1' LIMIT 1];
        Contact fetchedEmployedContact = [SELECT ID, No_Longer_Employed_by_Company__c  FROM CONTACT
                                          WHERE AccountId =: fetchedAccount.Id AND FirstName='primary_1' LIMIT 1];
                                          
        Contact fetchedNonEmployedContact = [SELECT ID, No_Longer_Employed_by_Company__c FROM CONTACT
                                             WHERE AccountId =: fetchedAccount.Id AND FirstName='nonPrimary_1' LIMIT 1];
                                             
                                           
            eventWithEmployedContact = new Event( Subject='TEST EVENT WITH EMPLOYED CONTACT', 
                                             StartDateTime=datetime.now(), 
                                             EndDateTime=datetime.now().addDays(1),
                                             WhoId = fetchedEmployedContact.id,
                                             WhatId = fetchedAccount.id);
            insert eventWithEmployedContact;
            Event fetchedEvent = [SELECT ID, WhoId, WhatId FROM EVENT WHERE Subject = 'TEST EVENT WITH EMPLOYED CONTACT'];
        
            EventRelation eventRelation1 = new EventRelation(RelationId = fetchedEmployedContact.id,
                                                        EventId = fetchedEvent.id);
            insert eventRelation1;
            EventRelation fetchedEventRelation = [SELECT ID, EventId FROM EventRelation WHERE RelationId =:fetchedEmployedContact.id ];
            System.assertEquals(fetchedEventRelation.EventId , fetchedEvent.id);
            //Test code
            Event fetchedEvent1 = [SELECT ID, WhoId, WhatId FROM EVENT WHERE Subject = 'TEST EVENT WITH EMPLOYED CONTACT'];
            fetchedEvent1.Subject='New';
            update fetchedEvent1;
           //Test Ends
         try{ 
            eventWithNonEmployedContact = new Event( Subject='TEST EVENT WITH NON-EMPLOYED CONTACT', 
                                             StartDateTime=datetime.now(), 
                                             EndDateTime=datetime.now().addDays(1),
                                             WhoId = fetchedNonEmployedContact.id,
                                             WhatId = fetchedAccount.id);
            insert eventWithNonEmployedContact;
            Event fetchedEventWithNonEmployedContact = [SELECT ID, WhoId, WhatId FROM EVENT WHERE Subject = 'TEST EVENT WITH NON-EMPLOYED CONTACT'];
        
            EventRelation eventRelation2 = new EventRelation(RelationId = fetchedNonEmployedContact.id,
                                                        EventId = fetchedEventWithNonEmployedContact.id);
            insert eventRelation2;
            }catch(Exception e) { 
            Boolean expectedExceptionThrown =  e.getMessage().contains('You cannot create an event with invitees who are no longer employed by company')? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        
    } 
}