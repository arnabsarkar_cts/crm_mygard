public with sharing class PrognosisMap {
	private static Map<String, Map<String, String>> testprogmap = null;
	//Get the Prognosis_Map__c list setting as a map of maps
	public static Map<String, Map<String, String>> GetPrognosisMap(){
		if(Test.isRunningTest() && testprogmap != null ){return testprogmap;			
		}else{
			testprogmap = new Map<String, Map<String, String>>();
			Map<String, Prognosis_Map__c> progMap = Prognosis_Map__c.getAll();
			System.Debug('*** Prognosis Settings Map = ' + progMap);
			System.Debug('*** Prognisis Settings KeySet = ' + progMap.keySet());
			for(String progField : progMap.keySet()){
				System.Debug('*** Prognosis Field = ' + progField);
				//run through the returned map and make the outer key the object name
				if(testprogmap.containsKey( progMap.get(progField).Object__c)){
					//append to the field map
					testprogmap.get(progMap.get(progField).Object__c).put(progMap.get(progField).Name, progMap.get(progField).Field__c);
				}else{
					Map<String, String> newObjMap = new Map<String, String>();
					newObjMap.put(progMap.get(progField).Name, progMap.get(progField).Field__c);
					testprogmap.put(progMap.get(progField).Object__c, newObjMap);
				}
			}
			return testprogmap;
		}
	}
}