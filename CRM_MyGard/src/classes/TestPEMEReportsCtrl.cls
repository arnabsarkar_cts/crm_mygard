@isTest(seeAllData=false)
public class TestPEMEReportsCtrl
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static List<Object__c> ObjectList=new List<Object__c>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    public static List<PEME_Report__c> prList=new List<PEME_Report__c>();
    public static Attachment attach;
    public static string typeSubBrStr = 'ESP - Agent'; 
    private static final String strClinic = 'Clinic';
    Static testMethod void cover(){
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab12000');
        insert Markt;
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c ='Test123@gmail.com');
        insert cvar;
        User user = new User(
            Alias = 'standt', 
            profileId = salesforceLicenseId ,
            Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8',
            CommunityNickname = 'test13',
            LastName='Testing',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',  
            TimeZoneSidKey='America/Los_Angeles',
            UserName='test008@testorg.com',
            ContactId__c = gc.id
        );
        insert user ;
        User user1 =new User();
        user1 = [select id, contactid__c from user where id=:user.id];
        /* Account brokerAcc = new Account( Name='testre',
            BillingCity = 'Bristol',
            BillingCountry = 'United Kingdom',
            BillingPostalCode = 'BS1 1AD',
            BillingState = 'Avon' ,
            BillingStreet = '1 Elmgrove Road',
            recordTypeId=System.Label.Broker_Contact_Record_Type,
            Site = '_www.cts.se',
            Type = 'Broker' ,
            Area_Manager__c = user1.id,      
            Market_Area__c = Markt.id                                                                    
            );
            AccountList.add(brokerAcc);
            Account clientAcc= New Account();
            clientAcc.Name = 'Testt';
            clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
            clientAcc.Site = '_www.test.se';
            clientAcc.Type = 'Customer';
            clientAcc.BillingStreet = 'Gatan 1';
            clientAcc.BillingCity = 'Stockholm';
            clientAcc.BillingCountry = 'SWE';    
            clientAcc.Area_Manager__c = user1.id;        
            clientAcc.Market_Area__c = Markt.id;    
            AccountList.add(clientAcc); */
        Account clinicAcc = new Account( Name='testClinic',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.ESP_Contact_Record_Type,
                                        company_Role__c='External Service Provider',
                                        Site = '_www.ctsclinic.se',
                                        Type = 'Surveyor' ,
                                        Area_Manager__c = user1.id,      
                                        Market_Area__c = Markt.id,
                                        Sub_Roles__c = typeSubBrStr,
                                        PEME_Enrollment_Status__c='Enrolled'                                                                  
                                       );
        AccountList.add(clinicAcc);
        insert AccountList; 
        /* Contact brokerContact = new Contact( 
            FirstName='Raan',
            LastName='Baan',
            MailingCity = 'London',
            MailingCountry = 'United Kingdom',
            MailingPostalCode = 'SE1 1AD',
            MailingState = 'London',
            MailingStreet = '1 London Road',
            AccountId = brokerAcc.Id,
            Email = 'test321@gmail.com'
            );
            ContactList.add(brokerContact) ;
            Contact clientContact= new Contact( FirstName='tsat_1',
            LastName='chak',
            MailingCity = 'London',
            MailingCountry = 'United Kingdom',
            MailingPostalCode = 'SE1 1AE',
            MailingState = 'London',
            MailingStreet = '4 London Road',
            AccountId = clientAcc.Id,
            Email = 'test321@gmail.com'
            );
            ContactList.add(clientContact); */
        Contact clinicContact = new Contact( 
            FirstName='medicalTest',
            LastName='medical',
            MailingCity = 'London',
            MailingCountry = 'United Kingdom',
            MailingPostalCode = 'SE1 1AD',
            MailingState = 'London',
            MailingStreet = '1 London Road',
            AccountId = clinicAcc.Id,
            Email = 'test321@gmail.com'
        );
        ContactList.add(clinicContact) ;
        insert ContactList;
        /* User brokerUser = new User(
            Alias = 'standt', 
            profileId = partnerLicenseId,
            Email='test120@test.com',
            EmailEncodingKey='UTF-8',
            LastName='estTing',
            LanguageLocaleKey='en_US',
            CommunityNickname = 'brokerUser',
            LocaleSidKey='en_US',  
            TimeZoneSidKey='America/Los_Angeles',
            UserName='testbrokerUser@testorg.com.mygard',
            ContactId = BrokerContact.Id
            );
            UserList.add(brokerUser);
            User clientUser = new User(Alias = 'alias_1',
            CommunityNickname = 'clientUser',
            Email='test_11@testorg.com', 
            profileid = partnerLicenseId, 
            EmailEncodingKey='UTF-8',
            LastName='tetst_006',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Los_Angeles',
            UserName='testclientUser@testorg.com.mygard',
            ContactId = clientContact.id
            );
            
            UserList.add(clientUser); */
        User clinicUser = new User(
            Alias = 'Clinic', 
            profileId = partnerLicenseId,
            Email='testClinic120@test.com',
            EmailEncodingKey='UTF-8',
            LastName='ClinicTesting',
            LanguageLocaleKey='en_US',
            CommunityNickname = 'clinicUser',
            LocaleSidKey='en_US',  
            TimeZoneSidKey='America/Los_Angeles',
            UserName='testClinicUser@testorg.com.mygard',
            ContactId = clinicContact.Id
        );
        UserList.add(clinicUser);
        insert UserList;
        AccountToContactMap__c acmCS = new AccountToContactMap__c(name = (clinicContact.id+''),AccountId__c = (clinicAcc.id+''),RolePreference__c = strClinic);
        insert acmCS;
        Account_Contact_Mapping__c acm = new Account_Contact_Mapping__c(
            contact__c = (clinicContact.id+''),Account__c = (clinicAcc.id+''),Active__c = true,
            Administrator__c = 'true', PI_Renewal_Access__c = true, IsPeopleClaimUser__c = true, PI_access__c = true, Marine_access__c = true, Read_Only__c = false
        );
        insert acm;
        PEME_Report__c prClinic=new PEME_Report__c();
        prClinic.Clinic__c=clinicAcc.ID;
        prClinic.Month__c='Dec';
        prClinic.Report_Name__c='BrokerTestReport';
        prClinic.Report_Type__c='Monthly';
        prClinic.Version__c='rt2015';
        prClinic.Year__c='2015';
        prList.add(prClinic);
        
        /* PEME_Report__c prClient=new PEME_Report__c();
            prClient.Clinic__c=clientAcc.ID;
            prClient.Month__c='Dec';
            prClient.Report_Name__c='ClientTestReport';
            prClient.Report_Type__c='Monthly';
            prClient.Version__c='rts2015';
            prClient.Year__c='2015';
            prList.add(prClient); */
        insert prList;   
        attach = new Attachment();
        attach.name= 'test Attachment';
        attach.body=Blob.valueOf('test Attachment body');
        attach.parentId= prClinic.id;
        attach.ContentType =   'application/pdf';
        Test.startTest();
        System.runAs(clinicUser)
        {
            PEMEReportsCtrl PEMEReportsCtrlClinic=new PEMEReportsCtrl();
            PEMEReportsCtrlClinic.attachment = attach;
            PEMEReportsCtrlClinic.noOfRecords = 0;
            PEMEReportsCtrlClinic.size = 0;
            PEMEReportsCtrlClinic.intStartrecord = 0;
            PEMEReportsCtrlClinic.intEndrecord = 0;
            PEMEReportsCtrlClinic.attachmentName = 'abc';
            PEMEReportsCtrlClinic.generateReports();  
            PEMEReportsCtrlClinic.getSortDirection();
            PEMEReportsCtrlClinic.setSortDirection('ASC');
            PEMEReportsCtrlClinic.getReportTypeOptions();
            PEMEReportsCtrlClinic.getMonthOptions();
            PEMEReportsCtrlClinic.getYearOptions();
            PEMEReportsCtrlClinic.getMonthFilterOptions();
            PEMEReportsCtrlClinic.getYearFilterOptions();
            PEMEReportsCtrlClinic.createOpenUserList();
            PEMEReportsCtrlClinic.firstOpen();
            PEMEReportsCtrlClinic.hasPreviousOpen=true;  
            PEMEReportsCtrlClinic.previousOpen();
            PEMEReportsCtrlClinic.setpageNumberOpen();
            PEMEReportsCtrlClinic.hasNextOpen=false;
            PEMEReportsCtrlClinic.nextOpen();
            PEMEReportsCtrlClinic.lastOpen();
            PEMEReportsCtrlClinic.clearOptions();
            PEMEReportsCtrlClinic.newReportType='testReport';
            PEMEReportsCtrlClinic.newYear='22.12.2015';
            PEMEReportsCtrlClinic.reportName='testrepo';
            PEMEReportsCtrlClinic.reportVersion='testver';
            //PEMEReportsCtrlClinic.createNewReport();
            PEMEReportsCtrlClinic.newReportType='testReport';
            PEMEReportsCtrlClinic.newYear='22.12.2015';
            PEMEReportsCtrlClinic.reportName='testrepo';
            PEMEReportsCtrlClinic.reportVersion='testver';
            PEMEReportsCtrlClinic.sendNotificationEmail();
            PEMEReportsCtrlClinic.cancelReport();
            PEMEReportsCtrl.fetchQueueMembers('testQueue');
            PEMEReportsCtrlClinic.print();
            PEMEReportsCtrlClinic.exportToExcel();
            PEMEReportsCtrlClinic.cancelReport();
            PEMEReportsCtrlClinic.print();
            PEMEReportsCtrlClinic.exportToExcel();
        }
        Test.stopTest();
        /* System.runAs(clientUser)
            {
            PEMEReportsCtrl PEMEReportsCtrlClient=new PEMEReportsCtrl();
            PEMEReportsCtrlClient.generateReports();  
            PEMEReportsCtrlClient.getSortDirection();
            PEMEReportsCtrlClient.setSortDirection('ASC');
            PEMEReportsCtrlClient.getReportTypeOptions();
            PEMEReportsCtrlClient.getMonthOptions();
            PEMEReportsCtrlClient.getYearOptions();
            PEMEReportsCtrlClient.getMonthFilterOptions();
            PEMEReportsCtrlClient.getYearFilterOptions();
            PEMEReportsCtrlClient.createOpenUserList();
            PEMEReportsCtrlClient.firstOpen();
            PEMEReportsCtrlClient.hasPreviousOpen=true;  
            PEMEReportsCtrlClient.previousOpen();
            PEMEReportsCtrlClient.setpageNumberOpen();
            PEMEReportsCtrlClient.hasNextOpen=false;
            PEMEReportsCtrlClient.nextOpen();
            PEMEReportsCtrlClient.lastOpen();
            PEMEReportsCtrlClient.clearOptions();
            PEMEReportsCtrlClient.newReportType='testReport';
            PEMEReportsCtrlClient.newYear='22.12.2015';
            PEMEReportsCtrlClient.reportName='testrepo';
            PEMEReportsCtrlClient.reportVersion='testver';
            PEMEReportsCtrlClient.createNewReport();
            PEMEReportsCtrlClient.newReportType='testReport';
            PEMEReportsCtrlClient.newYear='22.12.2015';
            PEMEReportsCtrlClient.reportName='testrepo';
            PEMEReportsCtrlClient.reportVersion='testver';
            PEMEReportsCtrlClient.sendNotificationEmail();
            PEMEReportsCtrlClient.cancelReport();
            PEMEReportsCtrl.fetchQueueMembers('testQueue');
            PEMEReportsCtrlClient.print();
            PEMEReportsCtrlClient.exportToExcel();
            } */ 
    }
}