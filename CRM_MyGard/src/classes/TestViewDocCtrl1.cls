@isTest(seealldata=false)
public class TestViewDocCtrl1{

         //public static commonvariables__c com_var;
        Public static Object__c test_object;
        public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    	public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
        public static testmethod void ViewDocument(){
            
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;    
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonrecord();
        system.debug('***********Accmap_1 '+GardTestData.Accmap_1 );
        system.debug('***********Accmap_2 '+GardTestData.Accmap_2 );
        gtdInstance.ViewDoc_testrecord(); 
         GardtestData.test_object_1st.guid__c = '8ce8ac89-a6fd-1836-9e07';
         update GardtestData.test_object_1st; 
         test_object = New Object__c(     Dead_Weight__c= 20,
                         Object_Unique_ID__c = '1234lkoko',
                         Name='hennessey',
                         Object_Type__c='Tanker',
                         Object_Sub_Type__c='Accommodation Ship',
                         No_of_passenger__c=100,
                         No_of_crew__c=70,
                         Gross_tonnage__c=1000,
                         Length__c=20,
                         Width__c=20,
                         Depth__c=30,
                         Port__c='Alabama',
                         Signal_Letters_Call_sign__c='hellooo',
                         Classification_society__c='CS 1',
                         Flag__c='Albania',
                         Rebuilt__c='2014-01-01',
                       //  Imo_Lloyds_No__c='4355555',
                        // Dummy_Object_flag__c = 0,
                         Year_built__c='2013-01-01',
                         Dummy_Object_flag__c=0);
                     insert test_object;
        //com_var = new commonvariables__c(Name = 'ReplyToMailId',value__c='no-reply@gard.no'); 
        //insert com_var;         
        //Invoking methods for ViewDocCtrl for broker and client users......
        GardTestData.junc_object_client.Broker__c = GardTestData.brokerAcc.ID;
        //GardTestData.junc_object_client.Client__c = GardTestData.clientAcc.ID;
        update GardTestData.junc_object_client;       
        
        System.runAs(GardTestData.clientUser)             
    {
        Test.startTest();
        Pagereference pge1 = page.ViewDocument;             
        test.setcurrentpage(pge1);
        system.debug('GardtestData.test_object_1st.guid__c************'+GardtestData.test_object_1st.guid__c);
        pge1.getParameters().put('objid',GardtestData.test_object_1st.guid__c);
        insert new MyGardDocumentAvailability__c(IsAvailable__c = true,Name ='Object Details Document Tab');
        insert new MyGardDocumentAvailability__c(IsAvailable__c = true,Name ='My Portfolio Document Tab');
        ViewDocCtrl test_client= new ViewDocCtrl();
        test_client.selectedDocType.add(GardTestData.brokerDoc.Document_Type__c);            
        test_client.selectedCover.add('P&I Cover');            
        test_client.selectedProductArea.add('P&I');            
        test_client.selectedPolicyYr.add('2014');            
        test_client.selectedRisk.add('Y');            
        //test_client.selectedRisk.add('N');            
        test_client.selectedObj.add('Curia');
        system.assertequals(test_client.selectedDocType.size()>0,true,true);
        system.assertequals(test_client.selectedCover.size()>0,true,true);
        system.assertequals(test_client.selectedProductArea.size()>0,true,true);
        system.assertequals(test_client.selectedRisk.size()>0,true,true);
        system.assertequals(test_client.selectedObj.size()>0,true,true);
        test_client.first();
        test_client.last();
        //test_client.checkMode();
        apexpages.currentpage().getparameters().put('objid',GardTestData.test_object_1st.id);
       // test_client.populateDocuments();  
       test_client.displayDoc();   
        test_client.fetchFavourites();
        test_client.strSelected = GardTestData.brokerAcc.id;
        test_client.fetchSelectedClients();
        test_client.createSetIds();
        test_client.resetExceptionVar();
        Pagereference pge = page.ViewDocument;             
        pge.getParameters().put('objid',GardtestData.test_object_1st.guid__c);
        test.setcurrentpage(pge);
        ApexPages.currentPage().getParameters().put('favId',GardTestData.test_fav_for_doc.id); 
        ViewDocCtrl test_client1= new ViewDocCtrl();
        Test.stopTest();
    }
    }
}