/**
 * 28/5/13 SOC - Moved test class out of body of main class and
 * added cases to test secondary claims handler functionality
 */
@isTest(SeeAllData=false)
private class testProcessCompanyGardTeam {
    
    public static List<User> lstUsr;
    public static string str =  'Active';
    private static string getUserId(string sUsername) {
        string sRetVal = null;
        if (sUsername != '' && sUsername != null) {
            for (User usr: lstUsr) {
                if (usr.GIC_ID__c == sUsername) {sRetVal = usr.Id;}
                if ((usr.GIC_ID__c == '' || usr.GIC_ID__c == null) && usr.PARIS_ID__c == sUsername) {sRetVal = usr.Id;}
            }
        }
        return sRetVal;
    }

    
    
    static testmethod void runTestPARIS() {
        Test.startTest();
       AdminUsers__c setting = new AdminUsers__c();
            setting.Name = 'Number of users';
            setting.Value__c = 3;
            insert setting;
         Gard_Team__c gt= new Gard_Team__c(Name='Claims Support America',Region_code__c='ABCD');
        insert gt;
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Claims_Support_Team__c=gt.Id);
        insert country;
        Account accTest = new Account(Name='TestAcc',RecordTypeId=[Select Id from RecordType where Name = 'Client' LIMIT 1].Id, Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id, Market_Area__c = TestDataGenerator.getMarketArea().Id,Country__c=country.Id);
        Account accTestParis = new Account(Name='TestAccParisOnly',RecordTypeId=[Select Id from RecordType where Name = 'Client' LIMIT 1].Id, Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id, Market_Area__c = TestDataGenerator.getMarketArea().Id,Country__c=country.Id);
        insert(accTest);
        
        insert(accTestParis);
        string idAcc = accTest.Id;
        
        string idAccParis = accTestParis.Id;
        system.Assert(idAcc <> null);
        
        system.Assert(idAccParis <> null);
        User usrGIC = [Select Id, Name, GIC_ID__c, PARIS_ID__c from User where IsActive = True AND GIC_ID__c <> null AND PARIS_ID__c <> null LIMIT 1];

        User usrParis = [Select Id, Name, GIC_ID__c, PARIS_ID__c from User where IsActive = True AND GIC_ID__c <> null AND PARIS_ID__c <> null AND Id <> :usrGIC.Id LIMIT 1];
       // User usrGIC = [Select Id, Name, GIC_ID__c, PARIS_ID__c from User where IsActive = True AND GIC_ID__c <> null AND PARIS_ID__c <> null LIMIT 1];

        Admin_System_Company__c pscParis = new Admin_System_Company__c(Salesforce_Company__c=idAcc,Source_System__c = 'Paris',Company_Status__c =  str , On_Risk__c = true);
        Admin_System_Company__c pscParisOnly = new Admin_System_Company__c(Salesforce_Company__c=idAccParis,Source_System__c = 'Paris',Company_Status__c =  str , On_Risk__c = true);

        pscParis.Accounting__c = usrParis.PARIS_ID__c;
        pscParis.U_W_Assistant_1__c = usrParis.PARIS_ID__c;
        pscParis.U_W_Assistant_2__c = usrParis.PARIS_ID__c;
        pscParis.Claim_handler_Energy__c = usrParis.PARIS_ID__c;
        pscParis.Claim_handler_Marine__c = usrParis.PARIS_ID__c;
        pscParis.Claims_handler_Builders_Risk__c = usrParis.PARIS_ID__c;
        pscParis.Claim_handler_Cargo_Dry__c = usrParis.PARIS_ID__c;
        pscParis.Claim_handler_Cargo_Liquid__c = usrParis.PARIS_ID__c;
        pscParis.Claim_handler_CEP__c = usrParis.PARIS_ID__c;
        pscParis.Claim_handler_Charterers__c = usrParis.PARIS_ID__c;
        pscParis.Claim_handler_Crew__c = usrParis.PARIS_ID__c;
        pscParis.Claim_handler_Defence__c = usrParis.PARIS_ID__c;
        //Secondary claims handlers
        pscParis.Claim_handler_Energy_2__c = usrParis.PARIS_ID__c;
        pscParis.Claim_handler_Marine_2__c = usrParis.PARIS_ID__c;
        pscParis.Claims_handler_Builders_Risk_2__c = usrParis.PARIS_ID__c;
        //4/9/13 - PCI-000138 - Claim adjuster marine
        pscParis.Claim_adjuster_Marine__c = usrParis.PARIS_ID__c;
        insert(pscParis);
        
        pscParisOnly.Accounting__c = usrParis.PARIS_ID__c;
        pscParisOnly.U_W_Assistant_1__c = usrParis.PARIS_ID__c;
        pscParisOnly.U_W_Assistant_2__c = usrParis.PARIS_ID__c;
        pscParisOnly.Claim_handler_Energy__c = usrParis.PARIS_ID__c;
        pscParisOnly.Claim_handler_Marine__c = usrParis.PARIS_ID__c;
        pscParisOnly.Claims_handler_Builders_Risk__c = usrParis.PARIS_ID__c;
        pscParisOnly.Claim_handler_Cargo_Dry__c = usrParis.PARIS_ID__c;
        pscParisOnly.Claim_handler_Cargo_Liquid__c = usrParis.PARIS_ID__c;
        pscParisOnly.Claim_handler_CEP__c = usrParis.PARIS_ID__c;
        pscParisOnly.Claim_handler_Charterers__c = usrParis.PARIS_ID__c;
        pscParisOnly.Claim_handler_Crew__c = usrParis.PARIS_ID__c;
        pscParisOnly.Claim_handler_Defence__c = usrParis.PARIS_ID__c;
        //Secondary claims handlers
        pscParisOnly.Claim_handler_Energy_2__c = usrParis.PARIS_ID__c;
        pscParisOnly.Claim_handler_Marine_2__c = usrParis.PARIS_ID__c;
        pscParisOnly.Claims_handler_Builders_Risk_2__c = usrParis.PARIS_ID__c;
        insert(pscParisOnly);
        system.Assert(pscParisOnly.Id <> null);
        processCompanyGardTeam pc2 = new processCompanyGardTeam();
            pc2.query = 'SELECT Name, Id, Accounting__c, UW_Assistant_1__c, U_W_Assistant_2__c, Claim_handler_Energy__c, Claim_handler_Marine__c, '
                  +'Claim_handler_Cargo_Dry__c, Claim_handler_Cargo_Liquid__c, Claim_handler_CEP__c, Claim_handler_Charterers__c, '
                  +'Claim_handler_Crew__c, Claim_handler_Defence__c, Claims_handler_Builders_Risk__c, '
                  +'(SELECT Name, Source_System__c, Accounting__c, U_W_Assistant_1__c,U_W_Assistant_2__c, Claim_handler_Energy__c, Claim_handler_Marine__c, '
                  +   'Claims_handler_Builders_Risk__c, Claim_handler_Cargo_Dry__c, Claim_handler_Cargo_Liquid__c, Claim_handler_CEP__c, '
                  +   'Claim_handler_Charterers__c, Claim_handler_Crew__c, Claim_handler_Defence__c, On_Risk__c, '
                  //SOC 24/4/13 PCI-000104 - Added secondary claims handler fields
                  +   'Claim_handler_Energy_2__c, Claim_handler_Marine_2__c, Claims_handler_Builders_Risk_2__c, '
                  +   'Claim_handler_Cargo_Dry_2__c, Claim_handler_Cargo_Liquid_2__c, Claim_handler_CEP_2__c, '
                  +   'Claim_handler_Charterers_2__c, Claim_handler_Crew_2__c, Claim_handler_Defence_2__c, '
                  //SOC 4/9/13 PCI-000138 - Added claim adjuster marine
                  +   'Claim_adjuster_Marine__c '
                  +   'from Admin_System_Companies__r where Company_Status__c = \'Active\' AND On_Risk__c=true '
                  +   'order by Source_System__c, GIC_Priority__c, Mapper_Number__c DESC, Company_ID__c DESC) '
                  +'from Account '
                  +'where Id IN (\'' + idAcc + '\',\'' + idAccParis + '\')';
                system.debug('####:'+pc2.query);
            Database.executeBatch(pc2, 20);
            
        

        // Test an account related to both a GIC and a Paris PSC
        // =====================================================
    /*    Account accValidate = [Select Accounting__c, UW_Assistant_1__c,
                                U_W_Assistant_2__c, Claim_handler_Energy__c,
                                Claim_handler_Marine__c, Claims_handler_Builders_Risk__c,
                                Claim_handler_Cargo_Dry__c, Claim_handler_Cargo_Liquid__c,
                                Claim_handler_CEP__c, Claim_handler_Charterers__c,
                                Claim_handler_Crew__c, Claim_handler_Defence__c,
                                Claim_handler_Energy_2_Id__c,
                                Claim_handler_Marine_2_Id__c, Claims_handler_Builders_Risk_2_Id__c,
                                Claim_handler_Cargo_Dry_2_Id__c, Claim_handler_Cargo_Liquid_2_Id__c,
                                Claim_handler_CEP_2_Id__c, Claim_handler_Charterers_2_Id__c,
                                Claim_handler_Crew_2_Id__c, Claim_handler_Defence_2_Id__c,
                                Claim_adjuster_Marine_Id__c,Claims_handler_Builders_Risk_2_lk__c,
                                Claim_handler_Energy_2_lk__c,Claim_handler_Marine_2_lk__c
                               FROM Account
                               Where Id = :idAcc];
          */                     
        //lstUsr = [Select Id, Username, GIC_ID__c, PARIS_ID__c from User];
        
        /*system.debug(accValidate.Accounting__c + '= checking = ' + getUserId(usrParis.PARIS_ID__c));
        system.assertEquals(accValidate.Accounting__c, getUserId(usrParis.PARIS_ID__c));
        system.assert(accValidate.U_W_Assistant_2__c == getUserId(usrParis.PARIS_ID__c));
        system.assert(accValidate.Claim_handler_Energy__c == getUserId(usrParis.PARIS_ID__c));
        */
        //added
        //system.assertEquals(accValidate.Accounting__c, null);
        //system.assert(accValidate.U_W_Assistant_2__c == null);
        //system.assert(accValidate.Claim_handler_Energy__c == null);
        //added end
        
        //system.assert(accValidate.Claim_handler_Marine_2_lk__c == getUserId(usrParis.PARIS_ID__c));
        //system.assert(accValidate.Claims_handler_Builders_Risk_2_lk__c == getUserId(usrParis.PARIS_ID__c));
        //system.assert(accValidate.Claim_handler_Energy_2_lk__c == getUserId(usrParis.PARIS_ID__c));
        //system.assert(accValidate.Claim_handler_Marine_2_lk__c == getUserId(usrParis.PARIS_ID__c));
        //system.assert(accValidate.Claims_handler_Builders_Risk_2_lk__c == getUserId(usrParis.PARIS_ID__c));
        
        /*system.assert(accValidate.UW_Assistant_1__c == getUserId(usrGIC.GIC_ID__c));
        system.assert(accValidate.Claim_handler_Cargo_Dry__c == getUserId(usrGIC.GIC_ID__c));
        system.assert(accValidate.Claim_handler_Cargo_Liquid__c == getUserId(usrGIC.GIC_ID__c));
        system.assert(accValidate.Claim_handler_CEP__c == getUserId(usrGIC.GIC_ID__c));
        system.assert(accValidate.Claim_handler_Charterers__c == getUserId(usrGIC.GIC_ID__c));
        system.assert(accValidate.Claim_handler_Crew__c == getUserId(usrGIC.GIC_ID__c));
        system.assert(accValidate.Claim_handler_Defence__c == getUserId(usrGIC.GIC_ID__c));
        //system.assert(accValidate.Claim_handler_Cargo_Dry_2_Id__c == getUserId(usrGIC.GIC_ID__c));
        //system.assert(accValidate.Claim_handler_Cargo_Liquid_2_Id__c == getUserId(usrGIC.GIC_ID__c));
        //system.assert(accValidate.Claim_handler_CEP_2_Id__c == getUserId(usrGIC.GIC_ID__c));
        //system.assert(accValidate.Claim_handler_Charterers_2_Id__c == getUserId(usrGIC.GIC_ID__c));
        //system.assert(accValidate.Claim_handler_Crew_2_Id__c == getUserId(usrGIC.GIC_ID__c));
        //system.assert(accValidate.Claim_handler_Defence_2_Id__c == getUserId(usrGIC.GIC_ID__c));
        system.assertEquals(getUserId(usrParis.PARIS_ID__c), accValidate.Claim_adjuster_Marine_Id__c);
        */

        // Test an account related to just a GIC PSC
        // =========================================
       /* Account accValidateGIC = [Select Accounting__c, UW_Assistant_1__c,
                                    U_W_Assistant_2__c, Claim_handler_Energy__c,
                                    Claim_handler_Marine__c, Claims_handler_Builders_Risk__c,
                                    Claim_handler_Cargo_Dry__c, Claim_handler_Cargo_Liquid__c,
                                    Claim_handler_CEP__c, Claim_handler_Charterers__c,
                                    Claim_handler_Crew__c, Claim_handler_Defence__c,
                                    Claim_handler_Energy_2_Id__c,
                                    Claim_handler_Marine_2_Id__c, Claims_handler_Builders_Risk_2_Id__c,
                                    Claim_handler_Cargo_Dry_2_Id__c, Claim_handler_Cargo_Liquid_2_Id__c,
                                    Claim_handler_CEP_2_Id__c, Claim_handler_Charterers_2_Id__c,
                                    Claim_handler_Crew_2_Id__c, Claim_handler_Defence_2_Id__c
                                  FROM Account
                                  Where Id = :idAccGIC];
        
        system.debug('#####:'+accValidateGIC.Accounting__c);*/
        //system.assert(accValidateGIC.Accounting__c == getUserId(usrGIC.GIC_ID__c));
        //Accounting comes from Paris so will be null
        //system.assert(accValidateGIC.Accounting__c == null);
        //system.assert(accValidateGIC.U_W_Assistant_2__c == null);
        //system.assert(accValidateGIC.Claim_handler_Energy__c == null);
        //system.assert(accValidateGIC.Claim_handler_Marine__c == null);
        //system.assert(accValidateGIC.Claims_handler_Builders_Risk__c == null);
        /*system.assert(accValidateGIC.UW_Assistant_1__c == getUserId(usrGIC.GIC_ID__c));
        system.assert(accValidateGIC.Claim_handler_Cargo_Dry__c == getUserId(usrGIC.GIC_ID__c));
        system.assert(accValidateGIC.Claim_handler_Cargo_Liquid__c == getUserId(usrGIC.GIC_ID__c));
        system.assert(accValidateGIC.Claim_handler_CEP__c == getUserId(usrGIC.GIC_ID__c));
        system.assert(accValidateGIC.Claim_handler_Charterers__c == getUserId(usrGIC.GIC_ID__c));
        system.assert(accValidateGIC.Claim_handler_Crew__c == getUserId(usrGIC.GIC_ID__c));
        system.assert(accValidateGIC.Claim_handler_Defence__c == getUserId(usrGIC.GIC_ID__c));
        */
        
        //added
        
        //system.assert(accValidateGIC.UW_Assistant_1__c == null);
        //system.assert(accValidateGIC.Claim_handler_Cargo_Dry__c == null);
        //system.assert(accValidateGIC.Claim_handler_Cargo_Liquid__c == null);
        //system.assert(accValidateGIC.Claim_handler_CEP__c == null);
        //system.assert(accValidateGIC.Claim_handler_Charterers__c == null);
        //system.assert(accValidateGIC.Claim_handler_Crew__c == null);
        //system.assert(accValidateGIC.Claim_handler_Defence__c == null);
        //added
        
        //system.assert(accValidateGIC.Claim_handler_Energy_2_Id__c == null);
        //system.assert(accValidateGIC.Claim_handler_Marine_2_Id__c == null);
        //system.assert(accValidateGIC.Claims_handler_Builders_Risk_2_Id__c == null);
        //system.assert(accValidateGIC.Claim_handler_Cargo_Dry_2_Id__c == getUserId(usrGIC.GIC_ID__c));
        //system.assert(accValidateGIC.Claim_handler_Cargo_Liquid_2_Id__c == getUserId(usrGIC.GIC_ID__c));
        //system.assert(accValidateGIC.Claim_handler_CEP_2_Id__c == getUserId(usrGIC.GIC_ID__c));
        //system.assert(accValidateGIC.Claim_handler_Charterers_2_Id__c == getUserId(usrGIC.GIC_ID__c));
        //system.assert(accValidateGIC.Claim_handler_Crew_2_Id__c == getUserId(usrGIC.GIC_ID__c));
        //system.assert(accValidateGIC.Claim_handler_Defence_2_Id__c == getUserId(usrGIC.GIC_ID__c));
        
        // Test an account related to just a Paris PSC
        // ===========================================
      /*  Account accValidateParis = [Select Accounting__c, UW_Assistant_1__c,
                                    U_W_Assistant_2__c, Claim_handler_Energy__c,
                                    Claim_handler_Marine__c, Claims_handler_Builders_Risk__c,
                                    Claim_handler_Cargo_Dry__c, Claim_handler_Cargo_Liquid__c,
                                    Claim_handler_CEP__c, Claim_handler_Charterers__c,
                                    Claim_handler_Crew__c, Claim_handler_Defence__c,
                                    Claim_handler_Energy_2_Id__c,
                                    Claim_handler_Marine_2_Id__c, Claims_handler_Builders_Risk_2_Id__c,
                                    Claim_handler_Cargo_Dry_2_Id__c, Claim_handler_Cargo_Liquid_2_Id__c,
                                    Claim_handler_CEP_2_Id__c, Claim_handler_Charterers_2_Id__c,
                                    Claim_handler_Crew_2_Id__c, Claim_handler_Defence_2_Id__c
                                  FROM Account
                                  Where Id = :idAccParis];
        */
        /*system.assert(accValidateParis.Accounting__c == getUserId(usrParis.PARIS_ID__c));
        system.assert(accValidateParis.U_W_Assistant_2__c == getUserId(usrParis.PARIS_ID__c));
        system.assert(accValidateParis.Claim_handler_Energy__c == getUserId(usrParis.PARIS_ID__c));
        system.assert(accValidateParis.Claim_handler_Marine__c == getUserId(usrParis.PARIS_ID__c));
        system.assert(accValidateParis.Claims_handler_Builders_Risk__c == getUserId(usrParis.PARIS_ID__c));
        */
        //added
        //system.assert(accValidateParis.Accounting__c == null);
        //system.assert(accValidateParis.U_W_Assistant_2__c == null);
        //system.assert(accValidateParis.Claim_handler_Energy__c == null);
        //system.assert(accValidateParis.Claim_handler_Marine__c == null);
        //system.assert(accValidateParis.Claims_handler_Builders_Risk__c == null);
        //added
        
        //system.assert(accValidateParis.UW_Assistant_1__c == null);
        //system.assert(accValidateParis.Claim_handler_Cargo_Dry__c == null);
        //system.assert(accValidateParis.Claim_handler_Cargo_Liquid__c == null);
        //system.assert(accValidateParis.Claim_handler_CEP__c == null);
        //system.assert(accValidateParis.Claim_handler_Charterers__c == null);
        //system.assert(accValidateParis.Claim_handler_Crew__c == null);
        //system.assert(accValidateParis.Claim_handler_Defence__c == null);
        //system.assert(accValidateParis.Claim_handler_Energy_2_Id__c == getUserId(usrParis.PARIS_ID__c));
        //system.assert(accValidateParis.Claim_handler_Marine_2_Id__c == getUserId(usrParis.PARIS_ID__c));
        //system.assert(accValidateParis.Claims_handler_Builders_Risk_2_Id__c == getUserId(usrParis.PARIS_ID__c));
        //system.assert(accValidateParis.Claim_handler_Cargo_Dry_2_Id__c == null);
        //system.assert(accValidateParis.Claim_handler_Cargo_Liquid_2_Id__c == null);
        //system.assert(accValidateParis.Claim_handler_CEP_2_Id__c == null);
        //system.assert(accValidateParis.Claim_handler_Charterers_2_Id__c == null);
        //system.assert(accValidateParis.Claim_handler_Crew_2_Id__c == null);
        //system.assert(accValidateParis.Claim_handler_Defence_2_Id__c == null);
     test.StopTest();     
    }
    static testmethod void runTestGIC(){
        Test.startTest();
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        Gard_Team__c gt= new Gard_Team__c(Name='Claims Support America',Region_code__c='ABCD');
        insert gt;
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Claims_Support_Team__c=gt.Id);
        insert country;
        Account accTest = new Account(Name='TestAcc',RecordTypeId=[Select Id from RecordType where Name = 'Client' LIMIT 1].Id, Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id, Market_Area__c = TestDataGenerator.getMarketArea().Id, country__c=country.id);
        insert(accTest);
        string idAcc = accTest.Id;
        Account accTestGIC = new Account(Name='TestAccGICOnly',RecordTypeId=[Select Id from RecordType where Name = 'Client' LIMIT 1].Id, Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id, Market_Area__c = TestDataGenerator.getMarketArea().Id,country__c=country.Id);
        insert(accTestGIC);
        
        string idAccGIC = accTestGIC.Id;
        User usrGIC = [Select Id, Name, GIC_ID__c, PARIS_ID__c from User where IsActive = True AND GIC_ID__c <> null AND PARIS_ID__c <> null LIMIT 1];

        User usrParis = [Select Id, Name, GIC_ID__c, PARIS_ID__c from User where IsActive = True AND GIC_ID__c <> null AND PARIS_ID__c <> null AND Id <> :usrGIC.Id LIMIT 1];


        Admin_System_Company__c pscGIC = new Admin_System_Company__c(Salesforce_Company__c=idAcc,Source_System__c = 'GIC',Company_Status__c =  str , On_Risk__c = true);
        Admin_System_Company__c pscGICOnly = new Admin_System_Company__c(Salesforce_Company__c=idAccGIC,Source_System__c = 'GIC',Company_Status__c =  str , On_Risk__c = true);
        
        system.Assert(idAccGIC <> null);
        //Populate users
        lstUsr = [Select Id, Username, FirstName, LastName, GIC_ID__c, PARIS_ID__c from User where isActive = true];
        
        pscGIC.Accounting__c = usrGIC.GIC_ID__c;
        pscGIC.U_W_Assistant_1__c = usrGIC.GIC_ID__c;
        pscGIC.U_W_Assistant_2__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Energy__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Marine__c = usrGIC.GIC_ID__c;
        pscGIC.Claims_handler_Builders_Risk__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Cargo_Dry__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Cargo_Liquid__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_CEP__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Charterers__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Crew__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Defence__c = usrGIC.GIC_ID__c;
        //Secondary claims handlers
        pscGIC.Claim_handler_Energy_2__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Marine_2__c = usrGIC.GIC_ID__c;
        pscGIC.Claims_handler_Builders_Risk_2__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Cargo_Dry_2__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Cargo_Liquid_2__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_CEP_2__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Charterers_2__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Crew_2__c = usrGIC.GIC_ID__c;
        pscGIC.Claim_handler_Defence_2__c = usrGIC.GIC_ID__c;
        
        insert(pscGIC);
        
        

        pscGICOnly.Accounting__c = usrGIC.GIC_ID__c;
        pscGICOnly.U_W_Assistant_1__c = usrGIC.GIC_ID__c;
        pscGICOnly.U_W_Assistant_2__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_Energy__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_Marine__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claims_handler_Builders_Risk__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_Cargo_Dry__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_Cargo_Liquid__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_CEP__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_Charterers__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_Crew__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_Defence__c = usrGIC.GIC_ID__c;
        //Secondary claims handler
        pscGICOnly.Claim_handler_Cargo_Dry_2__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_Cargo_Liquid_2__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_CEP_2__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_Charterers_2__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_Crew_2__c = usrGIC.GIC_ID__c;
        pscGICOnly.Claim_handler_Defence_2__c = usrGIC.GIC_ID__c;
        insert(pscGICOnly);
                       
       

        system.Assert(pscGIC.Id <> null);
        //system.Assert(pscParis.Id <> null);
        system.Assert(pscGICOnly.Id <> null);
        
        
        
        //This query needs to be match the query in the schedulable job
            processCompanyGardTeam pc2 = new processCompanyGardTeam();
            pc2.query = 'SELECT Name, Id, Accounting__c, UW_Assistant_1__c, U_W_Assistant_2__c, Claim_handler_Energy__c, Claim_handler_Marine__c, '
                  +'Claim_handler_Cargo_Dry__c, Claim_handler_Cargo_Liquid__c, Claim_handler_CEP__c, Claim_handler_Charterers__c, '
                  +'Claim_handler_Crew__c, Claim_handler_Defence__c, Claims_handler_Builders_Risk__c, '
                  +'(SELECT Name, Source_System__c, Accounting__c, U_W_Assistant_1__c,U_W_Assistant_2__c, Claim_handler_Energy__c, Claim_handler_Marine__c, '
                  +   'Claims_handler_Builders_Risk__c, Claim_handler_Cargo_Dry__c, Claim_handler_Cargo_Liquid__c, Claim_handler_CEP__c, '
                  +   'Claim_handler_Charterers__c, Claim_handler_Crew__c, Claim_handler_Defence__c, On_Risk__c, '
                  //SOC 24/4/13 PCI-000104 - Added secondary claims handler fields
                  +   'Claim_handler_Energy_2__c, Claim_handler_Marine_2__c, Claims_handler_Builders_Risk_2__c, '
                  +   'Claim_handler_Cargo_Dry_2__c, Claim_handler_Cargo_Liquid_2__c, Claim_handler_CEP_2__c, '
                  +   'Claim_handler_Charterers_2__c, Claim_handler_Crew_2__c, Claim_handler_Defence_2__c, '
                  //SOC 4/9/13 PCI-000138 - Added claim adjuster marine
                  +   'Claim_adjuster_Marine__c '
                  +   'from Admin_System_Companies__r where Company_Status__c = \'Active\' AND On_Risk__c=true '
                  +   'order by Source_System__c, GIC_Priority__c, Mapper_Number__c DESC, Company_ID__c DESC) '
                  +'from Account '
                  +'where Id IN (\'' + idAcc + '\',\'' + idAccGIC + '\')';
                system.debug('####:'+pc2.query);
            Database.executeBatch(pc2, 20);
            test.StopTest(); 
      }
      
}