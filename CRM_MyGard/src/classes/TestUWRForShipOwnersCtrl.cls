@isTest(seeAllData=false)
Public class TestUWRForShipOwnersCtrl
{
            Public static testmethod void ShipOwnersCtrl(){
            
            GardTestData test_rec = new GardTestData();
            test_rec.commonrecord(); 
            test_rec.customsettings_rec();
           //Invoking methods of UWRForShipOwnersCtrl for broker and client users.....
           
           
            System.runAs(GardTestData.brokerUser)         
            {    
                test.startTest();
                trading_certificates__c tcCS = new trading_certificates__c();tcCS.name = 'testReciever';tcCS.value__c = 'ownergard@gmail.com'; insert tcCS;//sfedit remove
                UWRForShipOwnersCtrl test_broker=new UWRForShipOwnersCtrl();
                test_broker.isSame = true;
                test_broker.lstContact = new List<Contact>();
                test_broker.imoNo = 123;
                test_broker.selectedCountryCode = 'IN - India';
                List<SelectOption> CollLiabilities= test_broker.getCollLiabilities();
                test_broker.selectedGlobalClient = new list<string>();
                test_broker.selectedGlobalClient.add(gardtestdata.clientAcc.id);
                List<SelectOption> clientOptions =  test_broker.getClientLst();
                List<SelectOption> flag=test_broker.getFlagLst();
                List<SelectOption> port=test_broker.getport();
                List<SelectOption> code=test_broker.getCountrycodes();
                //List<SelectOption> classification=so.getclassS();
                List<SelectOption> nationalities= test_broker.getNationality();
                system.assertequals(flag.size()>0,true,true);
                system.assertequals(CollLiabilities.size()>0,true,true);
                system.assertequals(clientOptions .size()>0,true,true);
                system.assertequals(port.size()>0,true,true);
                system.assertequals(nationalities.size()>0,true,true);
                test_broker.assetFinal = null;
                test_broker.selectedObj=GardTestData.test_object_1st.id;
                test_broker.selectedClient = GardTestData.clientAcc.id;
                test_broker.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c); 
                test_broker.FindBy='imo';          
                test_broker.searchByImoObjectName();
                test_broker.assetFinal = null;
                test_broker.selectedObj=GardTestData.test_object_1st.id;
                test_broker.selectedClient = GardTestData.clientAcc.id;
                test_broker.ObjName = GardTestData.test_object_1st.name; 
                test_broker.FindBy='objname';
                test_broker.searchByImoObjectName();
                test_broker.riskAttachmentDate='05.02.2015';
                test_broker.dateOutputForReview='05-02-2015';
                test_broker.selectedObj=GardTestData.test_object_1st.id;
                test_broker.setObjectSelection();
                test_broker.chkAccess();
                //test_broker.saveRecordForReview();
                test_broker.SelectedClassification.add('test');
                test_broker.Classification='test3';
                test_broker.selectedCollLiabilities= '0 RDC';
                test_broker.selectedIncludingFFO= 'yes';
               test_broker.saveRecord();
                test_broker.clearRecord();
                test_broker.getCName();
                 //test_broker.Classification='test3';
                test_broker.goNext();
               // test_broker.saveRecordForAddlInfo();
                test_broker.sendToForm();
                //test_broker.callSendEmail();
                test_broker.clearRecordForAddlInfo();
                test_broker.goPrevForAddlInfo();
                test_broker.cancelRecord();
                test_broker.goPrevForReview();
                test_broker.goNextForAddlInfo();
                test_broker.uwf.id=GardTestData.brokerCase.id;
               // test_broker.saveRecord();
                test_broker.saveRecordForAddlInfo();
                test_broker.saveRecordForReview();
                test_broker.strSelected='a;bc';
                test_broker.fetchSelectedClients();
                test_broker.strSelected='abc';
                test_broker.fetchSelectedClients();
                test_broker.strGlobalClient=GardTestData.brokerAcc.id+';'+GardTestData.brokerAcc.id;
                test_broker.fetchGlobalClients();
                
                Case updtUWF = [SELECT Id, short_description_vessel__c,Max_container_capacity__c,Nationality_of_officer__c,
                                     Nationality_of_crew__c,Car_Capacity__c,X1_4_RDC__c,X4_4_RDC__c,Name_Form_Period_Of_Charter__c,
                                     Name_on_Premium_Invoice__c,Country_prefix__c,Co_assureds_capacity__c
                                     FROM case WHERE Id =: GardTestData.brokerCase.Id];
                updtUWF.short_description_vessel__c = 'long text';
                updtUWF.Max_container_capacity__c = 'text';
                updtUWF.Nationality_of_officer__c= 'India';
                updtUWF.Nationality_of_crew__c = 'Australia';
                updtUWF.Car_Capacity__c = 'text';
                updtUWF.X1_4_RDC__c = 'Yes';
                updtUWF.X4_4_RDC__c = 'Yes';
                updtUWF.Name_Form_Period_Of_Charter__c = 'text Area';
                updtUWF.Name_on_Premium_Invoice__c = 'Long Text';
                updtUWF.Country_prefix__c = 'text';
                updtUWF.Co_assureds_capacity__c = 'long Text';
                update updtUWF;
                
                test_broker.saveRecordForReview();
                test_broker.setGlobalClientIds = new List<String>();
                test_broker.getClientLst();
                test_broker.selectedGlobalClient = new List<String>();
                //test_broker.getClientLst();
                test.stopTest();
            }
            
        }
    }