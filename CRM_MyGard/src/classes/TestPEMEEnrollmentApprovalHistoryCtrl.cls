@isTest(seeAllData=false)
public class TestPEMEEnrollmentApprovalHistoryCtrl
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string partnerLicenseId1 = [SELECT Id FROM profile WHERE Name='System Administrator' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    public static List<PEME_Enrollment_Form__c> pefList=new List<PEME_Enrollment_Form__c>();
    
    Static testMethod void cover()
    {
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
        AdminUsers__c setting = new AdminUsers__c();
            setting.Name = 'Number of users';
            setting.Value__c = 3;
            insert setting;
        Gard_Team__c gardTeam=new Gard_Team__c(P_I_email__c='test@test.com',Region_code__c='CASM', Office__c='Arendal');

        insert gardTeam;
        Valid_Role_Combination__c vrcESP = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
        insert vrcESP;
        Country__c country=new Country__c(Claims_Support_Team__c=gardTeam.id);
        insert country;
        User user = new User(
                            Alias = 'standt', 
                            profileId = salesforceLicenseId ,
                            Email='standarduser@testorg.com',
                            EmailEncodingKey='UTF-8',
                            CommunityNickname = 'test13',
                            LastName='Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',  
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='test008@testorg.com',
                            ContactId__c = gc.id
                             );
        insert user ;
        User usersa = new User(
                            Alias = 'standtsa', 
                            profileId = partnerLicenseId1 ,
                            Email='standardsauser@testorg.com',
                            EmailEncodingKey='UTF-8',
                            CommunityNickname = 'testsa13',
                            LastName='Testingsa',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',  
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='testsa008@testorg.com',
                            ContactId__c = gc.id
                             );
        insert usersa ;
        
        User user1 =new User();
        user1 = [select id, contactid__c from user where id=:user.id];
        Account brokerAcc = new Account( Name='testre',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.Broker_Contact_Record_Type,
                                        Site = '_www.cts.se',
                                        Type = 'Broker' ,
                                        Area_Manager__c = user1.id,      
                                        Market_Area__c = Markt.id ,
                                        Confirm_not_on_sanction_lists__c = true,
                                        License_description__c  = 'some desc',
                                        Description = 'some desc',
                                        Licensed__c = 'Pending'
                                                                   
                                     );
        AccountList.add(brokerAcc);
        Account clientAcc= New Account();
        clientAcc.Name = 'Testt';
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.Area_Manager__c = user1.id;        
        clientAcc.Market_Area__c = Markt.id; 
        clientAcc.company_Role__c = 'External Service Provider';
        clientAcc.Sub_Roles__c = 'ESP - Agent';
        clientAcc.country__c = country.Id ;      
        AccountList.add(clientAcc);
        insert AccountList; 
        Contact brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(brokerContact) ;
        Contact clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        insert ContactList;
        User brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='test120@test.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id
                                   );
        UserList.add(brokerUser);
        User clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
                
        UserList.add(clientUser) ;
        insert UserList;
        
        PEME_Enrollment_Form__c pefBroker=new PEME_Enrollment_Form__c();
        pefBroker.ClientName__c=brokerAcc.ID;
        pefBroker.Enrollment_Status__c='Draft';
        pefBroker.Submitter__c=brokerContact.Id;
        pefList.add(pefBroker);
        PEME_Enrollment_Form__c pefClient=new PEME_Enrollment_Form__c();
        pefClient.ClientName__c=clientAcc.ID;
        pefClient.Enrollment_Status__c='Draft';
        pefClient.Submitter__c=clientContact.Id;
        pefList.add(pefClient);
        insert pefList;
        
        System.runAs(brokerUser)
        {
            ApexPages.StandardController sc = new ApexPages.StandardController(pefBroker);
            PEMEEnrollmentApprovalHistoryCtrl brokerTest=new PEMEEnrollmentApprovalHistoryCtrl(sc);
            brokerTest.fetchHistory();
        }
        
        System.runAs(clientUser)
        {
            ApexPages.StandardController sc = new ApexPages.StandardController(pefClient);
            PEMEEnrollmentApprovalHistoryCtrl clientTest=new PEMEEnrollmentApprovalHistoryCtrl(sc);
            clientTest.fetchHistory();
        }
    }
}