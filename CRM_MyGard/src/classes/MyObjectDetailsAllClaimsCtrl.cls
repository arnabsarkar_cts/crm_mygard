public without sharing class MyObjectDetailsAllClaimsCtrl{
    public Object__c obj  { get; set; }
    List<Claim__c> lstRisk { get; set; }
    List<Claim__c> lstCase { get; set; }
    public String strId {get; set;}
    public String objectGUIID {get; set;}
    public boolean showconfirmPopUp { get;set; }
    public boolean showobjectPopUp { get; set; }
    public String strEditObjectFreeText { get; set; }
    public Claim__c caseIns{get;set;}
    public Boolean isAuthorized {get;set;}
    public Claim__c objCase = new Claim__c();
    List<String> prodArea = new List<String>();
    public String accGUID{get;set;}
    //pagination variables
    private static final Integer SMARTQUERY_PAGE_SIZE   = 10;
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public String sSoqlQuery{get;set;}
    private String nameOfMethod;
    Public Integer intStartrecord { get ; set ;}
    Public Integer intEndrecord { get ; set ;}
    public Integer pageNum{get;set;}
    public String caseId{get;set;}
    private Boolean isQueried;
   // private List<Contract> lstContractClient;
    public String header, footer, signature, link,link1, header_ack;
    User usr = new User();
    //Contact contct = new Contact();
    String loggedInAccountName;
    public Set<Id> setObjectIds;
    public Set<String> Account_ID_Set;//helperclass
    public String userType;
    public Set<String> total_Contract_Id_Set;//from helper class
    public Set<String> own_Contract_ID_Set;//won, Shared, Acquired Contracts
    //public Set<String> Shared_Contract_ID_Set;//Shared Clients Contracts
    public Boolean IsSharedClient {get; set;}
    //public Account_Contact_Mapping__c acmObj;//from helper class
    set<String> setAgreementIds;//for isAuthorized check:broker share
    //List<broker_share__c> lstbrokershare = new List<broker_share__c>();//for isAuthorized check:broker share
    
    String strASC;
    public String jointSortingParam { get;set; }
    private String sortDirection;
    private String sortExp;  
    private Set<ID> setRiskID;
    public string loggedInContactId;
    public string loggedInAccountId;
    public Boolean isObjectFav{get;set;}//Added For Favourites
    Public Boolean IsPeopleLineUser{get;set;}
    private Contact myContact;
    
    public ID caseTestId;
    
    // Used to display available docNo wrt object id selected
    Public Integer docNo{get;set;}
    List<DocumentMetadata_Junction_object__c> documentLst;
    List<String> ContractIdSet;
    set<id> clientIdSet = new set<id>();
    
    public List<List<Claim__c>> lstAllCaseForExcel{get;set;}
    public Integer noOfData{get;set;}
    public List<String> covers; // SF - 1916
    public boolean objDetailDocTab {get;set;}
    public String sortExpression
    {
        get
        {
            return sortExp;
        }
        set
        {
            //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortExp)
                sortDirection = (sortDirection == strASC)? 'DESC' : strASC;
            else
                sortDirection = strASC;
            sortExp = value;
        }
    }
    
    public String getSortDirection()
    {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
            return strASC;
        else
            return sortDirection;
    }
    
    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }
    
    
    public MyObjectDetailsAllClaimsCtrl(){
        setObjectIds = new Set<Id>();
        //lstContractClient = new List<Contract>();
    }
    
    public MyObjectDetailsAllClaimsCtrl(ApexPages.StandardController stdController) 
    {
        //broker share
        //calling helperclass method and helper class variables
        MyGardHelperCtrl.CreateCommonData();
        usr = MyGardHelperCtrl.usr;
        userType = MyGardHelperCtrl.USER_TYPE;
        loggedInContactId  = MyGardHelperCtrl.LOGGED_IN_CONTACT_ID;
        loggedInAccountId = MyGardHelperCtrl.LOGGED_IN_ACCOUNT_ID;
        loggedInAccountName = MyGardHelperCtrl.ACCOUNT_NAME;
        Account_ID_Set = MyGardHelperCtrl.ACCOUNT_ID_SET;
        total_Contract_Id_Set = MyGardHelperCtrl.TOTAL_CONTRACT_ID_SET;
        own_Contract_ID_Set = MyGardHelperCtrl.OWN_CONTRACT_ID_SET;
        IsPeopleLineUser = MyGardHelperCtrl.ACM_JUNC_OBJ.IsPeopleClaimUser__c;
        
        MyGardDocumentAvailability__c tabAvailCS  = MyGardDocumentAvailability__c.getValues('Object Details Document Tab');
        if(tabAvailCS.IsAvailable__c){
            objDetailDocTab = true;
        }
        else{
            objDetailDocTab = false;
        }
        
        /*Shared_Contract_ID_Set = new Set<String>();
        if((!total_Contract_Id_Set.isEmpty()) && (total_Contract_Id_Set.size() > 0))
        {
            for(string st: total_Contract_Id_Set)
            {
                Shared_Contract_ID_Set.add(st);
            }
        }
        Boolean checkShared = Shared_Contract_ID_Set.removeAll(Global_Contract_ID_Set);//collecting shared contracts only
        System.debug('Shared Contracts Present:'+checkShared);
        System.debug('Shared Contracts:'+Shared_Contract_ID_Set);*/
        //acmObj = MyGardHelperCtrl.ACM_JUNC_OBJ;
        prodArea = MyGardHelperCtrl.prodArea;
        
        //initializing non-static variables
        isAuthorized = false;
        pageNum = 1;
        isQueried = false;
        strASC = 'ASC';
        sortDirection = strASC;
        sortExp = 'Claim_Type__c';
        //IsPeopleLineUser = false;
        setObjectIds = new Set<Id>();
        setAgreementIds  = new set<String>();
        documentLst  = new List<DocumentMetadata_Junction_object__c>();
        docNo = 0;  
        header_ack = System.Label.header_Acknowledgement_mail;
        ContractIdSet = new List<String>();
        header = System.Label.Header;
        signature = System.Label.Signature;
        footer = System.Label.Footer;
        link = System.Label.InternalOrgURL;
         link1 = System.Label.InternalLightningOrgURL;
        lstAllCaseForExcel= new List<List<Claim__c>>();
        
        //contct = [SELECT otherphone, mobilephone, email from Contact where id =: loggedInContactId];
        
        //added for multi company
        AccountToContactMap__c acmCS = AccountToContactMap__c.getValues(loggedInContactId);
        system.debug('****************** acmCS : '+acmCS);
        if(acmCS!=null && acmCS.name!=null && acmCS.name!='')
        {
            system.debug('****************** acmCS : '+acmCS);
            loggedInAccountId = acmCS.AccountId__c;
            system.debug('****************** loggedInAccountId : '+loggedInAccountId);
        }
        
        
        /*Account_Contact_Mapping__c peoplelineuser_acm = acmObj;
        if(peoplelineuser_acm != null)
            IsPeopleLineUser = peoplelineuser_acm.IsPeopleClaimUser__c;
        if(peoplelineuser_acm != null && peoplelineuser_acm.PI_Access__c)
            prodArea.add('P&I');
        if(peoplelineuser_acm != null && peoplelineuser_acm.Marine_Access__c)
            prodArea.add('Marine'); 
        if(peoplelineuser_acm != null)
            IsPeopleLineUser = peoplelineuser_acm.IsPeopleClaimUser__c;*/
        
        loggedInAccountName = MyGardHelperCtrl.ACCOUNT_NAME;
        //Account acc;
        List<Object__c> lstObject = new List<Object__c>();
        List<Contract> lstContract = new List<Contract>();
        //lstContractClient = new List<Contract>();        
        
        /*if(loggedInAccountId != null && loggedInAccountId != ''){
            //acc = [Select ID,Company_Role__c from Account Where id =: loggedInAccountId ];        
            //if(acc != null && acc.Company_Role__c != null &&  acc.Company_Role__c.contains('Broker'))
            if(userType == 'Broker'){                   
                lstContractClient = [Select Id, Client__c from Contract Where Id IN: total_Contract_Id_Set];
                //broker share        
            }else{          
                lstContractClient = [Select id from Contract Where Client__c =: loggedInAccountId AND (broker__c=null or Shared_With_Client__c=true)];
            }
        }*/
        List<Asset> allContracts = new List<Asset>();
        try{
            objectGUIID = ApexPages.CurrentPage().getParameters().get('objid');
            accGUID = ApexPages.CurrentPage().getParameters().get('clientId');
            Object__c referedObject = [SELECT ID FROM Object__c WHERE guid__c =: objectGUIID];
            strId =String.valueOf(referedObject.Id);
            System.debug('**********************'+strId );
            //------------------------------------------------------------------------------------------------------------------------------------------
            
            
        //security check and hide 'New request' Link for Shared Contracts
            IsSharedClient = true; // button will not be hidden
            isAuthorized = false;
            String contractId;
        //Get related contracts from the asset IDs
            allContracts = [SELECT ID, Agreement__c, Agreement__r.Client__c, Agreement__r.Broker__c,Agreement__r.Shared_With_Client__c from Asset WHERE Object__c =: Id.valueof(strId) AND agreement__r.Business_Area__c IN: prodArea];        
            System.debug('AllContracts: '+ allContracts);
            //broker share

            for(Asset ast : allContracts)
            {
                contractId = ast.agreement__c;
                if(ast.Agreement__r.Broker__c != null && total_Contract_Id_Set.contains(ast.agreement__c))
                {
                    isAuthorized = true;                
                }
                else if(ast.Agreement__r.Client__c != null && Account_ID_Set.contains(ast.Agreement__r.Client__c))
                {
                    isAuthorized = true; 
                    IsSharedClient = true;        // for client - button will not be hidden       
                }
               
                /*if(own_Contract_ID_Set.contains(ast.Agreement__c))
                    IsSharedClient = true;
                if(total_Contract_Id_Set.contains(ast.Agreement__c))
                    isAuthorized = true;*/
                clientIdSet.add(ast.Agreement__r.Client__c);
                setAgreementIds.add(ast.Agreement__c);    
                if((isAuthorized))
                    break;

                //clientIdSet.add(ast.Agreement__r.Client__c);
                //setAgreementIds.add(ast.Agreement__c);
            }
            
            /*for(Asset ast: [select agreement__c,agreement__r.broker__c,Agreement__r.shared_with_client__c,agreement__r.client__c from Asset where object__r.guid__c =: objectGUIID])// AND agreement__r.shared_with_client__c = true])
            {
                contractId = ast.agreement__c;
                if(ast.Agreement__r.Broker__c != null && total_Contract_Id_Set.contains(ast.agreement__c))
                {
                    isAuthorized = true;                
                }
                else if(ast.Agreement__r.Client__c != null && Account_ID_Set.contains(ast.Agreement__r.Client__c))
                {
                    isAuthorized = true; 
                    IsSharedClient = true;        // for client - button will not be hidden       
                }   
                if((isAuthorized))
                    break;
            }*/
            
            //for broker
            if(MyGardHelperCtrl.USER_TYPE == 'Broker')
            {
                if(own_Contract_ID_Set.contains(contractId))
                    IsSharedClient = true; //button will not be hidden
                else
                    IsSharedClient = false; //button will be hidden
            }
        }catch(Exception e)
        {
            System.debug('Exception found .........'+e);
        }
        System.Debug('allContracts  : '+ allContracts );
        //Now Check
        /*isAuthorized = false;
        if(lstbrokershare.size() > 0)
        {
            isAuthorized = true; 
        }
        else
        {
            isAuthorized = false;
        }*/
        System.Debug('Is he/she authorized: '+ isAuthorized);
        Object__c object1 = [Select id,Name,Object_Type__c,Object_Sub_Type__c,No_of_passenger__c,No_of_crew__c,Gross_tonnage__c,Length__c,Width__c,Depth__c,Dead_Weight__c,Port__c,Signal_Letters_Call_sign__c,Classification_society__c,Flag__c,
                       Rebuilt__c,
                       Imo_Lloyds_No__c From Object__c Where id =: Id.valueof(strId)];        
        if(isAuthorized){
            //this.obj= [Select id,Imo_Lloyds_No__c From Object__c Where id =: Id.valueof(strId)];
            this.obj = object1;
            system.debug('********this.obj.Imo_Lloyds_No__c************'+this.obj.Imo_Lloyds_No__c);
        }        
        //-------------------------------------------------
        if(isAuthorized && this.obj.Imo_Lloyds_No__c != null)
        {
            List<Object__c> lstobj = [Select id from Object__c Where  Imo_Lloyds_No__c =: this.obj.Imo_Lloyds_No__c];
            setObjectIds = new Set<Id>();       
            for(Object__c obj : lstobj){       
                setObjectIds.add(obj.id);     
            }
        }
        else
        {
            if(isAuthorized)
            {
                List<Object__c> lstobj = [Select id from Object__c Where  id =: strId];
                setObjectIds = new Set<Id>();       
                for(Object__c obj : lstobj){       
                    setObjectIds.add(obj.id); 
                }
            }
        }   
        if(isAuthorized)
        {       
            /*this.obj= [Select id,Name,Object_Type__c,Object_Sub_Type__c,No_of_passenger__c,No_of_crew__c,Gross_tonnage__c,Length__c,Width__c,Depth__c,Dead_Weight__c,Port__c,Signal_Letters_Call_sign__c,Classification_society__c,Flag__c,
                       Rebuilt__c,
                       Imo_Lloyds_No__c From Object__c Where id =: Id.valueof(strId)];*/
              this.obj = object1;
            
            //*************************** Added to get no of document start ***************************     
            system.debug(strId +'********* '+ this.obj.Imo_Lloyds_No__c+'********* '+ loggedInAccountId+'********* '+ loggedInContactId+'********* '+ clientIdSet +'********* '+ this.obj.Name);
            docNo = MyObjectDetailsCtrl.countObjectDocs(strId, this.obj.Imo_Lloyds_No__c, loggedInAccountId, loggedInContactId, clientIdSet, this.obj.Name);
            
            //*************************** no of document End ***************************
        }
        fetchFavourites();//Added For Favourites
    }
    public List<Claim__c> getClaims(){        
        //lstCase = [Select Claim_Type__c, Status,Product__c, Event_Date__c ,Claim_Year__c, Claim_Incurred_USD__c, Member_reference__c, Claim_Reference_Number__c From Case Where Object__c =:Id.valueof(strId)];
        ViewData();
        
        return lstCase ;
    }
    
    
    
    public PageReference ViewData() {
        covers = MyGardHelperCtrl.DummyCovers(); // SF-1916
        if (jointSortingParam != null)
        {
            system.debug('******jointSortingParam**************'+jointSortingParam);
            String[] arrStr = jointSortingParam .split('##');
            sortDirection = arrStr[0];
            sortExp = arrStr[1];
            system.debug('******jointSortingParam**************'+jointSortingParam);
        }
        system.debug('total_Contract_Id_Set** ' + total_Contract_Id_Set);
        system.debug('covers** ' + covers);
        system.debug('setObjectIds** ' + setObjectIds);
        
        string sortFullExp = sortExpression  + ' ' + sortDirection;
        sSoqlQuery = 'Select id, Claim_Type__c, Status__c,Product__c, Event_Date__c ,Claim_Year__c, Total_100__c, Member_reference__c, Claim_Reference_Number__c,Risk_Coverage__r.Name,Vessel_Name__c From Claim__c Where Object__c IN : setObjectIds AND Contract_for_Review__c IN : total_Contract_Id_Set AND Status__c = \'Open\'  AND(NOT(Risk_Coverage__r.Name IN: covers)) ORDER BY '+sortFullExp+' '; // changes made for SF-1916 --> AND(NOT(Risk_Coverage__r.Name IN: covers))
        
        system.debug('******jointSortingParam**************'+jointSortingParam);
        
        
        isQueried = true;
        if(sSoqlQuery.length()>0)
        {
            system.debug('****setCon**********'+setCon);
            system.debug('****setCon.getResultSize()**********'+setCon.getResultSize());
            
            lstCase=(List<Claim__c>)setCon.getRecords();
            if(setCon.getPageNumber() == 1)
                intStartrecord = 1;
            else
                intStartrecord  = ((setCon.getPageNumber() - 1) * SMARTQUERY_PAGE_SIZE) + 1;
            
            intEndrecord = setCon.getPageNumber() * SMARTQUERY_PAGE_SIZE;
            if( intEndrecord > noOfRecords ){
                intEndrecord = noOfRecords ;
            }
        }
        
        return null;
    }
    
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null || isQueried){
                isQueried = false;
                size = SMARTQUERY_PAGE_SIZE;
                System.debug('Soql Query--->'+sSoqlQuery);
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery+' LIMIT 10000'));
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
                if (noOfRecords == null || noOfRecords == 0){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No search results found.'));
                }else if (noOfRecords == 10000){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The search returned 10000 records (maximum allowable limit).'));
                }
                
            }
            return setCon;
        }set;
    }
    
    //to go to a specific page number
    public void setpageNumber()
    {
        setCon.setpageNumber(pageNum);
        ViewData();
    }
    
    
    public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }
    
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }
    
    //Used for displaying the second page for the claim details
    public PageReference showCaseDetails()
    {
        CLaim__c referredCase = [SELECT guid__c FROM Claim__c WHERE id =: caseId ];
        PageReference page = new PageReference('/apex/ClaimsDetail?claimid='+ referredCase.guid__c +'&isMyClaims=false');
        return page;
    }
    
    public void navigate(){
        if(nameOfMethod=='previous'){
            setCon.previous();
            pageNum=setCon.getpageNumber();
            ViewData();
        }
        else if(nameOfMethod=='last'){
            setCon.last();
            pageNum=setCon.getpageNumber();
            ViewData();
        }
        else if(nameOfMethod=='next'){
            setCon.next();
            pageNum=setCon.getpageNumber();
            ViewData();
        }
        else if(nameOfMethod=='first'){
            setCon.first();
            pageNum=setCon.getpageNumber();
            ViewData();
        }
    }
    
    
    public void first() {
        nameOfMethod='first';
        navigate();
    }
    
    public void next(){
        nameOfMethod='next';
        pageNum = setCon.getPageNumber();
        navigate();
        
    }
    
    public void last(){
        nameOfMethod='last';
        navigate();
    }
    
    public void previous(){
        nameOfMethod='previous';
        pageNum = setCon.getPageNumber();
        navigate();
    }
    
    public PageReference sendMailforEditObject() 
    {
        system.debug('*******sendMailforEditObject:');
        List<Asset> listast = new List<Asset>([select AccountId from Asset where Object__c =: obj.id]);
        caseIns = new Claim__c();
        caseIns.Account__c = listast[0].AccountId;
        caseIns.Object__c = obj.id;
        caseIns.Type__c = 'Edit object details';
        caseIns.Status__c = 'New';
        caseIns.origin__c = 'Community';        
        caseIns.Description__c = strEditObjectFreeText;
        try
        {
            insert caseIns;
            system.debug('*******caseIns: '+caseIns);
            system.debug('*******caseIns: '+caseIns.Id);
        }
        catch(Exception e)
        {
            system.debug(e.getMessage());
        }
        if(!Test.isRunningTest()){
            objCase = [SELECT id, object__r.name, object__r.Imo_Lloyds_No__c, Description__c,caseNumber__c,GUID__c from Claim__c where id =: caseIns.id];
        }else{
            objCase = [SELECT id, object__r.name, object__r.Imo_Lloyds_No__c, Description__c,caseNumber__c,GUID__c  from Claim__c where id =: caseTestId];
        }        
        List<Asset> lstAsset = [Select id, Account.name,Account.Underwriter_4__r.Email,Account.X3rd_UWR__r.Email, Account.X2nd_UWR__r.Email, Account.UW_Assistant_1__r.Email, Account.U_W_Assistant_2__r.Email, Account.Underwriter_main_contact__r.Email From Asset Where Object__c =: obj.Id];               
        List<String> lstFedIds = new List<String>();
        List<String> lstToEmail = new List<String>();
        List<String> lstCcEmail = new List<String>();        
        lstCcEmail.add(UserInfo.getUserEmail());
        if(lstAsset != null && lstAsset.size()>0){
            if(lstAsset[0].Account.Underwriter_main_contact__r.Email != null){ 
                lstToEmail.add(lstAsset[0].Account.Underwriter_main_contact__r.Email);    
            }
            if(lstAsset[0].Account.Underwriter_4__r.Email != null){   
                lstToEmail.add(lstAsset[0].Account.Underwriter_4__r.Email);     
            }
            if(lstAsset[0].Account.X3rd_UWR__r.Email != null){ 
                lstToEmail.add(lstAsset[0].Account.X3rd_UWR__r.Email);      
            }
            if(lstAsset[0].Account.X2nd_UWR__r.Email != null){  
                lstToEmail.add(lstAsset[0].Account.X2nd_UWR__r.Email);    
            }
            if(lstAsset[0].Account.UW_Assistant_1__r.Email != null){  
                lstToEmail.add(lstAsset[0].Account.UW_Assistant_1__r.Email);    
            }
            if(lstAsset[0].Account.U_W_Assistant_2__r.Email != null){  
                lstToEmail.add(lstAsset[0].Account.U_W_Assistant_2__r.Email);    
            }
            
        }
        System.debug('***************lstToEmail*******************'+lstToEmail);       
        showconfirmPopUp = true;
        showobjectPopUp = false;
        sendAckMail();
        sendNotiMail();
        return null;
    }
    public void closeconfirmPopUp(){
        showconfirmPopUp = false;
        showobjectPopUp = false;
        strEditObjectFreeText = null;
    }
    public void displayEditPopUp(){
        showconfirmPopUp = false;
        showobjectPopUp = true;
        
    }
    public void sendAckMail()
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage() ;
        List<EmailTemplate> Ack_TempId = new List<EmailTemplate>([SELECT Id, body, subject FROM EmailTemplate WHERE Name =:'MyGard-Request Change In Object-Acknowledgement']);
        String emailBody = Ack_TempId[0].body;
        String mail_Subject = Ack_TempId[0].subject;
        System.debug('emailBody -->' + emailBody);
        if(emailBody != null && emailBody != '')
        {
            emailBody = emailBody.replace('[HEADER]',header_ack);
            emailBody = emailBody.replace('[SIGNATURE]',signature);
            emailBody = emailBody.replace('[FOOTER]',footer);
            emailBody = emailBody.replace('[OBJECTNAME]', objCase.object__r.name);
            if(objCase.object__r.Imo_Lloyds_No__c != null){
                String imo = String.valueOf(objCase.object__r.Imo_Lloyds_No__c);
                emailBody = emailBody.replace('[IMONUMBER]', imo);
            }
            else{
                emailBody = emailBody.replace('[IMONUMBER]', '');
            }
            if(objCase.Description__c != null)
            {
                emailBody = emailBody.replace('[DESCRIPTION]', objCase.Description__c.replace('\n', ' ') );
            }
            else
            {
                emailBody = emailBody.replace('[DESCRIPTION]', '' );
            }
             //Added by Geetham
            if(objCase.GUID__c!=null)
            {
                emailBody = emailBody.replace('[LINK]',label.MyGard_RequestDetails_URL+objCase.GUID__c);
            }
            else
            {
                emailBody = emailBody.replace('[LINK]','');
            }
            //***************************SF-3976 STARTS***************
            if(usr!=null){ 
            if(usr.Contact.name!=null){
                emailBody = emailBody.replace('[USERNAME]',usr.Contact.Name);
            }
            else{
                emailBody = emailBody.replace('[USERNAME]','N/A');
            }
            if(usr.Contact.phone!=null){
                emailBody = emailBody.replace('[PHONE]',usr.Contact.phone);
            }
            else{
                emailBody = emailBody.replace('[PHONE]','N/A');
            }
             if(usr.Contact.MobilePhone!=null){
               emailBody = emailBody.replace('[MOBILE]',usr.Contact.MobilePhone);
            }
            else{
               emailBody = emailBody.replace('[MOBILE]','N/A');
            }
            if(usr.Contact.email!=null){
            emailBody = emailBody.replace('[EMAIL]',usr.Contact.email);
            }
            else{
            emailBody = emailBody.replace('[EMAIL]','N/A');
            }
            }           
            //***************************SF-3976 ENDS************************* 
        }
        System.debug('emailBody -->' + emailBody);
        List<String> lstCcEmail = new List<String>();        
        lstCcEmail.add(usr.Contact.Email);
        mail.setSubject(mail_Subject);
        mail.setOrgWideEmailAddressId([select Id from OrgWideEmailAddress where DisplayName = 'no-reply@gard.no'].Id);
        CommonVariables__c cvar = CommonVariables__c.getInstance('ReplyToMailId');
        mail.setReplyTo(cvar.value__c);
        mail.targetObjectId =  usr.contactID;
        mail.saveAsActivity = false;
        emailBody = emailBody + MyGardHelperCtrl.getOrganizationDetails();mail.setPlainTextBody(emailBody);
        System.debug('mail : ' + emailBody);
        //Attach mail to loggedin contact and related object               
        MyGardHelperCtrl.attachMailToObject(loggedInContactId, caseIns.id, emailBody, mail_Subject);
        try
        {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
        }
        catch(Exception e)
        {
            system.debug('inside exception -------'+e);
        }      
    }
    public void sendNotiMail()
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage() ;
        List<Asset> lstAsset = [Select id, Account.name,Account.Underwriter_4__r.Email,Account.X3rd_UWR__r.Email, Account.X2nd_UWR__r.Email, Account.UW_Assistant_1__r.Email, Account.U_W_Assistant_2__r.Email, Account.Underwriter_main_contact__r.Email From Asset Where Object__c =: obj.Id];
        List<String> lstToEmail = new List<String>();
        List<String> lstToEmail_cc = new List<String>();
        if(lstAsset != null && lstAsset.size()>0){
            system.debug('lstAsset[0].Object__c: '+obj.Id);
            system.debug('lstAsset[0].Account.Underwriter_main_contact__c: '+lstAsset[0].Account.Underwriter_main_contact__c);
            system.debug('lstAsset[0].Account.Underwriter_4__c: '+lstAsset[0].Account.Underwriter_4__c);
            system.debug('lstAsset[0].Account.X3rd_UWR__c: '+lstAsset[0].Account.X3rd_UWR__c);
            system.debug('lstAsset[0].Account.X2nd_UWR__c: '+lstAsset[0].Account.X2nd_UWR__c);
            system.debug('lstAsset[0].Account.UW_Assistant_1__c: '+lstAsset[0].Account.UW_Assistant_1__c);
            system.debug('lstAsset[0].Account.U_W_Assistant_2__c: '+lstAsset[0].Account.U_W_Assistant_2__c);
            if(lstAsset[0].Account.Underwriter_main_contact__r.Email != null){
                system.debug('loop 1'); 
                lstToEmail.add(lstAsset[0].Account.Underwriter_main_contact__r.Email);    
            }
            //SF-3652 start
            if(BlueCardTeam__c.getInstance('BlueCardTeam').value__c != null)
            {
                lstToEmail.add(BlueCardTeam__c.getInstance('BlueCardTeam').value__c);
            }
            // end
            if(lstAsset[0].Account.Underwriter_4__r.Email != null){
                system.debug('loop 2');   
                lstToEmail_cc.add(lstAsset[0].Account.Underwriter_4__r.Email);     
            }
            if(lstAsset[0].Account.X3rd_UWR__r.Email != null){
                system.debug('loop 3'); 
                lstToEmail_cc.add(lstAsset[0].Account.X3rd_UWR__r.Email);      
            }
            if(lstAsset[0].Account.X2nd_UWR__r.Email != null){
                system.debug('loop 4');  
                lstToEmail_cc.add(lstAsset[0].Account.X2nd_UWR__r.Email);    
            }
            if(lstAsset[0].Account.UW_Assistant_1__r.Email != null){
                system.debug('loop 5');  
                lstToEmail_cc.add(lstAsset[0].Account.UW_Assistant_1__r.Email);    
            }
            if(lstAsset[0].Account.U_W_Assistant_2__r.Email != null){ 
                system.debug('loop 6'); 
                lstToEmail_cc.add(lstAsset[0].Account.U_W_Assistant_2__r.Email);    
            }
            
        }
        System.debug('***************lstToEmail*******************'+lstToEmail); 
        System.debug('***************lstToEmail_cc*******************'+lstToEmail_cc);
        List<EmailTemplate> Noti_TempId = new List<EmailTemplate>([SELECT Id, body,subject FROM EmailTemplate WHERE Name =:'MyGard-Request Change In Object-Notification']);
        String emailBody = Noti_TempId[0].body;
        String mail_Subject = Noti_TempId[0].subject+' for '+MyGardHelperCtrl.fetchSelectedClientName(lstAsset[0].AccountId);   //SF-4079;
        if(emailBody != null && emailBody != '')
        {
            emailBody = emailBody.replace('[HEADER]',header);
            emailBody = emailBody.replace('[SIGNATURE]',signature);
            emailBody = emailBody.replace('[FOOTER]',footer);
            if(objCase.object__r.name != null){
                emailBody = emailBody.replace('[Object name]', objCase.object__r.name);
            }
            else{
                emailBody = emailBody.replace('[Object name]', '');
            }
            if(objCase.object__r.Imo_Lloyds_No__c != null){
                String imoNotify = String.valueOf(objCase.object__r.Imo_Lloyds_No__c);
                emailBody = emailBody.replace('[Imo no]', imoNotify);
            }
            else{
                emailBody = emailBody.replace('[Imo no]', '');
            }
            if(objCase.Description__c != null)
            {
                emailBody = emailBody.replace('[Description]', objCase.Description__c.replace('\n', ' '));
            }
            else
            {
                emailBody = emailBody.replace('[Description]', '');
            }
            //Added by Geetham
            if(objCase.id!=null)
            {
                emailBody = emailBody.replace('[LINK]',link+ objCase.id);
                emailBody = emailBody.replace('[LightningLINK]',link1 + objCase.id+'/view');
            }
            else
            {
                emailBody = emailBody.replace('[LINK]','');
                emailBody = emailBody.replace('[LightningLINK]','');
            }
            emailBody = emailBody.replace('[Name]', usr.name);
            if(usr.Contact.OtherPhone != null){
                system.debug('************usr.Contact.OtherPhone '+usr.Contact.OtherPhone);
                emailBody = emailBody.replace('[Phone]', usr.Contact.OtherPhone);
            }
            else{
                emailBody = emailBody.replace('[Phone]', '');
            }
            if(usr.Contact.MobilePhone != null){
                emailBody = emailBody.replace('[Mobile]', usr.Contact.MobilePhone);
            }
            else{
                emailBody = emailBody.replace('[Mobile]', '');
            }
            if(usr.Contact.Email != null){
                emailBody = emailBody.replace('[Email]', usr.Contact.Email); 
            }
            else{
                emailBody = emailBody.replace('[Email]', '');
            }
            emailBody = emailBody.replace('[Company name]', loggedInAccountName);
            emailBody = emailBody.replace('[LINK]', link+objCase.id);
        }
        System.debug('emailBody -->' + emailBody);
        
        mail.setToAddresses(lstToEmail);
        mail.setCcAddresses(lstToEmail_cc);
        mail.setSubject(mail_subject);
        mail.setOrgWideEmailAddressId([select Id from OrgWideEmailAddress where DisplayName = 'no-reply@gard.no'].Id);
        List<Claim__c> caseObj = new List<Claim__c>([select Owner.id from Claim__c where id=: caseIns.id]);
        CommonVariables__c cvar = CommonVariables__c.getInstance('ReplyToMailId');
        mail.setReplyTo(cvar.value__c);
        emailBody = emailBody + MyGardHelperCtrl.getOrganizationDetails(); /*SF 3712*/ mail.setPlainTextBody(emailBody);
        mail.saveAsActivity = false;
        System.debug('mail : ' + mail );
        //Attach mail to loggedin contact and related object               
        MyGardHelperCtrl.attachMailToObject(loggedInContactId, caseIns.id, emailBody, mail_Subject);
        try
        {   //Added for SF-3951 on 15th March, 2019 STARTS
             List<String> QueueMemberEmail = new List<String>();
             QueueMemberEmail = MyGardHelperCtrl.queueEmailMembers('Portfolio requests handler queue');
             mail.setToAddresses(QueueMemberEmail);
             mail.setCcAddresses(new List<String>());
            //Added for SF-3951 on 15th March, 2019 ENDS
             
             System.debug('sending mail - '+mail);
            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
        }
        catch(Exception e)
        {
            system.debug('inside exception -------'+e);
        }      
    }
    
    public PageReference exportToExcel()
    {
        lstAllCaseForExcel = new List<List<Claim__c>>();
        List<Claim__c> lst = new List<Claim__c>();
        Integer count = 0;
        noOfData = 0;
        for(Claim__c cases : Database.Query(sSoqlQuery))// + ' limit 1000');
        {
            if(count<1000)
            {
                lst.add(cases);
                count++;
            }   
            else
            {
                lstAllCaseForExcel.add(lst);
                lst = new List<Claim__c>();
                lst.add(cases);
                count = 1;
            }
            noOfData++; 
        }
        lstAllCaseForExcel.add(lst);
        system.debug(' ********* lstAllCaseForExcel  : '+lstAllCaseForExcel.size());
        PageReference pr = new PageReference('/apex/GenerateExcelforMyObjectDetailsAllClaims?objid='+strId);
        return pr;
    }   
    public PageReference printData()
    {
        lstAllCaseForExcel = new List<List<Claim__c>>();
        List<Claim__c> lst = new List<Claim__c>();
        Integer count = 0;
        noOfData = 0;
        for(Claim__c cases : Database.Query(sSoqlQuery))// + ' limit 1000');
        {
            if(count<1000)
            {
                lst.add(cases);
                count++;
            }   
            else
            {
                lstAllCaseForExcel.add(lst);
                lst = new List<Claim__c>();
                lst.add(cases);
                count = 1;
            }
            noOfData++; 
        }
        lstAllCaseForExcel.add(lst);
        system.debug(' ********* lstAllCaseForExcel  : '+lstAllCaseForExcel.size());
        PageReference pr = new PageReference('/apex/PrintforMyObjectDetailsAllClaims?objid='+strId);
        return pr;
    }
    public Static String getUserName()
    {
        String name;
        User usr = [Select Id, contactID from User Where Id =: UserInfo.getUserId()];
        Contact myContact;
        if(usr!= null && usr.contactID != null){
            myContact = [Select AccountId,Name from Contact Where id=: usr.contactID];
            name=myContact.Name;
        }
        return name;
    }
    
    //Added For Favourites - Start
    public void createFavourites(){
        system.debug('createFavourites');
        String currentUserId = UserInfo.getUserId();        
        List<Extranet_Favourite__c> listobj = new List<Extranet_Favourite__c>();
        listobj = [select Id,Item_Id__c,Favourite_By__c from Extranet_Favourite__c where Favourite_By__c=:currentUserId and Item_Id__c=:obj.Id LIMIT 1];
        if(listobj.size()==0){
            system.debug('******** obj.Id : '+obj.Id);
            system.debug('********* obj.Name: '+obj.Name);
            Extranet_Favourite__c objFav = new Extranet_Favourite__c();
            objFav.Item_Id__c = obj.Id;
            objFav.Item_Type__c = 'Object';
            objFav.Favourite_By__c = UserInfo.getUserId();
            objFav.Item_Name__c = obj.Name;
            objFav.Object__c = obj.Id;
            AccountToContactMap__c acmCS = AccountToContactMap__c.getValues(usr.contactID);//Added For Favourites(Multiple Client)
            objFav.Favourite_For_Broker_Client__c = acmCS.AccountId__c;//Added For Favourites(Multiple Client)
            insert objFav;  
        }   
    }  
    
    public void deleteFavourites(){
        system.debug('deleteFavourites');
        String currentUserId = UserInfo.getUserId();        
        AccountToContactMap__c acmCS = AccountToContactMap__c.getValues(usr.contactID);//Added For Favourites(Multiple Client)
        Extranet_Favourite__c objFav = [select Id,Item_Id__c,Favourite_By__c from Extranet_Favourite__c where Favourite_By__c=:currentUserId and Item_Id__c=:obj.Id and Favourite_For_Broker_Client__c =:acmCS.AccountId__c LIMIT 1];//Added For Favourites(Multiple Client)
        if(objFav!=null){
            delete objFav;  
        }
    }
    
    public void fetchFavourites(){
        system.debug('fetchFavourites');
        String currentUserId = UserInfo.getUserId();
        //User usr = [Select Id, contactID, Name from User Where Id =: UserInfo.getUserId()];//Added For Favourites(Multiple Client)
        AccountToContactMap__c acmCS = AccountToContactMap__c.getValues(usr.contactID);//Added For Favourites(Multiple Client)
        List<Extranet_Favourite__c> listobj = new List<Extranet_Favourite__c>();
        if(isAuthorized)
            listobj = [select Id,Item_Id__c,Favourite_By__c from Extranet_Favourite__c where Favourite_By__c=:currentUserId and Item_Id__c=:obj.Id and Favourite_For_Broker_Client__c =:acmCS.AccountId__c LIMIT 1];//Added For Favourites(Multiple Client)
        if(listobj.size()>0){
            isObjectFav = true;     
        }else{
            isObjectFav = false;    
        }        
    } 
    //Added For Favourites - End
}