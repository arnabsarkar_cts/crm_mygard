//class used for Company Ribbon vf page, displayed as banner in Account Details Page
public without sharing class CompanyRibbonExtension{

    private final Account thisAccount;
    private final boolean useRoleRecords = false;
    public String imageURL{get; private set;}
    public string recordType {get; set;}
    public List<String> otherRoles{ 
        get{
            System.debug('sfEdit in otherRoles company - '+thisAccount);
            if(this.otherRoles == null){
                this.otherRoles = new List<string>();
                
                /*if(useRoleRecords){//to be removed
                    List<MDM_Role__c> actRoles = [SELECT Id, Name, Role__c, Active__c FROM MDM_Role__c WHERE Account__c = : this.thisAccount.id AND Active__c = true];
                                        
                    for(MDM_Role__c aRole:actRoles){
                        if(aRole.Role__c != thisAccount.recordType.Name){
                            otherRoles.add(aRole.Role__c);
                        }
                    }
                }else{
                */
                if (thisAccount.company_Role__c != null){
                        String[] companyRoles = thisAccount.company_Role__c.split(';');
                        for(String companyRole : companyRoles){
                            //system.debug('*** companyRole : '+companyRole);
                            if(companyRole != thisAccount.recordType.Name){                                
                                otherRoles.add(companyRole);                                
                            }
                        }
                }
                //}
            }
            return this.otherRoles;
        } 
        private set; 
    }
        
    public CompanyRibbonExtension (ApexPages.StandardController stdController){
        //this.thisAccount = (Account)stdController.getRecord();//commented for SF-4727
        
        String thisAccountId = ApexPages.currentPage().getParameters().get('editedAccId');//will have value when recordType gets updated
        //System.debug('thisAccountId - '+thisAccountId);
        //System.debug('stdController - '+stdController);
        //System.debug('(Account)stdController.getRecord() - '+(Account)stdController.getRecord());
        
        //when page is loaded with Account Standard page
        if(thisAccountId == null){
            thisAccountId = stdController.getId();
        }
        
        //retrieve thisAccount data
        this.thisAccount = [SELECT id, recordType.Name,recordTypeId,company_Role__c,LastModifiedDate,Ribbon_Image__c FROM Account WHERE id = :thisAccountId];
        
        setImageUrl();
        
        //System.debug('constructor run imageURL - '+imageURL);
        //System.debug('constructor run thisAccount - '+this.thisAccount);
    }

    
    //SF-3723:Changing Return type to void, was "PageReference", earlier
    public void applyRecordTypeChange(){//SF-4727
         try {
             //system.debug('*** recordType : '+recordType );
             recordType rt = [select Id from recordType where Name = :recordType and SobjectType = 'Account' Limit 1];
             thisAccount.recordTypeId = rt.id;
             //system.debug('*** rt.id: '+rt.id);
             update thisAccount;
         } catch (Exception ex) {
             apexPages.addMessages(ex);
         }
    }
    
    //added for SF-4727
    private void setImageUrl(){
        if(this.thisAccount != null && this.thisAccount.Ribbon_Image__c != null){
            String ribbonImageString = this.thisAccount.Ribbon_Image__c;
            Integer startIndex = ribbonImageString.lastIndexOf('/')+1;
            try{
                imageURL = PageReference.forResource(ribbonImageString.substring(startIndex)).getUrl();
                return;
            }catch(Exception ex){
                System.debug('Exception Caught @ CompanyRibbonExtension.setImageURL - '+ex.getMessage());
            }
        }
        imageURL = '';
    }
        
    //added for SF-4727,
    //reloads embedded page by passing updated account's value as parameter
    public PageReference reloadRibbon(){
        PageReference pr =  Page.companyRibbon; //new PageReference('/' + thisAccount.id);
        pr.getParameters().put('editedAccId',(thisAccount.id+''));
        pr.setRedirect(true);
        return pr; //SF-4727    
    }
/*  
    public List<String> SelectedRoles{
    get{
            if(this.SelectedRoles == null){
                this.SelectedRoles = new List<string>();
                
                if(useRoleRecords){    
                    List<MDM_Role__c> actRoles = [SELECT Id, Name, Role__c, Active__c FROM MDM_Role__c WHERE Account__c = : this.thisAccount.id AND Active__c = true];
                
                    for(MDM_Role__c aRole:actRoles){
                        SelectedRoles.add(aRole.Role__c);
                    }
                    
                }else{                  
                    if (thisAccount.company_Role__c != null){
                        String[] companyRoles = thisAccount.company_Role__c.split(';');
                        SelectedRoles.addAll(companyRoles);                        
                    } 
                }
                
            }
            return this.SelectedRoles;
            
        } 
        private set;        
    }
    
    public Set<String> AllRoles{
        get{
            if(this.AllRoles==null){
                this.AllRoles = new Set<String>();
                Schema.DescribeFieldResult F = Role__c.Role__c.getDescribe();
                List<Schema.PicklistEntry> picklist = F.getPicklistValues();
                for(Schema.PicklistEntry aPick: picklist){
                    this.AllRoles.add(aPick.getLabel());
                }
                return this.AllRoles;
            }else{
                return this.AllRoles;
            }
        }
        private set;
    }
    
    //Available roles shows a disjunction of the set of all roles and the set of other roles.
    public Set<String> AvailableRoles {
        get{
            this.AvailableRoles = this.AllRoles.clone();
            this.AvailableRoles.removeAll(this.SelectedRoles);    
            return this.AvailableRoles;     
        }
        private set;
    }
    
    public Set<String> UnapprovedRoles {
        get{
            if(this.UnapprovedRoles==null){
                this.UnapprovedRoles = MultiValueToolkit.multiValueToSet(this.thisAccount.Company_Role__c);
            }
            return this.UnapprovedRoles;
        }
        private set;
    } 
    public Set<String> CompanyRoles {
        get{
            if(this.CompanyRoles==null){
                this.CompanyRoles = MultiValueToolkit.multiValueToSet(this.thisAccount.Company_Role__c);
            }
            return this.CompanyRoles;
        }
        private set;
    }  
    
    public string AddRole {get; set;}  
    public string RemoveRole {get; set;} 
    
    public pagereference AddRole() {
         try {
             Role__c aRole = new Role__c(Role_Key__c = AddRole, Role__c = AddRole, IsActive__c = true, Activated__c = System.now(), Account__c = this.thisAccount.id);
             upsert aRole Role_Key__c;
             if(this.thisAccount.Unapproved_Company_Roles_to_Add__c!=null){
                this.thisAccount.Unapproved_Company_Roles_to_Add__c += ';' + AddRole;
             }else{
                this.thisAccount.Unapproved_Company_Roles_to_Add__c = AddRole;
             }
             if(this.thisAccount.Company_Role__c!=null){
                List<String> lstRoles = new List<String>();
                UnapprovedRoles.add(AddRole);
                lstRoles.addAll(UnapprovedRoles);
                this.thisAccount.Unapproved_Company_Roles__c = String.join(lstRoles, ';');
             }else{
                this.thisAccount.Unapproved_Company_Roles__c = AddRole;
             }
             update this.thisAccount;
             //Now call an approval process to get the role changes approved
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setObjectId(this.thisAccount.id);
            Approval.ProcessResult result = Approval.process(req1);
             return new PageReference('/' + thisAccount.id);
         } catch (Exception ex) {
             apexPages.addMessages(ex);
             return null;
         }
    } 
    public pagereference RemoveRole() {
         try {
             Role__c aRole = new Role__c(Role_Key__c = RemoveRole, Role__c = RemoveRole, IsActive__c = false, Inactivated__c = System.now(), Account__c = this.thisAccount.id);
             upsert aRole Role_Key__c;
             if(this.thisAccount.Unapproved_Company_Roles_to_Remove__c!=null){
                this.thisAccount.Unapproved_Company_Roles_to_Remove__c += ';' + RemoveRole;
             }else{
                this.thisAccount.Unapproved_Company_Roles_to_Remove__c = RemoveRole;
             }
             if(this.thisAccount.Company_Role__c!=null){
                List<String> lstRoles = new List<String>();
                UnapprovedRoles.remove(RemoveRole);
                lstRoles.addAll(UnapprovedRoles);
                this.thisAccount.Unapproved_Company_Roles__c = String.join(lstRoles, ';');
             }
             update this.thisAccount;
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setObjectId(this.thisAccount.id);
            Approval.ProcessResult result = Approval.process(req1);             
             return new PageReference('/' + thisAccount.id);
         } catch (Exception ex) {
             apexPages.addMessages(ex);
             return null;
         }
    }
    
*/
    
/*  public static Set<String> multiValueToSet(String mvFieldValue){
        Set<String> setValues = new Set<String>();
        List<String> lstValues = mvFieldValue.split(';');       
        if(lstValues!=null){
            setValues.addAll(lstValues);
        }
        return setValues;
    }
    */
}