/*
 --------------------------------------------------------------------------------------------------------
*    Developer                                Date                        Description
*    --------------------------------------------------------------------------------------------------------
*     Pulkit Sood                             19/02/2018        SF-3976  Include email address of submitter in "Form uploaded" notification mails
*/
public class PEMEClinicInvoiceDetailsCtrl{

    public String header, footer, signature, header_ack;
    private static final Integer SMARTQUERY_PAGE_SIZE   = 10;
    public set<String> lstExaminations {get;set;}
    public set<String> lstObjects {get;set;}
    public set<String> lstStatus {get;set;}  
    public PEME_Invoice__c preMedInvoice ; // SF 4687
    public PEME_Invoice__c objInvoice {get;set;}
    public list<PEME_Exam_Detail__c> lstExamDetails {get;set;}
    public list<PEME_Exam_Detail__c> lstExamsPrint {get;set;}
    public String selectedInvoice {get;set;}
    public list<String> selectedObject {get;set;}
    public list<String> selectedExam {get;set;}
    public list<String> selectedStatus {get;set;}
    //pagination variables
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public String sSoqlQuery, link, mail_subject;
    private String nameOfMethod;
    Public Integer intStartrecord { get ; set ;}
    Public Integer intEndrecord { get ; set ;}
    public Integer pageNum{get;set;}//{pageNum = 1;}
    public String jointSortingParam { get;set; }
 //   private String sortDirection = Ascending;
 //   private String sortExp = 'Date__c';
    public Integer noOfData{get;set;}
    //pagination variables
    //Added for exam edit inline
    private static string REJECTED_CLOSED = 'Rejected-Closed';
    public string editExamId{get;set;}
    public string deleteExamId{get;set;}    
    public boolean isEditClicked {get; set;} //{isEditClicked = false;}
    public PEME_Exam_Detail__c editableExamRecord{get;set;}
    public List<ProcessInstanceStep> lstRejectHistory {get; set;}
    public String orderBy{get;set;}//{orderBy = '';}
    private String sortDirection = 'ASC';
    private String sortExp = '';  
    public string loggedInContactId;
    public string loggedInAccountId;
    public List<Contact> lstContact{get;set;}
    public String usertype;
    public PEME_Invoice__c objInv {get;set;} //used for fetching edit invoice records
    private static String Ascending='ASC';
    public Boolean isSuccess{get; set;}
    public String USER_TYPE{get;set;}
    private final String strBroker = 'Broker';
    private final String strClient = 'Client';
    private final String strClinic = 'Clinic';
    private final String strESP = 'External Service Provider';
    Public String hidePemeObject=''; // for 4687
    Public String hidePemeObj='';  // for 4687
    public String orderByNew
    {
     get
         {
            return sortExp;
         }
         set
         {
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExp)
             sortDirection = (sortDirection.contains('DESC')? Ascending : 'DESC');
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     }
     public String getSortDirection()
     {
        //if not column is selected 
        if (orderByNew == null || orderByNew == '')
          return Ascending;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value)
     {  
       sortDirection = value;
     }
    public String userName{get;set;}
    public ApexPages.StandardSetController submissionSetConOpen{
    get{ 
           return submissionSetConOpen;         
    }
    set; }
    public List<SelectOption> getExamOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstExaminations!= null && lstExaminations.size()>0){
            for(String strOp:lstExaminations){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();            
        }
        return Options;
    }
    
    public List<SelectOption> getObjectOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstObjects!= null && lstObjects.size()>0){
            for(String strOp:lstObjects){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();            
        }
        return Options;
    }
    
    public List<SelectOption> getStatusOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstStatus!= null && lstStatus.size()>0){          
            for(String strOp:lstStatus){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();            
        }
        return Options;
    }
    
    public String sortExpression{
         get{
            return sortExp;
         }
         set{          
           if (value == sortExp)
             sortDirection = (sortDirection == Ascending)? 'DESC' : Ascending;
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     }
    
   /*  public String getSortDirection(){        
        if (sortExpression == null || sortExpression == '')
          return Ascending;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value){  
       sortDirection = value;
     }
     */
    public PEMEClinicInvoiceDetailsCtrl(){ 
        isSuccess = false;
        pageNum = 1;
        orderBy = '';
        isEditClicked = false;
        MyGardHelperCtrl.createCommonData();
        USER_TYPE = MyGardHelperCtrl.USER_TYPE; //SF-4516
        List<User> lstUser = new List<User>([select firstName,lastName,ContactId,Contact.Name,Contact.Account.Name from User where id=:UserInfo.getUserId()]);
        if(lstUser!=null && lstUser.size()>0 && lstUser[0].Contactid!=null)
        {
            loggedInContactId = lstUser[0].Contactid;
            lstContact = new List<Contact>([select name, account.id,account.name,accountid, account.recordtypeid,account.company_Role__c, /*IsPeopleLineUser__c,*/account.BillingPostalcode__c,account.BillingCountry__c,account.BillingState__c,account.Billing_Street_Address_Line_1__c,account.Billing_Street_Address_Line_2__c,account.Billing_Street_Address_Line_3__c,account.Billing_Street_Address_Line_4__c 
                                          from Contact 
                                          where id=:loggedInContactId]);  
            userName = lstUser[0].Contact.Name; 
            loggedInAccountId = lstContact[0].accountId;                   
        }       
    }
    public pageReference loadData()
    {
        if(lstContact!=null && lstContact.size()>0 && lstContact[0].accountId!=null)
            {
                //if(lstContact[0].account.company_Role__c.contains('External Service Provider')) //Added for MYG-2948
                if(USER_TYPE != null && USER_TYPE != '' && USER_TYPE.containsIgnoreCase(strClinic)) //Added for MYG-2948 //SF-4516
                { 
                    usertype = strClinic;
                    header = System.Label.Header;
                    signature = System.Label.Signature;
                    footer = System.Label.Footer;
                    link = System.Label.InternalOrgURL;
                    header_ack = System.Label.header_Acknowledgement_mail;        
                    lstExaminations = new set<String>(); 
                    lstObjects = new set<String>();  
                    lstStatus = new set<String>();     
                    lstExamDetails = new list<PEME_Exam_Detail__c>(); 
                    editableExamRecord = new PEME_Exam_Detail__c();
                    List<String> objLst = new List<String>(); //for 4687
                    String preMedexObj='';  // for 4687
                    String objectToDisplay='';  // for 4687
                    hidePemeObject = MyGardHelperCtrl.hidePrepemeObject(); // for 4687
                    
                  //  List<User> lstUser = new List<User>([select firstName,lastName,ContactId,Contact.Name,Contact.Account.Name from User where id=:UserInfo.getUserId()]);
                           
                    selectedInvoice = ApexPages.currentPage().getParameters().get('invoices');
                    if(selectedInvoice!=null && selectedInvoice!=''){      
                        
                        //modified query for MYG-2990
                        preMedInvoice = [SELECT Client__c,Client__r.Name,Clinic_Invoice_Number__c,Clinic__c,Clinic__r.Name,Crew_Examined__c,Invoice_Date__c,PEME_reference_No__c,Status__c,ObjectList__c,Brief_Description__c,
                        Total_Amount__c,Manning_agent__r.Client__r.Name,Vessels__c,Vessels__r.Name,Period_Covered_From__c,Period_Covered_To__c,GUID__c,Bank_address__c FROM PEME_Invoice__c where GUID__c=:selectedInvoice and Clinic__c =:loggedInAccountId];   
                        
                        if(preMedInvoice.ObjectList__c!=null) // SF - 4687 start
                        {
                        objLst=preMedInvoice.ObjectList__c.split(';');
                        system.debug('objLst---'+objLst);
                        }
                        if(objLst.size()>0 && preMedInvoice.ObjectList__c.containsIgnoreCase(hidePemeObject)) {
                         for(String ss: objlst) {
                          if(ss.containsIgnoreCase(hidePemeObject)){
                            preMedexObj=ss;
                          system.debug('preMedInvoice.ObjectList__c11'+objectToDisplay);
                          } else {
                              objectToDisplay = objectToDisplay.removeStart(';') +';'+ ss;
                              system.debug('objectToDisplay'+objectToDisplay );
                          } 
                         }
                         preMedInvoice.ObjectList__c= objectToDisplay;
                        } // SF - 4687 end
                        objInvoice =preMedInvoice;
                        
                        generateExams();        
                    }
                    
                    system.debug('lstExaminations.size() '+lstExaminations.size()); 
                    system.debug('lstObjects.size() '+lstObjects.size());        
                    system.debug('lstExamDetails.size() '+lstExamDetails.size());   
                }
                else
                {
                    PageReference pg = new PageReference('/apex/HomePage');          
                    return pg;
                }
            }
        return null;
    }
    
    public void generateExams(){
                
        pageNum = 1;
        lstExamDetails= new List<PEME_Exam_Detail__c>();
        orderByNew = 'Date__c';
        hidePemeObject = MyGardHelperCtrl.hidePrepemeObject(); // for 4687
        hidePemeObj = '%' + hidePemeObject + '%';  // for 4687
        
      /*  if (jointSortingParam != null){
            system.debug('******jointSortingParam**************'+jointSortingParam);
            String[] arrStr = jointSortingParam .split('##');
            sortDirection = arrStr[0];
            sortExp = arrStr[1];
        }else{
            jointSortingParam = 'ASC##Date__c';           
        } 
        */
        String strCondition ='';
        if(selectedExam!= null && selectedExam.size()>0){
            strCondition = ' AND Examination__c IN:selectedExam';
        }       
        if(selectedObject!= null && selectedObject.size()>0){
            strCondition = strCondition +' AND Object_Related__r.Name IN : selectedObject ';                
        }
        if(selectedStatus!= null && selectedStatus.size()>0){
            strCondition = strCondition +' AND Status__c IN : selectedStatus ';                
        }
        
        string sortFullExp = sortExpression  + ' ' + sortDirection;
                   
        sSoqlQuery ='SELECT Age__c,Amount__c,Invoice__r.GUID__c,Date__c,Examination__c,Examinee__c,Id,Invoice__c,Object_Related__c,Object_Related__r.Name,Rank__c,Status__c,Comments__c,GARD_Comments__c FROM PEME_Exam_Detail__c'+
                    ' where Invoice__r.GUID__c=:selectedInvoice and Status__c !=:REJECTED_CLOSED and Invoice__r.Clinic__c =:loggedInAccountId'+strCondition;
        orderBy = ' ORDER BY ' + orderByNew + ' ' + sortDirection; //For new sorting 
        sSoqlQuery = sSoqlQuery+ ' ' + orderBy;  
        submissionSetConOpen = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery + ' limit 10000')); 
        submissionSetConOpen.setPageSize(10);
        createOpenUserList();          
      //  searchAssets();
                            
        for(PEME_Exam_Detail__c objExam:[SELECT Age__c,Amount__c,Date__c,Examination__c,Examinee__c,Id,Invoice__c,Object_Related__c,Object_Related__r.Name,Rank__c,Status__c,Comments__c,GARD_Comments__c FROM PEME_Exam_Detail__c
        where Invoice__r.GUID__c=:selectedInvoice and Status__c !='Rejected-Closed' AND (NOT(Object_Related__r.Name LIKE : hidePemeObj)) ]){     
            lstExaminations.add(objExam.Examination__c);
            lstObjects.add(objExam.Object_Related__r.Name); 
            lstStatus.add(objExam.Status__c);              
        } 
    }
     Public void createOpenUserList()
    {
        if(submissionSetConOpen!=null)
        {
            lstExamDetails.clear(); 
            for(PEME_Exam_Detail__c inv : (List<PEME_Exam_Detail__c>)submissionSetConOpen.getRecords())             
                lstExamDetails.add(inv);
                System.debug('********'+lstExamDetails.size());         
        } 
    }
    //Returns to the first page of records
    public void firstOpen() {
        submissionSetConOpen.first(); 
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();
    }
    public Boolean hasPreviousOpen   
    {   
        get   
        {   
            return submissionSetConOpen.getHasPrevious();   
        }   
        set;   
    } 
     //Returns the previous page of records   
    public void previousOpen()   
    {   
        submissionSetConOpen.previous();
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();  
    } 
    //to go to a specific page number
    public void setpageNumberOpen()
    {
        submissionSetConOpen.setpageNumber(pageNum); 
        createOpenUserList();
    }
    public Boolean hasNextOpen   
    {   
        get   
        {   
            return submissionSetConOpen.getHasNext();   
        }   
        set;   
    } 
    //Returns the next page of records   
    public void nextOpen()   
    {   
        submissionSetConOpen.next(); 
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();
        
    }
     //Returns to the last page of records
    public void lastOpen(){
        submissionSetConOpen.last(); 
        pageNum = submissionSetConOpen.getPageNumber();
        
        createOpenUserList();
        
    }
   /* public void searchAssets(){
        if(sSoqlQuery.length()>0){           
            System.debug('selectedExam--->'+selectedExam);            
            System.debug('selectedObject--->'+selectedObject);
            System.debug('Soql Query--->'+sSoqlQuery);  
            setCon = null;            
            
            lstExamDetails=(List<PEME_Exam_Detail__c>)setCon.getRecords();
            system.debug('****setCon.getResultSize()**********'+setCon.getResultSize());
            if(setCon.getPageNumber() == 1)
                intStartrecord = 1;
            else
                intStartrecord  = ((setCon.getPageNumber() - 1) * SMARTQUERY_PAGE_SIZE) + 1;
          
            intEndrecord = setCon.getPageNumber() * SMARTQUERY_PAGE_SIZE;
            if( intEndrecord > noOfRecords ){
                intEndrecord = noOfRecords ;
            }
        }
                                                              
    }
        
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                size = SMARTQUERY_PAGE_SIZE;
                System.debug('Soql Query--->'+sSoqlQuery);              
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery+' LIMIT 10000'));
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
                system.debug('****Total records****'+noOfRecords);
                if (noOfRecords == null || noOfRecords == 0){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No search results found.'));
                }else if (noOfRecords == 10000){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The search returned 10000 records (maximum allowable limit).'));
                }                
            }
            return setCon;
        }set;
    }  
    
    public void setpageNumber(){
        setCon.setpageNumber(pageNum);
        searchAssets();
    }
        
    public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }
    
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }
       
    public void navigate(){
        if(nameOfMethod=='previous'){
             setCon.previous();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }else if(nameOfMethod=='last'){
             setCon.last();
             searchAssets();
        }else if(nameOfMethod=='next'){
             setCon.next();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }else if(nameOfMethod=='first'){
             setCon.first();
             searchAssets();
        }
    }
        
    public void first() {
        nameOfMethod='first';
        navigate();
    }
    
    public void next(){
        nameOfMethod='next';
        pageNum = setCon.getPageNumber();
        navigate();        
    }
   
    public void last(){
        nameOfMethod='last';
        navigate();
    }
  
    public void previous(){
        nameOfMethod='previous';
         pageNum = setCon.getPageNumber();
        navigate();
    } 
    */
    public pageReference clearOptions(){
        selectedExam.clear();
        selectedObject.clear();
        selectedStatus.clear();
        generateExams();
        return null;
    }
    
    public PageReference print(){
        lstExamsPrint = new List<PEME_Exam_Detail__c>();
        Integer count = 1;
        noOfData = 0;
        for(PEME_Exam_Detail__c mcl : Database.Query(sSoqlQuery+' LIMIT 10000')){               
            if(count<1000){
                lstExamsPrint.add(mcl);                    
                count++;
            }else{                   
                count = 1;
            }   
            noOfData++;                 
        }
        return page.PrintforClinicExamDetails;
    }
        
     public PageReference exportToExcel(){
        lstExamsPrint = new List<PEME_Exam_Detail__c>();
        Integer count = 1;
        noOfData = 0;
        for(PEME_Exam_Detail__c mcl : Database.Query(sSoqlQuery+' LIMIT 10000')){               
            if(count<1000){
                lstExamsPrint.add(mcl);                    
                count++;
            }else{                   
                count = 1;
            }   
            noOfData++;                 
        }
        return page.GenerateExcelForClinicExamDetails;
    }
    //Added for Exam details edit for rejection
    public void fetchExamLineItem()
    {
        system.debug('editExamId---->'+editExamId);
        isEditClicked = true;
        List<Id> lastProcessId = new List<Id>();            
        lastProcessId.clear();
        if(editExamId!= null)
        {
          editableExamRecord = [SELECT Age__c,Amount__c,Date__c,Examination__c,Examinee__c,Id,Invoice__c,Object_Related__c,Object_Related__r.Name,Rank__c,Status__c,Comments__c,GARD_Comments__c FROM PEME_Exam_Detail__c where id=:editExamId ];
          
          for(ProcessInstance proccessList:[Select Id, TargetObjectId, isDeleted, Status,LastModifiedDate,( Select id,StepStatus, Comments 
          From StepsAndWorkItems Where StepStatus = 'Rejected' and isDeleted = false Order By Createddate Desc ) From ProcessInstance 
          Where isDeleted = false and TargetObjectId= :editExamId Order By Createddate])
          {
               lastProcessId.add(proccessList.Id);
          }
          if(lastProcessId != null && lastProcessId.size()>0)
          {
              lstRejectHistory = [SELECT Comments,CreatedDate,Id,ProcessInstanceId,StepStatus FROM ProcessInstanceStep where ProcessInstanceId in:lastProcessId and StepStatus='Rejected' Order By Createddate DESC];
          }
           system.debug('proccessList---->'+lastProcessId);
           system.debug('editableExamRecord ---->'+ editableExamRecord); 
           system.debug('lstRejectHistory---->'+ lstRejectHistory); 
        }
    }
    public void updateExamDetails()
    {
        system.debug('editableExamRecord ---->'+ editableExamRecord); 
        if(editableExamRecord != null)
        {    
            editableExamRecord.Status__c = 'Under Approval';
        }
        if(!Test.isRunningTest())
        update(editableExamRecord);
        // MYG-2840 Start
        List<PEME_Exam_Detail__c> rejectedNeedInputCount = [SELECT Status__c FROM PEME_Exam_Detail__c WHERE Invoice__c =: editableExamRecord.Invoice__c AND Status__c = 'Rejected-Need Input'];
        System.debug('rejectedNeedInputCount --->'+rejectedNeedInputCount.size());
        if(rejectedNeedInputCount != null && rejectedNeedInputCount.size() == 0)
        {   
            MyGardHelperCtrl.createCommonData();
            GardUtils.usr = MyGardHelperCtrl.usr;   //SF-3976
            // Send mail when any record gets requires approval --> MYG-2840
            GardUtils.sendPEMERejectMail(editableExamRecord.Invoice__c, 'MyGard-PEME examination details rejection - Notification to Gard HK');
         
        }
        // MYG-2840 End 
        generateExams();
        isEditClicked = false;
    }
    public void closeEditPopup()
    {
        isEditClicked = false;
        List<Id> lastProcessId = new List<Id>();            
        lastProcessId.clear();
    }
    public void deleteRecord()
    {
        
        if(deleteExamId!= null)
        {          
          PEME_Exam_Detail__c deletePemeExam = [SELECT Age__c,Amount__c,Date__c,Examination__c,Examinee__c,Id,Invoice__c,Object_Related__c,Object_Related__r.Name,Rank__c,Status__c,Comments__c,GARD_Comments__c FROM PEME_Exam_Detail__c where id=:deleteExamId ];
          if(deletePemeExam != null)
          {
              deletePemeExam.Status__c='Rejected-Closed';
              deletePemeExam.Comments__c='Deleted by clinic';
              if(!Test.isRunningTest())
              update deletePemeExam;
          } 
           generateExams();
           isEditClicked = false;
           deleteExamId = null;
        }
    }
    public void fetchInvoiceDetails()
    {
        isEditClicked = true;
        if(objInvoice != null)
        {
            objInv = objInvoice;
        }
    } 
    
    public void updateInvoiceDetails() 
    {
        System.debug('objInv is:'+ objInv);
        if(objInv != null)
        {
            System.debug('objInv is:'+ objInv);
            objInv.Status__c = 'Under Approval';
            update objInv;  
        }
        isSuccess = false;
    }
    public void clancel()
    {
        isEditClicked = false;
        isSuccess = true;
        system.debug('Call cancel--------------');
    }
     public pageReference goBankDetails()
     {
         if(objInv != null)
        {
            System.debug('objInv is:'+ objInv);
            update objInv;  
        }
        PageReference pg = new PageReference('/apex/PEMEClinicBankDetails?invoices='+selectedInvoice);
        pg.setRedirect(true);
        return pg;
     }
     public pageReference goInvoiceDetails()
     {
         if(objInv != null)
        {
            System.debug('objInv is:'+ objInv);
            update objInv;  
        }
        PageReference pg = new PageReference('/apex/PEMEClinicInvoiceDetails?invoices='+selectedInvoice);
        pg.setRedirect(true);
        return pg;
     }
}