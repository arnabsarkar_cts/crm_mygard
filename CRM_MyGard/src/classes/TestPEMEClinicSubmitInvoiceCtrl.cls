@isTest(seeAllData=false)
public class TestPEMEClinicSubmitInvoiceCtrl
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static List<PEME_Debit_note_detail__c> pdnList = new List<PEME_Debit_note_detail__c>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    public static PEME_Invoice__c pi=new PEME_Invoice__c ();
    public static PEME_Exam_Detail__c PExamDetail = new PEME_Exam_Detail__c();
    public static PEME_Enrollment_Form__c pef, pef1;
    public static PEME_Manning_Agent__c  pmg; 
    Public static List<Object__c> ObjectList=new List<Object__c>();
    Public static Asset brokerAsset_1st ;
    Public static contract brokercontract_1st;
    public static List<PEME_Exam_Detail__c> PExamDetailLst = new List<PEME_Exam_Detail__c>();
    public static EmailTemplate et = new EmailTemplate();
    public static Market_Area__c Markt;
    public static User user1 , clinicUser ,clientUser;
    public static Account clientAcc , clinicAcc;
    public static Contact clientContact , clinicContact;
    public static Object__c Object1, Object2;
    public static PEME_Debit_note_detail__c pdn , pdn2;
    public static PEME_Contact__c pcTest;
    
    public static void createData(){
        CommonVariables__c cvar = new CommonVariables__c(Name='ReplyToMailId',Value__c='test@example.com');
        insert cvar;
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        Markt = new Market_Area__c(Market_Area_Code__c = 'ab12000');
        insert Markt;
        
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;
        Gard_Team__c gardTeam=new Gard_Team__c(P_I_email__c='test@test.com',Region_code__c='CASM', Office__c='Arendal');
        insert gardTeam;
        Valid_Role_Combination__c vrcESP = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
        insert vrcESP;
        Country__c country=new Country__c(Claims_Support_Team__c=gardTeam.id);
        insert country;
        
        //User creation
        user1 = new User(
                            Alias = 'standt', 
                            profileId = salesforceLicenseId ,
                            Email='standarduser@testorg.com',
                            EmailEncodingKey='UTF-8',
                            CommunityNickname = 'test13',
                            LastName='Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',  
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='test008@testorg.com',
                            isActive = true ,
                            ContactId__c = gc.id,
                            City= 'Arendal'
                             );
        insert user1 ;
        
        //Client account 
        Blob b1 = Crypto.GenerateAESKey(128);            //sfedit commented by pulkit 88-92
        String h1 = EncodingUtil.ConvertTohex(b1);            
        String guid1 = h1.SubString(0,8)+ '-' + h1.SubString(8,12) + '-' + h1.SubString(12,16) + '-' + h1.SubString(16,20);
        
        clientAcc = New Account();
        clientAcc.Name = 'Testt';
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.Area_Manager__c = user1.id;        
        clientAcc.Market_Area__c = Markt.id; 
        clientAcc.GUID__c = guid1;
        clientAcc.company_Role__c = 'Client';
        clientAcc.PEME_Enrollment_Status__c='Enrolled';  
        clientAcc.company_Role__c = 'External Service Provider';
        clientAcc.Sub_Roles__c = 'ESP - Agent';
        clientAcc.country__c = country.Id;
        AccountList.add(clientAcc);
        //Clinic account
        Blob b = Crypto.GenerateAESKey(128);            
        String h = EncodingUtil.ConvertTohex(b);            
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20);
        
        clinicAcc = new Account( Name='testClinic',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.ESP_Contact_Record_Type,
                                        company_Role__c='External Service Provider',
                                        Sub_Roles__c = 'ESP - Agent',
                                        Site = '_www.ctsclinic.se',
                                        Type = 'Surveyor' ,
                                        Area_Manager__c = user1.id,      
                                        Market_Area__c = Markt.id,
                                        GUID__c = guid,
                                        PEME_Enrollment_Status__c='Enrolled',
                                        country__c = country.Id
                                     );
        AccountList.add(clinicAcc);
        //insert AccountList; 
        insert AccountList;
        
        //Client Contact
        clientContact = new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        //Clinic contact
        clinicContact = new Contact( 
                                             FirstName='medicalTest',
                                             LastName='medical',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = clinicAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(clinicContact) ;
        insert ContactList;
        //Partner user
        clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
                
         UserList.add(clientUser) ;
         
         clinicUser = new User(
                                    Alias = 'Clinic', 
                                    profileId = partnerLicenseId,
                                    Email='testClinic120@test.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='ClinicTesting',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'clinicUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testClinicUser@testorg.com.mygard',
                                    ContactId = clinicContact.Id
                                   );
        UserList.add(clinicUser);
        //insert UserList;
        insert UserList;
        
        //Object Creation
        Object1 = new Object__c(Name= 'TestObject1',
                                         Object_Unique_ID__c='test11223'
                                         );
         ObjectList.add(Object1);
         Object2 = new Object__c(
                                         Name= 'TestObject2',
                                         Object_Unique_ID__c='test11224'
                                         );
         ObjectList.add(Object2);
         insert ObjectList;
         
         //Enrollment Form
         List<PEME_Enrollment_Form__c> pefs = new List<PEME_Enrollment_Form__c>();
         pef = new PEME_Enrollment_Form__c ();
         pef.ClientName__c=clientAcc.ID;
         pef.Comments__c='Report analyzed';
         pef.Enrollment_Status__c= 'Enrolled';
         pef.Gard_PEME_References__c='Test References1,Test References2';
         pef.Last_Status_Change_Date__c=Date.valueOf('2016-01-27');
         pef.Submitter__c=clinicContact.ID;
         System.debug('######1.5######');
        pefs.add(pef);
        //insert pef;
         System.debug('######2######');
        
         //Enrollment Form-2
         /*pef = new PEME_Enrollment_Form__c ();
         pef.ClientName__c=clientAcc.ID;
         pef.Comments__c='Report analyzed';
         pef.Enrollment_Status__c= 'Disenrolled';
         pef.Gard_PEME_References__c='Test References1,Test References2';
         pef.Last_Status_Change_Date__c=Date.valueOf('2016-01-27');
         pef.Submitter__c=clinicContact.ID;
         System.debug('######1.5######');
        pefs.add(pef);
         //insert pef;*/
         System.debug('######2######');
        insert pefs;
         
         //Manning agent
         pmg = new PEME_Manning_Agent__c  ();
         pmg.Client__c=clinicAcc.ID;
         pmg.PEME_Enrollment_Form__c=pef.ID;
         pmg.Company_Name__c='Test Company';
         pmg.Contact_Point__c=clinicContact.Id;
         pmg.Status__c='Approved';
         insert pmg;
         
         //Debit Note detail
         pdn = new PEME_Debit_note_detail__c();
         pdn.Comments__c = 'testing';
         pdn.Company_Name__c=clientAcc.ID;
         pdn.Contact_person_Email__c = 'test@mail.com';
         pdn.Contact_person_Name__c = 'tester';
         pdn.PEME_Enrollment_Form__c = pef.ID;
         pdn.Requested_Address__c='test1';
         pdn.Requested_Address_Line_2__c='test2';
         pdn.Requested_Address_Line_3__c='test3';
         pdn.Requested_Address_Line_4__c='test4';
         pdn.Requested_City__c='testCity';
         pdn.RequestedComments__c = 'testing';
         pdn.Requested_Contact_person_Email__c = 'test@mail.com';
         pdn.Requested_Contact_person_Name__c = 'tester';
         pdn.Requested_Country__c ='testCountry';
         pdn.Requested_State__c='testState';
         pdn.Requested_Zip_Code__c='testZip';
         pdn.Status__c='Pending Update';
         //insert pdn;
        pdn2 = new PEME_Debit_note_detail__c();
        pdn2.Comments__c = 'testing';
        pdn2.Company_Name__c=clientAcc.ID;
        pdn2.Contact_person_Email__c = 'test@mail.com';
        pdn2.Contact_person_Name__c = 'tester';
        //pdn2.Country__c ='testCountry';
        pdn2.PEME_Enrollment_Form__c = pef.ID;
        pdn2.Requested_Address__c='test1';
        pdn2.Requested_Address_Line_2__c='test2';
        pdn2.Requested_Address_Line_3__c='test3';
        pdn2.Requested_Address_Line_4__c='test4';
        pdn2.Requested_City__c='testCity';
        pdn2.RequestedComments__c = 'testing';
        pdn2.Requested_Contact_person_Email__c = 'test@mail.com';
        pdn2.Requested_Contact_person_Name__c = 'tester';
        pdn2.Requested_Country__c ='testCountry';
        pdn2.Requested_State__c='testState';
        pdn2.Requested_Zip_Code__c='testZip';
        pdn2.Status__c='Approved';
        //insert pdn2;
        pdnList.add(pdn2);
        pdnList.add(pdn);
        insert pdnList;
        //Update debit note detail
        //Commented on 2018R1 for too many soql
        //PEME_Debit_note_detail__c pdn1=[select id,Status__c from PEME_Debit_note_detail__c where id =: pdn.id];
        //pdn.Status__c='Approved';
        //update pdn;
         //Update enrollment form
         //pef.Enrollment_Status__c='Enrolled';
         //update pef;
         
         //PEME Invoice
        pi.Client__c=ClientAcc.id;
        pi.Clinic_Invoice_Number__c='qwerty123';
        pi.Clinic__c=clinicAcc.id;
        pi.Crew_Examined__c=11223;
        pi.Invoice_Date__c=Date.valueOf('2016-01-05');
        pi.PEME_reference_No__c='aasd23';
        pi.Status__c='Draft';
        pi.ObjectList__c='qwerty for test purpose';
        pi.Total_Amount__c=112230;
        pi.Vessels__c= Object1.id;
        pi.PEME_Enrollment_Detail__c = pef.id;
        pi.Period_Covered_From__c=Date.valueOf('2016-01-05');
        pi.Period_Covered_To__c =Date.valueOf('2016-01-28');
        pi.Brief_Description__c = 'test';
        pi.Bank_address__c = 'test';
        pi.Manning_agent__c = pmg.Id;
        insert pi;
        
        //PEME Exam Detail
        PExamDetail.Age__c=30;
        PExamDetail.Amount__c=12000;
        PExamDetail.Date__c=Date.valueOf('2016-01-10');
        PExamDetail.Examination__c='Demo Exam';
        PExamDetail.Examinee__c='Demo employee';
        PExamDetail.Invoice__c=pi.id;
        PExamDetail.Object_Related__c=Object1.id;
        PExamDetail.Rank__c='Five';
        PExamDetail.Status__c='Approved';
        insert PExamDetail;
        PExamDetailLst.add(PExamDetail);
        
        //PEME Contact
        pcTest = new PEME_Contact__c();
        pcTest.Contact__c = clientContact.ID;
        pcTest.PEME_Enrollment_Detail__c = pef.ID;
        Insert pcTest;
        
       /* Gard_Team__c a_gardTeam = new Gard_Team__c();
        a_gardTeam.P_I_email__c='test@test.com';
        a_gardTeam.Region_code__c='CASM';
        a_gardTeam.Office__c='Arendal';
        insert a_gardTeam; */
    }
    Static testMethod void cover()
    {
        //test.startTest();
        TestPEMEClinicSubmitInvoiceCtrl.createData();
        System.runAs(clinicUser)
        {
            
            System.debug('inside run&*&*&*');
            
            //PEMEClinicSubmitInvoiceCntl1.invoice.id=pi.id;
            
            PageReference PageRef = page.PEMEClinicSubmitInvoice2;
            Test.setCurrentPage(PageRef);
           //System.currentPageReference().getParameters().put('id' ,pi.guid__c);-------------------------------------------------
           PEME_Invoice__c piGuid = [SELECT Id,guid__c FROM PEME_Invoice__c WHERE Id =: pi.Id];
            System.currentPageReference().getParameters().put('id' ,piGuid.guid__c);
            test.startTest();
            PEMEClinicSubmitInvoiceCtrl PEMEClinicSubmitInvoiceCntl1 = new PEMEClinicSubmitInvoiceCtrl();
            test.stopTest();
            //System.assertEquals(pi.guid__c,ApexPages.currentPage().getParameters().get('Id'));
          
              }
          //test.stopTest();
    }
        Static testMethod void cover3()
        {
        test.startTest();
        TestPEMEClinicSubmitInvoiceCtrl.createData();
        System.runAs(clinicUser)
        {
            
            PageReference PageRef1 = page.PEMEClinicSubmitInvoice3;
            Test.setCurrentPage(PageRef1);
            System.currentPageReference().getParameters().put('id' ,pi.guid__c);
            PEMEClinicSubmitInvoiceCtrl PEMEClinicSubmitInvoiceCntl2 = new PEMEClinicSubmitInvoiceCtrl();
            
            PageReference PageRef2 = page.PEMEClinicSubmitInvoice4;
            Test.setCurrentPage(PageRef2);
            System.currentPageReference().getParameters().put('id' ,pi.guid__c);
            PEMEClinicSubmitInvoiceCtrl PEMEClinicSubmitInvoiceCntl3 = new PEMEClinicSubmitInvoiceCtrl();
            
            system.debug('PI.GIUD************'+pi.guid__c);
            system.debug('invoice ID*************'+ApexPages.currentPage().getParameters().get('Id'));
          //   PEMEClinicSubmitInvoiceCtrl PEMEClinicSubmitInvoiceCntl1 = new PEMEClinicSubmitInvoiceCtrl();
          //  PEMEClinicSubmitInvoiceCntl1.goExamPrev(); 
            //System.currentPageReference().getParameters().put('id' ,pi.guid__c);//[select guid__c from PEME_Invoice__c where id =: pi.id].guid__c);
            //ApexPages.currentPage().getParameters().put('id',pi.guid__c); 
            
             }
          test.stopTest();
    }
    
    Static testMethod void cover5()
        {
        test.startTest();
        TestPEMEClinicSubmitInvoiceCtrl.createData();
        System.runAs(clinicUser)
        {
    
         PEMEClinicSubmitInvoiceCtrl PEMEClinicSubmitInvoiceCntl1 = new PEMEClinicSubmitInvoiceCtrl();
          PEMEClinicSubmitInvoiceCntl1.goExamPrev(); 
            //System.currentPageReference().getParameters().put('id' ,pi.guid__c);//[select guid__c from PEME_Invoice__c where id =: pi.id].guid__c);
            //ApexPages.currentPage().getParameters().put('id',pi.guid__c); 
     }
          test.stopTest();
    }
    
    
         Static testMethod void cover4()
        {
        //test.startTest();
        TestPEMEClinicSubmitInvoiceCtrl.createData();
        test.startTest();
        System.runAs(clinicUser)
        {
            
            PEMEClinicSubmitInvoiceCtrl PEMEClinicSubmitInvoiceCntl=new PEMEClinicSubmitInvoiceCtrl();
            PEMEClinicSubmitInvoiceCntl.mapClient=new map<String,Id>();
            PEMEClinicSubmitInvoiceCntl.mapClient.put('testClinic',(pef.ClientName__c +'').substring(0,15));
            System.debug('mapClient-------'+PEMEClinicSubmitInvoiceCntl.mapClient);
            PEMEClinicSubmitInvoiceCntl.selectedclient=clientAcc.ID;
            PEMEClinicSubmitInvoiceCntl.populateRefNo();
            PEMEClinicSubmitInvoiceCntl.populateAgent();
            PEMEClinicSubmitInvoiceCntl.populateObject();
            PEMEClinicSubmitInvoiceCntl.populateBankDetails();
            PEMEClinicSubmitInvoiceCntl.getCName();
            PEMEClinicSubmitInvoiceCntl.addMore();
            PEMEClinicSubmitInvoiceCntl.getExamination();
            PEMEClinicSubmitInvoiceCntl.getClientLst();
            PEMEClinicSubmitInvoiceCntl.getAge();
            PEMEClinicSubmitInvoiceCntl.getRefNo();
            PEMEClinicSubmitInvoiceCntl.getSelectObj();
            PEMEClinicSubmitInvoiceCntl.getObjectLst();
            PEMEClinicSubmitInvoiceCntl.getAgentLst();
            PEMEClinicSubmitInvoiceCntl.checkExistedInvoice();
            PEMEClinicSubmitInvoiceCntl.deleteId=String.valueOf(PExamDetail.id);
            PEMEClinicSubmitInvoiceCntl.deleteRecord(); 
          //  PEMEClinicSubmitInvoiceCntl.removeExam();
            //PEMEClinicSubmitInvoiceCntl.periodFrom='fromdate';
            //-commented by -arpan
            //PEMEClinicSubmitInvoiceCntl.goExamdetailsNext();
            PEMEClinicSubmitInvoiceCntl.goInvoicePrev();
            //PEMEClinicSubmitInvoiceCntl.goBankDetailNext();
           // PEMEClinicSubmitInvoiceCntl.goExamPrev();
            PEMEClinicSubmitInvoiceCntl.goBankPrev();
            PEMEClinicSubmitInvoiceCntl.goYes();
            PEMEClinicSubmitInvoiceCntl.goNo();
            PEMEClinicSubmitInvoiceCntl.cancel();
            PEMEClinicSubmitInvoiceCntl.goReviewNext();
            
            //PEMEClinicSubmitInvoiceCntl.invoiceId='';
            
            system.debug('invoice client--'+pi.client__c+'--inv Id from main class--'+String.valueOf(pi.ID).substring(0,15));
        }
        test.stopTest();
    }
    Static testMethod void cover2()
    {
        
        TestPEMEClinicSubmitInvoiceCtrl.createData();
        System.runAs(clinicUser){
            test.startTest();
            PageReference PageRef = page.PEMEClinicSubmitInvoice2;
            Test.setCurrentPage(PageRef);
            PEME_Invoice__c piGuid = [SELECT Id,guid__c FROM PEME_Invoice__c WHERE Id =: pi.Id];
            System.currentPageReference().getParameters().put('id' ,piGuid.guid__c);
            PEMEClinicSubmitInvoiceCtrl PEMEClinicSubmitInvoiceCntl1 = new PEMEClinicSubmitInvoiceCtrl();
            PEMEClinicSubmitInvoiceCntl1.selectedclient=clientAcc.ID;
            PEMEClinicSubmitInvoiceCntl1.populateAgent();
           // PEMEClinicSubmitInvoiceCntl1.goExamdetailsNext();
            gardUtils.sendEnrollmentSubmissionEmails(pef.ID);
            gardUtils.updateGUIDExam(PExamDetailLst);
         // gardUtils.sendPEMERejectMail(String.valueOf(pi.ID).substring(0,15),'MyGard-PEME examination details rejection - Notification to Gard HK');
         //   gardUtils.sendPEMEDeleteMail(String.valueOf(pi.ID),'MyGard-PEME examination details rejection - Notification to Gard HK');
            gardUtils.sendPEMERejectMail(String.valueOf(pi.ID),'MyGard-PEME examination details rejection - Notification to clinics');
            test.stopTest();
        }
        
    }
        
}