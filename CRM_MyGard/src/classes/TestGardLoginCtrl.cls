@isTest
public class TestGardLoginCtrl{
    Public static testmethod void cover()
    {
        GardTestData test_rec = new GardTestData();
        test_rec.commonrecord();
        LogInDownTime__c downTime = new LogInDownTime__c (
                                                             name = 'DetailMessage',
                                                             Message__c = 'MyGard is currently down for maintenance and is expected to be back in normal operation by 20th April 08:00 Central European Time. We apologize for the inconvenience.'
                                                          );
        insert downTime;
        Browser_Worning__c browWarn = new Browser_Worning__c (
                                                                  name = 'DetailMessage',
                                                                  Message__c = 'MyGard has detected an incompatible browser. Please refer to the user manual for a list of supported browsers.'
                                                              );
        insert browWarn;
        
        System.runAs(GardTestData.brokerUser)
        {
            Test.startTest();
            System.assertEquals('DetailMessage',downTime.name);
            GardLoginCtrl glcBroker = new GardLoginCtrl();
            PageReference pfbroker1 = glcBroker.forwardToCustomAuthPage();
            PageReference pfbroker2 = glcBroker.forgotPassword();
            ApexPages.currentPage().getParameters().put('startURL','www.gard.com');
            //PageReference pfbroker3 = glcBroker.login();
            glcBroker.isRemember = true;
            glcBroker.userName = 'testUser';
            glcBroker.password = '12345';
            PageReference pfbroker3 = glcBroker.login();
            glcBroker.topMessage = 'message on top';
            glcBroker.detailMessage = 'message in detail';
            glcBroker.BrowsertopMessage = 'browser top message';
            glcBroker.BrowserDetailMessage = 'browser detail message';
        }
        
        System.runAs(GardTestData.clientUser)
        {
            GardLoginCtrl glcClient = new GardLoginCtrl();
            PageReference pfclient1 = glcClient.forwardToCustomAuthPage();
            PageReference pfclient2 = glcClient.forgotPassword();
            ApexPages.currentPage().getParameters().put('startURL','www.gard.com');
            //PageReference pfclient3 = glcClient.login();
            glcClient.isRemember = true;
            glcClient.userName = 'testUser';
            glcClient.password = '12345';
            PageReference pfclient3 = glcClient.login();
            glcClient.topMessage = 'message on top';
            glcClient.detailMessage = 'message in detail';
            glcClient.BrowsertopMessage = 'browser top message';
            glcClient.BrowserDetailMessage = 'browser detail message';
            Test.stopTest();
        }
    }
}