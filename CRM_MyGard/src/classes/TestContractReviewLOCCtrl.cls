//Test class for ContractReviewLOCCtrl .
@isTest(seealldata=false)
public class TestContractReviewLOCCtrl{
///////*******Variables for removing common literals********************//////////////
    public static string testStr =  'test' ; 
/////**********************end**************************/////
//Test method..............................
    public static testmethod void ContractReview(){
//Create test data.........................      
        GardTestData testRec  = new GardTestData();
        testRec.customsettings_rec();
        testRec.commonrecord();
        testRec.ViewDoc_testrecord();
        system.debug('***********Accmap_1 '+GardTestData.Accmap_1 );
        system.debug('***********Accmap_2 '+GardTestData.Accmap_2 ); 
        GardtestData.test_object_1st.guid__c = '8ce8ac89-a6fd-1836-9e07';
        update GardtestData.test_object_1st; 
        Attachment attach=new Attachment();
        attach.name='Unit Test Attachment';
        attach.body=Blob.valueOf('Unit Test Attachment Body');
        attach.parentId= GardTestData.brokerCase.id;
        attach.ContentType = 'application/pdf';
        insert attach;
        system.assertEquals(attach.name , 'Unit Test Attachment');
       
        System.runAs(GardTestData.brokerUser){    
            test.startTest();
            ContractReviewLOCCtrl cr = new ContractReviewLOCCtrl();
            cr.imoNo = 12;
            cr.noImoNo = true;
            cr.lstObject =new List<Object__c>();
            cr.contractName =  testStr ;
            cr.isConfidential = true;
            cr.isLocReqd = true;
            cr.comments =  testStr ;
            cr.lOCType =  testStr ;
            cr.objMode = true;
            cr.prvUrl =  testStr ;
            cr.isFav = true;
            cr.newFilterName =  testStr ;
            ApexPages.StandardController scontroller = new ApexPages.StandardController(GardTestData.brokerCase);
            ContractReviewLOCCtrl MyObject=new ContractReviewLOCCtrl(scontroller);
            cr.selectedCover = GardTestData.brokerAsset_2nd.id;
            cr.selectedClient = GardTestData.brokerAcc.id;
            //  cr.mapCover.put(GardTestData.brokerAsset_1st.id,GardTestData.brokerAsset_1st);
            cr.strSelected = 'ABc';
            cr.isPhone = true;
            // cr.caseIns.id= GardTestData.brokerCase.id;
            cr.chkAccess();
            cr.caseIns.OwnerId = GardTestData.brokerUser.id;
            cr.caseIns.accountId = GardTestData.brokerAcc.id;
            cr.caseIns.Type = 'Crew Contract Review';
            cr.caseIns.Name_Of_Contract__c =  testStr ;
            cr.caseIns.Status = 'DM Synchronising';
            cr.caseIns.ContactId = GardTestData.brokerContact.id;
            cr.mapClient.put(GardTestData.brokerAcc.id, testStr );
            List<SelectOption> Countrycode= cr.getCountrycodes();
            List<SelectOption> ClientOp= cr.getClientLst();
            cr.setGlobalClientIds = null;
            List<SelectOption> ClientOp2= cr.getClientLst();
            List<SelectOption> CoverOp= cr.getCoverLst();
            ApexPages.CurrentPage().getParameters().put('id',GardTestData.brokerCase.id);
            cr.ws_ConfirmationRequest();
            //  cr.previewDocWS();
            ApexPages.CurrentPage().getParameters().put('id',GardTestData.clientCase.id);
            // cr.ConfirmationRequest();
            cr.previewDocWS();
            System.assertEquals(ContractReviewLOCCtrl.showPreviewMsg ,  'Sorry, you are not the owner of this case.');
            // cr.LOCSendEmailRequestWS();
            List<SelectOption> TypeOfLoc= cr.getTypeOfLocLst();
            ApexPages.CurrentPage().getParameters().put('id',GardTestData.brokerCase.id);
            cr.reDirectToCase();
            cr.relatedLstDocs = new List<gardNoDm.DocumentBase>();
            cr.displayDoc();
            cr.retrieveCoverLst();
            cr.selectedCover = GardTestData.brokerAsset_1st.id;
            cr.retrieveAssetLst();
            List<SelectOption> ObjectOp= cr.getObjectLst();
            cr.selectedMultiObj.add(GardTestData.test_object_1st.id);
            cr.selectedCover = null;
            test.stopTest();
        }
    }
}