public class EventRelationHelper 
{
   // public static boolean runOnce = true;
    public static  List<EventRelation> whoRelations;
    public static  List<EventRelation> evntInvitees;
    public Set<ID> EventWhoIds = new Set<ID>();
    public static void FetchChildRecord(Event e_new,String ev)
    {
        List<EventRelation> evrLst = new List<EventRelation>();
        List<Event> evntLst=[select Meeting_ID__c,CurrencyIsoCode,IsAllDayEvent,OwnerId,ActivityDate,Purpose__c,Description,DurationInMinutes,EndDateTime,RecordTypeId,Location,Location__c,WhoId,IsPrivate,IsReminderSet,ShowAs,StartDateTime,Subject,ActivityDateTime,Type,Hospitality_Gift_accepted_or_offered__c,Gift_given__c,Gift__c,Hospitality__c,Hospitality_given__c,Estimated_value__c,Description_hospitality__c FROM Event where (Meeting_ID__c =:ev or Meeting_ID__c =:e_new.id) and IsDuplicate__c = true and ownerId=:e_new.ownerid];
        //if(test.isRunningTest()){evntLst = testTrigEventAIAU.createEvent(e_new);}
        system.debug('The event id *** is '+e_new);
        system.debug('The Event list returned is ***'+evntLst.size()+'size is***'+evntLst);
        system.debug('The string id returned is '+ev);
        for(Integer i=0;i<evntlst.size();i++)
                {
                    evntlst[i].CurrencyIsoCode = e_new.CurrencyIsoCode;
                    evntlst[i].IsAllDayEvent = e_new.IsAllDayEvent;
                    evntlst[i].ActivityDate = e_new.ActivityDate;
                    evntlst[i].Purpose__c = e_new.Purpose__c;
                    evntlst[i].DurationInMinutes = e_new.DurationInMinutes;
                    evntlst[i].EndDateTime = e_new.EndDateTime;
                    evntlst[i].Location = e_new.Location;
                    evntlst[i].Location__c = e_new.Location__c;
                    evntlst[i].type = e_new.Type;
                    evntlst[i].WhoId = e_new.WhoId;
                    evntlst[i].IsPrivate = e_new.IsPrivate;
                    evntlst[i].IsReminderSet = e_new.IsReminderSet;
                    evntlst[i].ShowAs = e_new.ShowAs;
                    evntlst[i].StartDateTime = e_new.StartDateTime;
                    evntlst[i].Subject = e_new.Subject;
                    evntlst[i].ActivityDateTime = e_new.ActivityDateTime;
                    evntlst[i].Description = e_new.Description;
                    evntlst[i].RecordTypeId = System.Label.Related_Event_RecordType;//SF-4310
                }
                try{
                    system.debug('child events size: '+evntlst.size());
                    if(evntlst.size() > 0)
                        update evntlst;
                }
                catch(Exception e){
                    system.debug('Exception Occured '+e);
                }          
         UpdateChildRecord(evntLst); 
     //UpdateChildRecordInsert(evntLst,ev);     
    }

    //SF-4440 Start
    public static void HirerachyEvent(Event e_new)
    {
        String TopLevelparentAccount;
        //Boolean TopLevelparentAccountToBeInserted = true;
        Set<ID> EventWhoIDS1=new Set<ID>();
        Set<ID> EventIDsSet=new Set<ID>();
        List<Event> TopParentlst = new List<Event>();
        List<Event> oldcreatedEvents  = new List<Event>();
        System.debug('e_new.Whatid----'+e_new.Whatid);
        if(e_new.Whatid!=null)
        {
            TopLevelparentAccount = GetTopLevleElement(e_new.Whatid);
        }
        system.debug('TopLevelparentAccount--->'+TopLevelparentAccount);
        //Event evnt1;
        Event ParentEvent;
        system.debug('IsRecurrence2 --GG--'+e_new.IsRecurrence +'e_new.id--GG--'+e_new.id+'--IsDuplicate__c--'+e_new.IsDuplicate__c);
        //String events_id = String.valueOf(e_new.id).substring(0,15).trim();
        /*if(e_new.id!=null)
        {
            evnt1=[SELECT id,Meeting_ID__c,IsDuplicate__c,Description,Location__c,CurrencyIsoCode,IsAllDayEvent,OwnerId,ActivityDate,Purpose__c,DurationInMinutes,EndDateTime,RecordTypeId,Location,WhoId,WhoCount,WhatID,IsPrivate,IsReminderSet,ShowAs,StartDateTime,Subject,ActivityDateTime,Type FROM Event where id=:e_new.id];
        }*/
        if(e_new.IsDuplicate__c!=true)//evnt1.IsDuplicate__c!=true)
        {
            /*system.debug('events_id ----->'+events_id );
            List<Event> NewcreatedEvents = [select id,WhatId,Meeting_Id__c from event where id=:events_id and IsDuplicate__c=true];
            if(NewcreatedEvents!=null && NewcreatedEvents.size()>0)
            {
                String strTemp = String.valueOf(NewcreatedEvents[0].Meeting_Id__c).substring(0,15).trim();
                oldcreatedEvents = [select id,WhatId from Event where Meeting_Id__c Like :strTemp+'%' and IsDuplicate__c=true];
            }
            system.debug('NewcreatedEvents --->'+NewcreatedEvents );
            system.debug('oldcreatedEvents--->'+oldcreatedEvents );

            if(oldcreatedEvents!=null && oldcreatedEvents.size()>0)
            {
                for(Event cEvents : oldcreatedEvents)
                {
                     system.debug('This is executed');
                     system.debug('cEvents.WhatId--->'+cEvents.WhatId);
                     if(TopLevelparentAccount == cEvents.WhatId)
                     {
                         system.debug('entered');
                         TopLevelparentAccountToBeInserted = false;
                     }
                 /*else
                 {
                     TopLevelparentAccountToBeInserted1 = false;
                 }       
                }
            }*/
            if(TopLevelparentAccount!=null && TopLevelparentAccount!=e_new.Whatid)// && TopLevelparentAccountToBeInserted)// && !TopLevelparentAccountToBeInserted1)
            {
               if(e_new!=null)//evnt1!=null)
               {
                    ParentEvent = e_new.clone();//evnt1.clone();
                    ParentEvent.WhatId = TopLevelparentAccount;
                    ParentEvent.Ultimate_Account_Parent__c = true;
                    ParentEvent.RecordTypeId = System.Label.Related_Event_RecordType;
                    ParentEvent.IsDuplicate__c=true;
                    ParentEvent.Meeting_ID__c=e_new.id;
                    ParentEvent.Description_hospitality__c ='';
                    ParentEvent.Hospitality_Gift_accepted_or_offered__c ='';
                    ParentEvent.Gift_given__c = false;
                    ParentEvent.Gift__c = false;
                    ParentEvent.Hospitality__c = false;
                    ParentEvent.Hospitality_given__c =false;
                    ParentEvent.Estimated_value__c =null;
                    TopParentlst.add(ParentEvent);
                 }
            }
            if(TopParentlst.size()>0 && TopParentlst!=null)
            {
                try
                {
                    system.debug('Gard_RecursiveBlocker.blocker-------->'+Gard_RecursiveBlocker.blocker);// = true;
                    insert TopParentlst;
                    system.debug('TopParentlst--->'+TopParentlst);
                }
                catch(DMLException de){
                       System.debug('Exception occurred  - \n'+de);
                    }
            }
         }
        }
         
        public static String GetTopLevleElement( String objId ){
        Boolean topLevelParent = false;
        if(objId!=null && objId!='' && objId.startsWithIgnoreCase('001'))
        {
            while ( !topLevelParent ) {
            Account a = [Select Id, ParentId From Account where Id =: objId limit 1];
                if(a.ParentID != null ) {
                    objId = a.ParentID;
                }
                else{
                    topLevelParent = true;
                }
            }
         }
        return objId ;
    }
    //SF-4440 End
      
    public static void UpdateChildRecord(List<Event> evntLst)
    {  
        List<EventRelation> toBeDeleted = new List<EventRelation>();
        //system.debug('The Ev returned from Trigger is ***'+ev);
       // if(runOnce)
       // {
        List<EventRelation> evrLst = new List<EventRelation>();
        //List<Event> evntLst=[select Meeting_ID__c,CurrencyIsoCode,IsAllDayEvent,OwnerId,ActivityDate,Purpose__c,Description,DurationInMinutes,EndDateTime,RecordTypeId,Location,Location__c,WhoId,IsPrivate,IsReminderSet,ShowAs,StartDateTime,Subject,ActivityDateTime,Type,Hospitality_Gift_accepted_or_offered__c,Gift_given__c,Gift__c,Hospitality__c,Hospitality_given__c,Estimated_value__c,Description_hospitality__c FROM Event where Meeting_ID__c =:ev and IsDuplicate__c = true];
        system.debug('The Event list returned is ***'+evntLst.size()+'size is***'+evntLst);
        Set<ID> EventWhoIds1 = new Set<ID>();
        Set<ID> EventIds = new Set<ID>();
        for(Event childEv:evntLst)
        {
            EventWhoIds1.add(childEv.whoId);
            EventIds.add(childEv.id);
        }
            evrLst=[SELECT Status,EventId,RelationId FROM EventRelation WHERE EventId IN: EventIds AND isWhat = false and RelationId NOT IN :EventWhoIds1];//AND isParent = true
            toBeDeleted.addAll(evrLst);
        
        Database.delete(toBeDeleted,false);
        //evrLst=[SELECT RelationId FROM EventRelation WHERE EventId =:ev AND isParent = true AND isWhat = false and RelationIdNOT IN:PrimaryContact];
        system.debug('The Evrlst returned is****'+evrLst.size());
        system.debug('The to be Deleted is****'+evrLst.size());
          //runOnce = false; 
     //  }  
    }
    @future
    public static void UpdateChildRecordInsert(String ev)
    {
       // if(runOnce)
      // {
        List<Event> evntLst1=[select Meeting_ID__c,CurrencyIsoCode,IsAllDayEvent,OwnerId,ActivityDate,Purpose__c,Description,DurationInMinutes,EndDateTime,RecordTypeId,Location,Location__c,WhoId,IsPrivate,IsReminderSet,ShowAs,StartDateTime,Subject,ActivityDateTime,Type,Hospitality_Gift_accepted_or_offered__c,Gift_given__c,Gift__c,Hospitality__c,Hospitality_given__c,Estimated_value__c,Description_hospitality__c FROM Event where Meeting_ID__c =:ev and IsDuplicate__c = true];
        //system.debug('The meeting id'+ev);
        //system.debug('checkbox---'+evntLst1[0].IsDuplicate__c);
        whoRelations = new List<EventRelation>(); 
        evntInvitees = new List<EventRelation>(); 
        Set<ID> EventWhoIds = new Set<ID>();
        Set<ID> EventIds = new Set<ID>();
         for(Event childEv:evntLst1)
         {
             EventWhoIds.add(childEv.whoId);
             EventIds.add(childEv.id);
         }
         System.debug('The event Who Ids**'+EventWhoIds.size()+'list **'+EventWhoIds);
         for(EventRelation childEvRelation:[SELECT Status,EventId,RelationId,IsInvitee FROM EventRelation WHERE EventId =:ev AND isParent = true AND isWhat = false and RelationId NOT IN :EventWhoIds])
         {
                for(Event childEv:evntLst1)
                {
                            EventRelation evtRel = new EventRelation();
                            evtRel.EventId = childEv.id;
                            evtRel.RelationId = childEvRelation.RelationId;
                            evtRel.isParent = true;
                            evtRel.isWhat = false;
                            evtRel.IsInvitee = childEvRelation.IsInvitee;
                            evtRel.Status = childEvRelation.Status;
                            whoRelations.add(evtRel);
                            system.debug('The number of times the loop is execited '+childEv.id+'---->'+childEvRelation.RelationId);
                }  
          }
          
          for(EventRelation childEvRelation:[SELECT Status,isInvitee,RelationId,isParent FROM EventRelation WHERE EventId =:ev AND isWhat = false and isParent=false and RelationId NOT IN :EventWhoIds])
                   {
                       for(Event childEv:evntLst1)
                       {
                           EventRelation evtRel = new EventRelation();
                            evtRel.EventId = childEv.id;
                            evtRel.RelationId=childEvRelation.RelationId;
                            //evtRel.isParent = true;
                            evtRel.isWhat = false;
                            evtRel.IsInvitee = childEvRelation.IsInvitee; 
                            evtRel.Status = childEvRelation.Status;                                                     
                           whoRelations.add(evtRel);
                       }
                    }      
                    
           List<EventRelation> WhoIdInvitees = [SELECT Status,isInvitee,RelationId,EventId FROM EventRelation WHERE EventId =:ev AND isParent = true AND isWhat = false and RelationId IN:EventWhoIds];
           for(EventRelation chieldEvents:[SELECT Status,isInvitee,RelationId,EventId FROM EventRelation WHERE EventId IN :EventIds AND isParent = true AND isWhat = false and RelationId IN :EventWhoIds])
                 {
                     system.debug('Child Events ***'+chieldEvents);
                     if(WhoIdInvitees[0].isInvitee == true)
                     {
                          chieldEvents.isInvitee = true;
                     }
                     else
                     {
                         chieldEvents.isInvitee = false;
                     }
                     if(WhoIdInvitees[0].isInvitee == true && WhoIdInvitees[0].Status == 'Accepted')
                     {
                         chieldEvents.Status = 'Accepted';
                     }
                     else if(WhoIdInvitees[0].isInvitee == true && WhoIdInvitees[0].Status == 'Rejected')
                     {
                         chieldEvents.Status = 'Rejected';
                     }
                      system.debug('Child Events11 ***'+chieldEvents);
                      evntInvitees.add(chieldEvents);
                 }    
       //}
       system.debug('The list added to whoRelations are***'+whoRelations.size()+'values****'+whoRelations);
       //system.debug('The list added to tobeDeleted are ***'+toBeDeleted.size()+'values***'+toBeDeleted);
              
               if(whoRelations != null && whoRelations.size()>0)
               {
                   //FutureMethodToInsert();
                   system.debug('The EventRelation Inserted ***'+whoRelations.size()+'values**'+whoRelations);
                   //database.insert(whoRelations, false);
                   Database.insert(whoRelations,false);
                   //insert whoRelations;
                   
               } 
               if(evntInvitees!=null && evntInvitees.size()>0)
               {
                   Database.update(evntInvitees,false);
               }
        //  runOnce = false; 
       // }
    }
    public static void DeleteChildRecord(String ev)
    {
        List<Event> AccountId = new List<Event>();
       List<Event> evntlst_update = new List<Event>();
       List<Event> evntlst = new List<Event>();
       List<FeedItem> FeedItemlst=new List<FeedItem>();
       set<ID> EventWhatIDS = new set<ID>();
       set<ID> EventWhatIDS1 = new set<ID>();
       AccountId=[select WhatId from Event where Meeting_Id__c Like :ev+'%'];//=:ev];//String.valueOf(e_old.id).substring(0,15)
       system.debug('The Account Ids are'+AccountId);
       for(Event EventItreator:AccountId)        
       {       
            EventWhatIDS.add(EventItreator.WhatId);     
       }  
       if(EventWhatIDS.size()>0 && EventWhatIDS!=null)
       {
           evntlst = [select Meeting_ID__c,CurrencyIsoCode,IsAllDayEvent,OwnerId,ActivityDate,Purpose__c,Description,DurationInMinutes,EndDateTime,RecordTypeId,Location,Location__c,WhoId,IsPrivate,IsReminderSet,ShowAs,StartDateTime,Subject,ActivityDateTime,Type FROM Event where WhatId IN :EventWhatIDS and Meeting_ID__c Like :ev+'%'];//=:ev];//String.valueOf(e_old.id).substring(0, 15)        
           try
           {
                delete evntlst;
           }
            catch(Exception e){
                system.debug('Exception Occured '+e);
            }
        }
        
        List<task> tasklst=[select id from task where whatId IN :EventWhatIDS and Meeting_ID__c Like :ev+'%'];//=:ev];//e_old.id
            if(tasklst!=null && tasklst.size()>0)
            {
                delete tasklst;
            }
            
         List<Event> evntOldTrigger = Trigger.old;
         for(Event AccountIdIterator:evntOldTrigger)        
         {       
            EventWhatIDS1.add(AccountIdIterator.WhatId);     
         }     
          List<FeedItem> fitemlst =[SELECT id,LinkUrl FROM FeedItem where ParentId IN :EventWhatIDS1 and Title='View Meeting'];
          for(FeedItem f:fitemlst)
          { 
               if(f.LinkUrl!=null)
               {
                   for(Event e1:evntOldTrigger)
                   {
                       if(f.LinkUrl.right(18).substring(0,15) == e1.id || f.LinkUrl.right(18)== e1.id)
                       {
                            FeedItemlst.add(f);
                       }
                   }
               }
           } 
            system.debug('The feeditemlist***'+FeedItemlst);
            delete FeedItemlst;
    }
    
}