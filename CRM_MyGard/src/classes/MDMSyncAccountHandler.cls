/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 06/11/2013
***************************************************************************/
    
/// <summary>
///  Handler class for the Account.MDMSyncAccount trigger
/// </summary>
public without sharing class MDMSyncAccountHandler {
    
    public static Boolean CheckMDMFieldsChanged(List<Account> newAccounts, Map<id, Account> oldAccountsMap) {
        List<MDM_Service_Fields__c> fields = new List<MDM_Service_Fields__c>();
        for (MDM_Service_Fields__c field : MDM_Service_Fields__c.getAll().values()) {
            if ('Account'.equalsIgnoreCase(field.Source_Object__c) && 'Field'.equalsIgnoreCase(field.Type__c) 
                && 'upsertPartnerRequest'.equalsIgnoreCase(field.request_Name__c) && field.Trigger_Synchronisation__c) {
                fields.add(field);
            }
        }
        
        Boolean haveChanged = false;
        for(Account a : newAccounts) {
            Account oldAccount = oldAccountsMap.get(a.ID);
            if (a.Synchronisation_Status__c != 'Synchronised') {
                for(MDM_Service_Fields__c field : fields) {
                    if (a.get(field.Field_Name__c) != oldAccount.get(field.Field_Name__c)) {
                        a.addError(String.format(System.Label.Company_Locked_For_Edit_Due_To_Sync_Status, new List<String> { a.Synchronisation_Status__c}));
                        haveChanged = true;
                        break;
                    }
                }
            }
        }
        
        return haveChanged;
    }
    
    public static void InsertAccountSyncStatus(List<Account> newAccounts, Boolean webServicesEnabled) {
        for(account a : newAccounts) {
            if (webServicesEnabled) {
                if (System.IsBatch() || System.IsFuture()) {
                    a.Synchronisation_Status__c = 'Waiting To Sync';
                    a.Sync_Date__c = Datetime.now().addMinutes(-59);
                } else {
                    a.Synchronisation_Status__c = 'Sync In Progress';
                }
            } else {
                a.Synchronisation_Status__c = 'Synchronised';
            }                   
            a.Synchronisation_Action__c = 'create';
        }
    }
    
    public static void InsertAddressesSyncStatus(List<Account> newAccounts, Boolean webServicesEnabled) {
        for(account a : newAccounts) {
            if (webServicesEnabled) {
                if (a.BillingPostalCode != null || a.BillingStreet != null) {
                    if (System.IsBatch() || System.IsFuture()) {
                        a.Billing_Address_Sync_Status__c = 'Waiting To Sync';
                        a.Sync_Billing_Address_Date__c = Datetime.now().addMinutes(-59);
                    } else {
                        a.Billing_Address_Sync_Status__c = 'Sync In Progress';
                    }
                    a.Billing_Address_Sync_Action__c = 'create';
                }
                if (a.ShippingPostalCode != null || a.ShippingStreet != null) {
                    if (System.IsBatch() || System.IsFuture()) {
                        a.Shipping_Address_Sync_Status__c = 'Waiting To Sync';
                        a.Sync_Shipping_Address_Date__c = Datetime.now().addMinutes(-59);
                    } else {
                        a.Shipping_Address_Sync_Status__c = 'Sync In Progress';
                    }
                    a.Shipping_Address_Sync_Action__c = 'create';
                }
            } else {
                a.Billing_Address_Sync_Status__c = 'Synchronised';
                a.Shipping_Address_Sync_Status__c = 'Synchronised';
            }                   
        }
    }
    
    public static void UpdateAccountSyncStatus(List<Account> newAccounts, Map<id, Account> oldAccountsMap, Boolean webServicesEnabled) {
        System.Debug('*** Inside UpdateAccountSyncStatus ***');
        List<MDM_Service_Fields__c> fields = new List<MDM_Service_Fields__c>();
        for (MDM_Service_Fields__c field : MDM_Service_Fields__c.getAll().values()) {
            if ('Account'.equalsIgnoreCase(field.Source_Object__c) && 'Field'.equalsIgnoreCase(field.Type__c) && 'upsertPartnerRequest'.equalsIgnoreCase(field.request_Name__c) && field.Trigger_Synchronisation__c) {
                fields.add(field);
            }
        }
        
        system.debug('*** before update fieldset - ' + fields);
        for(Account a : newAccounts) {
            Account oldAccount = oldAccountsMap.get(a.ID);
            //Loop through each field in the fieldSet, checking if the value is different between
            //new and old record. If one of these values has changed, we want to sync.
            if (webServicesEnabled) {
                if (a.Synchronisation_Status__c == 'Synchronised') {
                    if (!oldAccount.Deleted__c && a.deleted__c) {
                        if (System.IsBatch() || System.IsFuture()) {
                            a.Synchronisation_Status__c = 'Waiting To Sync';
                            a.Sync_Date__c = Datetime.now().addMinutes(-59);
                        } else {
                            a.Synchronisation_Status__c = 'Sync In Progress';
                        }
                        a.Synchronisation_Action__c = 'update';
                    } else if (!a.deleted__c) {
                        for(MDM_Service_Fields__c field : fields) {
                            Boolean isThereAnyChange = false;
                            if((a.get(field.Field_Name__c)!=null) && (oldAccount.get(field.Field_Name__c)!=null)){
                                String newValue = String.valueOf(a.get(field.Field_Name__c));
                                String oldValue = String.valueOf(oldAccount.get(field.Field_Name__c));
                                if(newValue.equals(oldValue)){
                                    isThereAnyChange=false;
                                }else{
                                    isThereAnyChange=true;
                                }
                            }else{
                                if(a.get(field.Field_Name__c) != oldAccount.get(field.Field_Name__c)){
                                    isThereAnyChange=true;
                                }else{
                                    isThereAnyChange=false;
                                }
                            }
                            if(isThereAnyChange){   
                                if (System.IsBatch() || System.IsFuture()) {
                                    a.Synchronisation_Status__c = 'Waiting To Sync';
                                    a.Sync_Date__c = Datetime.now().addMinutes(-59);
                                } else {
                                    a.Synchronisation_Status__c = 'Sync In Progress';
                                }
                                a.Synchronisation_Action__c = 'update';
                                break;
                            }
                        }
                    } 
                }
            } else {
                a.Synchronisation_Status__c = 'Synchronised';
                a.Synchronisation_Action__c = 'update';
            }
        }
    }
    
    public static void UpdateAddressesSyncStatus(List<Account> newAccounts, Map<id, Account> oldAccountsMap, Boolean webServicesEnabled) {
        boolean isActivated = false;
        List<MDM_Service_Fields__c> fields = new List<MDM_Service_Fields__c>();
        for (MDM_Service_Fields__c field : MDM_Service_Fields__c.getAll().values()) {
            if ('Account'.equalsIgnoreCase(field.Source_Object__c) && 'Field'.equalsIgnoreCase(field.Type__c)  && field.Trigger_Synchronisation__c) {
                fields.add(field);
            }
        }
        
        system.debug('*** before update fieldset - ' + fields);
        for(Account a : newAccounts) {
            Account oldAccount = oldAccountsMap.get(a.ID);
            //Loop through each field in the fieldSet, checking if the value is different between
            //new and old record. If one of these values has changed, we want to sync.
            if (webServicesEnabled && a.Company_Status__c != 'Inactive') {
                if(a.Company_Status__c != 'Inactive' && oldAccount.Company_Status__c == 'Inactive'){
                    isActivated = true;
                    a.Deleted__c = false;
                }
                for(MDM_Service_Fields__c field : fields) {
                    if (('upsertAddressRequest'.equalsIgnoreCase(field.request_Name__c) && a.get(field.Field_Name__c) != oldAccount.get(field.Field_Name__c)) || isActivated) {
                        if ('Billing'.equalsIgnoreCase(field.address_Type__c)) {
                            if (System.IsBatch() || System.IsFuture()) {
                                a.Billing_Address_Sync_Status__c = 'Waiting To Sync';
                                a.Sync_Billing_Address_Date__c = Datetime.now().addMinutes(-59);
                            } else {
                                a.Billing_Address_Sync_Status__c = 'Sync In Progress';
                            }
                            if (oldAccount.get(field.Field_Name__c) == null) { 
                                a.Billing_Address_Sync_Action__c = 'create';
                            } else if(a.Billing_Address_Sync_Action__c != 'create'){
                                a.Billing_Address_Sync_Action__c = 'update';
                            }
                        } else if ('Shipping'.equalsIgnoreCase(field.address_Type__c)) {
                            if (System.IsBatch() || System.IsFuture()) {
                                a.Shipping_Address_Sync_Status__c = 'Waiting To Sync';
                                a.Sync_Shipping_Address_Date__c = Datetime.now().addMinutes(-59);
                            } else {
                                System.Debug('Updating Shipping_Address_Sync_Status__c to Sync in Progress');
                                a.Shipping_Address_Sync_Status__c = 'Sync In Progress';
                            }
                            if (oldAccount.get(field.Field_Name__c) == null) { 
                                a.Shipping_Address_Sync_Action__c = 'create';
                            } else if(a.Shipping_Address_Sync_Action__c != 'create'){
                                a.Shipping_Address_Sync_Action__c = 'update';
                            }
                        }
                    }
                }
            } else {
                a.Billing_Address_Sync_Status__c = 'Synchronised';
                a.Shipping_Address_Sync_Status__c = 'Synchronised';
            }
        }
    }
    
    public static void SyncPartner(List<Account> newAccounts, Map<id, Account> newAccountsMap, Boolean webServicesEnabled) {
        List<id> accountIds = new List<id>();
        accountIds.addAll(newAccountsMap.keyset());
        if (webServicesEnabled) {
            if (!System.IsBatch() && !System.IsFuture()) {
                MDMPartnerProxy.MDMUpsertPartnerAsync(accountIds);
            }
        }
    }
    
    public static void SyncPartner(List<Account> newAccounts, Map<id, Account> newAccountsMap, List<Account> oldAccounts, Map<id, Account> oldAccountsMap, Boolean webServicesEnabled) {
        if (webServicesEnabled) {
            //***The section below is for a Webservice that can handle a list of Accounts***//
            list<id> UpsertAccountIds = new list<id>();
            list<id> DeleteAccountIds = new list<id>();
            
            for(account a : newAccounts) {
                account oldAccount = oldAccountsMap.get(a.ID);
                system.debug('*** After Update a.syncstatus = '+a.synchronisation_Status__c);
                system.debug('*** After Update oldAccount.syncstatus = '+oldAccount.synchronisation_Status__c);                 
                
                if (a.synchronisation_Status__c == 'Sync In Progress' && oldAccount.Synchronisation_Status__c != 'Sync In Progress'){
                    if (a.Deleted__c) {
                        system.debug('*** After Update (DELETE) calling sync for Account : '+a.id); 
                        DeleteAccountIds.add(a.id); 
                    } else {
                        system.debug('*** After Update (UPSERT) calling sync for Account : '+a.id); 
                        UpsertAccountIds.add(a.id); 
                    }
                }
            }
            
            if (UpsertAccountIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    MDMPartnerProxy.MDMUpsertPartnerAsync(UpsertAccountIds);
                }
            }
            if (DeleteAccountIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    MDMPartnerProxy.MDMDeletePartnerAsync(DeleteAccountIds);
                }
            }
        }
    }
    
    public static void SyncAddresses(List<Account> newAccounts, Map<id, Account> newAccountsMap, Boolean webServicesEnabled) {
        list<id> UpsertBillingAddressIds = new list<id>();
        list<id> UpsertShippingAddressIds = new list<id>();
        
        if (webServicesEnabled) {   
            for(account a : newAccounts) {
                if (a.Billing_Address_Sync_Status__c == 'Sync In Progress') {
                    system.debug('*** After Update (UPSERT) calling sync for Billing Address : '+a.id); 
                    UpsertBillingAddressIds.add(a.id);  
                }
                if (a.Shipping_Address_Sync_Status__c == 'Sync In Progress') {
                    system.debug('*** After Update (UPSERT) calling sync for Shipping Address : '+a.id);    
                    UpsertShippingAddressIds.add(a.id); 
                }
            }
            
            if (UpsertBillingAddressIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    MDMAddressProxy.MDMUpsertBillingAddressAsync(UpsertBillingAddressIds);
                }
            }
            if (UpsertShippingAddressIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    MDMAddressProxy.MDMUpsertShippingAddressAsync(UpsertShippingAddressIds);
                }
            }
        }
    }
    
    public static void SyncAddresses(List<Account> newAccounts, Map<id, Account> newAccountsMap, List<Account> oldAccounts, Map<id, Account> oldAccountsMap, Boolean webServicesEnabled) {
        if (webServicesEnabled) {
            
            list<id> UpsertBillingAddressIds = new list<id>();
            list<id> DeleteBillingAddressIds = new list<id>();
            
            list<id> UpsertShippingAddressIds = new list<id>();
            list<id> DeleteShippingAddressIds = new list<id>();
            
            for(account a : newAccounts) {
                account oldAccount = oldAccountsMap.get(a.ID);
                System.Debug('*** New Value: ' + a.Billing_Address_Sync_Status__c + ' Old Value: ' + oldAccount.Billing_Address_Sync_Status__c);
                
                if (a.Billing_Address_Sync_Status__c == 'Sync In Progress' && oldAccount.Billing_Address_Sync_Status__c != 'Sync In Progress') {
                    if (a.Deleted__c || (a.BillingPostalCode == null && a.BillingStreet == null)) {
                        system.debug('*** After Update (DELETE) calling sync for Billing Address : '+a.id); 
                        DeleteBillingAddressIds.add(a.id);  
                    } else {
                        system.debug('*** After Update (UPSERT) calling sync for Billing Address : '+a.id); 
                        UpsertBillingAddressIds.add(a.id);  
                    }
                }
                
                if (a.Shipping_Address_Sync_Status__c == 'Sync In Progress' && oldAccount.Shipping_Address_Sync_Status__c != 'Sync In Progress') {
                    if (a.Deleted__c || (a.ShippingPostalCode == null && a.ShippingStreet == null)) {
                        system.debug('*** After Update (DELETE) calling sync for Shipping Address : '+a.id);    
                        DeleteShippingAddressIds.add(a.id); 
                    } else {
                        system.debug('*** After Update (UPSERT) calling sync for Shipping Address : '+a.id);    
                        UpsertShippingAddressIds.add(a.id); 
                    }
                }
            }
            
            if (UpsertBillingAddressIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    MDMAddressProxy.MDMUpsertBillingAddressAsync(UpsertBillingAddressIds);
                }
            }
            if (UpsertShippingAddressIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    MDMAddressProxy.MDMUpsertShippingAddressAsync(UpsertShippingAddressIds);
                }
            }
            if (DeleteBillingAddressIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    //CRM-33 - No need to send Address delete notification to MDM
                    //MDMAddressProxy.MDMDeleteBillingAddressAsync(DeleteBillingAddressIds);
                }
            }
            if (DeleteShippingAddressIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    //CRM-33 - No need to send Address delete notification to MDM
                    //MDMAddressProxy.MDMDeleteShippingAddressAsync(DeleteShippingAddressIds);
                }
            }
        }
    }
    
    public static void SumbitApprovals(List<Account> newAccounts, Map<id, Account> newAccountsMap) {
        DQ__c DQSettings = DataQualitySupport.GetSetting(UserInfo.getUserId());
        // changes done for CRM-31
        //if(DQSettings.Enable_Role_Governance__c){
            Set<String> approvalAcctIds = new Set<String>();
            for(account a : newAccounts){
                if( a.Unapproved_Company_Roles__c != null || 
                    a.Unapproved_Company_Roles_to_Remove__c != null ||
                    a.Unapproved_Company_Roles_to_Add__c != null ||
                    a.Sub_Roles_Unapproved__c != null || 
                    a.Sub_Roles_to_Add__c != null || 
                    a.Sub_Roles_to_Remove__c != null  ){
                    approvalAcctIds.add(a.Id);
                }
            }
            if(approvalAcctIds.size() > 0){
                System.Debug('*** after insert trigger : Submitting async approval process for ids ' + approvalAcctIds);
                accountTriggerHelper.submitApproval(approvalAcctIds);
            }  
        //}
    }
    
    public static void SetRoleText(List<Account> newAccounts) {
        System.debug('MDMSyncAccountHandler.SetRoleText STARTS');
        for (Account a : newAccounts) {
            System.debug('account.Company_Role__c'+a.Company_Role__c);
            a.Company_Role_Text__c = a.Company_Role__c;
            a.Sub_Roles_Text__c = a.Sub_Roles__c;
            //Added for SF-4272 Validation Error Message
            if(a.Sub_Roles_Text__c != null && a.Sub_Roles_Text__c.length()>255)
            {
                a.addError('SubRoles Data Too Large');
            }
        }
        System.debug('MDMSyncAccountHandler.SetRoleText ENDS');
    }

    public static void BeforeInsert(List<Account> newAccounts, Map<id, Account> newAccountsMap) {
        System.debug('MDMSyncAccountHandler.BeforeInsert STARTS');
        try {
            MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
            
            //firstly make sure that the approval area manager is set correctly
            AccountMarketAreaToolkit.setAccountMarketAreaManagers(newAccountsMap);
            
            //This need to be called before the accountTriggerHelper.companyRolesChanged(trigger.new); (as that call removes roles, therefore making everything invalid)...
            new MDMValidRoleCombinationsHelper().CheckInvalidRoleCombinations(newAccounts);
            
            //First of all, check for any unapproved Company Role Updates
            if(!Test.isRunningTest())//if added by Pulkit to stop company_role values from going null
                accountTriggerHelper.companyRolesChanged(newAccounts);
            
            
            //sfEdit STARTS
            System.debug('MDMSyncAccountHandler.BeforeInsert mdmSettings - '+mdmSettings);
            if(newAccounts != null && Test.isRunningTest()){
                for(Account account : newAccounts){
                    System.debug('sfEdit test account - '+account);
                }
                System.debug('sfEdit newAccounts.size() - '+newAccounts.size());
            }
            //sfEdit ENDS

            //Set the role \ sub role text fields for use in formula fields
            SetRoleText(newAccounts);
                            
            InsertAccountSyncStatus(newAccounts, mdmSettings.Web_Services_Enabled__c);
            InsertAddressesSyncStatus(newAccounts, mdmSettings.Web_Services_Enabled__c);
        } catch (Exception ex) {
            Logger.LogException('MDMSyncAccountHandler.BeforeInsert', ex);
            throw ex;
        }
        System.debug('MDMSyncAccountHandler.BeforeInsert ENDS');
    }
    
    public static void AfterInsert(List<Account> newAccounts, Map<id, Account> newAccountsMap) {
        try {
            MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
            
            SyncPartner(newAccounts, newAccountsMap, mdmSettings.Web_Services_Enabled__c);
            SyncAddresses(newAccounts, newAccountsMap, mdmSettings.Web_Services_Enabled__c);
            
            SumbitApprovals(newAccounts, newAccountsMap);
            
            //need this for release 1 to ensure the MDM Roles get created before the role governance and web services are enabled.
            new MDMValidRoleCombinationsHelper().SetValidRoleCombinations(trigger.new);
        } catch (Exception ex) {
            Logger.LogException('MDMSyncAccountHandler.AfterInsert', ex);
            throw ex;
        }
    }
    
    public static void BeforeUpdate(List<Account> newAccounts, Map<id, Account> newAccountsMap, List<Account> oldAccounts, Map<id, Account> oldAccountsMap) {
        System.debug('MDMSyncAccountHandler.BeforeUpdate STARTS');
        try {
            MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
            
            //firstly make sure that the approval area manager is set correctly
            AccountMarketAreaToolkit.setAccountMarketAreaManagers(newAccountsMap);
            
            //This need to be called before the accountTriggerHelper.companyRolesChanged(trigger.new); (as that removes roles, therefore making everything invalid)...
            new MDMValidRoleCombinationsHelper().CheckInvalidRoleCombinations(newAccounts);
            
            //First of all, check for any unapproved Company Role Updates
            accountTriggerHelper.companyRolesChanged(newAccountsMap, oldAccountsMap);
            //Then check for any approved company role changes
            accountTriggerHelper.approvedRolesChanged(newAccountsMap);
            
            //sfEdit STARTS
            if(newAccounts != null && Test.isRunningTest()){
                for(Account account : newAccounts){
                    System.debug('sfEdit test account - '+account);
                }
                System.debug('sfEdit newAccounts.size() - '+newAccounts.size());
            }
            //sfEdit ENDS

            //Set the role \ sub role text fields for use in formula fields
            SetRoleText(newAccounts);

            UpdateAddressesSyncStatus(newAccounts, oldAccountsMap, mdmSettings.Web_Services_Enabled__c);
            UpdateAccountSyncStatus(newAccounts, oldAccountsMap, mdmSettings.Web_Services_Enabled__c);
        } catch (Exception ex) {
            Logger.LogException('MDMSyncAccountHandler.BeforeUpdate', ex);
            throw ex;
        }
        System.debug('MDMSyncAccountHandler.BeforeUpdate ENDS');
    }
    
    public static void AfterUpdate(List<Account> newAccounts, Map<id, Account> newAccountsMap, List<Account> oldAccounts, Map<id, Account> oldAccountsMap) {
        try {
            
            MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
            System.Debug('*** UserInfo.getUserId() : ' + UserInfo.getUserId());
            System.Debug('*** mdmSettings.Web_Services_Enabled__c: ' + mdmSettings.Web_Services_Enabled__c);
            //Create the MDM Roles...
            new MDMValidRoleCombinationsHelper().SetValidRoleCombinations(newAccounts);
            
            SyncAddresses(newAccounts, newAccountsMap, oldAccounts, oldAccountsMap, mdmSettings.Web_Services_Enabled__c);
            SyncPartner(newAccounts, newAccountsMap, oldAccounts, oldAccountsMap, mdmSettings.Web_Services_Enabled__c);
        
        } catch (Exception ex) {
            Logger.LogException('MDMSyncAccountHandler.AfterUpdate', ex);
            throw ex;
        }
    }
    
    public static void BeforeDelete(List<Account> oldAccounts, Map<id, Account> oldAccountsMap) {
    
    }
    
    public static void AfterDelete(List<Account> oldAccounts, Map<id, Account> oldAccountsMap) {
        // This is the only point to trap a Salesforce merge.
        // In Trigger.Old, the MasterRecordId will be set on records that have been deleted as part of a merge.
        try {
            List<MDM_Account_Merge__c> mergedAccounts = new List<MDM_Account_Merge__c>();
            for(account a : oldAccounts) {
                if(a.masterrecordid != null)
         //CRM-35   mergedAccounts.add(new MDM_Account_Merge__c(From_Account__c = a.id, From_Account_Name__c = a.Name, To_Account__c = a.masterrecordid));
                    mergedAccounts.add(new MDM_Account_Merge__c(From_Company_ID__c = a.Company_ID__c, From_Account__c = a.id, From_Account_Name__c = a.Name, To_Account__c = a.masterrecordid));
           }
            insert mergedAccounts;
        
        } catch (Exception ex) {
            Logger.LogException('MDMSyncAccountHandler.AfterDelete', ex);
            throw ex;
        }
    }
    
    // Method written for CRM-32
    public static void ResyncNewAccounts(List<Account> newAccounts, Map<id, Account> newAccountsMap){
        System.Debug('*** Inside ResyncNewAccounts');
        try {
            MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
            
            SyncPartner(newAccounts, newAccountsMap, mdmSettings.Web_Services_Enabled__c);
            SyncAddresses(newAccounts, newAccountsMap, mdmSettings.Web_Services_Enabled__c);
            
            new MDMValidRoleCombinationsHelper().SetValidRoleCombinations(newAccounts);
        } catch (Exception ex) {
            Logger.LogException('MDMSyncAccountHandler.ResyncNewAccounts', ex);
            throw ex;
        }
    }
    
    public static void ResyncExistingAccounts(List<Account> existingAccounts){
        System.Debug('*** Inside ResyncExistingAccounts');
        Boolean needUpdate = false;
        for(Account anExistingAccount : existingAccounts){
            if(anExistingAccount.Roles_Sync_Status__c == 'Sync Failed'){
                List<MDM_Role__c> allMdmRoles = [SELECT ID, Synchronisation_Status__c FROM MDM_Role__c WHERE Account__c =: anExistingAccount.ID];
                for(MDM_Role__c singleMDMRole : allMdmRoles){
                    if(singleMDMRole.Synchronisation_Status__c == 'Sync Failed'){
                        singleMDMRole.Synchronisation_Status__c = 'Sync In Progress';
                    }
                }
                update allMdmRoles;
            }
      if(anExistingAccount.Shipping_Address_Sync_Status__c == 'Sync Failed'){
                needUpdate = true;
                anExistingAccount.Shipping_Address_Sync_Status__c = 'Sync In Progress';
            }
            if(anExistingAccount.Billing_Address_Sync_Status__c == 'Sync Failed'){
                needUpdate = true;
                anExistingAccount.Billing_Address_Sync_Status__c = 'Sync In Progress';
            }
            
            if(anExistingAccount.Synchronisation_Status__c == 'Sync Failed'){
                System.Debug('Updating anExistingAccount.Synchronisation_Status__c');
                anExistingAccount.Synchronisation_Status__c = 'Sync In Progress';
                update anExistingAccount;
            }
      
        }
        
        if((existingAccounts.size() > 0) && (needUpdate == true)){
            try{
                update existingAccounts;
            }catch (Exception ex) {
                Logger.LogException('MDMSyncAccountHandler.ResyncExistingAccounts', ex);
                throw ex;
            }
        }
    }
}