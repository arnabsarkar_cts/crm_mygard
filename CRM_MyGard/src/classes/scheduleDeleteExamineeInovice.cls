global class scheduleDeleteExamineeInovice implements schedulable
{
    global void execute(SchedulableContext sc)
    {
        BatchDeleteApprovedExaminee bdr = new BatchDeleteApprovedExaminee();
        database.executebatch(bdr);
    }
}