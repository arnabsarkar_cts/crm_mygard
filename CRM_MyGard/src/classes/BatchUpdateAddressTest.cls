@isTest
private class BatchUpdateAddressTest {

    static testmethod void processAccountAddresses() {
        setupTestData();
        system.assert([SELECT Id FROM Account WHERE Name = 'TEST ADDRESS ACCOUNT' LIMIT 1]!=null);
        Test.startTest();
        String query = 'SELECT Id FROM Account WHERE Name = \'TEST ADDRESS ACCOUNT\'';
        BatchUpdateAddress batch = new BatchUpdateAddress();
        batch.query = query;
        Database.executeBatch(batch);
        Test.stopTest();
    }
    
    private static void setupTestData() {
        Country__c country = new Country__c(name='TESTCOUNTRY');
        insert country;
        system.assert(country.Id!=null);
        
        Market_Area__c marketarea = new Market_Area__c(name='TESTMA');
        insert marketarea;
        system.assert(marketarea.Id!=null);
        
        User areamgr = [SELECT Id FROM User WHERE Position__c LIKE '%Area Manager%' LIMIT 1];
        system.assert(areamgr!=null);
        
        Account acc = new Account();
        acc.Name = 'TEST ADDRESS ACCOUNT';
        acc.RecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('Client').RecordTypeId;
        acc.Site = 'TESTSITE';
        acc.Country__c = country.Id;
        acc.Market_Area__c = marketarea.Id;
        acc.Area_Manager__c = areamgr.Id;
        acc.BillingStreet = 'BILLING 1\nBILLING 2\nBILLING 3\nBILLING 4';
        acc.BillingCity = 'BILLINGCITY';
        acc.BillingState = 'BILLINGSTATE';
        acc.BillingPOstalCode = 'BILLINGPOSTALCODE';
        acc.BillingCountry = 'BILLINGCOUNTRY';
        acc.ShippingStreet = 'SHIPPING 1\nSHIPPING 2\nSHIPPING 3\nSHIPPING 4';
        acc.ShippingCity = 'SHIPPINGCITY';
        acc.ShippingState = 'SHIPPINGSTATE';
        acc.ShippingPostalCode = 'SHIPPINGPOSTALCODE';
        acc.ShippingCountry = 'SHIPPINGCOUNTRY';
        insert acc;
        system.assert(acc.Id!=null);
    }
}