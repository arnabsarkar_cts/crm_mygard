/* Test class for EditOppProductExtension
*  Steve O'Connell, cDecisions Ltd
*  March 2013
*
*
*/
@isTest (seeAllData = true) //Need this to see standard price book
private class testEditOppProductExtension {

    public static Opportunity[] testOpps;
    public static Integer noOfMarineProds = -1;
    public static Integer noOfEnergyProds = -1;
    public static String currencyISOCode = 'USD';

    private static void setupTestData()
    {
        /*
        //Pricebooks
        Pricebook2[] priceBooks = new Pricebook2[]{
            //new Pricebook2(Name='Standard', isStandard=true),
            new Pricebook2(Name='Marine'),
            new Pricebook2(Name='Energy')
        };
        insert priceBooks;*/
        
        Pricebook2 standardPb = [SELECT Id FROM Pricebook2 WHERE isStandard=true];
        System.Assert(standardPb != null);
        
        Product2[] products = new Product2[]{
            new Product2(Name='testProd1', Description='Test Product One', Product_Type__c = 'Marine'),
            new Product2(Name='testProd2', Description='Test Product Two', Product_Type__c = 'Energy'),
            new Product2(Name='testProd3', Description='Test Product Three', Product_Type__c = 'Energy;Marine'),
            new Product2(Name='testProd3', Description='Test Product Four', Product_Type__c = 'Energy;Marine')
        };
        insert products;
        
        PricebookEntry[] standardPrices = new PricebookEntry[]{
            new PricebookEntry(Pricebook2Id = standardPb.Id, Product2Id = products[0].Id, UnitPrice=1, isActive=true),
            new PricebookEntry(Pricebook2Id = standardPb.Id, Product2Id = products[1].Id, UnitPrice=1, isActive=true),
            new PricebookEntry(Pricebook2Id = standardPb.Id, Product2Id = products[2].Id, UnitPrice=1, isActive=true)
        };
        insert standardPrices;
        
        /*PricebookEntry[] priceBookEntries = new PricebookEntry[]{
            new PricebookEntry(Pricebook2Id = priceBooks[0].Id, Product2Id = products[0].Id, UnitPrice=1, UseStandardPrice=true, isActive=true),
            new PricebookEntry(Pricebook2Id = priceBooks[1].Id, Product2Id = products[0].Id, UnitPrice=1, UseStandardPrice=true, isActive=true),
            new PricebookEntry(Pricebook2Id = priceBooks[0].Id, Product2Id = products[1].Id, UnitPrice=1, UseStandardPrice=true, isActive=true),
            new PricebookEntry(Pricebook2Id = priceBooks[1].Id, Product2Id = products[2].Id, UnitPrice=1, UseStandardPrice=true, isActive=true)
        };
        insert priceBookEntries;*/
        
        RecordType oppMarineRecType = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' AND Name='Marine'];
        RecordType oppEnergyRecType = [SELECT Id FROM RecordType WHERE SObjectType='Opportunity' AND Name='Energy'];
        
        
        testOpps = new Opportunity[]{
            new Opportunity(Name='testOpp1', RecordTypeId=oppMarineRecType.Id, StageName='Renewable Opportunity', CloseDate=Date.Today()+7, CurrencyISOCode = currencyISOCode),
            new Opportunity(Name='testOpp2', RecordTypeId=oppEnergyRecType.Id, StageName='Renewable Opportunity', CloseDate=Date.Today()+7, CurrencyISOCode = currencyISOCode)
        };
        insert testOpps;
        
        OpportunityLineItem[] oppLineItems = new OpportunityLineItem[]{
            new OpportunityLineItem(OpportunityId = testOpps[1].Id, PriceBookEntryId=standardPrices[1].Id, Line_Size__c = 100)
        };
        insert oppLineItems;
        
        //Useful metrics
        AggregateResult ar = [SELECT COUNT(Id) FROM PriceBookEntry WHERE Product2.Product_Type__c includes ('Marine') AND PriceBook2.IsStandard=true AND CurrencyISOCode = :currencyISOCode];
        noOfMarineProds = (Integer)ar.get('expr0');
        System.Debug('Number of Marine products: ' + noOfMarineProds);
        
        AggregateResult ar2 = [SELECT COUNT(Id) FROM PriceBookEntry WHERE Product2.Product_Type__c includes ('Energy') AND PriceBook2.IsStandard=true AND CurrencyISOCode = :currencyISOCode];
        noOfEnergyProds = (Integer)ar2.get('expr0');
        System.Debug('Number of Energy products: ' + noOfEnergyProds);
        
        
    }

    static testMethod void myUnitTest() {
        setupTestData();
        
        Test.startTest();
        PageReference pageRef = Page.EditOppProduct;
        pageRef.getParameters().put('Id', testOpps[0].Id);
        Test.setCurrentPage(pageRef);
        
        EditOppProductExtension EOPE = new EditOppProductExtension(new ApexPages.StandardController(testOpps[0]));
        
        //Check number of available products
        System.Debug('Number of Available Products: ' + EOPE.availableProducts.size() + ' Number of Marine Products:' + noOfMarineProds);
        System.Assert(EOPE.availableProducts.size() == noOfMarineProds);
        
        EOPE.toSelect = EOPE.availableProducts[0].Id;
        String addingProd = EOPE.availableProducts[0].Id;
        EOPE.addProduct();
        
        System.Debug('availableProduct Id: ' + addingProd + ' selectedProduct PriceBookEntryId: ' + EOPE.selectedProducts[0].PriceBookEntryId);
        System.Assert(EOPE.selectedProducts[0].PriceBookEntryId == addingProd);
        
        //Test search functionality
        EOPE.searchString = 'Three';
        EOPE.updateAvailableProducts();
        
        System.Debug('No. of available products after search: ' + EOPE.availableProducts.size());
        System.Assert(EOPE.availableProducts.size() == 1);
        
        PageReference onSave = EOPE.onSave();
        System.Assert(onSave.getUrl()=='/' + ApexPages.currentPage().getParameters().get('Id'));
        
        PageReference onCancel = EOPE.onCancel();
        System.Assert(onCancel.getUrl()=='/' + ApexPages.currentPage().getParameters().get('Id'));
    
        PageReference pageRef2 = Page.EditOppProduct;
        pageRef2.getParameters().put('Id', testOpps[1].Id);
        Test.setCurrentPage(pageRef2);
        
        EditOppProductExtension EOPE2 = new EditOppProductExtension(new ApexPages.StandardController(testOpps[0]));
        
        //Check that number of products added to opp in testData() match
        System.Assert(EOPE2.selectedProducts.size() == 1);
        Integer noOfAvailProds = EOPE2.availableProducts.size();
        
        EOPE2.toUnselect = EOPE2.selectedProducts[0].Id;
        EOPE2.RemoveProduct();
        
        System.Assert(EOPE2.selectedProducts.size() == 0);
        //System.Assert(EOPE2.forDeletion.size() == 1);
        //SOC - Products appear all the time so availableproducts should be constant
        //System.Assert(EOPE2.availableProducts.size() == noOfAvailProds + 1);
        System.Assert(EOPE2.availableProducts.size() == noOfAvailProds);
        
        PageReference onSave2 = EOPE2.onSave();
        System.Assert(onSave2.getUrl()=='/' + ApexPages.currentPage().getParameters().get('Id'));
        
        PageReference onCancel2 = EOPE2.onCancel();
        System.Assert(onCancel2.getUrl()=='/' + ApexPages.currentPage().getParameters().get('Id'));
    //--------------------------------------------
       /* OpportunityLineItem oliDel = [SELECT OpportunityId ,PriceBookEntryId,Line_Size__c from OpportunityLineItem WHERE Id  = :oppLineItems.Id];
        Delete oliDel;
        EOPE2.RemoveProduct(); 
        */
    //---------------------------------------------    
        Test.stopTest();
    }
}