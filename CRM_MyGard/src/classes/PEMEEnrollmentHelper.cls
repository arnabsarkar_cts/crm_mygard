public with sharing class PEMEEnrollmentHelper {
    
    
    public static void afterDelete(List<PEME_Enrollment_Form__c> enrollmentList){
        List<String> accIdLst = new List<String>();
        List<Account> updatedAccountsLst = new List<Account>();
        List<PEME_Enrollment_Form__c> pefLst = new List<PEME_Enrollment_Form__c>();
        
        for(PEME_Enrollment_Form__c pef : enrollmentList){
            if(pef.ClientName__c != null){
                accIdLst.add(pef.ClientName__c+'');
            }
        }
        for(Account acc : [SELECT id,(SELECT id FROM PEME_enrollment_details__r) FROM Account WHERE id IN :accIdLst]){
            pefLst = acc.PEME_enrollment_details__r;
            if(pefLst.size() == 0){
                acc.PEME_Enrollment_Status__c = 'Not Enrolled';
                updatedAccountsLst.add(acc);
            }
        }
        update updatedAccountsLst;
    }
    
    public static void PEMEEnrollmentBeforeInsert(List<PEME_Enrollment_Form__c>enrollmentList){
        system.debug('PEMEEnrollmentBeforeInsertUpdate****************');
        Map<id,Profile> partnerProfileMap=new Map<id,Profile>([SELECT Id,Name FROM Profile WHERE Name IN ('Partner Community Login User Custom','Partner Community Pooled (copy from Login) User Custom')]);
        // Map<Id,List<PEME_Enrollment_Form__c>>submitterIdMap=new Map<Id,List<PEME_Enrollment_Form__c>>();
        if(!partnerProfileMap.containsKey(userInfo.getProfileId())){
            for(PEME_Enrollment_Form__c eform:enrollmentList){
                eform.IsCreatedInCRM__c=true;
            }
        }
    }   
    
    
    /*//Commented for SF-4871
    public static void PEMEEnrollmentAfterInsert(List<PEME_Enrollment_Form__c>enrollmentList){
        boolean isManningAgentApproved=false;
        boolean isDebitnoteApproved=false;
        map<String,List<PEME_Debit_note_detail__c>>enrollmentDebitMap= new map<String,List<PEME_Debit_note_detail__c>>();
        map<String,List<PEME_Manning_Agent__c>>enrollmentManningAgentMap= new map<String,List<PEME_Manning_Agent__c>>();
        List<PEME_Debit_note_detail__c>debitList=new List<PEME_Debit_note_detail__c>([SELECT id,status__c,PEME_Enrollment_Form__c FROM PEME_Debit_note_detail__c 
                                                                                      WHERE PEME_Enrollment_Form__c IN:enrollmentList]);
        
        if(!debitList.isEmpty()){
            
            for(PEME_Debit_note_detail__c debitNote:debitList){
                
                if(enrollmentDebitMap.containsKey(debitNote.PEME_Enrollment_Form__c)){
                    
                    enrollmentDebitMap.get(debitNote.PEME_Enrollment_Form__c).add(debitNote);
                    
                }
                
                else{
                    
                    enrollmentDebitMap.put(debitNote.PEME_Enrollment_Form__c,new List<PEME_Debit_note_detail__c>{debitNote});
                }
            }
        }
        
        List<PEME_Manning_Agent__c>manningAgentList=new List<PEME_Manning_Agent__c>([SELECT id,status__c,PEME_Enrollment_Form__c FROM PEME_Manning_Agent__c 
                                                                                     WHERE PEME_Enrollment_Form__c IN:enrollmentList]);
        
        if(!manningAgentList.isEmpty()){
            
            for(PEME_Manning_Agent__c manningAgent:manningAgentList){
                if(enrollmentManningAgentMap.containsKey(manningAgent.PEME_Enrollment_Form__c)){
                    enrollmentManningAgentMap.get(manningAgent.PEME_Enrollment_Form__c).add(manningAgent);
                }
                else{
                    enrollmentManningAgentMap.put(manningAgent.PEME_Enrollment_Form__c,new List<PEME_Manning_Agent__c>{manningAgent});
                }
            }
        }
        for(PEME_Enrollment_Form__c enrollment:enrollmentList){
            if(enrollment.Channel__c != 'Community'){ //added by Pulkit SF-1902, to skip this check for Community PEFs
                if(!(enrollmentDebitMap.containsKey(enrollment.id) && enrollmentManningAgentMap.containsKey(enrollment.id))){
                    enrollment.addError(Label.PEMEEnrollmentError+' ' +enrollment.Enrollment_Status__c );
                }
                else{
                    if(enrollmentManningAgentMap.containsKey(enrollment.id)){
                        for(PEME_Manning_Agent__c manAgent:enrollmentManningAgentMap.get(enrollment.id)){
                            if(manAgent.status__c.equalsIgnoreCase('Approved')){
                                isManningAgentApproved=true;
                                break;
                            }
                        }
                    }
                    if(enrollmentDebitMap.containsKey(enrollment.id)){
                        for(PEME_Debit_note_detail__c debitNote:enrollmentDebitMap.get(enrollment.id)){
                            //Added for SF-4858
                            if(debitNote.status__c!=null && debitNote.status__c!='')
                            {
                                if(debitNote.status__c.equalsIgnoreCase('Approved')){
                                    isDebitnoteApproved=true;
                                    break;
                                }
                            }
                        }
                    }
                    if(!(isManningAgentApproved && isDebitnoteApproved)){
                        enrollment.addError(Label.ManningAndDebitNoteApprovalError);
                    }
                }
            }
        }
    }  
    */
    
    //Added for SF-4871
    //If Both Children aren't Approved, gives error
    public static void approvedStatusEligibilityCheck(Map<ID,PEME_Enrollment_Form__c> oldPefMap,Map<ID,PEME_Enrollment_Form__c> newPefMap){
        
        PEME_Enrollment_Form__c oldPef,newPef;
        for(Id pefId : newPefMap.keySet()){
            newPef = newPefMap.get(pefId);
            oldPef = oldPefMap.get(pefId);
            if(oldPef.Enrollment_Status__c == 'Under Approval' && newPef.Enrollment_Status__c == 'Enrolled' && (newPef.Approved_Debit_Notes_Count__c == 0 || newPef.ApprovedManningAgentsCount__c == 0)){
                newPef.addError(Label.ManningAndDebitNoteApprovalError);
                //newPef.addError('approvedStatusEligibilityCheck Error');
            }
        }
    }
    
    //Added for SF-4871
    //If Both Children aren't in the correct state for "these" records to go Under Approval, gives error
    public static void underApprovalStatusEligibiltyCheck(Map<ID,PEME_Enrollment_Form__c> oldPefMap,Map<ID,PEME_Enrollment_Form__c> newPefMap){
        if(newPefMap == null) return;
        Set<Id> allowedUnderApprovalPefIds = getUnderApprovalEligibles(newPefMap);
        PEME_Enrollment_Form__c oldPef,newPef;
        for(Id pefId : newPefMap.keySet()){
            newPef = newPefMap.get(pefId);
            oldPef = oldPefMap.get(pefId);
            if(oldPef.Enrollment_Status__c != 'Under Approval' && newPef.Enrollment_Status__c == 'Under Approval' && !allowedUnderApprovalPefIds.contains(pefId)){
                newPef.addError(Label.ManningAndDebitNoteApprovalError);
                //newPef.addError('underApprovalStatusEligibiltyCheck Error');
            }
        }
    }
    
     //Added for SF-4871
    //returns a set of PEME Enrollment Ids which can go under approval; this is based on Debit Notes and Manning Agents, i.e. child objects
    //Community channelled PEME Enrollment Forms - Both child objects can be either Approved or Under Approval
    //Physical channelled PEME Enrollment Forms - Both child objects are Approved
    private static Set<Id> getUnderApprovalEligibles(Map<ID,PEME_Enrollment_Form__c> newPefMap){
        if(newPefMap == null) return new Set<Id>();
        Set<Id> pefIds = newPefMap.keySet();
        String debitNoteQuery = 'SELECT id,status__c,PEME_Enrollment_Form__c FROM PEME_Debit_note_detail__c WHERE PEME_Enrollment_Form__c IN :pefIds AND ((status__c = \'Approved\') OR (status__c = \'Under Approval\' AND PEME_Enrollment_Form__r.channel__c = \'Community\'))';
        String manningAgentQuery = 'SELECT id,status__c,PEME_Enrollment_Form__c FROM PEME_Manning_Agent__c WHERE PEME_Enrollment_Form__c IN :pefIds AND ((status__c = \'Approved\') OR (status__c = \'Under Approval\' AND PEME_Enrollment_Form__r.channel__c = \'Community\'))';
        Set<Id> pefsWithCorrectDebitNotes = new Set<Id>();
        Set<Id> pefsWithCorrectManningAgents = new Set<Id>();
        Set<Id> pefsAllowedToGoUnderApproval = new Set<Id>();
        
        //which PEFs have required PDNs
        for(PEME_Debit_note_detail__c pdn : Database.query(debitNoteQuery)){
            pefsWithCorrectDebitNotes.add(pdn.PEME_Enrollment_Form__c);
            break;
        }
        
        //which PEFs have required PMAs
        for(PEME_Manning_Agent__c pdn : Database.query(manningAgentQuery)){
            pefsWithCorrectManningAgents.add(pdn.PEME_Enrollment_Form__c);
            break;
        }
        
        for(Id pefId : pefIds){
            if(
                pefsWithCorrectManningAgents.contains(pefId)
                && pefsWithCorrectDebitNotes.contains(pefId)
            ){
                pefsAllowedToGoUnderApproval.add(pefId);
            }
        }
        
        return pefsAllowedToGoUnderApproval;
    }
}