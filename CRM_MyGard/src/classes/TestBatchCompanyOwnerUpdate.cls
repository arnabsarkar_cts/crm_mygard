@isTest(SeeallData = false)
public class TestBatchCompanyOwnerUpdate {
     Static testMethod void  BatchCompanyOwnerUpdatetest ()
    {
         User u = new User();
        //User u2 = new User();
        u = [Select id From User Where id = : Userinfo.getUserId()];
        
        Profile p = [Select id,Name From Profile Where Name = 'System Administrator'];      
        
        
            User u2 = new User();
            u2.Username = 'username@test.com';
            u2.LastName = 'Test-User';
            u2.Email = 'username@test.com';
            u2.Alias = 'J. Doe';
            u2.CommunityNickname = 'usernametest';
            u2.TimeZoneSidKey = 'Europe/Amsterdam';
            u2.LocaleSidKey = 'en_US';
            u2.EmailEncodingKey = 'ISO-8859-1';
            u2.ProfileId = p.id;
            u2.LanguageLocaleKey = 'en_US';
            insert u2;
        system.runAs(u){
        Test.startTest();
               
        List<Account> accList = new List<Account>();
        
       
      
        Account acc = new Account();
        
            acc.Name = 'Customer Account';
            acc.Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().id;
            acc.Market_Area__c = TestDataGenerator.getMarketArea().Id;    
            acc.Synchronisation_Status__c = 'Synchronised';
            acc.Billing_Address_Sync_Status__c = 'Synchronised';
            acc.Shipping_Address_Sync_Status__c = 'Synchronised';
            acc.Roles_Sync_Status__c  = 'Synchronised'; 
            acc.Company_Status__c  = 'Active';
            acc.Company_Role__c = 'Broker;'; 
            //acc.Manning_Agent_or_Debit_Note_ID__c = pma.id;
            acc.Underwriter_main_contact__c =u2.id;
            //acc.Underwriter_main_contact__r.IsActive =true;
            insert acc;
            
            acc.OwnerId = u.id;
            update acc;
        
        
            accList.add(acc);
        //   
         
            BatchCompanyOwnerUpdate job = new BatchCompanyOwnerUpdate();
            Database.QueryLocator ql = job.start(null);
            job.execute(null,accList);
            job.Finish(null);
        Test.StopTest();
     }   
    }

}