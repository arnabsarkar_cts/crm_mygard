@isTest
public class TestOnRiskFieldUpdator{
    static List<Account> testAccounts;
    static Gard_Team__c gt;
    //same code from ObjectTreeTest
    static void createTestData(){
        Integer numLevels = 3;
        Integer numAccountsPerLevel = 2;
		Set<Id> lowestLevelAccountIds;
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c = '');
        vrcList.add(vrcClient);
        insert vrcList;
        Gard_Contacts__c gardCon =    new  Gard_Contacts__c(
            FirstName__c = 'Testreqchng',
            LastName__c = 'Baann',
            //Company_Type__c = 'Grd', 
            Email__c = 'WR_gards.12@Test.com',
            //MailingCity__c = 'Arendal', 
            //MailingCountry__c = 'Norway', 
            // MailingPostalCode__c =  '487155',
            // MailingState__c = 'Oslo', 
            // MailingStreet__c = 'Rd Road', 
            //Mobile__c = '98323322', 
            MobilePhone__c = '548645', 
            Nick_Name__c  = 'NilsPeter', 
            Office_city__c = 'Arendal', 
            Phone__c = '5454454',
            Portal_Image__c ='<img alt="User-added image" src="https://c.cs8.content.force.com/servlet/rtaImage?eid=a1hL0000000kSyZ&amp;feoid=00NL0000003OySC&amp;refid=0EML00000008Zjs"></img>', 
            Title__c = 'testTitle' 
        );
        insert gardCon ;
		gt = new Gard_Team__c(Active__c = true,Office__c = 'test',Name = 'GTeam',Region_code__c = 'TEST',ContactId__c = gardCon.id);
        insert gt;
        
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Gard_Team__c=gt.Id,Claims_Support_Team__c=gt.Id);
        insert country;
        String accountName =  'TestObjectTree';
        Account template = new Account(name = accountName+'0', ParentId = null, Client_On_Risk_Flag__c = true, Child_Company_On_Risk__c = false, ShippingStreet = '1 Main Street', ShippingState = 'TX', ShippingPostalCode = '123456', ShippingCountry = 'US', ShippingCity = 'Any Town', Description = 'This is a test account', BillingStreet = '1 Main Street', BillingState = 'TX', BillingPostalCode = '123456', BillingCountry = 'USA', BillingCity = 'Any Town', AnnualRevenue = 10000,Country__c = country.id);
        
        List<Account> prevLevelAccounts = new List<Account>();
        List<Account> lowestLevelAccounts = new List<Account>();
        testAccounts = new List<Account>();
        
        for(Integer i = 0;i<numLevels;i++){
            List<Account> levelAccounts = new List<Account>();		
            
            if(prevLevelAccounts == null || prevLevelAccounts.size() == 0){
                //Top level
                for(Integer j = 0;j<numAccountsPerLevel;j++){
                    Account tempAccount = template.clone(false,true);
                    //if(i == 0){tempAccount.Client_On_Risk_Flag__c = false;tempAccount.Child_Company_On_Risk__c = true;}
                    levelAccounts.add(template.clone(false,true));
                    //insert levelAccounts;
                }
            }else{
                //iterate through each account in the level before
                for(Account parent:prevLevelAccounts){
                    //clone the template numAccountsPerLevel times
                    for(Integer j = 0;j<numAccountsPerLevel;j++){
                        Account thisAccount = template.clone(false,true);
                        thisAccount.ParentId = parent.Id;
                        levelAccounts.add(thisAccount);				
                    }
                }
            }
            
            if(levelAccounts != null && levelAccounts.size() > 0){
                Boolean flipFlop = true;
                for(Account eachAccount : levelAccounts){
                    if(!flipFlop){
                        eachAccount.Client_On_Risk_Flag__c = false;
                        eachAccount.Child_Company_On_Risk__c = true;
                    }
                    flipFlop = !flipFlop;
                }
                insert levelAccounts;
                
                prevLevelAccounts = levelAccounts.deepClone(true);
                testAccounts.addAll(levelAccounts.deepClone(true));
                if(i == numLevels-1){
                    lowestLevelAccounts.addAll(levelAccounts.deepClone(true));
                    if(lowestLevelAccountIds  ==  null){lowestLevelAccountIds = new Set<Id>();}
                    for(Account acc:lowestLevelAccounts){
                        lowestLevelAccountIds.add(acc.Id);
                    }
                }
            }
            
        }
			
        /*for(Account testAccount : testAccounts){
            	testAccount.Company_Role_Text__c = 'Client';
        		testAccount.Child_Company_On_Risk__c = false;
        		testAccount.Client_On_Risk_Flag__c = true;
        }
        update testAccounts;
		*/
    }
    
    @isTest private static void testRelatedGardContact(){
        createTestData();
        gt.Name = 'newTestName';
		update gt;
    }
    
    @isTest private static void testBatch(){
        createTestData();
        ObjectTree.antiRecursionflag = false;
        OnRiskFieldUpdator.reportUpdate(true);
    }
}