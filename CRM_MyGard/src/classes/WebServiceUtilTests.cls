@isTest
public class WebServiceUtilTests {
	
	private static WebServiceUtilTests instanceField;
	
	public static WebServiceUtilTests Instance {
		get {
			if (instanceField == null) {
				instanceField = new WebServiceUtilTests();
			}
			return instanceField;
		}
	}
	
	private WebServiceUtilTests() {
	}
	
	public Boolean GenerateResponseFromRequestIds { get; set; }
	public Boolean ResponseSuccessStatus { get; set; }
	public Boolean MixedResponseSuccessStatus { get; set; }
	
	private void SetResponseFromRequest() {
		if (GenerateResponseFromRequestIds && MockWebServiceRequest instanceof HttpRequest) {
			Dom.Document xmlRequest = new Dom.Document();
			xmlRequest.load(((HttpRequest)MockWebServiceRequest).getBody());
			Map<string, Boolean> success = new Map<string, boolean>();
			
			Set<String> ids = new Set<String>();
			for (DOM.XmlNode node : xmlRequest.getRootElement().getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/').getChildren()) {
				ids.addAll(findIdsFromNode(node));
			}
			
			for (String id : ids) {
				if (MixedResponseSuccessStatus) {
					ResponseSuccessStatus = !ResponseSuccessStatus;
				}
				success.put(id, ResponseSuccessStatus);
			}
			
			MockWebServiceResponse = new MDMProxyTests().GenerateMDMServiceResponse(success);
		}
	}
	
	private Set<String> findIdsFromNode(DOM.XmlNode node) {
		Set<String> ids = new Set<String>();
		for (DOM.XmlNode child : node.getChildren()) {
			System.debug('*** Node Name: ' + child.getName());
			if (node.getName() == 'objectId' && child.getName() == 'sourceSystemId') {
				ids.add(child.getText());
			}
			ids.addAll(findIdsFromNode(child));
		}
		return ids;
	}
	
	private object MockWebServiceRequestField;
	public object MockWebServiceRequest { 
		get { return MockWebServiceRequestField; } 
		set { 
			MockWebServiceRequestField = value; 
			//SetResponseFromRequest(); 
		}
	}
	public object MockWebServiceResponse { get; set; }
	
}