// Test Class for PiRenewalPoliciesCtrl
@isTest
public class testPiRenewalPoliciesCtrl{
    public static List<PIRenewal_CoE_Document_Type__c> PIRCoeDocList = new List<PIRenewal_CoE_Document_Type__c>();
    public static List<PI_Renewal_Document_Download_Tracking__c> PIRenDocDwnldtrackingList = new List<PI_Renewal_Document_Download_Tracking__c>();
    // Test Method
    public static testMethod void PiRenewalPoliciesMethod(){
        
        GardTestData test_data = new GardTestData();
        test_data.customsettings_rec();
        test_data.commonRecord();
        test_data.ViewDoc_testrecord();
        DocumentMetadata_Junction_object__c docMjunctnObj = new DocumentMetadata_Junction_object__c(Agreement__c = GardTestData.brokerContract_1st.id,
                                                                                                    Broker__c = GardTestData.brokerAcc.Id,  
                                                                                                    Claim__c = GardTestData.clientCase.id,
                                                                                                    Client__c = GardTestData.clientAcc.Id,
                                                                                                    Cover__c = GardTestData.brokerAsset_1st.id,
                                                                                                    Document_Metadata__c = GardTestData.brokerDoc.Id,
                                                                                                    External_ID__c = 'ID0057',
                                                                                                    Object__c = GardTestData.test_object_1st.id
                                                                                                    );
        insert docMjunctnObj ;
        /*DocumentMetadata_Junction_object__c docMjunctnObj2 = [select id,Client__r.name from DocumentMetadata_Junction_object__c  where  External_ID__c = 'ID0057' limit 1];
        DocumentMetadata_Junction_object__c docMjunctnObj3 = new DocumentMetadata_Junction_object__c();
        docMjunctnObj3.id = docMjunctnObj2.id;
        docMjunctnObj3.Client__c = GardTestData.clientAcc.id;//docMjunctnObj2.Client__c;
        //docMjunctnObj3.Client__r.name = 'Test123';
        upsert docMjunctnObj3;
        */
        PIRenewal_CoE_Document_Type__c PIRCoeDoc1 = new PIRenewal_CoE_Document_Type__c(name = 'Type 2' , value__c = 'Appendix/endorsement testing');
        PIRenewal_CoE_Document_Type__c PIRCoeDoc2 = new PIRenewal_CoE_Document_Type__c(name = 'Type 3' , value__c = 'Bunkers Blue Card testing');
        PIRCoeDocList.add(PIRCoeDoc1);
        PIRCoeDocList.add(PIRCoeDoc2) ;
        insert PIRCoeDocList;
        PI_Renewal_Document_Download_Tracking__c PIRenDocDwnldtracking1 = new PI_Renewal_Document_Download_Tracking__c(CompanyId__c = GardTestData.brokerAcc.Id,
                                                                                                                      DownloadBy__c = GardTestData.brokerContact.Id,
                                                                                                                      Document_id__c = GardTestData.junc_object_broker.Id
                                                                                                                      );
        PIRenDocDwnldtrackingList.add(PIRenDocDwnldtracking1);
        PI_Renewal_Document_Download_Tracking__c PIRenDocDwnldtracking2 = new PI_Renewal_Document_Download_Tracking__c(CompanyId__c = GardTestData.clientAcc.Id,
                                                                                                                      DownloadBy__c = GardTestData.clientContact.Id,
                                                                                                                      Document_id__c = GardTestData.junc_object_client.Id
                                                                                                                      );
        PIRenDocDwnldtrackingList.add(PIRenDocDwnldtracking2);
        insert PIRenDocDwnldtrackingList;
        test.startTest();
            System.runAs(Gardtestdata.brokerUser){
            PageReference pageRef = page.PiRenewalPolicies;
            Test.setCurrentPage(pageRef);
            //ApexPages.currentPage().getParameters().put('favId',String.valueOf(Gardtestdata.test_fav.Id));
            ApexPages.currentPage().getParameters().put('favId','test');
            PiRenewalPoliciesCtrl PiRenewalPol = new PiRenewalPoliciesCtrl();
            PiRenewalPoliciesCtrl.DocumentWrapper DocWrap = new PiRenewalPoliciesCtrl.DocumentWrapper();
            system.debug('**prodArea** :  '+PiRenewalPol.prodArea);
            
            PiRenewalPol.strSelected = Gardtestdata.clientAcc.Id;
            PiRenewalPol.selectLocalClient = Gardtestdata.clientAcc.Id;
            PiRenewalPol.selectedPolicyYr.add('2015');
            PiRenewalPol.selectedPolicyYr.add('2014');
            PiRenewalPol.selectedProductArea.add('P&I');
            PiRenewalPol.selectedDocType.add('test');
            PiRenewalPol.selectedCover.add('P&I');
            PiRenewalPol.selectedCover.add('P&I Cover');
            PiRenewalPol.PolicyYr.add(new selectOption('2015','2015'));
            PiRenewalPol.ProductArea.add(new selectOption('P&I','P&I'));
            PiRenewalPol.coverOption.add(new selectOption('P&I','P&I'));
            PiRenewalPol.docTypeOption.add(new selectOption('test','test'));
            PiRenewalPol.currentDocumentId = '2asas123';
            PiRenewalPol.isDownload = 'true';
            PiRenewalPol.isDisplay = 'true';
            PiRenewalPol.newFilterName = 'testObj';
            PiRenewalPol.strDocId = docMjunctnObj.Id;
            List<DocumentMetadata_Junction_object__c> docJuncObjLst = new List<DocumentMetadata_Junction_object__c>();
            docJuncObjLst.add(GardTestData.junc_object_broker);
            PiRenewalPol.own_contract_id_set.add(GardTestData.brokercontract_1st.Id);
            PiRenewalPol.own_contract_id_set.add(GardTestData.clientcontract_1st.Id);
            PiRenewalPol.populateFilterData(docJuncObjLst);
            PiRenewalPol.displayDoc();
            PiRenewalPol.isDisplay = 'false';
            PiRenewalPol.displayDoc();
            PiRenewalPol.getSortDirection();
            PiRenewalPol.deleteDoc();
            PiRenewalPol.createFavourites();
            PiRenewalPol.deleteFavourites();
            PiRenewalPol.viewPolicies();
            PiRenewalPol.exportToExcel();
            PiRenewalPol.closeconfirmPopUp();
            PiRenewalPol.displayEditPopUp(); 
            //PiRenewalPol.viewPolicies();
            PiRenewalPol.first();
            PiRenewalPol.next();
            PiRenewalPol.last();
            PiRenewalPol.previous();
            PiRenewalPol.getShares();
            PiRenewalPol.docDownload();
            PiRenewalPol.strDocId = null;
            PiRenewalPol.docDownload();
            PiRenewalPoliciesCtrl.getGenerateTimeStamp();
            PiRenewalPoliciesCtrl.getGenerateTime();
            PiRenewalPoliciesCtrl.getUserName();
            PiRenewalPol.setpageNumber();
            PiRenewalPol.clearOptions();
            PiRenewalPol.fetchSelectedClients(); 
            DocWrap.isDownloadExist = true;
            DocWrap.cont = 1;
            PiRenewalPol.search();
        }
        
        account ClientAccountTest = new account(name = 'test Client',
                                                Market_Area__c = GardTestData.Markt.id,
                                                Area_Manager__c = GardTestData.salesforceLicUser.id,
                                                Type =   'client',
                                                company_Role__c =   'client',
                                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                                Role_Client__c = true
                                                );
        insert ClientAccountTest ;
        
        Extranet_Global_Client__c  global_client_testing = new Extranet_Global_Client__c(Selected_By__c= GardTestData.clientUser.id,
                                                                                         Selected_For_Client__c= GardTestData.clientAcc.id,
                                                                                         Selected_Global_Clients__c = ClientAccountTest.id
                                                                                         );
        insert global_client_testing;
        
        System.runAs(Gardtestdata.clientUser){
            PageReference pageRef = page.PiRenewalPolicies;
            Test.setCurrentPage(pageRef);
            
            ApexPages.currentPage().getParameters().put('favId','test');
            PiRenewalPoliciesCtrl PiRenewalPolClient2 = new PiRenewalPoliciesCtrl();
            system.debug('***visibility**: '+ [select id,Visibility__c from Document_Metadata__c where id =: GardTestData.clientDoc.Id].Visibility__c );
            ApexPages.currentPage().getParameters().put('favId',String.valueOf(Gardtestdata.test_fav.Id));
            PiRenewalPoliciesCtrl PiRenewalPolClient = new PiRenewalPoliciesCtrl();
            PiRenewalPolClient2.isPageLoad = true;
            PiRenewalPolClient2.fetchSelectedClients();
            PiRenewalPolClient2.strSelected = Gardtestdata.clientAcc.Id + ';' + ClientAccountTest.Id;
            PiRenewalPolClient2.fetchSelectedClients();
            PiRenewalPolClient.hasNext = true;
            PiRenewalPolClient.hasPrevious = true;
            PiRenewalPolClient.pageNumber = 3;
            system.debug('**getFilterCriteria**: ' + PiRenewalPolClient.selectedGlobalClient);
            system.debug('**clientId**: ' + gardtestdata.clientAcc.Id);
        }
        test.stopTest();
    }
}