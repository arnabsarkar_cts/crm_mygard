@istest


Public Class TestPortfolioReportsCtrl_for_Marine{
    
    private String payload = 'payload ';
    private String client = 'client ';
    private String broker = 'broker';
    private String coverGroup = 'coverGroup ';
    private String agreementType = 'agreementType ';
    private string dashboardname = 'dashboardname ';
    public boolean IsPeopleLineUser;
    static AdminUsers__c numberofusers;
    static GardTestData test_data;
    static PortfolioReportsCtrl_for_Marine_endpoint__c testEndPoint ;
    
    // public static PIGeneralIncrease__c pigi = new PIGeneralIncrease__c(); //commented object is not in used
    
    //public static testMethod void TestPortfolioReportsCtrl(){
    public static void createData(){
        //numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        //insert numberofusers;
        test_data=new GardTestData();
        test_data.commonRecord();
		Test.startTest();
        /*pigi.About__c = 'this information is for the testing purpose testing testing testing testing testing testing';
        insert pigi;*/
        testEndPoint = new PortfolioReportsCtrl_for_Marine_endpoint__c();
        testEndPoint.Name = 'endpoint';
        testEndPoint.Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/Reports/ReportsFetcher/ReportsService';
        insert testEndPoint;
        //GardTestData.clientAcc.Company_ID__c = '64144';
        //update GardTestData.clientAcc;
        //Invoking methods of PortfolioReportsCtrl for broker and client users...
        system.AssertEquals(GardTestData.brokerAcc.Name , 'testre' );
        List<Account_Contact_Mapping__c> acms = new List<Account_Contact_Mapping__c>();
        GardTestData.Accmap_1st.IsPeopleClaimUser__c = true;
        GardTestData.Accmap_2nd.IsPeopleClaimUser__c = true;
        acms.add(GardTestData.Accmap_1st);
        acms.add(GardTestData.Accmap_2nd);
        update acms;
        PIrenewalAvailability__c piraTest = new PIrenewalAvailability__c(
            Name = 'value'
        );
        insert piraTest;
        List<MyGardDocumentAvailability__c> mgDocAvailabilityCSs = new List<MyGardDocumentAvailability__c>();
        mgDocAvailabilityCSs.add(new MyGardDocumentAvailability__c(name = 'P&I Renewal Blue Card Tab',IsAvailable__c = true));
        mgDocAvailabilityCSs.add(new MyGardDocumentAvailability__c(name = 'P&I Renewal Certificate Tab',IsAvailable__c = true));
        mgDocAvailabilityCSs.add(new MyGardDocumentAvailability__c(name = 'My Portfolio Document Tab',IsAvailable__c = true));
        mgDocAvailabilityCSs.add(new MyGardDocumentAvailability__c(name = 'Object Details Document Tab',IsAvailable__c = true));
        insert mgDocAvailabilityCSs;
        
        //Added after SF-4650 STARTS
        String strStartTime,strEndTime,strName;
        strName = 'PortfolioDataLoad';
        DateTime dtNow = DateTime.now();
        strStartTime = dtNow.format('hh:mm','GMT');
        strEndTime = dtNow.addHours(1).format('hh:mm','GMT');
        PortfolioReportLoadTime__c prlt = new PortfolioReportLoadTime__c(name = strName,
                                                                         startTime__c = strStartTime,
                                                                         endTime__c = strEndTime
                                                                        );
        insert prlt;
        //Added after SF-4650 ENDS
        
        /*GardTestData.clientAsset_1st.Cover_Group_Code__c = 'CR';
GardTestData.clientAsset_2nd.Cover_Group_Code__c = 'CR';

update GardTestData.clientAsset_1st;
update GardTestData.clientAsset_2nd;*/
        
        Asset Asset_Client = new Asset(Name= 'AssetForResultSet',
                                       accountid = GardTestData.clientAcc.id,
                                       contactid = GardTestData.clientContact.Id,  
                                       Agreement__c = GardTestData.clientContract_1st.id, 
                                       Cover_Group__c =  'P&I' ,
                                       CurrencyIsoCode= 'USD'  ,
                                       product_name__c =  'P&I Cover' ,
                                       Object__c = GardTestData.test_object_2nd.id,
                                       Underwriter__c = GardTestData.clientUser.id,
                                       On_risk_indicator__c=true,
                                       Risk_ID__c = '564759ght',
                                       Cover_Group_Code__c = 'CR',
                                       Expiration_Date__c = date.today().adddays(50),
                                       Inception_date__c = date.today()
                                      ); 
        insert Asset_Client;
    }
    
    
    @isTest private static void method1(){
        createData();
        System.runAs(GardTestData.brokerUser){
        //test.startTest();
            //test.starttest();
            PortfolioReportsCtrl_for_Marine test_broker = new  PortfolioReportsCtrl_for_Marine();
            pagereference pgref = page.PortfolioReports;
            test.setcurrentpage(pgref);
            ApexPages.CurrentPage().getParameters().put('paramClient', gardtestdata.clientAcc.id);
            ApexPages.CurrentPage().getParameters().put('SelectedAgreementType' , 'paramAgreement');
            ApexPages.CurrentPage().getParameters().put('SelectedCoverGroup ' , 'paramCoverGroup');
            ApexPages.currentPage().getParameters().put('paramValue',gardtestdata.clientAcc.GUID__c );
            //test_broker.SelClient = gardtestdata.clientAcc.id;
            
            test_broker.AccountTestId = gardtestdata.clientAcc.Id; 
            
            PortfolioReportsCtrl_for_Marine.isPeopleClaimUser();
            test_broker.rerenderList();
            test_broker.SelectedDashboard = 'Marine';
            test_broker.getAgreementList();
            test_broker.rerenderListCover();
            //test_broker.SelectedDashboard = 'P&I Reports';
            test_broker.getCoverGroupList();
            test_broker.PopulateDashBoardList();
            PortfolioReportsCtrl_for_Marine.Data dt = new PortfolioReportsCtrl_for_Marine.Data();
            //  test_broker.getNewReport();
            //string htmldata = 'abc';
            //   test_broker.retriveHtmlData( ); //string
            test_broker.callFinalReport();
            test_broker.SelectedDashboard = 'P&I Reports';
            test_broker.callFinalReport();
            PortfolioReportsCtrl_for_Marine.data temp = new PortfolioReportsCtrl_for_Marine.data();
            test_broker.hideSearch = true;
            test_broker.searchUrl = 'ABC';
            PortfolioReportsCtrl_for_Marine.redirectUrl = 'SBC';
            test_broker.showReport = true;
            test_broker.htmlRequest = 'ABC';
            test_broker.displayReport = true;
            test_broker.selectedGlobalClient.add(GardTestData.clientAcc.id);     
            test_broker.getClientList();   
            System.assertEquals(test_broker.isBroker , true);
            test_broker.selectedGlobalClient.clear();       
            test_broker.selectedGlobalClient = null;        
            test_broker.getClientList();        
            test_broker.SelectedClient = null;      
            test_broker.getAgreementList();     
            test_broker.SelectedClient = null;      
            test_broker.exportToExcelData();
            
            //test_broker.SelectedAgreementType = 'Mou Operators';
            test_broker.getCoverGroupList();
            //test_broker.SelectedClient = String.valueOf(GardTestData.clientAcc.guid__c);
            test_broker.SelectedDashboard = 'P&I Reports';
            test_broker.SelectedClient = GardTestData.clientAcc.guid__c;
            test_broker.getCoverGroupList();
            test_broker.getAgreementList();
            test_broker.loadLossReport();
            String xyz = test_broker.xlsHeader;
            test_broker.blueCardTab = false;
            test_broker.certificateTab = true;
            test_broker.portfolioDocumentTab = true;
            test_broker.acc = new Account();
            
            test_broker.fetchSelectedClients();
            PortfolioReportsCtrl_for_Marine.processDeleteDocss(new List<Id>());
            //list<selectoption> piclist = test_broker.getPicklistValues(); //selop
            
            //test_broker.ViewDataRemote(String SelClient, String SelectedAgreementType,String SelectedCoverGroup );
            test.stopTest();
        }
        
    }//method 1 ENDS
    
    @isTest private static void method2(){
        //Test.StartTest();
        createData();
        System.runAs(GardTestData.clientuser){
            //Test.StartTest();
            PortfolioReportsCtrl_for_Marine test_client = new  PortfolioReportsCtrl_for_Marine();
            test_client.AccountTestId = gardtestdata.clientAcc.Id;
            
            test_client.rerenderList();
            
            
            test_client.rerenderListCover();
            //test_client.SelectedDashboard = 'P&I Reports';
            //test_client.getCoverGroupList();
            test_client.PopulateDashBoardList();
            pagereference pgref = page.PortfolioReports;
            test.setcurrentpage(pgref);
            ApexPages.CurrentPage().getParameters().put('paramClient', gardtestdata.clientAcc.id);
            ApexPages.CurrentPage().getParameters().put('SelectedAgreementType' , 'paramAgreement');
            ApexPages.CurrentPage().getParameters().put('SelectedCoverGroup ' , 'paramCoverGroup');
            // test_client.SelClient = gardtestdata.clientAcc.id;
            //  test_client.getNewReport(); 
            String payload = 'payload ';
            String client = String.valueOf(GardTestData.clientAcc.guid__c);
            system.debug('clientAcc --->'+[select name,guid__c from Account where name = 'Testuu']+GardTestData.clientAcc.guid__c);
            String broker = 'broker';
            String coverGroup = 'coverGroup ';
            String agreementType = 'agreementType ';
            string dashboardname = 'dashboardname ';
            test_client.AccountTestId = gardtestdata.clientAcc.Id;
            PortfolioReportsCtrl_for_Marine.data testobj = new PortfolioReportsCtrl_for_Marine.data();
            //testobj reportName;
            testobj.setPayload(payload); 
            testobj.setClient(client);
            testobj.setBroker(broker);
            testobj.setCoverGroup(coverGroup);
            testobj.setAgreementType(agreementType);
            testobj.setDashboardname(dashboardname);
            //test_client.getReport(testobj);
            test_client.SelClient = gardtestdata.clientAcc.GUID__c; 
            String SelectedAgreementType = gardtestdata.clientContract_1st.id;
            String SelectedCoverGroup = gardtestdata.clientAsset_1st .id;
            //   PortfolioReportsCtrl_for_Marine.ViewDataRemote(SelClient,SelectedAgreementType,SelectedCoverGroup);
            test_client.strSelected = gardtestdata.clientAcc.id+';'+gardtestdata.clientAcc.id;
            //test_client.fetchSelectedClients();
            test_client.SelectedDashboard = 'P&I Reports';
            test_client.SelectedAgreementType = SelectedAgreementType;
            test_client.SelectedClient = GardTestData.clientAcc.guid__c;
            test_client.getAgreementList();
            //test_client.getCoverGroupList();
            test_client.clientId = null;     
            test_client.IsPeopleLineUser = true;
            //test_client.getClientList();
            System.assertEquals(test_client.isBroker , false);
            test_client.fetchSelectedClients();
            Test.stopTest();
        }
        
    }
    /*
    @isTest private static void method3(){
        createData();
        System.runAs(GardTestData.clientuser){
            PortfolioReportsCtrl_for_Marine test_client = new  PortfolioReportsCtrl_for_Marine();
            test_client.AccountTestId = gardtestdata.clientAcc.Id;
            PortfolioReportsCtrl_for_Marine.redirectUrl = 'SBC';
            test_client.hideSearch = true;
            test_client.searchUrl = 'ABC';
            test_client.showReport = true;
            test_client.htmlRequest = 'ABC';
            test_client.displayReport = true;
            test_client.SelectedDashboard = 'P&I Reports';
            //test_client.testCallFinalReport();
        }
    }
    */
}