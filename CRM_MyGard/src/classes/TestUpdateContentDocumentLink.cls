@isTest
public class TestUpdateContentDocumentLink{

static testMethod void testcontentDocLink(){
Case c = new Case();
c.status='New';
insert c;
//Matter__c thisMatter = [select id,Request__r.Requestor__r.Id from matter__c  limit 1];
   Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
    ContentVersion contentVersion_1 = new ContentVersion(
        Title='Header_Picture1', 
        PathOnClient ='/Header_Picture1.jpg',
        VersionData = bodyBlob, 
        origin = 'H'
    );
    insert contentVersion_1;
   
    ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
    List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
    
    ContentDocumentLink contentlink = new ContentDocumentLink();
    contentlink.LinkedEntityId = c.id;
    contentlink.contentdocumentid = contentVersion_2.contentdocumentid;
    contentlink.ShareType = 'V';
    contentlink.Visibility='AllUsers';
    insert contentlink; 

}

}