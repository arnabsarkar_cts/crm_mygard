/*************************************************************************************************
    Author      : Martin Gardner
    Company     : cDecisions
    Date Created: 13/09/2013
    Description : This class tests that the deletion trigger on opportunity fires correctly
    Modified    :   
***************************************************************************************************/
@isTest (seeAllData = true) 
private class Opportunity_Delete_Test {

    static testMethod void TestOpportunityDelete() {
        Test.startTest();     
	       //Setup the test data
	       SetupTestData.setupTestData();
	       
	       //Attempt a delete
	       delete SetupTestData.testOpps;
			//check for the Deleted_Record__c records
			List<Deleted_Record__c> delrecs =  new List<Deleted_Record__c>();
			delrecs = [SELECT Id, RecordId__c, Object__c, UnDeleted__c FROM Deleted_Record__c WHERE RecordId__c in: SetupTestData.oppIds];
			
			System.assert(delrecs.Size()>0, '*** No records found in Deleted_Record__c for opps ' + SetupTestData.testOpps);
			if(delrecs.Size()>0){
				System.Debug('*** delrecs = ' + delrecs);
			}
			
			System.assert(delrecs.Size() == SetupTestData.testOpps.Size(), '*** Discrepency between the number of records in delrecs vs testOpps. delrecs.Size() = ' + delrecs.Size() + ' SetupTestData.testOpps.Size() = ' +  SetupTestData.testOpps.Size());

	       
	       
	       //Attempt an undelete
	       undelete SetupTestData.testOpps;
			//check for the undelted Deleted_Record__c records
			List<Deleted_Record__c> udelrecs =  new List<Deleted_Record__c>();
			udelrecs = [SELECT Id, RecordId__c, Object__c, UnDeleted__c FROM Deleted_Record__c WHERE RecordId__c in: SetupTestData.oppIds and UnDeleted__c = true];
			
			System.assert(udelrecs.Size()>0, '*** No undelted records found in Deleted_Record__c for opps ' + SetupTestData.testOpps);
			if(udelrecs.Size()>0){
				System.Debug('*** udelrecs = ' + udelrecs);
			}
			
			System.assert(udelrecs.Size() == SetupTestData.testOpps.Size(), '*** Discrepency between the number of records in udelrecs vs testOpps. delrecs.Size() = ' + udelrecs.Size() + ' SetupTestData.testOpps.Size() = ' +  SetupTestData.testOpps.Size());
	       
	       //Then try another delete
	       delete SetupTestData.testOpps;
			//check for the Deleted_Record__c records
			List<Deleted_Record__c> rdelrecs =  new List<Deleted_Record__c>();
			rdelrecs = [SELECT Id, RecordId__c, Object__c, UnDeleted__c FROM Deleted_Record__c WHERE RecordId__c in: SetupTestData.oppIds];
			
			System.assert(rdelrecs.Size()>0, '*** No records found in Deleted_Record__c for opps ' + SetupTestData.testOpps);
			if(rdelrecs.Size()>0){
				System.Debug('*** rdelrecs = ' + rdelrecs);
			}
			
			System.assert(rdelrecs.Size() == SetupTestData.testOpps.Size(), '*** Discrepency between the number of records in rdelrecs vs testOpps. delrecs.Size() = ' + rdelrecs.Size() + ' SetupTestData.testOpps.Size() = ' +  SetupTestData.testOpps.Size());
	       
	       
	    Test.stopTest();      
    }
}