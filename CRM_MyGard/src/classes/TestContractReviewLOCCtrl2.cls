//Test class for ContractReviewLOCCtrl .
@isTest(seealldata=false)
public class TestContractReviewLOCCtrl2{
    public static string testStr =  'test'; 
// Test method.................................
    public static testmethod void ContractReview(){
//Create test data.............................
    GardTestData testRec = new GardTestData();
    testRec.customsettings_rec();
    testRec.commonrecord();
    testRec.ViewDoc_testrecord();
    system.debug('***********Accmap_1 '+GardTestData.Accmap_1 );
    system.debug('***********Accmap_2 '+GardTestData.Accmap_2 ); 
    GardtestData.test_object_1st.guid__c = '8ce8ac89-a6fd-1836-9e07';
    update GardtestData.test_object_1st; 
    Attachment attach=new Attachment();
    attach.name='Unit Test Attachment';
    attach.body=Blob.valueOf('Unit Test Attachment Body');
    attach.parentId= GardTestData.brokerCase.id;
    attach.ContentType = 'application/pdf';
    insert attach;
    system.assertEquals(attach.name , 'Unit Test Attachment');
    ClientActivityEvents__c testclientActEvent = new ClientActivityEvents__c(
                                                                                Name = 'Crew Contract Review',
                                                                                Event_Type__c = 'Crew Contract Review'
                                                                             );
    insert testclientActEvent;
    
    test.startTest();
    System.runAs(GardTestData.brokerUser){
    
        ContractReviewLOCCtrl cr = new ContractReviewLOCCtrl();
        ContractReviewLOCCtrl.ShowDocs  showDoc = new ContractReviewLOCCtrl.ShowDocs();
        cr.imoNo = 12;
        cr.noImoNo = true;
        cr.lstObject =new List<Object__c>();
        cr.contractName =  testStr  ;
        cr.isConfidential = true;
        cr.isLocReqd = true;
        cr.comments =  testStr  ;
        cr.lOCType =  testStr  ;
        cr.objMode = true;
        cr.prvUrl =  testStr  ;
        cr.isFav = true;
        cr.newFilterName =  testStr  ;
        ApexPages.StandardController scontroller = new ApexPages.StandardController(GardTestData.brokerCase);
        ContractReviewLOCCtrl MyObject=new ContractReviewLOCCtrl(scontroller);
        cr.selectedCover = GardTestData.brokerAsset_2nd.id;
        cr.selectedClient = GardTestData.clientAcc.id;
        //  cr.mapCover.put(GardTestData.brokerAsset_1st.id,GardTestData.brokerAsset_1st);
        cr.strSelected = 'ABc';
        cr.isPhone = true;
        //  cr.caseIns.id= GardTestData.brokerCase.id;
        
        cr.caseIns.OwnerId = GardTestData.brokerUser.id;
        cr.caseIns.accountId = GardTestData.clientAcc.id;
        cr.caseIns.Type = 'Crew Contract Review';
        cr.caseIns.Name_Of_Contract__c =  testStr  ;
        cr.caseIns.Status = 'DM Synchronising';
        List<SelectOption> Countrycode= cr.getCountrycodes();
        List<SelectOption> ClientOp= cr.getClientLst();
        List<SelectOption> CoverOp= cr.getCoverLst();
        ApexPages.CurrentPage().getParameters().put('id',GardTestData.brokerCase.id);
        ApexPages.CurrentPage().getParameters().put('id',GardTestData.clientCase.id);
        List<SelectOption> TypeOfLoc= cr.getTypeOfLocLst();
        ApexPages.CurrentPage().getParameters().put('id',GardTestData.brokerCase.id);
        cr.reDirectToCase();
        cr.relatedLstDocs = new List<gardNoDm.DocumentBase>();
        
        
        cr.retrieveCoverLst();
        cr.selectedCover = GardTestData.brokerAsset_1st.id;
        List<SelectOption> ObjectOp= cr.getObjectLst();
        cr.selectedMultiObj.add(GardTestData.test_object_1st.id);
        cr.attRowNum = 1;
        cr.startDate = '01.01.2015';
        cr.endDate = '01.01.2016';
        cr.saveRecord();
        
        /*cr.isPhone = false;
        cr.saveRecord();
        cr.isEmail = false;
        cr.isPhone = true;
        cr.saveRecord();*/
        
        
        System.assertEquals(ContractReviewLOCCtrl.chkboolean , false);
        cr.caseIns.id= GardTestData.brokerCase.id;
        cr.lstAttachments.add(GardTestData.attachmnt);
        ContractReviewLOCCtrl.ContractReviewRequestWS(GardTestData.brokerCase.id);
        ContractReviewLOCCtrl.changeOwner_WS(GardTestData.brokerUser.id,'Abc');
        ContractReviewLOCCtrl.ShowDocs sd = new ContractReviewLOCCtrl.ShowDocs();
        ContractReviewLOCCtrl.startReview(GardTestData.brokerCase.id);
        ContractReviewLOCCtrl.completeReview(GardTestData.brokerCase.id);
        ContractReviewLOCCtrl.reopen(GardTestData.brokerCase.id);
        
        cr.sendType();
        cr.cancel();
        cr.deleteDocuments();
        ContractReviewLOCCtrl.createTaskOnOwnerChange('abc','xyz',GardTestData.brokerUser.id,GardTestData.brokerCase.id);
        }
    /*System.runAs(GardTestData.clientUser){
        ContractReviewLOCCtrl cr = new ContractReviewLOCCtrl();
        cr.sendType();
        }*/
    test.stopTest();
    }
}