@isTest(seeAllData=false)
public class TestPEMEClinicClientDetailsCtrl
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    public static PEME_Enrollment_Form__c pef;
    public static PEME_Manning_Agent__c  pmg; 
    public static PEME_Debit_note_detail__c pefDebitClient;
    Public static List<Object__c> ObjectList=new List<Object__c>();
    public static List<Contract> contractList=new List<Contract>();
    public static list<Asset> assetTest=new list<Asset>();
    public static User clientUser,brokerUser;
    public static Account clientAcc,brokerAcc,clinicAcc;
    
    private static void createTestData()
    {
        Test.startTest();
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
        Gard_Team__c gardTeam=new Gard_Team__c(P_I_email__c='test@test.com',Region_code__c='CASM', Office__c='Arendal');
        insert gardTeam;
        Country__c country=new Country__c(Claims_Support_Team__c=gardTeam.id);
        insert country;
        
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrcClient);
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Insurance Broker');
        vrcList.add(vrcBroker);
        Valid_Role_Combination__c vrcESP = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
        vrcList.add(vrcESP);
        insert vrcList;
        User user = new User(
                            Alias = 'standt', 
                            profileId = salesforceLicenseId ,
                            Email='standarduser@testorg.com',
                            EmailEncodingKey='UTF-8',
                            CommunityNickname = 'test13',
                            LastName='Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US',  
                            TimeZoneSidKey='America/Los_Angeles',
                            UserName='test008@testorg.com',
                            ContactId__c = gc.id,
                            City= 'Arendal'
                             );
        insert user ;
        User user1 =new User();
        user1 = [select id, contactid__c from user where id=:user.id];
        Blob b = Crypto.GenerateAESKey(128);            
        String h = EncodingUtil.ConvertTohex(b);            
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20);
        
        brokerAcc = new Account( Name='testre',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.Broker_Contact_Record_Type,
                                        Site = '_www.cts.se',
                                        Type = 'Broker' ,
                                        Area_Manager__c = user1.id,    
                                        company_Role__c = 'Broker',
                                        GUID__c=guid,
                                        Sub_Roles__c = 'Broker - Insurance Broker',
                                        Market_Area__c = Markt.id   ,
                                        Confirm_not_on_sanction_lists__c = true,
                                        License_description__c  = 'some desc',
                                        Description = 'some desc',
                                        Licensed__c = 'Pending',
                                        country__c = country.Id                                                            
                                     );
        AccountList.add(brokerAcc);
        Blob b1 = Crypto.GenerateAESKey(128);            
        String h1 = EncodingUtil.ConvertTohex(b1);            
        String guid1 = h1.SubString(0,8)+ '-' + h1.SubString(8,12) + '-' + h1.SubString(12,16) + '-' + h1.SubString(16,20);
        clientAcc= New Account();
        clientAcc.Name = 'Testt';
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.Area_Manager__c = user1.id;   
        clientAcc.Market_Area__c = Markt.id;  
        clientAcc.company_Role__c = 'Client';
        clientAcc.GUID__c=guid1;
        clientAcc.Country__c = country.Id;
        //clientAcc.Sub_Roles__c = 'ESP - Agent';
        AccountList.add(clientAcc);
        
        Blob b2 = Crypto.GenerateAESKey(128);            
        String h2 = EncodingUtil.ConvertTohex(b2);            
        String guid2 = h2.SubString(0,8)+ '-' + h2.SubString(8,12) + '-' + h2.SubString(12,16) + '-' + h2.SubString(16,20);
         AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        CommonVariables__c common_var_test = new CommonVariables__c(Name = 'ReplyToMailId',Value__c = 'ownergard@gmail.com');
        insert common_var_test ; 
        Account clinicAcc = new Account( Name='testClinic',
                                        BillingCity = 'Bristol',
                                        BillingCountry = 'United Kingdom',
                                        BillingPostalCode = 'BS1 1AD',
                                        BillingState = 'Avon' ,
                                        BillingStreet = '1 Elmgrove Road',
                                        recordTypeId=System.Label.ESP_Contact_Record_Type,
                                        Site = '_www.ctsclinic.se',
                                        Type = 'Surveyor' ,
                                        GUID__c=guid2,
                                        Area_Manager__c = user1.id,    
                                        company_Role__c = 'External Service Provider',
                                        Sub_Roles__c = 'ESP - Agent',
                                        Market_Area__c = Markt.id ,
                                        Country__c = country.Id                                                                 
                                     );
        AccountList.add(clinicAcc);
        insert AccountList; 
         
        Contact brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(brokerContact) ;
        Contact clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              MailingStreet = '4 London Road',
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        Contact clinicContact = new Contact( 
                                             FirstName='medicalTest',
                                             LastName='medical',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = clinicAcc.Id,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(clinicContact) ;
        
        insert ContactList;
        brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='test120@test.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.com.mygard',
                                    ContactId = BrokerContact.Id
                                   );
        UserList.add(brokerUser);
        clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.com.mygard',
                                    ContactId = clientContact.id
                                   );
                
        UserList.add(clientUser) ;
        User clinicUser = new User(
                                    Alias = 'Clinic', 
                                    profileId = partnerLicenseId,
                                    Email='testClinic120@test.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='ClinicTesting',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'clinicUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testClinicUser@testorg.com.mygard',
                                    ContactId = clinicContact.Id
                                   );
        UserList.add(clinicUser);
        insert UserList;
        
       /* acc=new Account();
        acc.Name='test User';
        acc.Billing_Street_Address_Line_1__c='1122 C Test Road';
        acc.Billing_Street_Address_Line_2__c='Check Town';
        acc.Billing_Street_Address_Line_3__c='Test Check City';
        acc.Billing_Street_Address_Line_4__c='';
        acc.BillingCity__c='Test Check City';
        acc.BillingPostalCode__c='112232';
        acc.BillingState__c='Kolkata';
        acc.BillingCountry__c='India';
        acc.Shipping_Street_Address_Line_1__c='1122 C Test Road';
        acc.Shipping_Street_Address_Line_2__c='Check Town';
        acc.Shipping_Street_Address_Line_3__c='Test Check City';
        acc.Shipping_Street_Address_Line_4__c='';
        acc.ShippingCity__c='Test Check City';
        acc.ShippingPostalCode__c='112232';
        acc.ShippingState__c='Kolkata';
        acc.ShippingCountry__c='India';
        acc.Phone='3322334433';
        acc.Fax='2244333454';
        acc.Website='www.abc.com';
        acc.Corporate_email__c='abc@abc.com';
        acc.Mobile_Phone_1__c='3322334433';
      //  insert acc;
       // Blob b = Crypto.GenerateAESKey(128);            
      //  String h = EncodingUtil.ConvertTohex(b);            
      //  String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20);
       // acc.GUID__c=guid;
        insert acc; */
          
        Object__c Object1=new Object__c(Name= 'TestObject1',
                                     //CurrencyIsoCode='NOK-Norwegian Krone',
                                     Object_Unique_ID__c='test11223'
                                     );
        ObjectList.add(Object1);
        Object__c Object2=new Object__c(
                                     Name= 'TestObject2',
                                    // CurrencyIsoCode='NOK-Norwegian Krone',
                                     Object_Unique_ID__c='test11224'
                                     );
        ObjectList.add(Object2);
        insert ObjectList;
        
        pef=new PEME_Enrollment_Form__c ();
        pef.ClientName__c=clientAcc.ID;
        pef.Comments__c='Report analyzed';
        pef.Enrollment_Status__c='Draft';
        pef.Gard_PEME_References__c='Test References';
        pef.Last_Status_Change_Date__c=Date.valueOf('2016-01-27');
     //   pef.Person_in_charge_Email__c='test@mail.com';
     //   pef.Person_in_charge_Name__c='test Person';
        pef.Submitter__c=clinicContact.ID;
        insert pef;
        
        
        pmg=new PEME_Manning_Agent__c  ();
        pmg.Client__c=clientAcc.ID;
        pmg.PEME_Enrollment_Form__c=pef.ID;
        pmg.Company_Name__c='Test Company';
       // pmg.Contact_Person__c='Test Person';
        pmg.Contact_Point__c=brokerContact.Id;
        pmg.Status__c='Approved';
        insert pmg;
        
        pefDebitClient = new PEME_Debit_note_detail__c();
        pefDebitClient.PEME_Enrollment_Form__c = pef.id;
        pefDebitClient.Company_Name__c = clientAcc.id;
        pefDebitClient.Status__c = 'Approved';
        insert pefDebitClient;
        
        pef.Enrollment_Status__c='Under Approval';
        Test.stopTest();
        update pef;
        
        Contract brokerContract=new Contract(
                                            Accountid = brokerAcc.id, 
                                            Account = brokerAcc, 
                                            Status = 'Draft',
                                            CurrencyIsoCode = 'SEK', 
                                            StartDate = Date.today(),
                                            ContractTerm = 2,
                                            Agreement_Type__c = 'Marine', 
                                            Broker__c = brokerAcc.id,
                                            Client__c = clientAcc.id,
                                            Broker_Name__c = brokerAcc.id,
                                            Expiration_Date__c = date.valueof('2016-02-01'),
                                            Inception_date__c = date.valueof('2012-01-01'),
                                            Agreement_Type_Code__c = 'Agreement127',
                                            Agreement_ID__c = 'Agreement_ID_881',
                                            Business_Area__c='Marine',
                                            Accounting_Contacts__c = brokerUser.id,
                                            Contract_Reviewer__c = brokerUser.id,
                                            Contracting_Party__c = brokerAcc.id,
                                            Policy_Year__c='2015',
                                            Name_of_Contract__c = 'ABC'             
                                            );
        contractList.add(brokerContract);                                    
        
        Contract clientContract=new Contract (
                                              Accountid = clientAcc.id, 
                                              Account = clientAcc, 
                                              Status = 'Draft',
                                              CurrencyIsoCode = 'SEK', 
                                              StartDate = Date.today(),
                                              ContractTerm = 2,
                                              //Broker__c = brokerAcc.id,
                                              Client__c = clientAcc.id,
                                              Expiration_Date__c = date.valueof('2016-02-01'),
                                              Agreement_Type__c = 'P&I',
                                              Agreement_Type_Code__c = 'Agreement123',
                                              Inception_date__c = date.valueof('2012-01-01'),
                                              Business_Area__c='P&I',
                                              Policy_Year__c='2014',
                                              Name_of_Contract__c = 'ABC'
                                              );
        contractList.add(clientContract);
        insert contractList;
        
        Asset brokerAsset=new Asset();
        
        brokerAsset.AccountId=brokerAcc.ID;
        brokerAsset.ContactId=brokerContact.ID;
        brokerAsset.Agreement__c=brokerContract.ID;
        brokerAsset.Object__c=Object1.ID;
        brokerAsset.Name='Marine';
        brokerAsset.Expiration_Date__c=date.valueof('2016-02-01');
        brokerAsset.On_risk_indicator__c=true;
        brokerAsset.Inception_date__c =date.valueof('2012-01-01');
        brokerAsset.Risk_ID__c='112233eeffddggff';
        assetTest.add(brokerAsset);
        
        Asset clientAsset=new Asset();
        
        clientAsset.AccountId=clientAcc.ID;
        clientAsset.ContactId=clientContact.ID;
        clientAsset.Agreement__c=clientContract.ID;
        clientAsset.Object__c=Object2.ID;
        clientAsset.Name='P&I Cover';
        clientAsset.Expiration_Date__c=date.valueof('2016-02-01');
        clientAsset.On_risk_indicator__c=true;
        clientAsset.Inception_date__c =date.valueof('2012-01-01');
        clientAsset.Risk_ID__c='112233eeffddggft';
        assetTest.add(clientAsset);
        
        insert assetTest;
        /*Chinku 2
        System.runAs(brokerUser)
        {
            System.currentPageReference().getParameters().put('client',brokerAcc.GUID__c);
           // system.debug('====+++client+++========+++guid+++===='+brokerAcc.GUID__c);
            PEMEClinicClientDetailsCtrl PEMEClinicClientDetailsCntl=new PEMEClinicClientDetailsCtrl();
            system.debug('====+++client+++========+++guid+++===='+PEMEClinicClientDetailsCntl.clientName+PEMEClinicClientDetailsCntl.selectedClient);
           // PEMEClinicClientDetailsCntl.objAcc = new Account(); 
            system.debug('====+++client+++========+++guid+++===='+PEMEClinicClientDetailsCntl.clientName+PEMEClinicClientDetailsCntl.selectedClient);
            PEMEClinicClientDetailsCntl.loadData();
            PEMEClinicClientDetailsCntl.generateClientDetails();
            PEMEClinicClientDetailsCntl.createOpenUserList();
            //PEMEClinicClientDetailsCntl.sortExpression='ASC';
            PEMEClinicClientDetailsCntl.getSortDirection();
            PEMEClinicClientDetailsCntl.setSortDirection('ASC');
            PEMEClinicClientDetailsCntl.getObjectOptions();
            PEMEClinicClientDetailsCntl.firstOpen();
            PEMEClinicClientDetailsCntl.hasPreviousOpen=true;
            PEMEClinicClientDetailsCntl.previousOpen();
            PEMEClinicClientDetailsCntl.setpageNumberOpen();
            PEMEClinicClientDetailsCntl.hasNextOpen=false;
            PEMEClinicClientDetailsCntl.nextOpen();
            PEMEClinicClientDetailsCntl.lastOpen();
            PEMEClinicClientDetailsCntl.selectedObject=new list<String>();
            PEMEClinicClientDetailsCntl.selectedObject.add('check');
            PEMEClinicClientDetailsCntl.generateClientDetails();
            PEMEClinicClientDetailsCntl.noOfRecords=10;
            PEMEClinicClientDetailsCntl.size=10;
            PEMEClinicClientDetailsCntl.intStartrecord=1;
            PEMEClinicClientDetailsCntl.intEndrecord=8;
            PEMEClinicClientDetailsCntl.jointSortingParam='test';
            PEMEClinicClientDetailsCntl.sSoqlQuery='SELECT Object__r.Id,Object__r.Name,Name,Agreement__r.Client__r.GUID__c,Expiration_Date__c,Agreement__r.Policy_Year__c,Object__r.Imo_Lloyds_No__c,On_risk_indicator__c FROM Asset';
            PEMEClinicClientDetailsCntl.print();
            PEMEClinicClientDetailsCntl.exportToExcel();
            PEMEClinicClientDetailsCntl.clearOptions();
    
        }
        */
        /*Chinku
        System.runAs(clientUser)
        {
            System.currentPageReference().getParameters().put('client' ,clientAcc.GUID__c);
            PEMEClinicClientDetailsCtrl PEMEClinicClientDetailsCntl=new PEMEClinicClientDetailsCtrl();
            PEMEClinicClientDetailsCntl.loadData();
            PEMEClinicClientDetailsCntl.generateClientDetails();
            PEMEClinicClientDetailsCntl.createOpenUserList();
            //PEMEClinicClientDetailsCntl.sortExpression='ASC';
            PEMEClinicClientDetailsCntl.getSortDirection();
            PEMEClinicClientDetailsCntl.setSortDirection('ASC');
            PEMEClinicClientDetailsCntl.getObjectOptions();
            PEMEClinicClientDetailsCntl.firstOpen();
            PEMEClinicClientDetailsCntl.hasPreviousOpen=true;
            PEMEClinicClientDetailsCntl.previousOpen();
            PEMEClinicClientDetailsCntl.setpageNumberOpen();
            PEMEClinicClientDetailsCntl.hasNextOpen=false;
            PEMEClinicClientDetailsCntl.nextOpen();
            PEMEClinicClientDetailsCntl.lastOpen();
            PEMEClinicClientDetailsCntl.selectedObject=new list<String>();
            PEMEClinicClientDetailsCntl.selectedObject.add('check');
            PEMEClinicClientDetailsCntl.generateClientDetails();
            //PEMEClinicClientDetailsCntl.loadData();
            //PEMEClinicClientDetailsCntl.print();
            //PEMEClinicClientDetailsCntl.exportToExcel();
            PEMEClinicClientDetailsCntl.noOfRecords=10;
            PEMEClinicClientDetailsCntl.size=10;
            PEMEClinicClientDetailsCntl.intStartrecord=1;
            PEMEClinicClientDetailsCntl.intEndrecord=8;
            PEMEClinicClientDetailsCntl.jointSortingParam='test';
            //PEMEClinicClientDetailsCntl.loadData();
        }
        */
        /*System.runAs(clinicUser)
        {
            System.currentPageReference().getParameters().put('client' ,clinicAcc.GUID__c);
            //PEMEClinicClientDetailsCtrl PEMEClinicClientDetailsCntl=new PEMEClinicClientDetailsCtrl();
            //PEMEClinicClientDetailsCntl.loadData();
            /* PEMEClinicClientDetailsCntl.generateClientDetails();
            PEMEClinicClientDetailsCntl.print();
            PEMEClinicClientDetailsCntl.exportToExcel(); 
        } */
    }
    @isTest public static void cover1(){
	    createTestData();
        System.runAs(clientUser)
        {
            System.currentPageReference().getParameters().put('client' ,clientAcc.GUID__c);
            PEMEClinicClientDetailsCtrl PEMEClinicClientDetailsCntl=new PEMEClinicClientDetailsCtrl();
            PEMEClinicClientDetailsCntl.loadData();
            PEMEClinicClientDetailsCntl.generateClientDetails();
            PEMEClinicClientDetailsCntl.createOpenUserList();
            //PEMEClinicClientDetailsCntl.sortExpression='ASC';
            PEMEClinicClientDetailsCntl.getSortDirection();
            PEMEClinicClientDetailsCntl.setSortDirection('ASC');
            PEMEClinicClientDetailsCntl.getObjectOptions();
            PEMEClinicClientDetailsCntl.firstOpen();
            PEMEClinicClientDetailsCntl.hasPreviousOpen=true;
            PEMEClinicClientDetailsCntl.previousOpen();
            PEMEClinicClientDetailsCntl.setpageNumberOpen();
            PEMEClinicClientDetailsCntl.hasNextOpen=false;
            PEMEClinicClientDetailsCntl.nextOpen();
            PEMEClinicClientDetailsCntl.lastOpen();
            PEMEClinicClientDetailsCntl.selectedObject=new list<String>();
            PEMEClinicClientDetailsCntl.selectedObject.add('check');
            PEMEClinicClientDetailsCntl.generateClientDetails();
            //PEMEClinicClientDetailsCntl.loadData();
            //PEMEClinicClientDetailsCntl.print();
            //PEMEClinicClientDetailsCntl.exportToExcel();
            PEMEClinicClientDetailsCntl.noOfRecords=10;
            PEMEClinicClientDetailsCntl.size=10;
            PEMEClinicClientDetailsCntl.intStartrecord=1;
            PEMEClinicClientDetailsCntl.intEndrecord=8;
            PEMEClinicClientDetailsCntl.jointSortingParam='test';
            //PEMEClinicClientDetailsCntl.loadData();
            /*PEMEClinicClientDetailsCntl.sSoqlQuery='SELECT Object__r.Id,Object__r.Name,Name,Agreement__r.Client__r.GUID__c,Expiration_Date__c,Agreement__r.Policy_Year__c,Object__r.Imo_Lloyds_No__c,On_risk_indicator__c FROM Asset';
            PEMEClinicClientDetailsCntl.print();
            PEMEClinicClientDetailsCntl.exportToExcel();
            PEMEClinicClientDetailsCntl.clearOptions(); */
        }
    }
    @isTest public static void cover2(){
    	createTestData();
        System.runAs(brokerUser)
        {
            System.currentPageReference().getParameters().put('client',brokerAcc.GUID__c);
           // system.debug('====+++client+++========+++guid+++===='+brokerAcc.GUID__c);
            PEMEClinicClientDetailsCtrl PEMEClinicClientDetailsCntl=new PEMEClinicClientDetailsCtrl();
            system.debug('====+++client+++========+++guid+++===='+PEMEClinicClientDetailsCntl.clientName+PEMEClinicClientDetailsCntl.selectedClient);
           // PEMEClinicClientDetailsCntl.objAcc = new Account(); 
            system.debug('====+++client+++========+++guid+++===='+PEMEClinicClientDetailsCntl.clientName+PEMEClinicClientDetailsCntl.selectedClient);
            PEMEClinicClientDetailsCntl.loadData();
            PEMEClinicClientDetailsCntl.generateClientDetails();
            PEMEClinicClientDetailsCntl.createOpenUserList();
            //PEMEClinicClientDetailsCntl.sortExpression='ASC';
            PEMEClinicClientDetailsCntl.getSortDirection();
            PEMEClinicClientDetailsCntl.setSortDirection('ASC');
            PEMEClinicClientDetailsCntl.getObjectOptions();
            PEMEClinicClientDetailsCntl.firstOpen();
            PEMEClinicClientDetailsCntl.hasPreviousOpen=true;
            PEMEClinicClientDetailsCntl.previousOpen();
            PEMEClinicClientDetailsCntl.setpageNumberOpen();
            PEMEClinicClientDetailsCntl.hasNextOpen=false;
            PEMEClinicClientDetailsCntl.nextOpen();
            PEMEClinicClientDetailsCntl.lastOpen();
            PEMEClinicClientDetailsCntl.selectedObject=new list<String>();
            PEMEClinicClientDetailsCntl.selectedObject.add('check');
            PEMEClinicClientDetailsCntl.generateClientDetails();
            //PEMEClinicClientDetailsCntl.print();
            //PEMEClinicClientDetailsCntl.exportToExcel();
            //PEMEClinicClientDetailsCntl.clearOptions();
            /* PEMEClinicClientDetailsCntl.generateClientDetails();
            //PEMEClinicClientDetailsCntl.loadData();
            PEMEClinicClientDetailsCntl.print();
            PEMEClinicClientDetailsCntl.exportToExcel(); */
            PEMEClinicClientDetailsCntl.noOfRecords=10;
            PEMEClinicClientDetailsCntl.size=10;
            PEMEClinicClientDetailsCntl.intStartrecord=1;
            PEMEClinicClientDetailsCntl.intEndrecord=8;
            PEMEClinicClientDetailsCntl.jointSortingParam='test';
            PEMEClinicClientDetailsCntl.sSoqlQuery='SELECT Object__r.Id,Object__r.Name,Name,Agreement__r.Client__r.GUID__c,Expiration_Date__c,Agreement__r.Policy_Year__c,Object__r.Imo_Lloyds_No__c,On_risk_indicator__c FROM Asset';
            PEMEClinicClientDetailsCntl.print();
            PEMEClinicClientDetailsCntl.exportToExcel();
            PEMEClinicClientDetailsCntl.clearOptions();
    
        }
    }
}