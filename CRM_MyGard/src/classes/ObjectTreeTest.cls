/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class ObjectTreeTest {
    public static Integer numLevels = 3;
    public static Integer numAccountsPerLevel = 2;
    public static List<Account> testAccounts;
    public static List<Account> lowestLevelAccounts;
    public static Set<Id> lowestLevelAccountIds;
    public static void setupTestData(){
            
                //setup top level
                List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
                Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
                vrcList.add(vrcClient);
                insert vrcList;
                Gard_Team__c gt = new Gard_Team__c(Active__c = true,Office__c = 'test',Name = 'GTeam',Region_code__c = 'TEST');
                insert gt;
                Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Gard_Team__c=gt.Id,Claims_Support_Team__c=gt.Id);
                insert country;
                String accountName= 'TestObjectTree';
                Account template = new Account(name=accountName+'0', ParentId = null, Client_On_Risk_Flag__c = true, Child_Company_On_Risk__c = false, ShippingStreet='1 Main Street', ShippingState='TX', ShippingPostalCode='123456', ShippingCountry='US', ShippingCity='Any Town', Description='This is a test account', BillingStreet='1 Main Street', BillingState='TX', BillingPostalCode='123456', BillingCountry='USA', BillingCity='Any Town', AnnualRevenue=10000,Country__c = country.id);
                
                list<Account> prevLevelAccounts = new list<Account>();
                testAccounts = new List<Account>();
                lowestLevelAccounts = new List<Account>();
                
                for(Integer i=0;i<numLevels;i++){
                    list<Account> levelAccounts = new list<Account>();      
                    
                    if(prevLevelAccounts==null || prevLevelAccounts.size()==0){
                        //Top level
                        for(Integer j=0;j<numAccountsPerLevel;j++){
                            levelAccounts.add(template.clone(false,true));
                            //insert levelAccounts;
                        }
                    }else{
                        //iterate through each account in the level before
                        for(Account parent:prevLevelAccounts){
                            //clone the template numAccountsPerLevel times
                            for(Integer j=0;j<numAccountsPerLevel;j++){
                                Account thisAccount = template.clone(false,true);
                                thisAccount.ParentId = parent.Id;
                                levelAccounts.add(thisAccount);             
                            }
                        }
                    }
                    
                    if(levelAccounts!=null && levelAccounts.size()>0){
                        Boolean flipFlop = true;
                        for(Account eachAccount : levelAccounts){
                            if(!flipFlop){
                                eachAccount.Client_On_Risk_Flag__c = !eachAccount.Client_On_Risk_Flag__c;
                                eachAccount.Child_Company_On_Risk__c = true;
                            }
                            flipFlop = !flipFlop;
                        }
                        insert levelAccounts;
                        prevLevelAccounts = levelAccounts.deepClone(true);
                        testAccounts.addAll(levelAccounts.deepClone(true));
                        if(i==numLevels-1){
                            lowestLevelAccounts.addAll(levelAccounts.deepClone(true));
                            if(lowestLevelAccountIds==null){lowestLevelAccountIds = new Set<Id>();}
                            for(Account acc:lowestLevelAccounts){
                                lowestLevelAccountIds.add(acc.Id);
                            }
                        }
                    }
                    
                }
                System.Debug('Created ' + testAccounts.size() + ' accounts for testing.');
                for(Account acc : testAccounts){
                    System.debug('Account - '+acc.id+' - '+acc.parentId);
                }
            
            //return setupTestData;           
       }
    
    static testMethod void objectTreeTest() {
        Test.startTest();
        setupTestData();
        ObjectTree.TestTrigger = true;
        //List<Account> testData = setupTestData;
        System.Debug('Test Data ->' + testAccounts);
        Test.stopTest();
        
        try{
        //Run through the lowest level and set the on risk flag to true
        /* for(Account acc:lowestLevelAccounts){
            acc.On_Risk__c = true;
        }
        update lowestlevelaccounts;*/
        List<Account> accounts = [SELECT Id, ParentId, Name, On_Risk__c, Child_Company_On_Risk__c, Type FROM Account WHERE Id in :lowestLevelAccountIds];
        List<sObject> updateResults = new List<sObject>();
        List<sObject> updateResults2 = new List<sObject>();
        List<sObject> updateResults3 = new List<sObject>();
        
        //Take the first account and setup a new ObjectTree for it.
        ObjectTree testTree = new ObjectTree(accounts[0]);
        
        
            
            
            String pqs = testTree.PrimaryObjectQueryString;
            String oqs = testTree.ObjectQueryString;
            String cqs = testTree.ChildQueryString;
            String paqs = testTree.ParentQueryString;
            
            //updateResults = ObjectTree.updateParentRisk(lowestLevelAccounts);
            /*
            ObjectTree.TestTrigger = true;
            //Now test for off risk
            ObjectTree.antiRecursionflag = false;
            for(Account acc:lowestLevelAccounts){
                acc.Child_Company_On_Risk__c = true;
                acc.Client_On_Risk_Flag__c = false;//on_risk__c
            }
            update lowestLevelAccounts;
            //updateResults2 = ObjectTree.updateParentRisk(testAccounts);
            */
            //Test off risk option
            ObjectTree.antiRecursionflag = false;
            ObjectTree.TestTrigger = true;
            for(Account acc:testAccounts){
                acc.Child_Company_On_Risk__c = false;
                acc.Client_On_Risk_Flag__c = !acc.Client_On_Risk_Flag__c;//on_risk__c
            }
            update testAccounts;
            
            ObjectTree.antiRecursionflag = false;
            ObjectTree.TestTrigger = true;
            for(Account acc:testAccounts){
                acc.Child_Company_On_Risk__c = false;
                acc.Client_On_Risk_Flag__c = !acc.Client_On_Risk_Flag__c;//on_risk__c
            }
            //update testAccounts;
            //updateResults3 = ObjectTree.updateParentRisk(testAccounts);
            
        
        }catch(Exception ex){System.debug('Exception caught - '+ex.getMessage());}
    }

}