@isTest
public class TestUpdateMembershipType{
    private static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    private static String strAMId = [SELECT Id FROM User WHERE Position__c='Area Manager' LIMIT 1].Id;
    private static String strSVP1Id = [SELECT Id FROM User WHERE Position__c='SVP' LIMIT 1].Id;
    private static String strSVP2Id = UserInfo.getUserId();
    private static User salesforceUser;
    private static List<Valid_Role_Combination__c> vrcList;
    private static Market_Area__c Markt; 
    private static Country__c country;
    private static Gard_Contacts__c gc;
    private static Account acc;
    private static Company_Membership_Review__c rev;
    
    //Test Data Setup Methods
    //Create a user
    private static void createUser(){
        salesforceUser = new User(
            Alias = 'standt', 
            profileId = salesforceLicenseId ,
            Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8',
            CommunityNickname = 'test13',
            LastName='Testing',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',  
            TimeZoneSidKey='America/Los_Angeles',
            UserName='test008@testorg.com'
        );
        insert salesforceUser; 
    }
    //create an account record
    private static void createAccount(){
        acc = new Account(  Name='APEXTESTACC001',
                          role__c = vrcList.get(1).role__c,
                          Sub_Roles__c = vrcList.get(1).Sub_Role__c,
                          BillingCity = 'Bristol',
                          BillingCountry = 'United Kingdom',
                          BillingPostalCode = 'BS1 1AD',
                          BillingState = 'Avon' ,
                          BillingStreet = '1 Elmgrove Road',
                          Area_Manager__c = salesforceUser.id,//TestDataGenerator.getMarketAreaManagerUser().Id,
                          Market_Area__c = Markt.id,//TestDataGenerator.getMarketArea().Id,
                          country__c = country.id,
                          OwnerId = salesforceUser.id,
                          Type = 'Customer',
                          Membership_Type__c = '',
                          Membership_Status__c = 'Approved'
                         );
        insert acc;
    }
    //create a membership review record
    private static void createCoMembership(){
        
        rev = new Company_Membership_Review__c();
        rev.Company_Name__c = acc.id ;
        rev.Area_Manager_del__c = strAMId;
        rev.Senior_Approver_1__c = strSVP1Id;
        rev.Senior_Approver_2__c = strSVP2Id;
        rev.Confirm_company_not_on_santion_list__c = true;
        rev.status__c = 'approved';
        rev.Approval_type_required__c = 'Shipowners, MOU, Charterers P&I approval';
        insert rev;
    }
    //prerequisite data
    private static void createPrerequisiteData(){
        AdminUsers__c adminUsersCS = new AdminUsers__c();
        adminUsersCS.Name = 'Number of users';	
        adminUsersCS.Value__c = 3;
        insert adminUsersCS;
        
        vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrc;
        vrc = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        vrcList.add(vrc);
        vrc = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrc);
        vrc = new Valid_Role_Combination__c(Role__c = 'External Service Provider',Sub_Role__c='ESP - Agent');
        vrcList.add(vrc);
        insert vrcList;
        
        Markt = new Market_Area__c(Market_Area_Code__c = 'abc120000');
        insert Markt;  
        
        if(country == null){
            country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA');
            insert country;
        }
        
        gc = new Gard_Contacts__c(FirstName__c ='test1',lastName__c = 'test1');
        insert gc;
    }
    
    //Test Setup Driver
    private static void createTestData(){
        //pre-requisite data
        createPrerequisiteData();
        
        //main data
        createUser();
        createAccount();
        createCoMembership();
        
        List<Account> accounts = [Select id, name, Membership_Type__c,P_I_Member_Flag__c,Type,Membership_Status__c,PI_member_review_count__c from Account where Id = :acc.id];
        accounts[0].Membership_Type__c = '';
        update accounts;        
    }
    
    //Test Method
    @isTest static void UpdateMembershipTypeMethod(){
        UpdateMembershipType UpdateMType = new UpdateMembershipType();
        Test.startTest();
        createTestData();
        UpdateMType.doOperation();
        Test.stopTest();
    }
}