public with sharing class SearchHelper {
    public static string searchBySoql(String textToBeSearched, String objSearch, String extraWhereClause, Boolean IsPeopleLUser){
        
        textToBeSearched = textToBeSearched.remove('"*').remove('*"');
        System.Debug('*** textToBeSearched: ' + textToBeSearched);
        String stringStr = 'String';
        String orStr = ' OR ';
        String whereStr = ' Where';
        String fromStr = ' from ';
        String textToBeSearchedfull = '\'%' + textToBeSearched + '%\'';
        System.Debug('*** textToBeSearchedfull : ' + textToBeSearchedfull );
        String searchClause='';  
        String selectFiled = '';  
        String finalQuery = '';
        String hidePemeObject = MyGardHelperCtrl.hidePrepemeObject(); // for 4687
        String hidePemeObj = '%' + hidePemeObject + '%';  // for 4687
        
        Map<String, Schema.SObjectField> FieldMap = Schema.getGlobalDescribe().get(objSearch).getDescribe().fields.getMap();
        /*  Start: New Code for Search with Custom settings*/
        string finalSearchFields = '';
        string actualSearchField = '';
        string finalSearchFieldsGardContacts = '';
        string actualSearchFieldGardContacts = '';
        Map<String, Search_Fields__c> allSearchFields = Search_Fields__c.getAll();
        System.Debug('*** getAll : ' + Search_Fields__c.getAll());
        for(String singleKey : allSearchFields.keySet()){
            if(singleKey.contains(objSearch)){
                Search_Fields__c singleSearchField = allSearchFields.get(singleKey);
                actualSearchField = singleSearchField.FieldName__c;
                finalSearchFields = finalSearchFields + actualSearchField + ',';
            } 
            else if(singleKey.contains('Gard_Cont'))
            {
                Search_Fields__c singleSearchFieldGardContact = allSearchFields.get(singleKey);
                actualSearchFieldGardContacts = singleSearchFieldGardContact.FieldName__c;
                finalSearchFieldsGardContacts = finalSearchFieldsGardContacts + actualSearchFieldGardContacts + ',';
            }
            System.Debug('finalSearchFields..: ' + finalSearchFields);
            System.Debug('finalSearchFieldsGardContacts..: ' + finalSearchFieldsGardContacts);
        } 
        if(finalSearchFields.contains(','))
            finalSearchFields = finalSearchFields.substring(0,finalSearchFields.lastIndexOf(','));
        if(finalSearchFieldsGardContacts.contains(','))
            finalSearchFieldsGardContacts = finalSearchFieldsGardContacts.substring(0,finalSearchFieldsGardContacts.lastIndexOf(','));
        System.Debug('finalSearchFields11..: ' + finalSearchFields);
        /*  End : New Code for Search with Custom settings*/
        
        if(objSearch == 'Contact')
        {
            for(Schema.SObjectField f : FieldMap.values()){ 
                Schema.DescribeFieldResult DescField = f.getDescribe();
                selectFiled =  selectFiled  + DescField.getName()+ ', ';
                if(String.valueOf(DescField.getType()).equalsIgnoreCase(stringStr)){
                    searchClause =  searchClause + ' ' + DescField.getName() + ' LIKE ' + textToBeSearchedfull + orStr;
                }
            }
            searchClause = searchClause.removeEnd(orStr);
            if(extraWhereClause !=null){
                searchClause =  ' ('+searchClause + ') '+extraWhereClause;
            }
            system.debug('****** searchClause Contact *****' + searchClause);
            if(finalSearchFields.equalsIgnoreCase('')){
                finalQuery= 'select id, Account.Name from '+ objSearch +whereStr+ searchClause +' order by LastModifiedDate limit 20';
            }else{
                finalQuery= 'select id, Account.Name, '+ finalSearchFields +fromStr+ objSearch +whereStr+ searchClause +' order by LastModifiedDate limit 20';
            }
            
            system.debug('******finalQuery Contact *****' + finalQuery);
        }
        else  if(objSearch == 'Object__c')
        {
            for(Schema.SObjectField f : FieldMap.values()){ 
                Schema.DescribeFieldResult DescField = f.getDescribe();
                selectFiled =  selectFiled  + DescField.getName()+ ', ';
                if(String.valueOf(DescField.getType()).equalsIgnoreCase(stringStr)){
                    searchClause =  searchClause + ' ' + 'Object__r.' +DescField.getName() + ' LIKE ' + textToBeSearchedfull + orStr;
                }
            }
            searchClause = searchClause.removeEnd(orStr);
            if(extraWhereClause !=null){
                searchClause =  ' ('+searchClause + ') '+extraWhereClause;
            }
            system.debug('****** searchClause object__c *****' + searchClause);
            if(finalSearchFields.equalsIgnoreCase('')){
                finalQuery= 'select Object__c,Object__r.guid__c, Product_Name__c, Object__r.Name, Object__r.Object_Type__c, Object__r.Object_Sub_Type__c, Object__r.Imo_Lloyds_No__c, Agreement__r.Policy_Year__c from  Asset  WHERE  '+ searchClause +' and On_risk_indicator__c=true AND (NOT(Object__r.name LIKE : hidePemeObj)) order by Object__r.LastModifiedDate limit 20';   
            }else{
                finalQuery= 'select Object__c,Object__r.guid__c, Product_Name__c, Object__r.Name, Object__r.Object_Type__c, Object__r.Object_Sub_Type__c, Object__r.Imo_Lloyds_No__c, Agreement__r.Policy_Year__c from  Asset  WHERE '+ searchClause +' and On_risk_indicator__c=true AND (NOT(Object__r.name LIKE : hidePemeObj)) order by Object__r.LastModifiedDate limit 20';  
            }
            system.debug('******finalQuery object__c *****' + finalQuery);
        }
        else if(objSearch == 'Gard_Contacts__c')
        {
            system.debug('****** searchClause Gard1 *****' + searchClause);
            for(Schema.SObjectField f : FieldMap.values()){ 
                Schema.DescribeFieldResult DescField = f.getDescribe();
                selectFiled =  selectFiled  + DescField.getName()+ ', ';
                if(String.valueOf(DescField.getType()).equalsIgnoreCase(stringStr)){
                    searchClause =  searchClause + ' ' + DescField.getName() + ' LIKE ' + textToBeSearchedfull + orStr;
                }
            }
            searchClause = searchClause.removeEnd(orStr);
            if(extraWhereClause !=null){
                searchClause =  ' ('+searchClause + ') '+extraWhereClause;
            }
            system.debug('****** searchClause Gard *****' + searchClause);
            if(finalSearchFieldsGardContacts .equalsIgnoreCase('')){
            finalQuery= 'select id from '+ objSearch +whereStr+ searchClause  +' order by LastModifiedDate limit 20';
            }else{
            finalQuery= 'select id,portal_Image__c,Nick_Name__c,MobilePhone__c,Office_city__c, '+ finalSearchFieldsGardContacts +fromStr+ objSearch +whereStr+ searchClause  +' order by LastModifiedDate limit 20';
            }
            system.debug('******finalQuery Gard *****' + finalQuery);
        }
        else if(objSearch == 'PEME_Invoice__c')
        {
            system.debug('****** searchClause Gard1 *****' + searchClause);
            for(Schema.SObjectField f : FieldMap.values()){ 
                Schema.DescribeFieldResult DescField = f.getDescribe();
                selectFiled =  selectFiled  + DescField.getName()+ ', ';
                if(String.valueOf(DescField.getType()).equalsIgnoreCase(stringStr)){
                    searchClause =  searchClause + ' ' + DescField.getName() + ' LIKE ' + textToBeSearchedfull + orStr;
                }
            }
            searchClause = searchClause.removeEnd(orStr);
            searchClause = searchClause + ' OR Client__r.name like '+ textToBeSearchedfull ; // extra for client name
            if(extraWhereClause !=null){
                searchClause =  ' ('+searchClause + ') '+extraWhereClause;
            }
            system.debug('****** searchClause Gard *****' + searchClause);
            
            finalQuery= 'Select id,  guid__c, Name, Bank_address__c, Brief_Description__c, Client__r.name, Clinic__r.name, Clinic_Invoice_Number__c, Comments_del__c, Crew_Examined__c, '+
               ' Date_of_Final_Verification__c, PEME_reference_No__c, Invoice_Date__c, Period_Covered_From__c,ObjectList__c, Period_Covered_To__c, Vessels__r.Name '+fromStr+ objSearch +whereStr+ searchClause  +' order by LastModifiedDate limit 20';   // added for 4687 ObjectList__c
            
            system.debug('******finalQuery Gard *****' + finalQuery);
        }
        else if(objSearch == 'Case' || objSearch == 'Claim__c')
        {
            if(IsPeopleLUser)
            {
                for(Schema.SObjectField f : FieldMap.values()){
                    Schema.DescribeFieldResult DescField = f.getDescribe();
                    selectFiled =  selectFiled  + DescField.getName()+ ', ';
                    if((String.valueOf(DescField.getType()).equalsIgnoreCase(stringStr))  && (String.valueOf(DescField.getName()) != 'Claim_Description_Restricted__c')){ // Excluding 'Claim_Description_Restricted__c' for people user
                        searchClause =  searchClause + ' ' + DescField.getName() + ' LIKE ' + textToBeSearchedfull + orStr;
                    }
                }
                system.debug('******searchClause *****' + searchClause);
                
                system.debug('******peopleclaimuser *****' + IsPeopleLUser);
            }else
            {
                for(Schema.SObjectField f : FieldMap.values()){ // No need to exclude Description field because 'Description' field not retrived by getDescribe() method. 
                    Schema.DescribeFieldResult DescField = f.getDescribe();
                    selectFiled =  selectFiled  + DescField.getName()+ ', ';
                    if(String.valueOf(DescField.getType()).equalsIgnoreCase(stringStr)){
                        searchClause =  searchClause + ' ' + DescField.getName() + ' LIKE ' + textToBeSearchedfull + orStr;
                    }
                } 
                system.debug('******peopleclaimuser *****' + IsPeopleLUser);
            }
            // selectFiled = 'Claim_Type__c, Event_Date__c, ' + selectFiled;
            selectFiled = selectFiled.substring(0,selectFiled.lastIndexOf(',')); //to remove the last comma (,)
            system.debug('select Fields--'+selectFiled);
            searchClause = searchClause.removeEnd(orStr);
            if(extraWhereClause !=null){
                searchClause =  ' ('+searchClause + ') '+extraWhereClause;
            }
            if(finalSearchFields.equalsIgnoreCase('')){
            finalQuery= 'select '+selectFiled+' from '+ objSearch +whereStr+ searchClause  +' order by LastModifiedDate limit 20';
            }else{
            finalQuery= 'select '+selectFiled+' from '+ finalSearchFields +fromStr+ objSearch +whereStr+ searchClause +' order by LastModifiedDate limit 20';
            }
        }
        else
        {
            for(Schema.SObjectField f : FieldMap.values()){
                Schema.DescribeFieldResult DescField = f.getDescribe();
                selectFiled =  selectFiled  + DescField.getName()+ ', ';
                System.debug('search DescField.getName contact is-->' + DescField.getName() );
                if(String.valueOf(DescField.getType()).equalsIgnoreCase(stringStr)){
                    searchClause =  searchClause + ' ' + DescField.getName() + ' LIKE ' + textToBeSearchedfull + orStr;
                }
            }
            System.debug('search objSearcht is-->' + objSearch );
            System.debug('search clause contact is-->' + searchClause );
            searchClause = searchClause.removeEnd(orStr);
            if(extraWhereClause !=null){
                searchClause =  ' ('+searchClause + ') '+extraWhereClause;
            }
            system.debug('****** searchClause else *****' + searchClause);
            //finalQuery= 'select id from '+ objSearch +whereStr+ searchClause  +' order by LastModifiedDate limit 20';
            finalQuery= 'select id,Product_Name__c,Agreement__r.Client__r.Name,Agreement__r.Business_Area__c,Expiration_Date__c,Agreement__r.Policy_Year__c,Object__r.Name,Object__r.Object_Type__c,Claims_Lead__c,On_risk_indicator__c,Gard_Share__c from '+ objSearch +whereStr+ searchClause  +' AND (NOT(Object__r.name LIKE : hidePemeObj)) order by LastModifiedDate limit 20';
            
            //System.debug('search clause is-->' + searchClause );
        }
        
        system.debug('******selectFiled *****' + selectFiled);
        // selectFiled = selectFiled.substring(0,selectFiled.length()-1);
        selectFiled = selectFiled.substring(0,selectFiled.lastIndexOf(',')); //to remove the last comma (,)
        system.debug('******selectFiled *****' + selectFiled);
        //String finalQuery= 'select '+ selectFiled +' from '+ objSearch +whereStr+ searchClause;
        // String finalQuery= 'select id from '+ objSearch +' WHERE'+ searchClause;
        //List<Contact> contacts = Database.query(finalQueary);
        system.debug('****final query***'+finalQuery);
        return finalQuery;
    }
}