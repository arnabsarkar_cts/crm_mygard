@isTest//(seeAllData=true)
Class TestMyObjectDetailsCtrl
{
    
    public static string enUsrStr =  'en_US';
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    public static String typeBrStr = 'Broker';
    public static String typeClStr = 'Client';
    public static testmethod void cover()
    { 
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        MyGardDocumentAvailability__c docAvailabilityOD = new MyGardDocumentAvailability__c(name='Object Details Document Tab',IsAvailable__c=true);
        insert docAvailabilityOD;
        string partnerLicenseId = [SELECT Id FROM profile WHERE Name= 'Partner Community Login User Custom' limit 1].id;
        string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        Account brokerAcc,brokerAcc2, clientAcc;
        User brokerUser, clientUser, GardContactUser_1;
        Contact clientContact, brokerContact ,brokerContact2;
        //Claim_Type__c brokerClaimType, clientClaimType;
        List<SelectOption> options = new List<SelectOption>();
        PageReference var4 = new PageReference('');
        Gard_Contacts__c gardCon;
        List<Account> accList = new List<Account>();
        List<Contact> contactList = new List<Contact>();
        List<User> userList = new List<User>();
        List<Case> caseList = new List<Case>();
        Extranet_Favourite__c exfav;
        AccountToContactMap__c map_broker, map_client;
        Account_Contact_Mapping__c Acmap_1,Acmap_2;
        Group__c test_group1 ,test_group2,test_group3;

        
        // ************************************************************************************************************
        //dummyAcc, dummyContact and salesforceLicUser is for Main_Claims_Handler__c field of broker and client user, 
        //and that is also the reason these things are not added to list.
        //************************************************************************************************************
        
        //** Here, the salesforcelicenseuser is used as a gard contact, and its email ID is feched to the account's underwriter fields.
        
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonRecord();
        gtdInstance.customsettings_rec();
        
        gardCon= new Gard_Contacts__c(Phone__c='123456',
                                      MobilePhone__c='123456',
                                      Office_city__c='Arendal');
        insert gardCon;
        /*
Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
insert Markt ;



User salesforceLicUser = new User(
Alias = 'standt', 
profileId = salesforceLicenseId ,
Email='standarduser@testorg.com',
EmailEncodingKey='UTF-8',
CommunityNickname = 'test13',
LastName='Testing',
LanguageLocaleKey= enUsrStr ,
LocaleSidKey= enUsrStr ,  
TimeZoneSidKey='America/Los_Angeles',
ContactId__c=gardCon.id,
UserName='test008@testorg.com' 
);
insert salesforceLicUser ;

brokerAcc= New Account( Name = 'Testt',
Site = 'www.test.se',
Type = 'Customer',
BillingStreet = 'Gatan 1',
BillingCity = 'Stockholm',
BillingCountry = 'SWE',
recordTypeId=System.Label.Broker_Contact_Record_Type,
Main_Claims_Handler__c = salesforceLicUser.id,
Market_Area__c = Markt.id,
UW_Assistant_1__c=salesforceLicUser.id,
U_W_Assistant_2__c=salesforceLicUser.id,
X2nd_UWR__c=salesforceLicUser.id,
X3rd_UWR__c=salesforceLicUser.id,
Underwriter_4__c=salesforceLicUser.id,
Underwriter_main_contact__c=salesforceLicUser.id,
Area_Manager__c = salesforceLicUser.id,
Description = 'Broker Description',
Company_Role__c='Broker',
Sub_Roles__c = 'Broker - Broker'
);

accList.add(brokerAcc);
brokerAcc2 = New Account( Name = 'Testtt',
Site = 'www.testt.se',
Type = 'Customer',
BillingStreet = 'Gatan 1',
BillingCity = 'Stockholm',
BillingCountry = 'SWE',
recordTypeId=System.Label.Broker_Contact_Record_Type,
Main_Claims_Handler__c = salesforceLicUser.id,
Market_Area__c = Markt.id,
UW_Assistant_1__c=salesforceLicUser.id,
U_W_Assistant_2__c=salesforceLicUser.id,
X2nd_UWR__c=salesforceLicUser.id,
X3rd_UWR__c=salesforceLicUser.id,
Underwriter_4__c=salesforceLicUser.id,
Underwriter_main_contact__c=salesforceLicUser.id,
Area_Manager__c = salesforceLicUser.id,
Description = 'Broker Description',
Company_Role__c='Broker',
Sub_Roles__c ='Broker - Broker'
);
accList.add(brokerAcc2);
clientAcc= New Account( Name = 'Testt',
Site = '_www.test.se',
Type = 'Customer',
BillingStreet = 'Gatan 1',
BillingCity = 'Stockholm',
BillingCountry = 'SWE',
recordTypeId=System.Label.Client_Contact_Record_Type,
UW_Assistant_1__c=salesforceLicUser.id,
U_W_Assistant_2__c=salesforceLicUser.id,
X2nd_UWR__c=salesforceLicUser.id,
X3rd_UWR__c=salesforceLicUser.id,
Underwriter_4__c=salesforceLicUser.id,
Underwriter_main_contact__c=salesforceLicUser.id,
Main_Claims_Handler__c = salesforceLicUser.id, 
Market_Area__c = Markt.id,
Area_Manager__c = salesforceLicUser.id
);

accList.add(clientAcc);
Insert accList;
brokerContact = new Contact( Accountid = brokerAcc.id,
FirstName='Test', 
lastName = 'Test',
OtherPhone = '12345678',
Email = 'standard_user@salesforce.org',
MobilePhone = '990088776');
contactList.add(brokerContact) ;
brokerContact2 = new Contact( Accountid = brokerAcc2.id,
FirstName='Test', 
lastName = 'Test',
OtherPhone = '12345678',
Email = 'standard_user@salesforce.org',
MobilePhone = '990089776');
contactList.add(brokerContact2) ;

clientContact = new Contact( Accountid = clientAcc.id, 
FirstName='Test1', 
lastName = 'Test1',
OtherPhone = '12345678',
Email = 'standard_user@salesforce.org',
MobilePhone = '990088776');
contactList.add(clientContact) ; 
insert contactList;
brokerUser = new User(Alias = 'standtt'
,CommunityNickname = 'test134'
,Email='testUser134@testorg.com', 
profileid = partnerLicenseId , 
EmailEncodingKey='UTF-8',
LastName='testttt',
LanguageLocaleKey= enUsrStr ,
LocaleSidKey= enUsrStr ,
TimeZoneSidKey='America/Los_Angeles',
UserName='testUser134@testorg.com.mygard',
//ContactId__c=gardCon.id,
ContactId = brokerContact.id);
userList.add(brokerUser);


clientUser = new User(Alias = 'statt',
CommunityNickname = 'test1234',
Email='testUser1234@testorg.com', 
profileid = partnerLicenseId ,
EmailEncodingKey='UTF-8',
LastName='testttt',
LanguageLocaleKey= enUsrStr ,
LocaleSidKey= enUsrStr ,
TimeZoneSidKey='America/Los_Angeles',
UserName='testUser1234@testorg.com.mygard',
//ContactId__c=gardCon.id,
ContactId = clientContact.id);
userList.add(clientUser) ;
insert userList;

//CustomSetting added by Abhirup
AdminUsers__c setting = new AdminUsers__c();
setting.Name = 'Number of users';
setting.Value__c = 3;
insert setting;

Acmap_1 = new Account_Contact_Mapping__c(Account__c = brokerAcc.id,
Active__c = true,
Administrator__c = 'Admin',
Contact__c = brokerContact.id,
IsPeopleClaimUser__c = true,
Marine_access__c = true,
PI_Access__c = true
//Show_Claims__c = false,
//Show_Portfolio__c = false 
);

Acmap_2 = new Account_Contact_Mapping__c(Account__c =  brokerAcc2.id,
Active__c = true,
Administrator__c = 'Normal',
Contact__c = brokerContact2.id,
IsPeopleClaimUser__c = false,
Marine_access__c = true,
PI_Access__c = true
//Show_Claims__c = false,
//Show_Portfolio__c = false 
);
insert Acmap_1;
insert Acmap_2;


map_broker = new AccountToContactMap__c(AccountId__c=brokerAcc.id,Name=brokerContact.id,RolePreference__c=typeBrStr);
map_client = new AccountToContactMap__c(AccountId__c= clientAcc.id,Name=clientContact.id,RolePreference__c=typeClStr);

Insert map_broker;
Insert map_client; 
*/
        
        Object__c objt= new Object__c(Name = 'Test',
                                      Object_Unique_ID__c='abc',
                                      //guid__c = '8ce8ad89-a6ed-1836-9e17',
                                      Imo_Lloyds_No__c='123456789');        
        insert objt;
        system.assertEquals(objt.Name , 'Test');
        /*
Contract contract = New Contract(Accountid = brokerAcc.id,
Account = brokerAcc,
Status = 'Draft',
Broker__c = brokerAcc.id,
Client__c = clientAcc.id,
Accounting_Contacts__c = brokerUser.id,
Contract_Reviewer__c = brokerUser.id,
Contracting_Party__c = brokerAcc.id,
Policy_Year__c='2015',
Business_Area__c='P&I'
);        
Insert contract;


Asset asset = new Asset( Name= 'Asset 1',
accountid =brokerAcc.id,
contactid = brokerContact.Id,  
Agreement__c = contract.id, 
CurrencyIsoCode='USD',
Expiration_Date__c=Date.today(),
Inception_date__c=Date.today() + 5,
Object__c= objt.id,
Risk_ID__c='abcd'           
); 
insert asset;

Case brokerCase = new Case(Accountid = brokerAcc.id,
ContactId = brokerContact.id,
Origin = 'Community',
//Sub_Claim_Handler__c = brokerContact.id,
//Registered_By__c = brokerContact.id, 
Object__c = objt.id,
//Version_Key__c = 'kolka2342',
Claim_Incurred_USD__c= 500,
Claim_Reference_Number__c='kol23232',
Reserve__c = 350,
Paid__c =50000,
Total__c= 750,
//Voyage_To__c = 'Voyage_To',
Member_reference__c = 'ash1232',
//Voyage_From__c = 'Voyage_From',
Claim_Type__c = 'Cargo',
Risk_Coverage__c = asset.id ,
Contract_for_Review__c  = contract.id   ,
Status = 'Open'                                             
);
caseList.add(brokerCase);

Case clientcase = new Case(Accountid = clientAcc.id,
ContactId = clientContact.id, 
//Registered_By__c = clientContact.id,
Origin = 'Community',
Object__c = objt.id, 
Status='New', 
Claim_Incurred_USD__c = 50000,
//Claims_handler__c = clientContact.id,
Event_details__c = 'Testing ReportedClaimsCtrl',
Paid__c = 50000, 
Event_Type__c = 'Incident', 
//Version_Key__c = 'hgdsjhdAs232',
Claim_Type__c = 'Cargo',
Event_Date__c =  Date.today(),
Claim_Reference_Number__c = 'ClaiMRef1234', 
//Claims_Currency__c = 'USD',
//Voyage_To__c = 'Voyage_To',
//Voyage_From__c = 'Voyage_From',
Risk_Coverage__c = asset.id
);
caseList.add(clientcase);
insert caseList;*/
        
        exfav= new Extranet_Favourite__c(Item_Id__c='MyObjects', Item_Type__c='Saved Search', Item_Name__c='mycovers 1',Favourite_By__c=GardTestData.brokerUser.Id,
                                         Favourite_For_Broker_Client__c = GardTestData.brokerAcc.Id);
        insert exfav;
        objt.guid__c = '8ce8ac89-a6fd-1836-9e07';
        update objt;
        
        CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c ='Test123@gmail.com');
        insert cvar;
        
        test_group1 = new Group__c(name = 'OPEN', Group_Id__c = '4');
        test_group2 = new Group__c(name = 'CLAIM',Group_Id__c = '5');
        test_group3 = new Group__c(name = 'RESTRICTED',Group_Id__c = '6');
        
        insert test_group1;
        insert test_group2;
        insert test_group3;
        system.assertEquals(test_group1.name , 'OPEN');
        /*


Document_Metadata__c brokerDoc= new Document_Metadata__c(Client__c=clientAccId,
Document_Metadata_External_ID__c='99584',
Document_Type__c='Type 2',
Last_Updated__c=date.valueOf('2015-03-12'),
Document_Source__c = 'UWR',
isPaymentDocument__c = false,
isPolicyDocument__c = true,
isTradingCertificate__c = true
);
insert brokerDoc;
DocumentMetadata_Group__c docgroup1 = new DocumentMetadata_Group__c(Document_Metadata__c = brokerDoc.id ,Group__c = test_group1.id);
insert docgroup1 ;
DocumentMetadata_Junction_object__c junc_object_broker = new DocumentMetadata_Junction_object__c(Agreement__c=contract.id,
Claim__c=brokercase.id,
Cover__c=Asset.id,
Document_Metadata__c=brokerDoc.id,
External_ID__c='2asas123',
Name='test record',
Object__c=objt.id

// Type__c='type1'
);
insert  junc_object_broker ;

BlueCardTeam__c testbluecardteam = new BlueCardTeam__c(
Name = 'BlueCardTeam',
Value__c = 'nils.petter.nilsen@testgard.no'
);
insert testbluecardteam;

*/
        System.runAs(GardTestData.brokerUser)
        {
            test.startTest();
            ApexPages.StandardController scontroller = new ApexPages.StandardController(GardTestData.BrokerAcc);
            Pagereference pge = page.MyObjectDetails;
            pge.getparameters().put('objid',objt.guid__c);
            test.setcurrentpage(pge);
            MyObjectDetailsCtrl MyObject=new MyObjectDetailsCtrl(scontroller);
            MyObjectDetailsCtrl MyObjects=new MyObjectDetailsCtrl();
            // MyObjects.acmObj = [SELECT Id,IsPeopleClaimUser__c FROM Account_Contact_Mapping__c where Id =: GardTestData.Accmap_1st.Id];
            MyObjects.caseId = GardTestData.brokerCase.Id;
            MyObjects.acmObj = GardTestData.Accmap_1st;
            MyObjects.showCaseDetails();
            MyObject.underwrId='abc#xyz';
            //MyObject.caseId='500L0000005GMK5IAO';
            //MyObject.showCaseDetails();
            //MyObject.underwrId = 'a1hL0000000kT2HIAU#theHome:pageFrame:feedbackPopUpSection1:j_id186:0:j_id193';
            MyObject.underwrId = gardCon.Id + '#' + 'theHome:pageFrame:feedbackPopUpSection1:j_id186:0:j_id193';
            MyObject.underwriterPopup();
            MyObject.setObjectIds.add(objt.id);
            MyObject.total_Contract_Id_Set.add(GardTestData.brokercontract_1st.id);
            List<Case> caseli = MyObject.getClaims();
            List<Asset> assetli = MyObject.getCovers();
            MyObject.viewAllCovers();
            MyObject.viewAllClaims();
            MyObject.viewAllDocuments();
            MyObject.exportToExcel();
         //   MyObject.obj = objt;
            //Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
            
            MyObject.caseTestId = GardTestData.clientCase.id;
            MyObject.obj = GardTestData.test_object_1st;
            MyObject.sendMailforEditObject();
            MyObject.strEditObjectFreeText = 'abc\ndef\nghi\njkl\nmno';
            // MyObject.sendMailforEditObject();
            MyObject.closeconfirmPopUp();
            MyObject.displayEditPopUp();
            MyObject.sendAckMail();
            MyObject.sendNotiMail();
            MyObject.createFavourites();
            MyObject.deleteFavourites();
            MyObject.fetchFavourites();
            Integer count = MyObjectDetailsCtrl.countObjectDocs(GardTestData.test_object_1st.Id,GardTestData.test_object_1st.Imo_Lloyds_No__c, null,null,null, GardTestData.test_object_1st.Name);
            test.stopTest();
        }
    }
    public static testmethod void cover2()
    { 
        
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonRecord();
        gtdInstance.customsettings_rec();
        
       Gard_Contacts__c gardCon= new Gard_Contacts__c(Phone__c='123456',
                                      MobilePhone__c='123456',
                                      Office_city__c='Arendal');
        insert gardCon;
        
        Object__c objt= new Object__c(Name = 'Test',
                                      Object_Unique_ID__c='abc',
                                      //guid__c = '8ce8ad89-a6ed-1836-9e17',
                                      Imo_Lloyds_No__c='123456789');        
        insert objt;
        system.assertEquals(objt.Name , 'Test');
  
        objt.guid__c = '8ce8ad66-a6ec-1834-9e21';
        update objt;

        System.runAs(GardTestData.clientUser)
        {
            test.startTest();
            ApexPages.StandardController scontroller = new ApexPages.StandardController(GardTestData.BrokerAcc);
            Pagereference pge = page.MyObjectDetails;
            pge.getparameters().put('objid',objt.guid__c);
            test.setcurrentpage(pge);
            MyObjectDetailsCtrl MyObject=new MyObjectDetailsCtrl(scontroller);
            MyObjectDetailsCtrl MyObjects=new MyObjectDetailsCtrl();
            MyObjects.caseId = GardTestData.brokerCase.Id;
            MyObjects.acmObj = GardTestData.Accmap_1st;
            MyObjects.showCaseDetails();
          
            MyObject.underwrId = gardCon.Id + '#' + 'theHome:pageFrame:feedbackPopUpSection1:j_id186:0:j_id193';
            MyObject.underwriterPopup();
           //MyObject.setObjectIds.add(objt.id);
            
            MyObject.isAuthorized=true;
            MyObject.obj =GardTestData.test_object_1st;
            MyObject.docNo=MyObjectDetailsCtrl.countObjectDocs(GardTestData.test_object_1st.Id,GardTestData.test_object_1st.Imo_Lloyds_No__c, null,null,null, GardTestData.test_object_1st.Name);
            MyObject.setObjectIds.add(objt.id);
            MyObject.userType='Client';
            
            MyObject.total_Contract_Id_Set.add(GardTestData.brokercontract_1st.id);
            List<Case> caseli = MyObject.getClaims();
            List<Asset> assetli = MyObject.getCovers();
            MyObject.viewAllCovers();
            MyObject.viewAllClaims();
            MyObject.viewAllDocuments();
            MyObject.exportToExcel();
            
            MyObject.caseTestId = GardTestData.clientCase.id;
            //MyObject.obj = GardTestData.test_object_1st;
            MyObject.sendMailforEditObject();
            MyObject.strEditObjectFreeText = 'abc\ndef\nghi\njkl\nmno';
            //MyObject.sendMailforEditObject();
            MyObject.closeconfirmPopUp();
            MyObject.displayEditPopUp();
            MyObject.sendAckMail();
            MyObject.sendNotiMail();
            MyObject.createFavourites();
            MyObject.deleteFavourites();
            MyObject.fetchFavourites();
            //2Integer count = MyObjectDetailsCtrl.countObjectDocs(GardTestData.test_object_1st.Id,GardTestData.test_object_1st.Imo_Lloyds_No__c, null,null,null, GardTestData.test_object_1st.Name);
            test.stopTest();
    }
    
  } 
}