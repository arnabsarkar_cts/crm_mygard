public with sharing class MDMSyncMergeAccountRetryExtension {

	private final MDM_Account_Merge__c mergeAccount;
	
	public String redirectUrl {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}
	
	public Boolean IsAdmin { 
		get { 
			MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
			return mdmSettings.Retry_Buttons_Enabled__c;
		} 
	}
	
	public MDMSyncMergeAccountRetryExtension (ApexPages.StandardController stdController) {
    	this.mergeAccount = (MDM_Account_Merge__c)stdController.getRecord();
		redirectUrl = stdController.view().getUrl();
		shouldRedirect = false;
    }
	
	public ApexPages.PageReference RetryMergeAccountSync() {
		shouldRedirect = true;
		mergeAccount.Synchronisation_Status__c = 'Sync In Progress';
		update mergeAccount;
		return null;
	}

}