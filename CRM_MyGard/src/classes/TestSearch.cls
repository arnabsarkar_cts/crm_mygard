@isTest(seeAllData=false)
Public class TestSearch
{    
/*        Public static List<Account> AccountList=new List<Account>();
        Public static List<Contact> ContactList=new List<Contact>();
        Public static List<Contract> ContractList=new List<Contract>();
        Public static List<User> UserList=new List<User>();
        Public static List<Asset> AssetList=new List<Asset>();
        Public static List<Case> caseList =new List<Case>();
        Public static List<Market_Area__c> Market_AreaList=new List<Market_Area__c>();
        Public static List<Object__c> ObjectList=new List<Object__c>();
        Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name= 'Partner Community Login User Custom' limit 1].id;
        Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
        public static Account clientAcc;
        public static Account brokerAcc;
        public static Contact brokerContact;
        public static Contact clientContact;
        public static User brokerUser;
        public static User clientUser;
        public static Contract clientContract;
        public static Contract brokerContract;
        public static Object__c obj;
        public static Asset brokerAsset;
        public static Asset clientAsset;
        public static Case brokerCase;
        public static Case clientCase;
        public static Market_Area__c Markt;
        public static User salesforceLicUser;
        public static Gard_Contacts__c grdobj;
        public static Account_Contact_Mapping__c Accmp1, Accmp2;
        public static list<Account_Contact_Mapping__c> Accmaplst = new list<Account_Contact_Mapping__c>();
        public static AccountToContactMap__c map_broker, map_client;    
           
     public Static void data()
    
    {
       ////***************adding custom settings for seealldata=false*********************************///  
              
     /*  Search_webservice_endpoint__c wb = new Search_webservice_endpoint__c(Endpoint__c = ' https://soa-test.gard.no/MyGard/Search/SearchService', Name = 'Search_SOA_Endpoint');
       insert wb;
       
       CommonVariables__c cv = new CommonVariables__c(Name = 'DefaultProfilePic' , Value__c = '/mygard/profilephoto/005/T');
       
       insert cv;
       
       Map<String, Topics__c> topicTotal = Topics__c.getAll();
       List<String> topicOptions = new List<String>();  
        
       Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
          Market_AreaList.add(Markt);
          insert Market_AreaList;  
          
          grdobj =new  Gard_Contacts__c(
                         FirstName__c = 'Test',
                         LastName__c = 'Baann'                         
                         );
        insert grdobj ;
          
          salesforceLicUser = new User(
                                            Alias = 'standt', 
                                            profileId = salesforceLicenseId ,
                                            Email='standarduser@testorg.com',
                                            EmailEncodingKey='UTF-8',
                                            CommunityNickname = 'test13',
                                            LastName='Testing',
                                            LanguageLocaleKey='en_US',
                                            LocaleSidKey='en_US',  
                                            TimeZoneSidKey='America/Los_Angeles',
                                            UserName='test008@testorg.commygard',
                                            contactid__c = grdobj.id
                                           );
        insert salesforceLicUser;
          
       //Accounts............
       brokerAcc = new Account(  Name='testre',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    recordTypeId=System.Label.Broker_Contact_Record_Type,
                                    Site = '_www.cts.se',
                                    Type = 'Broker',
                                    Market_Area__c = Markt.id,
                                    Area_Manager__c = salesforceLicUser.id, 
                                    Claim_Adjuster_Marine_lk__c = salesforceLicUser.id,
                                    Claim_handler_Cargo_Dry_2_lk__c= salesforceLicUser.id,
                                    Claim_handler_Cargo_Liquid_2_lk__c= salesforceLicUser.id,
                                    Claim_handler_Crew_2_lk__c= salesforceLicUser.id,
                                    Claim_handler_Defence_2_lk__c= salesforceLicUser.id,
                                    Claim_handler_Energy_2_lk__c= salesforceLicUser.id,
                                    Claim_handler_Marine_2_lk__c= salesforceLicUser.id,
                                    Claims_handler_Builders_Risk_2_lk__c= salesforceLicUser.id,
                                    Claim_handler_Cargo_Dry__c= salesforceLicUser.id,
                                    Claim_handler_Cargo_Liquid__c= salesforceLicUser.id,
                                    Claim_handler_CEP__c= salesforceLicUser.id,
                                    UW_Assistant_1__c= salesforceLicUser.id,
                                    U_W_Assistant_2__c= salesforceLicUser.id,
                                    X2nd_UWR__c= salesforceLicUser.id,
                                    Underwriter_4__c= salesforceLicUser.id,
                                    Accounting_P_I__c= salesforceLicUser.id,
                                    Underwriter_main_contact__c= salesforceLicUser.id,
                                    Claim_handler_Energy__c= salesforceLicUser.id,
                                    Claim_handler_Marine__c= salesforceLicUser.id,
                                    Claims_handler_Builders_Risk__c= salesforceLicUser.id
                                                                                                         
                                 );
       AccountList.add(brokerAcc);
    
      
       clientAcc = new Account( Name = 'Test_1', 
                                        Site = '_www.test_1.se', 
                                        Type = 'Client', 
                                        BillingStreet = 'Gatan 1',
                                        BillingCity = 'Stockholm', 
                                        BillingCountry = 'SWE', 
                                        BillingPostalCode = 'BS1 1AD',
                                        recordTypeId=System.Label.Client_Contact_Record_Type,
                                        Market_Area__c = Markt.id,
                                        Area_Manager__c = salesforceLicUser.id,
                                        Claim_Adjuster_Marine_lk__c = salesforceLicUser.id,
                                        Claim_handler_Cargo_Dry_2_lk__c= salesforceLicUser.id,
                                        Claim_handler_Cargo_Liquid_2_lk__c= salesforceLicUser.id,
                                        Claim_handler_Crew_2_lk__c= salesforceLicUser.id,
                                        Claim_handler_Defence_2_lk__c= salesforceLicUser.id,
                                        Claim_handler_Energy_2_lk__c= salesforceLicUser.id,
                                        Claim_handler_Marine_2_lk__c= salesforceLicUser.id,
                                        Claims_handler_Builders_Risk_2_lk__c= salesforceLicUser.id,
                                        Claim_handler_Cargo_Dry__c= salesforceLicUser.id,
                                        Claim_handler_Cargo_Liquid__c= salesforceLicUser.id,
                                        Claim_handler_CEP__c= salesforceLicUser.id,
                                        UW_Assistant_1__c= salesforceLicUser.id,
                                        U_W_Assistant_2__c= salesforceLicUser.id,
                                        X2nd_UWR__c= salesforceLicUser.id,
                                        Underwriter_4__c= salesforceLicUser.id,
                                        Accounting_P_I__c= salesforceLicUser.id,
                                        Underwriter_main_contact__c= salesforceLicUser.id,
                                        Claim_handler_Energy__c= salesforceLicUser.id,
                                        Claim_handler_Marine__c= salesforceLicUser.id,
                                        Claims_handler_Builders_Risk__c= salesforceLicUser.id
                                       );
                                    
        AccountList.add(clientAcc); 
        insert AccountList; 
        //Contacts.................
        brokerContact = new Contact( 
                                             FirstName='Raan',
                                             LastName='Baan',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             //IsPeopleLineUser__c = true,
                                             MailingStreet = '1 London Road',
                                             AccountId = brokerAcc.Id,
                                              account = brokerAcc,
                                             Email = 'test321@gmail.com'
                                           );
        ContactList.add(brokerContact) ;

        clientContact= new Contact( FirstName='tsat_1',
                                              LastName='chak',
                                              MailingCity = 'London',
                                              MailingCountry = 'United Kingdom',
                                              MailingPostalCode = 'SE1 1AE',
                                              MailingState = 'London',
                                              //IsPeopleLineUser__c = true,
                                              MailingStreet = '4 London Road',
                                              account = clientAcc,
                                              AccountId = clientAcc.Id,
                                              Email = 'test321@gmail.com'
                                          );
        ContactList.add(clientContact);
        insert ContactList;
        
        //Users..................
        brokerUser = new User(
                                    Alias = 'standt', 
                                    profileId = partnerLicenseId,
                                    Email='mdjawedm@gmail.com',
                                    EmailEncodingKey='UTF-8',
                                    LastName='estTing',
                                    LanguageLocaleKey='en_US',
                                    CommunityNickname = 'brokerUser',
                                    LocaleSidKey='en_US',  
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testbrokerUser@testorg.commygard',
                                    contact = BrokerContact,
                                    ContactId = BrokerContact.Id
                                    //contactId__c = grdobj.id
                                   );
        UserList.add(brokerUser);
                                   
        clientUser = new User(Alias = 'alias_1',
                                    CommunityNickname = 'clientUser',
                                    Email='test_11@testorg.com', 
                                    profileid = partnerLicenseId, 
                                    EmailEncodingKey='UTF-8',
                                    LastName='tetst_006',
                                    LanguageLocaleKey='en_US',
                                    LocaleSidKey='en_US',
                                    contact = clientContact,
                                    TimeZoneSidKey='America/Los_Angeles',
                                    UserName='testclientUser@testorg.commygard',
                                    ContactId = clientContact.id
                                    //contactId__c = grdobj.id
                                   );
                
         UserList.add(clientUser) ;
         insert UserList;
         
         map_broker = new AccountToContactMap__c(AccountId__c=brokerAcc.id,Name=brokerContact.id);
         map_client = new AccountToContactMap__c(AccountId__c= clientAcc.id,Name=clientContact.id);
         
         Insert map_broker;
         Insert map_client;
         
          Accmp1 = new Account_Contact_Mapping__c(Account__c = brokerAcc.id,
                                                   Active__c = true,
                                                   Administrator__c = 'Admin',
                                                   Contact__c = brokerContact.id,
                                                   IsPeopleClaimUser__c = false,
                                                   Marine_access__c = true,
                                                   PI_Access__c = true
                                                   //Show_Claims__c = false,
                                                   //Show_Portfolio__c = false 
                                                   );
          
         Accmp2 = new Account_Contact_Mapping__c(Account__c =  clientAcc.id,
                                                   Active__c = true,
                                                   Administrator__c = 'Admin',
                                                   Contact__c = clientContact.id,
                                                   IsPeopleClaimUser__c = false,
                                                   Marine_access__c = true,
                                                   PI_Access__c = true
                                                   //Show_Claims__c = false,
                                                   //Show_Portfolio__c = false 
                                                   );
        Accmaplst.add(Accmp1);
        Accmaplst.add(Accmp2);
        Insert Accmaplst;
        brokerContract = New Contract(Accountid = brokerAcc.id, 
                                        Account = brokerAcc, 
                                        Status = 'Draft',
                                        CurrencyIsoCode = 'SEK', 
                                        StartDate = Date.today(),
                                        ContractTerm = 2,
                                        Agreement_Type__c = 'P&I', 
                                        Broker__c = brokerAcc.id,
                                        Client__c = clientAcc.id,
                                        Broker_Name__c = brokerAcc.id,
                                        Expiration_Date__c = date.valueof('2015-01-01'),
                                        Inception_date__c = date.valueof('2012-01-01'),
                                        Agreement_Type_Code__c = 'Agreement123',
                                        Agreement_ID__c = 'Agreement_ID_879',
                                        Business_Area__c='P&I',
                                        Accounting_Contacts__c = brokerUser.id,
                                        Contract_Reviewer__c = brokerUser.id,
                                        Contracting_Party__c = brokerAcc.id,
                                        Policy_Year__c='2015'                                            
                                        );
         
         ContractList.add(brokerContract);
         
         clientContract = New Contract(Accountid = clientAcc.id, 
                                        Account = clientAcc, 
                                        Status = 'Draft',
                                        CurrencyIsoCode = 'SEK', 
                                        StartDate = Date.today(),
                                        ContractTerm = 2,
                                        Broker__c = brokerAcc.id,
                                        Client__c = clientAcc.id,
                                        Expiration_Date__c = date.valueof('2015-01-01'),
                                        Agreement_Type__c = 'P&I',
                                        Agreement_Type_Code__c = 'Agreement123',
                                        Inception_date__c = date.valueof('2012-01-01'),
                                        Business_Area__c='P&I',
                                        Policy_Year__c='2014'                                        
                                        );
         
         ContractList.add(clientContract);
         insert ContractList ;  
         
         obj = New Object__c( Dead_Weight__c= 20,Object_Unique_ID__c = '1234lkolko');
         ObjectList.add(obj);
         insert ObjectList ;   
         
         brokerAsset = new Asset(Name= 'Asset 1',
                                 accountid =brokerAcc.id,
                                 contactid = BrokerContact.Id,  
                                 Agreement__c = brokerContract.id, 
                                 Cover_Group__c = 'P&I',
                                 CurrencyIsoCode='USD',
                                 product_name__c = 'P&I',
                                 Object__c = obj.id,
                                 Underwriter__c = brokerUser.id,
                                 Risk_ID__c = '123456ris',
                                 Expiration_Date__c = date.valueof('2015-01-01'),
                                 Inception_date__c = date.valueof('2012-01-01')
                                 ); 
         AssetList.add(brokerAsset);
         
         clientAsset = new Asset(Name= 'Asset 1',
                                 accountid =clientAcc.id,
                                 contactid = clientContact.Id,  
                                 Agreement__c = brokerContract.id, 
                                 Cover_Group__c = 'P&I',
                                 CurrencyIsoCode='USD',
                                 product_name__c = 'War',
                                 Object__c = obj.id,
                                 Underwriter__c = clientUser.id,
                                 Expiration_Date__c = date.valueof('2015-01-01'),
                                 Inception_date__c = date.valueof('2012-01-01'),
                                 Risk_ID__c = '123456ri'
                                 ); 
         AssetList.add(clientAsset);   
         insert AssetList;
         
         brokerCase = new Case(Accountid = brokerAcc.id,
                            ContactId = brokerContact.id,
                            Origin = 'Community',
                            //Sub_Claim_Handler__c = brokerContact.id,
                            //Registered_By__c = brokerContact.id, 
                            Object__c = obj.id,
                            //Version_Key__c = 'kolka2342',
                            Claim_Incurred_USD__c= 500,
                            Claim_Reference_Number__c='kol23232',
                            Reserve__c = 350,
                            Paid__c =50000,
                            Total__c= 750,
                            //Voyage_To__c = 'Voyage_To',
                            Member_reference__c = 'ash1232',
                            //Voyage_From__c = 'Voyage_From',
                            Claim_Type__c = 'Cargo',
                            Risk_Coverage__c = brokerAsset.id,
                            Status = 'open',
                            Contract_for_Review__c = brokerContract.id,
                            Claims_handler__c = brokerUser.id,
                            Type = 'Add new user',
                            Description = 'dfsnfjsdbskjbdkf ksdfbsdkfjsd fks dfs kdf shd fsh dfks dhf sdkf'                                                  
                            );
        caseList.add(brokerCase);
                
        clientCase = new Case(Accountid = clientAcc.id,
                             ContactId = clientContact.id, 
                             //Registered_By__c = clientContact.id,
                             Origin = 'Community',
                             Claims_handler__c = brokerUser.id,
                             Object__c = obj.id, 
                             Claim_Incurred_USD__c = 50000,
                             Event_details__c = 'Testing ReportedClaimsCtrl',
                             Paid__c = 50000, 
                             Event_Type__c = 'Incident', 
                             //Version_Key__c = 'hgdsjhdAs232',
                             Claim_Type__c = 'Cargo',
                             Event_Date__c = Date.valueof('2014-12-12'),
                             Claim_Reference_Number__c = 'ClaiMRef1234', 
                             //Claims_Currency__c = 'USD',
                             //Voyage_To__c = 'Voyage_To',
                             //Voyage_From__c = 'Voyage_From',
                             Risk_Coverage__c = clientAsset.id,
                             Status = 'Closed',
                             Contract_for_Review__c = clientContract.id,
                             Type = 'Add new user',
                             Description = 'dfsnfjsdbskjbdkf ksdfbsdkfjsd fks dfs kdf shd fsh dfks dhf sdkf' 
                              );
        caseList.add(clientCase);
        insert caseList;
         
        }
        
        Public static testmethod void searchClass()
    {
        TestSearch.data();
        SearchWSCall_1 searchWSCall_demo =new SearchWSCall_1();  
        //ApexPages.CurrentPage().getParameters().put('id',searchWSCall_demo.searchtext);
        
         System.runAs(brokerUser)
        {
            system.debug('*******************contactId__c'+salesforceLicUser.contactId__c);
            system.debug('*******************grdobj '+grdobj);
            system.debug('*******************brokerUser'+brokerUser.contact.account.Underwriter_main_contact__r.contactID__c);
            Pagereference pg = Page.Search_Page;
            pg.getParameters().put('id','test Baanndolr"/');//searchWSCall_demo.searchtext
            Test.setcurrentpage(pg);
            SearchWSCall_1 searchWSCall =new SearchWSCall_1();
            searchWSCall_1.Result resultObj1= new searchWSCall_1.Result('url','header');  
            //searchWSCall_1.grdContIdSet.add(brokercontact.id);
           // searchWSCall_1.setObjId.add(obj.id); 
            //ApexPages.CurrentPage().getParameters().put('id',searchWSCall.searchtext);
            // searchWSCall.setClient.add(clientacc.id); 
            //searchWSCall.genretGrdContId();
            searchWSCall.searchText = 'test Baanndolr"/';
            searchWSCall.Identifier = 'kj2sdfkjsd23901234';
            
            searchWSCall.doSearch();
            searchWSCall.searchText = 'test Baann';
            searchWSCall.mapAsset.put(brokerAsset.id,brokerAsset);
            searchWSCall.mapAsset.put(clientAsset.id,clientAsset);  
            searchWSCall.mapObject.put(obj.id,obj);
            searchWSCall.mapContact.put(brokercontact.id,brokercontact);
            searchWSCall.mapGrdCont.put(grdobj.id,grdobj);
            //searchWSCall.doSearch();            
            List<Asset> objList =  searchWSCall.getObjectLstFSP();
            List<Asset> assetList_1 =  searchWSCall.getResultAssetList();
            List<Case> casList =  searchWSCall.getResultCaseList ();
            searchWSCall.Identifier = brokerContact.id;
            PageReference contactPg = searchWSCall.contactRetriver();
            searchWSCall .Identifier = brokerAsset.id;
            PageReference assetPg =  searchWSCall.assetRetriver();
            searchWSCall.Identifier = grdobj.id;
            PageReference grdPg =  searchWSCall.GrdContRetriver();
            searchWSCall.Identifier = obj.id;
            searchWSCall.viewObject();         
            String str = searchWSCall.getImageUrl('<img alt="User-added image" src="https://c.cs8.content.force.com/servlet/rtaImage?eid=a1hL0000000kSyZ&amp;feoid=00NL0000003OySC&amp;refid=0EML00000008Zjs"></img>');
            //searchWSCall.genretGrdContId();
            searchWSCall.searchText = '91477/11/EDWJIM';            
            searchWSCall.doSearch(); 
            searchWSCall.Identifier = brokerCase.id;           
            PageReference pg2 = searchWSCall.exportToExcelForClaimDet();  
        }
        
        /*System.runAs(clientUser)
        {
            Pagereference pg1 = Page.Search_Page;
            pg1.getParameters().put('id','test Baanndolr"/');//searchWSCall_demo.searchtext
            Test.setcurrentpage(pg1);   
            SearchWSCall_1 searchObj =new SearchWSCall_1(); 
            searchWSCall_1.Result resultObj1= new searchWSCall_1.Result('url','header'); 
           // ApexPages.CurrentPage().getParameters().put('id',searchObj .searchtext);
            searchObj.setClient.add(clientacc.id); 
            searchObj.genretGrdContId();                
            searchObj.searchText = 'test_1 baann';
            searchObj.Identifier = 'kj2sdfkjsd23901234';
            //searchObj.doSearch();
            List<Asset> objList =  searchObj.getObjectLstFSP();
            List<Asset> assetList_1 =  searchObj.getResultAssetList();
            List<Case> casList =  searchObj.getResultCaseList ();
            searchObj.Identifier = clientContact.id;
            PageReference contactPg = searchObj.contactRetriver();
            searchObj.Identifier = clientAsset.id;
            PageReference assetPg =  searchObj.assetRetriver();
            searchObj.Identifier = clientacc.id;            
            //searchObj.showCaseDetails();
            //searchObj.viewObject();
                    
           
        }*/
  //  }

 }