@isTest
public class TestGardContactsController{
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    Public static testmethod void GardContacts1(){
         
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        GardTestData test_rec = new GardTestData();
        test_rec.commonrecord();  
        test_rec.customsettings_rec();
        SelectedClientId_AddBook__c selClient = new SelectedClientId_AddBook__c(
                                              name=GardTestData.BrokerContact.Id,
                                              Value__c = GardTestData.brokerAcc.Id);
                      insert selClient ;
        System.runAs(GardTestData.brokerUser) 
        {     
             Test.startTest();  
             GardContactsController gc = new GardContactsController();
             List<SelectOption> clients = gc.getClients();
             gc.currentContactId = GardTestData.ClientContact.Id;
             gc.descClient();
             gc.ascClient();
             gc.changeClient();
             gc.printData();
             //gc.previous();
             //gc.next();
             //gc.setpageNumber();
             //gc.last();
             //gc.first();
             gc.showPopup();
             system.assertEquals(gc.displayPopup , true );
             gc.closePopup();
             gc.getPickValues();
             gc.populateEditableData();
             gc.textInput = 'test321@gmail.com#sat_1#hak';
             gc.currentContactId = GardTestData.clientContact.Id;
             gc.updateContact();
             gc.textInput = 'test#test#test';
             gc.updateContact();
             gc.closeEditPopup();
             gc.callAlert();
             gc.forceSave();
             gc.addNewContact();
             gc.createCaseForNewUser();
             /*
             gc.strSelected = GardTestData.brokerAcc.Id + ';' + GardTestData.clientAcc.Id;
             gc.fetchSelectedClients();
             gc.gotoPrevTab();
             gc.hasPrevious = false;
             gc.hasNext  = false;
             //gc.pageNumber = 1;
             gc.downloadClientContactsPDF();
             */
            Test.stopTest();
        }
    }
    Public static testmethod void GardContacts2(){
         /*
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; */
        
        GardTestData test_rec = new GardTestData();
        test_rec.commonrecord();  
        test_rec.customsettings_rec();
        SelectedClientId_AddBook__c selClient = new SelectedClientId_AddBook__c(
                                              name=GardTestData.BrokerContact.Id,
                                              Value__c = GardTestData.brokerAcc.Id);
                      insert selClient ;
        System.runAs(GardTestData.brokerUser) 
        {    
            Test.startTest();
            GardContactsController gc = new GardContactsController();
            List<SelectOption> clients = gc.getClients();
            gc.strSelected = GardTestData.brokerAcc.Id + ';' + GardTestData.clientAcc.Id;
            gc.selectedBrokerCon = GardTestData.brokerContact.Id+'';
            GardContactsController.selectedAccountId = GardTestData.brokerAcc.Id+'';
            gc.fetchSelectedClients();
            gc.gotoPrevTab();
            gc.hasPrevious = false;
            gc.hasNext  = false;
            gc.updateCompanyInfo();
            gc.closeAlert();
            //gc.pageNumber = 1;
            gc.downloadClientContactsPDF();
            gc.getBrokerContactLst();
            gc.selectedBrokerCon = GardTestData.brokerContact.Id+'';
            GardContactsController.selectedAccountId = GardTestData.brokerAcc.Id+'';
            gc.populateBrokerName();
            gc.updateBrokerContactRole();
            Test.stopTest();
        }
        
    }
}