/**
*
* cDecisions Ltd 19/9/2013
*
*/
@isTest
private class testTrigContactAIAU {

	static fluidoconnect__Survey__c[] events;
	static Contact[] contacts;
	static fluidoconnect__Invitation__c[] invites;
	
	static void setupEvents()
	{
		events = new fluidoconnect__Survey__c[]{ 
			new fluidoconnect__Survey__c(	Name='Test Event',
											Event_Start_Date__c = Date.Today(),
											fluidoconnect__Status__c='Open'),
			new fluidoconnect__Survey__c(	Name='Test Event',
											Event_Start_Date__c = Date.Today(),
											fluidoconnect__Status__c='Open')	
			};
		insert events;
	}
	
	static void setupContacts()
	{
		Account testAccount = new Account(Name='Test');
		insert testAccount;
		
		//Contacts are added as invitees
		contacts = new Contact[]{
			new Contact(FirstName='Test', LastName='TestOne', AccountId=testAccount.Id),
			new Contact(FirstName='Test', LastName='TestTwo', AccountId=testAccount.Id),
			new Contact(FirstName='Test', LastName='TestThree', AccountId=testAccount.Id),
			new Contact(FirstName='Test', LastName='TestFour', AccountId=testAccount.Id),
			new Contact(FirstName='Test', LastName='TestFive', AccountId=testAccount.Id),
			new Contact(FirstName='Test', LastName='TestSix', AccountId=testAccount.Id)
			};
		insert contacts;
	}
	
	static void addContactToEvent(Contact conToAdd, fluidoconnect__Survey__c eventToAdd)
	{
		system.debug('Adding contact ' + conToAdd.Id + ' to event ' + eventToAdd.Id);

		fluidoconnect__Invitation__c newInvitee = new fluidoconnect__Invitation__c();
		newInvitee.fluidoconnect__Contact__c = conToAdd.Id;
		newInvitee.fluidoconnect__Survey__c = eventToAdd.Id;
		//insert newInvitee;
		invites.add(newInvitee);
		
	}

    static testMethod void unitTest() {
        
        setupContacts();
        setupEvents();
        Set<Id> eventIds = new Set<Id>();
        
        for(fluidoconnect__Survey__c s:events)
        {
        	eventIds.add(s.Id);
        }
        
        //Add all contacts to all events
        invites = new fluidoconnect__Invitation__c[]{};
        for(fluidoconnect__Survey__c s: events)
        {
	        for(contact c: contacts)
	        {
	        	addContactToEvent(c, s);
	        }
        }
        insert invites;
                
        //fluidoconnect__Invitation__c[] invites = [SELECT Id FROM fluidoconnect__Invitation__c WHERE fluidoconnect__Survey__c IN :eventIds];
        invites = [SELECT Id FROM fluidoconnect__Invitation__c WHERE fluidoconnect__Survey__c IN :eventIds];
        
        system.assertEquals(events.size()*contacts.size(), invites.size());
        
        Test.startTest();
        
        events[0].fluidoconnect__Status__c = 'Closed';
        update events;
        
        contacts[0].No_Longer_Employed_by_Company__c = true;
        update contacts;
        
        Test.stopTest();
        
        //Trigger should have removed contact from open event
        invites = [SELECT Id FROM fluidoconnect__Invitation__c WHERE fluidoconnect__Survey__c IN :eventIds AND fluidoconnect__Survey__r.fluidoconnect__Status__c = 'Open'];
        system.assertEquals(contacts.size()-1, invites.size());
        
        //Contact should remain on closed event
        invites = [SELECT Id FROM fluidoconnect__Invitation__c WHERE fluidoconnect__Survey__c IN :eventIds AND fluidoconnect__Survey__r.fluidoconnect__Status__c = 'Closed'];
        system.assertEquals(contacts.size(), invites.size());
        
    }
}