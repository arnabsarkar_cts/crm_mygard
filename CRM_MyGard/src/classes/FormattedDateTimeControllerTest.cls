@isTest
public class FormattedDateTimeControllerTest {    
    @isTest
     public static void FormatDate(){
         
         FormattedDateTimeController objFormatedDate=new FormattedDateTimeController();
         objFormatedDate.valueToFormat = DateTime.newInstance(2019, 9, 2);
         objFormatedDate.definedFormat='dd-MM-yyyy';
         
         System.debug(objFormatedDate.getFormattedDatetime());
         objFormatedDate.valueToFormat = null;
         objFormatedDate.definedFormat='dd-MM-yyyy';
         //System.debug(objFormatedDate.getFormattedDatetime());
         System.assertEquals('', objFormatedDate.getFormattedDatetime());
         
         objFormatedDate.valueToFormat = DateTime.newInstance(2019, 9, 2);
         objFormatedDate.definedFormat= null;
         //System.debug(objFormatedDate.getFormattedDatetime());
         
         System.assertEquals( objFormatedDate.valueToFormat.Format(), objFormatedDate.getFormattedDatetime());
          
         return;
     }

}