@isTest
public class TestMyEventsCtrl {
    
        public static User usr = new User();
        public static User usrStandard = new User();
        public static Contact con = new Contact();
        public static Account acc = new Account();
        public static Account_Contact_Mapping__c acm = new Account_Contact_Mapping__c();
        public static AccountToContactMap__c acmCS = new AccountToContactMap__c();
        public static GardTestData gtd = new GardTestData();
   
    @isTest static void testmethod1(){     
        createData();    
        System.runAs(usr){
        MyEventsCtrl mec = new MyEventsCtrl();
        mec.getSortDirection();
        mec.setSortDirection('ASC');        
        mec.viewData();        
        MyEventsCtrl.getGenerateTimeStamp();
        MyEventsCtrl.getGenerateTime();
        MyEventsCtrl.getUserName();
        mec.exportToExcel();
        mec.print();
        mec.searchEvents();
        mec.getEvents();
        mec.getStatusOptions();
        mec.getEventOptions();
        mec.clearOptions();
        mec.setpageNumber();
        mec.first();
        mec.navigate();
        mec.last();
        mec.navigate();
        mec.previous();
        mec.navigate();
        mec.next();
        mec.navigate(); 
        }            
    }
    
    public static void createData(){
        Test.startTest();
        gtd.commonRecord();
        usr = GardTestData.clientUser;
        usrStandard = GardTestData.salesforceLicUser;
        con = GardTestData.clientContact;
        acc = GardTestData.clientAcc;
        acm = GardTestData.Accmap_2nd;
        acmCS = GardTestData.map_client;
        acm.Marine_access__c = true;
        acm.IsPeopleClaimUser__c = true;
        acm.PI_Access__c = true;
        acm.PI_renewal_access__c = true;
        acm.Administrator__c = 'Admin';
        update acm;
        System.runAs(usrStandard){
            fluidoconnect__Survey__c fcs = new fluidoconnect__Survey__c(Accessible_via_MyGard__c = true,
                                                                   Event_End_Date__c = System.today().addMonths(2),
                                                                   Event_Start_Date__c = System.today(),
                                                                   fluidoconnect__Event_Type__c = 'Registration',                                                                   
                                                                   fluidoconnect__Status__c = 'Open',
                                                                   Invitations_Locked__c = true,
                                                                   fluidoconnect__Survey_Title_English__c = 'testTitleSurvey',
                                                                   Link_to_program__c = 'www.linkToProgram.com',
                                                                   fluidoconnect__Event_Organiser__c = usrStandard.id
                                                                   );
        
            insert fcs;
            fluidoconnect__Invitation__c fci = new fluidoconnect__Invitation__c(fluidoconnect__Survey__c = fcs.id,
                                                                               fluidoconnect__Contact__c = con.id,
                                                                               fluidoconnect__Status__c = 'Attending'
                                                                               );                                                                           
            insert fci;
        Test.stopTest();
        }
        
    }
 
}