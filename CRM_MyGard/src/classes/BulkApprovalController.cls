public with sharing class BulkApprovalController {

    private ApexPages.StandardSetController myController;
    public BulkApprovalController(ApexPages.StandardSetController controller) {
        this.myController = controller;
    }
    
    public Integer getRecordCount() {
        return ((List<Opportunity>)myController.getRecords()).size();
    }
    
    public PageReference OppPage(){
    PageReference oppPage = new pageReference ('/apex/OppProducts');
    oppPage.setRedirect(true);
    return oppPage;
    }    
    
    public void submitAll() {
        Set<String> oIds = new Set<String>();
        Set<ID>  stRecipients = new Set<ID>();
        List<ID> lstRecipients = new List<ID>();
        integer iSize = myController.getResultSize();
        if (iSize > 0) {
            myController.setPageSize(1000);
            List<Opportunity> opps = (List<Opportunity>)myController.getRecords();
            for (Opportunity setopp : opps) 
            {
                oIds.add(setopp.Id);
            }
            
            List<Opportunity> lstOpp = [Select Id, Name, Area_Manager__c, Budget_Status__c from Opportunity where Id in :oIds];
            try
            {
                list<Approval.ProcessSubmitRequest> reqList = new list<Approval.ProcessSubmitRequest>();
                for (Opportunity opp : lstOpp)
                {
                    approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                    if (opp.Budget_Status__c == 'Budget In Progress')    // Only submit opportunities which are set to Budget In Progress
                    {
                        req1.setObjectId(opp.id);
                        reqList.add(req1);
                        if (opp.Area_Manager__c != null)
                            stRecipients.Add(opp.Area_Manager__c);
                    }
                }
                approval.ProcessResult[] result = Approval.process(reqList, True);
                
                // We'll only get to here if the submission was successful so send the notification email(s)
                for (String recip : stRecipients)        // Put the unique set of recipient IDs into a List 
                    lstRecipients.add(recip);
                sendEmailNotification(lstRecipients);    // Pass the recipients list to the email method to send the notification emails
            }
            catch (System.DmlException e)
            {
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    //System.debug(e.getDmlMessage(i)); 
                    if (e.getDmlMessage(i) == 'No applicable approval process found.')
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'At least one of these records does not meet the entry criteria or initial submitters of any active approval processes. Please contact your administrator for assistance.'));
                    else
                        system.debug(e.getDmlMessage(i));
                }
            }
        }
    }
    
    public void sendEmailNotification(List<ID> recips)
    {
        String sTemplateId = [Select Id from EmailTemplate where Name = 'Submit Opportunity Budget Notification' LIMIT 1].Id;
        for (ID recipid : recips)
        {
            //Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEMailMessage();
            mail.setTargetObjectId(recipid);
            mail.saveAsActivity = false;
            mail.setTemplateId(sTemplateId);
            //if (UserInfo.getUserId() == '00520000001Zd4jAAC' || UserInfo.getUserId() == '00520000001JnOyAAK')    // used for testing
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    //### TEST METHODS ###
    public static TestMethod void testBulkApprovalController() {
        //set up test records
        Account acc = [Select Id from Account where Type = 'Customer' AND Area_Manager__c <> null LIMIT 1]; 
        Opportunity opp1 = new Opportunity(Name='test opp 1',Budget_Status__c = 'Budget In Progress', StageName = 'Renewable Opportunity', CloseDate = System.today(), AccountId = acc.Id);
        Opportunity opp2 = new Opportunity(Name='test opp 2',Budget_Status__c = 'Budget In Progress', StageName = 'Renewable Opportunity', CloseDate = System.today(), AccountId = acc.Id);
        List<Opportunity> lopp = new List<Opportunity>();
        lopp.add(opp1);
        lopp.add(opp2);
        insert(lopp);
        
        //set up the test records which will trigger the exception (exclude record adjustment from one of the oppty records)
        Opportunity opp3 = new Opportunity(Name='test opp 3',Budget_Status__c = 'Budget In Progress', StageName = 'Under Approval - AM', CloseDate = System.today(), AccountId = acc.Id);
        Opportunity opp4 = new Opportunity(Name='test opp 4',Budget_Status__c = 'Budget In Progress', StageName = 'Under Approval - AM', CloseDate = System.today(), AccountId = acc.Id);
        List<Opportunity> lopp2 = new List<Opportunity>();
        lopp2.add(opp3);
        lopp2.add(opp4);
        insert(lopp2);
        
        PageReference pageRef = Page.BulkApproval;
        Test.setCurrentPage(pageRef);
        
        // controller for valid opportunities
        List<Opportunity> lopp_test = new List<Opportunity>();
        lopp_test = [Select Name,Budget_Status__c, StageName, CloseDate, AccountId from Opportunity where Id = :opp1.Id OR Id = :opp2.Id];
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lopp_test);
        BulkApprovalController bac = new BulkApprovalController(sc);

        // controller for opportunities which don't satisfy approval entry criteria
        List<Opportunity> lopp2_test = new List<Opportunity>();
        lopp2_test = [Select Name,Budget_Status__c, StageName, CloseDate, AccountId from Opportunity where Id = :opp3.Id OR Id = :opp3.Id];
        ApexPages.StandardSetController sc2 = new ApexPages.StandardSetController(lopp2_test);
        BulkApprovalController bac2 = new BulkApprovalController(sc2);
        
        //execute test case
        Test.StartTest();
            bac.submitAll();
            System.Assert(bac.GetRecordCOunt() == 2);
            //System.Assert([Select Budget_Status__c from Opportunity where Id = :opp1.Id].Budget_Status__c == 'Under Approval - AM');  // validate that the approval process has changed the budget status
    
            //test with an opportunity which fails entry criteria, this ensures coverage of the exception code
            bac2.submitAll();
            BulkApprovalController bac2_b = new BulkApprovalController(sc2);
            //System.Assert([Select Budget_Status__c from Opportunity where Id = :opp3.Id].Budget_Status__c == 'Budget In Progress');  // validate that the status stays the same (i.e. thew approval process hasn't changed the status)
            
        Test.StopTest();
        
    }
    
}