@isTest(seealldata=false)
Public class TestUWRLayUpFormCtrl 
{
    public static void commonData(){
        AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;        
        GardTestData test_rec = new GardTestData();
        test_rec.commonRecord();
        test_rec.customsettings_rec();   
    }
    
    @isTest public static void UWRLayUpFormCtrlBrokerPI(){
        UWRLayUpFormCtrl  uwrLayUp = new UWRLayUpFormCtrl();
        uwrLayUp.header='';
        uwrLayUp.footer='';
        uwrLayUp.signature='';
        uwrLayUp.header_ack='';
        uwrLayUp.dt=NULL;
        uwrLayUp.mail_subject='';
        uwrLayUp.notificationMail='';
        uwrLayUp.link='';
        uwrLayUp.AcknowledgeMail='';
        uwrLayUp.mapClient= new map<String, Id> ();
        uwrLayUp.mapIdClient= new map<Id, String>();
        uwrLayUp.id_attachment=new List<Id>();
        uwrLayUp.selectedClientfromPrev='';
        uwrLayUp.attachment=GardTestData.brokerAttachment1;
        CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c ='Test123@gmail.com');
        insert cvar;
        ClientActivityEvents__c UwrCaEvents  = new ClientActivityEvents__c(name = 'Layup form P&I', Event_Type__c ='Layup form P&I');
        insert UwrCaEvents ;
        ClientActivityEvents__c UwrCaEvents1  = new ClientActivityEvents__c(name = 'Layup form Marine', Event_Type__c ='Layup form Marine');
        insert UwrCaEvents1 ;
        Test.startTest();
        commonData();  
        Test.stopTest();
        //invoking methods of UWRLayUpFormCtrl for client and broker users.....
        //system.debug('GARDTESTDATA.BROKERUSERID**************' +GardTestData.brokerUser.id);
        System.runAs(GardTestData.brokerUser){
            //Test.startTest();
            UWRLayUpFormCtrl test_broker=new UWRLayUpFormCtrl();  
            
            //system.debug('brokercontract_2nd**************' +GardTestData.brokercontract_1st.id);
            system.debug('brokercontract_2nd**************' +GardTestData.brokercontract_2nd.id);
            system.debug('brokerContract_3rd**************' + GardTestData.brokerContract_3rd.id);
            system.debug('clientContract_1st**************' +GardTestData.clientContract_1st.id);
            system.debug('clientContract_2nd **************'+GardTestData.clientContract_2nd.id );
            system.debug('clientContract_3rd **************'+GardTestData.clientContract_3rd.id );
            test_broker.lstContact = new List<Contact>();
            test_broker.noImoNo = true;
            //test_broker.attachmentList = new List<Attachment>();
            test_broker.errMsg = 'hello';
            test_broker.Cargo = '';
            test_broker.layup = '';
            test_broker.selectedCover ='';
            test_broker.check1='';
            test_broker.check2='';
            test_broker.check3='';
            test_broker.check4='';
            test_broker.check5='';
            test_broker.check6='';
            test_broker.check7='';
            //test_broker.getClientLst();
            
            test_broker.mapIdClient.put(GardTestData.clientAcc.id,GardTestData.clientAcc.Name);
            test_broker.isSelGlobalClient = true;  
            test_broker.setGlobalClientIds=null;
            test_broker.selectedGlobalClient=null; 
            //test_broker.getClientLst();
            
            List<SelectOption> layupOption=test_broker.getlayupOption();
            List<SelectOption> clientOptions = test_broker.getClientLst(); 
            //test_broker.getCName();
            
            system.debug('Broker account.Id**********'+ GardTestData.brokerAcc.id);
            
            Date dt=date.today();
            test_broker.arrivalDate = String.valueof(DateTime.newInstance(dt.year(),dt.month(),dt.day()).format('d-MM-YYYY'));
            test_broker.formType='PI';
            test_broker.FindBy='objname';
            test_broker.searchByImoObjectName();
            
            test_broker.selectedClient = GardTestData.clientAcc.id;
            test_broker.ObjName = GardTestData.test_object_1st.name;
            test_broker.searchByImoObjectName();
            //test_broker.ObjName ='4355555';
            test_broker.FindBy='imo';
            test_broker.searchByImoObjectName();
            test_Broker.selectedObj = GardTestData.test_object_1st.id;
            test_broker.saveRecord();
        }
        //Test.stopTest();
    }
    
    @isTest public static void UWRLayUpFormCtrlBrokerMarine(){
        UWRLayUpFormCtrl  uwrLayUp = new UWRLayUpFormCtrl();
        uwrLayUp.header='';
        uwrLayUp.footer='';
        uwrLayUp.signature='';
        uwrLayUp.header_ack='';
        uwrLayUp.dt = NULL;
        uwrLayUp.mail_subject='';
        uwrLayUp.notificationMail='';
        uwrLayUp.link='';
        uwrLayUp.AcknowledgeMail='';
        uwrLayUp.mapClient= new map<String, Id> ();
        uwrLayUp.mapIdClient= new map<Id, String>();
        uwrLayUp.id_attachment=new List<Id>();
        uwrLayUp.selectedClientfromPrev='';
        uwrLayUp.attachment=GardTestData.brokerAttachment1;
        CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c ='Test123@gmail.com');
        insert cvar;
        ClientActivityEvents__c UwrCaEvents  = new ClientActivityEvents__c(name = 'Layup form P&I', Event_Type__c ='Layup form P&I');
        insert UwrCaEvents ;
        ClientActivityEvents__c UwrCaEvents1  = new ClientActivityEvents__c(name = 'Layup form Marine', Event_Type__c ='Layup form Marine');
        insert UwrCaEvents1 ;
        Test.startTest();
        commonData();  
        Test.stopTest();
        System.runAs(GardTestData.brokerUser){
            //Test.startTest();
            UWRLayUpFormCtrl test_broker = new UWRLayUpFormCtrl(); 
            test_broker.formType='Marine';
            test_broker.FindBy='objname';
            test_broker.selectedClient = GardTestData.clientAcc.id;
            //test_broker.ObjName = GardTestData.test_object_1st.name;
            test_broker.searchByImoObjectName();
            
            test_broker.ObjName ='4355555';
            test_broker.FindBy='imo';
            test_broker.searchByImoObjectName();
            
            test_broker.selectedObj=GardTestData.test_object_1st.id; 
            test_broker.setObjectSelection();
            test_broker.selectedClient = null;
            test_broker.loggedInAccountId =GardTestData.clientAcc.id;
            uwrLayUp.dt = Date.today();
            test_broker.departureDate = String.valueof(DateTime.newInstance(uwrLayUp.dt.year(),uwrLayUp.dt.month(),uwrLayUp.dt.day()).format('d-MM-YYYY'));
            //test_broker.saveRecord();
            
            test_broker.uwf = new Case();
            test_broker.uwf.id = GardTestData.brokerCase.id;
            /*test_broker.attachment=GardTestData.brokerAttachment1;
            test_broker.attachment.name='.abc';
            test_broker.attachment1=GardTestData.brokerAttachment2;
            test_broker.attachment1.name='.abc';
            test_broker.attachment2=GardTestData.brokerAttachment3;
            test_broker.attachment2.name='.abc';
            test_broker.attachment3=GardTestData.brokerAttachment4;
            test_broker.attachment3.name='.abc';
            test_broker.attachment4=GardTestData.brokerAttachment5;
            test_broker.attachment4.name='.abc';
            system.debug('brokerattachment4****'+test_broker.attachment4);*/
            
            //test_broker.saverecord();
            
            test_broker.clearRecord();
            //test_broker.goNext();
            test_broker.goPrevPIFrom();
            Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
            test_broker.caseTestId = singleCase.Id;
            test_broker.caseIns= GardTestData.brokercase;
            test_broker.saveSubmit();
            test_broker.cancelRecord();
            
            test_broker.formType='PI';
            test_broker.saveSubmit();
            
            test_broker.arrivalDate=null;
            test_broker.departureDate=null;
            test_broker.cargo=null;
            test_broker.layup=null;
            Case updtUWF = [SELECT Id,Object_name__c,tonnage__c,Layup_location__c,Lay_up_location__c,Mooring_arrangements__c ,
                                 Other_Info__c ,Manning_arrangements__c,Lay_up_site__c,Maintenance_Preservation_declaration__c,
                                 Owners_Insured__c ,Complete_Lay_up_plan_including_follow__c ,Lay_up_declaration__c,Imo_Lloyds_No__c,Number_of_crew__c FROM Case WHERE Id =: GardTestData.brokerCase.Id];
            updtUWF.Object_name__c = null;
            updtUWF.Imo_Lloyds_No__c=null;
            updtUWF.tonnage__c = null;
            updtUWF.Layup_location__c = 'Text';
            updtUWF.Lay_up_location__c = false;
            updtUWF.Mooring_arrangements__c = true;
            updtUWF.Manning_arrangements__c = true;
            updtUWF.Other_Info__c = 'Text';
            updtUWF.Lay_up_site__c = true;
            updtUWF.Maintenance_Preservation_declaration__c = true;
            updtUWF.Owners_Insured__c = 'Text';
            updtUWF.Number_of_crew__c = '100';
            updtUWF.Layup_location__c = 'True';
            updtUWF.Lay_up_declaration__c=True;
            updtUWF.Number_of_crew__c=null;
            update updtUWF;
            //test_broker.goNext();
            test_broker.saveSubmit();
            updtUWF.Lay_up_location__c = true;
            update updtUWF;
            test_broker.formType='Marine';
            //test_broker.saveSubmit();
            
            test_broker.uwf.Complete_Lay_up_plan_including_follow__c=true;
            test_broker.uwf.Lay_up_declaration__c=true;
            test_broker.uwf.Lay_up_location__c=true;
            test_broker.uwf.Mooring_arrangements__c=true;
            test_broker.uwf.Lay_up_site__c=true;
            test_broker.uwf.Manning_arrangements__c=true;
            test_broker.uwf.Maintenance_Preservation_declaration__c=true;     
            test_broker.goNext();
            
            test_broker.sendToForm();
            test_broker.strSelected=GardTestData.clientAcc.id+';'+GardTestData.clientAcc.id;
            test_broker.fetchSelectedClients();
            
            test_broker.formType='PI';
            test_broker.fetchSelectedClients();
            /*PageReference pr = Page.UWRLayUpForm;
            Test.setCurrentPageReference(pr);
            ApexPages.currentPage().getParameters().put('strParams','PI');
            test_broker.chkAccess();
            ApexPages.currentPage().getParameters().put('strParams','Marine');*/
            test_broker.chkAccess();
            test_broker.formType='Marine';
            //MyGardHelperCtrl.ACM_JUNC_OBJ.Marine_Access__c=true;
            test_broker.chkAccess();
            test_broker.userType='Clinic';
            test_broker.chkAccess();
            test_broker.getCName();
            test_broker.changeSelectedClientIdFormsCS();
            //Test.stopTest();
        }
    }
    
    @isTest public static void UWRLayUpFormCtrlClient(){
        commonData();
        Test.startTest();
        System.runAs(GardTestData.clientUser){
            UWRLayUpFormCtrl test_client=new UWRLayUpFormCtrl();
            
            Date dt=date.today();
            test_client.arrivalDate = String.valueof(DateTime.newInstance(dt.year(),dt.month(),dt.day()).format('d-MM-YYYY'));
            test_client.formType='PI';
            
            test_client.FindBy='objname';
            //test_client.selectedClient = GardTestData.clientAcc.id;
            test_client.ObjName = GardTestData.test_object_1st.name;
            test_client.searchByImoObjectName();
            
            test_client.FindBy='imo';
            test_client.ObjName ='4355555';
            test_client.searchByImoObjectName();
            
            //test_client.saveRecord();
            
            test_client.formType='Marine';
            
            test_client.FindBy='objname';
            //test_client.selectedClient = GardTestData.clientAcc.id;
            test_client.ObjName = GardTestData.test_object_1st.name;
            test_client.searchByImoObjectName();
            
            test_client.FindBy='imo';
            test_client.ObjName ='4355555';
            test_client.searchByImoObjectName();
            
            test_client.formType='Marine';
            test_client.FindBy='objname';
            test_client.selectedObj=GardTestData.test_object_1st.id;          
            test_client.selectedClient = null;
            test_client.loggedInAccountId =GardTestData.clientAcc.id;
            //Date dt=date.today();
            test_client.departureDate = String.valueof(DateTime.newInstance(dt.year(),dt.month(),dt.day()).format('d-MM-YYYY'));
            test_client.setGlobalClientIds= new List<String>();
            test_client.setGlobalClientIds.add(GardTestData.clientAcc.id);
            test_client.fetchSelectedClients();
            test_client.fetchGlobalClients();
            //test_client.saveRecord();
            
            //test_client.uwf=null;
            //test_client.saveSubmit();
        }
        Test.stopTest();  
    }
    
    @isTest public static void UWRLayUpFormCtrl2(){
        commonData();
        Test.startTest();
        System.runAs(GardTestData.brokerUser){
            UWRLayUpFormCtrl test_broker=new UWRLayUpFormCtrl();
            test_broker.isSelGlobalClient = false;  
            //test_broker.setGlobalClientIds.add(GardTestData.clientAcc.id);
            test_broker.selectedGlobalClient.add(GardTestData.clientAcc.id);
            test_broker.getClientLst();
            
            test_broker.setGlobalClientIds= new List<String>();
            test_broker.setGlobalClientIds.add(GardTestData.clientAcc.id);
            test_broker.fetchSelectedClients();
            test_broker.getClientLst();
            
            test_broker.formType='Marine';
            test_broker.selectedObj=GardTestData.test_object_1st.id;          
            test_broker.selectedClient = null;
            test_broker.loggedInAccountId =GardTestData.clientAcc.id;
            Date dt=date.today();
            test_broker.departureDate = String.valueof(DateTime.newInstance(dt.year(),dt.month(),dt.day()).format('d-MM-YYYY'));
            test_broker.saveRecord();
            //test_broker.uwf.id=null;
            //test_broker.savesubmit();
            test_broker.formType='PI';
            MyGardHelperCtrl.ACM_JUNC_OBJ.Marine_Access__c=false;
            test_broker.chkAccess();
            test_broker.formType='Marine';
            test_broker.chkAccess();
            test_broker.arrivalDate = String.valueof(DateTime.newInstance(dt.year(),dt.month(),dt.day()).format('d-MM-YYYY'));
            //test_broker.searchByImoObjectName();
            test_broker.formType='PI';
            test_broker.FindBy='objname';
            test_broker.searchByImoObjectName();
            test_broker.FindBy='imo';
            test_broker.searchByImoObjectName();
            test_broker.formType='Marine';
            test_broker.FindBy='objname';
            test_broker.searchByImoObjectName();
            test_broker.FindBy='imo';
            test_broker.searchByImoObjectName();
        }
        Test.stopTest();
        
    }
}