@isTest
private class TestUpdateCheckListName {
    
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    public static Opportunity_Checklist__c [] testOppChecklists;
    static TestMethod void testQuoteAccepted() {
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        //### create test records ###
        String strRecId = [SELECT Id FROM RecordType WHERE Name='Marine' AND sObjectType='Opportunity' LIMIT 1].Id;
        //create Account
        Account acc = TestDataGenerator.getApprovedMemberAccount();//new Account(Name='APEXTESTACC001', Membership_Status__c = 'Approved');
        //insert acc;
        String strAccId = acc.Id;
        //create opportunity
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = strRecId;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId;
        opp.StageName = 'Risk Evaluation';//Renewable Opportunity
        opp.Type = 'New Business';        
        opp.Business_Type__c = 'MOUs';
        opp.Approval_Criteria__c = 'Self Approval';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        opp.Confirm_not_on_sanction_list__c = true;
        opp.Amount = 100;
        
        
        insert opp;
        String strOppId = opp.Id;
        
        testOppChecklists = new Opportunity_Checklist__c []{
            new Opportunity_Checklist__c (Opportunity__c = opp.id, Checklist_Completed__c = true,Additional_Covers1__c = true),
            new Opportunity_Checklist__c (Opportunity__c = opp.id,  Checklist_Completed__c = true),
            new Opportunity_Checklist__c (Opportunity__c = opp.id, Checklist_Completed__c = true)
        };
        
         insert testOppChecklists ;
         List<Opportunity_Checklist__c> OppCheckList = [SELECT ID,  Checklist_Completed__c, Opportunity_Name__c FROM Opportunity_Checklist__c WHERE Opportunity__c =: strOppId ];
       
       String name = 'Checklist - ' + OppCheckList[0].Opportunity_Name__c;
       system.assert(name.length()>8);
       
         System.assertEquals(OppCheckList[0].Checklist_Completed__c, true);
         /*
        for (Opportunity_Checklist__c optCklist : OppCheckList) {
         if(optCklist.Checklist_Completed__c){
            if(optCklist.Agreement_type__c  == 'Owners'){
                optCklist.Owners_P_I_Completed__c = true;
                System.assertEquals(optCklist.Agreement_type__c  , 'Owners'); 
                System.assertEquals(optCklist.Owners_P_I_Completed__c , true); 
                
          }
            if(optCklist.Agreement_type__c  == 'MOU'){
                optCklist.MOU_Completed__c = true;
                System.assertEquals(optCklist.Agreement_type__c  , 'MOU');
                System.assertEquals(optCklist.MOU_Completed__c , true);
            }
            if(optCklist.Agreement_type__c  == 'Charterers'){
                optCklist.Charterers_Completed__c = true;
                System.assertEquals(optCklist.Agreement_type__c  , 'Charterers');
                System.assertEquals(optCklist.Charterers_Completed__c , true);
            }
          }
        }*/
     List<Opportunity_Checklist__c> OppCheckList2 = [SELECT ID, Checklist_Completed__c, Opportunity_Name__c FROM Opportunity_Checklist__c];
     OppCheckList2[2].Additional_Covers1__c = false;
     update OppCheckList2;
    }
}