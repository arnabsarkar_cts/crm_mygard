@isTest
public class testGardForgotPasswordController{
public static User salesforceUser;
public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
 //Create a user
    public static void createUser(){
        salesforceUser = new User(
        Alias = 'standt', 
        profileId = salesforceLicenseId ,
        Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8',
        CommunityNickname = 'test13',
        LastName='Testing',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',  
        TimeZoneSidKey='America/Los_Angeles',
       // UserName='test_user_123@salesforce.com'
        UserName='test_1@salesforce.com.2016R3' //Aritra 12.9
        );
        insert salesforceUser; 
    }
   
    @isTest static void  testGardForgotPsswrdMethod(){
        createUser();
        test.StartTest();
        GardForgotPasswordController  GFPC = new GardForgotPasswordController();
        GFPC.username_1 = 'test_1@salesforce.com';
        GFPC.Submit();
        test.StopTest();
        }
    @isTest static void  testGardForgotPsswrdMethod2(){
        createUser();
        test.StartTest();
        GardForgotPasswordController  GFPC = new GardForgotPasswordController();
        GFPC.username_1 = 'test _1@salesforce.com';
        GFPC.Submit();
        test.StopTest();
        }
}