@isTest

public class TestEmailWrapper{
public static string testStr =  'asd'; 
//Public list<EmailTemplate> Emaillist = new list<EmailTemplate>();
//Public EmailTemplate e1,e2,e3,e4,e5,e6;

@future

public static void inserttemp(){
 list<EmailTemplate> Emaillist = new list<EmailTemplate>();

 EmailTemplate  e1 = new EmailTemplate(Name = 'MyGard-Request Change Type 1-Acknowledgement');

Emaillist.add(e1); 

 EmailTemplate  e2 = new EmailTemplate(Name = 'MyGard-Request Change Type 2-Notification');
Emaillist.add(e2 );

 EmailTemplate  e3 = new EmailTemplate(Name = 'MyGard-Request Change Type 1-Notification');
Emaillist.add(e3 );

 EmailTemplate  e4 = new EmailTemplate(Name = 'MyGard-Request Change Type 2-Acknowledgement');
Emaillist.add(e4 );

 EmailTemplate  e5 = new EmailTemplate(Name = 'MyGard-Request Change Type 3-Acknowledgement');
Emaillist.add(e5 );

 EmailTemplate  e6 = new EmailTemplate(Name = 'MyGard-Request Change Type 3-Notification');
Emaillist.add(e6 );

insert Emaillist;

}

public static testmethod void testemail(){

Gardtestdata test_rec = new gardtestdata();
test_rec.commonrecord();

EmailServicesAddress testobj1= new EmailServicesAddress(LocalPart = 'ownergard',IsActive = true); 
//insert testobj1;

OrgWideEmailAddress owea = new OrgWideEmailAddress(DisplayName = 'no-reply@gard.no', Address = 'no-reply@gard.no');
//insert owea;
list<EmailTemplate> Emaillist = new list<EmailTemplate>();
EmailTemplate e1,e2,e3,e4,e5,e6;

Test.startTest();
System.runAs(gardtestdata.brokerUser)
{

    EmailWrapper newobj = new EmailWrapper();
    newobj.selectedReqType ='type 1';
    newobj.clientName = 'DHT Management AS';
    newobj.coverName = 'Defence Cover';
    newobj.effcDate = 'today';
    newobj.comments = 'abc';
    newobj.phn =  testStr ;
    newobj.Email =  testStr ;
    newobj.orgName =  testStr ;
    newobj.loggedInUsrPhnNo =  testStr ;
    newobj.loggedInUsrMobileNo =  testStr ;
    newobj.loggedInUsrMailId =  testStr ;
    newobj.UnderWriterMailId =  testStr ;
    newobj.loggedInUsrName =  testStr ;
    newobj.selectedprodArea = 'marine';
    newobj.objType = 'Mumbai';
    newobj.yrBuilt = '1991';
    newobj.tonnage = '11';
    newobj.sendAckMail();
    newobj.send_UWR_Notify_Mail();
    System.assert(newobj.emailPlainTextBody != null,true);
    newobj.URW_NotifyMailType2();
    newobj.sendAckMailForType2();
    newobj.sendAckMailForType3();
    newobj.URW_NotifyMailType3();

}
    test.stopTest();
}
}