public class PEMEEnrollmentApprovalHistoryCtrl
{
public string pemeInvoiceId{get;set;}
public PEME_Enrollment_Form__c pemeInvoiceInstance;
public List<Id> InvProcessIds;
public List<Id> InvUserIds;
public List<ProcessInstanceStep> processSteps{get;set;}
public map<Id,String> idNameMap{get;set;}
public List<ProcessInstanceWorkitem> lstPiw{get;set;}

public PEMEEnrollmentApprovalHistoryCtrl(ApexPages.StandardController controller)
{
    pemeInvoiceInstance = (PEME_Enrollment_Form__c)controller.getRecord();
    pemeInvoiceId = String.valueOf(pemeInvoiceInstance.id).substring(0,15);
    fetchHistory();
}
public void fetchHistory()
{
    InvProcessIds = new List<Id>();
    InvUserIds = new List<Id>();
    processSteps = new  List<ProcessInstanceStep>();
    idNameMap = new map<Id,String>();
    lstPiw = new List<ProcessInstanceWorkitem>();
    if(pemeInvoiceId!= null) 
    {
        for(ProcessInstance p:[SELECT Id FROM ProcessInstance where TargetObjectId=:pemeInvoiceId])
        {
            InvProcessIds.add(p.Id);            
        }
    }
    if(InvProcessIds!=null && InvProcessIds.size()>0)
    {
        
        for(Group g:[Select Id, Name,DeveloperName,Type from Group where Type = 'Queue' and DeveloperName in ('PEME_GARD_ARENDAL_QUEUE','PEME_GARD_HONGKONG_QUEUE')])
        {
            idNameMap.put(g.Id,g.Name);
        }
        for(ProcessInstanceStep pStep:[SELECT ActorId,Comments,CreatedById,CreatedDate,Id,OriginalActorId,ProcessInstanceId,StepNodeId,StepStatus FROM ProcessInstanceStep where ProcessInstanceId in:InvProcessIds order by CreatedDate DESC ])
        {
           processSteps.add(pStep);           
           InvUserIds.add(pStep.ActorId);            
        }
         for(ProcessInstanceWorkitem piw : [SELECT Id,CreatedDate,OriginalActorId,ActorId FROM ProcessInstanceWorkitem WHERE ProcessInstance.TargetObjectId =: pemeInvoiceId AND ProcessInstance.Status = 'Pending' LIMIT 1])
         {           
           InvUserIds.add(piw.ActorId);
           lstPiw.add(piw);           
         }
         for(User u:[SELECT Id,Name from user where id in:InvUserIds])
        {
            idNameMap.put(u.Id,u.Name);
        }
    }
    system.debug('lstPiw----------->'+lstPiw);
    system.debug('idNameMap---->'+idNameMap);
}
}