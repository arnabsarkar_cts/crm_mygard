/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=true)
private class GardLogoutControllerTest {

    static testMethod void myUnitTest() {
User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
ID ProfileID = [ Select id,UserType from Profile where name = 'Partner Community Pooled (copy from Login) User Custom' and UserType='PowerPartner'].id;
 User user=[select id,UserRole.id from user where profileId=:profileId and isActive=true limit 1];

        GardLogoutController controller=new GardLogoutController();
        
        test.startTest();
        system.runAs(user){
        controller.deleteSession();
        }
        test.stopTest();
    }
}