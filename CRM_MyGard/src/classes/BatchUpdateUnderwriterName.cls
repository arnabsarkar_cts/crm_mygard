global class BatchUpdateUnderwriterName implements Database.Batchable<sObject> {
    
    //Declaring variables.
    global String strquery;
    global BatchUpdateUnderwriterName (){                  
        
        strquery = 'SELECT Id,Gard_Share__c FROM Asset';                                
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){               
        return Database.getQueryLocator(strquery);        
    }
    
    /**
     * Implementation
     *
     * @return : null
     */
     global void execute(Database.BatchableContext BC, List<Asset> scope){  
        
        try{
            Database.update(scope,false);
                            
        }catch( Exception ex){            
        }
      
    } 
    
    global void finish(Database.BatchableContext BC){ 
    }
}