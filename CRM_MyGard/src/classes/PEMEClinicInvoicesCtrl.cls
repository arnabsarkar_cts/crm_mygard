public class PEMEClinicInvoicesCtrl{
    
    private static final Integer SMARTQUERY_PAGE_SIZE   = 10;
    public set<String> lstClientsSubmitted {get;set;}   
    public list<String> selectedClientsSubmitted {get;set;} 
    public set<String> lstStatus {get;set;}   
    public list<String> selectedStatus {get;set;}    
    public list<PEME_Invoice__c> lstInvoicesSubmitted {get;set;}
    public list<PEME_Invoice__c> lstinv {get;set;}     
    public String selectedClinic {get;set;}
    //pagination variables
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public String sSoqlQuery;
    private String nameOfMethod;
    Public Integer intStartrecord { get ; set ;}
    Public Integer intEndrecord { get ; set ;}
    public Integer pageNum{get;set;}//{pageNum = 1;}
    public String jointSortingParam { get;set; }
 //   private String sortDirection = Ascending;
 //   private String sortExp = 'Client__r.Name';
    public Integer noOfData{get;set;}
    set<String> strStatus;
    public String userName{get;set;}
    //pagination variables
    public String orderBy{get;set;}//{orderBy = '';}
    private String sortDirection = 'ASC';
    private String sortExp = '';  
    public string loggedInContactId;
    public List<Contact> lstContact{get;set;}
    private static String Ascending='ASC';
    public String orderByNew
    {
     get
         {
            return sortExp;
         }
         set
         {
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExp)
             sortDirection = (sortDirection.contains('DESC')? Ascending : 'DESC');
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     }
     public String getSortDirection()
     {
        //if not column is selected 
        if (orderByNew == null || orderByNew == '')
          return Ascending;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value)
     {  
       sortDirection = value;
     }
    public ApexPages.StandardSetController submissionSetConOpen{
    get{ 
           return submissionSetConOpen;         
    }
    set; }
    public List<SelectOption> getClientOptionsSub() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstClientsSubmitted!= null && lstClientsSubmitted.size()>0){
            for(String strOp:lstClientsSubmitted){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();            
        }
        return Options;
    }
    
    public List<SelectOption> getStatusOptions() {
        List<SelectOption> Options = new List<SelectOption>();
        if(lstStatus!= null && lstStatus.size()>0){
            for(String strOp:lstStatus){
                if(strOp != null )
                Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();            
        }
        return Options;
    }
    
    public String sortExpression{
         get{
            return sortExp;
         }
         set{          
           if (value == sortExp)
             sortDirection = (sortDirection == Ascending)? 'DESC' : Ascending;
           else
             sortDirection = Ascending;
             sortExp = value;
         }
     }
    
  /*   public String getSortDirection(){        
        if (sortExpression == null || sortExpression == '')
          return Ascending;
        else
         return sortDirection;
     }
    
     public void setSortDirection(String value){  
       sortDirection = value;
     }
     */
    public PEMEClinicInvoicesCtrl(){ 
        pageNum = 1;
        orderBy = '';
        lstClientsSubmitted = new set<String>();         
        selectedClientsSubmitted = new list<String>();
        lstStatus = new set<String>();         
        selectedStatus = new list<String>();                
        lstInvoicesSubmitted = new list<PEME_Invoice__c>();         
        list<User> lstUser = new list<User>();
        lstUser = [select Id,contactId,Contact.AccountId,Contact.Name from User where Id=:UserInfo.getUserId()];
        userName = lstUser[0].Contact.Name;
        orderByNew = 'Client__r.Name';
        selectedClinic = lstUser[0].Contact.AccountId;
        if(selectedClinic!=null && selectedClinic!=''){      
            generateInvoices();
        }             
    }
    
    public void generateInvoices(){
                
        List<User> lstUser = new List<User>([select id,Contactid,name,Contact.Name,Email from User where id=:UserInfo.getUserId()]);
        if(lstUser!=null && lstUser.size()>0 && lstUser[0].Contactid!=null)
        {
            loggedInContactId = lstUser[0].Contactid;
            lstContact = new List<Contact>([select name, account.id,account.name,accountid, account.recordtypeid,account.company_Role__c, /*IsPeopleLineUser__c,*/account.BillingPostalcode__c,account.BillingCountry__c,account.BillingState__c,account.Billing_Street_Address_Line_1__c,account.Billing_Street_Address_Line_2__c,account.Billing_Street_Address_Line_3__c,account.Billing_Street_Address_Line_4__c 
                                          from Contact 
                                          where id=:loggedInContactId]);           
            if(lstContact!=null && lstContact.size()>0 && lstContact[0].accountId!=null)
            {
                if(lstContact[0].account.company_Role__c.contains('External Service Provider')) //Added for MYG-2948
                { 
                    pageNum = 1;
                    lstInvoicesSubmitted= new List<PEME_Invoice__c>();
                    
                  /*  if (jointSortingParam != null){
                        system.debug('******jointSortingParam**************'+jointSortingParam);
                        String[] arrStr = jointSortingParam .split('##');
                        sortDirection = arrStr[0];
                        sortExp = arrStr[1];            
                    }
                    */
                    String strCondition ='';
                    
                    if(selectedClientsSubmitted!= null && selectedClientsSubmitted.size()>0){
                        strCondition = ' AND Client__r.Name IN:selectedClientsSubmitted';
                    }
                    if(selectedStatus!= null && selectedStatus.size()>0){
                        strCondition = strCondition + ' AND Status__c IN:selectedStatus';
                    }
                    
                    string sortFullExp = sortExpression  + ' ' + sortDirection;
                    
                    strStatus = new set<String>{'Under Approval','Approved','Partially Approved','Rejected'};       
                    sSoqlQuery = 'SELECT Client__c,GUID__c,Client__r.Name,Clinic_Invoice_Number__c,Clinic__c,Crew_Examined__c,Invoice_Date__c,PEME_reference_No__c,Total_Amount__c,Status__c,Date_of_Final_Verification__c'+
                                ' FROM PEME_Invoice__c where Status__c IN:strStatus AND Clinic__c=:selectedClinic'+strCondition;
                    
                  //  sSoqlQuery = sSoqlQuery+strCondition+' ORDER BY '+sortFullExp+' ';
                    orderBy = ' ORDER BY ' + orderByNew + ' ' + sortDirection; //For new sorting 
                    sSoqlQuery = sSoqlQuery+ ' ' + orderBy; 
                    submissionSetConOpen = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery + ' limit 10000')); 
                    submissionSetConOpen.setPageSize(10);
                    createOpenUserList();      
                  //  searchAssets();
                           
                    for(PEME_Invoice__c objInvoice:[SELECT Client__c,Client__r.Name,Clinic_Invoice_Number__c,Clinic__c,Crew_Examined__c,Invoice_Date__c,PEME_reference_No__c,Total_Amount__c,Date_of_Final_Verification__c, 
                    Status__c FROM PEME_Invoice__c where Status__c IN:strStatus AND Clinic__c=:selectedClinic order by Client__r.Name asc]){               
                        lstClientsSubmitted.add(objInvoice.Client__r.Name);
                        lstStatus.add(objInvoice.Status__c);                
                    }
                }
            }
        }
               
    }
    Public void createOpenUserList()
    {
        if(submissionSetConOpen!=null)
        {
            lstInvoicesSubmitted.clear(); 
            for(PEME_Invoice__c inv : (List<PEME_Invoice__c>)submissionSetConOpen.getRecords())             
                lstInvoicesSubmitted.add(inv);
                System.debug('********'+lstInvoicesSubmitted.size());         
        } 
    }
    //Returns to the first page of records
    public void firstOpen() {
        submissionSetConOpen.first(); 
        pageNum = submissionSetConOpen.getPageNumber();
        
        createOpenUserList();
        
    }
    public Boolean hasPreviousOpen   
    {   
        get   
        {   
            return submissionSetConOpen.getHasPrevious();   
        }   
        set;   
    } 
     //Returns the previous page of records   
    public void previousOpen()   
    {   
        submissionSetConOpen.previous();
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();  
    } 
    //to go to a specific page number
    public void setpageNumberOpen()
    {
        submissionSetConOpen.setpageNumber(pageNum); 
        createOpenUserList();
    }
    public Boolean hasNextOpen   
    {   
        get   
        {   
            return submissionSetConOpen.getHasNext();   
        }   
        set;   
    } 
    //Returns the next page of records   
    public void nextOpen()   
    {   
        submissionSetConOpen.next(); 
        pageNum = submissionSetConOpen.getPageNumber();
        createOpenUserList();
        
    }
     //Returns to the last page of records
    public void lastOpen(){
        submissionSetConOpen.last(); 
        pageNum = submissionSetConOpen.getPageNumber();
        
        createOpenUserList();
        
    }
  /*  public void searchAssets(){
        if(sSoqlQuery.length()>0){            
            System.debug('Soql Query--->'+sSoqlQuery);  
            setCon = null;            
            
            lstInvoicesSubmitted=(List<PEME_Invoice__c>)setCon.getRecords();
            system.debug('****setCon.getResultSize()**********'+setCon.getResultSize());
            if(setCon.getPageNumber() == 1)
                intStartrecord = 1;
            else
                intStartrecord  = ((setCon.getPageNumber() - 1) * SMARTQUERY_PAGE_SIZE) + 1;
          
            intEndrecord = setCon.getPageNumber() * SMARTQUERY_PAGE_SIZE;
            if( intEndrecord > noOfRecords ){
                intEndrecord = noOfRecords ;
            }
        }
                                                              
    }
        
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                size = SMARTQUERY_PAGE_SIZE;
                System.debug('Soql Query--->'+sSoqlQuery);
                if(sSoqlQuery!=null && sSoqlQuery!=''){             
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(sSoqlQuery+' LIMIT 10000'));
                    setCon.setPageSize(size);
                    noOfRecords = setCon.getResultSize();
                    system.debug('****Total records****'+noOfRecords);
                    if (noOfRecords == null || noOfRecords == 0){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No search results found.'));
                    }else if (noOfRecords == 10000){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The search returned 10000 records (maximum allowable limit).'));
                    }
                }                
            }
            return setCon;
        }set;
    }  
    
    public void setpageNumber(){
        if(setCon!=null){
            setCon.setpageNumber(pageNum);
            searchAssets();
        }
    }
        
    public Boolean hasNext {
        get {
            if(setCon!=null)
            return setCon.getHasNext();
            else return false;
        }
        set;
    }
    
    public Boolean hasPrevious {
        get {
            if(setCon!=null)
            return setCon.getHasPrevious();
            else return false;
        }
        set;
    }
    
    public Integer pageNumber {
        get {
            if(setCon!=null)
            return setCon.getPageNumber();
            else return 0;
        }
        set;
    }
       
    public void navigate(){
        if(nameOfMethod=='previous'){
             setCon.previous();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }else if(nameOfMethod=='last'){
             setCon.last();
             searchAssets();
        }else if(nameOfMethod=='next'){
             setCon.next();
             pageNum=setCon.getpageNumber();
             searchAssets();
        }else if(nameOfMethod=='first'){
             setCon.first();
             searchAssets();
        }
    }
        
    public void first() {
        nameOfMethod='first';
        navigate();
    }
    
    public void next(){
        nameOfMethod='next';
        pageNum = setCon.getPageNumber();
        navigate();        
    }
   
    public void last(){
        nameOfMethod='last';
        navigate();
    }
  
    public void previous(){
        nameOfMethod='previous';
         pageNum = setCon.getPageNumber();
        navigate();
    } */
    public PageReference print(){
        lstinv = new List<PEME_Invoice__c>();
        Integer count = 1;
        noOfData = 0;
        for(PEME_Invoice__c mcl : Database.Query(sSoqlQuery+' LIMIT 10000')){               
            if(count<1000){
                lstinv.add(mcl);                    
                count++;
            }else{                   
                count = 1;
            }   
            noOfData++;                 
        }
        return page.PrintforSubmittedInvoices;
    }
        
     public PageReference exportToExcel(){
        lstinv = new List<PEME_Invoice__c>();
        Integer count = 1;
        noOfData = 0;
        for(PEME_Invoice__c mcl : Database.Query(sSoqlQuery+' LIMIT 10000')){               
            if(count<1000){
                lstinv.add(mcl);                    
                count++;
            }else{                   
                count = 1;
            }   
            noOfData++;                 
        }
        return page.GenerateExcelForSubmittedInvoices;
    }
    public pageReference clearOptions(){
        selectedClientsSubmitted.clear();
        selectedStatus.clear();
        generateInvoices();
        return null;
    }
    
}