global class SchedulerforAccountFlagUpdate implements Schedulable 
{
    global void execute(SchedulableContext sc)
    {   
        List<Id> listofAccounts = new List<Id>(); // empty list created to run the batch without any specific Account id
        BatchUpdateAccountFlag bu = new BatchUpdateAccountFlag(listofAccounts);
        database.executebatch(bu);
    }
}