@isTest(seeAllData=false)
Public class TestUWRMOUCtrl
{
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    public static void UWRMOUCtrlTestData(){
        
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonrecord();
        gtdInstance.customsettings_rec();
        /*
        //invoking methods of UWRMOUCtrl for broker and client users...
        Contract brokercontract_1st= New Contract(Accountid = GardTestData.brokerAcc.id, 
        Account = GardTestData.brokerAcc, 
        Status =  'Draft'   ,
        CurrencyIsoCode =  GardTestData.isoCodeStr , 
        StartDate = Date.today(),
        ContractTerm = 2,
        Agreement_Type__c = 'Charterers', 
        Broker__c = GardTestData.brokerAcc.id,
        Client__c = GardTestData.clientAcc.id,
        Broker_Name__c = GardTestData.brokerAcc.id,
        Expiration_Date__c =  date.valueof( GardTestData.expDateStr  ),
        Inception_date__c =   date.valueof( GardTestData.incptnDateStr )  ,
        Agreement_Type_Code__c =  GardTestData.agrmntTypCdStr ,
        Agreement_ID__c = 'Agreement_ID_87999',
        Business_Area__c= 'Marine' ,
        Accounting_Contacts__c = GardTestData.brokerUser.id,
        Contract_Reviewer__c = GardTestData.brokerUser.id,
        Contracting_Party__c = GardTestData.brokerAcc.id,
        Policy_Year__c= GardTestData.polYrStr  ,
        Name_of_Contract__c =  GardTestData.cntrctNmStr                                                              
        );
        insert brokercontract_1st;
        Asset brokerAsset_1st = new Asset(Name= 'Assettest',
        accountid = GardTestData.brokerAcc.id,
        contactid = GardTestData.BrokerContact.Id,                                 
        Agreement__c = brokerContract_1st.id, 
        Cover_Group__c =  GardTestData.piStr ,
        Cover_Group_Code__c = 'CR',
        CurrencyIsoCode= GardTestData.cntryISOStr  ,
        product_name__c =  GardTestData.piStr ,
        Object__c = GardTestData.test_object_1st.id,
        Underwriter__c = GardTestData.brokerUser.id,
        Risk_ID__c = '123456risqq',
        On_risk_indicator__c=true,
        Expiration_Date__c = date.today().adddays(50),
        Inception_date__c = date.today()
        ); 
        insert brokerAsset_1st ;
        Agreement_set__c agrSet = new Agreement_set__c(agreement_set_type__c = 'Test data for broker share',
        shared_by__c = GardTestData.brokerAcc.ID,
        shared_with__c = GardTestData.brokerAcc.ID,
        status__c = 'Active'
        );
        insert agrSet;
        
        Broker_share__c brokerShr1 = new Broker_share__c();
        brokerShr1.Active__c = true;
        brokerShr1.Agreement_set_id__c = agrSet.ID;
        brokerShr1.contract_external_id__c = String.valueOf(brokercontract_1st.ID);
        brokerShr1.Contract_id__c = brokercontract_1st.ID;
        brokerShr1.shared_to_client__c = true;
        insert brokerShr1;
        */
    }
    public static testMethod void UWRMOUCtrlBroker(){
        UWRMOUCtrlTestData();
        Test.startTest();
        System.runAs(GardTestData.brokerUser)         
        {
            
            UWRMOUCtrl test_broker=new UWRMOUCtrl();
            test_broker.setGlobalClientIds = new List<String>();
            test_broker.selectedGlobalClient = new List<String>();
            test_broker.lstContact = new List<Contact>();
            test_broker.imoNo = 123;
            test_broker.selectedIncludingFFO = 'yes';
            test_broker.selectedCountryCode =  'IN - India';
            test_broker.isSame = true;
            test_broker.selectedGlobalClient.add(GardTestData.clientAcc.id);
            test_broker.confirmationCheckbox=true;
            test_broker.CountryPrefix='91';
            test_broker.IMONotKnown=true;
            test_broker.mapObjects = new map<id,object__c>();
            test_broker.selectedObj=GardTestData.test_object_1st.id;
            test_broker.setObjectSelection();
            test_broker.setGlobalClientIds = new List<String>();
            List<SelectOption> clientOptions =  test_broker.getClientLst();
            List<SelectOption> flag=test_broker.getFlagList();
            List<SelectOption> port=test_broker.getPortList();
            List<SelectOption> classification=test_broker.getClassificationList();
            List<SelectOption> CollLiabilities=test_broker.getCollLiabilities();
            List<SelectOption> Nationalities= test_broker.getNationality();
            List<SelectOption> Countrycodes= test_broker.getCountrycodes();
            system.assertequals(clientOptions.size()>0,true,true);
            system.assertequals(flag.size()>0,true,true); 
            system.assertequals(port.size()>0,true,true); 
            system.assertequals(classification.size()>0,true,true); 
            system.assertequals(CollLiabilities.size()>0,true,true);
            system.assertequals(Nationalities.size()>0,true,true);
            test_broker.selectedCollLiabilities='1/4 RDC';
            test_broker.SelectedClassification.add('typo');
            test_broker.loggedInUsrName=GardTestData.clientUser.Name;
            test_broker.dateOutput='05.02.2015';
            test_broker.assetFinal = null;
            test_broker.selectedClient = GardTestData.clientAcc.id;
            test_broker.ObjName = string.valueof(GardTestData.test_object_1st.name);
            test_broker.FindBy='objname';           
            test_broker.searchByImoObjectName();
            test_broker.chkAccess();
            test_broker.assetFinal = null;
            test_broker.selectedClient = GardTestData.clientAcc.id;
            test_broker.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c);
            test_broker.FindBy='imo';
            test_broker.searchByImoObjectName();
            test_broker.dateOutput='05.02.2015';
            test_broker.dateOutputForReview='05.02.2015';               
            test_broker.setObjectSelection();
            test_broker.saveRecord();
            test_broker.uwf.id=GardTestData.brokerCase.id;
            test_broker.loggedInAccountId = GardTestData.brokerAcc.Id;
            test_broker.selectedCollLiabilities = '0 RDC';
            test_broker.selectedIncludingFFO = 'yes';
            test_broker.saveRecord();
            //  test_broker.uwf=new uwform__c();
            // test_broker.uwf=new uwform__c();
            //test_broker.grosstonnage=34567;
            test_broker.goNext();
            test_broker.clearRecord();
            test_broker.saveRecordForAddlInfo();
            //test_broker.saveRecordForReview();
            test_broker.uwf.id=GardTestData.brokerCase.id;
            test_broker.saveRecordForAddlInfo();
            test_broker.uwf.id=GardTestData.brokerCase.id;
            test_broker.saveRecordForReview();
            test_broker.clearRecordForAddlInfo();
            test_broker.goPrevForAddlInfo();
            test_broker.cancelRecord();
            test_broker.goPrevForReview();
            test_broker.goNextForAddlInfo();
            test_broker.getCName();
            test_broker.sendToForm();
            test_broker.sendAckMail();
            test_broker.sendNotiMail();
            test_broker.strSelected=GardTestData.clientAcc.id+';'+GardTestData.clientAcc.id;
            test_broker.fetchSelectedClients();
            test_broker.strGlobalClient=GardTestData.clientAcc.id+';'+GardTestData.clientAcc.id;
            test_broker.fetchGlobalClients();
            test_broker.uwf.id=GardTestData.brokerCase.id;
            test_broker.uwf=new Case();
            test_broker.saveRecord();
            test_broker.selectedGlobalClient.clear();       
            test_broker.setGlobalClientIds.clear();     
            test_broker.setGlobalClientIds = null;      
            test_broker.selectedGlobalClient = null;        
            test_broker.loggedInAccountId = null;       
            test_broker.getClientLst();
            
            Case updtUWF = [SELECT Id,short_description_vessel__c,Other_statutory_certification__c,personnel_on_board__c,
                            Nationality_of_officer__c,CGL_Offshore_Cover_PI__c,Nationality_of_crew__c,X0_RDC__c,X4_4_RDC__c,
                            Name_Form_Period_Of_Charter__c,Co_assureds_capacity__c,Name_on_Premium_Invoice__c,Country_prefix__c,
                            Flag__c,tonnage__c,Number_of_officers__c,Port_Of_Registry__c,Year_built__c,Signal_Letters_Call_sign__c,
                            Classification_society__c,Limit_of_Cover_USD__c,Number_of_crew__c,X1_4_RDC__c,
                            Registered_owner_s_name_and_address__c,Name_of_Assured_Member__c,Mortgagee_Name_and_Address__c,
                            VAT_number__c,Imo_Lloyds_No__c,Object_name__c FROM Case WHERE Id =: GardTestData.brokerCase.Id];
            updtUWF.short_description_vessel__c = 'text';
            updtUWF.Other_statutory_certification__c = 'text';
            updtUWF.personnel_on_board__c = 'text';
            updtUWF.Nationality_of_officer__c = 'India';
            updtUWF.CGL_Offshore_Cover_PI__c = 'Yes';
            updtUWF.Nationality_of_crew__c = 'Australia';
            updtUWF.X0_RDC__c = 'Yes';
            updtUWF.X4_4_RDC__c = 'Yes';
            updtUWF.Name_Form_Period_Of_Charter__c = 'text';
            updtUWF.Co_assureds_capacity__c = 'text';
            updtUWF.Name_on_Premium_Invoice__c = 'Text Name';
            updtUWF.Country_prefix__c = 'IND';
            updtUWF.Flag__c = null;
            updtUWF.tonnage__c = null;
            updtUWF.Port_Of_Registry__c = null;
            updtUWF.Year_built__c = null;
            updtUWF.Signal_Letters_Call_sign__c = null;
            updtUWF.Classification_society__c = null;
            updtUWF.Number_of_officers__c = null;
            updtUWF.Limit_of_Cover_USD__c = null;
            updtUWF.Number_of_crew__c = null;
            updtUWF.X1_4_RDC__c = null;
            updtUWF.Registered_owner_s_name_and_address__c = null;
            updtUWF.Name_of_Assured_Member__c = null;
            updtUWF.Mortgagee_Name_and_Address__c = null;
            updtUWF.Imo_Lloyds_No__c = null;
            updtUWF.VAT_number__c = null;
            updtUWF.Object_name__c = null;
            //update updtUWF ;
            test_broker.dateOutput  = null;
            //test_broker.uwf.id=GardTestData.brokerCase.id;
            //test_broker.saveRecordForReview();
            
        }
        Test.stopTest();
    }
    public static testMethod void UWRMOUCtrlClient(){
        UWRMOUCtrlTestData();
        Test.startTest();
        System.runAs(GardTestData.clientUser)  
        {
            UWRMOUCtrl test_client=new UWRMOUCtrl();
            test_client.confirmationCheckbox=true;
            test_client.CountryPrefix='91';
            test_client.IMONotKnown=true;
            test_client.selectedObj=GardTestData.test_object_1st.id;
            List<SelectOption> clientOptions =  test_client.getClientLst();
            List<SelectOption> flag=test_client.getFlagList();
            List<SelectOption> port=test_client.getPortList();
            List<SelectOption> classification=test_client.getClassificationList();
            List<SelectOption> CollLiabilities=test_client.getCollLiabilities();  
            system.assertequals(clientOptions.size()>0,true,true);
            system.assertequals(flag.size()>0,true,true); 
            system.assertequals(port.size()>0,true,true); 
            system.assertequals(classification.size()>0,true,true); 
            system.assertequals(CollLiabilities.size()>0,true,true);   
            test_client.SelectedClassification.add('typo');
            test_client.loggedInUsrName=GardTestData.clientUser.Name;
            test_client.dateOutput='05.02.2015';
            test_client.dateOutputForReview='05.02.2015';
            test_client.selectedCollLiabilities='0 RDC';
            test_client.selectedIncludingFFO = 'no';
            test_client.assetFinal = null;
            test_client.selectedClient = GardTestData.clientAcc.id;
            test_client.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c); 
            test_client.FindBy='imo';          
            // test_client.searchByImoObjectName();
            test_client.chkAccess();
            test_client.assetFinal = null;
            test_client.selectedClient = GardTestData.clientAcc.id;
            test_client.ObjName = string.valueof(GardTestData.test_object_1st.name);  
            test_client.FindBy='objname';
            test_client.searchByImoObjectName();
            test_client.setGlobalClientIds = new List<String>();        
            test_client.setGlobalClientIds = null;      
            test_client.selectedGlobalClient = null;        
            test_client.getClientLst();
            Set<Id> setAssetIdsPI = new Set<Id>();
            setAssetIdsPI = null;
            test_client.FindBy='objname';
            test_client.searchByImoObjectName();
            
            //test_client.goNext();
            
        }
        Test.stopTest();
    }
}