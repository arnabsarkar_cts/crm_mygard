@isTest

public class TestEmailSender{
        public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
        public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;

//Public list<EmailTemplate> Emaillist = new list<EmailTemplate>();
//Public EmailTemplate e1,e2,e3,e4,e5,e6;

@future

public static void inserttemp1(){
 list<EmailTemplate> Emaillist = new list<EmailTemplate>();

 EmailTemplate  e1 = new EmailTemplate(Name = 'MyGard-Extranet Feedback-Acknowledgement', body = 'HEADER] Thank you for contacting Gard. Your feedback has been received.   Below is a summary of the details submitted on MyGard:  Key feedback issue/area: [AREA] Comments: [COMMENTS]    Main reason for visiting MyGard today: [REASON] Overall site rating: [SITE_RATING]  Objective accomplished through MyGard: [OBJECTIVES_ACCOMPLISHED]    Any other comments: [OTHER_COMMENTS]    Preferred method of contact:    Phone: [PREF_PHONE] Email: [PREF_EMAIL] [SIGNATURE] [FOOTER]');

Emaillist.add(e1); 

 EmailTemplate  e2 = new EmailTemplate(Name = 'MyGard-Contact Me-Acknowledgement', body = '[HEADER] Thank you for contacting Gard. Your request for Gard to contact you has been registered. Below is a summary of the details submitted on MyGard: Area: [AREA]    Client: [CLIENT]    Cover: [COVER]  Claim type: [CLAIMTYPE] Subject: [SUBJECT]  Preferred method of contact:    Phone: [PHONE]  Email: [EMAIL]  [SIGNATURE] [FOOTER]');
Emaillist.add(e2 );

 EmailTemplate  e3 = new EmailTemplate(Name = 'MyGard-Start Sharing-Acknowledgement', body = '[HEADER] You are now sharing all your records with [CLIENTNAME]. All users of MyGard from [CLIENTNAME] are now able to see all records, e.g. claims, covers etc., related to agreements brokered by you. If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
Emaillist.add(e3 );

 EmailTemplate  e4 = new EmailTemplate(Name = 'MyGard-Stop Sharing-Acknowledgement', body ='[HEADER] You are no longer sharing your records with [CLIENTNAME]. As a result, none of the users of MyGard from [CLIENTNAME] are able to see records, e.g. claims, covers etc., related to agreements brokered by you. If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
Emaillist.add(e4 );

 EmailTemplate  e5 = new EmailTemplate(Name = 'MyGard-Request To Mark As Admin-Acknowledgement',body ='[HEADER] Thank you for submitting your request for a change of administrator user. As per your request, the administrator right will be transferred to following user:   First Name: [FIRSTNAME] Last Name: [LASTNAME]   If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
Emaillist.add(e5 );

 EmailTemplate  e6 = new EmailTemplate(Name = 'MyGard-User access revokation-Acknowledgement',body = '[HEADER] Thank you for contacting Gard and we have registered your request. Based on your request, [USERNAME] does not have access to MyGard anymore. If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
Emaillist.add(e6 );

 EmailTemplate  e7 = new EmailTemplate(Name = 'Acknowledgement - Change in Primary Contact',body ='[HEADER] Based on your request, [USERNAME] is now the new primary contact for your organisation. If you have any queries, please contact us at MyGard.support@gard.no. [SIGNATURE] [FOOTER]');
Emaillist.add(e7 );

EmailTemplate  e8 = new EmailTemplate(Name = 'MyGard-Request For Revoking People Claim Access-Acknowledgement', body ='[HEADER] Based on your request, [USERNAME] will now not be able to see the restricted claim description text in MyGard whenever the claim type falls within “Crew, Passenger, Person(s) other than crew or passenger”. If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
Emaillist.add(e8);

EmailTemplate  e9 = new EmailTemplate(Name = 'MyGard-Request For Granting People Claim Access-Acknowledgement', body = '[HEADER] Based on your request, [USERNAME] will now be able to see the restricted claim description text in MyGard whenever the claim type falls within “Crew, Passenger, Person(s) other than crew or passenger”. If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
Emaillist.add(e9);

EmailTemplate  e10 = new EmailTemplate(Name = 'MyGard-Request For New User Access-Acknowledgement', body = '[HEADER] Thank you for contacting Gard and we have registered your request. Below is a summary of the details submitted on MyGard:  First Name: [FIRSTNAME] Last Name: [LASTNAME]   Phone: [PHONE]  Email address: [EMAIL] Comments: [COMMENTS] If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
Emaillist.add(e10);


EmailTemplate e11 = new EmailTemplate(Name = 'MyGard-Service Request File Upload/Case Comment', body = '[HEADER] A new [File/Comment] has been added to your Case. Case Reference Number: {!Case.CaseNumber} Comment added by MyGard: [caseComment] Please find the details of the case here - [Case URL] If you have any queries, please contact us at MyGard.support@gard.no. [SIGNATURE] [FOOTER]');
Emaillist.add(e11);

//EmailTemplate  e11 = new EmailTemplate(Name = 'MyGard-Request To Revoke Admin Access-Notification', body = '[HEADER] Thank you for contacting Gard and we have registered your request. Below is a summary of the details submitted on MyGard:  First Name: [FIRSTNAME] Last Name: [LASTNAME]   Phone: [PHONE]  Email address: [EMAIL] Comments: [COMMENTS] If you have any queries, please contact us at mygard.support@gard.no. [SIGNATURE] [FOOTER]');
//Emaillist.add(e11);
    
//EmailTemplate  e11 = new EmailTemplate(Name = 'MyGard-Request For New User Access-Acknowledgement');
//Emaillist.add(e11);


insert Emaillist;

}

public static testmethod void testemail(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 

Gardtestdata test_rec = new gardtestdata();
test_rec.commonrecord();

CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c ='test123@gmail.com');
insert cvar;

EmailServicesAddress testobj1= new EmailServicesAddress(LocalPart = 'ownergard',IsActive = true); 
//insert testobj1;

OrgWideEmailAddress owea = new OrgWideEmailAddress(DisplayName = 'no-reply@gard.no', Address = 'no-reply@gard.no');
//insert owea;

 Customer_Feedback__c fdbck = new Customer_Feedback__c(Key_feedback_issue_area__c = 'Availability' ,
                                                       Did_you_accomplish_what_you_wanted_to__c = 'yes',
                                                       What_was_your_main_reason_for_visiting__c = 'testclass' ,
                                                       Overall_site_rating__c = 'Excellent' ,
                                                       //Any_Other_Comments__c = 'testcomment',
                                                       Contact_preference_Phone__c = true,
                                                       Contact_preference_email__c = true );
                                                       
 insert fdbck;
 Customer_Feedback__c fdbck1 = new Customer_Feedback__c(Key_feedback_issue_area__c = 'MyGard bugs or problems' ,
                                                       Did_you_accomplish_what_you_wanted_to__c = 'yes',
                                                       What_was_your_main_reason_for_visiting__c = 'testclass' ,
                                                       Overall_site_rating__c = 'Excellent' ,
                                                       Any_Other_Comments__c = 'testcomment',
                                                       Contact_preference_Phone__c = true,
                                                       Contact_preference_email__c = true );
                                                       
 insert fdbck1;
 Customer_Feedback__c fdbck12 = new Customer_Feedback__c(Key_feedback_issue_area__c = 'MyGard experience' ,
                                                       Did_you_accomplish_what_you_wanted_to__c = 'yes',
                                                       What_was_your_main_reason_for_visiting__c = 'testclass' ,
                                                       Overall_site_rating__c = 'Excellent' ,
                                                       Any_Other_Comments__c = 'testcomment',
                                                       Contact_preference_Phone__c = true,
                                                       Contact_preference_email__c = true );
                                                       
 insert fdbck12;    
  Feedback_mail_id__c f1 = new Feedback_mail_id__c(name = 'Feedback notification',Email_address__c = 't1@gmail.com');
  insert f1;


System.runAs(gardtestdata.brokerUser)
{
    Test.startTest();
    EmailSender newobj = new emailsender();
    string[] toadress = new string[]{'ownergard@gmail.com'};
    toadress.add('test1');
    toadress.add('test2');
    toadress.add('test3');
    toadress.add('test4');
    toadress.add('test5');
    toadress.add('test6');
    toadress.add('test7');
    toadress.add('test8');
    toadress.add('test9');
    toadress.add('test10');
    newobj.gardExtranetAdministrator='';
    newobj.gardExtranetAdministratorEmail='';
    newobj.newUserInfoFirstName='';
    newobj.newUserInfoLastName='';
    newobj.newUserInfoPhone='';
    newobj.newUserInfoMobile='';
    newobj.newUserInfoEmail='';
    //newobj.sendCopyToExtranetAdmin='';
    newobj.existingContactId='';
    newobj.clientBrokerOrganization='';
    newobj.clientBrokerAdminFirstName='';
    newobj.clientBrokerAdminLastName='';
    newobj.clientBrokerAdminFullName='';
    newobj.clientBrokerAdminPhone='';
    newobj.clientBrokerAdminMobile='';
    newobj.clientBrokerAdminEmail='';
    newobj.caseLink='';
    newobj.removeUserInfoName='';
    newobj.CaseId='';
    newobj.clientBrokerOldAdminName='';
    newobj.clientBrokerNewAdminName='';
    newobj.logo='';
    newobj.contactMe_recipentName='';
    newobj.contactMe_loggedInUser='';
    newobj.contactMe_loggedInUserOrganization='';
    newobj.contactMe_area='';
    newobj.contactMe_client='';
    newobj.contactMe_cover='';
    newobj.contactMe_claimType='';
    newobj.contactMe_subject='';
    newobj.clientOrganization='';
    newobj.contactMeRecipientEmail='';
    newobj.registerClaim_recipientEmail='';
    newobj.registerClaim_recipientName='';
    newobj.registerClaim_caseID='';
           newobj.registerClaim_myClaimId='';
           newobj.registerClaim_userType='';
           newobj.registerClaim_clientName='';
           newobj.registerClaim_referencenumber='';
           newobj.registerClaim_objectName='';
           newobj.registerClaim_eventDate='';
           newobj.registerClaim_place='';
           newobj.registerClaim_coverName='';
           newobj.registerClaim_claimType='';
           newobj.registerClaim_claimDescription='';
           newobj.registerClaim_name='';
           newobj.registerClaim_rank='';
           newobj.registerClaim_nationality='';
           newobj.registerClaim_claimDetail='';
           newobj.registerClaim_portOfEmbarkation='';
           newobj.registerClaim_dateOfEmbarkation='';
           newobj.registerClaim_numberofStowaways='';
            newobj.revokedUserName='';
            newobj.editObject_emailBody='';
            newobj.editObject_fromemail='';
            newobj.feedbackId='';
            newobj.feedbackComments='';
            newobj.feedbackReason='';
            newobj.feedbackRating='';
            newobj.feedbackObjective='';
            newobj.feedbackArea='';
            newobj.caseInfo = 'File';
    
   newobj.createAndSendEmail(Gardtestdata.brokerCase.id,'MyGard-Service Request File Upload/Case Comment','',toadress,Gardtestdata.brokerUser.id);
   newobj.caseInfo = 'Case Comment';
   newobj.caseComment ='Test comment';
  
   newobj.createAndSendEmail(Gardtestdata.brokerCase.id,'MyGard-Service Request File Upload/Case Comment','',toadress,Gardtestdata.brokerUser.id);
   newobj.createAndSendEmail(fdbck.id, 'MyGard-Extranet Feedback-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
   newobj.createAndSendEmail(fdbck1.id, 'MyGard-Extranet Feedback-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
  newobj.createAndSendEmail(fdbck12.id, 'MyGard-Extranet Feedback-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
    newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'MyGard-Contact Me-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
     newobj.createAndSendEmail(Gardtestdata.brokerContract_1st.id, 'MyGard-Start Sharing-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
    newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'MyGard-Request To Revoke Admin Access-Notification', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
    newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'MyGard-Request To Mark As Admin-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
    //newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'MyGard-Request To Mark As Admin-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
    newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'MyGard-User access revokation-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
    //newobj.createAndSendEmail(Gardtestdata.brokerContact.id, 'Acknowledgement - Change in Primary Contact', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
    //newobj.createAndSendEmail(Gardtestdata.brokerContact.id, 'MyGard-Request For Revoking People Claim Access-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
    //newobj.createAndSendEmail(Gardtestdata.brokerContact.id, 'MyGard-Request For Granting People Claim Access-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
    //newobj.createAndSendEmail(Gardtestdata.brokercontract_1st.id, 'MyGard-Stop Sharing-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
    System.assertEquals(newobj.contactMe_emailPreference , null); 
  //  newobj.createAndSendEmail(Gardtestdata.brokerCase.id, 'MyGard-Request For New User Access-Acknowledgement', Gardtestdata.brokerContact.id, toadress , Gardtestdata.brokerUser.id);
  //  newobj.send('contactme');
  //newobj.send('registerclaim_copytoRequestor');
 //   newobj.send('editObject');
 //   newobj.send('editObject');
 //   newobj.send('userFeedback');
    Test.stopTest();
 
 }

   
  
  
 }
 }