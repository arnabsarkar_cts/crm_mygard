// Test class for COFR_RequestCtrl.
@isTest
public class TestCOFR_RequestCtrl
{
//Variable Declaration............................................
    Public static List<Account> accountList=new List<Account>();
    Public static List<Contact> contactList=new List<Contact>();
    Public static List<User> userList=new List<User>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    public static list<Asset> assetTest=new list<Asset>();
    Public static List<Object__c> objectList=new List<Object__c>();
    public static List<Contract> contractList=new List<Contract>();
    Public static Extranet_global_Client__c global_Client, global_Client_1;
    Public static List<Case> crList = new List<Case>();
    Public static List<Case> caseList =new List<Case>();
    Public static AccountToContactMap__c map_Broker_Test, map_Client_Test;
    Public static Account_Contact_Mapping__c accMap_1st, accMap_2nd ;
    Public static list<Account_Contact_Mapping__c> accMap_List = new list<Account_Contact_Mapping__c>();  
    public static Agreement_set__c agrSet = new Agreement_set__c();
    public static Broker_share__c brokerShr = new Broker_share__c();
    public static set<String> accountIdSet = new set<String>();
    public static COFR_Request_Email_CS__c COFRReqEmail ; 
    ///////*******Variables for removing common literals********************//////////////
    public static string enUsrStr =  'en_US';
    public static string mailngStateStr =   'London';
    public static string expDateStr =   '2016-02-01';
    public static string piStr =   'P&I';
    public static string incptnDateStr =   '2012-01-01';
    public static BlueCardTeam__c testBlueCardTeam;
    /////**********************end**************************/////
//Test Method.........................................   
    public static void cover(){
//Create test data....................................   
         //correspondant contacts Custom Settings
        List<ContactSubscriptionFields__c> conFieldsList = new List<ContactSubscriptionFields__c>();
        ContactSubscriptionFields__c conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        ContactSubscriptionFields__c conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        ContactSubscriptionFields__c conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList;
        GardTestData gtdInstance = new GardTestData();
        gtdInstance.commonRecord();        
        agrSet.agreement_set_type__c = 'Test data for broker share';
        agrSet.shared_by__c = GardTestData.brokerAcc.Id;// brokerAcc.ID;
        agrSet.shared_with__c = GardTestData.brokerAcc.Id;//brokerAcc.ID;
        agrSet.status__c = 'Active';
        insert agrSet;
        
        system.assertEquals(agrSet.agreement_set_type__c , 'Test data for broker share');
        brokerShr.Active__c = true;
        brokerShr.Agreement_set_id__c = agrSet.ID;
        brokerShr.contract_external_id__c = String.valueOf(GardTestData.brokercontract_1st.id);// brokerContract.ID);
        brokerShr.Contract_id__c = GardTestData.brokercontract_1st.id;//brokerContract.ID;
        brokerShr.shared_to_client__c = true;
        system.assertEquals(brokerShr.shared_to_client__c , true);
        insert brokerShr;  
        COFRReqEmail = new COFR_Request_Email_CS__c(name = 'Notification_Email_ID' , value__c = 'test123@gmail.com');
        insert COFRReqEmail ;
        testBlueCardTeam = new BlueCardTeam__c(
                                                   Name = 'BlueCardTeam',
                                                   Value__c = 'nils.petter.nilsen@testgard.no'
                                               );
        insert testBlueCardTeam;
    }
      public static testMethod void Brokercover(){
          cover();
        Test.startTest();
        System.runAs(GardTestData.brokerUser){// brokerUser){
        
            COFR_RequestCtrl COFR_RequestCtrlBroker = new COFR_RequestCtrl();
            MyGardHelperCtrl.setExtranetGobalClients();
            MyGardHelperCtrl.fetchSelectedClientsHelper(''+(GardTestData.AccountList[0].id));// accountList[1].id));
            MyGardHelperCtrl.fetchUWRFormHelper(GardTestData.brokerContact.id);
            MyGardHelperCtrl.DummyCovers();
            COFR_RequestCtrlBroker.prvUrl = 'ObjectDetail';
            COFR_RequestCtrlBroker.fetchGlobalClients();
            COFR_RequestCtrlBroker.populateClientOption();
            COFR_RequestCtrlBroker.getCOFR_GuarantorsOptions();
            COFR_RequestCtrlBroker.accountIdSet = new set<String>();
            COFR_RequestCtrlBroker.accountIdSet.add(GardTestData.brokerAcc.ID);
            COFR_RequestCtrlBroker.fetchGlobalClients();
            COFR_RequestCtrlBroker.FindBy = 'imo';
            COFR_RequestCtrlBroker.objSearchString = '1123wer';
            COFR_RequestCtrlBroker.selectedClientId = GardTestData.brokerAcc.id;
            COFR_RequestCtrlBroker.searchByImoObjectName();
            COFR_RequestCtrlBroker.selectedObj=  GardTestData.test_object_1st.id;
            COFR_RequestCtrlBroker.populateObjNameNImo();
            COFR_RequestCtrlBroker.applicantAddress = 'testAddress';
            COFR_RequestCtrlBroker.selectedGuarantor= GardTestData.brokerAcc.Id;
            COFR_RequestCtrlBroker.imoNumber= 'test1511'; 
            COFR_RequestCtrlBroker.selectedObj=  GardTestData.test_object_1st.id;//Object1.Id;//
            String guid = GardTestData.test_object_1st.GUID__c;
            COFR_RequestCtrlBroker.objName = 'testObjName';
            COFR_RequestCtrlBroker.selectedClientId = GardTestData.brokerAcc.id;
            COFR_RequestCtrlBroker.saveCase();
            COFR_RequestCtrlBroker.onSubmit();
            COFR_RequestCtrlBroker.onCancel();
            COFR_RequestCtrlBroker.prvUrl = 'https://uat-mygard.cs87.force.com/mygard/MyObjectDetails?objid='+guid;//fc0a5fdb-5b70-a4ed-8917';
            COFR_RequestCtrlBroker.selectedObjid = GardTestData.test_object_1st.id;
            accountIdSet.add(GardTestData.brokerAcc.id);
            COFR_RequestCtrlBroker.accountIdSet = accountIdSet;
            //GardTestData.test_object_1st.GUID__c = 'fc0a5fdb-5b70-a4ed-8917';
            COFR_RequestCtrlBroker.sendType();
            COFR_RequestCtrlBroker.popUpClose();  
            COFR_RequestCtrlBroker.setGlobalClientIds = new List<String>();
            COFR_RequestCtrlBroker.populateClientOption();
        }
          Test.stopTest();
      }
    public static testMethod void Clientcover(){
        cover();
        Test.startTest();
        System.runAs(GardTestData.clientUser){
        
            COFR_RequestCtrl COFR_RequestCtrlClient = new COFR_RequestCtrl();
            COFR_RequestCtrlClient.prvUrl = 'ObjectDetail';
            COFR_RequestCtrlClient.fetchGlobalClients();
            COFR_RequestCtrlClient.populateClientOption();
            COFR_RequestCtrlClient.getCOFR_GuarantorsOptions();
            COFR_RequestCtrlClient.fetchGlobalClients();
            COFR_RequestCtrlClient.searchByImoObjectName();
            COFR_RequestCtrlClient.FindBy = 'imo';
            system.debug('findBy***************'+COFR_RequestCtrlClient.FindBy );
            COFR_RequestCtrlClient.populateObjNameNImo();
            system.debug('findBy***************'+COFR_RequestCtrlClient.FindBy );
            //COFR_RequestCtrlClient.saveCase();
            //COFR_RequestCtrlClient.onSubmit();
            COFR_RequestCtrlClient.onCancel();
            COFR_RequestCtrlClient.prvUrl = 'https://uat-mygard.cs87.force.com/mygard/MyObjectDetails?objid=fc0a5fdb-5b70-a4ed-8917';
            COFR_RequestCtrlClient.selectedObjid = GardTestData.test_object_1st.id+'#'+GardTestData.test_object_1st.id;
            accountIdSet.add(GardTestData.ClientAcc.id);
            COFR_RequestCtrlClient.accountIdSet = accountIdSet;
            COFR_RequestCtrlClient.sendType();
            COFR_RequestCtrlClient.popUpClose();
            COFR_RequestCtrlClient.setGlobalClientIds = new List<String>();
            COFR_RequestCtrlClient.populateClientOption();
        }  
        Test.stopTest();
    } 
}