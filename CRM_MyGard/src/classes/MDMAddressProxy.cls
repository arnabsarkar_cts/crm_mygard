/**************************************************************************
* Author  : Phil Spenceley
* Company : cDecisions
* Date    : 21/10/2013
***************************************************************************/
    
/// <summary>
///  This proxy class provides methods to access the MDM Address Service
/// </summary>
public without sharing class MDMAddressProxy extends MDMProxy {

	private Set<String> getAccountFields(String requestName, String addressType) {
		Set<String> fields = new Set<String>();
		for (MDM_Service_Fields__c field : MDM_Service_Fields__c.getAll().values()) {
			if (addressType.equalsIgnoreCase(field.address_type__c)  && requestName.equalsIgnoreCase(field.Request_Name__c) && 'Account'.equalsIgnoreCase(field.Source_Object__c) && 'Field'.equalsIgnoreCase(field.Type__c)) {
				fields.add(field.Field_Name__c.toLowerCase());
			}
		}
		return fields;
	}
	private Account getAccountById(id accountId, String requestName, String addressType) {
		return (Account)Database.query(buildSoqlRequest(getAccountFields(requestName, addressType), 'Account', accountId));
    }
    private List<Account> getAccountsByIds(List<id> accountIds, String requestName, String addressType) {
        return (List<Account>)Database.query(buildSoqlRequest(getAccountFields(requestName, addressType), 'Account', accountIds));
    }
	
	private MDMUpsertAddressResponse MDMUpsertAddresses(MDMUpsertAddressRequest request) {
        DOM.Document responseDoc = sendRequest(request);
		MDMUpsertAddressResponse response = new MDMUpsertAddressResponse();
		try {
			response.Deserialize(
	            responseDoc
	                .getRootElement()
	               	.getChildElement(SOAP_Body, SOAPNS)
		            .getChildElement(MDMChangeResponse_Label, MDMChangeResponse_Namespace)
	        );
		} catch (Exception ex) {
			throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMAddressProxy.MDMUpsertAddressResponse', ex);
		}
		return response;
    }
	private MDMDeleteAddressResponse MDMDeleteAddresses(MDMDeleteAddressRequest request) {
        DOM.Document responseDoc = sendRequest(request);
		MDMDeleteAddressResponse response = new MDMDeleteAddressResponse();
		try {
			response.Deserialize(
	            responseDoc
	                .getRootElement()
	               	.getChildElement(SOAP_Body, SOAPNS)
		            .getChildElement(MDMChangeResponse_Label, MDMChangeResponse_Namespace)
	        );
		} catch (Exception ex) {
			throw new MDMDeserializationException(responseDoc, 'Failed to deserialize MDMAddressProxy.MDMDeleteAddressResponse', ex);
		}
		return response;
    }
	
	public void MDMUpsertBillingAddress(Map<id, Account> upsertAccounts) {
		MDMUpsertBillingAddressRequest request = new MDMUpsertBillingAddressRequest();
		Map<id, Account> accounts = new Map<id, Account>(getAccountsByIds(new List<id> (upsertAccounts.keySet()), request.getName(), request.getAddressType()));
		
		try {
			request.accounts = accounts.values();
            MDMUpsertAddressResponse response = MDMUpsertAddresses(request);
			for (MDMResponse mdmResponse : response.MDMResponses) {
				System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
				if (mdmResponse.Success) {
	                accounts.get(mdmResponse.Id).Billing_Address_Sync_Status__c = 'Synchronised';
	            } else {
	                accounts.get(mdmResponse.Id).Billing_Address_Sync_Status__c = 'Sync Failed';
	            }	
			}
			
			update accounts.values(); 
			
        } catch (MDMDeserializationException ex) {
			Logger.LogException('MDMAddressProxy.MDMUpsertBillingAddressAsync (@Future, ex)', ex, ex.xmlString);
			if (accounts != null) {
				SetBillingAddressSyncFailed(accounts.values(), 'MDMAddressProxy.MDMUpsertBillingAddressAsync (@Future, innerEx)');
			}
        } catch (Exception ex) {
            Logger.LogException('MDMAddressProxy.MDMUpsertBillingAddressAsync (@Future, ex)', ex);
			if (accounts != null) {
				SetBillingAddressSyncFailed(accounts.values(), 'MDMAddressProxy.MDMUpsertBillingAddressAsync (@Future, innerEx)');
			}
        }
	}
	
	public void MDMUpsertShippingAddress(Map<id, Account> upsertAccounts) {
		MDMUpsertShippingAddressRequest request = new MDMUpsertShippingAddressRequest();
		Map<id, Account> accounts = new Map<id, Account>(getAccountsByIds(new List<id> (upsertAccounts.keySet()), request.getName(), request.getAddressType()));
		
		try {
			request.accounts = accounts.values();
            MDMUpsertAddressResponse response = MDMUpsertAddresses(request);
			for (MDMResponse mdmResponse : response.MDMResponses) {
				System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
				if (mdmResponse.Success) {
	                accounts.get(mdmResponse.Id).Shipping_Address_Sync_Status__c = 'Synchronised';
	            } else {
	                accounts.get(mdmResponse.Id).Shipping_Address_Sync_Status__c = 'Sync Failed';
	            }	
			}
			
			update accounts.values(); 
			
        } catch (MDMDeserializationException ex) {
			Logger.LogException('MDMAddressProxy.MDMUpsertShippingAddressAsync (@Future, ex)', ex, ex.xmlString);
			if (accounts != null) {
				SetShippingAddressSyncFailed(accounts.values(), 'MDMAddressProxy.MDMUpsertShippingAddressAsync (@Future, innerEx)');
			}
        } catch (Exception ex) {
            Logger.LogException('MDMAddressProxy.MDMUpsertShippingAddressAsync (@Future, ex)', ex);
			if (accounts != null) {
				SetShippingAddressSyncFailed(accounts.values(), 'MDMAddressProxy.MDMUpsertShippingAddressAsync (@Future, innerEx)');
			}
        }
	}
	
	public void MDMDeleteBillingAddress(Map<id, Account> deleteAccounts) {
		MDMDeleteBillingAddressRequest request = new MDMDeleteBillingAddressRequest();
		Map<id, Account> accounts = new Map<id, Account>(getAccountsByIds(new List<id> (deleteAccounts.keySet()), request.getName(), request.getAddressType()));
		
		try {
			request.accounts = accounts.values();
            MDMDeleteAddressResponse response = MDMDeleteAddresses(request);
			for (MDMResponse mdmResponse : response.MDMResponses) {
				System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
				if (mdmResponse.Success) {
	                accounts.get(mdmResponse.Id).Billing_Address_Sync_Status__c = 'Synchronised';
	            } else {
	                accounts.get(mdmResponse.Id).Billing_Address_Sync_Status__c = 'Sync Failed';
	            }	
			}
			
			update accounts.values(); 
			
        } catch (MDMDeserializationException ex) {
			Logger.LogException('MDMAddressProxy.MDMDeleteBillingAddressAsync (@Future, ex)', ex, ex.xmlString);
			if (accounts != null) {
				SetBillingAddressSyncFailed(accounts.values(), 'MDMAddressProxy.MDMDeleteBillingAddressAsync (@Future, innerEx)');
			}
        } catch (Exception ex) {
            Logger.LogException('MDMAddressProxy.MDMDeleteBillingAddressAsync (@Future, ex)', ex);
			if (accounts != null) {
				SetBillingAddressSyncFailed(accounts.values(), 'MDMAddressProxy.MDMDeleteBillingAddressAsync (@Future, innerEx)');
			}
        }
	}
	
	public void MDMDeleteShippingAddress(Map<id, Account> deleteAccounts) {
		MDMDeleteShippingAddressRequest request = new MDMDeleteShippingAddressRequest();
		Map<id, Account> accounts = new Map<id, Account>(getAccountsByIds(new List<id> (deleteAccounts.keySet()), request.getName(), request.getAddressType()));
		
        try {
			request.accounts = accounts.values();
            MDMDeleteAddressResponse response = MDMDeleteAddresses(request);
			for (MDMResponse mdmResponse : response.MDMResponses) {
				System.debug('*** MDMResponse:[ id: + ' + mdmResponse.id + ', success: ' + string.valueOf(mdmResponse.success) + ']');
				if (mdmResponse.Success) {
	                accounts.get(mdmResponse.Id).Shipping_Address_Sync_Status__c = 'Synchronised';
	            } else {
	                accounts.get(mdmResponse.Id).Shipping_Address_Sync_Status__c = 'Sync Failed';
	            }	
			}
			
			update accounts.values(); 
			
        } catch (MDMDeserializationException ex) {
			Logger.LogException('MDMAddressProxy.MDMDeleteShippingAddressAsync (@Future, ex)', ex, ex.xmlString);
			if (accounts != null) {
				SetShippingAddressSyncFailed(accounts.values(), 'MDMAddressProxy.MDMDeleteShippingAddressAsync (@Future, innerEx)');
			}
        } catch (Exception ex) {
            Logger.LogException('MDMAddressProxy.MDMDeleteShippingAddressAsync (@Future, ex)', ex);
			if (accounts != null) {
				SetShippingAddressSyncFailed(accounts.values(), 'MDMAddressProxy.MDMDeleteShippingAddressAsync (@Future, innerEx)');
			}
        }
	}
	
	@future (callout = true)
    public static void MDMUpsertBillingAddressAsync(list<id> accountIds) {
		MDMAddressProxy proxy = new MDMAddressProxy();
		MDMUpsertBillingAddressRequest request = new MDMUpsertBillingAddressRequest();
        Map<id, Account> accounts = new Map<id, Account>(proxy.getAccountsByIds(accountIds, request.getName(), request.getAddressType()));
		proxy.MDMUpsertBillingAddress(accounts);
	}
	@future (callout = true)
    public static void MDMUpsertShippingAddressAsync(list<id> accountIds) {
		MDMAddressProxy proxy = new MDMAddressProxy();
		MDMUpsertShippingAddressRequest request = new MDMUpsertShippingAddressRequest();
        Map<id, Account> accounts = new Map<id, Account>(proxy.getAccountsByIds(accountIds, request.getName(), request.getAddressType()));
		proxy.MDMUpsertShippingAddress(accounts);
	}
	@future (callout = true)
    public static void MDMDeleteBillingAddressAsync(list<id> accountIds) {
		MDMAddressProxy proxy = new MDMAddressProxy();
		MDMDeleteBillingAddressRequest request = new MDMDeleteBillingAddressRequest();
        Map<id, Account> accounts = new Map<id, Account>(proxy.getAccountsByIds(accountIds, request.getName(), request.getAddressType()));
		proxy.MDMDeleteBillingAddress(accounts);
	}
	@future (callout = true)
    public static void MDMDeleteShippingAddressAsync(list<id> accountIds) {
		MDMAddressProxy proxy = new MDMAddressProxy();
		MDMDeleteShippingAddressRequest request = new MDMDeleteShippingAddressRequest();
        Map<id, Account> accounts = new Map<id, Account>(proxy.getAccountsByIds(accountIds, request.getName(), request.getAddressType()));
		proxy.MDMDeleteShippingAddress(accounts);
	}
	
	//Called from an exception to set all sync status to failed
	private static void SetBillingAddressSyncFailed(List<account> accounts, String source) {
		try {
			if (accounts != null) {
				for (Account a : accounts) {
					a.Billing_Address_Sync_Status__c = 'Sync Failed';
				}
				update accounts; 
			}
		} catch (Exception ex) {
			//Simple update, hopefully shouldn't get here.
			Logger.LogException(source, ex);
		}
	}
	
	//Called from an exception to set all sync status to failed
	private static void SetShippingAddressSyncFailed(List<account> accounts, String source) {
		try {
			if (accounts != null) {
				for (Account a : accounts) {
					a.Shipping_Address_Sync_Status__c = 'Sync Failed';
				}
				update accounts; 
			}
		} catch (Exception ex) {
			//Simple update, hopefully shouldn't get here.
			Logger.LogException(source, ex);
		}
	}

	
/// ----------------------------------------------------------------------------
///  -- Sub classes for the request and response messages called above
/// ----------------------------------------------------------------------------
	public virtual class MDMAddressRequest extends MDMRequest {
        //TODO: move to custom setting...
		protected String MDMChangeAddressRequest_Label {
			get {
				return 'changeAddressesRequest';
			}
		}
		protected String MDMChangeAddressRequest_Namespace {
			get {
				return 'http://www.gard.no/mdm/v1_0/mdmmessage';
			}
		}
		protected String MDMChangeAddressRequest_Prefix {
			get {
				return 'mdm';
			}
		}
		
		public virtual override String getName() { return 'changeAddressesRequest'; }
		public virtual String getAddressType() { return 'Billing'; }
		public List<Account> Accounts { get; set; }
		        
        public virtual override DOM.Document getSoapBody() {
            return getSoapBody(this.getName(), this.getAddressType(), MDMChangeAddressRequest_Namespace, MDMChangeAddressRequest_Prefix);
        }
		
        public DOM.Document getSoapBody(string method, String addressType, String namespace, String prefix) {
			system.debug('*** method: '+method);
            DOM.Document body = new DOM.Document();
			DOM.XMLNode root = body.createRootElement(MDMChangeAddressRequest_Label, MDMChangeAddressRequest_Namespace, MDMChangeAddressRequest_Prefix);		

			//Get XML element list from Custom Setting
			List<MDM_Service_Fields__c> serviceFields = [
				SELECT Name, Request_Name__c, Sequence__c, Namespace_Prefix__c, Parent_Node__c, Type__c, XmlLabel__c, Source_Object__c, Field_Name__c, Address_Type__c, Omit_Node_If_Null__c, Strip_Special_Characters__c, Strip_Whitespaces__c 
				FROM MDM_Service_Fields__c 
				WHERE Request_Name__c = :method
				AND Address_Type__c = :addressType
				ORDER BY Sequence__c];
			
            for(Account a : this.Accounts) {
				DOM.XmlNode partnerXml = root.addChildElement(method, namespace, prefix);
				appendSObjectXml(partnerXml, a, serviceFields);
            }
            return body;
        }
    }
    
	public virtual class MDMUpsertAddressRequest extends MDMAddressRequest {
		public override String getName() { return 'upsertAddressRequest'; }
    }
	
	public class MDMUpsertBillingAddressRequest extends MDMUpsertAddressRequest {
		public override String getAddressType() { return 'Billing'; }
    }
	
	public class MDMUpsertShippingAddressRequest extends MDMUpsertAddressRequest {
		public override String getAddressType() { return 'Shipping'; }
    }
	
	public virtual class MDMChangeResponse { 
		public List<MDMResponse> mdmResponses { get; set; }
		
		public MDMChangeResponse() {
			mdmResponses = new List<MDMResponse>();
		}
		
		protected void Deserialize(Dom.XmlNode root, String parentNode) {
			try {
				for  (Dom.XmlNode node : root.getChildElements()) {
					try {
						XmlSerializer s = new XmlSerializer();
			            object o = s.Deserialize(node, 'MDMAddressProxy.MDMResponse');
			            mdmResponses.add((MDMResponse)o);
					} catch (Exception ex) {
						//TODO: handle exceptions better...
						//Don't let one failure failt he whole batch...?
					}
				}
			} catch (Exception ex) {
				//TODO: handle exceptions better...
				throw ex;
			}
		}
	}
	
	public class MDMResponse implements XmlSerializable {
		public Boolean Success { get; set; }
		public String Id { get; set; }
		public String Error { get; set; }
		
		public object get(string fieldName) {
	        Map<string, object> thisobjectmap = new Map<string, object> 
	        { 
	            'id' => this.Id, 
	            'success' => this.Success,
				'error' => this.Error
	        };
	        return thisobjectmap.get(fieldname);
	    }
	    
	    public boolean put(string fieldName, object value) {
	        if (fieldName == 'id') {
	            this.Id = String.valueOf(value);
	        } else if (fieldName == 'success') {
	            this.Success = Boolean.valueOf(value);  
			} else if (fieldName == 'error') {
	            this.Error = String.valueOf(value);
	        } else {
	            return false;
	        }
	        return true;
	    }
	    
	    public Set<string> getFields() {
	        Set<string> fields = new Set<string> 
	        { 
	            'success',
				'id',
				'error'
	        };
	        
	        return fields;
	    }
	}
	
	public class MDMUpsertAddressResponse extends MDMChangeResponse {
		public void Deserialize(Dom.XmlNode root) {
			Deserialize(root, 'MDMAddressProxy.MDMChangeResponse');
		}
	}
	
	public virtual class MDMDeleteAddressRequest extends MDMAddressRequest {
		public override String getName() { return 'deleteAddressRequest'; }
    }
	
	public class MDMDeleteBillingAddressRequest extends MDMDeleteAddressRequest {
		public override String getAddressType() { return 'Billing'; }
    }
	
	public class MDMDeleteShippingAddressRequest extends MDMDeleteAddressRequest {
		public override String getAddressType() { return 'Shipping'; }
    }
	
	public class MDMDeleteAddressResponse extends MDMChangeResponse {
		public void Deserialize(Dom.XmlNode root) {
			Deserialize(root, 'MDMAddressProxy.MDMChangeResponse');
		}
	}
	
}