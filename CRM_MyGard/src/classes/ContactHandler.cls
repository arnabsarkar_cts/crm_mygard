public class ContactHandler {
    @future
    public static void updateContactsFuture(List<Id> allContactIds){
        updateContacts(allContactIds);
    }
    
    public static void updateContacts(List<Id> allContactIds){
       /* List<Contact> allContactsToBeUpdated = new List<Contact>();
        for(Contact aContact: [SELECT ID, Administrator__c, MyGard_user_type__c FROM CONTACT WHERE ID IN: allContactIds]){
            if(aContact.Administrator__c){
                aContact.MyGard_user_type__c='Admin';
            }else{
                aContact.MyGard_user_type__c='';//'Normal';
            }
            allContactsToBeUpdated.add(aContact);
        }
        update allContactsToBeUpdated;*/
    }
    
    @future
    public static void updateContactRecordForNotifyAdminFuture(Boolean value,List<Id> contactIds){
        updateContactRecordForNotifyAdmin(value,contactIds);
    }
    
    public static void updateContactRecordForNotifyAdmin(Boolean value,List<Id> contactIds){
        List<Contact> allContacts = [SELECT ID,Notify_Admin__c FROM CONTACT WHERE ID IN: contactIds];
        for(Contact aContact : allContacts){
            aContact.Notify_Admin__c=value;
        }
        if(allContacts.size() > 0){
            update allContacts;
        }
    }
    
    @future
    public static void updateContactsForInactiveUserFuture(List<Id> allContactIds){
        updateContactsForInactiveUser(allContactIds);
    }    
    
    public static void updateContactsForInactiveUser(List<Id> allContactIds){
        List<Contact> allContactsToBeUpdated = new List<Contact>();
        for(Contact aContact: [SELECT ID,  MyGard_user_type__c,No_Longer_Employed_by_Company__c FROM CONTACT WHERE ID IN: allContactIds]){
            aContact.MyGard_user_type__c='';
            //aContact.No_Longer_Employed_by_Company__c = NoLongerEmp__c.getInstance(aContact.Id).No_Longer_Emp__c;
            allContactsToBeUpdated.add(aContact);
        }
        
        //update allContactsToBeUpdated;
        
        if(GardUtils.blockFurtherUpdate == false) // avoiding only when user record is updated through trigger when anyone update noLongerEmployed checkbox through backend.(Manually)
        {
            update allContactsToBeUpdated;
        }          
    }
}