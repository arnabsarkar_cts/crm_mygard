@isTest(seeAllData=false)
Public class TestUWRforOffshoreVesselCtrl
{
    private static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    private static  ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    private static GardTestData test_rec;
    
    private static void createTestData(){
        Test.startTest();
        
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        
        test_rec=new GardTestData();
        test_rec.customsettings_rec();
        test_rec.commonrecord();
        
        Test.stopTest();
    }
    
    @isTest private static void OffshoreVesselCtrlBroker(){
        createTestData();
        //Invoking methods of UWRforOffshoreVesselCtrl for broker users... 
        System.runAs(GardTestData.brokerUser)         
        {
            UWRforOffshoreVesselCtrl test_broker=new UWRforOffshoreVesselCtrl();
            test_broker.selectedGardEmployee='';
            test_broker.selectedGardTeam='';
            test_broker.clientName='';
            test_broker.setGlobalClientIds=new List<String>();
            test_broker.lstAccount = new List<Account>();
            test_broker.selectedCountryCode = 'IN - India';
            test_broker.isSame = true;
            test_broker.lstContact =new List<Contact>();
            test_broker.imoNo=123;
            test_broker.confirmationCheckbox=true;
            test_broker.CountryPrefix='91';
            test_broker.selectedGlobalClient = new list<string>();
            test_broker.selectedGlobalClient.add(GardTestData.clientAcc.id);
            test_broker.IMONotKnown=true;
            test_broker.selectedObj=GardTestData.test_object_1st.id;
            List<SelectOption> Countrycodes=  test_broker.getCountrycodes();
            List<SelectOption> clientOptions =  test_broker.getClientLst();
            List<SelectOption> flag=test_broker.getFlagList();
            List<SelectOption> port=test_broker.getPortList();
            List<SelectOption> classification=test_broker.getClassificationList();
            List<SelectOption> collliabilties= test_broker.getCollLiabilities();
            List<SelectOption> nationalities= test_broker.getNationality();
            system.assertequals(flag.size()>0,true,true);
            system.assertequals(port.size()>0,true,true);
            system.assertequals(classification.size()>0,true,true);
            system.assertequals(collliabilties.size()>0,true,true);
            system.assertequals(clientOptions .size()>0,true,true);
            system.assertequals(nationalities.size()>0,true,true);
            test_broker.assetFinal = null;
            test_broker.selectedClient = GardTestData.clientAcc.id;
            test_broker.ObjName = string.valueof(GardTestData.test_object_1st.name); 
            test_broker.ObjName = GardTestData.test_object_1st.name;
            test_broker.FindBy='objname';           
            test_broker.searchByImoObjectName();
            test_broker.assetFinal = null;
            test_broker.selectedClient = GardTestData.clientAcc.id;
            test_broker.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c);
            test_broker.FindBy='imo';
            test_broker.searchByImoObjectName();
            test_broker.FindBy='objname';
            //test_broker.ObjName = GardTestData.test_object_1st.name;
            //test_broker.searchByImoObjectName();
            test_broker.dateOutput='05.02.2015';
            test_broker.dateOutputForReview='05.02.2015';
            test_broker.SelectedClassification.add('class B');
            test_broker.uwf = new Case();
            test_broker.setObjectSelection();
            test_broker.goNext();
            test_broker.uwf = new Case();
            test_broker.objectIns = new Object__c();
            test_broker.selectedCollLiabilities= '0 RDC';
            test_broker.selectedIncludingFFO ='yes';
            test_broker.dateOutputForReview='05.02.2015';
            test_broker.saveRecord();
            test_broker.selectedCollLiabilities= '1/4 RDC';
            test_broker.objectIns = new Object__c();
            test_broker.saveRecord();
            test_broker.selectedCollLiabilities= '4/4 RDC';
            test_broker.saveRecord();
            test_broker.grosstonnage=23333;
            
            //test_broker.goNext();
            test_broker.clearRecord();
            test_broker.changeSelectedClientIdFormsCS();
            test_broker.saveRecordForAddlInfo();
            test_broker.uwf = new Case();
            test_broker.uwf.id=GardTestData.brokerCase.id;
            test_broker.saveRecordForReview();
            test_broker.clearRecordForAddlInfo();
            test_broker.goPrevForAddlInfo();
            test_broker.cancelRecord();
            test_broker.goPrevForReview();
            test_broker.goNextForAddlInfo();
            test_broker.getCName();
            test_broker.sendToForm();
            test_broker.chkAccess();
            //test_broker.sendAckMail();
            //test_broker.sendNotiMail();
            test_broker.uwf.id=GardTestData.brokerCase.id;
            test_broker.saveRecord();
            test_broker.saveRecordForAddlInfo();
            
            Case updtUWF = [SELECT Id,Object_name__c,Imo_Lloyds_No__c,Flag__c,tonnage__c,Port_Of_Registry__c,Year_built__c,
                            Signal_Letters_Call_sign__c,Classification_society__c,short_description_vessel__c,personnel_on_board__c,
                            Nationality_of_officer__c,Number_of_officers__c,CGL_Offshore_Cover_PI__c,X0_RDC__c,X1_4_RDC__c,
                            X4_4_RDC__c,Name_Form_Period_Of_Charter__c,Other_statutory_certification__c,Co_assureds_capacity__c,
                            Name_on_Premium_Invoice__c,Mortgagee_Name_and_Address__c,VAT_number__c,VAT_Name_AddressOf_Company__c,
                            Number_of_crew__c,Nationality_of_crew__c,Limit_of_Cover_USD__c,Registered_owner_s_name_and_address__c,
                            Name_of_Assured_Member__c FROM Case WHERE Id =: GardTestData.brokerCase.Id];
            updtUWF.Object_name__c = null;
            updtUWF.Imo_Lloyds_No__c = null;
            updtUWF.Flag__c = null;
            updtUWF.tonnage__c = null;
            updtUWF.Port_Of_Registry__c = null;
            updtUWF.Year_built__c = null;
            updtUWF.Signal_Letters_Call_sign__c = null;
            updtUWF.Classification_society__c = null;
            updtUWF.short_description_vessel__c = 'text';
            updtUWF.personnel_on_board__c = 'text';
            updtUWF.Nationality_of_officer__c = 'India';
            updtUWF.Number_of_officers__c = null;
            updtUWF.CGL_Offshore_Cover_PI__c = 'Yes';
            updtUWF.X0_RDC__c= 'Yes';
            updtUWF.X1_4_RDC__c= 'Yes';
            updtUWF.X4_4_RDC__c='Yes';
            updtUWF.Name_Form_Period_Of_Charter__c= 'text';
            updtUWF.Other_statutory_certification__c= 'text';
            updtUWF.Co_assureds_capacity__c= 'text';
            updtUWF.Mortgagee_Name_and_Address__c = null;
            updtUWF.Name_on_Premium_Invoice__c = null;
            updtUWF.VAT_number__c = null;
            updtUWF.Country_prefix__c = null;
            updtUWF.VAT_Name_AddressOf_Company__c = null;
            updtUWF.Number_of_crew__c = null;
            updtUWF.Limit_of_Cover_USD__c = null;
            updtUWF.Nationality_of_crew__c = 'Australia';
            updtUWF.Registered_owner_s_name_and_address__c = null;
            updtUWF.Name_of_Assured_Member__c = null;
            //update updtUWF;
            test_broker.dateOutput = null;
            //test_broker.saveRecordForReview();
            test_broker.strSelected=GardTestData.brokerAcc.id+';'+GardTestData.brokerAcc.id;
            test_broker.fetchSelectedClients();
            //test_broker.strGlobalClient=GardTestData.brokerAcc.id+';'+GardTestData.brokerAcc.id;
            //test_broker.fetchGlobalClients();
            
            test_broker.userType='GUEST';
            test_broker.clearRecordForAddlInfo();
            //       UWRforOffshoreVesselCtrl.UWRChartersVesselCtrl uwr=new UWRforOffshoreVesselCtrl.UWRChartersVesselCtrl();
            //test_broker.searchByImoObjectName();
            // test_broker.sendAckMail();
            //Test.stopTest();
        }
        
        
    }  
    @isTest private static void offshoreVesselCtrlClient(){
        createTestData();
        /*
       
        
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        
        GardTestData test_rec=new GardTestData();
        test_rec.customsettings_rec();
        test_rec.commonrecord();
        
        Test.stopTest();
        */
        //Invoking methods of UWRforOffshoreVesselCtrl for client users... 
        System.runAs(GardTestData.clientUser)  
        {
            //Test.startTest();
            
            UWRforOffshoreVesselCtrl test_client=new UWRforOffshoreVesselCtrl();
            test_client.setGlobalClientIds=new List<String>();
            test_client.confirmationCheckbox=true;
            test_client.CountryPrefix='91';
            test_client.IMONotKnown=true;
            test_client.selectedGlobalClient = new list<string>();
            test_client.selectedGlobalClient.add(GardTestData.clientAcc.id);
            //test_client.selectedObj=GardTestData.test_object_1st.id;
            //List<SelectOption> clientOptions =  test_client.getClientLst();
            List<SelectOption> flag=test_client.getFlagList();
            List<SelectOption> port=test_client.getPortList();
            List<SelectOption> classification=test_client.getClassificationList();
            system.assertequals(flag.size()>0,true,true);
            system.assertequals(port.size()>0,true,true);
            system.assertequals(classification.size()>0,true,true);
            //system.assertequals(clientOptions.size()>0,true,true);
            test_client.assetFinal = null;
            
            test_client.selectedClient = GardTestData.clientAcc.id;
            test_client.ObjName = string.valueof(GardTestData.test_object_1st.name); 
            test_client.FindBy='objname';           
            test_client.searchByImoObjectName();
            test_client.assetFinal = null;
            test_client.selectedClient = GardTestData.clientAcc.id;
            test_client.ObjName = string.valueof(GardTestData.test_object_1st.Imo_Lloyds_No__c);
            test_client.FindBy='imo';
            test_client.searchByImoObjectName();
            test_client.dateOutput='05/02/2015';
            test_client.dateOutputForReview='05/02/2015';
            test_client.SelectedClassification.add('class B');
            //test_client.setObjectSelection();
            test_client.objectIns = new Object__c();
            //   test_client.saveRecord();
            test_client.grosstonnage=23333;
            //test_client.goNext();
            // test_client.clearRecord();
            //   test_client.saveRecordForAddlInfo();
            // test_client.saveRecordForReview();
            test_client.clearRecordForAddlInfo();
            test_client.chkAccess();
            test_client.goPrevForAddlInfo();
            test_client.cancelRecord();
            test_client.goPrevForReview();
            test_client.goNextForAddlInfo();
            test_client.getCName();
            test_client.sendToForm();
            //test_client.sendAckMail();
            //test_client.sendNotiMail();
            test_client.uwf.id=GardTestData.clientCase.id;
            // test_client.saveRecord();
            //      test_client.saveRecordForAddlInfo();
            //test_client.saveRecordForReview();
            test_client.clearRecord();
            test_client.getCountry();
            //Test.stopTest();
        }
    }
    
}