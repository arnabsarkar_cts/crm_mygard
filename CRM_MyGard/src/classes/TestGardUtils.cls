@isTest
public class TestGardUtils{
    //local use variables
    private static GardTestData gtdInstance;
    private static map<id,contact> new_map;
    private static map<id,contact> old_map;
    private static map<Id,Contact> mapNewContact;
    private static Map<id,Account_Contact_Mapping__c> acmMap;
    private static Map<id,Case> csMap;
    private static User dummyUser;
    private static User dummyUser21;
    private static Business_Area_Coverage_Mapping__c area12;
    private static Case CaseForTestCover;
    private static Case CaseForTestCover1;
    private static Contract_Review__c crTest;
    private static set<ID> setUserId;
    private static set<ID> setContactId;
    private static list<contact> contactlist;
    
    private static void createTestData(){
        gtdInstance = new GardTestData();
        gtdInstance.customsettings_rec();
        gtdInstance.commonrecord();
        
        Test.startTest();
        //specificData creation, need to be refactored later and merged with the Common Data Generator
        List<User> dummyUsers = new List<User>();
        dummyUser = new User(
            Alias = 'standtD' , 
            profileId = GardTestData.salesforceLicenseId  ,
            Email='Claim@gard.no',
            EmailEncodingKey = GardTestData.utfStr,
            LastName='tetst_005Dummy',
            LanguageLocaleKey= GardTestData.enUsrStr ,
            CommunityNickname = 'DummyUser',
            LocaleSidKey = GardTestData.enUsrStr ,  
            TimeZoneSidKey = GardTestData.tymZnStr ,
            UserName='mygardtestdummyUser@testorg.com.mygard',
            //IsPortalEnabled = false,
            //userType = 'Standard',
            //ContactId = BrokerContact.Id,
            contactId__c = GardTestData.grdobj.id,
            IsActive = true
        );
        
        //insert dummyUser;
        dummyUsers.add(dummyUser);
        
        dummyUser21 = new User(
            Alias = 'standtD1' , 
            profileId = GardTestData.salesforceLicenseId  ,
            Email='Claim@gard.no',
            EmailEncodingKey = GardTestData.utfStr,
            LastName='tetst_005Dummy21',
            LanguageLocaleKey= GardTestData.enUsrStr ,
            CommunityNickname = 'DummyUser21',
            LocaleSidKey = GardTestData.enUsrStr ,  
            TimeZoneSidKey = GardTestData.tymZnStr ,
            UserName='mygardtestdummyUser21@testorg.com.mygard',
            //IsPortalEnabled = false,
            //userType = 'Standard',
            //ContactId = BrokerContact.Id,
            //contactId__c = GardTestData.grdobj.id,
            IsActive = true
        );
        
        //insert dummyUser21;
        dummyUsers.add(dummyUser21);
        insert dummyUsers;
        
        area12 = new Business_Area_Coverage_Mapping__c(
            Coverage_Description__c = 'P&I',
            Coverage_Code__c='AZ',
            Name='testfortestready',
            Business_Area_Code__c='PI'
        );
        insert area12;
        
        CaseForTestCover = new Case(
            Accountid = GardtestData.brokerAcc.id,
            Account = GardtestData.brokerAcc,
            Contact = GardtestData.brokerContact, 
            ContactId = GardtestData.brokerContact.id,
            Origin =  GardtestData.orignStr ,                        
            Object__c = GardtestData.test_object_1st.id,                          
            Claim_Incurred_USD__c= 500,
            Claim_Reference_Number__c='kol23232',
            Reserve__c = 350,
            //  recordTypeId =System.Label.Broker_Contact_Record_Type,
            Uwr_form_name__c = GardtestData.testStr,
            Paid__c =50000,
            Total__c= 750, 
            //email = 'a2z@gmail.com',                         
            Member_reference__c = 'ash1232',
            Contact_me_area__c = 'Claims',
            Last_name__c = 'yo1',
            guid__c = '8ce8ad66-a6ec-1834-9e21',
            Claim_Type__c = GardtestData.cargoStr ,
            Risk_Coverage__c = GardtestData.brokerAsset_1st.id,
            Status = 'Closed',
            Status_Change_Date__c = datetime.now(),
            Contract_for_Review__c = GardtestData.brokerContract_1st.id,
            Claims_handler__c = GardtestData.brokerUser.id,
            Assetid = GardtestData.brokerAsset_1st.id,
            OwnerId = GardtestData.brokerUser.id,
            LOC__c =  GardtestData.cntrctNmStr  ,
            Type = 'Contact me',
            Submitter_company__c = GardtestData.brokerAcc.id
            //   Name_Of_Contract__c =  cntrctNmStr  
            //Owner=brokerUser.id
        );
        //GardUtils.claimType = 'cargo';
        insert CaseForTestCover;
        
        CaseForTestCover1 = new Case(
            Accountid = GardtestData.brokerAcc.id,
            Account = GardtestData.brokerAcc,
            Contact = GardtestData.brokerContact, 
            ContactId = GardtestData.brokerContact.id,
            Origin =  GardtestData.orignStr ,                        
            Object__c = GardtestData.test_object_1st.id,                          
            Claim_Incurred_USD__c= 500,
            Claim_Reference_Number__c='kol23231',
            Reserve__c = 350,
            //  recordTypeId =System.Label.Broker_Contact_Record_Type,
            Uwr_form_name__c = GardtestData.testStr,
            Paid__c =50000,
            Total__c= 750, 
            //email = 'a2z@gmail.com',                         
            Member_reference__c = 'ash1232',
            Contact_me_area__c = 'Claims',
            Last_name__c = 'yo2',
            guid__c = '8ce8ad66-a6ec-1834-9e22',
            Claim_Type__c = GardtestData.cargoStr ,
            Risk_Coverage__c = GardtestData.brokerAsset_1st.id,
            Status = 'Review Completed',
            Status_Change_Date__c = datetime.now(),
            Contract_for_Review__c = GardtestData.brokerContract_1st.id,
            Claims_handler__c = GardtestData.brokerUser.id,
            Assetid = GardtestData.brokerAsset_1st.id,
            OwnerId = GardtestData.brokerUser.id,
            LOC__c =  GardtestData.cntrctNmStr  ,
            Type = 'Crew Contract Review',
            Submitter_company__c = GardtestData.brokerAcc.id
            //   Name_Of_Contract__c =  cntrctNmStr  
            //Owner=brokerUser.id
        );
        
        //GardUtils.claimType = 'defence';
        insert CaseForTestCover1;
        
        CaseForTestCover1.Status = 'Closed';
        update CaseForTestCover1;
        
        crTest = new Contract_Review__c(
            Contract_Name__c = 'conTestforReview',
            CaseId__c = CaseForTestCover1.ID
        );
        insert crTest;
        //case created for the gardutil coverage -stop
        new_map = new map<id,contact>();
        old_map = new map<id,contact>();
        new_map.put(GardtestData.brokerContact.ID,GardtestData.brokerContact);
        old_map.put(GardtestData.brokerContact.ID,GardtestData.brokerContact);
        mapNewContact = new map<Id,Contact>();
        mapNewContact.put(GardtestData.clientContact.id,GardtestData.clientContact);
        GardtestData.clientContact.No_Longer_Employed_by_Company__c = true;
        acmMap = new Map<id,Account_Contact_Mapping__c>();
        acmMap.put(GardtestData.Accmap_1st.id,GardtestData.Accmap_1st );
        setUserId = new set<Id>();
        setUserId.add(GardtestData.brokerUser.id);
        setUserId.add(dummyUser.id);
        setUserId.add(dummyUser21.id);
        setContactId = new set<Id>();
        setContactId.add(GardtestData.brokerContact.id);
        contactlist = new list<contact>();
        contactlist.add(gtdInstance.broker_Contact_1st);
        contactlist.add(gtdInstance.client_Contact_1st);
        csMap = new Map<id,Case>();
        csMap.put(CaseForTestCover.ID,CaseForTestCover);
        
        System.assert(GardtestData.brokerAcc != null , true);
    }
    
    @isTest public static void gardutil(){
        //case created for the gardutil coverage -start
        /*BlueCard_webservice_endpoint__c bweTest = new BlueCard_webservice_endpoint__c();
        bweTest.Name = 'BlueCard_WS_endpoint';
        bweTest.Endpoint__c = 'https://soa-test.gard.no/soa-infra/services/Documents/DocumentFetcher/GetDocumentService_ep';
        insert bweTest;
        
        BlueCardTermsAndCondition__c bctTest = new BlueCardTermsAndCondition__c();
        bctTest.Name = 'Tems and Conditions';
        bctTest.gardUserId__c = 'eliing';
        bctTest.Doc_Id__c = '8684124';
        insert bctTest;*/
        createTestData();
        System.runAs(GardTestData.brokerUser)
        {
            //GardUtils.createContactRecord(setUserId);
            //GardUtils.updateContactRecordFuture(setUserId);
            //GardUtils.updateUserRecord(setContactId);
            //GardUtils.UpdateUser(GardtestData.brokerContact.ID,'abc@temp.com');
            
            GardtestData.brokerContact.AccountId=GardtestData.clientAcc.Id;
            GardUtils.restrictAccountUpdate(old_map,new_map);
            GardtestData.brokerContact.AccountId=GardtestData.brokerAcc.Id;
            GardUtils.updateAccountContactMappingForNoLongerEmployed(mapNewContact,mapNewContact);
            GardUtils.updateAccount(acmMap);
            GardUtils.createAccountContactJuncRecord(setUserId);
            GardUtils.createContactRecord(setUserId);
            GardUtils.updateContactRecordFuture(setUserId);
            //GardUtils.updateContactRecord(setUserId);
            //GardUtils.UpdateUser(String.valueOf(GardTestDAta.brokerContact.ID),GardTestDAta.brokerContact.Email);
            GardUtils.updateUserRecord(setContactId);
            GardUtils.UpdateUser(String.valueOf(GardtestData.brokerContact.ID),GardtestData.brokerContact.email);
            GardUtils.updateUser(setContactId);
            GardUtils.updateMultiCompanyFuture(setContactId);
            GardUtils.deactivate(setContactId);
            GardUtils.deactivateUsrFuture(setContactId);
            GardUtils.updateMultiCompanyJuncObjFuture(setContactId);
            Case dummyCase = new Case();
            dummyCase = GardTestDAta.brokerCase;
            dummyCase.Status = 'Delete';
            set<ID> setCaseId = new set<Id>();
            setCaseId.add(dummyCase.id);
            GardUtils.deleteCase(setCaseId);
            GardUtils.updatePIAccess_PeopleClaimAccess(acmMap,acmMap);
            GardUtils.formatDateToString(date.today());
            GardUtils.formatDate('17-08-2016');
            GardUtils.insertContractReview(String.valueOf(CaseForTestCover1.ID));
            GardUtils.updateContractReview(CaseForTestCover1);
            /*GardUtils.claimType = 'cargo';
            GardUtils.changeCaseOwner(csMap);
            GardUtils.claimType = 'defence';
            GardUtils.changeCaseOwner(csMap);
            GardUtils.claimType = 'Crew';
            GardUtils.changeCaseOwner(csMap);*/
            
            //GardUtils.restrictAccountUpdate(mapNewContact,new_map);
            
            //GardUtils.displayDoc(String.valueOf(GardTestDAta.brokerAcc.ID));
            //GardUtils.updateMultiCompanyJuncObjFuture(contactlist);
            //
            //GardUtils.displayDoccument(GardtestData.clientAcc.Id);
        }
        Test.stopTest();
    }
    
    @istest private static  void test2(){
        createTestData();
        //gtdInstance.customsettings_rec();
        //gtdInstance.PEMEClinicSubmitInvoiceCtrl();
        System.runAs(GardTestData.clientUser){
            List<case> updateCaseList = new List<Case>();
            updateCaseList.add(GardTestDAta.brokerCase);
            Set<String> accIdSet = new Set<String>();
            accIdSet.add(GardTestDAta.clientAcc.Id);
            GardUtils.updateClaimOwners(updateCaseList);
            GardUtils.updateAccountEnrollmentStatus(accIdSet,'Enrolled');
            GardUtils.createServiceReqForClaimComment('test',GardTestDAta.brokerClaim.Id, GardTestDAta.brokerUser.Id );
            Test.stopTest();
            //GardUtils.sendPEMEDeleteMail(GardTestData.pi.id,'PEMEManDeleteNotification');
        }
    }
}