//Test class for MyObjectCtrl .
@istest(seeAllData = false)
public class TestMyobjectctrl{ 
    ////////*******Variables for removing common literals********************//////////////
    public static string prodStr =  'curia';
    public static string prodAreaStr =  'P&I Cover';
    public static string testStr =  'Test_1';
    /////**********************end**************************/////
//Test method...........................................  
    public static testmethod void Myobject()
    { 
          Gardtestdata test_rec = new Gardtestdata();
          test_rec.Mycover_list_objectCtrl_testrecord();  
          test_rec.customsettings_rec();
          Gard_Contacts__c gard_contact_1= new Gard_Contacts__c(LastName__c='Sengupta', Nick_Name__c  = 'NilsPeter', 
                                                                Office_city__c = 'Arendal', 
                                                                Phone__c = '5454454'
                                                                );
          insert gard_contact_1 ;
          Contract client_Contract = New Contract(Accountid = GardTestData.client_Acc.id,                                         
                                        Status =   'Draft',
                                        CurrencyIsoCode =  'SEK', 
                                        StartDate = Date.today(),
                                        ContractTerm = 2,
                                        //Broker__c = broker_Acc.id,
                                        Client__c = GardTestData.client_Acc.id,
                                        Expiration_Date__c =  date.valueof('2015-01-01'),
                                        Agreement_ID__c = 'Agreement_ID_87833',
                                        Agreement_Type__c =   'P&I',
                                        Policy_Year__c ='2008',
                                        Agreement_Type_Code__c =   'Agreement123433',
                                        Inception_date__c =   date.valueof('2012-01-01')  ,
                                        Business_Area__c=  'Marine'
                                        );
         insert client_Contract ;
         Object__c testobjtest = New Object__c( Dead_Weight__c= 20,
                             Object_Unique_ID__c = '1234lkolkol',
                             Object_Type__c='Bulk Vessel',
                             Imo_Lloyds_No__c = '8475833',
                             //guid__c = '8ce8ad79-a6ed-1837-9e11',
                             //object_type_formula__c ='Bulk Vessel', 
                             Name='Curia' );
        insert testobjtest ;
        system.debug('**test Params****'+testobjtest.Id);
        Asset client_Asset = new Asset(Name=  'Test Asset',
                                 accountid = GardTestData.client_Acc.id,
                                 contactid = GardTestData.client_Contact.Id,  
                                 Agreement__c = client_Contract.id, 
                                 Cover_Group__c =   'P&I',
                                 CurrencyIsoCode= 'USD',
                                 product_name__c =   'P&I Cover',
                                 Object__c = testobjtest.id,
                                 Underwriter__c = GardTestData.client_User.id,
                                 On_risk_indicator__c = true,
                                 UnderwriterNameTemp__c= GardTestData.client_User.id,
                                 Expiration_Date__c =  date.valueof( '2015-01-01'),
                                 Inception_date__c =   date.valueof( '2012-01-01')  ,
                                 Risk_ID__c = '123456riq'
                                 );    
         insert client_Asset ;
        System.runAs(Gardtestdata.broker_User)        
        {
            Test.startTest();
            MyObjectCtrl test_broker = new MyObjectCtrl();
            test_broker.isFav=false;
            //test_broker.strGlobalClient=Gardtestdata.Broker_Acc.id+';'+Gardtestdata.Broker_Acc.id;
            String cltn=Gardtestdata.client_Acc.id;
            String obj_Id=Gardtestdata.testobj.id;
            String obj_name=Gardtestdata.testobj.name;
            String obj_type='vessel';
            String unwrt=Gardtestdata.salesforceLicUser_1.id;
            String prod= prodStr ;
            String prod_area= prodAreaStr ;
            String brk='break';
            Date exdt=date.valueof('2009-01-01');
            String plcyr='2008-01-01'; 
            String clm_ld=Gardtestdata.broker_User.id;
            String grd_sh='100';
            String rsk='Y'; 
            String under_ID=Gardtestdata.salesforceLicUser_1.id;
            String dummy=Gardtestdata.testobj.id;
            MyObjectCtrl.coverwrapper cover= new MyObjectCtrl.coverwrapper(cltn,obj_Id,obj_name,obj_type,unwrt,prod,prod_area,brk,exdt,plcyr,clm_ld,grd_sh,rsk,under_ID,dummy,'012345678901234',true,Gardtestdata.broker_Contract.id);
            ApexPages.currentPage().getParameters().put('favId',Gardtestdata.efav_test.id);
            test_broker.objSet.add( prodStr );
            List<selectoption> objlst=test_broker.getObjOptions();
            List<selectoption> cvrlst= test_broker.getCoverOptions();
            test_broker.lstClientOptions = new set<string>();
            //test_broker.lstClientOptions.add('product');
            test_broker.lstClientOptions.add(String.valueOf(Gardtestdata.client_Acc.id).substring(0,15));
            test_broker.getClientOptions();
            List<SelectOption> abcd = test_broker.getClientOptions();
            test_broker.lstClientOptions.add('New product');
            test_broker.lstCoverOptions = new set<string>();
            test_broker.lstCoverOptions.add( prodAreaStr );
            test_broker.lstCoverOptions.add('abcde');
            List<selectoption> cvrlst1= test_broker.getCoverOptions();
            test_broker.lstObjectTypeOptions = new set<string>();
            test_broker.lstObjectTypeOptions.add('product');
            test_broker.lstObjectTypeOptions.add('New product');
            test_broker.lstProdAreaOptions = new set<string>();
            test_broker.lstProdAreaOptions.add('Area');
            test_broker.lstProdAreaOptions.add('New Area');
            test_broker.lstPolYrOptions = new set<string>();
            test_broker.lstPolYrOptions.add('2000');
            test_broker.lstPolYrOptions.add('2005');
            test_broker.lstOnRiskOptions = new set<string>();
           // test_broker.lstOnRiskOptions.add(true);
            test_broker.lstOnRiskOptions.add('New Risk');
            test_broker.jointSortingParam = 'DESC##Agreement__r.Business_Area__c';
            test_broker.strParams = Gardtestdata.testobj.id;
            //test_broker.getClientOptions();
            test_broker.getObjectTypeOptions();
            //List<SelectOption> abc = test_broker.getClientOptions();
            test_broker.getProdAreaOptions();
            test_broker.getPolYrOptions();
            test_broker.getOnRiskOptions();
            string str = test_broker.sortExpression;
            String str1 = test_broker.getSortDirection();
            System.assertEquals('ASC',str1);
            test_broker.setSortDirection(str);
            test_broker.underwrId = gard_contact_1.Id + '#' + Gardtestdata.salesforceLicUser_1.id ;
            test_broker.underwriterPopup();
            test_broker.viewObject();
            test_broker.getCovers();
           // test_broker.fetchFavourites();
            test_broker.ViewData();
            //test_broker.exportToExcelForMyObject();
            test_broker.SelectedCovers = new List<string>();
            test_broker.SelectedCovers.add( prodAreaStr );
            test_broker.SelectedObjectType = new List<string>();
            test_broker.SelectedObjectType.add('Bulk Vessel');
            test_broker.SelectedObject=new List<string>();
            test_broker.SelectedObject.add( prodStr );
            //test_broker.SelectedObjectType.add('Barge');
            test_broker.SelectedProdArea = new List<string>();
            test_broker.SelectedProdArea.add('P&I');
            test_broker.SelectedProdArea.add('Energy');
            test_broker.SelectedClient = new List<string>();
            test_broker.SelectedClient.add( testStr );
            test_broker.selectedGlobalClient =new List<string>();
            test_broker.selectedGlobalClient.add( testStr ); 
            test_broker.SelectedPolYr = new List<string>();
            test_broker.SelectedPolYr.add('2008');
            test_broker.SelectedOnRisk = new List<string>();
            test_broker.SelectedOnRisk.add('true');
            system.assertequals(test_broker.SelectedCovers.size()>0,true,true);
            system.assertequals(test_broker.selectedGlobalClient.size()>0,true,true);
            system.assertequals(test_broker.SelectedProdArea.size()>0,true,true);
            system.assertequals(test_broker.SelectedClient.size()>0,true,true);
            system.assertequals(test_broker.SelectedPolYr.size()>0,true,true);
            system.assertequals(test_broker.SelectedOnRisk.size()>0,true,true);
            system.assertequals(test_broker.SelectedObjectType.size()>0,true,true);
            test_broker.ViewData();
            //test_broker.exportToExcelForMyObject();
            //test_broker.blBrokerView=false;
            //test_broker.searchAssets();
            Boolean var1 = test_broker.hasNext;
            Boolean var2 = test_broker.hasPrevious;
            Integer var3 = test_broker.pageNumber;             
            test_broker.strParams = Gardtestdata.testobj.id;
            test_broker.exportToExcel();
            test_broker.navigate();
            test_broker.next();
            test_broker.last();
            test_broker.previous();
            test_broker.exportToExcelForMyObject();
            test_broker.isFav=true;
            test_broker.newFilterName='test_filter';
            test_broker.createFavourites();
            test_broker.deleteFavourites();
            test_broker.fetchFavourites();
            MyObjectCtrl.getGenerateTimeStamp();
            MyObjectCtrl.getGenerateTime();
            MyObjectCtrl.getUserName();
            test_broker.print();
            test_broker.setpageNumber();
            test_broker.first();
            test_broker.strSelected='a;bc';
            test_broker.fetchSelectedClients(); // it  calls viewData internally
            test_broker.strSelected='abc';
            test_broker.fetchSelectedClients(); // it  calls viewData internally
            test_broker.clearOptions();
            MyObjectCtrl test_broker1 = new MyObjectCtrl();
            PageReference myobjects_page = Page.MyObjects;
            Test.setCurrentPage(myobjects_page);
            test_broker1.ViewData();
            test_broker1.blBrokerView = true;
            test_broker1.searchAssets();
            test_broker1.blBrokerView = true;
            test_broker1.exportToExcelForMyObject();
            test_broker1.print();
            test_broker1.downloadVcardCover();
            test_broker1.getLstAssetfilter();
            
            
         } 
       System.runAs(Gardtestdata.client_User)
         {    
            pageReference pageRef = page.MyObjects;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('param', testobjtest.Id);
            MyObjectCtrl test_client = new MyObjectCtrl();
            System.debug('UserType***** '+test_client.blBrokerView);
            //test_client.blBrokerView=false;
            test_client.ObjOptions1.add(new SelectOption('test','test'));
            test_client.ObjOptions2.add(new SelectOption('test','test'));
            test_client.ObjOptions3.add(new SelectOption('test','test'));
            test_client.jointSortingParam = 'DESC##Agreement__r.Business_Area__c';
            MyObjectCtrl.coverwrapper tempp = new MyObjectCtrl.coverwrapper('test_client','test_obj_id','test_obj_name','test_obj_type','test_underwriter','test_product','test_product_area','test_broker',Date.valueof('2014-12-12'),'test_policy_year','test_claim_id','test_gard_share','true','test_underwriter_id','test_dummy','012345678901234',true,Gardtestdata.broker_Contract.id); //ClientId added by Abhirup
            //test_client.strParams = Gardtestdata.testobj.id;
            test_client.SelectedCovers = new List<string>();
            test_client.SelectedCovers.add( prodAreaStr );
            test_client.SelectedObjectType = new List<string>();
            test_client.SelectedObjectType.add('Bulk Vessel');
            test_client.SelectedObject=new List<string>();
            test_client.SelectedObject.add( prodStr );
            //test_client.SelectedObjectType.add('Barge');
            test_client.SelectedProdArea = new List<string>();
            test_client.SelectedProdArea.add('P&I');
            test_client.SelectedProdArea.add('Energy');
            test_client.SelectedClient = new List<string>();
            test_client.SelectedClient.add( testStr );
            test_client.selectedGlobalClient =new List<string>();
            test_client.selectedGlobalClient.add( testStr ); 
            test_client.SelectedPolYr = new List<string>();
            test_client.SelectedPolYr.add('2008');
            test_client.SelectedOnRisk = new List<string>();
            test_client.SelectedOnRisk.add('true');
            test_client.SelectedOnRisk.add('false');
            system.assertequals(test_client.SelectedCovers.size()>0,true,true);
            system.assertequals(test_client.selectedGlobalClient.size()>0,true,true);
            system.assertequals(test_client.SelectedProdArea.size()>0,true,true);
            system.assertequals(test_client.SelectedClient.size()>0,true,true);
            system.assertequals(test_client.SelectedPolYr.size()>0,true,true);
            system.assertequals(test_client.SelectedOnRisk.size()>0,true,true);
            system.assertequals(test_client.SelectedObjectType.size()>0,true,true);
            string str = test_client.sortExpression;
            test_client.getSortDirection();
            test_client.setSortDirection(str);
            test_client.viewObject();
            test_client.getCovers();   
            test_client.ViewData();
            test_client.searchAssets();
            test_client.setpageNumber();
            test_client.first(); 
            test_client.exportToExcelForMyObject();
            //test_client.blBrokerView = true;
            test_client.print(); 
            //test_client.blBrokerView = true;
            //test_client.searchAssets();
            Test.stopTest();
         }
     }   
 }