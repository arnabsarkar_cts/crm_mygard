/****************************************************************************************************************************
*    Deveployed By   :    Cognizant Technology Solution
*    Created Date    :    9/12/2016 
*    Descriptions    :    This class is used to display activity feed as per Logged In User in ClientActivity page.   
*    Modification Log
*    --------------------------------------------------------------------------------------------------------
*    Developer                                Date                        Description
*    --------------------------------------------------------------------------------------------------------
*    Arindam Ganguly                          03/11/2017                SF-3990 Support users created activity will not be displayed in MyGard.
*****************************************************************************************************************************/
public without sharing class ClientActivityCtrl {
    
    //pagination
    private Integer SMARTQUERY_PAGE_SIZE = 10;    
    Public Integer noOfRecords{get;set;}
    Public Integer size{get;set;}
    public String nameOfMethod;
    Public Integer intStartrecord {get;set;}
    Public Integer intEndrecord {get;set;}
    public Integer pageNum{get;set;}    
    public Integer noOfData{get;set;}
    private Boolean isQueried = false;
    Public String strASC;
    public String strLimit;
    private String sortDirection;
    public String strCondition;
    private String sortExp;
    private string sortFullExp;
    
    public List<Client_Activity__c> lstActivities;
    public List<List<ClientActivityWrapper>> lstAllActivitiesForExcel{get;set;}
    public string strSoql;
    public string loggedInContactId;    
    public string loggedInAccountId;
    public Set<String> own_Contract_Id_Set;
    public Set<String> total_Contract_Id_Set;
    public string UserType {get; set;}
    User usr;    
    public List<ClientActivityWrapper> lstWrapperData{get;set;}
    public String strSelected{get;set;}//added For Global Client
    public String strGlobalClient{get;set;}//added For Global Client
    public List<String> selectedGlobalClient {get;set;}//added For Global Client    
    public List<String> lstGlobalClientIds;//added For Global Client 
    public set<string> setClients;    
    public boolean isPageLoad;
    
    public list<string> prodArea {get;set;}
    
    //SF-3760--Added For Favourites - Start
    String favouriteId;
    public boolean isFav{get;set;}
    public String newFilterName{get;set;}
    //SF-3760--Added For Favourites - End
    
    //filter
    // used to show more than 1000 record in drop down.     
    public List<SelectOption> ObjOptions1{get;set;}
    public List<SelectOption> ObjOptions2{get;set;}
    public List<SelectOption> ObjOptions3{get;set;}        
    public Set<String> setEventTypeOptions;
    public List<String> SelectedEventType {get;set;}    
    public Set<String> setObjectOptions;
    public List<String> SelectedObject {get;set;}
    public Set<String> setClientOptions;
    public String SelectedClient {get;set;}
    public String selectedFromDate {get; set;}
    public DateTime dtFromDate;
    public String selectedToDate {get; set;}
    public DateTime dtToDate;
    
    //To populate Client DropDownList
    public List<SelectOption> getClientOptions() {
        List<SelectOption> Options = new List<SelectOption>();        
        /*if(setClientOptions!= null && setClientOptions.size()==1){
            SelectedClient = '';
            for(String strOp: setClientOptions){
                if(strOp != null ){
                    Options.add(new SelectOption(strOp,strOp));
                    SelectedClient = strOp;
                }
            }
        }else if(setClientOptions!= null && setClientOptions.size()>1){
            for(String strOp:setClientOptions){
                if(strOp != null )
                    Options.add(new SelectOption(strOp,strOp));
            }   
            Options.sort();
            if(SelectedClient=='' || SelectedClient == null)
                SelectedClient = Options[0].getLabel();
        }    */  
        // added for 3911 - start
        Options.add(new SelectOption('',''));//Added for SF-4459
        for(String strOp:setClientOptions){
                if(strOp != null )
                    Options.add(new SelectOption(strOp,strOp));
            }  
        // added for 3911 - end      
        Options.add(new SelectOption('My Activities','My Activities')); // Changes done for 3911
        return Options;
    }
    
    //To Populate Object DropDownList
    public List<SelectOption> getObjOptions() 
    {
        if(ObjOptions1 != null && ObjOptions1.size() >0)
            ObjOptions1.clear();
        if(ObjOptions2 != null && ObjOptions2.size() >0)
            ObjOptions2.clear();
        if(ObjOptions3 != null && ObjOptions3.size() >0)
            ObjOptions3.clear();
        List<SelectOption> options = new List<SelectOption>();        
        List<String> temp = new List<String>();
        temp.addAll(setObjectOptions);
        temp.sort();
        Integer i = 1;        
        if(setObjectOptions != null && setObjectOptions.size()>0)
        {
            for(String strOp: temp)
            {
                if(strOp != null && strOp != '')
                {                    
                    if( i<1000)
                    {
                        options.add(new SelectOption(strOp,strOp));
                    }
                    else if(i>= 1000 && i< 2000)
                    {
                        ObjOptions1.add(new SelectOption(strOp,strOp));                        
                    }
                    else if(i>=2000 && i<3000)
                    {
                        ObjOptions2.add(new SelectOption(strOp,strOp));
                    }
                    else if(i>=3000 && i<4000)
                    {
                        ObjOptions3.add(new SelectOption(strOp,strOp));
                    }                   
                }
                i++;
            }
            options.sort();
            ObjOptions1.sort();
            ObjOptions2.sort();
            ObjOptions3.sort();            
        }
        return options;
    }
    
    //To Populate Object-Type DropDownList
    public List<SelectOption> getEventTypeOptions() {
        List<SelectOption> Options = new List<SelectOption>();        
        if(setEventTypeOptions!= null && setEventTypeOptions.size()>0){
            for(String strOp:setEventTypeOptions){
                if(strOp != null )
                    Options.add(new SelectOption(strOp,strOp));
            }
            Options.sort();
        }        
        return Options;
    }
    
    //To Build Sort Expression
    public String sortExpression
    {
        get
        {
            return sortExp;
        }
        set
        {
            //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortExp)
                sortDirection = (sortDirection == strASC)? 'ASC' : strASC;
            else
                sortDirection = strASC;
            sortExp = value;
        }
    }
    
    //To Get Sort Direction on Column Header Click
    public String getSortDirection()
    {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
            return strASC;
        else
            return sortDirection;
    }
    
    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }
    
    //Constructor
    public ClientActivityCtrl()
    {
        //calling helperclass method
        MyGardHelperCtrl.CreateCommonData();
        
        //non-static variable initialization
        sortExp = '';
        sortDirection = '';
        strSoql ='';
        strASC = 'DESC';
        strLimit = 'LIMIT 10000' ;   
        sortExpression = 'CreatedDate'; 
        pageNum = 1;
        isPageLoad = true;
        lstActivities = new List<Client_Activity__c>();
        prodArea = new list<string>();
        ObjOptions1 = new List<SelectOption>();
        ObjOptions2 = new List<SelectOption>();
        ObjOptions3 = new List<SelectOption>();
        setClientOptions = new Set<String>();
        setObjectOptions = new Set<String>();
        setEventTypeOptions = new Set<String>();        
        
        //helper class variables
        usr = MyGardHelperCtrl.usr;        
        UserType = MyGardHelperCtrl.USER_TYPE;
        loggedInContactId  = MyGardHelperCtrl.LOGGED_IN_CONTACT_ID;
        loggedInAccountId = MyGardHelperCtrl.LOGGED_IN_ACCOUNT_ID;    
        own_Contract_Id_Set = MyGardHelperCtrl.OWN_CONTRACT_ID_SET;
        total_Contract_Id_Set = MyGardHelperCtrl.TOTAL_CONTRACT_ID_SET;        
        
        prodArea = MyGardHelperCtrl.prodArea;
        
        if(own_Contract_Id_Set!= NULL && own_Contract_Id_Set.size()>0)
        {
            List<Contract> lstContract = [Select Id, Client__c from Contract where Id In : own_Contract_Id_Set];
            if(lstContract != null && lstContract.size() > 0)
            {                
                setClients = new Set<String>();
                for(Contract con: lstContract)
                {
                    setClients.add(con.Client__c);
                }
            }    
        } 
        System.debug('*****Acquired and Own Clients:'+setClients);
        
        /* if(total_Contract_Id_Set!= NULL && total_Contract_Id_Set.size()>0)
{
List<Contract> lstContract = [Select Id, Client__c from Contract where Id In : total_Contract_Id_Set];
if(lstContract != null && lstContract.size() > 0)
{
setClients = new Set<String>();
for(Contract con: lstContract)
{
setClients.add(con.Client__c);
}
}    
} 
System.debug('*****Acquired, Own and Shared Clients:'+setClients);*/
        
        //Favourite Section: Loads when page is accessed from Favouties Section
        favouriteId = ApexPages.currentPage().getParameters().get('favId');
        if(favouriteId!=null && favouriteId!=''){
            if((FavouritesCtrl.fetchFavsListById(favouriteId)).size()>0){
                isFav = true;
                selectedObject = new List<String>();
                selectedClient = '';//new List<String>();
                selectedEventType = new List<String>();
                selectedFromDate = '';//new String();
                selectedToDate = '';//new String();                
                selectedGlobalClient = new List<String>();//added For Global Client
                fetchFavourites();
            }else{
                isFav = false;  
            }
        }else{
            isFav = false;
        } 
        
        //setting up global client fliter
        if(!isFav)
        {
            lstGlobalClientIds = MyGardHelperCtrl.fetchGlobalClientsHelper(loggedInAccountId, UserInfo.getUserId());   
            System.debug('********setGlobalClientIds'+lstGlobalClientIds);     
            if(lstGlobalClientIds != NULL && lstGlobalClientIds.size()>0){
                selectedGlobalClient = new List<String>();//added For Global Client
                for(Account acc:[Select Id,Name from Account where Id IN:lstGlobalClientIds]){
                    selectedGlobalClient.add(acc.Name);//added For Global Client    
                }   
            }  
        }
    }
    
    //page action
    public PageReference viewData()
    {
        System.debug('In View Data Method');
        System.debug('*****sortExpression'+sortExpression);
        System.debug('*****sortDirection'+sortDirection);             
        if(UserType == 'Broker' || UserType == 'Client')
        {
            pageNum = 1;
            populateFilter();
            if(ispageLoad)
            {
                buildQuery();
                isPageLoad = false;
                populateFilter();            
            } 
            searchActivities();
            return null;
        }
        else {
            return page.HomePage;
        }
    }
    
    //string to DateTime
    Public DateTime strToDateTime(String strDate)
    {
        Date dt = GardUtils.formatDate(strDate);
        DateTime dtConvert = dt;
        String formatStr = 'yyyy-MM-dd HH:mm:ss';        
        dtConvert = DateTime.valueOf(dtConvert.format(formatStr));
        return dtConvert;
    }
    
    public void buildQuery()
    {
        strCondition ='';
        /*System.debug('******selectedClient:'+selectedClient);
System.debug('******SelectedObject:'+SelectedObject);
System.debug('******SelectedEventType:'+SelectedEventType);
System.debug('******selectedFromDate:'+selectedFromDate);
System.debug('******selectedToDate:'+selectedToDate);*/
        system.debug('-- prodArea --'+prodArea);
        if(SelectedObject != null && SelectedObject.size()>0){
            List<String> strCheck = new List<String>();            
            for(String str: SelectedObject)
            {
                strCheck.add(' Object_Name__c LIKE \'%'+ str +'%\'');                            
            } 
            String s;
            if(strCheck != null && strCheck.size()>0)
            {
                s = String.join(strCheck,' OR ');
            }
            System.debug('***strCheck'+strCheck);  
            System.debug('***strCheck'+s);  
            strCondition = strCondition + ' AND ('+ s + ')';
        }
        
        if( SelectedEventType != null && SelectedEventType.size()>0){
            strCondition = strCondition +' AND Event_type__c IN : SelectedEventType ';
        }
        
        if(selectedFromDate != null && selectedFromDate != ''){
            dtFromDate = strToDateTime(selectedFromDate);
            System.debug('******FromDate'+dtFromDate);
            strCondition = strCondition + ' AND CreatedDate >= : dtFromDate ';
        }
        
        if(selectedToDate != null && selectedToDate != ''){
            dtToDate = strToDateTime(selectedToDate);
            System.debug('******ToDate'+dtToDate);
            strCondition = strCondition + ' AND CreatedDate <= : dtToDate ';
        }
        System.debug('******strCondition'+strCondition);
        sortFullExp = sortExpression  + ' ' + sortDirection;
        isQueried = true;            
        if(userType == 'Broker')
        {
            if(SelectedClient == null || SelectedClient == ''){
                strSoql = 'SELECT Additional_Details__c, Created_by_account__r.Name, Client_account__c, Client_account__r.Name,Cover_Name__c,Event_type__c,Object_Name__c,CreatedDate,User__r.Name,Broker_Account__c, User__c FROM Client_Activity__c where Client_Account__c In: setClients AND Broker_Account__c =:loggedInAccountId AND (NOT User__r.Name like \'%test%\') AND (NOT User__r.Name  like \'%support%\') AND Client_account__c = \'\''+ strCondition+ ' ORDER BY '+sortFullExp; // Changes done for SF-3990            
            }
            else if(SelectedClient == 'My Activities')
            {
               strSoql = 'SELECT Additional_Details__c, Created_by_account__r.Name, Client_account__c, Client_account__r.Name,Cover_Name__c,Event_type__c,Object_Name__c,CreatedDate,User__r.Name,Broker_Account__c, User__c FROM Client_Activity__c where Client_Account__c = null AND Broker_Account__c =:loggedInAccountId AND (NOT User__r.Name like \'%test%\') AND (NOT User__r.Name  like \'%support%\') '+ strCondition+ ' ORDER BY '+sortFullExp; // Changes done for 3911
            }
            else
            {
                strSoql = 'SELECT Additional_Details__c, Created_by_account__r.Name, Client_account__c, Client_account__r.Name,Cover_Name__c,Event_type__c,Object_Name__c,CreatedDate,User__r.Name,Broker_Account__c, User__c FROM Client_Activity__c where Client_Account__c In: setClients AND Broker_Account__c =:loggedInAccountId AND (NOT User__r.Name like \'%test%\') AND (NOT User__r.Name  like \'%support%\') AND Client_account__r.Name =: SelectedClient'+ strCondition+ ' ORDER BY '+sortFullExp; // Changes done for SF-3990
            }
        }
        else
        {
            strSoql = 'SELECT Additional_Details__c, Created_by_account__r.Name, Client_account__c, Client_account__r.Name,Cover_Name__c,Event_type__c,Object_Name__c,CreatedDate,User__r.Name,Broker_Account__c, User__c FROM Client_Activity__c where Client_account__c =: loggedInAccountId AND (NOT User__r.Name like \'%test%\') AND (NOT User__r.Name  like \'%support%\') '+ strCondition+ ' ORDER BY '+sortFullExp; // Changes done for SF-3990 
        }        
        system.debug('--------loggedInAccountId @@@@@@'+loggedInAccountId);
        system.debug('--------prodArea @@@@@@'+prodArea);
        System.debug('query****'+strSoql);
    }
    
    public void populateFilter()
    {
        //System.debug('******setClientOptions:'+setClientOptions);
        //System.debug('******setEventTypeOptions:'+setEventTypeOptions);
        //System.debug('******setObjectOptions:'+setObjectOptions);
        //System.debug('******selectedClient:'+SelectedClient);
        List<Client_Activity__c> lstActivites = new List<Client_Activity__c>();
        if(isPageLoad)
        {
            if(userType == 'Broker'){
                lstActivites = Database.query('SELECT Additional_Details__c, Created_by_account__r.Name, Client_account__c, Client_account__r.Name,Cover_Name__c,Event_type__c,Object_Name__c,CreatedDate,Broker_Account__c,User__r.Name, User__c FROM Client_Activity__c where Client_Account__c In: setClients AND (NOT User__r.Name like \'%test%\') AND (NOT User__r.Name  like \'%support%\')'); // Changes done for SF-3990            
            }
            else
                lstActivites = Database.query('SELECT Additional_Details__c, Created_by_account__r.Name, Client_account__c, Client_account__r.Name,Cover_Name__c,Event_type__c,Object_Name__c,CreatedDate,Broker_Account__c,User__r.Name, User__c FROM Client_Activity__c where Client_Account__c =: loggedInAccountId AND (NOT User__r.Name like \'%test%\') AND (NOT User__r.Name  like \'%support%\')'); // Changes done for SF-3990           
        }
        else
        {
            buildQuery();
            System.debug('query in populateFilter****'+strSoql);  
            System.debug('SelectedClient in populateFilter****'+SelectedClient);
            lstActivites = Database.query(strSoql);
            System.debug('****size'+lstActivites.size());
            System.debug('****lstActivites'+lstActivites);
        }        
        
        //clear values
        if(setEventTypeOptions != null && setEventTypeOptions.size() >0)
            setEventTypeOptions.clear();        
        if(setObjectOptions != null && setObjectOptions.size() >0)
            setObjectOptions.clear();           
        
        Set<String> setClientOptions1 = new Set<String>();
        for(Client_Activity__c acti : lstActivites)
            if(lstActivites != null && lstActivites.size()>0){
                if(acti.Client_account__r.Name!=null)
                {
                    setClientOptions1.add(acti.Client_account__r.Name);                   
                }                
                if(acti.Object_Name__c!=null)
                {
                    acti.Object_Name__c = acti.Object_Name__c.trim();
                    acti.Object_Name__c = acti.Object_Name__c.replaceAll( ',\\s+', ',');
                    System.debug('***Object Name:'+acti.Object_Name__c);
                    List<String> lstStrObjects = acti.Object_Name__c.split(',');
                    if(lstStrObjects != null && lstStrObjects.size()>0)
                        setObjectOptions.addAll(lstStrObjects);                          
                }
                if(acti.Event_type__c!=null)
                {
                    setEventTypeOptions.add(acti.Event_type__c);                   
                }              
                
            }
        if(selectedGlobalClient != null && selectedGlobalClient.size() > 0)
        {
            for(String str: selectedGlobalClient)
            {
                if(setClientOptions1.contains(str))
                {
                    setClientOptions.add(str);
                }
            }
        }
        else
        {
            setClientOptions.addAll(setClientOptions1);
        }      
        /*System.debug('******setClientOptions:'+setClientOptions);
System.debug('******setEventTypeOptions:'+setEventTypeOptions);*/
        System.debug('******setObjectOptions:'+setObjectOptions);
        //if(ispageLoad){getClientOptions();}
        getClientOptions();
        getObjOptions();
        getEventTypeOptions();
    }
    
    public void searchActivities()
    {
        System.debug('check date values:');
        System.debug('******selectedClient:'+selectedClient);
        //System.debug('toDate:'+selectedToDate);
        if(strSoql.length()>0)
        {
            System.debug('Inside Search Function');
            lstActivities = (List<Client_Activity__c>)setCon.getRecords();            
            if(setCon.getPageNumber() == 1){
                intStartrecord = 1;
            }
            else{
                intStartrecord  = ((setCon.getPageNumber() - 1) * SMARTQUERY_PAGE_SIZE) + 1;
            }
            intEndrecord = setCon.getPageNumber() * SMARTQUERY_PAGE_SIZE;
            if( intEndrecord > noOfRecords ){
                intEndrecord = noOfRecords ;
            }
        }
        lstWrapperData = new List<ClientActivityWrapper>();
        for(Client_Activity__c act: lstActivities)
        {
            lstWrapperData.add(new ClientActivityWrapper
                               (                                   
                                   string.valueof(act.Object_Name__c),
                                   string.valueof(act.Cover_Name__c),
                                   string.valueof(act.Created_by_account__r.Name),                                   
                                   string.valueof(act.Event_Type__c),
                                   string.valueof(act.User__r.Name),
                                   string.valueof(act.Additional_Details__c),
                                   act.CreatedDate
                               )                              
                              );
        }
        System.debug('*****lstActivities:'+lstActivities);
        System.debug('*****lstWrapperData:'+lstWrapperData);
    }
    
    //Generate Value Binded to Table in VF Page
    public List<ClientActivityWrapper> getActivities() 
    {
        return lstWrapperData;
    }
    
    public pageReference clearOptions()
    {
        setClientOptions.Clear();
        setEventTypeOptions.clear();
        setObjectOptions.clear();
        pageReference pg = page.ClientActivity;
        pg.setRedirect(true);
        return pg;
    }
    
    //Pagination - StandardSetController Code
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null || isQueried){
                isQueried = false;
                size = SMARTQUERY_PAGE_SIZE;            
                System.debug('Query in StandardSetController:'+strSoql);
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(strSoql+' '+strLimit));            
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
                system.debug('****Total records****'+noOfRecords);
                if (noOfRecords == null || noOfRecords == 0){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No search results found.'));
                }else if (noOfRecords == 10000){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The search returned 10000 records (maximum allowable limit).'));
                }
            }
            return setCon;
        }set;
    }
    
    //Pagination - To go to a specific page number
    public void setpageNumber()
    {
        setCon.setpageNumber(pageNum);
        searchActivities();
    }
    
    //Pagination - To Check for Next Page
    public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }
    
    //Pagination - To Check for Previous Page
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }
    
    //Pagination - Get/Set specific page number
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }
    
    //Used in Pagination for navigation
    public void navigate(){
        if(nameOfMethod=='previous'){
            
            setCon.previous();
            pageNum=setCon.getpageNumber();
            searchActivities();
        }
        else if(nameOfMethod=='last'){            
            setCon.last();
            pageNum = setCon.getPageNumber();
            searchActivities();
        }
        else if(nameOfMethod=='next'){
            System.debug('PageNum:'+pageNum);
            System.debug('next method');            
            setCon.next();
            pageNum=setCon.getpageNumber();
            System.debug('PageNum:'+pageNum);
            searchActivities();
        }
        else if(nameOfMethod=='first'){
            
            setCon.first();
            pageNum = setCon.getPageNumber();
            searchActivities();
        }
    }
    //Pagination - To go to a First Page
    public void first() {
        nameOfMethod='first';        
        navigate();
    }
    
    //Pagination - To go to a Next Page
    public void next(){
        nameOfMethod='next';
        pageNum = setCon.getPageNumber();
        navigate();
    }
    
    //Pagination - To go to a Last Page
    public void last(){
        nameOfMethod='last';                
        navigate();
    }
    
    //Pagination - To go to a Previous Page
    public void previous(){
        nameOfMethod='previous';
        pageNum = setCon.getPageNumber();
        navigate();
    }
    
    //added For Global Client - Start
    public pageReference fetchSelectedClients(){        
        selectedGlobalClient = MyGardHelperCtrl.fetchSelectedClientsHelper(strSelected);                   
        ViewData();
        
        //Reload the page after globalclient go
        pageReference pg = page.ClientActivity;
        pg.setRedirect(true);
        return pg;
    }
    
    //wrapper class
    public class ClientActivityWrapper
    {
        Public String ObjectName {get; set;}
        Public String Cover {get; set;}
        Public String EventType {get; set;}
        Public String UserName {get; set;}
        Public String additionDetails {get; set;}
        Public String acc {get; set;}
        Public DateTime dt {get; set;}
        
        public ClientActivityWrapper(String objName, String coverName, String createdByAccountName, String event, String uName, String addDetails, DateTime dt1)
        {
            ObjectName = objName;
            Cover = coverName;
            acc = createdByAccountName;
            EventType = event;
            UserName = uName;
            additionDetails = addDetails;
            dt = dt1;
        }
    }
    
    //print method    
    public PageReference print()
    {
        lstAllActivitiesForExcel = new List<List<ClientActivityWrapper>>();
        List<ClientActivityWrapper> lst = new List<ClientActivityWrapper>();
        Integer count = 1;
        noOfData = 0;
        lstWrapperData  = new List<ClientActivityWrapper>();        
        for(Client_Activity__c act : Database.Query(strSoql+' '+strLimit))
        {
            if(count<1000)
            {
                lstWrapperData.add(new ClientActivityWrapper(
                    string.valueof(act.Object_Name__c),
                    string.valueof(act.Cover_Name__c),
                    string.valueof(act.Created_by_account__r.Name),
                    string.valueof(act.Event_type__c),
                    string.valueof(act.User__r.Name), 
                    string.valueof(act.Additional_Details__c),
                    Date.valueof(act.CreatedDate)
                )); 
                count++;
            }   
            else
            {
                lstAllActivitiesForExcel.add(lstWrapperData);
                lstWrapperData = new List<ClientActivityWrapper>();
                lstWrapperData.add(new ClientActivityWrapper(
                    string.valueof(act.Object_Name__c),
                    string.valueof(act.Cover_Name__c),
                    string.valueof(act.Created_by_account__r.Name),
                    string.valueof(act.Event_type__c),
                    string.valueof(act.User__r.Name), 
                    string.valueof(act.Additional_Details__c),
                    Date.valueof(act.CreatedDate)
                ));
                count = 1;
            }
            noOfData++;  
        }
        lstAllActivitiesForExcel.add(lstWrapperData);            
        return page.PrintforClientACtivity;        
    }
    
    //Generate Excel Method
    public PageReference exportToExcelForClientActivity()
    {
        lstAllActivitiesForExcel = new List<List<ClientActivityWrapper>>();
        List<ClientActivityWrapper> lst = new List<ClientActivityWrapper>();
        Integer count = 1;
        noOfData = 0;
        lstWrapperData  = new List<ClientActivityWrapper>();        
        for(Client_Activity__c act : Database.Query(strSoql+' '+strLimit))
        {
            if(count<1000)
            {
                lstWrapperData.add(new ClientActivityWrapper(
                    string.valueof(act.Object_Name__c),
                    string.valueof(act.Cover_Name__c),
                    string.valueof(act.Created_by_account__r.Name),
                    string.valueof(act.Event_type__c),
                    string.valueof(act.User__r.Name), 
                    string.valueof(act.Additional_Details__c),
                    Date.valueof(act.CreatedDate)
                )); 
                count++;
            }   
            else
            {
                lstAllActivitiesForExcel.add(lstWrapperData);
                lstWrapperData = new List<ClientActivityWrapper>();
                lstWrapperData.add(new ClientActivityWrapper(
                    string.valueof(act.Object_Name__c),
                    string.valueof(act.Cover_Name__c),
                    string.valueof(act.Created_by_account__r.Name),
                    string.valueof(act.Event_type__c),
                    string.valueof(act.User__r.Name), 
                    string.valueof(act.Additional_Details__c),
                    Date.valueof(act.CreatedDate)
                ));
                count = 1;
            }
            noOfData++;  
        }
        lstAllActivitiesForExcel.add(lstWrapperData);            
        return page.GenerateExcelForClientActivity;        
    }
    
    //For displaying TimeStamps in Print and Exported Data  
    public Static String getGenerateTimeStamp()
    {
        Datetime myDT = Datetime.now();
        String formatted = myDT.formatGMT('d/MMM/yy HH:mm a');
        return formatted ;
    }
    public Static String getGenerateTime()
    {
        Datetime myDT = Datetime.now();
        String formatted = myDT.formatGMT('EEE MMM d HH:mm:ss yyyy');
        return formatted ;
    }
    public Static String getUserName()
    {
        String name;
        User usr = [Select Id, contactID from User Where Id =: UserInfo.getUserId()]  ;      
        Contact myContact;
        if(usr!= null && usr.contactID != null){
            myContact = [Select AccountId,Name from Contact Where id=: usr.contactID];
            name=myContact.Name;
        }
        return name;
    }
    
     //Added For Favourites - Start
    public void createFavourites(){
        //system.debug('createFavourites');
        //system.debug('newFilterName: '+newFilterName);
        String currentUserId = UserInfo.getUserId();
        List<Extranet_Favourite__c> listExisting = new List<Extranet_Favourite__c>();
        
        String pageName = FavouritesCtrl.fetchPageName(ApexPages.CurrentPage().getUrl());                       
        listExisting.addAll(FavouritesCtrl.fetchExistingFavsList(pageName));
        String itemType = 'Saved Search';
        
        String query = strSoql + ' '+strLimit;
        
        if(newFilterName!=null && newFilterName!=''){
            Extranet_Favourite__c objFav = new Extranet_Favourite__c();
            //added For Global Client - Start
            List<String> listClientIds = new List<String>();
            if(lstGlobalClientIds!=null && lstGlobalClientIds.size()>0){                    
                listClientIds.addAll(lstGlobalClientIds);                   
            }
            //added For Global Client - End             
            objFav = FavouritesCtrl.createFavs(objFav, pageName, newFilterName, itemType, currentUserId, query, listExisting);
            objFav.Filter_Client__c = selectedClient;//FavouritesCtrl.generateFavFilters(selectedClient);            
            objFav.Filter_Object__c = FavouritesCtrl.generateFavFilters(selectedObject);  
            objFav.Filter_Claim_Type__c = FavouritesCtrl.generateFavFilters(selectedEventType); 
            objFav.Filter_Claim_Year__c = selectedFromDate; 
            objFav.Filter_Policy_Year__c = selectedToDate; 
             
            objFav.Filter_Global_Client__c = FavouritesCtrl.generateFavFilters(selectedGlobalClient);//added For Global Client   
            objFav.Filter_Global_Client_Id__c = FavouritesCtrl.generateFavFilters(listClientIds);//added For Global Client   
            insert objFav;
            favouriteId = objFav.Id;
        }   
    }
    
    public void deleteFavourites(){
        //system.debug('deleteFavourites');
        String currentUserId = UserInfo.getUserId();
        List<Extranet_Favourite__c> listFav = new List<Extranet_Favourite__c>();                        
        listFav.addAll(FavouritesCtrl.fetchFavsListById(favouriteId));
        if(listFav.size()>0){
            delete listFav;
        }
    }
    
    public void fetchFavourites(){
        system.debug('fetchFavourites');
        //system.debug('favouriteId: '+favouriteId);
        String currentUserId = UserInfo.getUserId();
        List<Extranet_Favourite__c> listobj = new List<Extranet_Favourite__c>();
        listobj.addAll(FavouritesCtrl.fetchFavsListById(favouriteId));
        system.debug('****listobj:'+listobj);
        system.debug('***selectedClient:'+selectedClient);
        // system.debug('***listobj[0].Filter_Client__c:'+listobj[0].Filter_Client__c);
        // system.debug('***from fav class:'+FavouritesCtrl.fetchFavFilters(listobj[0].Filter_Client__c));
        if(listobj.size()>0){
            selectedClient = listobj[0].Filter_Client__c;
            system.debug('***selectedClient:'+selectedClient);
            selectedObject.addAll(FavouritesCtrl.fetchFavFilters(listobj[0].Filter_Object__c));            
            selectedEventType.addAll(FavouritesCtrl.fetchFavFilters(listobj[0].Filter_Claim_Type__c));
            selectedFromDate = listobj[0].Filter_Claim_Year__c;
            selectedToDate = listobj[0].Filter_Policy_Year__c;
            selectedGlobalClient.addAll(FavouritesCtrl.fetchFavFilters(listobj[0].Filter_Global_Client__c));//added For Global Client               
            //added For Global Client - Start
            if(listobj[0].Filter_Global_Client_Id__c!=null && listobj[0].Filter_Global_Client_Id__c!=''){
                strGlobalClient = listobj[0].Filter_Global_Client_Id__c;    
            }else{
                strGlobalClient = '';
            }                                               
            //added For Global Client - End                 
        }      
    }       
    //Added For Favourites - End
    
    
}