// This class is called from GardExtHomeController to fetch claim timebars
public class fetchClaimTimebarsCtrl {
    public List<TimeBarResponseWrapper> fetchClaimTimebars(List<String> ClientIdList, String endpoint, string clientId, string clientSecret,GardExtHomeController homePgCtrl){
        List<TimeBarResponseWrapper> timeBarWrp = new List<TimeBarResponseWrapper>(); 
        //creating HTTP request
        Http http = new Http();
        HttpRequest request = new HttpRequest();   
        String params = '';
        for(Account acc:[SELECT company_id__c, Id FROM Account WHERE Id IN: ClientIdList]){
            params = params + acc.company_id__c + ',';
        }
        system.debug('---params---'+params);
        //params = '4410';
        params = params.removeEnd(',');
        endpoint = endpoint + params;
        request.setEndpoint(endpoint);
        System.debug('endpoint in class:'+endpoint);   
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('client_id', clientId);
        request.setHeader('client_secret', clientSecret);
        request.setMethod('GET');
        
        try{
            HttpResponse response = http.send(request);
            System.debug('Response:'+response);
            System.debug('Response body:'+response.getBody());
            System.debug('Serialized response body:'+JSON.serialize(response.getBody().remove(response.getBody().substring(1,3))));
            // If the request is successful, parse the JSON response.    
            if (response.getStatusCode() == 200){
                System.debug('Success Occurred with status code: '+response.getStatusCode());
                timeBarWrp = (List<TimeBarResponseWrapper>)JSON.deserialize(response.getBody(),List<TimeBarResponseWrapper>.class);
                System.debug('--deserialized results--'+timeBarWrp);   
                System.debug('deserializedDocumentResponse : '+ System.JSON.serialize(timeBarWrp));
                homePgCtrl.isServiceAvailable = true;
                system.debug('---is service available---'+homePgCtrl.isServiceAvailable);
                //throw new CalloutException();
            }
            else{
                homePgCtrl.isServiceAvailable = false;
            }
            //in case status code is not 200 (It could be 400, 404, 405, 406, 415, 500)
        }        
        catch(Exception e)
        {
            homePgCtrl.isServiceAvailable = false;
            System.debug('homePageCtrl.isServiceAvailable--'+homePgCtrl.isServiceAvailable);
            System.debug('Exception:'+e);
        }
        return timeBarWrp;
    }
}