//Generated by wsdl2apex

public class OBIEEReports {
    public class execute_pt {
        //public String endpoint_x = 'https://soa-test.gard.no/soa-infra/services/Reports/ReportsFetcher/ReportsService';
        PortfolioReportsCtrl_for_Marine_endpoint__c wsdlGardNoV10Reportsservice_CS = PortfolioReportsCtrl_for_Marine_endpoint__c.getValues('endpoint');            
        String endpnt = wsdlGardNoV10Reportsservice_CS.Endpoint__c;
        public String endpoint_x = endpnt;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://wsdl.gard.no/v1_0/ReportsService', 'OBIEEReports', 'http://messages.gard.no/v1_0/ReportResponse', 'OBIEEReportResponse', 'http://messages.gard.no/v1_0/ReportParameters', 'OBIEEReportParameters'};
        public String execute(Integer ClientId,String ReportName,String ParameterString,String UserAgent,Boolean IsBroker,Boolean ShowPeopleClaim) {
            OBIEEReportParameters.ReportParametersBase request_x = new OBIEEReportParameters.ReportParametersBase();
            request_x.ClientId = ClientId;
            request_x.ReportName = ReportName;
            request_x.ParameterString = ParameterString;
            request_x.UserAgent = UserAgent;
            request_x.IsBroker = IsBroker;
            request_x.ShowPeopleClaim = ShowPeopleClaim;
            OBIEEReportResponse.ReportResponseBase response_x;
            Map<String, OBIEEReportResponse.ReportResponseBase> response_map_x = new Map<String, OBIEEReportResponse.ReportResponseBase>();
            response_map_x.put('response_x', response_x);
            if(Test.isRunningTest()) { return 'Test Response'; } // Hard coded Mock Response
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'execute',
              'http://messages.gard.no/v1_0/ReportParameters',
              'ReportParameters',
              'http://messages.gard.no/v1_0/ReportResponse',
              'ReportResponse',
              'OBIEEReportResponse.ReportResponseBase'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.Report;
        }
    }
}