@isTest
private class ApprovalProcessTest {
      
    static testMethod void approvalSubmissionFailsIfEpiIsZero() {
        
        Opportunity opportunity = TestDataGenerator.getOpportunity('P&I', 'Risk Evaluation', 'Small Craft');
        opportunity = TestDataGenerator.changeApprovalCriterion(opportunity, 'Self Approval');
        opportunity = TestDataGenerator.changeEpi(opportunity, 0); // Default is >0
        opportunity = TestDataGenerator.refreshOpportunity(opportunity);
        
        System.assert(opportunity.Risk_Evaluation_Status__c != 'Ready for Approval', 'EPI 0 was submittable for approval');
    }
    
    
    static testMethod void approvalSubmissionSucceedsIfEpiIsGreaterThanZero() {

        Opportunity opportunity = TestDataGenerator.getOpportunity('P&I', 'Risk Evaluation', 'Small Craft');
        opportunity = TestDataGenerator.changeApprovalCriterion(opportunity, 'Self Approval');
        opportunity = TestDataGenerator.refreshOpportunity(opportunity);
        
        System.assert(opportunity.Risk_Evaluation_Status__c == 'Ready for Approval', 'EPI >0 was not submittable for approval');
        
        User user =  TestDataGenerator.getUnderwriterUser();
        Approval.ProcessSubmitRequest submitRequest = new Approval.ProcessSubmitRequest();
        submitRequest.setObjectId(opportunity.Id);
        submitRequest.setSubmitterId(user.Id);      
        Approval.ProcessResult result = Approval.process(submitRequest);
        
        System.assert(result.isSuccess(), 'EPI >0 was not submittable for approval');
    }
    
    /*
        The below approval criteria tests for Marine and P&I Small Craft are split into several methods
        due to a governance limit of 100 SOQL invocations per context.
    */
    static testMethod void testSmallCraftMarinePart1() {
        
    //| Record type                 | Approval Criteria                       | AM   | SVP  | PM   |
        testSmallCraftMarineScenario('Self Approval',                           true,  false, false);
        testSmallCraftMarineScenario('Other outside guidelines',                true,  true,  false);
        testSmallCraftMarineScenario('Vessel > 1500 GT',                        true,  false, false);
        testSmallCraftMarineScenario('RA < 90%',                                true,  false, false);
        testSmallCraftMarineScenario('RA < 75%',                                true,  true,  false);
        testSmallCraftMarineScenario('Gard Exposure > $5000000',                true,  true,  false);
        
    }
    
    static testMethod void testSmallCraftMarinePart2() {
    
    //| Record type                 | Approval Criteria                       | AM   | SVP  | PM   |
        testSmallCraftMarineScenario('Deviation from trading area',             true,  true,  false);
        testSmallCraftMarineScenario('Non Standard T&Cs',                       true,  true,  true);
        testSmallCraftMarineScenario('Multiple year deal',                      true,  true,  false);       
        testSmallCraftMarineScenario('Small craft Marine outside Scandinavia',  true,  true,  false);
        testSmallCraftMarineScenario('Deviation from min. volume requirement',  true,  true,  false);       
    }
    
    
    static testMethod void testSmallCraftPAndIPart1() {

    //| Record type                 | Approval Criteria                       | AM   | SVP  | PM   |
        testSmallCraftPAndIScenario('Self Approval',                            true,  false, false);
        testSmallCraftPAndIScenario('Other outside guidelines',                 true,  true,  false);
        testSmallCraftPAndIScenario('Vessel > 1500 GT',                         true,  true,  false);
        testSmallCraftPAndIScenario('RA < 90%',                                 true,  false, false);
        testSmallCraftPAndIScenario('RA < 75%',                                 true,  true,  false);
        //testSmallCraftPAndIScenario('Deviation from trading area',              true,  true,  false);
    }
    
    
    static testMethod void testSmallCraftPAndIPart2() {

    //| Record type                 | Approval Criteria                       | AM   | SVP  | PM   |
        testSmallCraftPAndIScenario('Non Standard T&Cs',                        true,  true,  true);
        testSmallCraftPAndIScenario('Multiple year deal',                       true,  true,  false);       
        testSmallCraftPAndIScenario('Small craft P&I outside Scandinavia',      true,  true,  false);
        testSmallCraftPAndIScenario('Deviation from min. volume requirement',   true,  true,  false);
        testSmallCraftPAndIScenario('Non-renewal',                              true,  true,  false);
    }
    
    
    private static void testSmallCraftPAndIScenario(String approvalCriterion, Boolean am, Boolean svp, Boolean pm) {
    
        Opportunity opportunity = TestDataGenerator.getOpportunity('P&I', 'Risk Evaluation', 'Small Craft');
        testSmallCraftScenario(opportunity, approvalCriterion, am, svp, pm);
    }
    
        
    private static void testSmallCraftMarineScenario(String approvalCriterion, Boolean am, Boolean svp, Boolean pm) {
        /* commented out for prod migration phase I
        Opportunity opportunity = TestDataGenerator.getOpportunity('Marine', 'Risk Evaluation', 'Small Craft');
        testSmallCraftScenario(opportunity, approvalCriterion, am, svp, pm);
        */
    }


    private static void testSmallCraftScenario(Opportunity Opportunity, String approvalCriterion, Boolean am, Boolean svp, Boolean pm) {
        /* commented out for production migration phase I
        
        opportunity = TestDataGenerator.changeApprovalCriterion(opportunity, approvalCriterion);            
    
        // Check approvers
        System.assert((opportunity.Area_Manager__c != null) == am, 'Incorrect Area Manager in scenario ' + approvalCriterion);
        System.assert((opportunity.PM_Approver__c != null) == pm, 'Incorrect Product Manager in scenario ' + approvalCriterion);
        System.assert((opportunity.Senior_Approver__c != null) == svp, 'Incorrect svp in scenario ' + approvalCriterion);
        
        // Check that approval submission doesn't auto reject the opportunity
        User user =  TestDataGenerator.getUnderwriterUser();
        Approval.ProcessSubmitRequest submitRequest = new Approval.ProcessSubmitRequest();
        submitRequest.setObjectId(opportunity.Id);
        submitRequest.setSubmitterId(user.Id);      
        Approval.ProcessResult result = Approval.process(submitRequest);

        opportunity = TestDataGenerator.refreshOpportunity(opportunity);        
        System.assert(opportunity.Approval_Process_ID__c == '04aD0000000TOsn', 'Unexpected approval process Id: ' + opportunity.Approval_Process_ID__c);
        
        System.assert(result.isSuccess(), approvalCriterion + ' could not be submitted for approval');      
        System.assert(result.getInstanceStatus() != 'Rejected', approvalCriterion + ' was rejected during submission');
        */
    }   
}