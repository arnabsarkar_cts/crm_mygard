@isTest(seeAllData=false)
Public class TestPortfolioFormsCtrl
{
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
    
    static testmethod void PortfolioFormsCtrl(){
        conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
        conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
        conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
        conFieldsList.add(conSubFields);
        conFieldsList.add(conSubFields2);
        conFieldsList.add(conSubFields3);
        insert conFieldsList; 
        
        Attachment att = new Attachment();
        gardtestdata test_rec= new gardtestdata();
        test_rec.commonrecord();
        test_rec.customsettings_rec();
        
        Forms_List__c Forms=new Forms_List__c(Type__c='Claim forms');
        insert Forms;
          
        Blob b = Blob.valueOf('Brokers Data');  
        Attachment attachment1 = new Attachment();  
        attachment1.ParentId = Forms.Id;
        attachment1.Name = 'Application form - Charterers.pdf';  
        attachment1.Body = b;  
        insert(attachment1); 
        MLC_Certificate_availability__c MLC_Certificate_CS_Obj = new MLC_Certificate_availability__c(Name = 'MLC availability', value__c = false);       
        insert MLC_Certificate_CS_Obj ;    
        
        //Blue_Card_renewal_availability__c CSBCAvailability = new Blue_Card_renewal_availability__c(name = 'value', BlueCardRenewalAvailability__c = true); 
        //insert CSBCAvailability;  
        //Invoking methods of Portfolioformsctrl for broker and client users.......
 		Test.startTest();
        
        System.runAs(gardtestdata.clientUser)         
        {
           // Test.startTest();
            PortfolioFormsCtrl client_user = new PortfolioFormsCtrl();
            client_user.lstContact = new List<Contact>();
           // client_user.currentFileId = String.valueof(attachment.id);
            client_user.cancelForUwrForms();
            List<SelectOption> clientOptions =  client_user.getClientLst();
            //broker_user.strGlobalClient=brokerAcc.id+';'+brokerAcc.id;
            client_user.DisplayOnline(); 
            
            
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment 1';
            Blob bodyBlob1a=Blob.valueOf('Unit Test Attachment Body 1');
            client_user.attachment.body=bodyBlob1a;
            client_user.uwFormType = 'uw-ship-owner-form-option';   
            client_user.uploadFileForAll();
            
            
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachmen t2';
            Blob bodyBlob2b=Blob.valueOf('Unit Test Attachment Body 2');
            client_user.attachment.body=bodyBlob2b;
            client_user.uwFormType = 'uw-char-vesl-form-option';
            client_user.uploadFileForAll();
            
            

            
            /*  
            
           
             client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment 11';
            Blob bodyBlob11a=Blob.valueOf('Unit Test Attachment Body 11');
            client_user.attachment.body=bodyBlob11a;
            client_user.uwFormType = 'uw-lou-loi-option';
            client_user.uploadFileForAll();
            
            
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment 12';
            Blob bodyBlob12a=Blob.valueOf('Unit Test Attachment Body 12');
            client_user.attachment.body=bodyBlob12a;
            client_user.uwFormType = 'uw-lou-loi-MOU-option';
            client_user.uploadFileForAll();

            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment 14';
            Blob bodyBlob14z=Blob.valueOf('Unit Test Attachment Body 14');
            client_user.attachment.body=bodyBlob14z;
            client_user.uwFormType = 'uw-mlc-certificate';
            client_user.uploadFileForAll();
            
            */
            
			/*
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment 3';
            Blob bodyBlob3=Blob.valueOf('Unit Test Attachment Body 3');
            client_user.attachment.body=bodyBlob3;
            client_user.uwFormType = 'uw-mou-form-option';
            client_user.uploadFileForAll();
            
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment 4';
            Blob bodyBlob4=Blob.valueOf('Unit Test Attachment Body 4');
            client_user.attachment.body=bodyBlob4;
            client_user.uwFormType = 'uw-off-obj-form-option';
            client_user.uploadFileForAll();
            
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment 5';
            Blob bodyBlob5=Blob.valueOf('Unit Test Attachment Body 5');
            client_user.attachment.body=bodyBlob5;
            client_user.uwFormType = 'uw-for-free-pass-option';
            client_user.uploadFileForAll();
            

            
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment 7';
            Blob bodyBlob7=Blob.valueOf('Unit Test Attachment Body 7');
            client_user.attachment.body=bodyBlob7;
            client_user.uwFormType = 'uw-charter-client-form-option';
            client_user.uploadFileForAll();
            

            

            
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment 10';
            Blob bodyBlob10=Blob.valueOf('Unit Test Attachment Body 10');
            client_user.attachment.body=bodyBlob10;
            client_user.uwFormType = 'uw-ri-declaration-form-option';
            client_user.uploadFileForAll();
            

            
            
            client_user.attachment =new Attachment();
            client_user.attachment.Name='Unit Test Attachment 13';
            Blob bodyBlob13=Blob.valueOf('Unit Test Attachment Body 13');
            client_user.attachment.body=bodyBlob13;
            client_user.uwFormType = 'uw-marine-layup-form-option';
            client_user.uploadFileForAll();
            

            */
            
            PageReference pageRef = Page.PortfolioForms;
            Test.setCurrentPage(pageRef);
            client_user.displayDoc();
           // client_user.showLOUForms();
            System.assertEquals(client_user.hasPiCoverOnrisk , false);
            client_user.caseId = gardtestdata.clientCase.id;
            client_user.setGlobalClientIds = new List<ID>{gardtestdata.clientContact.id};
           // client_user.redirectQuestionnaire();
          //  client_user.showLOUForms();
            client_user.displayDoc();
            client_user.selectedClient = gardtestdata.clientAcc.id;
            client_user.fetchSelectedClients();
            //client_user.currentFileId = gardtestdata.brokerAttachment1.id;
            client_user.forceDownload();
            
            
           // Test.stopTest();
           
            /*
         	client_user.uwFormType = 'uw-ship-owner-form-option'; 
            client_user.uploadFileForAll();
            
            client_user.uwFormType = 'uw-char-vesl-form-option'; 
            client_user.uploadFileForAll();
            client_user.uwFormType = 'uw-mou-form-option'; 
            client_user.uploadFileForAll();
            client_user.uwFormType = 'uw-off-obj-form-option'; 
            client_user.uploadFileForAll();                       
            client_user.uwFormType = 'uw-for-free-pass-option'; 
            client_user.uploadFileForAll();
            client_user.uwFormType = 'uw-charter-client-form-option'; 
            client_user.uploadFileForAll();
            client_user.uwFormType = 'uw-itopf-form-option'; 
            client_user.uploadFileForAll();
             client_user.uwFormType = 'uw-pi-layup-form-option'; 
            client_user.uploadFileForAll();
             client_user.uwFormType = 'uw-ri-declaration-form-option'; 
            client_user.uploadFileForAll();
             client_user.uwFormType = 'uw-lou-loi-option'; 
            client_user.uploadFileForAll();
             client_user.uwFormType = 'uw-lou-loi-MOU-option'; 
            client_user.uploadFileForAll();
            client_user.uwFormType = 'uw-marine-layup-form-option'; 
            client_user.uploadFileForAll();
             client_user.uwFormType = 'uw-mlc-certificate'; 
            client_user.uploadFileForAll();
            
            //pfc.uwFormType = 'uw-marine-layup-form-option'; 
            //pfc.uploadFileForAll();
            client_user.strSelected=gardtestdata.clientAcc.id+';'+gardtestdata.clientAcc.id; 
            client_user.fetchSelectedClients();
            client_user.sendToForm();
            PortfolioFormsCtrl.NewAttachmentWrapper newobj = new PortfolioFormsCtrl.NewAttachmentWrapper('originalName', 'base64Value', att);
            //client_user.currentFileId = 'Application form - Charterers.pdf';
            client_user.mapIdForms.put('Application form - Charterers.pdf',attachment1.id);
            client_user.forceDownload();
			*/
        }
      /*  System.runAs(gardtestdata.brokerUser)  
        {
            PortfolioFormsCtrl client_user=new PortfolioFormsCtrl();           
        } */
        Test.stopTest();
    }
}