@isTest
public class TestAccountHelper {
    public static Account clientAcc;
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Market_Area__c Markt;
    public static User salesforceLicUser;   	
    public static string utfStr =  'UTF-8';
    public static string enUsrStr =  'en_US';
    public static string tymZnStr =   'America/Los_Angeles';
    static Country__c country;
    private static void createUsers(){
         AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        Gard_Team__c gt = new Gard_Team__c(Active__c = true,office__c = 'test',name = 'gTeam',region_Code__c = 'test');
        insert gt;
        country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA');
        insert country;
        
        salesforceLicUser = new User(
                                Alias = 'standt', 
                                profileId = salesforceLicenseId ,
                                Email='standarduser@testorg.com',
                                EmailEncodingKey='UTF-8',
                                CommunityNickname = 'test13',
                                LastName='Testing',
                                LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US',  
                                TimeZoneSidKey='America/Los_Angeles',
                                UserName='test008@testorg.com'
                                     );
        
        insert salesforceLicUser; 
    }
   
    private static void createAccount(){
        Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt; 
        createUsers();        
        
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrcClient);
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        vrcList.add(vrcBroker);
        Valid_Role_Combination__c vrcBroker2 = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Insurance Broker');
        vrcList.add(vrcBroker2);
        insert vrcList;
      // createPEMEDebitNoteDetail();
      //String PEMEDebit='a2g9E000000K4gS';
        User dummyUser = new User(
                                    Alias = 'standt', 
                                    profileId = salesforceLicenseId ,
                                    Email='standarduser@testorg.com',
                                    EmailEncodingKey= utfStr ,
                                    CommunityNickname = 'test3',
                                    LastName='Testing',
                                    LanguageLocaleKey= enUsrStr ,
                                    LocaleSidKey= enUsrStr ,  
                                    TimeZoneSidKey= tymZnStr ,
                                    UserName='dummyUser@testorg.com.mygard'                                                                   
                                 );
        insert dummyUser;
        /*PEME_Enrollment_Form__c pef=new PEME_Enrollment_Form__c(Enrollment_Status__c = 'Under Approval',
                                                               ClientName__c =,
                                                               Submitter__c = );*/
        
        /*PEME_Manning_Agent__c peme = new PEME_Manning_Agent__c(Company_Name__c = 'ManAgentComp',
                                                                           Requested_Contact_Person__c = 'ManAgentContact',
                                                                           Requested_Address__c = 'address1',
                                                                           Requested_Address_Line_2__c = 'address2',
                                                                           Requested_Address_Line_3__c = 'address3',
                                                                           Requested_Address_Line_4__c = 'address4',
                                                                           Requested_City__c = 'city',
                                                                           Requested_Zip_Code__c = 'zip001',
                                                                           Requested_State__c = 'state',
                                                                           Requested_Country__c = 'country',
                                                                           Requested_Email__c = 'man@email.com',
                                                                           Requested_Phone__c = '42512' 
                                                                           );
        insert peme;*/
        clientAcc = new Account( Name = 'Test_1', 
                                Site = '_www.test_1.se', 
                                Type = 'Client', 
                                BillingStreet = 'Gatan 1',
                                BillingCity = 'Stockholm', 
                                BillingCountry = 'SWE', 
                                BillingPostalCode = 'BS1 1AD',
                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                Company_Role_Text__c='Client',
                                Market_Area__c = Markt.id,
                                Area_Manager__c = salesforceLicUser.id,
                                company_Role__c =  'Client;Broker' ,
                                Sub_Roles__c = 'Broker - Reinsurance Broker;Broker - Insurance Broker',
                                OwnerId = salesforceLicUser.id,
                                P_I_Member_Flag__c = true,
                                Company_Status__c = 'Active',
                                Client_On_Risk_Flag__c = true,
                                Underwriter_main_contact__c = dummyUser.id,
                                Claim_handler_Defence__c = dummyUser.id,
                                Claim_handler_Cargo_Dry__c = dummyUser.id,
                                Claim_handler_Cargo_Liquid__c = dummyUser.id,
                                Claim_handler_CEP__c = dummyUser.id,
                                Claim_handler_Crew__c = dummyUser.id,
                                Claim_handler_Energy__c = dummyUser.id,
                                Claim_handler_Marine__c = dummyUser.id,
                                Claims_handler_Builders_Risk__c = dummyUser.id,
                                Claim_Adjuster_Marine_lk__c = dummyUser.id,
                                Claim_handler_Cargo_Dry_2_lk__c = dummyUser.id,
                                Claim_handler_Cargo_Liquid_2_lk__c = dummyUser.id,                                
                                Claim_handler_CEP_2_lk__c = dummyUser.id,
                                Claim_handler_Crew_2_lk__c = dummyUser.id,
                                Claim_handler_Defence_2_lk__c = dummyUser.id,
                                Claim_handler_Energy_2_lk__c = dummyUser.id,
                                Claim_handler_Marine_2_lk__c = dummyUser.id,
                                Claims_handler_Builders_Risk_2_lk__c = dummyUser.id,
                                Key_Claims_Contact__c = dummyUser.id,
                                Synchronisation_Status__c = 'Sync Failed',
                                notifyAdmin__c = false,
                                country__c = country.id
                               );
        insert clientAcc;
        Contact clientContact = new Contact( 
                                             FirstName='medicalTest',
                                             LastName='medical',
                                             MailingCity = 'London',
                                             MailingCountry = 'United Kingdom',
                                             MailingPostalCode = 'SE1 1AD',
                                             MailingState = 'London',
                                             MailingStreet = '1 London Road',
                                             AccountId = clientAcc.Id,
                                             Email = 'test321@gmail.com',
            								 Primary_Contact__c = true
                                           );
        insert clientContact;
        PEME_Enrollment_Form__c pef = new PEME_Enrollment_Form__c ();
        //pef1 = new PEME_Enrollment_Form__c ();
        pef.ClientName__c=clientAcc.ID;
        pef.Comments__c='Report analyzed';
        pef.Enrollment_Status__c= 'Draft';
        pef.Gard_PEME_References__c='Test References,  ';
        pef.Last_Status_Change_Date__c=Date.valueOf('2016-01-27');
        pef.Submitter__c = clientContact.ID;
        pef.DisEnrolled_Expire_Date__c=System.today().adddays(3);
        insert pef;
        
        Test.startTest();
        PEME_Debit_note_detail__c pdn = new PEME_Debit_note_detail__c();
        pdn.Comments__c = 'testing';
        pdn.Company_Name__c=clientAcc.ID;
        pdn.Contact_person_Email__c = 'test@mail.com';
        pdn.Contact_person_Name__c = 'tester';
        //pdn.Country__c ='testCountry';
        pdn.PEME_Enrollment_Form__c = pef.ID;
        pdn.Requested_Address__c='test1';
        pdn.Requested_Address_Line_2__c='test2';
        pdn.Requested_Address_Line_3__c='test3';
        pdn.Requested_Address_Line_4__c='test4';
        pdn.Requested_City__c='testCity';
        pdn.RequestedComments__c = 'testing';
        pdn.Requested_Contact_person_Email__c = 'test@mail.com';
        pdn.Requested_Contact_person_Name__c = 'tester';
        pdn.Requested_Country__c ='testCountry';
        pdn.Requested_State__c='testState';
        pdn.Requested_Zip_Code__c='testZip';
        pdn.Status__c='Pending Update';
        insert pdn;
        /*PEME_Manning_Agent__c pmg=new PEME_Manning_Agent__c  ();
        pmg.Client__c=clientAcc.ID;
        pmg.PEME_Enrollment_Form__c=pef.ID;
        pmg.Company_Name__c='Test Company';
        //  pmg.Contact_Person__c='Test Person';
        pmg.Contact_Point__c=clientContact.Id;
        pmg.Status__c='Approved';
        insert pmg;*/
        clientAcc.Manning_Agent_or_Debit_Note_ID__c = pdn.id;
        clientAcc.Company_role__c = 'Client;Broker';
        clientAcc.Sub_Roles__c = 'Broker - Reinsurance Broker;Broker - Insurance Broker';
        /*List<Account> accList = new List<Account>{clientAcc};
        Account clientAcc2 = new Account( Name = 'Test_1', 
                                Site = '_www.test_1.se', 
                                Type = 'Client', 
                                BillingStreet = 'Gatan 1',
                                BillingCity = 'Stockholm', 
                                BillingCountry = 'SWE', 
                                BillingPostalCode = 'BS1 1AD',
                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                Company_Role_Text__c='Client',
                                Market_Area__c = Markt.id,
                                Area_Manager__c = salesforceLicUser.id,
                                company_Role__c =  'Client' ,
                                OwnerId = salesforceLicUser.id,
                                P_I_Member_Flag__c = true,
                                Company_Status__c = 'Active',
                                Client_On_Risk_Flag__c = true,
                                Underwriter_main_contact__c = dummyUser.id,
                                Claim_handler_Defence__c = dummyUser.id,
                                Claim_handler_Cargo_Dry__c = dummyUser.id,
                                Claim_handler_Cargo_Liquid__c = dummyUser.id,
                                Claim_handler_CEP__c = dummyUser.id,
                                Claim_handler_Crew__c = dummyUser.id,
                                Claim_handler_Energy__c = dummyUser.id,
                                Claim_handler_Marine__c = dummyUser.id,
                                Claims_handler_Builders_Risk__c = dummyUser.id,
                                Claim_Adjuster_Marine_lk__c = dummyUser.id,
                                Claim_handler_Cargo_Dry_2_lk__c = dummyUser.id,
                                Claim_handler_Cargo_Liquid_2_lk__c = dummyUser.id,                                
                                Claim_handler_CEP_2_lk__c = dummyUser.id,
                                Claim_handler_Crew_2_lk__c = dummyUser.id,
                                Claim_handler_Defence_2_lk__c = dummyUser.id,
                                Claim_handler_Energy_2_lk__c = dummyUser.id,
                                Claim_handler_Marine_2_lk__c = dummyUser.id,
                                Claims_handler_Builders_Risk_2_lk__c = dummyUser.id,
                                Key_Claims_Contact__c = dummyUser.id,
                                Synchronisation_Status__c = 'Sync Failed',
                                notifyAdmin__c = false,
                                country__c = country.id,
                                Manning_Agent_or_Debit_Note_ID__c = pdn.id
                               );
        accList.add(clientAcc2);*/
        //upsert accList;
        update clientAcc;
    }
    static testMethod void testUpdateAccountRecordForNotifyAdminWithFalse(){
        TestAccountHelper.createAccount();
        List<Id> allAccountIds = new List<Id>();
        allAccountIds.add(clientAcc.Id);
        List<Account> accList = new List<Account>();
        accList.add(clientAcc);
        Account clientAccOld = clientAcc;
        clientAccOld.P_I_Member_Flag__c = false;
        Map<id,Account> oldAccMap = new Map<id,Account>();
        oldAccMap.put(clientAcc.Id,clientAccOld);
        //Test.startTest();
        AccountHelper.updateAccountRecordForNotifyAdmin(false,allAccountIds);
        AccountHelper.addToAccountFollowersList(accList,oldAccMap);
        AccountHelper.accountAfterUpdate(accList,oldAccMap);
        AccountHelper.accountBeforeUpdate(accList,oldAccMap);
        Test.stopTest();
        
        Account fetchedAccount = [SELECT ID,notifyAdmin__c FROM ACCOUNT WHERE ID =: clientAcc.ID];
        System.assertEquals(false,fetchedAccount.notifyAdmin__c);
    }
}