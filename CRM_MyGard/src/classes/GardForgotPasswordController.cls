/****************************************************************************************************************************
*    Deveployed By   :    Cognizant Technology Solution
*    Created Date    :    6/03/2015 
*    Descriptions    :    MyGard portal login has been controll from this class.
*    Modification Log
*    --------------------------------------------------------------------------------------------------------
*    Developer                                Date                        Description
*    --------------------------------------------------------------------------------------------------------
*    Arindam Ganguly                          6/03/2015                   Custom forgot password for MyGard(portal) user
*    Arindam Ganguly                          17/11/2016                  Empty space in last char not accepted in username SF-259 
*****************************************************************************************************************************/
public without sharing class GardForgotPasswordController  
{
 
    public String username_1{ get; set; }
    public String error_message{ get; set; }
   
    public PageReference Submit()
    {
     //Pattern p = Pattern.compile('^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$');
     //Pattern p = Pattern.compile('^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w-]+\\.)+[\\w-]+[\\w-]$');
       //SF-259
       if(username_1 != null)
        username_1 = username_1.trim();
     Pattern p = Pattern.compile('^[a-zA-Z0-9.!#$%&*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$');
     Matcher m = p.matcher(username_1);
     
     List<User> Listuser;
     System.debug('NAme............'+username_1);
     Listuser = new List<User>([select id, Name from User where Username =:username_1]);
     System.debug('User details are ' +Listuser);
     if(m.matches())
     {
        for(User usr : Listuser)
        {
           // System.resetPassword(usr.Id,true);
           boolean success = Site.forgotPassword(username_1);
        }
     error_message = null;
     PageReference newPage = new PageReference('/GardForgotPasswordConfirm');
     newPage.setRedirect(true);
     return newPage;
          }
     else
     {
     error_message = 'Invaild Username';
     return null;
     }
    }
}