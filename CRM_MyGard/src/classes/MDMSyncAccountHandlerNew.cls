/**************************************************************************
* Author  : Arpan Muhuri
* Company : Cognizant
* Date    : 05/03/2018
***************************************************************************/
    
/// <summary>
///  Handler class for the Account.MDMSyncAccount trigger
/// </summary>
public without sharing class MDMSyncAccountHandlerNew {
    
        static String SOAPNS { 
        get {
            if (Test.isRunningTest()) {
                return 'http://schemas.xmlsoap.org/soap/envelope/';
            } else {
                return MDMConfig__c.getInstance().SOAPNS__c;
            }
        }
    }
    static String SOAPNS_Prefix { 
        get {
            if (Test.isRunningTest()) {
                return 'soapenv';
            } else {
                return MDMConfig__c.getInstance().SOAPPrefix__c;
            }
        }
    }
    public static DOM.XMLNode SyncPartner(List<Account> newAccounts, Map<id, Account> newAccountsMap, Boolean webServicesEnabled) {
        List<id> accountIds = new List<id>();
        DOM.XMLNode requestPartner;
        accountIds.addAll(newAccountsMap.keyset());
        if (webServicesEnabled) {
            if (!System.IsBatch() && !System.IsFuture()) {
                requestPartner = MDMPartnerProxyNew.MDMUpsertPartnerAsync(accountIds);
            }
        }
        return requestPartner;
    }
    public static DOM.XMLNode SyncPartner(List<Account> newAccounts, Map<id, Account> newAccountsMap, List<Account> oldAccounts, Map<id, Account> oldAccountsMap, Boolean webServicesEnabled) {
        DOM.XMLNode requestPartner; //for capturing partner request xml
        if (webServicesEnabled) {
            //***The section below is for a Webservice that can handle a list of Accounts***//
            list<id> UpsertAccountIds = new list<id>();
            list<id> DeleteAccountIds = new list<id>();
            
            for(account a : newAccounts) {
                account oldAccount = oldAccountsMap.get(a.ID);
                system.debug('*** After Update a.syncstatus = '+a.synchronisation_Status__c);
                system.debug('*** After Update oldAccount.syncstatus = '+oldAccount.synchronisation_Status__c);                 
                
                if (a.synchronisation_Status__c == 'Sync In Progress' && oldAccount.Synchronisation_Status__c != 'Sync In Progress'){
                    if (a.Deleted__c) {
                        system.debug('*** After Update (DELETE) calling sync for Account : '+a.id); 
                        DeleteAccountIds.add(a.id); 
                    } else {
                        system.debug('*** After Update (UPSERT) calling sync for Account : '+a.id); 
                        UpsertAccountIds.add(a.id); 
                    }
                }
            }
            
            if (UpsertAccountIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    requestPartner = MDMPartnerProxyNew.MDMUpsertPartnerAsync(UpsertAccountIds);
                }
            }
            
            //added for SF-4226
             if (DeleteAccountIds.size() > 0) {
                 system.debug('Deleted ids: '+DeleteAccountIds);
                if (!System.IsBatch() && !System.IsFuture()) {
                    MDMPartnerProxy.MDMDeletePartnerAsync(DeleteAccountIds);
                }
            }
            //SF-4226 ends
            
        }
        return requestPartner ;
    }
    
    public static DOM.XMLNode SyncBillingAddresses(List<Account> newAccounts, Map<id, Account> newAccountsMap, Boolean webServicesEnabled) {
        list<id> UpsertBillingAddressIds = new list<id>();
        DOM.XMLNode requestAddress;
        if (webServicesEnabled) {   
            for(account a : newAccounts) {
                if (a.Billing_Address_Sync_Status__c == 'Sync In Progress') {
                    system.debug('*** After Update (UPSERT) calling sync for Billing Address : '+a.id); 
                    UpsertBillingAddressIds.add(a.id);  
                }
            }
            
            if (UpsertBillingAddressIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    requestAddress = MDMAddressProxyNew.MDMUpsertBillingAddressAsync(UpsertBillingAddressIds);
                }
            }
        }
        return requestAddress;
    }
    public static DOM.XMLNode SyncShippingAddresses(List<Account> newAccounts, Map<id, Account> newAccountsMap, Boolean webServicesEnabled) {
        list<id> UpsertShippingAddressIds = new list<id>();
        DOM.XMLNode requestAddress;
        if (webServicesEnabled) {   
            for(account a : newAccounts) {
                if (a.Shipping_Address_Sync_Status__c == 'Sync In Progress') {
                    system.debug('*** After Update (UPSERT) calling sync for Shipping Address : '+a.id);    
                    UpsertShippingAddressIds.add(a.id); 
                }
            }
            if (UpsertShippingAddressIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    requestAddress = MDMAddressProxyNew.MDMUpsertShippingAddressAsync(UpsertShippingAddressIds);
                }
            }
        }
        return requestAddress;
    }
    
    public static DOM.XMLNode SyncBillingAddresses(List<Account> newAccounts, Map<id, Account> newAccountsMap, List<Account> oldAccounts, Map<id, Account> oldAccountsMap, Boolean webServicesEnabled) {
        DOM.XMLNode requestAddress;    //for capturing address request xml
        if (webServicesEnabled) {
            list<id> UpsertBillingAddressIds = new list<id>();
            list<id> DeleteBillingAddressIds = new list<id>();
            for(account a : newAccounts) {
                account oldAccount = oldAccountsMap.get(a.ID);
                System.Debug('*** New Value: ' + a.Billing_Address_Sync_Status__c + ' Old Value: ' + oldAccount.Billing_Address_Sync_Status__c);
                
                if (a.Billing_Address_Sync_Status__c == 'Sync In Progress' && oldAccount.Billing_Address_Sync_Status__c != 'Sync In Progress') {
                    /* if (a.Deleted__c || (a.BillingPostalCode == null && a.BillingStreet == null)) {
                        system.debug('*** After Update (DELETE) calling sync for Billing Address : '+a.id); 
                        DeleteBillingAddressIds.add(a.id);  
                    } else { */   //Commented for SF-3462
                    system.debug('*** After Update (UPSERT) calling sync for Billing Address : '+a.id); 
                    UpsertBillingAddressIds.add(a.id);  
                    //}
                }
                
                
            }
            
            if (UpsertBillingAddressIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    requestAddress = MDMAddressProxyNew.MDMUpsertBillingAddressAsync(UpsertBillingAddressIds);
                }
            }
        }
        return requestAddress;
    }
        public static DOM.XMLNode SyncShippingAddresses(List<Account> newAccounts, Map<id, Account> newAccountsMap, List<Account> oldAccounts, Map<id, Account> oldAccountsMap, Boolean webServicesEnabled) {
        DOM.XMLNode requestAddress;    //for capturing address request xml
        if (webServicesEnabled) {
            
            list<id> UpsertShippingAddressIds = new list<id>();
            list<id> DeleteShippingAddressIds = new list<id>();
            for(account a : newAccounts) {
                account oldAccount = oldAccountsMap.get(a.ID);
                System.Debug('*** New Value: ' + a.Billing_Address_Sync_Status__c + ' Old Value: ' + oldAccount.Billing_Address_Sync_Status__c);
                if (a.Shipping_Address_Sync_Status__c == 'Sync In Progress' && oldAccount.Shipping_Address_Sync_Status__c != 'Sync In Progress') {
                    /* if (a.Deleted__c || (a.ShippingPostalCode == null && a.ShippingStreet == null)) {
                        system.debug('*** After Update (DELETE) calling sync for Shipping Address : '+a.id);    
                        DeleteShippingAddressIds.add(a.id); 
                    } else { */ //Commented for SF-3462
                    system.debug('*** After Update (UPSERT) calling sync for Shipping Address : '+a.id);    
                    UpsertShippingAddressIds.add(a.id); 
                    //}
                }
            }
            if (UpsertShippingAddressIds.size() > 0) {
                if (!System.IsBatch() && !System.IsFuture()) {
                    requestAddress = MDMAddressProxyNew.MDMUpsertShippingAddressAsync(UpsertShippingAddressIds);
                }
            }
        }
        return requestAddress;
    }
    
    public static void SumbitApprovals(List<Account> newAccounts, Map<id, Account> newAccountsMap) {
        DQ__c DQSettings = DataQualitySupport.GetSetting(UserInfo.getUserId());
        // changes done for CRM-31
        //if(DQSettings.Enable_Role_Governance__c){
            Set<String> approvalAcctIds = new Set<String>();
            for(account a : newAccounts){
                if( a.Unapproved_Company_Roles__c != null || 
                    a.Unapproved_Company_Roles_to_Remove__c != null ||
                    a.Unapproved_Company_Roles_to_Add__c != null ||
                    a.Sub_Roles_Unapproved__c != null || 
                    a.Sub_Roles_to_Add__c != null || 
                    a.Sub_Roles_to_Remove__c != null  ){
                    approvalAcctIds.add(a.Id);
                }
            }
            if(approvalAcctIds.size() > 0){
                System.Debug('*** after insert trigger : Submitting async approval process for ids ' + approvalAcctIds);
                accountTriggerHelper.submitApproval(approvalAcctIds);
            }  
        //}
    }

    
    public static void AfterUpdate(List<Account> newAccounts, Map<id, Account> newAccountsMap, List<Account> oldAccounts, Map<id, Account> oldAccountsMap) {
        String request;
        Boolean partnerChange = false;
        Boolean roleChange = false;
        Boolean billingAddChange = false;
        Boolean shippingAddChange = false;
        Boolean isApproved = false;
        try {
            System.debug('sfEdit inside afterUpdate MDMSyncAccountHandler New');
            Gard_RecursiveBlocker.blocker = true;
            MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
            System.Debug('*** UserInfo.getUserId() : ' + UserInfo.getUserId());
            System.Debug('*** mdmSettings.Web_Services_Enabled__c: ' + mdmSettings.Web_Services_Enabled__c);
            //Create the MDM Roles...
            List<MDM_Role__c> mdmroles = new MDMValidRoleCombinationsHelper().SetValidRoleCombinations(newAccounts);
            system.debug('sfEdit mdmroles from helper AFTER UPDATE-->'+mdmroles );
            Map<id, MDM_Role__c> newRolesMap ;
            List<MDM_Role__c> mdmRolesNew = new List<MDM_Role__c>();
            //List<MDM_Role__c> mdmRolesOld = new List<MDM_Role__c>();
            // Membership approved from unapproved - STARTS
            system.debug('newAccounts[0].Membership_Status__c before approving-->'+oldAccounts[0].Membership_Status__c);
            DOM.XMLNode roleRequestXML;
            if(newAccounts[0].Membership_Status__c == 'Approved' && oldAccounts[0].Membership_Status__c != 'Approved'){
                mdmRolesNew = [SELECT id,Synchronisation_Status__c from MDM_Role__c where Account__c = :newAccounts[0].id and Active__c = TRUE];
                if(mdmRolesNew != null && mdmRolesNew.size() > 0 && newAccounts[0].Role_Broker__c){
                    //mdmRolesOld.addAll(mdmRolesNew);
                    for(MDM_Role__c m : mdmRolesNew){
                        m.Synchronisation_Status__c = 'Sync In Progress';
                        m.Synchronisation_Action__c = 'update';
                    }
                }
                newRolesMap = new Map<id, MDM_Role__c>(mdmRolesNew);
                roleRequestXML = SyncRoles(newRolesMap, mdmSettings.Web_Services_Enabled__c);
            }
            // Membership approved from unapproved - ENDS
            
            system.debug('roleRequestXML after account approved--->' +roleRequestXML);
            DOM.XMLNode partnerReqXML = SyncPartner(newAccounts, newAccountsMap, oldAccounts, oldAccountsMap, mdmSettings.Web_Services_Enabled__c);
            DOM.XMLNode addressBillingReqXML = SyncBillingAddresses(newAccounts, newAccountsMap, oldAccounts, oldAccountsMap, mdmSettings.Web_Services_Enabled__c);
            DOM.XMLNode addressShippingReqXML = SyncShippingAddresses(newAccounts, newAccountsMap, oldAccounts, oldAccountsMap, mdmSettings.Web_Services_Enabled__c);
            system.debug('partnerReqXML'+partnerReqXML);
            system.debug('addressBillingReqXML'+addressBillingReqXML);
            DOM.Document xmlDoc = new DOM.Document();

            DOM.XmlNode envelope = xmlDoc.createRootElement('Envelope', SOAPNS, SOAPNS_Prefix);
            for(MDM_Namespaces__c n : MDM_Namespaces__c.getAll().values()){
                envelope.setNamespace(n.name,n.namespace__c);
            }           
            DOM.XMLNode soapHeader = envelope.addChildElement('Header', SOAPNS, null);
            DOM.XMLNode soapBody = envelope.addChildElement('Body', SOAPNS, null);
            if(partnerReqXML!=null){
                XmlUtils.copyXmlNode(partnerReqXML,soapBody);
                partnerChange = true;
            }
            if(addressBillingReqXML!=null){
                XmlUtils.copyXmlNode(addressBillingReqXML,soapBody);
                billingAddChange = true;
            }
            if(addressShippingReqXML!=null){
                XmlUtils.copyXmlNode(addressShippingReqXML,soapBody);
                shippingAddChange = true;
            }
            if(roleRequestXML!=null){
                XmlUtils.copyXmlNode(roleRequestXML,soapBody);
                roleChange = true;
                isApproved = true;
            }
            //XmlUtils.copyXmlNode(request, soapBody);
            system.debug('***final  soapBody : ' + xmlDoc.toXMLString());
            
            request = xmlDoc.toXMLString();
            request = request.replaceAll('<MyBody>','');
            request = request.replaceAll('</MyBody>','');
            // Added for One-Integration
            if(partnerReqXML!=null && newAccounts[0].Gard_verification_status__c != 'Success'){
                system.debug('-- not success verification --');
                Integer startIndx = request.indexOf('<par:gardContactId>', 0);
                Integer endIndx = request.indexOf('</par:gardContactId>', 0);
                system.debug('gard startIndx -'+startIndx +'--endIndx --'+endIndx );
                string finalRequest = request.replace(request.substring(startIndx,endIndx),'<par:gardContactId>');
                request = finalRequest;
            }
            if(partnerReqXML != null){
                Integer startIndx2 = request.indexOf('<par:gardVerificationStatus>', 0);
                Integer endIndx2 = request.indexOf('</par:gardVerificationStatus>', 0);
                system.debug('start-'+startIndx2+'-end-'+endIndx2);
                string req = request.replace(request.substring(startIndx2,endIndx2),'');
                req = req.replace('</par:gardVerificationStatus>','');
                request = req;
            }
            
            system.debug('***final  soapBody : ' + request);
            
            // End - One-Integration
            /*
            if(request.containsIgnoreCase('</par:gardVerificationStatus>'))
                request = request.replace('</par:gardVerificationStatus>','');
            if(request.containsIgnoreCase('<par:gardVerificationStatus>'))
                request = request.replace('<par:gardVerificationStatus>','');
            */
            System.debug('sfEdit inside afterUpdate MDMSyncAccountHandlerNew just before callout');
            if(partnerReqXML != null || addressShippingReqXML != null || addressBillingReqXML != null)
                MDMPartnerProxyNew.singleCallOut(request,newAccountsMap.keySet(),partnerChange,roleChange,billingAddChange,shippingAddChange,isApproved);
        
        } catch (Exception ex) {
            Logger.LogException('MDMSyncAccountHandlerNew.AfterUpdate', ex);
            throw ex;
        }
    }
    
     public static void AfterInsert(List<Account> newAccounts, Map<id, Account> newAccountsMap) {
        String request;
        Boolean partnerChange = false;
        Boolean roleChange = false;
        Boolean billingAddChange = false;
        Boolean shippingAddChange = false;
        try {
            MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
            
            DOM.XMLNode partnerReqXML = SyncPartner(newAccounts, newAccountsMap, mdmSettings.Web_Services_Enabled__c);
            DOM.XMLNode addressBillingReqXML = SyncBillingAddresses(newAccounts, newAccountsMap, mdmSettings.Web_Services_Enabled__c);
            DOM.XMLNode addressShippingReqXML = SyncShippingAddresses(newAccounts, newAccountsMap, mdmSettings.Web_Services_Enabled__c);
            
            //==================================================================================================================================
            
            List<MDM_Role__c> mdmroles = new MDMValidRoleCombinationsHelper().SetValidRoleCombinations(newAccounts);
            system.debug('mdmroles from helper-->'+mdmroles );
            Map<id, MDM_Role__c> newRolesMap = new Map<id, MDM_Role__c>(mdmroles);
            DOM.XMLNode roleRequestXML = SyncRoles(newRolesMap, mdmSettings.Web_Services_Enabled__c);
            
            DOM.Document xmlDoc = new DOM.Document();

            DOM.XmlNode envelope = xmlDoc.createRootElement('Envelope', SOAPNS, SOAPNS_Prefix);
            for(MDM_Namespaces__c n : MDM_Namespaces__c.getAll().values()){
                envelope.setNamespace(n.name,n.namespace__c);
            }           
            DOM.XMLNode soapHeader = envelope.addChildElement('Header', SOAPNS, null);
            DOM.XMLNode soapBody = envelope.addChildElement('Body', SOAPNS, null);
            //Added for Partner informations
            if(partnerReqXML!=null){
                XmlUtils.copyXmlNode(partnerReqXML,soapBody);
                partnerChange = true;
            }
            //added for Role
            if(roleRequestXML != null){
                XmlUtils.copyXmlNode(roleRequestXML,soapBody);
                roleChange = true;
            }
            //added for Billing Address
            if(addressBillingReqXML!=null){
                XmlUtils.copyXmlNode(addressBillingReqXML,soapBody);
                billingAddChange = true;
            }
            //Added for shipping address
            if(addressShippingReqXML!=null){
                XmlUtils.copyXmlNode(addressShippingReqXML,soapBody);
                shippingAddChange = true;
            }
            system.debug('***final after insert soapBody : ' + xmlDoc.toXMLString());
            
            request = xmlDoc.toXMLString();
            request = request.replaceAll('<MyBody>','');
            request = request.replaceAll('</MyBody>','');
            //Added for One Integration
            if(partnerReqXML!=null && newAccounts[0].Gard_verification_status__c != 'Success'){
                system.debug('-- not success verification --');
                Integer startIndx = request.indexOf('<par:gardContactId>', 0);
                Integer endIndx = request.indexOf('</par:gardContactId>', 0);
                system.debug('gard startIndx -'+startIndx +'--endIndx --'+endIndx );
                string finalRequest = request.replace(request.substring(startIndx,endIndx),'<par:gardContactId>');
                request = finalRequest;
            }
            if(partnerReqXML != null){
                Integer startIndx2 = request.indexOf('<par:gardVerificationStatus>', 0);
                Integer endIndx2 = request.indexOf('</par:gardVerificationStatus>', 0);
                system.debug('start-'+startIndx2+'-end-'+endIndx2);
                string req = request.replace(request.substring(startIndx2,endIndx2),'');
                req = req.replace('</par:gardVerificationStatus>','');
                request = req;
            }
            //for removing extra tags added for one integration. to be commented once go-live occurs
            /*
            if(request.containsIgnoreCase('</par:gardVerificationStatus>'))
                request = request.replace('</par:gardVerificationStatus>','');
            if(request.containsIgnoreCase('<par:gardVerificationStatus>'))
                request = request.replace('<par:gardVerificationStatus>','');
            
            */
            system.debug('***final afterInsert soapBody : ' + request);
            MDMPartnerProxyNew.singleCallOut(request,newAccountsMap.keySet(),partnerChange,roleChange,billingAddChange,shippingAddChange,false);
            SumbitApprovals(newAccounts, newAccountsMap);
            
            //SF - 3803
            //Call Role sync 
            //need this for release 1 to ensure the MDM Roles get created before the role governance and web services are enabled.
            //List<MDM_role__c> roles = new MDMValidRoleCombinationsHelper().SetValidRoleCombinations(trigger.new);
            
        } catch (Exception ex) {
            Logger.LogException('MDMSyncAccountHandler.AfterInsert', ex);
            throw ex;
        }
    }
     
    //==================================================================================================================================
    //==========================================FOR ROLES========================================================================================
    public static void AfterInsert(List<MDM_Role__c> newRoles, Map<id, MDM_Role__c> newRolesMap) {
        try {
            MDM_Settings__c mdmSettings = MDMSettingSupport.GetSetting(UserInfo.getUserId());
            SyncRoles(newRolesMap, mdmSettings.Web_Services_Enabled__c);
            ////PS 15/04/2014 - Comment out as now calculated by rollups and workflow
            //InsertAccountSyncStatus(newRoles, newRolesMap, mdmSettings.Web_Services_Enabled__c);
        } catch (Exception ex) {
            Logger.LogException('MDMSyncRoleHandlerNew.AfterInsert', ex);
            throw ex;
        }
    }
    public static DOM.XMLNode SyncRoles(Map<id, MDM_Role__c> newRolesMap, Boolean webServiceEnabled){
        DOM.XMLNode requestRoles;
        if(webServiceEnabled){
            List<id> roleIds = new List<id>();
            roleIds.addAll(newRolesMap.keyset());
            if (!System.IsBatch() && !System.IsFuture()){
                requestRoles = MDMRoleProxyNew.MDMUpsertRoleAsync(roleIds);
            }
        }
        return requestRoles ;
    }
    //==================================================================================================================================
    //==================================================================================================================================
    //==================================================================================================================================
}