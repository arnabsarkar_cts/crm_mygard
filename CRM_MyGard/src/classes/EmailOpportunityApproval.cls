global class EmailOpportunityApproval implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.Inboundenvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

        string sOpptyRole = '';
        string sOpptyOwnerId = '';
        string sOpptyId = '';
        string sBody = '';
        string sSubject = '';
        sSubject = email.subject;
        sBody = email.plainTextBody;
        
        if (sBody.length() > 0)
        {
            // strip opportunity id out of email body
            //sOpptyId = sBody.substring(sBody.indexOf('Opportunity ID:')+15,sBody.length());
            integer startPos = sBody.indexOf('Opportunity ID:')+15;
            integer endPos = startPos + 15;
            if (endPos > sBody.length()) {
                endPos = sBody.length();
            }
            sOpptyId = sBody.substring(startPos, endPos);
            system.debug('#### opptyid:'+sOpptyId);
            system.debug('#### subject:'+sSubject);
            system.debug('#### body:'+sBody);
            
            if (sOpptyId.length() > 0)
            {
                // get the opportunity...
                //Opportunity oppty = [SELECT Id, Name, OwnerId from Opportunity where ID = :sOpptyId LIMIT 1];
                //EXCLUDE Small Craft and self approval opportunities from email distribution
                Opportunity oppty;
                List<Opportunity> lsOpps = new List<Opportunity>();
                lsOpps = [SELECT Id, Name, OwnerId, Approval_Criteria__c, Budget_Status__c, Gard_Market_Area__c, Agreement_Market_Area__c, RecordType.Name FROM Opportunity WHERE Id = :sOpptyId LIMIT 1];
                
                if(lsOpps.size()>0){oppty = lsOpps[0];}                                         
                
                if (oppty != null && oppty.Gard_Market_Area__c != 'NRDSC' && !(oppty.RecordType.Name == 'P&I' && oppty.Approval_Criteria__c == 'Self Approval' && oppty.Budget_Status__c == 'Budget Approved' ))
                {
                    try
                    {
                        
                        List<User> lstusrs = new List<User>();
                        lstusrs = getUserList(oppty);                        
                        // build a new email
                        if(lstusrs != null){
                            if(lstusrs.size()>0){
                                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                                // add the emails addresses of all users with the same role
                                String[] toaddress = new String[]{};
                                for (User usr : lstusrs)
                                    toaddress.add(usr.Email);
                                mail.setToAddresses(toaddress);
                                mail.setSubject(sSubject);
                                mail.setPlainTextBody(sBody);
            
                                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                                result.success = true;
                            }
                        }
                    }
                    catch (Exception e){
                        result.success = false;
                        result.message = 'An error has occurred processing the approval message: ' + e.getMessage();
                    }
                }
            }
        }
        return result;
    }
    
    private static List<User> getUserList(Opportunity opp) {
        //Phil Spenceley 11/09/2013
        // PCI-000144 - Issue 00762 - Emails should be sent based on Market Area rather than Role
        
        id marketAreaId = opp.Agreement_Market_Area__c;
        if (marketAreaId != null) {
            set<id> userIds = new set<id>();
            for (User_Market_Area__c uma : [SELECT User__c FROM User_Market_Area__c WHERE Market_Area__c = :marketAreaId]) {
                userIds.add(uma.User__c);
            }
            List<User> users = [SELECT Id, Name, Email, UserRole.Name FROM User WHERE id IN :userIds AND isActive=true];
            return users;
        }
        
        return null;
    }
    
    
    static TestMethod void testSendEmail() {
        /// ARRANGE...
        
        UserRole rl = new UserRole(Name='Area Manager'+system.now());        
        insert(rl);
        UserRole r2 = new UserRole(Name='UWR'+system.now(), ParentRoleId = rl.Id);
        insert(r2);
        Profile p = [Select Id, Name from Profile WHERE UserType = 'Standard' LIMIT 1];
        User tempusr = [SELECT TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, LanguageLocaleKey from User where IsActive=true LIMIT 1];
        User usr1 = new User(UserName='EOAtest1@gard.no', Alias='test1', LastName='test1', ProfileId=p.Id, Email='test1@gard.no', UserRoleId=rl.Id, TimeZoneSidKey=tempusr.TimeZoneSidKey, LocaleSidKey=tempusr.LocaleSidKey, EmailEncodingKey=tempusr.EmailEncodingKey, LanguageLocaleKey=tempusr.LanguageLocaleKey);
        User usr2 = new User(UserName='EOAtest2@gard.no', Alias='test2', LastName='test2', ProfileId=p.Id, Email='test2@gard.no', UserRoleId=r2.Id, TimeZoneSidKey=tempusr.TimeZoneSidKey, LocaleSidKey=tempusr.LocaleSidKey, EmailEncodingKey=tempusr.EmailEncodingKey, LanguageLocaleKey=tempusr.LanguageLocaleKey);
        User usr3 = new User(UserName='EOAtest3@gard.no', Alias='test3', LastName='test3', ProfileId=p.Id, Email='test3@gard.no', UserRoleId=r2.Id, TimeZoneSidKey=tempusr.TimeZoneSidKey, LocaleSidKey=tempusr.LocaleSidKey, EmailEncodingKey=tempusr.EmailEncodingKey, LanguageLocaleKey=tempusr.LanguageLocaleKey);
        
        insert(usr1);insert(usr2);insert(usr3);
        
        system.runAs(usr2)
        {
            Market_Area__c ma1 = new Market_Area__c(Name = 'Nordic');
            Market_Area__c ma2 = new Market_Area__c(Name = 'Asia');
            insert(ma1);insert(ma2);
            
            List<User_Market_Area__c> uma = new List<User_Market_Area__c> ();
            uma.add(new User_Market_Area__c(User__c = usr1.id, Market_area__c = ma1.id));
            uma.add(new User_Market_Area__c(User__c = usr2.id, Market_area__c = ma1.id));
            uma.add(new User_Market_Area__c(User__c = usr3.id, Market_area__c = ma1.id));
            uma.add(new User_Market_Area__c(User__c = usr1.id, Market_area__c = ma2.id));
            
            insert(uma);
            
            Opportunity oppty = new Opportunity(Name='Unit Test Oppty'+system.now(),OwnerId=usr2.Id, StageName='Budget Set', CloseDate=Date.today(), Agreement_Market_Area__c = ma1.id);
            insert(oppty);
            Opportunity oppty2 = new Opportunity(Name='Unit Test Oppty2'+system.now(),OwnerId=usr2.Id, StageName='Budget Set', CloseDate=Date.today(), Agreement_Market_Area__c = ma2.id);
            insert(oppty2);
            
            List<User> testUsers = new List<User>();
            List<User> testUsers2 = new List<User>();
            
            //set up email fields
            string sSubject = 'test email subject';
            string sBody = 'test email body, Opportunity ID:'+oppty.ID;
            string sTo = 'test@test.com';
            
            // instantiate the email object
            Messaging.InboundEmail email = new Messaging.InboundEmail();
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
            email.fromAddress = sTo;
            email.Subject = sSubject;
            email.plainTextBody = sBody;
            env.fromAddress='test@test.com';
            
            // instantiate the EmailOpportunityApproval object
            EmailOpportunityApproval eoa = new EmailOpportunityApproval();
        
            ///ACT...
            Test.startTest();
            testUsers = getUserList(oppty); 
            testUsers2 = getUserList(oppty2); 
            
            // simulate sending the email. The only result we can confirm is whether the email sent successfully - as sending the email
            // doesn't perform any DML or set any publically accessible variables there aren't any other outcomes to assert.
            Messaging.InboundEmailResult result = eoa.handleInboundEmail(email, env);
            Test.stopTest();
            
            ///ASSERT...
            system.Assertequals(3, testUsers.size(), 'First list of users should contain 3');
            system.Assertequals(1, testUsers2.size(), 'Second list of users should contain 1');
            system.Assertequals(true, result.success, 'Result Success should be true');
        }
    }
    
    
    /*
    ///Phil Spenceley 11/09/2013
    /// PCI-000144 - Issue 00762 - Emails should be sent based on Market Area rather than Role
    /// START OBSELETE Methods...
    
    private static List<User> getUserList(Opportunity theOpp){

        //Martin Gardner cDecisions 26/07/2012
        // PCI-000069 - Issue00232 - Opp notification email not send to team when AM manager submit for approval
        //  Lookup subordinates if the area manager is the owner of the opportunity that has been submitted for approval.
        // get the user role...
        Id sOpptyOwnerId = theOpp.OwnerId;
        User oOpptyOwner = new User();
        oOpptyOwner = [SELECT UserRole.Name, UserRole.Id from User where Id = :sOpptyOwnerId LIMIT 1];
        String sOpptyRole = oOpptyOwner.UserRole.Name;  
        ID OpptyRoleId =    oOpptyOwner.UserRole.Id;                 
        system.debug('#### sOpptyRole: '+sOpptyRole);
        // get all users with the same role
        List<User> lstusrs = new List<User>();
        if(sOpptyRole != null){
            if(sOpptyRole.contains('Area Manager')){
                //get a list of subordinate roles to then get all 
                //subordinate users
                Set<ID> sSubordinates = new Set<ID>();
                if(OpptyRoleId != null){
                    sSubordinates = EmailOpportunityApproval.getAllSubRoleIds(new Set<ID>{OpptyRoleId});
                    lstusrs = [SELECT Id, Name, Email, UserRole.Id from User where UserRole.Id IN :sSubordinates];
                }
            }else{
            
                // get all users with the same role
                lstusrs = [SELECT Id, Name, Email, UserRole.Name from User where UserRole.Name = :sOpptyRole];
            } 
        }
                        
                        
        return lstusrs;
    }
    
    public static Set<ID> getAllSubRoleIds(Set<ID> roleIds) {
     
        Set<ID> currentRoleIds = new Set<ID>();
     
        // get all of the roles underneath the passed roles
        for(UserRole userRole :[select Id from UserRole where ParentRoleId 
          IN :roleIds AND ParentRoleID != null])
        currentRoleIds.add(userRole.Id);
     
        // go fetch some more roles!
        if(currentRoleIds.size() > 0)
          currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
     
        return currentRoleIds;
     
      }    
    
    static TestMethod void testGetAllSubRoleIds(){
        
        //Get top of role hierarchy
        
        List<UserRole> lstTopRole = [SELECT Id FROM UserRole WHERE ParentRoleId = null Limit 1];
        Set<Id> sTopRole = new Set<Id>();
        for(UserRole aRole : lstTopRole){
            sTopRole.add(aRole.Id);
        }
        Set<ID> subRoles = new Set<ID>();
        subRoles = EmailOpportunityApproval.getAllSubRoleIds(sTopRole );
    }
    
    
    static TestMethod void testSendEmail() {
        // set up core test data
        UserRole rl = new UserRole(Name='Area Manager'+system.now());        
        insert(rl);
        UserRole r2 = new UserRole(Name='UWR'+system.now(), ParentRoleId = rl.Id);
        insert(r2);
        Profile p = [Select Id, Name from Profile LIMIT 1];
        User tempusr = [SELECT TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, LanguageLocaleKey from User where IsActive=true LIMIT 1];
        User usr1 = new User(UserName='EOAtest1@gard.no', Alias='test1', LastName='test1', ProfileId=p.Id, Email='test1@gard.no', UserRoleId=rl.Id, TimeZoneSidKey=tempusr.TimeZoneSidKey, LocaleSidKey=tempusr.LocaleSidKey, EmailEncodingKey=tempusr.EmailEncodingKey, LanguageLocaleKey=tempusr.LanguageLocaleKey);
        User usr2 = new User(UserName='EOAtest2@gard.no', Alias='test2', LastName='test2', ProfileId=p.Id, Email='test2@gard.no', UserRoleId=r2.Id, TimeZoneSidKey=tempusr.TimeZoneSidKey, LocaleSidKey=tempusr.LocaleSidKey, EmailEncodingKey=tempusr.EmailEncodingKey, LanguageLocaleKey=tempusr.LanguageLocaleKey);
        User usr3 = new User(UserName='EOAtest3@gard.no', Alias='test3', LastName='test3', ProfileId=p.Id, Email='test3@gard.no', UserRoleId=r2.Id, TimeZoneSidKey=tempusr.TimeZoneSidKey, LocaleSidKey=tempusr.LocaleSidKey, EmailEncodingKey=tempusr.EmailEncodingKey, LanguageLocaleKey=tempusr.LanguageLocaleKey);
        
        insert(usr1);insert(usr2);insert(usr3);
        
        system.runAs(usr2)
        {
            Opportunity oppty = new Opportunity(Name='Unit Test Oppty'+system.now(),OwnerId=usr2.Id, StageName='Budget Set', CloseDate=Date.today());
            insert(oppty);
            
            List<User> testUsers = new List<User>();
            testUsers = getUserList(oppty); 
            //set up email fields
            string sSubject = 'test email subject';
            string sBody = 'test email body, Opportunity ID:'+oppty.ID;
            string sTo = 'test@test.com';
            
            // instantiate the email object
            Messaging.InboundEmail email = new Messaging.InboundEmail();
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
            email.fromAddress = sTo;
            email.Subject = sSubject;
            email.plainTextBody = sBody;
            env.fromAddress='test@test.com';
            
            // instantiate the EmailOpportunityApproval object
            EmailOpportunityApproval eoa = new EmailOpportunityApproval();
            
            // simulate sending the email. The only result we can confirm is whether the email sent successfully - as sending the email
            // doesn't perform any DML or set any publically accessible variables there aren't any other outcomes to assert.
            Test.startTest();
            Messaging.InboundEmailResult result = eoa.handleInboundEmail(email, env);
            system.Assert(result.success == true);
            Test.stopTest();
        }
    }
    static TestMethod void testSendEmailManager() {
        // set up core test data
        UserRole rl = new UserRole(Name='Area Manager');        
        insert(rl);
        UserRole r2 = new UserRole(Name='UWR'+system.now(), ParentRoleId = rl.Id);
        insert(r2);
        Profile p = [Select Id, Name from Profile LIMIT 1];
        User tempusr = [SELECT TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, LanguageLocaleKey from User where IsActive=true LIMIT 1];
        User usr1 = new User(UserName='EOAtest1@gard.no', Alias='test1', LastName='test1', ProfileId=p.Id, Email='test1@gard.no', UserRoleId=rl.Id, TimeZoneSidKey=tempusr.TimeZoneSidKey, LocaleSidKey=tempusr.LocaleSidKey, EmailEncodingKey=tempusr.EmailEncodingKey, LanguageLocaleKey=tempusr.LanguageLocaleKey);
        User usr2 = new User(UserName='EOAtest2@gard.no', Alias='test2', LastName='test2', ProfileId=p.Id, Email='test2@gard.no', UserRoleId=r2.Id, TimeZoneSidKey=tempusr.TimeZoneSidKey, LocaleSidKey=tempusr.LocaleSidKey, EmailEncodingKey=tempusr.EmailEncodingKey, LanguageLocaleKey=tempusr.LanguageLocaleKey);
        User usr3 = new User(UserName='EOAtest3@gard.no', Alias='test3', LastName='test3', ProfileId=p.Id, Email='test3@gard.no', UserRoleId=r2.Id, TimeZoneSidKey=tempusr.TimeZoneSidKey, LocaleSidKey=tempusr.LocaleSidKey, EmailEncodingKey=tempusr.EmailEncodingKey, LanguageLocaleKey=tempusr.LanguageLocaleKey);
        
        insert(usr1);insert(usr2);insert(usr3);
        
        system.runAs(usr1)
        {
            Opportunity oppty = new Opportunity(Name='Unit Test Oppty'+system.now(),OwnerId=usr1.Id, StageName='Budget Set', CloseDate=Date.today());
            insert(oppty);
            
            List<User> testUsers = new List<User>();
            testUsers = getUserList(oppty);                    
            
            //set up email fields
            string sSubject = 'test email subject';
            string sBody = 'test email body, Opportunity ID:'+oppty.ID;
            string sTo = 'test@test.com';
            
            // instantiate the email object
            Messaging.InboundEmail email = new Messaging.InboundEmail();
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
            email.fromAddress = sTo;
            email.Subject = sSubject;
            email.plainTextBody = sBody;
            env.fromAddress='test@test.com';
            
            // instantiate the EmailOpportunityApproval object
            EmailOpportunityApproval eoa = new EmailOpportunityApproval();
            
            // simulate sending the email. The only result we can confirm is whether the email sent successfully - as sending the email
            // doesn't perform any DML or set any publically accessible variables there aren't any other outcomes to assert.
            Test.startTest();
            Messaging.InboundEmailResult result = eoa.handleInboundEmail(email, env);
            system.Assert(result.success == true);
            Test.stopTest();
        }
    }  
    ///END OBSELETE METHODS...
*/  
}