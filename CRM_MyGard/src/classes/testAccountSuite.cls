@isTest
private class testAccountSuite {
    static TestMethod void testMembershipApproval() {
        //### CREATE TEST RECORDS ###
        String strAMId = [SELECT Id FROM User WHERE Position__c='Area Manager' LIMIT 1].Id;
        String strSVP1Id = [SELECT Id FROM User WHERE Position__c='SVP' LIMIT 1].Id;
        String strSVP2Id = UserInfo.getUserId();
        //create an account record
        Account acc = TestDataGenerator.getClientAccount();//new Account(Name='APEXTESTACC001');
        
        
        //insert acc;
        String strAccId = acc.Id;
        //create a membership review record
        Company_Membership_Review__c rev = new Company_Membership_Review__c();
        rev.Company_Name__c = strAccId;
        rev.Area_Manager_del__c = strAMId;
        rev.Senior_Approver_1__c = strSVP1Id;
        rev.Senior_Approver_2__c = strSVP2Id;
        rev.Confirm_company_not_on_santion_list__c = true;
        
        insert rev;
        String strRevId = rev.Id;

        Test.startTest();
            //set review to approved
            rev.Status__c = 'Approved';
            
            update rev;
        Test.stopTest();
        
        //assert account approved status
        acc = [SELECT Id, Name, Membership_Status__c FROM Account WHERE Id=:strAccId];
        //System.assertEquals('Approved', acc.Membership_Status__c);
    }

    static TestMethod void testMembershipRejection() {
        //### CREATE TEST RECORDS ###
        String strAMId = [SELECT Id FROM User WHERE Position__c='Area Manager' LIMIT 1].Id;
        String strSVP1Id = [SELECT Id FROM User WHERE Position__c='SVP' LIMIT 1].Id;
        String strSVP2Id = UserInfo.getUserId();
        //create an account record
        Account acc = TestDataGenerator.getClientAccount();// new Account(Name='APEXTESTACC001');
        
        //insert acc;
        String strAccId = acc.Id;
        //create a membership review record
        Company_Membership_Review__c rev = new Company_Membership_Review__c();
        rev.Company_Name__c = strAccId;
        rev.Area_Manager_del__c = strAMId;
        rev.Senior_Approver_1__c = strSVP1Id;
        rev.Senior_Approver_2__c = strSVP2Id;
        rev.Confirm_company_not_on_santion_list__c = true;
        insert rev;
        String strRevId = rev.Id;

        Test.startTest();
            //set review to approved
            rev.Status__c = 'Declined';
            update rev;
        Test.stopTest();
        
        //assert account approved status
        acc = [SELECT Id, Name, Membership_Status__c FROM Account WHERE Id=:strAccId];
        //System.assertEquals('Declined', acc.Membership_Status__c);
    }
    
    static TestMethod void testAccountAddressChange() {
        //### CREATE TEST RECORDS ###
        String strAMId = [SELECT Id FROM User WHERE Position__c='Area Manager' LIMIT 1].Id;
        String strSVP1Id = [SELECT Id FROM User WHERE Position__c='SVP' LIMIT 1].Id;
        String strSVP2Id = UserInfo.getUserId();
        //create an account record
        Account acc = new Account(  Name='APEXTESTACC001',
                                    Site='TESTSITE',
                                    Area_Manager__c=strAMId,
                                    RecordTypeId=Schema.SObjectType.Account.RecordTypeInfosByName.get('Client').RecordTypeId,
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',                                  
                                    Market_Area__c = TestDataGenerator.getMarketArea().Id
                                );
        insert acc;
        String strAccId = acc.Id;
        System.AssertNotEquals(strAccId,null);
        
        Contact con = new Contact(  FirstName='APEXTESTCON001',
                                    LastName='TEST LAST NAME',
                                    MailingCity = 'London',
                                    MailingCountry = 'United Kingdom',
                                    MailingPostalCode = 'SE1 1AD',
                                    MailingState = 'London',
                                    MailingStreet = '1 London Road',
                                    AccountId = acc.Id
                                    );
        insert con;
        String strConId = con.Id;        
        System.AssertNotEquals(strConId,null);

        Test.startTest();
            //update address fields
            acc.BillingCity = 'Paris';
            acc.BillingCountry = 'FRANCE';
            acc.BillingPostalCode = '0123122';
            acc.BillingState = 'Paris';
            acc.BillingStreet = '1 Rue de Change';
            acc.Synchronisation_Status__c = 'Synchronised';
            acc.Billing_Address_Sync_Status__c = 'Synchronised';
            acc.Shipping_Address_Sync_Status__c = 'Synchronised';
            acc.Roles_Sync_Status__c  = 'Synchronised';
            update acc;
        Test.stopTest();
        
        //assert contact address
        con = [SELECT Id, Name, MailingCity, MailingCountry, MailingPostalCode, MailingState, MailingStreet FROM Contact WHERE Id=:strConId];
//        System.assertEquals('Paris', con.MailingCity);
//        System.assertEquals('FRANCE', con.MailingCountry);
//        System.assertEquals('0123122', con.MailingPostalCode);
//        System.assertEquals('Paris', con.MailingState);
//        System.assertEquals('1 Rue de Change', con.MailingStreet);
    }   
    
    static TestMethod void testAccountObjectivePlanChange() {
        //### CREATE TEST RECORDS ###
        String strAMId = [SELECT Id FROM User WHERE Position__c='Area Manager' LIMIT 1].Id;
        String strSVP1Id = [SELECT Id FROM User WHERE Position__c='SVP' LIMIT 1].Id;
        String strSVP2Id = UserInfo.getUserId();
        //create an account record
        Account acc = new Account(  Name='APEXTESTACC001',
                                    BillingCity = 'Bristol',
                                    BillingCountry = 'United Kingdom',
                                    BillingPostalCode = 'BS1 1AD',
                                    BillingState = 'Avon' ,
                                    BillingStreet = '1 Elmgrove Road',
                                    objectives__c = 'Test Objective',
                                    plan__c = 'Test Plan',
                                    Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().id,
                                    Market_Area__c = TestDataGenerator.getMarketArea().Id
                                );
        insert acc;
        String strAccId = acc.Id;
        

        Test.startTest();
            //update address fields
            acc.objectives__c = 'New Test Objective';
            acc.plan__c = 'New Test Plan';
            update acc;
        Test.stopTest();
        
        //assert contact address
        acc = [SELECT Id, Name, objectives__c, plan__c FROM Account WHERE Id=:strAccId];
        //System.assertEquals('New Test Objective', acc.objectives__c);
        //System.assertEquals('New Test Plan', acc.plan__c);
    } 
    static TestMethod void testMembershipType1() {
        //### CREATE TEST RECORDS ###
        String strAMId = [SELECT Id FROM User WHERE Position__c='Area Manager' LIMIT 1].Id;
        String strSVP1Id = [SELECT Id FROM User WHERE Position__c='SVP' LIMIT 1].Id;
        String strSVP2Id = UserInfo.getUserId();
        //create an account record
        Account acc = TestDataGenerator.getClientAccount();//new Account(Name='APEXTESTACC001');
    
    
        //insert acc;
        String strAccId = acc.Id;
        //create a membership review record
        Company_Membership_Review__c rev = new Company_Membership_Review__c();
        rev.Company_Name__c = strAccId;
        rev.Area_Manager_del__c = strAMId;
        rev.Senior_Approver_1__c = strSVP1Id;
        rev.Senior_Approver_2__c = strSVP2Id;
        rev.Confirm_company_not_on_santion_list__c = true;
        rev.status__c = 'Approved';        
                
        insert rev;
        String strRevId = rev.Id;

        Test.startTest();
            rev.Approval_type_required__c = 'Shipowners, MOU, Charterers P&I approval';
            update rev;
        Test.stopTest();
        
        //assert account approved status
        acc = [SELECT Id, Name, Membership_Type__c FROM Account WHERE Id=:strAccId];
        //System.assertEquals('Shipowners, MOU, Charterers P&I approval', acc.Membership_Type__c);
    }
    static TestMethod void testMembershipType2() {
        //### CREATE TEST RECORDS ###
        String strAMId = [SELECT Id FROM User WHERE Position__c='Area Manager' LIMIT 1].Id;
        String strSVP1Id = [SELECT Id FROM User WHERE Position__c='SVP' LIMIT 1].Id;
        String strSVP2Id = UserInfo.getUserId();
        //create an account record
        Account acc = TestDataGenerator.getClientAccount();//new Account(Name='APEXTESTACC001');
    
    
        //insert acc;
        String strAccId = acc.Id;
        //create a membership review record
        Company_Membership_Review__c rev = new Company_Membership_Review__c();
        rev.Company_Name__c = strAccId;
        rev.Area_Manager_del__c = strAMId;
        rev.Senior_Approver_1__c = strSVP1Id;
        rev.Senior_Approver_2__c = strSVP2Id;
        rev.Confirm_company_not_on_santion_list__c = true;
        rev.status__c = 'Approved';
                        
        insert rev;
        String strRevId = rev.Id;

        Test.startTest();
            rev.Approval_type_required__c = 'Small Craft, Fixed P&I approval';
            update rev;
        Test.stopTest();
        
        //assert account approved status
        acc = [SELECT Id, Name, Membership_Type__c FROM Account WHERE Id=:strAccId];
        //System.assertEquals('Small Craft, Fixed P&I approval', acc.Membership_Type__c);
    }
}