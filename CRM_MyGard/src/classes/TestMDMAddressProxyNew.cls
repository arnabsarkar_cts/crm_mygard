@istest
private class TestMDMAddressProxyNew
{
    private static testmethod void UpsertBillingAddressAsync_Success() {
        MDMProxyTests.setupMDMConfigSettings();
        Account a = new Account (Name = 'Unit Test Account', billingStreet = 'Test Road', BillingState = 'Test State', BillingPostalCode = 'TE12 3ST', BillingCountry = 'UK');
		insert a;
		id id = a.id;
        WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
        Test.StartTest();
		// manually call the web service
		MDMAddressProxyNew.MDMUpsertBillingAddressAsync(new List<id> { id });
		Test.StopTest();
    }
    private static testmethod void UpsertBillingAddressAsync_Failure() {
		///Arrange...
		// ensure web services \ triggers are turned off (custom setting)
		// insert an account
		// set the web service mock (using the account id)
		MDMProxyTests.setupMDMConfigSettings();
		Account a = new Account (Name = 'Unit Test Account', billingStreet = 'Test Road', BillingState = 'Test State', BillingPostalCode = 'TE12 3ST', BillingCountry = 'UK');
		insert a;
		id id = a.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
		List<Account> acct=new List<Account>();
        acct.add(a);
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMAddressProxyNew.MDMUpsertBillingAddressAsync(new List<id> { id });
        MDMAddressProxyNew.SetBillingAddressSyncFailed(acct,'source');
		Test.StopTest();
	}
    private static testmethod void UpsertBillingAddressAsync_NullResponse() {
		///Arrange...
		// ensure web services \ triggers are turned off (custom setting)
		// insert an account
		// set the web service mock (using the account id)
		MDMProxyTests.setupMDMConfigSettings();
		Account a = new Account (Name = 'Unit Test Account', billingStreet = 'Test Road', BillingState = 'Test State', BillingPostalCode = 'TE12 3ST', BillingCountry = 'UK');
		insert a;
		id id = a.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = null;
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMAddressProxyNew.MDMUpsertBillingAddressAsync(new List<id> { id });
        
		Test.StopTest();	
	}
    private static testmethod void UpsertShippingAddressAsync_Success() {
		///Arrange...
		// ensure web services \ triggers are turned off (custom setting)
		// insert an account
		// set the web service mock (using the account id)
		MDMProxyTests.setupMDMConfigSettings();
		Account a = new Account (Name = 'Unit Test Account', ShippingStreet = 'Test Road', ShippingState = 'Test State', ShippingPostalCode = 'TE12 3ST', ShippingCountry = 'UK');
		insert a;
		id id = a.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, true);
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMAddressProxyNew.MDMUpsertShippingAddressAsync(new List<id> { id });
		Test.StopTest();
       
    }
    private static testmethod void UpsertShippingAddressAsync_Failure() {
		///Arrange...
		// ensure web services \ triggers are turned off (custom setting)
		// insert an account
		// set the web service mock (using the account id)
		MDMProxyTests.setupMDMConfigSettings();
		Account a = new Account (Name = 'Unit Test Account', ShippingStreet = 'Test Road', ShippingState = 'Test State', ShippingPostalCode = 'TE12 3ST', ShippingCountry = 'UK');
		insert a;
		id id = a.id;
		List<Account> acct=new List<Account>();
        acct.add(a);
		WebServiceUtilTests.Instance.MockWebServiceResponse = new MDMProxyTests().generateMDMServiceResponse(id, false);
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMAddressProxyNew.MDMUpsertShippingAddressAsync(new List<id> { id });
        MDMAddressProxyNew.SetShippingAddressSyncFailed(acct,'source');
		Test.StopTest();
	}
	
	private static testmethod void UpsertShippingAddressAsync_NullResponse() {
		///Arrange...
		// ensure web services \ triggers are turned off (custom setting)
		// insert an account
		// set the web service mock (using the account id)
		MDMProxyTests.setupMDMConfigSettings();
		Account a = new Account (Name = 'Unit Test Account', ShippingStreet = 'Test Road', ShippingState = 'Test State', ShippingPostalCode = 'TE12 3ST', ShippingCountry = 'UK');
		insert a;
		id id = a.id;
		
		WebServiceUtilTests.Instance.MockWebServiceResponse = null;
				
		///Act...
		Test.StartTest();
		// manually call the web service
		MDMAddressProxyNew.MDMUpsertShippingAddressAsync(new List<id> { id });
		Test.StopTest();
	}
}