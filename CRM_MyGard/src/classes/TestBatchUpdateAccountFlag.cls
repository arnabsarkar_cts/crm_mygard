//Created for SF-4553
//By Pulkit because the lazy Granny(Preethika) didn't
@isTest
public class TestBatchUpdateAccountFlag {
	static GardTestData gtd;
    static Opportunity opp;
    static String strRecId = [SELECT Id FROM RecordType WHERE Name='Marine' AND sObjectType='Opportunity' LIMIT 1].Id;
    
    
    private static void createTestData(){    
        Test.startTest();
        gtd = new GardTestData();
        gtd.commonRecord();
        Test.stopTest();
        GardTestData.clientAcc.TYPE = 'CUSTOMER';
        update GardTestData.clientAcc;
        
        opp = new Opportunity();
        opp.RecordTypeId = strRecId;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = GardTestData.clientAcc.id;
        opp.StageName = 'Risk Evaluation';//Renewable Opportunity
        opp.Type = 'New Business';        
        opp.Business_Type__c = 'Small Craft';
        opp.Approval_Criteria__c = 'Self Approval';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        opp.Confirm_not_on_sanction_list__c = true;
        opp.Amount = 100;
        insert opp;
    }
    
    @isTest private  static void testBatch(){
        createTestData();
        List<Id> accountIds = new List<Id>{GardTestdata.clientAcc.id};
        BatchUpdateAccountFlag buaf = new BatchUpdateAccountFlag(accountIds);
        Database.executeBatch(buaf);
    }
    
    @isTest private  static void testBatchSchedule(){
        createTestData();
        List<Id> accountIds = new List<Id>{GardTestdata.clientAcc.id};
        SchedulerforAccountFlagUpdate sfafu = new SchedulerforAccountFlagUpdate();
        System.schedule('Test Schedule', '0 0 0 1 1 ?', sfafu);
    }
}