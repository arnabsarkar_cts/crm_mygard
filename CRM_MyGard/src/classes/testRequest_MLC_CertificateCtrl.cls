@isTest
public class testRequest_MLC_CertificateCtrl{

    public static MLC_Certificate_effective_date__c effDate1,effDate2,effDate3;

    public static testMethod void MLC_CertificateCtrlMethod(){
        GardTestData testRec = new GardTestData();
        testRec.customsettings_rec();
        List<Group> q = [SELECT id FROM Group WHERE Name LIKE 'Trading Certification Team'];
        if(q.size() == 0){
            q.add(new Group(Name = 'Trading Certificates Team',Type = 'Queue'));
            insert q;
        }
        List<QueueSobject> qso = [Select QueueId from QueueSobject where queue.name like 'Trading Certification Team' and SobjectType = 'Case'];
        if(qso.size() == 0 ){
            qso.add(new QueueSobject(queueId = q[0].id,SobjectType = 'Case'));
            //insert qso; //commented because failing
        }
        testRec.commonRecord();
        //Group grp = new Group(name = 'Trading Certification Team',type = 'Queue');
        //insert grp;
        
        
        test.startTest();
        object__c obj = [SELECT Id,guid__c FROM object__c WHERE Id =: GardTestData.test_object_3rd.Id];
        string guid = obj.guid__c;
         guid = obj.guid__c;
        system.debug('**guid** : '+guid);
        MLC_certificate__c MLCObj = new MLC_certificate__c(Client__c = GardTestData.ClientAcc.Id,
                                                           CaseId__c = GardTestData.clientCase.Id,
                                                           Comment__c = 'test Comments',
                                                           Email__c = true,
                                                           Phone__c = false,
                                                           Reporter_Contact__c = GardTestData.ClientContact.Id,
                                                           Your_faithfully__c = 'Test Name',
                                                           Effect_From__c= '14.06.2017' ,
                                                           Effective_To__c= '20.06.2017' ,
                                                           Fleet_Name__c='abctest'
                                                           );
        insert MLCObj;
        Asset clientAsset_Test = new Asset(Name= 'Test Asset',
                 accountid = GardTestData.clientAcc.id,
                 contactid = GardTestData.clientContact.Id,  
                 Agreement__c = GardTestData.clientContract_2nd.id, 
                 Cover_Group__c =  GardTestData.piStr ,
                 CurrencyIsoCode= GardTestData.cntryISOStr  ,
                 product_name__c = GardTestData.prodName ,
                 Object__c = GardTestData.test_object_3rd.id,
                 Underwriter__c = GardTestData.clientUser.id,
                 On_risk_indicator__c = true,
                 Expiration_Date__c = date.today().adddays(50),
                 Inception_date__c = date.today(),
                 Risk_ID__c = '123111ri'
                 ); 
        insert clientAsset_Test ;
        //effDate1 = new MLC_Certificate_effective_date__c(name = '1' ,  value__c = '18 January 2017 - 20 February 2018');
       // effDate2 = new MLC_Certificate_effective_date__c(name = '2' ,  value__c = '18 January 2017 - 20 February 2017');
        //effDate3 = new MLC_Certificate_effective_date__c(name = '3' ,  value__c = '20 February 2017 - 20 February 2018');
       // insert effDate1 ;
        //insert effDate2 ;
        //insert effDate3 ;
        trading_certificates__c tradingCerti = new trading_certificates__c(name = 'trading_email', value__c = 'testEmail@example.com');
        insert tradingCerti ;
        //effDateList.add(effDate1);
        //effDateList.add(effDate2);
        //effDateList.add(effDate3);
        //insert effDateList;
        system.debug('*testContractIds* : '+ GardTestData.clientAsset_1st.Agreement__c +': * :'+GardTestData.clientAsset_2nd.Agreement__c +': * :' 
                    + clientAsset_Test.Agreement__c  );
        
        System.RunAs(GardTestData.brokerUser){
            
               Request_MLC_CertificateCtrl reqMLCBroker = new Request_MLC_CertificateCtrl();
               
               reqMLCBroker.prvUrl = 'ObjectDetail?objid='+ guid ;
               reqMLCBroker.caseTestId = GardTestData.clientCase.Id;
               reqMLCBroker.sendType();
               reqMLCBroker.resetFleetCheckbox();
               reqMLCBroker.regOwnerChng=false;
               reqMLCBroker.vesselNameChng=false;
               reqMLCBroker.flagChng=false;
               reqMLCBroker.resetChkboxFields();
               reqMLCBroker.dateEffective  = '01.02.2016';
               reqMLCBroker.strMLC_certificate='Request change to MLC Certificate';
               reqMLCBroker.vesselNameChng=true;
               reqMLCBroker.flagChng=true;
               reqMLCBroker.regOwnerChng=true;
               //reqMLCBroker.toggleFleet_N_Object=true;
               reqMLCBroker.submit();
               reqMLCBroker.strMLC_certificate='Request new MLC Certificate';
               reqMLCBroker.getPort();
               reqMLCBroker.getFlagOption();
               reqMLCBroker.submit();
               reqMLCBroker.displayDoc();
               reqMLCBroker.sendMails();
               reqMLCBroker.cancel();
               reqMLCBroker.userType = 'Client';
               reqMLCBroker.populateClient();
              
        }
        contract testCon = [SELECT Id,broker__c FROM contract WHERE Id =: GardTestData.clientContract_2nd.id];
        testCon.broker__c = null;
        update testCon;
     /*   System.RunAs(GardTestData.clientUser){
            Request_MLC_CertificateCtrl reqMLCClient = new Request_MLC_CertificateCtrl();   
            reqMLCClient.prvUrl = 'PiRenewalBlueCard?objid='+guid+'test';
            //reqMLCClient.sendType(); 

        } */
        
        test.stopTest();
    }
}