@isTest
private class MDMSyncMergeAccountHandlerTests {
	
	static testMethod void InsertMergeAccountSyncStatus_WebServiceEnabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		Account a1 = new Account (Name = 'Unit Test Account 1');
		Account a2 = new Account (Name = 'Unit Test Account 2');
		insert a1;
		insert a2;
		
		MDM_Account_Merge__c m = new MDM_Account_Merge__c(From_Account__c = a1.Id, To_Account__c = a2.Id);
		insert m;
		
		///ACT...
		Test.startTest();
		MDMSyncMergeAccountHandler.InsertMergeAccountSyncStatus(new List<MDM_Account_Merge__c> { m }, true);
		Test.stopTest();
		
		///ASSERT...
		System.assertEquals('Sync In Progress', m.Synchronisation_Status__c);
	}
	
	static testMethod void InsertMergeAccountSyncStatus_WebServiceDisabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		Account a1 = new Account (Name = 'Unit Test Account 1');
		Account a2 = new Account (Name = 'Unit Test Account 2');
		insert a1;
		insert a2;
		
		MDM_Account_Merge__c m = new MDM_Account_Merge__c(From_Account__c = a1.Id, To_Account__c = a2.Id);
		insert m;
		
		///ACT...
		Test.startTest();
		MDMSyncMergeAccountHandler.InsertMergeAccountSyncStatus(new List<MDM_Account_Merge__c> { m }, false);
		Test.stopTest();
		
		///ASSERT...
		System.assertEquals('Synchronised', m.Synchronisation_Status__c);
	}
	
	static testMethod void SyncMergePartner_WebServiceEnabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		Account a1 = new Account (Name = 'Unit Test Account 1');
		Account a2 = new Account (Name = 'Unit Test Account 2');
		insert a1;
		insert a2;
		
		MDM_Account_Merge__c m = new MDM_Account_Merge__c(From_Account__c = a1.Id, To_Account__c = a2.Id, Synchronisation_Status__c = 'TEST');
		insert m;
		
		///ACT...
		Test.startTest();
		MDMSyncMergeAccountHandler.SyncMergePartner(new List<MDM_Account_Merge__c> { m }, new Map<id, MDM_Account_Merge__c> { m.Id => m }, true);
		Test.stopTest();
		
		///ASSERT...
		m = [SELECT id, Synchronisation_Status__c FROM MDM_Account_Merge__c WHERE id = :m.id];
		System.assertEquals('Sync Failed', m.Synchronisation_Status__c);
	}
	
	static testMethod void SyncMergePartner_WebServiceDisabled() {
		///ARRANGE...
		MDMProxyTests.setupMDMConfigSettings();
		Account a1 = new Account (Name = 'Unit Test Account 1');
		Account a2 = new Account (Name = 'Unit Test Account 2');
		insert a1;
		insert a2;
		
		MDM_Account_Merge__c m = new MDM_Account_Merge__c(From_Account__c = a1.Id, To_Account__c = a2.Id, Synchronisation_Status__c = 'TEST');
		insert m;
		
		///ACT...
		Test.startTest();
		MDMSyncMergeAccountHandler.SyncMergePartner(new List<MDM_Account_Merge__c> { m }, new Map<id, MDM_Account_Merge__c> { m.Id => m }, false);
		Test.stopTest();
		
		///ASSERT...
		m = [SELECT id, Synchronisation_Status__c FROM MDM_Account_Merge__c WHERE id = :m.id];
		System.assertEquals('TEST', m.Synchronisation_Status__c);
	}
	
	static testMethod void MDMSyncMergeAccountHandler_Coverage() {
		MDMProxyTests.setupMDMConfigSettings();
		
		Account a1 = new Account (Name = 'Unit Test Account 1');
		Account a2 = new Account (Name = 'Unit Test Account 2');
		insert a1;
		insert a2;
		
		MDM_Account_Merge__c m = new MDM_Account_Merge__c(From_Account__c = a1.Id, To_Account__c = a2.Id, Synchronisation_Status__c = 'TEST');
		insert m;
		update m;
		MDM_Account_Merge__c m2 = m.clone(true, true, true, true);
		m2.Synchronisation_Status__c = 'Sync In Progress';
		
		List<MDM_Account_Merge__c> newMerge = new List<MDM_Account_Merge__c> { m } ;
		Map<id, MDM_Account_Merge__c> newMergeMap = new Map<id, MDM_Account_Merge__c> (newMerge);
		
		List<MDM_Account_Merge__c> oldMerge = new List<MDM_Account_Merge__c> { m2 } ;
		Map<id, MDM_Account_Merge__c> oldMergeMap = new Map<id, MDM_Account_Merge__c> (oldMerge);
		
		MDMSyncMergeAccountHandler.BeforeInsert(newMerge, newMergeMap);
		MDMSyncMergeAccountHandler.AfterInsert(newMerge, newMergeMap);
		MDMSyncMergeAccountHandler.BeforeUpdate(newMerge, newMergeMap, oldMerge, oldMergeMap);
		MDMSyncMergeAccountHandler.AfterUpdate(newMerge, newMergeMap, oldMerge, oldMergeMap);
		MDMSyncMergeAccountHandler.BeforeDelete(oldMerge, oldMergeMap);
		MDMSyncMergeAccountHandler.AfterDelete(oldMerge, oldMergeMap);
		
		delete m;
	}
}