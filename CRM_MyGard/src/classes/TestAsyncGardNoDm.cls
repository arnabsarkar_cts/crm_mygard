@IsTest
public class TestAsyncGardNoDm {
    
    private static testMethod void testGetDocumentResponse_elementFutureGetValue(){
        System.assertEquals('Test Response', new AsyncGardNoDm.getDocumentResponse_elementFuture().getValue());
    }
    
    private static testMethod void testGetDocumentByGardUserIdResponse_elementFuture(){
        System.assertEquals('Test Response', new AsyncGardNoDm.getDocumentByGardUserIdResponse_elementFuture().getValue());
    }
    
    private static testMethod void testGetDocumentWithExternalGroupResponse_elementFuture(){
        System.assertEquals('Test Response', new AsyncGardNoDm.getDocumentWithExternalGroupResponse_elementFuture().getValue());
    }
    
    private static testMethod void TestAsyncGardDocumentServicePort1(){
        new AsyncGardNoDm.AsyncGardDocumentServicePort();
    }
    
    private static testMethod void TestBeginGetDocument(){
        AsyncGardNoDm.getDocumentResponse_elementFuture response = new AsyncGardNoDm.AsyncGardDocumentServicePort().beginGetDocument(null,null,null);
        System.assertEquals('Test Response', response.getValue());
    }
    
    private static testMethod void TestBeginGetDocumentByGardUserId(){
        AsyncGardNoDm.getDocumentByGardUserIdResponse_elementFuture response = new AsyncGardNoDm.AsyncGardDocumentServicePort().beginGetDocumentByGardUserId(null,null,null);
        System.assertEquals('Test Response', response.getValue());
    }
    
    private static testMethod void TestBeginGetDocumentWithExternalGroup(){
        AsyncGardNoDm.getDocumentWithExternalGroupResponse_elementFuture response = new AsyncGardNoDm.AsyncGardDocumentServicePort().beginGetDocumentWithExternalGroup(null,null,null,null);
        System.assertEquals('Test Response', response.getValue());
    }
}