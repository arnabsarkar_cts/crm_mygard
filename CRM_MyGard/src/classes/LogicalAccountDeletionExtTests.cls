@isTest
private class LogicalAccountDeletionExtTests {
    private static List<Valid_Role_Combination__c> validRoles;
    private static Gard_Team__c gt;
    private static Country__C country;
    private static Account setupTestAccount() {
        return new Account (Name = 'Unit Test Account',
                            Company_Role__c = validRoles[0].Role__c,
                            sub_Roles__c = validRoles[0].Sub_Role__c,
                            Broker_On_Risk_Flag__c = false,
                            Client_On_Risk_Flag__c = false,
                            Deleted__c = false,
                            Company_status__c = 'active',
                            Country__c = country.id
                            );
    }
    
    private static void setupValidRoles() {
        ////For some reason this seems to be throwing an internal Salesforce error /
        ////System.UnexpectedException: Salesforce System Error: 1583762872-13077 (1716107030) (1716107030)
        //validRoles = (List<Valid_Role_Combination__c>)Test.loadData(Valid_Role_Combination__c.sObjectType, 'MDMValidRoles');
        ////END...
        validRoles = new List<Valid_Role_Combination__c>();
        validRoles.add(new Valid_Role_Combination__c(Role__c = 'Investments', Sub_Role__c = 'Investments - Service Provider'));
        validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 1', Sub_Role__c = 'Sub Role 12'));
        validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 21'));
        validRoles.add(new Valid_Role_Combination__c(Role__c = 'Role 2', Sub_Role__c = 'Sub Role 22'));
        validRoles.add(new Valid_Role_Combination__c(Role__c = 'Other', Sub_Role__c = 'Other Sub Role'));
        insert validRoles;
    }
    
    private static void createCountry(){
        gt = new Gard_Team__c(Active__c = true,Office__c = 'test',Name = 'GTeam',Region_code__c = 'TEST');
        insert gt;
        country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Gard_Team__c=gt.Id,Claims_Support_Team__c=gt.Id);
        insert country;
    }
    static testMethod void LogicalAccountDeletionExt_Coverage() {
        ///ARRANGE...
        createCountry();
        setupValidRoles();
        Account a = setupTestAccount();
        a.Synchronisation_Status__c = 'Sync In Progress';
        insert a;
        
        Apexpages.currentPage().getParameters().put('id',a.Id);     
        ApexPages.StandardController sc = new ApexPages.standardController(a);
                
        Test.startTest();
        DataQualitySupport.DisableRoleGovernance(UserInfo.getUserId());
        
        LogicalAccountDeletionExt logicalAccountDeletionExt = new LogicalAccountDeletionExt(sc);
        logicalAccountDeletionExt.setRequestFlag();
        logicalAccountDeletionExt.setFlag();
        logicalAccountDeletionExt.accountsHome();
        logicalAccountDeletionExt.accountRecord();
        Test.stopTest();
        
        Account fetchedAccount = [SELECT Company_Status__c , Company_Role__c , Sub_Roles__c , Deleted__c 
                                    from ACCOUNT WHERE Name = 'Unit Test Account' LIMIT 1];
        System.assertEquals('Active',fetchedAccount.Company_Status__c);
        //System.assertEquals('',fetchedAccount.Company_Role__c );
        //System.assertEquals('',fetchedAccount.Sub_Roles__c);
        //System.assert(fetchedAccount.Deleted__c );
        
        
        /*
        DQ__c DQSettings = DataQualitySupport.GetSetting(UserInfo.getUserId());
        DQSettings.Enable_Role_Governance__c = true;
        Account fetchedAccountNew = [SELECT Company_Status__c , Company_Role__c , Sub_Roles__c , Deleted__c ,Inactivate_Company_Request__c
                                    from ACCOUNT WHERE Name = 'Unit Test Account' LIMIT 1];
        System.assert(fetchedAccountNew.Inactivate_Company_Request__c);
        */
        
    }
}