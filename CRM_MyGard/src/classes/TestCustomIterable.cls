@istest
public class TestCustomIterable 
{
	@istest static void test()
    {       
        testBranch(25);
        testBranch(30);
    }
        
    static List<ContactRoleWrapper> createCRWList(Integer n){
        List<ContactRoleWrapper> crwl=new List<ContactRoleWrapper>();
        
        for(Integer i=1;i<=n;i++){
            crwl.add(new ContactRoleWrapper(new Contact(LastName='Contact - '+(i)),'Role - '+i,true));
        }
        return crwl;
    }
    
    static void testBranch(Integer n){
        System.debug('N - '+n);
        List<ContactRoleWrapper> crwl=createCRWList(n);
        CustomIterable cit=new CustomIterable(crwl);
        List<ContactRoleWrapper> crwlAsc=new List<ContactRoleWrapper>();
        List<ContactRoleWrapper> crwlDesc=new List<ContactRoleWrapper>();
        
        while(cit.hasNext()){
            crwlAsc.addAll(cit.next());
        }
        crwlDesc.addAll(cit.Previous());
        while(cit.hasPrevious()){
            crwlDesc.addAll(cit.previous());
        }
        
        
        
        System.debug('Ascending:-');
        for(ContactRoleWrapper c:crwlAsc){
            System.debug(c);
        }
        System.debug('Descending:-');
        for(ContactRoleWrapper c:crwlDesc){
            System.debug(c);
        }
        
    }
}