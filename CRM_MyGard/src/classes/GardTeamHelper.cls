//Test Class - TestGardTeamTrigger
//Created for SF-4662, later, if Trigger Framework is implemented, Handler Methods are to be moved in a Specific or Generic Handler Class
public class GardTeamHelper{
    
    //Handler Methods
    public static void beforeInsert(List<Gard_Team__c> gts){
        createOrUpdateGardContacts(gts);
    }
    public static void beforeUpdate(List<Gard_Team__c> gts){
        createOrUpdateGardContacts(gts);
    }
    public static void afterDelete(List<Gard_Team__c> gts){
        deleteRelatedGardContacts(gts);
    }
    
    //Helper Methods
    //Created for SF-4662
    //If no GardContact is linked, creates and relates new GardContacts with GardTeams
    //If GardContact is Linked, updates it with the field values of GardTeam
    private static void createOrUpdateGardContacts(List<Gard_Team__c> gts){
        Set<Id> alreadyRelatedGardContactsIds = new Set<Id>();
        List<Gard_Contacts__c> relatedGardContacts = new List<Gard_Contacts__c>();
        Map<Id,Gard_Contacts__c> idToGardContactMap;
        List<Integer> gcsToBeRelatedIndices = new List<Integer>();
        Integer gcToBeRelatedIndex = 0;
            
        // retreive "these" GardTeams' already related GardContacts
        for(Gard_Team__c gt : gts){
            if(gt.ContactId__c != null) alreadyRelatedGardContactsIds.add(gt.contactId__c);
        }
        if(alreadyRelatedGardContactsIds.size() > 0) idToGardContactMap = new Map<Id,Gard_Contacts__c>([SELECT id FROM Gard_Contacts__c WHERE Id IN :alreadyRelatedGardContactsIds]);
        
        // create/update related GardContacts
        Gard_Contacts__c gc;
        for(Gard_Team__c gt : gts){
            if(gt.contactId__c != null) gc = idToGardContactMap.get(gt.contactId__c);
            else{//create a new contact
                gc = new Gard_Contacts__c();
                gcsToBeRelatedIndices.add(gcToBeRelatedIndex);
            }
            
            //set/update the related GardContact's fields with this GardTeam's data
            gc.FirstName__c = gt.name;
            gc.phone__c = gt.phone__c;
            gc.Office_city__c = gt.office__c;
            gc.email__c = gt.P_I_email__c;
            gc.marine_email__c = gt.Marine_Email__c;
            gc.Is_Gard_Team__c = true;
            gc.isActive__c = gt.Active__c;
            
            relatedGardContacts.add(gc);
            gcToBeRelatedIndex++;
        }
        if(relatedGardContacts.size() > 0) upsert relatedGardContacts;
        
        // relate/link the newly created GardContacts with GardTeams
        for(Integer index : gcsToBeRelatedIndices){
            gts[index].contactId__c = relatedGardContacts[index].id;//the sequence of the gts and gcs must be same
        }
    }
    
    //created for SF-4662, deletes related GardContact
    private static void deleteRelatedGardContacts(List<Gard_Team__c> gts){
        List<Id> relatedGardContactIds = new List<Id>();
        for(Gard_Team__c gt : gts){
            if(gt.ContactId__c != null) relatedGardContactIds.add(gt.ContactId__c);
        }
        
       Database.delete(relatedGardContactIds);
    }
}