@isTest
private class TestCaseClaimHandler_Batch {
    
    Static testMethod void  CaseClaimHandler()
    {
        String RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Claim').getRecordTypeId();
        Account acc = TestDataGenerator.getClientAccount();
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        
            /*Object__c   test_object_2nd = New Object__c(Dead_Weight__c= 20,
                                 Object_Unique_ID__c = '1934lkolko',
                                 Name='hennessey all',
                                 Object_Type__c='Cruise Vessel',
                                 No_of_passenger__c=100,
                                 No_of_crew__c=70,
                                 Gross_tonnage__c=1000,
                                 Length__c=20,
                                 Width__c=20,
                                 Depth__c=30,
                                 Port__c='Alabama',
                                 Signal_Letters_Call_sign__c='hellooo',
                                 //Dummy_Object_flag__c = 0,
                                 Dummy_Object_flag__c=0);
            
            insert  test_object_2nd;*/
            
            /*Contract brokercontract_1st= New Contract(Accountid = acc.id, 
                                                        Account = acc, 
                                                        Status =   'Draft'   ,
                                                        CurrencyIsoCode =  'USD' , 
                                                        StartDate = Date.today(),
                                                        ContractTerm = 2,
                                                        Agreement_Type__c = 'Charterers', 
                                                        Client__c = acc.id,
                                                        Agreement_ID__c = 'Agreement_ID_879'                                                           
                                                );
            insert brokercontract_1st;*/
                 
            /*Asset clientAsset_1st = new Asset(Name=  'Asset 1' ,
                         accountid =acc.id,
                         Cover_Group__c =  'P&I' ,
                         CurrencyIsoCode= 'USD'  ,
                         Object__c = test_object_2nd.id,
                         product_name__c =  'P&I Cover' ,
                         Underwriter__c = usr.id,
                         On_risk_indicator__c=true,
                         Expiration_Date__c = date.today().adddays(50),
                         Inception_date__c = date.today(),
                         Agreement__c =brokercontract_1st.id ,
                         Risk_ID__c = '123456ri'
                                     ); 
            insert clientAsset_1st;*/
            
            Case clientCase = new Case(Accountid = acc.id, 
                         Origin =  'Community' ,
                         Account = acc,
                         Claims_handler__c = usr.id,
                         Claim_Incurred_USD__c = 50000,
                         Uwr_form_name__c =  'test1' ,
                         Event_details__c = 'Testing ReportedClaimsCtrl',
                         guid__c = '8ce8ad16-a6ef-1824-9e24',
                         Contact_me_area__c = 'Claim',
                         Paid__c = 50000, 
                         Event_Type__c = 'Incident', 
                         Claim_Type__c = 'Cargo' ,
                         Last_name__c = 'yo1',
                         Event_Date__c = Date.valueof('2014-12-12'),
                         Claim_Reference_Number__c = 'ClaiMRef1234',
                         Status_Change_Date__c = datetime.now(),
                         Status = 'Open',
                         Type = 'Add new user',
                         RecordtypeId= RecordTypeIdCase,
                         Submitter_company__c = acc.id,
                          OwnerId = usr.id
                          );
                    insert clientCase;
                    List<Case> CaseList = new List<Case>();
                    CaseList.add(clientCase);
        Test.startTest();
            CaseClaimHandler_Batch job = new CaseClaimHandler_Batch();
            Database.QueryLocator ql = job.start(null);
            job.execute(null,CaseList);
            job.Finish(null);
            
            //Id batchprocessid = Database.executeBatch(job);
        Test.StopTest();
        
    }
}