@isTest(seeAllData=false)
public class TestPEMELandingPageCtrlClient
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static List<Object__c> ObjectList=new List<Object__c>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    //Public static List<Object__c> ObjectList=new List<Object__c>();
    public static List<Contract> contractList=new List<Contract>();
    public static list<Asset> assetTest=new list<Asset>();
    public static List<PEME_Enrollment_Form__c> pefList=new List<PEME_Enrollment_Form__c>();
    public static List<PEME_Manning_Agent__c>  pmgList=new List<PEME_Manning_Agent__c>(); 
    public static List<PEME_Debit_note_detail__c> pdnList=new List<PEME_Debit_note_detail__c>();
    public static PEME_Invoice__c pi;
    public static PEME_Exam_Detail__c PExamDetail;
    public static Extranet_Favourite__c ef;
    public static List<Extranet_Global_Client__c> globalClientList=new List<Extranet_Global_Client__c>();
    public static set<String> manAgentIdSet=new set<String>();
    public static List<AccountToContactMap__c> AccConMapList=new List<AccountToContactMap__c>(); 
    Public static Account_Contact_Mapping__c Accmap_2nd;
    Public static list<Account_Contact_Mapping__c> Accmap_list = new list<Account_Contact_Mapping__c>();
    public static List<Extranet_Global_Client__c> extClientList=new List<Extranet_Global_Client__c>();
    public static PEME_LandingPage__c pemeLP = new PEME_LandingPage__c();
    public static  Attachment pemeAttach = new Attachment();
    public static User user1,clientUser;
    public static Account clientAcc;
    public static Contact clientContact;
    public static Object__c Object1,Object2;
    public static Contract clientContract;
    public static  Asset clientAsset;
    public static PEME_Enrollment_Form__c pefClient;
    public static PEME_Manning_Agent__c pmgClient;
    public static PEME_Debit_note_detail__c pdnClient;
    public static Extranet_Global_Client__c globalClient;
    public static AccountToContactMap__c AccConMapClient;
    public static Extranet_Global_Client__c clientClient1;
    public static List<ContactSubscriptionFields__c> conFieldsList = new list<ContactSubscriptionFields__c>();
    public static ContactSubscriptionFields__c conSubFields,conSubFields2,conSubFields3;
        
    public static  void createTestData()
    {
        //added
        ClientActivityEvents__c cae = new ClientActivityEvents__c(Name = 'Delete Debit Note' ,Event_Type__c ='Delete Debit Note');
        insert cae;
        //added
        CommonVariables__c cvar = new CommonVariables__c(name = 'ReplyToMailId', value__c ='Test123@gmail.com');
        insert cvar;
        //added
        AdminUsers__c setting = new AdminUsers__c();
            setting.Name = 'Number of users';
            setting.Value__c = 3;
        insert setting;
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA');
        insert country;
        gc = new Gard_Contacts__c(FirstName__c ='test',lastName__c = 'test');
        insert gc;
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab12000');
        insert Markt;
        User user = new User(
            Alias = 'standt', 
            profileId = salesforceLicenseId ,
            Email='standarduser@testorg.com',
            EmailEncodingKey='UTF-8',
            CommunityNickname = 'test13',
            LastName='Testing',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',  
            TimeZoneSidKey='America/Los_Angeles',
            UserName='test008@testorg.com',
            ContactId__c = gc.id,
            City= 'Arendal'
        );
        insert user ;
        user1 =new User();
        user1 = [select id, contactid__c from user where id=:user.id];
        clientAcc= New Account();
        clientAcc.Name = 'Testt';
        clientAcc.recordTypeId = System.Label.Client_Contact_Record_Type;
        clientAcc.Site = '_www.test.se';
        clientAcc.Type = 'Customer';
        clientAcc.BillingStreet = 'Gatan 1';
        clientAcc.BillingCity = 'Stockholm';
        clientAcc.BillingCountry = 'SWE';    
        clientAcc.Area_Manager__c = user1.id;        
        clientAcc.Market_Area__c = Markt.id;
        clientAcc.PEME_Enrollment_Status__c = 'Enrolled';
        clientAcc.country__c = country.id;
        AccountList.add(clientAcc);
        insert AccountList; 
        clientContact= new Contact( FirstName='tsat_1',
                                   LastName='chak',
                                   MailingCity = 'London',
                                   MailingCountry = 'United Kingdom',
                                   MailingPostalCode = 'SE1 1AE',
                                   MailingState = 'London',
                                   MailingStreet = '4 London Road',
                                   AccountId = clientAcc.Id,
                                   Email = 'test321@gmail.com'
                                  );
        ContactList.add(clientContact);
        insert ContactList;
        clientUser = new User( Alias = 'alias_1',
                              CommunityNickname = 'clientUser',
                              Email='test_11@testorg.com', 
                              profileid = partnerLicenseId, 
                              EmailEncodingKey='UTF-8',
                              LastName='tetst_006',
                              LanguageLocaleKey='en_US',
                              LocaleSidKey='en_US',
                              TimeZoneSidKey='America/Los_Angeles',
                              UserName='testclientUser@testorg.com.mygard',
                              ContactId = clientContact.id
                             );
        
        UserList.add(clientUser) ;
        insert UserList;
        
        Object1=new Object__c(Name= 'TestObject1',
                              //CurrencyIsoCode='NOK-Norwegian Krone',
                              Object_Unique_ID__c='test11223'
                             );
        ObjectList.add(Object1);
        Object2=new Object__c(
            Name= 'TestObject2',
            // CurrencyIsoCode='NOK-Norwegian Krone',
            Object_Unique_ID__c='test11224'
        );
        ObjectList.add(Object2);
        insert ObjectList;
        clientContract=new Contract(
            Accountid = clientAcc.id, 
            Account = clientAcc, 
            Status = 'Draft',
            CurrencyIsoCode = 'SEK', 
            StartDate = Date.today(),
            ContractTerm = 2,
            //Broker__c = brokerAcc.id,
            Client__c = clientAcc.id,
            Expiration_Date__c = date.valueof('2016-02-01'),
            Agreement_Type__c = 'P&I',
            Agreement_Type_Code__c = 'Agreement123',
            Inception_date__c = date.valueof('2012-01-01'),
            Business_Area__c='P&I',
            Policy_Year__c='2014',
            Name_of_Contract__c = 'ABC'
        );
        contractList.add(clientContract);
        insert contractList;
        clientAsset=new Asset();
        
        clientAsset.AccountId=clientAcc.ID;
        clientAsset.ContactId=clientContact.ID;
        clientAsset.Agreement__c=clientContract.ID;
        clientAsset.Object__c=Object2.ID;
        clientAsset.Name='P&I Cover';
        clientAsset.Expiration_Date__c=date.valueof('2016-02-01');
        clientAsset.On_risk_indicator__c=true;
        clientAsset.Inception_date__c =date.valueof('2012-01-01');
        clientAsset.Risk_ID__c='112233eeffddggft';
        assetTest.add(clientAsset);
        
        insert assetTest;
        
        pefClient=new PEME_Enrollment_Form__c();
        pefClient.ClientName__c=clientAcc.ID;
        pefClient.Enrollment_Status__c='Draft';
        pefClient.Submitter__c=clientContact.Id;
        pefList.add(pefClient);
        insert pefList;
        
        pmgClient=new PEME_Manning_Agent__c();
        //pmg.Name='testpmg';
        pmgClient.Client__c=clientAcc.ID;
        pmgClient.PEME_Enrollment_Form__c=pefClient.ID;
        pmgClient.Company_Name__c='Testc Company';
        // pmgClient.Contact_Person__c='Testc Person';
        pmgClient.Contact_Point__c=clientContact.Id;
        //pmgClient.Email__c='testc@mail.com';
        //  pmgClient.Fax__c=1222334455;
        //  pmgClient.Object__c=Object2.ID;
        //pmgClient.Phone__c='1132334433';
        pmgClient.Status__c='Approved';
        pmgClient.Requested_Address__c='testcaddr1';
        pmgClient.Requested_Address_Line_2__c='testcaddr2';
        pmgClient.Requested_Address_Line_3__c='testcaddr3';
        pmgClient.Requested_Address_Line_4__c='testcaddr4';
        pmgClient.Requested_City__c='testcCity1';
        pmgClient.Requested_Contact_Person__c='testc person1';
        pmgClient.Requested_Country__c='testccountry1';
        pmgClient.Requested_Email__c='testc1@mail.com';
        //   pmgClient.Requested_Fax__c=1124332211;
        pmgClient.Requested_Phone__c='1154228855';
        pmgClient.Requested_State__c='testcstate1';
        pmgClient.Requested_Zip_Code__c='testczip';
        pmgList.add(pmgClient);
        
        insert pmgList;
        
        manAgentIdSet.add(pmgClient.Id);
        
        pdnClient=new PEME_Debit_note_detail__c();
        //pdn.Name='testpdn';
        //pdnClient.Address__c='testpdncaddr';
        //pdnClient.City__c='testpdnccity';
        pdnClient.PEME_Enrollment_Form__c=pefClient.ID;
        //pdnClient.Country__c='testpdnccountry';
        //pdnClient.State__c='testpdncstate';
        //pdnClient.Zip_code__c='442355';
        pdnClient.Contact_person_Email__c='pdnc@test.com';
        pdnClient.Contact_person_Name__c='conpdncPerson';
        //  pdnClient.Related_to_manning_agents__c='testclistman';
        //  pdnClient.Related_to_objects__c='objclisttest';
        //pdn.PEME_Enrollment_Form__c=pef.Id;
        //pdnClient.Address_Line_2__c='testc2';
        //pdnClient.Address_Line_3__c='testc3';
        //pdnClient.Address_Line_4__c='testc4';
        pdnClient.Requested_Contact_person_Email__c='pdnc@test1.com';
        pdnClient.Requested_Contact_person_Name__c='conpdncPerson1';
        pdnClient.Requested_City__c='testpdnccity1';
        pdnClient.Requested_Country__c='testccountry1';
        pdnClient.Requested_State__c='testcstate1';
        pdnClient.Requested_Zip_Code__c='222755';
        pdnClient.Requested_Address__c='testcaddr1';
        pdnClient.Requested_Address_Line_2__c='testcaddr2';
        pdnClient.Requested_Address_Line_3__c='testcaddr3';
        pdnClient.Requested_Address_Line_4__c='testcaddr4';
        pdnClient.Status__c='Approved';
        pdnClient.Comments__c='testc comment';
        pdnClient.RequestedComments__c='testc comment1';
        pdnList.add(pdnClient);
        
        insert pdnList; 
        
        pi=new PEME_Invoice__c ();
        //pi.Client__c=clientAcc.id;
        pi.Clinic_Invoice_Number__c='qwerty123';
        pi.Clinic__c=clientAcc.id;
        pi.Crew_Examined__c=11223;
        pi.Invoice_Date__c=Date.valueOf('2016-01-05');
        pi.PEME_reference_No__c='aasd23';
        pi.Status__c='Approved';
        pi.ObjectList__c='qwerty for test purpose';
        pi.Total_Amount__c=112230;
        pi.Vessels__c= Object1.id;
        pi.PEME_Enrollment_Detail__c = pefClient.id;
        //pi.GUID__c='guid1';
        pi.Period_Covered_From__c=Date.valueOf('2016-01-05');
        pi.Period_Covered_To__c =Date.valueOf('2016-01-28');
        insert pi;
        pi.guid__c = '1ghsdjhasgdjhasg';
        update pi;
        PExamDetail=new PEME_Exam_Detail__c ();
        PExamDetail.Age__c=30;
        PExamDetail.Amount__c=12000;
        PExamDetail.Comments__c='test comment'; 
        PExamDetail.Date__c=Date.valueOf('2016-01-10');
        PExamDetail.Examination__c='Demo Exam';
        PExamDetail.Examinee__c='Demo employee';
        // PExamDetail.Id=;
        PExamDetail.Invoice__c=pi.id;
        PExamDetail.Object_Related__c=Object1.id;
        PExamDetail.Rank__c='Five';
        PExamDetail.Status__c='Approved';
        
        insert PExamDetail;
        //creating more than 1000 PEME Exam details
        List<PEME_Exam_Detail__c> lstPEMEexamDetails = new List<PEME_Exam_Detail__c>();
        for(integer i=0;i<1500;i++){
        PEME_Exam_Detail__c PExamDetail=new PEME_Exam_Detail__c();  
        PExamDetail.Age__c=30;
        PExamDetail.Amount__c=12000;
        PExamDetail.Comments__c='test comment'+i; 
        PExamDetail.Date__c=Date.valueOf('2016-01-10');
        PExamDetail.Examination__c='Demo Exam'+i;
        PExamDetail.Examinee__c='Demo employee';
        // PExamDetail.Id=;
        PExamDetail.Invoice__c=pi.id;
        PExamDetail.Object_Related__c=Object1.id;
        PExamDetail.Rank__c='Five';
        PExamDetail.Status__c='Approved';
        lstPEMEexamDetails.add(PExamDetail);
        }
        insert lstPEMEexamDetails;
        
        PEME_Contact__c pcTest = new PEME_Contact__c();
        pcTest.Contact__c = clientContact.ID;
        pcTest.PEME_Enrollment_Detail__c = pefClient.ID;
        Insert pcTest;
        
        ef=new Extranet_Favourite__c(
            Client__c=clientAcc.id,
            Favourite_By__c=clientUser.id, 
            Filter_Claim_Type__c='crew',
            Filter_Claim_Year__c = '2015',  
            Filter_Client__c='Borealis Maritime Limited',
            Filter_Cover__c= 'p&i',
            Filter_Document_Type__c='type 1',
            Filter_Global_Client_Id__c=clientAcc.id,
            Filter_Global_Client__c='Golden Ocean Management As',
            Filter_On_Risk__c='Y',
            Filter_Policy_Year__c= '2015',
            Filter_Product_Area__c= 'p&i',
            Item_Name__c='testGARD',
            Item_Type__c= 'Saved Search',
            Object__c=Object1.id, 
            Saved_Query__c=' SELECT Agreement__r.Client__r.Name,Object__r.Id, Object__r.Name, Object__r.Object_Type__c, UnderwriterNameTemp__c,UnderwriterId__c, Product_Name__c,Agreement__r.Business_Area__c, Agreement__r.Broker__r.Name , Expiration_Date__c , Agreement__r.Policy_Year__c , Claims_Lead__c , Gard_Share__c , On_risk_indicator__c,Object__r.Dummy_Object_flag__c FROM Asset Where Agreement__c IN : setCoverID AND On_risk_indicator__c IN : lstblOnrisk ORDER BY Object__r.Object_Type__c ASC limit 10000'
        );
        insert ef;
        globalClient=new Extranet_Global_Client__c();
        globalClient.Selected_By__c=clientUser.Id;
        globalClient.Selected_For_Client__c=clientAcc.Id;
        globalClient.Selected_Global_Clients__c='global client list';
        globalClientList.add(globalClient);
        insert globalClientList;
        
        AccConMapClient=new AccountToContactMap__c();     
        AccConMapClient.Name=clientContact.id;
        AccConMapClient.AccountId__c=clientAcc.id;
        AccConMapClient.RolePreference__c ='Client';
        AccConMapList.add(AccConMapClient);
        
        insert(AccConMapList);     
        
        Accmap_2nd = new Account_Contact_Mapping__c(Account__c =  clientAcc.id,
                                                    Active__c = true,
                                                    Administrator__c = 'Normal',
                                                    Contact__c = clientContact.id,
                                                    IsPeopleClaimUser__c = false,
                                                    Marine_access__c = true,
                                                    PI_Access__c = true
                                                    //Show_Claims__c = false,
                                                    //Show_Portfolio__c = false 
                                                   );
        Accmap_list.add(Accmap_2nd);
        
        Insert Accmap_list;                                           
        
        clientClient1=new Extranet_Global_Client__c();
        clientClient1.Selected_By__c=clientUser.Id;
        clientClient1.Selected_For_Client__c=clientAcc.Id;
        clientClient1.Selected_Global_Clients__c=''+';'+clientAcc.Id;
        extClientList.add(clientClient1);
        
        insert extClientList;
        
        pemeLP.About__c = 'checking';
        pemeLP.Administration__c = 'testing';
        pemeLP.Clinics__c = 'Clinic';
        pemeLP.Confidentiality__c = 'confidentiality';
        pemeLP.Enhanced_Medical_test__c = 'EnhancedMedicalTest';
        pemeLP.Expenses__c = 'expenses';
        pemeLP.Fit_for_sea_duty_standards__c = 'FitForSeaDutyStandards';
        pemeLP.Manning_agents__c = 'manningAgents';
        pemeLP.PEME_certificates__c = 'pemeCertificates';
        pemeLP.Validity_for_PEME__c = 'validityForPeme';
        insert pemeLP;
        
        pemeAttach.parentID = pemeLP.ID;
        pemeAttach.name = 'testAttach';
        pemeAttach.body = Blob.valueOf('Unit Test Attachment Body');
        pemeAttach.ContentType = 'application/pdf';
        insert pemeAttach;
    }
    Public static testmethod void cover1(){
        /*conSubFields = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields' , Subscription_Fields__c ='Publication_Container__c,Publication_Dry_bulk__c,Publication_Energy__c,Publication_General_cargo__c');
conSubFields2 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 2' , Subscription_Fields__c = 'Publication_English__c,Publication_Japanese__c,Publication_Chinese__c,Publication_HTML_image_text__c,Publication_Alerts__c');
conSubFields3 = new ContactSubscriptionFields__c (name = 'Contact Subscription Fields 3' , Subscription_Fields__c  = 'Publication_Gard_Rules_Norwegian__c,Publication_Rules_Norwegian_Number_of_co__c,Publication_Guidance_to_Master__c,Publication_GtM_Number_of_Copies__c');
conFieldsList.add(conSubFields);
conFieldsList.add(conSubFields2);
conFieldsList.add(conSubFields3);
insert conFieldsList; */
        createTestData();
        System.runAs(clientUser)
        {
            Test.startTest();
            System.currentPageReference().getParameters().put('favId' ,ef.ID);
            PEMELandingPageCtrl clientLandingPageCtrl=new PEMELandingPageCtrl();
            clientLandingPageCtrl.selectedClient=clientAcc.ID;
            clientLandingPageCtrl.getSortDirection();
            clientLandingPageCtrl.setSortDirection('ASC');
            //clientLandingPageCtrl.getClientLst();
            //clientLandingPageCtrl.getClientOptions();
            clientLandingPageCtrl.getClinicOptions();
            clientLandingPageCtrl.getExamYrLst();
            clientLandingPageCtrl.getExamination();
            clientLandingPageCtrl.getObj();
            clientLandingPageCtrl.getYearOptions();
            //clientLandingPageCtrl.sortExpression='ASC';
            //clientLandingPageCtrl.fetchClientForBroker();  //sfedit uncomment it
            //clientLandingPageCtrl.fetchLandingPageInfo();
            clientLandingPageCtrl.fetchEnrollments();
            clientLandingPageCtrl.selectedClientFilter=new List<String>();
            clientLandingPageCtrl.selectedClinic=new List<String>();
            clientLandingPageCtrl.selectedYear=new List<String>();
            clientLandingPageCtrl.selectedExam=new List<String>();
            clientLandingPageCtrl.selectedObj=new List<String>();
            clientLandingPageCtrl.selectedClientFilter.add('testFilter');
            clientLandingPageCtrl.selectedClinic.add('testClinic');
            clientLandingPageCtrl.selectedYear.add('2015');
            clientLandingPageCtrl.selectedExam.add('testYear');
            clientLandingPageCtrl.selectedObj.add('testObj');
            clientLandingPageCtrl.loadData();
            //clientLandingPageCtrl.generateClientInvoices();
            clientLandingPageCtrl.getClinicOptions();
            clientLandingPageCtrl.getExamYrLst();
            clientLandingPageCtrl.getExamination();
            clientLandingPageCtrl.getObj();
            clientLandingPageCtrl.getYearOptions();
            clientLandingPageCtrl.createOpenUserList();
            clientLandingPageCtrl.firstOpen();
            clientLandingPageCtrl.hasPreviousOpen=true;
            clientLandingPageCtrl.previousOpen();
            clientLandingPageCtrl.setpageNumberOpen();
            clientLandingPageCtrl.hasNextOpen=false;
            clientLandingPageCtrl.nextOpen();
            clientLandingPageCtrl.lastOpen();
            clientLandingPageCtrl.newManningAgent=new PEME_Manning_Agent__c();
            clientLandingPageCtrl.newManningAgent=pmgClient;
            clientLandingPageCtrl.addManningAgent();
            clientLandingPageCtrl.manningAgent='0';
            clientLandingPageCtrl.populateManForUpdate();
            clientLandingPageCtrl.updateManningAgent();
            clientLandingPageCtrl.populateManForDelete();
            clientLandingPageCtrl.deleteManningAgent();
            clientLandingPageCtrl.updtDebitNote = pdnClient;
            clientLandingPageCtrl.updateBillingAddress();
            PEMELandingPageCtrl.delManAgentFuture(manAgentIdSet);
            clientLandingPageCtrl.sendDebitNotesUpdateEmails();
            //clientLandingPageCtrl.sendManAgentInsertEmails();
            //clientLandingPageCtrl.sendManAgentUpdateEmails();
            //clientLandingPageCtrl.sendManAgentDeleteEmails();
            PEMELandingPageCtrl.sendEnrollmentApprovalEmails(pefClient);
            clientLandingPageCtrl.selectedClinic=new list<String>();
            clientLandingPageCtrl.selectedYear=new list<String>();
            clientLandingPageCtrl.selectedExam=new list<String>();
            clientLandingPageCtrl.selectedObj=new list<String>();
            clientLandingPageCtrl.selectedClientFilter=new list<String>();
            clientLandingPageCtrl.clearOptions();
            clientLandingPageCtrl.print();
            clientLandingPageCtrl.exportToExcel();
            clientLandingPageCtrl.checkIfClientEnrolled();
            clientLandingPageCtrl.returnToPage();
            clientLandingPageCtrl.fetchGlobalClients();
            clientLandingPageCtrl.strSelected=''+';'+clientAcc.Id;
            clientLandingPageCtrl.fetchSelectedClients();
            clientLandingPageCtrl.getClientLst();
            clientLandingPageCtrl.getClientOptions();
            
            Test.stopTest();
        }
    }
    Public static testmethod void cover2(){
        createTestData();
        System.runAs(clientUser){
            Test.startTest();
            System.currentPageReference().getParameters().put('favId' ,ef.ID);
            PEMELandingPageCtrl clientLandingPageCtrl2=new PEMELandingPageCtrl();
            clientLandingPageCtrl2.newFilterName='testFilter';
            clientLandingPageCtrl2.createFavourites();
            clientLandingPageCtrl2.deleteFavourites();
            //clientLandingPageCtrl2.fetchFavourites();
            clientLandingPageCtrl2.refreshPage();
            clientLandingPageCtrl2.closeConfirmPopUp();
            clientLandingPageCtrl2.userAccount=clientAcc;
            clientLandingPageCtrl2.size=10;
            clientLandingPageCtrl2.intStartrecord=5;
            clientLandingPageCtrl2.intEndrecord=10;
            clientLandingPageCtrl2.jointSortingParam='testing';
            clientLandingPageCtrl2.lstClients=new Set<String>();
            clientLandingPageCtrl2.lstClients.add(clientAcc.Id);
            clientLandingPageCtrl2.getClientOptions();
            clientLandingPageCtrl2.newDebitNote = pdnClient;
            clientLandingPageCtrl2.addDebitNotes();
            clientLandingPageCtrl2.populateDebForUpdate();
            PEME_Debit_Note_Detail__c tempPDNClient= pdnClient.clone(false,false,false,false);
            insert tempPDNClient;
            clientLandingPageCtrl2.updtDebitNote = tempPDNClient;
            clientLandingPageCtrl2.lstEnrollments.add(pefClient);
            clientLandingPageCtrl2.updateDebitNote();
            clientLandingPageCtrl2.populateDebForDelete();
            clientLandingPageCtrl2.sendDebitNotesInsertEmails();
            clientLandingPageCtrl2.delDebitNote = pdnClient;
            clientLandingPageCtrl2.deleteDebitNote();
            PEMELandingPageCtrl.delDebitNoteFuture(new Set<String>{pdnClient.id});
            Test.stopTest();
        }
    }
}