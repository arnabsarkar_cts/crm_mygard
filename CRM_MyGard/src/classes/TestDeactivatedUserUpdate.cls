@isTest(seeAllData=False)
private class TestDeactivatedUserUpdate{
    private static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    private static Gard_Contacts__c dummyGardContact;
    private static User dummyUser;
    private static GardContacts__c dummyCustomSetting ;
    private static User gardStandardUser,sysAdmin,fetchedUser;
     ///////*******Variables for removing duplicate literals********************//////////////
    public static string mailStr =   'abc@gmail.com';
    public static string cntryISOStr =   'USD';
    public static string phStr =  '9874887039';
    public static string cityStr =   'Arendal'; 
    /////**********************end**************************/////
    public static void setupCustomSettingTestData(){
        dummyCustomSetting = new GardContacts__c(Email__c= 'companymail@gard.no',
                                            Office_city__c= 'Arendal',
                                            Phone__c ='+47 37019100',
                                            Name='DeactivatedUsers'
                                           );
        insert  dummyCustomSetting ;  
        AdminUsers__c setting = new AdminUsers__c();
        setting.Name = 'Number of users';
        setting.Value__c = 3;
        insert setting;                                     
    }
      
    public static void setupTestData(){
        dummyUser= new User(profileId = salesforceLicenseId,
                            Alias='Akka',
                            LastName ='malhotra',
                            CommunityNickname='Pun',
                            Email= mailStr  ,
                            Username='abc@gmail.com007.com',
                            CurrencyIsoCode= cntryISOStr ,
                            License_Required__c='Salesforce',
                            LanguageLocaleKey='en_US',
                            DefaultCurrencyIsoCode= cntryISOStr ,
                            TimeZoneSidKey='Europe/London',
                            LocaleSidKey='no_NO',
                            EmailEncodingKey='ISO-8859-1',
                            Phone= phStr ,
                            City= cityStr 
                           );
        insert dummyUser;
    }
   
    private static testmethod void testDeactiveUser(){
        Test.startTest();
        
        TestDeactivatedUserUpdate.setupTestData();
        TestDeactivatedUserUpdate.setupCustomSettingTestData();
        
        Test.stopTest();
        
        List<User> usrList = new List<User>();
        
        fetchedUser = [SELECT ID, isActive, LastName, ContactId__c FROM USER WHERE Alias='Akka'];
        System.assertEquals('malhotra', fetchedUser.LastName,'malhotra');
        List<Gard_Contacts__c> fetchedGardContacts = [SELECT ID,Email__c,Phone__c,Office_city__c FROM Gard_Contacts__c];
        System.AssertEquals(1,fetchedGardContacts.size() , 1);
        System.AssertEquals( mailStr  ,fetchedGardContacts.get(0).Email__c ,  mailStr  );
        System.AssertEquals( cityStr ,fetchedGardContacts.get(0).Office_city__c,  cityStr );
        System.AssertEquals( phStr ,fetchedGardContacts.get(0).Phone__c ,  phStr );
        
        Profile sysAdminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        Profile gardStandardUserProfile = [SELECT Id FROM Profile WHERE Name = 'Gard Standard User (UWR)'];
        sysAdmin = new User(profileId = sysAdminProfile.Id,
                            Alias='sysadmin',
                            LastName ='deb',
                            CommunityNickname='Sudipta',
                            Email= mailStr  ,
                            Username='suddeb@gmail.com007.com',
                            CurrencyIsoCode= cntryISOStr ,
                            License_Required__c='Salesforce',
                            LanguageLocaleKey='en_US',
                            DefaultCurrencyIsoCode= cntryISOStr ,
                            TimeZoneSidKey='Europe/London',
                            LocaleSidKey='no_NO',
                            EmailEncodingKey='ISO-8859-1',
                            Phone= phStr ,
                            City= cityStr ,
                            isActive=true
                           );
        usrList.add(sysAdmin);
        gardStandardUser = new User(profileId = gardStandardUserProfile.Id,
                            Alias='GrdStd',
                            LastName ='UWR',
                            CommunityNickname = 'UwrUsr',
                            Email = 'gardStandardUser@gmaill.com'  ,
                            Username = 'gardStandardUser@gmaill.com',
                            CurrencyIsoCode = cntryISOStr ,
                            License_Required__c = 'Salesforce',
                            LanguageLocaleKey = 'en_US',
                            DefaultCurrencyIsoCode = cntryISOStr ,
                            TimeZoneSidKey = 'Europe/London',
                            LocaleSidKey = 'no_NO',
                            EmailEncodingKey ='ISO-8859-1',
                            Phone = phStr ,
                            City = cityStr ,
                            isActive = true
                           );
        usrList.add(GardStandardUser);
        insert usrList;
        
        System.runAs(sysAdmin){ 
            fetchedUser.isActive = false;
            update fetchedUser; 
        }
        System.runAs(GardStandardUser){
            updateUser();
        }
    }
    
    @future
    public static void updateUser(){
        System.runAs(GardStandardUser){
            fetchedUser.isActive = false;
            GardUtils.flag = false;
            update fetchedUser; 
        }
    }
}