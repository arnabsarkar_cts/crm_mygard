/**
 * This class contains unit tests for validating the behavior of Apex trigger fluidoInvitation_BeforeDelete
 */
@isTest
private class fluidoInvitation_BeforeDeleteTest {

    static testMethod void myUnitTest() {         
        fluidoconnect__Survey__c s1 = new  fluidoconnect__Survey__c(name='Kokeilukysely1', 
                    fluidoconnect__Status__c='Open',
                    Invitations_Locked__c=true,
                    fluidoconnect__Event_Owner__c=UserInfo.getUserId(),
                    fluidoconnect__Event_Organiser__c=UserInfo.getUserId()
                    );        
        insert s1;
        system.assert(s1.Id != null);
          
        fluidoconnect__Invitation__c inv = new fluidoconnect__Invitation__c(fluidoconnect__Survey__c=s1.id);
        insert inv;
        system.assert(inv.Id != null);

        Test.StartTest();
        
        delete inv; // try to delete the survey member
        List<fluidoconnect__Invitation__c> invTmp = [SELECT Id FROM fluidoconnect__Invitation__c WHERE Id=:inv.Id];
        system.assertEquals(invTmp.size(), 0);                 
        
        Test.StopTest();
 
        
    }
}