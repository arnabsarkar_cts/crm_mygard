@isTest
public class testOpportunityChecklistNameMigration{
    @isTest static void TestOTMNotesAtt(){
        OpportunityChecklistNameMigration aotm= new OpportunityChecklistNameMigration();
        aotm.doOperation();
    }
/*
    public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public List<Opportunity> allOpp = [SELECT ID FROM Opportunity WHERE RecordTypeName__c = 'P_I'];
    public static String strAccId;
    public static String strOppId;
    public static User salesforceUser;
    public static Opportunity_Checklist__c testOppChecklistsOwners,testOppChecklistsMOU,testOppChecklistsCharterers;
     //Create a user
    public static void createUser(){
    
        salesforceUser = new User(
        Alias = 'standt', 
        profileId = salesforceLicenseId ,
        Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8',
        CommunityNickname = 'test13',
        LastName='Testing',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',  
        TimeZoneSidKey='America/Los_Angeles',
        UserName='test008@testorg.com'
        );
        insert salesforceUser; 
    }
     //create an account record
    public static void createAccount(){

        Account acc = new Account(  Name='APEXTESTACC001',
        BillingCity = 'Bristol',
        BillingCountry = 'United Kingdom',
        BillingPostalCode = 'BS1 1AD',
        BillingState = 'Avon' ,
        BillingStreet = '1 Elmgrove Road',
        Area_Manager__c = TestDataGenerator.getMarketAreaManagerUser().Id,
        Market_Area__c = TestDataGenerator.getMarketArea().Id,
        OwnerId = salesforceUser.id
        );
        insert acc;
        strAccId = acc.Id; 
    }
     //create opportunity
        public static void createOpp(){
        Id strRecId =[SELECT Id FROM RecordType WHERE Name='P&I' AND sObjectType='Opportunity' ].id;
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = strRecId ;
        opp.Name = 'APEXTESTOPP001';
        opp.AccountId = strAccId;
        opp.StageName = 'Risk Evaluation';//Renewable Opportunity
        opp.Type = 'New Business';        
        opp.Business_Type__c = 'MOUs';
        opp.Approval_Criteria__c = 'Self Approval';
        opp.Sales_Channel__c = 'Direct';
        opp.CloseDate = Date.Today();
        opp.Confirm_not_on_sanction_list__c = true;
        opp.Amount = 100;
        insert opp;
        strOppId = opp.Id;
    }
        //Create Opportunity checklists
        public static void createOppCListOwners(){
        testOppChecklistsOwners = new Opportunity_Checklist__c (Name = 'TestNameOwners',
                                       Owners_P_I_Completed__c = true,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Charterers_Deductiblesv2__c= true, Charterers_Deductibles_Commentsv2__c='test',
                                       MOU_Completed__c=true,Charterers_Completed__c =false,
                                       Agreement_type__c = 'Owners');
                                       
        insert testOppChecklistsOwners ;
    }
    
    public static void createOppCListMOU(){
        testOppChecklistsMOU = new Opportunity_Checklist__c (Name = 'TestNameMOU',
                                       Owners_P_I_Completed__c = true,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Charterers_Deductiblesv2__c= true, Charterers_Deductibles_Commentsv2__c='test',
                                       MOU_Completed__c=true,Charterers_Completed__c =false,
                                       Agreement_type__c = 'MOU');
                                       
        insert testOppChecklistsMOU ;
    }
    
    public static void createOppCListCharterers(){
        testOppChecklistsCharterers = new Opportunity_Checklist__c (Name = 'TestNameCharterers',
                                       Owners_P_I_Completed__c = false,Opportunity__c = strOppId ,
                                       Checklist_Completed__c = true,No_change__c = true ,
                                       Capital_Provider1__c = true,Capital_Provider_Comments__c='test',
                                       Broker__c=true,Broker_details_including_contact__c='test',
                                       Broker_Commission__c=true,Commission_Comments__c='test',
                                       Charterers_Deductiblesv2__c= true, Charterers_Deductibles_Commentsv2__c='test',
                                       MOU_Completed__c=true,Charterers_Completed__c =true,
                                       Agreement_type__c = 'Charterers');
                                       
        insert testOppChecklistsCharterers ;
    }
    //Test Method
    
     @isTest static void TestOTMNotesAtt(){
        OpportunityChecklistNameMigration aotm= new OpportunityChecklistNameMigration();
        createUser();
        createAccount();
        createOpp();
        Test.startTest();
        createOppCListOwners();   
        aotm.doOperation();
        createOppCListMOU();
        aotm.doOperation();
        createOppCListCharterers();
        aotm.doOperation();
        Test.stopTest();
    }
    */
}