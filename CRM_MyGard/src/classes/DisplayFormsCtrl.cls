public without sharing class DisplayFormsCtrl
{
    Account_Contact_Mapping__c acmJuncObj; //MYG-1829
    Public Boolean marineAccess{get;set;}
    Public Boolean PIAccess{get;set;}
    //Public transient List<Attachment> lstClaimsAtt{get;set;}
    //Public transient List<Attachment> lstUnderwritingAtt{get;set;}
    Public transient List<Attachment> lstLayupAtt{get;set;}
    //Public transient List<AttachmentWrapper> lstWrapperAtt{get;set;}
    //Public transient List<AttachmentWrapper> lstWrapperAttMarine{get;set;}
    String userType='',loggedInContactId,loggedInAccId;
    public string fileName {get;set;}
    public string downLoadedFileType {get;set;} 
    public List<ContentDistribution> contentDistributionListPI{get;set;}
    public List<ContentDistribution> contentDistributionListMarine{get;set;}
    public DisplayFormsCtrl()
    {
        marineAccess = false;
        PIAccess = false;
        //lstClaimsAtt = new List<Attachment>();
        //lstUnderwritingAtt = new List<Attachment>();
        lstLayupAtt = new List<Attachment>();
        //lstWrapperAtt = new List<AttachmentWrapper>();
        //lstWrapperAttMarine = new List<AttachmentWrapper>();
        downLoadedFileType = 'application/download';
        onLoad();
    }
    
    public void onLoad()
    {
        List<User> lstUser = new List<User>([select id,Contactid from User where id=:UserInfo.getUserId()]);
        if(lstUser!=null && lstUser.size()>0 && lstUser[0].Contactid!=null)
        {
            loggedInContactId = lstUser[0].Contactid;
            List<Contact> lstContact = new List<Contact>([select account.id,account.name, account.recordtypeid 
                                                        from Contact where id=:loggedInContactId]);
            if(lstContact!=null && lstContact.size()>0 && lstContact[0].accountId!=null)
            {
                AccountToContactMap__c acmCS = AccountToContactMap__c.getValues(loggedInContactId);
                system.debug('****************** acmCS : '+acmCS);
                if(acmCS!=null && acmCS.name!=null && acmCS.name!='')
                {
                    system.debug('****************** acmCS : '+acmCS);
                    loggedInAccId = acmCS.AccountId__c;
                }
            
                acmJuncObj = [Select PI_access__c, Marine_access__c, id from Account_Contact_Mapping__c where Contact__c =:loggedInContactId AND Account__c =:loggedInAccId];
                if(acmJuncObj != null && acmJuncObj.PI_access__c && acmJuncObj.Marine_access__c)
                {
                    marineAccess = true;
                    PIAccess = true;
                }
                else if(acmJuncObj != null && acmJuncObj.PI_access__c)
                    PIAccess = true;
                else if(acmJuncObj != null && acmJuncObj.Marine_access__c)
                    marineAccess = true;
                string recordTypeId = ((lstContact[0].account.recordtypeid)+'').substring(0,15);
                if(recordTypeId.equals(System.Label.Broker_Contact_Record_Type))
                {
                    userType = 'Broker';
                }
                else if(recordTypeId.equals(System.Label.Client_Contact_Record_Type))
                {
                    userType = 'Client';
                }
                List<Forms_List__c> listFL = new List<Forms_List__c>([select id,type__c from Forms_List__c WHERE Type__c = 'Claim forms']);   //where user_Type__c=:userType
                integer i = 0;
                List<Id> conDocIDs =  new List<Id>();
                if(listFL != null && listFL.size() >0){
                    for(ContentDocumentLink cdl : [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: listFL[0].Id]){
                        conDocIDs.add(cdl.ContentDocumentId);
                    }
                }
                List<Id> conVerIDs = new List<Id>();
                for(ContentVersion cv:[SELECT Id,createdby.name,CreatedDate FROM ContentVersion where ContentDocumentId IN: conDocIDs]){
                    conVerIDs.add(cv.Id);
                }
                contentDistributionListPI = new List<ContentDistribution>();
                contentDistributionListMarine = new List<ContentDistribution>();
                for(ContentDistribution cd : [SELECT ContentDownloadUrl,Name,Id,ContentVersion.FileType FROM ContentDistribution where ContentVersionId IN: conVerIDs]){
                    if(cd.Name.contains('General account template'))
                        contentDistributionListMarine.add(cd);
                    else
                        contentDistributionListPI.add(cd);
                }
                /*
                for(Attachment att:[select id, parentId, body, name, contenttype from Attachment where parentId=:mapFL.keyset() ORDER BY Name])
                {
                        lstClaimsAtt.add(att);
                       if(att.name == 'General account template.xlsx')
                       {
                       lstWrapperAttMarine.add(new AttachmentWrapper(att.Name.substringBefore('.'),att.Name,att));
                       }
                       else
                       {
                        lstWrapperAtt.add(new AttachmentWrapper(att.Name.substringBefore('.'),att.Name,att)); 
                       }  
                    //else if(mapFL.get(att.parentId).Type__c == 'Layup forms')
                        //lstLayupAtt.add(att);    
                    else if(mapFL.get(att.parentId).Type__c == 'Underwriting Forms')
                        lstUnderwritingAtt.add(att);
                }
                */
                //system.debug('********* lstClaimsAtt : '+lstClaimsAtt);
                //system.debug('********* lstLayupAtt: '+lstLayupAtt);
                //system.debug('********* lstUnderwritingAtt: '+lstUnderwritingAtt);
             }
         }
    }
    /*
    public class AttachmentWrapper {       
        public String attachmentName {get;set;}        
        public Attachment att {get;set;}
        public String base64Value{get;set;}
        public String originalName{get;set;}
        public AttachmentWrapper(String attachmentName,String originalName,Attachment att) {           
            this.attachmentName = attachmentName;            
            this.att = att; 
            this.base64Value = EncodingUtil.base64Encode(att.Body);
            this.originalName = originalName;  
          system.debug('File name----------------'+this.originalName);       
        }
    } 
    
     public PageReference forceDownloadPDF()
    {

                //setup a default file name
                
                system.debug('inside download@@@@@@@@@@@@@@');
                
                //we can even get more created and allow the user to pass in a filename via the URL so it can be customized further
                system.debug('inside download@@@@@@@@@@@@@@ file name->'+fileName );
                PageReference page = new PageReference('/apex/commonFileDownload?downloadedfileid='+ fileName);
                //here is were the magic happens. We have to set the content disposition as attachment.
                Apexpages.currentPage().getHeaders().put('content-disposition', 'attachemnt; filename='+fileName);
                return page;
    }               
       */
}