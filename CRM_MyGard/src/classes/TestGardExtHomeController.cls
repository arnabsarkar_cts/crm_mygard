@isTest(seealldata = false)
Public class TestGardExtHomeController{
    private static GardTestData gtdInstance;
    private static Recent_Updates__c recUpdts;
     private static Claim__c claims;
    public static String testStr = 'test';
    private static void createTestData(){
        gtdInstance = new GardTestData();
        gtdInstance.commonrecord();
        gtdInstance.GardExthomecontroller_testrecord();   
        //invoking methods of GardExtHomeController for broker and client users......
        recUpdts = new Recent_Updates__c(Update_Value__c = 'long Text');
        insert recUpdts;
        //Custom setting for claimtimebar integration
        Claim_Timebar_Webservice_Endpoint__c cs = new Claim_Timebar_Webservice_Endpoint__c(Name = 'Claim timebar endpoint',
                                                                                           Endpoint__c = 'https://gard-el-gard-test.eu.cloudhub.io/api/timebars?clients=',
                                                                                           ClientId__c = 'a7714024342840b38fbea219de4ba7eb',
                                                                                           ClientSecret__c = 'C1D611EF7Ec44c819dE7c476a2787B85'
                                                                                          );
        insert cs;
        
       Hide_Peme_Objects_in_Portal__c hidepemeobject = new Hide_Peme_Objects_in_Portal__c(name = 'Object Name',Hide_peme_objects__c='Pre-Empl');
        insert hidepemeobject;
        
    }
    
    @isTest public static void testWithBroker(){
        createTestData();
        System.runAs(GardTestData.brokerUser){
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new TestMockHttpResponseGeneratorForClaimTb());
            GardExtHomeController gardExtHomeControllerInstance= new GardExtHomeController();
            List<SelectOption> itemoptions = gardExtHomeControllerInstance.getToDoItems();
            List<SelectOption> ratingoptions = gardExtHomeControllerInstance.getRatingItems();
            //List <Case> lstcase = gardExtHomeControllerInstance.myClaims;
            List <Claim__c> lstcase = gardExtHomeControllerInstance.myClaims;
            gardExtHomeControllerInstance.strGlobalClient='a;bc';
            gardExtHomeControllerInstance.getleftImageId();
            gardExtHomeControllerInstance.getrightImageId();
            gardExtHomeControllerInstance.getuserFeedBack();
            gardExtHomeControllerInstance.submitAction();
            gardExtHomeControllerInstance.reDirection();
            System.assertEquals(gardExtHomeControllerInstance.showClient , true);
            gardExtHomeControllerInstance.isUnderWR=true;
            //clientAsset.Expiration_Date__c=date.valueof('2015-05-01');
            //update clientAsset;
            gardExtHomeControllerInstance.showClient=false; 
            gardExtHomeControllerInstance.getmyCovers();
            gardExtHomeControllerInstance.selectedTab = 'Claims';
            //gardExtHomeControllerInstance.underwrId=GardTestData.clientUser.id+'#'+GardTestData.brokerUser.id;
            // gardExtHomeControllerInstance.underwrId='test';
            gardExtHomeControllerInstance.underwrId = GardTestData.grdobj.id+'#'+'Anything here, doesn\'t matter';
            gardExtHomeControllerInstance.underwriterPopup();
            gardExtHomeControllerInstance.caseId = GardTestData.brokercase.id;
            
            Blob cryptoKey = Crypto.generateAesKey(256);
            // Blob afterblob = EncodingUtil.base64Encode(GardTestData.grdobj.id);
            //  gardExtHomeControllerInstance.claimsHandlerId=(Crypto.encryptWithManagedIV('AES256', cryptoKey, afterblob)).toString()+'#'+GardTestData.grdobj.id;
            //gardExtHomeControllerInstance.claimsHandlerId=EncodingUtil.Base64Encode(Crypto.encryptWithManagedIV('AES256', cryptoKey, Blob.valueOf(GardTestData.grdobj.id)))+'#chid';
            gardExtHomeControllerInstance.claimsHandlerId='#'+EncodingUtil.Base64Encode(Crypto.encryptWithManagedIV('AES256', cryptoKey, Blob.valueOf(GardTestData.grdobj.id)))+'#chid';
            gardExtHomeControllerInstance.showClaimsHandler(); 
            gardExtHomeControllerInstance.cancelAction();
            gardExtHomeControllerInstance.strParams='aa##bb##cc##dd##ee##10##hh##100##jj##ii';
            gardExtHomeControllerInstance.viewObject();
            gardExtHomeControllerInstance.strParams=null+'##'+null+'##'+null+'##'+null+'##'+null+'##'+null+'##'+null+'##'+1234+'##'+null+'##'+null;
            gardExtHomeControllerInstance.showClient = false;
            gardExtHomeControllerInstance.viewObject();
            gardExtHomeControllerInstance.strSelected='test;12';
            gardExtHomeControllerInstance.fetchSelectedClients();
            gardExtHomeControllerInstance.fetchClaimsOnTimebar();
            gardExtHomeControllerInstance.strSelected=null;
            Integer dateIndex=100; 
            String timebarDate='2014-01-01';
            Claim_Timebar__c object_1= new Claim_Timebar__c();
            Case objCase=new Case();
            gardExtHomeControllerInstance.downloadVcard();
            //GardExtHomeController.TimebarWrapper test=new GardExtHomeController.TimebarWrapper(dateIndex,timebarDate,object_1,objCase,object_1.Id);
            //GardExtHomeController.TimebarWrapper gehc_tb=new GardExtHomeController.TimebarWrapper(dateIndex,timebarDate,objCase);
            //gardExtHomeControllerInstance.fetchSelectedClients();
            //testb.sortCoverTable();
            gardExtHomeControllerInstance.populateRecentForClinic();
            gardExtHomeControllerInstance.setSelectedClients.clear();
            //List <Case> lstcase11 =gardExtHomeControllerInstance.myClaims;
            List <Claim__c> lstcase11 =gardExtHomeControllerInstance.myClaims;
            //gardExtHomeControllerInstance.requestguid = objCase.guid__c;
            gardExtHomeControllerInstance.caseGUIDForTimeBar = objCase.guid__c;
            gardExtHomeControllerInstance.passValTimeBar();
            gardExtHomeControllerInstance.viewRequest();
            //gardExtHomeControllerInstance.ownerId = GardTestData.salesforceLicUser.id+'#'+'Anything here, Doesn\'t matter';
            gardExtHomeControllerInstance.ownerPopup();
            gardExtHomeControllerInstance.searchServiceRequests();
           // gardExtHomeControllerInstance.ownerQueueId = GardTestData.salesforceLicUser.id+'#'+'Anything here, Doesn\'t matter';
            //gardExtHomeControllerInstance.ownerQueuePopup();
            gardExtHomeControllerInstance.viewAllNews();
            gardExtHomeControllerInstance.caseId = GardTestData.brokerClaim.id;
            gardExtHomeControllerInstance.showCaseDetails();
            Test.stopTest();
        }
        
    }
    @isTest public static void testWithClient(){
        createTestData();
        System.runAs(GardTestData.clientUser){
            Test.startTest();
            GardExtHomeController gardExtHomeControllerInstance= new GardExtHomeController();
            //List <Case> lstcase =gardExtHomeControllerInstance.myClaims; 
            List <Claim__c> lstcase =gardExtHomeControllerInstance.myClaims; 
            gardExtHomeControllerInstance.getmyCovers();
            gardExtHomeControllerInstance.feedbackComment = 'testing';
            gardExtHomeControllerInstance.reasonToday = 'checking';
            gardExtHomeControllerInstance.userContact = new Contact();
            gardExtHomeControllerInstance.userContact = GardTestData.clientContact;
            gardExtHomeControllerInstance.claimsId = 'qwertychecking';
            gardExtHomeControllerInstance.pointingId = 'qwertychecking';
            gardExtHomeControllerInstance.caseData = new Case();
            gardExtHomeControllerInstance.caseData = GardTestData.clientCase;
            gardExtHomeControllerInstance.mapClaimsHandlerDetail = new map<String,String>();
            //gardExtHomeControllerInstance.mapClaimsHandlerDetail.add('checking','testing');
            gardExtHomeControllerInstance.globalClientId = 'newTesting';
            gardExtHomeControllerInstance.lstCont = new List<Gard_Contacts__c>();
            gardExtHomeControllerInstance.lstCont.add(GardTestData.grdobj);
            gardExtHomeControllerInstance.lstContact = new List<Gard_Contacts__c>();
            gardExtHomeControllerInstance.lstContact.add(GardTestData.grdobj);
            gardExtHomeControllerInstance.lstGardContact = new List<Gard_Contacts__c>();
            gardExtHomeControllerInstance.lstGardContact.add(GardTestData.grdobj);
            //gardExtHomeControllerInstance.OwnerInfo = new Gard_Contacts__c();
            Test.stopTest();
        }
    }
    @isTest public static void testTimebarWebservice(){
        createTestData();
        System.runAs(GardTestData.clientUser){
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new TestMockHttpResponseGeneratorForClaimTb());
            GardExtHomeController gardExtHomeControllerInstance= new GardExtHomeController();
            gardExtHomeControllerInstance.strSelected='45516';
            gardExtHomeControllerInstance.selectedTab = 'Claims';
            gardExtHomeControllerInstance.fetchSelectedClients();
            gardExtHomeControllerInstance.fetchClaimsOnTimebar();
            gardExtHomeControllerInstance.timeBarWrp[0].claimId = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].clientId = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].eventName = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].reminderDate = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].sDescription = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].eventType = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].eventGuid = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].subscribers = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].sstatus = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].systemsource = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].creator = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].resolution = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].resolvedBy = testStr;
            gardExtHomeControllerInstance.timeBarWrp[0].resolveDate = testStr;
            Test.stopTest();
        }
    }
}