global class CompanyChangeApprovalCtrl
{
    public Account account{get;set;}
    
    Webservice static String approveRequest(Id accountId)
    {
        Account account = new Account();
        account = [select id, name,
                          Billing_Street_Address_Line_1__c, Billing_Street_Address_Line_2__c, Billing_Street_Address_Line_3__c,
                          Billing_Street_Address_Line_4__c, BillingCity__c, BillingPostalCode__c, BillingState__c, BillingCountry__c,
                          Shipping_Street_Address_Line_1__c, Shipping_Street_Address_Line_2__c, Shipping_Street_Address_Line_3__c, 
                          Shipping_Street_Address_Line_4__c, ShippingCity__c, ShippingPostalCode__c, ShippingState__c, ShippingCountry__c,
                          BillingState_1__c,Corporate_email__c, BillingCity_1__c, BillingCountry_1__c, BillingPostalCode_1__c, 
                          BillingStreet_1__c, BillingStreet_2__c, BillingStreet_3__c, BillingStreet_4__c, Phone_1__c,
                          //BillingCity, BillingCountry, BillingPostalCode, BillingStreet,
                          //ShippingState,ShippingCity, ShippingCountry, ShippingPostalCode, BillingState, 
                          ShippingStreet, ShippingState_1__c, ShippingStreet_2__c, ShippingStreet_3__c, ShippingStreet_4__c,
                          ShippingCity_1__c, ShippingCountry_1__c,Website, Phone, Type, Fax_1__c, Email_1__c,Company_Website_1__c,
                          ShippingPostalCode_1__c, ShippingStreet_1__c,Fax
                          from Account where id =: accountId];
        if(account!=null)
        {          
            account.Request_Status__c = 'Approved';
            account.Billing_Street_Address_Line_1__c = (account.BillingStreet_1__c == ''|| account.BillingStreet_1__c == null ? account.Billing_Street_Address_Line_1__c : account.BillingStreet_1__c);
            account.Billing_Street_Address_Line_2__c = (account.BillingStreet_2__c == ''|| account.BillingStreet_2__c == null ? account.Billing_Street_Address_Line_2__c : account.BillingStreet_2__c);
            account.Billing_Street_Address_Line_3__c = (account.BillingStreet_3__c == ''|| account.BillingStreet_3__c == null ? account.Billing_Street_Address_Line_3__c : account.BillingStreet_3__c);
            account.Billing_Street_Address_Line_4__c = (account.BillingStreet_4__c == ''|| account.BillingStreet_4__c == null ? account.Billing_Street_Address_Line_4__c : account.BillingStreet_4__c);
            account.BillingCity__c = (account.BillingCity_1__c == ''|| account.BillingCity_1__c == null ? account.BillingCity__c : account.BillingCity_1__c);
            account.BillingState__c = (account.BillingState_1__c == ''|| account.BillingState_1__c == null ? account.BillingState__c : account.BillingState_1__c);
            account.BillingCountry__c = (account.BillingCountry_1__c == ''|| account.BillingCountry_1__c == null ? account.BillingCountry__c : account.BillingCountry_1__c);
            account.BillingPostalCode__c = (account.BillingPostalCode_1__c == ''|| account.BillingPostalCode_1__c == null ? account.BillingPostalCode__c : account.BillingPostalCode_1__c);
            account.Shipping_Street_Address_Line_1__c = (account.ShippingStreet_1__c == ''|| account.ShippingStreet_1__c == null ? account.Shipping_Street_Address_Line_1__c : account.ShippingStreet_1__c);
            account.Shipping_Street_Address_Line_2__c = (account.ShippingStreet_2__c == ''|| account.ShippingStreet_2__c == null ? account.Shipping_Street_Address_Line_2__c : account.ShippingStreet_2__c);
            account.Shipping_Street_Address_Line_3__c = (account.ShippingStreet_3__c == ''|| account.ShippingStreet_3__c == null ? account.Shipping_Street_Address_Line_3__c : account.ShippingStreet_3__c);
            account.Shipping_Street_Address_Line_4__c = (account.ShippingStreet_4__c == ''|| account.ShippingStreet_4__c == null ? account.Shipping_Street_Address_Line_4__c : account.ShippingStreet_4__c);
            account.ShippingCity__c = (account.ShippingCity_1__c == ''|| account.ShippingCity_1__c == null ? account.ShippingCity__c : account.ShippingCity_1__c);
            account.ShippingState__c = (account.ShippingState_1__c == ''|| account.ShippingState_1__c == null ? account.ShippingState__c : account.ShippingState_1__c);
            account.ShippingCountry__c = (account.ShippingCountry_1__c == ''|| account.ShippingCountry_1__c == null ? account.ShippingCountry__c : account.ShippingCountry_1__c);
            account.ShippingPostalCode__c = (account.ShippingPostalCode_1__c == ''|| account.ShippingPostalCode_1__c == null ? account.ShippingPostalCode__c : account.ShippingPostalCode_1__c);
            account.Corporate_email__c = (account.Email_1__c == ''|| account.Email_1__c == null ? account.Corporate_email__c : account.Email_1__c);
            account.Website = (account.Company_Website_1__c == ''|| account.Company_Website_1__c == null ? account.Website : account.Company_Website_1__c);
            system.debug('****** account.Website : '+account.Website);
            system.debug('****** account.Corporate_email__c: '+account.Corporate_email__c);
            account.Phone = (account.Phone_1__c == ''|| account.Phone_1__c == null ? account.Phone : account.Phone_1__c);
            account.Fax = (account.Fax_1__c == ''|| account.Fax_1__c == null ? account.Fax : account.Fax_1__c);
            account.BillingStreet_1__c = '';
            account.BillingStreet_2__c = '';
            account.BillingStreet_3__c = '';
            account.BillingStreet_4__c = '';
            account.BillingCity_1__c = '';
            account.BillingState_1__c = '';
            account.BillingCountry_1__c = '';
            account.BillingPostalCode_1__c = '';
            account.ShippingStreet_1__c = '';
            account.ShippingStreet_2__c = '';
            account.ShippingStreet_3__c = '';
            account.ShippingStreet_4__c = '';
            account.ShippingCity_1__c = '';
            account.ShippingState_1__c = '';
            account.ShippingCountry_1__c = '';
            account.ShippingPostalCode_1__c = '';
            account.Phone_1__c = '';
            account.Company_Website_1__c = '';
            account.Email_1__c = ''; 
            account.Fax_1__c = '';
            
            try
            {
                update account;
                return 'SUCCESS';
            }
            catch(Exception e)
            {
                system.debug(e.getMessage());
            }
        }
        return 'FAILURE';
    }
    
    Webservice static String rejectRequest(Id accountId, String rejectReason)
    {
        Account account = new Account(id=accountId);
        account.Reject_Reason__c = rejectReason;
        account.Request_Status__c = 'Rejected';
        account.BillingStreet_1__c = '';
        account.BillingStreet_2__c = '';
        account.BillingStreet_3__c = '';
        account.BillingStreet_4__c = '';
        account.BillingCity_1__c = '';
        account.BillingState_1__c = '';
        account.BillingCountry_1__c = '';
        account.BillingPostalCode_1__c = '';
        account.ShippingStreet_1__c = '';
        account.ShippingStreet_2__c = '';
        account.ShippingStreet_3__c = '';
        account.ShippingStreet_4__c = '';
        account.ShippingCity_1__c = '';
        account.ShippingState_1__c = '';
        account.ShippingCountry_1__c = '';
        account.ShippingPostalCode_1__c = '';
        account.Phone_1__c = '';
        account.Company_Website_1__c = '';
        account.Email_1__c = '';
        account.Fax_1__c = '';
        try
        {
            update account;
            return 'SUCCESS';
        }
        catch(Exception e)
        {
            system.debug(e.getMessage());
        }    
        return 'FAILURE';
    }
    
}