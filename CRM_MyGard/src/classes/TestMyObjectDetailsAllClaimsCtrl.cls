@isTest
Class TestMyObjectDetailsAllClaimsCtrl
{     
    public static testmethod void cover()
    { 
      set<Id> setObjectIds = new set<Id>();
      List<String> covers = new List<String>();
      AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
      insert numberofusers;
      GardTestData test_rec = new GardTestData();
      test_rec.commonRecord();
      //test_rec.MyObjectDetailsAllClaimsCtrl_AllCoversctrl_testrecord(); 
      GardTestData.test_object_1st.guid__c = '8ce8ac89-a6fd-1836-9e07';
      update GardTestData.test_object_1st;
      system.assertEquals(GardTestData.test_object_1st.guid__c , '8ce8ac89-a6fd-1836-9e07' );
      test_rec.customsettings_rec();
      /*
      BlueCardTeam__c testbluecardteam = new BlueCardTeam__c(
                                                                 Name = 'BlueCardTeam',
                                                                 Value__c = 'nils.petter.nilsen@testgard.no'
                                                             );
      insert testbluecardteam;
      */
      setObjectIds.add(GardTestData.test_object_1st.id);
        covers.add(GardTestData.brokerAsset_1st.id);
        
      System.runAs(GardTestData.brokerUser)
        {
            test.startTest();
            //test_rec.obj_1st.guid__c = '8ce8ac89-a6fd-1836-9e07';
            //update test_rec.obj_1st;
            ApexPages.StandardController scontroller = new ApexPages.StandardController(GardTestData.brokerAcc);
            Pagereference pge = page.MyObjectDetailsAllClaims;
            pge.getparameters().put('objid',GardTestData.test_object_1st.guid__c);
            test.setcurrentpage(pge);
            MyObjectDetailsAllClaimsCtrl MyObject=new MyObjectDetailsAllClaimsCtrl(scontroller);
            MyObjectDetailsAllClaimsCtrl MyObjects=new MyObjectDetailsAllClaimsCtrl();
            MyObjects.caseId = GardTestData.brokerClaim.id;
            MyObject.getClaims();
            MyObjects.showCaseDetails();
            system.debug('**************loggedInContactId'+MyObjects.loggedInContactId);
            system.debug('**************loggedInAccountId'+MyObjects.loggedInAccountId);
            MyObject.jointSortingParam = 'ASC##Claim_Type__c';
            MyObject.covers = covers;
            MyObject.setObjectIds = setObjectIds;
            MyObject.ViewData();
            MyObject.setSortDirection('Claim_Type__c');
            MyObject.getSortDirection();
            MyObject.setpageNumber();
            MyObject.hasNext=true;
            MyObject.hasPrevious=true;
            MyObject.pageNumber=100;
            //MyObject.showCaseDetails();
            MyObject.printData();
            MyObjectDetailsAllClaimsCtrl.getUserName();
            MyObject.navigate();
            MyObject.first();
            MyObject.next();
            MyObject.last();
            MyObject.previous();
            MyObject.exportToExcel();
            MyObject.closeconfirmPopUp();
            System.assertEquals(MyObject.showconfirmPopUp , false );
            MyObject.displayEditPopUp();
            MyObject.obj = new object__c();
            MyObject.obj.id = GardTestData.test_object_1st.id;
            //Case singleCase = [SELECT ID FROM CASE WHERE Claim_Reference_Number__c='kol23232' LIMIT 1];
            
            MyObject.caseTestId = GardTestData.brokerClaim.Id;//singleCase.ID;
            MyObject.sendMailforEditObject(); 
            MyObject.createFavourites();
            MyObject.deleteFavourites(); 
        }
        test.stopTest();
        /*System.runAs(test_rec.client_User_1st)
        {
            ApexPages.StandardController scontroller = new ApexPages.StandardController(test_rec.client_Acc_1st);
            //test_rec.obj_1st.guid__c = '8ce8ac89-a6fd-1836-9e07';
            //update test_rec.obj_1st;
            Pagereference pge_1 = page.MyObjectDetailsAllClaims;
            pge_1.getparameters().put('objid',test_rec.obj_1st.guid__c);
            test.setcurrentpage(pge_1);
            MyObjectDetailsAllClaimsCtrl MyObject=new MyObjectDetailsAllClaimsCtrl(scontroller);
            MyObjectDetailsAllClaimsCtrl MyObjects=new MyObjectDetailsAllClaimsCtrl();
            MyObject.jointSortingParam = 'ASC##Claim_Type__c';
         
        }      */        
    }
}