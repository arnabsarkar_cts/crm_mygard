@isTest
public class TestClientActivityCtrl
{
    
    static testMethod void cover()
    {
         AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        insert numberofusers;
        GardTestData test_record = new GardTestData();
        test_record.commonRecord();
        
        
       
        
        system.runAs(GardTestData.brokerUser)
        {
            
            system.debug('user id:'+GardTestData.brokerUser.Id);
            Client_Activity__c clientActBroker = new Client_Activity__c(
                                                                        //Name = 'brokerClientAct',
                                                                        Additional_Details__c = 'activity for broker',
                                                                        Client_account__c = GardTestData.brokerAcc.id,
                                                                        Cover_link__c = 'test/cover',
                                                                        Cover_Name__c = 'testCover',
                                                                        Event_type__c = 'Request MLC Certificate',
                                                                        Object_link__c = 'https://test/object',
                                                                        Object_Name__c = 'testObject',
                                                                        User__c = GardTestData.brokerUser.Id
                                                                    );
            insert clientActBroker;
            Test.startTest();
            //System.currentPageReference().getParameters().put('favId',String.valueOf(GardTestData.test_fav.ID));
            ClientActivityCtrl testBroker = new ClientActivityCtrl();
            ClientActivityCtrl.ClientActivityWrapper testwrap = new ClientActivityCtrl.ClientActivityWrapper('testObject','testCover','Request MLC Certificate',GardTestData.brokerUser.Name,'test details','addDetails',system.today());
            testBroker.getClientOptions();
            //testBroker.selectedFromDate = '02.05.2015';
            //testBroker.selectedToDate = '02.04.2015';
            testBroker.buildQuery();
            testBroker.viewData();
            testBroker.populateFilter();
            testBroker.getClientOptions();
            testBroker.getObjOptions();
            testBroker.getEventTypeOptions();
            testBroker.getSortDirection();
            testBroker.setSortDirection('ASC');
            DateTime dtTest = testBroker.strToDateTime('21.11.2016');
            testBroker.searchActivities();
            testBroker.getActivities();
            testBroker.setpageNumber();
            testBroker.hasNext = true;
            testBroker.hasPrevious  = true;
            testBroker.pageNumber = 1;
            testBroker.navigate();
            testBroker.last();
            testBroker.previous();
            testBroker.fetchSelectedClients();
            testBroker.print();
            testBroker.exportToExcelForClientActivity();
            testBroker.first();
            testBroker.navigate();
            testBroker.next();
            testBroker.navigate();
            /*testBroker.newFilterName = 'TestFav';
            testBroker.createFavourites();
            testBroker.fetchFavourites();
            testBroker.deleteFavourites();*/
            ClientActivityCtrl.getGenerateTimeStamp();
            ClientActivityCtrl.getGenerateTime();
            ClientActivityCtrl.getUserName();
            testBroker.clearOptions();
            System.currentPageReference().getParameters().put('favId',String.valueOf(GardTestData.test_fav.ID));
            ClientActivityCtrl testBroker1 = new ClientActivityCtrl();
            testBroker1.newFilterName = 'TestFav';
            testBroker1.createFavourites();
            testBroker1.fetchFavourites();
            testBroker1.deleteFavourites();
        }
        system.runAs(GardTestData.clientUser)
        {
             
        Client_Activity__c clientActClient = new Client_Activity__c(
                                                                        //Name = 'clientClientAct',
                                                                        Additional_Details__c = 'activity for client',
                                                                        Client_account__c = GardTestData.clientAcc.id,
                                                                        Cover_link__c = 'test/cover',
                                                                        Cover_Name__c = 'testCover',
                                                                        Event_type__c = 'Request MLC Certificate',
                                                                        Object_link__c = 'https://test/object',
                                                                        Object_Name__c = 'testObject',
                                                                        User__c = GardTestData.clientUser.Id
                                                                    );
        insert clientActClient;
            System.currentPageReference().getParameters().put('favId',null);
            ClientActivityCtrl testClient = new ClientActivityCtrl();
            ClientActivityCtrl.ClientActivityWrapper testwrap = new ClientActivityCtrl.ClientActivityWrapper('testObject','testCover','Request MLC Certificate',GardTestData.brokerUser.Name,'test details','addDetails',system.today());
            testClient.buildQuery();
            testClient.viewData();
            testClient.populateFilter();
            testClient.getClientOptions();
            testClient.getObjOptions();
            testClient.getEventTypeOptions();
            testClient.getSortDirection();
            testClient.setSortDirection('ASC');
            DateTime dtTest = testClient.strToDateTime('21.11.2016');
            testClient.searchActivities();
            testClient.getActivities();
            testClient.setpageNumber();
            testClient.hasNext = true;
            testClient.hasPrevious  = true;
            testClient.pageNumber = 1;
            testClient.navigate();
            testClient.last();
            testClient.previous();
            testClient.fetchSelectedClients();
            testClient.print();
            testClient.exportToExcelForClientActivity();
            ClientActivityCtrl.getGenerateTimeStamp();
            ClientActivityCtrl.getGenerateTime();
            ClientActivityCtrl.getUserName();
            testClient.clearOptions();
            Test.stopTest();
        }
    }
}