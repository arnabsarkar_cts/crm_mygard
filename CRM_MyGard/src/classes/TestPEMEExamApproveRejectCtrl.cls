@isTest(seeAllData=true)
public class TestPEMEExamApproveRejectCtrl
{
    Public static List<Account> AccountList=new List<Account>();
    Public static List<Contact> ContactList=new List<Contact>();
    Public static List<User> UserList=new List<User>();
    Public static string partnerLicenseId = [SELECT Id FROM profile WHERE Name='Partner Community Login User Custom' limit 1].id;
    Public static string salesforceLicenseId = [SELECT Id FROM profile WHERE Profile.UserLicense.Name='Salesforce' limit 1].id;
    public static Gard_Contacts__c gc;
    public static PEME_Invoice__c pi=new PEME_Invoice__c ();
    public static List<PEME_Exam_Detail__c> PExamDetailList = new List<PEME_Exam_Detail__c>();
    Public static List<Object__c> ObjectList=new List<Object__c>();
    public static User adminUser;
    public static string AdminLicense = [SELECT Id FROM profile WHERE Name= 'System Administrator' limit 1].id;
    public static string bilCntryStr =  'United Kingdom' ;
    public static string bilCntryStr2 =  'SWE'  ;
    public static string typeClStr = 'client';
    public static string typeBrStr =  'Broker';
    public static string typeSubBrStr = 'Broker - Reinsurance Broker'; //sfedit
    public static string piStr =  'P&I';
    public static string marineStr =  'Marine';
    public static string postlCdStr =  'BS1 1AD';
    public static string otherPhnStr =  '1112223356' ;
    public static string mblPhnStr =   '1112223334';
    public static string mailngStateStr =  'London' ;
    public static string mailStr =  'test321@gmail.com';
    public static string aliasStr =  'alias_1';
    public static string lNameStr =  'tetst_006';
    public static string standtStr = 'standt';
    public static string stdUsrMailStr = 'standarduser@testorg.com';
    public static string utfStr = 'UTF-8';
    public static string test13Str = 'test13';
    public static string testingStr = 'Testing';
    public static string enUsrStr = 'en_US';
    public static string tymZnStr =  'America/Los_Angeles' ;
    public static string objSubTypStr =  'Accommodation Ship' ;
    public static string csStr =  'CS 1' ;
    public static string flagStr =  'Albania';
    public static string rebuiltStr =  '2014-01-01';
    public static string imoStr = '4355555';
    public static string yrBuiltStr =  '2013-01-01';
    public static Account brokerAcc, clientAcc;
    public static Contact clientContact, brokerContact; 
    public static Gard_Contacts__c grdobj;
    public static User salesforceLicUser;
    Static testMethod void cover()
    {
        //GardTestData testRec = new GardTestData();
        //testRec.customsettings_rec();
        //testRec.commonRecord();
        Market_Area__c Markt = new Market_Area__c(Market_Area_Code__c = 'ab120');
        insert Markt;
        //AdminUsers__c numberofusers = new AdminUsers__c(name = 'Number of users', value__c=3);
        //insert numberofusers;
        List<Valid_Role_Combination__c> vrcList = new List<Valid_Role_Combination__c>();
        Valid_Role_Combination__c vrcClient = new Valid_Role_Combination__c(Role__c = 'Client',Sub_Role__c='');
        vrcList.add(vrcClient);
        Valid_Role_Combination__c vrcBroker = new Valid_Role_Combination__c(Role__c = 'Broker',Sub_Role__c='Broker - Reinsurance Broker');
        vrcList.add(vrcBroker);
        // insert vrcList;
        Gard_Team__c gt = new Gard_Team__c();
        gt.Active__c = true;
        gt.Office__c = 'test';
        gt.Name = 'GTeam';
        gt.Region_code__c = 'TEST';
        insert gt;
        Country__c country = new Country__c(Country_Name__c='Israel',Country_ISO_Code__c='ISRA',Gard_Team__c=gt.Id,Claims_Support_Team__c=gt.Id);
        insert country;
        grdobj  =    new  Gard_Contacts__c(
            FirstName__c = 'Testreqchng',
            LastName__c = 'Baann',
            Email__c = 'WR_gards.12@Test.com',
            MobilePhone__c = '548645', 
            Nick_Name__c  = 'NilsPeter', 
            Office_city__c = 'Arendal', 
            Phone__c = '5454454',
            Portal_Image__c ='<img alt="User-added image" src="https://c.cs8.content.force.com/servlet/rtaImage?eid=a1hL0000000kSyZ&amp;feoid=00NL0000003OySC&amp;refid=0EML00000008Zjs"></img>', 
            Title__c = 'testStr'
        );
        insert grdobj ;
        salesforceLicUser = new User(
            Alias = standtStr , 
            profileId = salesforceLicenseId ,
            Email=stdUsrMailStr ,
            EmailEncodingKey=utfStr ,
            CommunityNickname = test13Str ,
            LastName=testingStr ,
            LanguageLocaleKey= enUsrStr ,
            LocaleSidKey= enUsrStr , 
            //contactID = brokercontact.id, 
            TimeZoneSidKey= tymZnStr ,
            UserName='mygardtest008@testorg.com.mygard',
            contactId__c = grdobj.id,
            City= 'Arendal'
            
        );
        insert salesforceLicUser;
        adminUser = new User(Alias = 'admin',
                             profileId = AdminLicense ,
                             Email= 'testMail@test.com',
                             EmailEncodingKey= 'UTF-8',
                             CommunityNickname = 'Nick',
                             LastName= 'testName',
                             LanguageLocaleKey= 'en_US',
                             LocaleSidKey= 'en_US',  
                             TimeZoneSidKey=  'America/Los_Angeles',
                             contactId__c= grdobj.id,
                             UserName='test008@testorg.com.mygard',
                             City= 'Arendal'
                            );
        insert adminUser ;
        brokerAcc = new Account(   Name='testre',
                                BillingCity = 'Southampton',
                                BillingCountry =  bilCntryStr ,
                                BillingPostalCode = 'BS2 AD!',
                                BillingState = 'Avon LAng' ,
                                BillingStreet = '1 Mangrove Road',
                                recordTypeId=System.Label.Broker_Contact_Record_Type,
                                Site = '_www.tcs.se',
                                Type =  typeBrStr ,
                                company_Role__c =  typeBrStr ,
                                Sub_Roles__c = typeSubBrStr, //sfedit
                                //Company_ID__c = '64143',
                                guid__c = '8ce8ad89-a6ed-1836-9e17',
                                Market_Area__c = Markt.id,
                                Product_Area_UWR_2__c= piStr ,
                                Product_Area_UWR_4__c= piStr ,
                                Product_Area_UWR_3__c= marineStr ,
                                Claim_handler_Marine_2_lk__c = salesforceLicUser.id ,
                                Claim_handler_Cargo_Liquid__c = salesforceLicUser.id ,
                                Role_Broker__c = true,
                                Claim_handler_Charterers__c = salesforceLicUser.id ,
                                Claim_handler_Defence__c = salesforceLicUser.id ,
                                Claim_handler_Energy__c = salesforceLicUser.id ,
                                Claim_handler_Energy_2_lk__c= salesforceLicUser.id ,
                                Claims_handler_Builders_Risk_2_lk__c= salesforceLicUser.id ,
                                Claim_handler_Charterers_2_lk__c = salesforceLicUser.id ,
                                Claim_handler_Cargo_Liquid_2_lk__c = salesforceLicUser.id ,
                                Claims_handler_Builders_Risk__c = salesforceLicUser.id ,
                                Company_Role_Text__c = 'Broker',
                                //Claim_handler_Charterers__c = salesforceLicUser.id ,
                                Area_Manager__c = salesforceLicUser.id ,
                                X2nd_UWR__c=salesforceLicUser.id,
                                X3rd_UWR__c=salesforceLicUser.id,
                                Underwriter_4__c=salesforceLicUser.id,
                                Underwriter_main_contact__c=salesforceLicUser.id,
                                UW_Assistant_1__c=salesforceLicUser.id,
                                U_W_Assistant_2__c=salesforceLicUser.id,
                                Claim_handler_Crew__c = salesforceLicUser.id,
                                Enable_share_data__c = true,
                                Company_Status__c='Active',
                                GIC_Office_ID__c = null,
                                Key_Claims_Contact__c = salesforceLicUser.id,
                                Claim_Adjuster_Marine_lk__c = salesforceLicUser.id,
                                Claim_handler_Cargo_Dry__c = salesforceLicUser.id,
                                Claim_handler_CEP__c = salesforceLicUser.id,
                                Claim_handler_Marine__c = salesforceLicUser.id,
                                Claim_handler_Cargo_Dry_2_lk__c = salesforceLicUser.id,
                                Claim_handler_CEP_2_lk__c = salesforceLicUser.id,
                                Claim_handler_Crew_2_lk__c =  salesforceLicUser.id,
                                Claim_handler_Defence_2_lk__c = salesforceLicUser.id,
                                Accounting_P_I__c = salesforceLicUser.id,
                                Accounting__c = salesforceLicUser.id,
                                OwnerId = salesforceLicUser.id,
                                Confirm_not_on_sanction_lists__c = true,
                                License_description__c  = 'some desc',
                                Description = 'some desc',
                                Licensed__c = 'Pending',
                                Active__c = true,
                                country__c = country.Id
                                //Owner = salesforceLicUser
                                //Account_Sync_Status__c = 'Synchronised'
                                //Ownerid=salesforceLicUser.id
                                
                               );
        insert brokerAcc;        
        
        clientAcc = new Account(  Name = 'Testuu', 
                                Site = '_www.test_1.s', 
                                Type =  typeClStr , 
                                BillingStreet = 'Gatanfol 1',
                                BillingCity = 'phuket', 
                                BillingCountry =  bilCntryStr2  , 
                                BillingPostalCode =  postlCdStr ,
                                company_Role__c =  typeClStr ,
                                //Company_ID__c = '64144',
                                recordTypeId=System.Label.Client_Contact_Record_Type,
                                Market_Area__c = Markt.id,
                                guid__c = '8ce8ad66-a6ec-1834-9e21',
                                Product_Area_UWR_2__c= piStr ,
                                Product_Area_UWR_4__c= piStr ,
                                Product_Area_UWR_3__c= marineStr ,
                                Claim_handler_Marine_2_lk__c = salesforceLicUser.id ,
                                X2nd_UWR__c = salesforceLicUser.id ,
                                X3rd_UWR__c = salesforceLicUser.id ,
                                Role_Client__c = true,
                                Underwriter_4__c = salesforceLicUser.id ,
                                Claim_handler_Cargo_Liquid__c = salesforceLicUser.id ,
                                Claim_handler_Charterers__c = salesforceLicUser.id ,
                                Claim_handler_Defence__c = salesforceLicUser.id ,
                                Claim_handler_Energy__c = salesforceLicUser.id ,
                                Claim_handler_Energy_2_lk__c= salesforceLicUser.id ,
                                Claims_handler_Builders_Risk_2_lk__c= salesforceLicUser.id ,
                                Claim_handler_Charterers_2_lk__c = salesforceLicUser.id ,
                                Claim_handler_Cargo_Liquid_2_lk__c = salesforceLicUser.id ,
                                Claims_handler_Builders_Risk__c = salesforceLicUser.id ,
                                //Claim_handler_Charterers__c = salesforceLicUser.id ,
                                Area_Manager__c = salesforceLicUser.id,
                                //X2nd_UWR__c=salesforceLicUser.id,
                                //X3rd_UWR__c=salesforceLicUser.id,
                                //Underwriter_4__c=salesforceLicUser.id,
                                Underwriter_main_contact__c=salesforceLicUser.id,
                                UW_Assistant_1__c=salesforceLicUser.id,
                                U_W_Assistant_2__c=salesforceLicUser.id    ,
                                Claim_handler_Crew__c = salesforceLicUser.id,
                                Company_Status__c='Active',
                                P_I_Member_Flag__c = true,
                                Key_Claims_Contact__c = salesforceLicUser.id,
                                Claim_Adjuster_Marine_lk__c = salesforceLicUser.id,
                                Claim_handler_Cargo_Dry__c = salesforceLicUser.id,
                                Claim_handler_CEP__c = salesforceLicUser.id,
                                Claim_handler_Marine__c = salesforceLicUser.id,
                                Claim_handler_Cargo_Dry_2_lk__c = salesforceLicUser.id,
                                Claim_handler_CEP_2_lk__c = salesforceLicUser.id,
                                Claim_handler_Crew_2_lk__c =  salesforceLicUser.id,
                                Claim_handler_Defence_2_lk__c = salesforceLicUser.id,
                                Accounting_P_I__c = salesforceLicUser.id,
                                Accounting__c = salesforceLicUser.id,
                                PEME_Enrollment_Status__c = 'Enrolled',
                                country__c = country.Id
                                
                               );                                    
        insert clientAcc;
        brokerContact = new Contact( 
            FirstName='Yoo',
            LastName='Baooo',
            MailingCity = 'Kingsville',
            OtherPhone =  otherPhnStr ,
            mobilephone =  mblPhnStr ,
            MailingCountry =  bilCntryStr ,
            MailingPostalCode = 'SE3 1AD',
            MailingState =  mailngStateStr ,
            MailingStreet = '1 Eastwood Road',
            AccountId = brokerAcc.Id,
            Email = 'test32@gmail.com',
            Synchronisation_Status__c ='Synchronised'
        );
        insert brokerContact;
        
        clientContact= new Contact(   FirstName='potatoulli',
                                   LastName='Ullipotato',
                                   MailingCity =  mailngStateStr ,
                                   OtherPhone =  otherPhnStr ,
                                   mobilephone =  mblPhnStr ,
                                   MailingCountry =  bilCntryStr ,
                                   MailingPostalCode = 'SE1 1AE',
                                   MailingState =  mailngStateStr ,
                                   MailingStreet = '4 London Road',
                                   AccountId = clientAcc.Id,
                                   Email =  mailStr ,
                                   Primary_Contact__c = true,
                                   Synchronisation_Status__c  ='Synchronised'
                                  );
        //insert clientContact;
        
        Object__c test_object_1st = New Object__c(     Dead_Weight__c= 20,
                                                  Object_Unique_ID__c = '1234lkolko',
                                                  Name='hennessey',
                                                  Object_Type__c='Tanker',
                                                  Object_Sub_Type__c= objSubTypStr ,
                                                  No_of_passenger__c=100,
                                                  No_of_crew__c=70,
                                                  Gross_tonnage__c=1000,
                                                  Length__c=20,
                                                  Width__c=20,
                                                  Depth__c=30,
                                                  Port__c='Alabama',
                                                  Signal_Letters_Call_sign__c='hellooo',
                                                  Classification_society__c= csStr  ,
                                                  Flag__c= flagStr ,
                                                  Rebuilt__c= rebuiltStr ,
                                                  Imo_Lloyds_No__c= imoStr ,
                                                  // Dummy_Object_flag__c = 0,
                                                  Year_built__c= yrBuiltStr ,
                                                  Dummy_Object_flag__c=0);
        insert test_object_1st;
        PEME_Enrollment_Form__c pefBroker=new PEME_Enrollment_Form__c();
        pefBroker.ClientName__c= brokerAcc.ID;
        pefBroker.Enrollment_Status__c='Draft';
        pefBroker.Submitter__c= brokerContact.Id;
        insert pefBroker;
        
        PEME_Manning_Agent__c pmgClient=new PEME_Manning_Agent__c();
        pmgClient.Client__c=clientAcc.ID;
        pmgClient.PEME_Enrollment_Form__c=pefBroker.ID;
        pmgClient.Company_Name__c='Testc Company';
        pmgClient.Client__c = clientAcc.id;
        pmgClient.Contact_Point__c=brokerContact.id;//clientContact.Id;
        pmgClient.Status__c='Approved';
        pmgClient.Requested_Address__c='testcaddr1';
        pmgClient.Requested_Address_Line_2__c='testcaddr2';
        pmgClient.Requested_Address_Line_3__c='testcaddr3';
        pmgClient.Requested_Address_Line_4__c='testcaddr4';
        pmgClient.Requested_City__c='testcCity1';
        pmgClient.Requested_Contact_Person__c='testc person1';
        pmgClient.Requested_Country__c='testccountry1';
        pmgClient.Requested_Email__c='testc1@mail.com';
        pmgClient.Requested_Phone__c='1154228855';
        pmgClient.Requested_State__c='testcstate1';
        pmgClient.Requested_Zip_Code__c='testczip';
        
        insert pmgClient;
        
        PEME_Debit_note_detail__c pefDebitClient = new PEME_Debit_note_detail__c();
        pefDebitClient.PEME_Enrollment_Form__c = pefBroker.id;
        pefDebitClient.Company_Name__c = clientAcc.id;
        pefDebitClient.Status__c = 'Approved';
        insert pefDebitClient;
        
        /* pefBroker.Enrollment_Status__c='Under Approval';
update pefBroker;

pefBroker.Enrollment_Status__c='Enrolled';
update pefBroker;*/
        
        pi.Client__c= clientAcc.id;
        pi.Clinic_Invoice_Number__c='qwerty123';
        pi.Clinic__c= clientAcc.id;
        pi.Crew_Examined__c=11223;
        pi.Invoice_Date__c=Date.valueOf('2016-01-05');
        pi.PEME_reference_No__c='aasd23';
        pi.Status__c='Under Approval';
        pi.ObjectList__c='qwerty for test purpose';
        pi.Total_Amount__c=112230;
        pi.Vessels__c= test_object_1st.id;
        pi.PEME_Enrollment_Detail__c = pefBroker.id;
        //pi.GUID__c='guid1';
        pi.Period_Covered_From__c=Date.valueOf('2016-01-05');
        pi.Period_Covered_To__c =Date.valueOf('2016-01-28');
        insert pi;
        pi.guid__c = '1ghsdjhasgdjhasg';
        update pi;
        //  PExamDetail=new PEME_Exam_Detail__c ();
        PEME_Exam_Detail__c PExamDetail=new PEME_Exam_Detail__c(); 
        PExamDetail.Age__c=30;
        PExamDetail.Amount__c=12000;
        PExamDetail.Date__c=Date.valueOf('2016-01-10');
        PExamDetail.Examination__c='Demo Exam';
        PExamDetail.Examinee__c='Demo employee';
        PExamDetail.Invoice__c= pi.id;
        PExamDetail.Object_Related__c= test_object_1st.id;
        PExamDetail.Rank__c='Five';
        PExamDetail.Status__c='Under Approval';
        PExamDetailList.add(PExamDetail);
        insert PExamDetailList;
        
        Test.startTest();
        System.runAs(adminUser)
        {
            Test.setCurrentPage(Page.PEMEExamApproveReject);
            //System.currentPageReference().getParameters().put('id' ,'1ghsdjhasgdjhasg');
            System.currentPageReference().getParameters().put('id' , pi.Id);
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(PExamDetailList);
            sc.setSelected(PExamDetailList);
            PEMEExamApproveRejectCtrl PEMEExamApproveRejectCntlAdmin=new PEMEExamApproveRejectCtrl(sc);
            Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitrequest();
            app.setObjectId(PExamDetail.Id);
            //Approval.ProcessResult result = Approval.process(app);
            PEMEExamApproveRejectCntlAdmin.Examdetail();
            
            //Test.stopTest();
            // PEMEExamApproveRejectCntlAdmin.performApprovals('Approve');
            // PEMEExamApproveRejectCntlAdmin.performApprovals('Reject');
            // PEMEExamApproveRejectCntl.fetchLatestApprovals(PExamDetail);
            try{
                PEMEExamApproveRejectCntlAdmin.approve();
                PEMEExamApproveRejectCntlAdmin.reject();
                PEMEExamApproveRejectCntlAdmin.approvalComments='Form not filled';
                PEMEExamApproveRejectCntlAdmin.reject();
                PEMEExamApproveRejectCntlAdmin.cancel();
            }
            catch(Exception e){System.debug(e.getMessage());}
            PEMEExamApproveRejectCntlAdmin.cancel();
            PEMEExamApproveRejectCntlAdmin.redirect();   
            PEMEExamApproveRejectCntlAdmin.lstExaminations = PExamDetailList;
            try{
                PEMEExamApproveRejectCntlAdmin.approveRejectExams('Approved');
            }
            catch(Exception e2){System.debug(e2.getMessage());}
            
        }
        User usr = [select id from User where IsActive = true LIMIT 1];
        brokerAcc.ownerId = usr.id;
        Customer_Feedback__c custFeedbck = new Customer_Feedback__c();
        custFeedbck.Responsible__c = salesforceLicUser.id;
        custFeedbck.From_Company__c = brokerAcc.id;
        custFeedbck.Source__c = 'MyGard';   
        insert custFeedbck;
        update brokerAcc;
        Test.stopTest();
    }
}