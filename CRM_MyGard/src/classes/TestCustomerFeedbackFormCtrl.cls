/***************************************************************** 
*    Deveployed By   :    Cognizant Technology Solution
*    Created Date    :    5/08/2018
*    Description     :    Controller for internal qualitative feedback (using Fluido objects)
******************************************************************/
@isTest
public class TestCustomerFeedbackFormCtrl{
    public static testMethod void CustomerFeedbackFormCtrl(){
        GardTestData test_data=new GardTestData();
        test_data.commonRecord();
        test_data.customsettings_rec();
        
        Test.startTest();
        System.runAs(GardTestData.salesforceLicUser){    
            List<fluidoconnect__Question__c> questList = new List<fluidoconnect__Question__c>();
            questList.add(GardTestData.fluidoQuestion1);
            customerFeedbackFormCtrl cfForm = new customerFeedbackFormCtrl();
            customerFeedbackFormCtrl.questionsOptionsWrapper qoWrapper = new customerFeedbackFormCtrl.questionsOptionsWrapper(GardTestData.clientAcc,questList,null,true);
            customerFeedbackFormCtrl.questionsAnswers qAns = new customerFeedbackFormCtrl.questionsAnswers(GardTestData.fluidoQuestion1,'Very demanding');
            cfForm.selectedProdArea = 'Claim_handler_Defence__c';
            cfForm.fetchQuestionsAndOptions();
            cfForm.mapHoldingSelectedRecords.put(GardTestData.clientAcc.Id,qoWrapper);
            cfForm.saveFeedback();
            cfForm.cancel();
            cfForm.getCampaignArea();
            cfForm.saveFeedback();
            
        }
        Test.stopTest();
    }
}