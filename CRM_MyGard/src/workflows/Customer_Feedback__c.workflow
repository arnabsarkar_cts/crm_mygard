<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Negative_Customer_feedback_notification_to_QM</fullName>
        <description>Negative Customer feedback notification to QM</description>
        <protected>false</protected>
        <recipients>
            <recipient>martin.skjaevestad@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>thor.magnus.berg@gard.no</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Templates/Customer_feedback_complain_logged</template>
    </alerts>
    <alerts>
        <fullName>Negative_feedback_notification</fullName>
        <description>Customer feedback notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>Customer_Relationship_Management</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>line.dahle@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olga.tome@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Responsible__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Support_Templates/Customer_feedback_complain_logged</template>
    </alerts>
    <rules>
        <fullName>Create a task</fullName>
        <actions>
            <name>Follow_up_on_feedback</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Customer_Feedback__c.Status__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Partner</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Customer NEGATIVE feedback notification</fullName>
        <actions>
            <name>Negative_Customer_feedback_notification_to_QM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Customer_Feedback__c.Type_of_feedback__c</field>
            <operation>equals</operation>
            <value>Negative feedback</value>
        </criteriaItems>
        <description>when Negative feedback has been logged QM to be notified</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cutomer feedback notification</fullName>
        <actions>
            <name>Negative_feedback_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Customer_Feedback__c.Type_of_feedback__c</field>
            <operation>equals</operation>
            <value>Negative feedback,Positive feedback,Suggestion</value>
        </criteriaItems>
        <description>2017.12.18 QM only wants negative feedback notification
when any feedback has been logged QM to be notified
2014.03.17 Kirsti requested for all not only for complains</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Follow_up_on_feedback</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Follow up on feedback</subject>
    </tasks>
</Workflow>
