<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>MDM_Set_Role_Sync_Date</fullName>
        <field>Sync_Date__c</field>
        <name>MDM Set Role Sync Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDM_Set_Role_Sync_Status_In_Progress</fullName>
        <field>Synchronisation_Status__c</field>
        <literalValue>Sync In Progress</literalValue>
        <name>MDM Set Role Sync Status In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Role_Field</fullName>
        <field>Role_Stamped__c</field>
        <formula>Valid_Role_Combination__r.Role__c</formula>
        <name>Set Role Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>MDM Set Role Sync Status</fullName>
        <active>true</active>
        <criteriaItems>
            <field>MDM_Role__c.Synchronisation_Status__c</field>
            <operation>equals</operation>
            <value>Waiting To Sync</value>
        </criteriaItems>
        <description>Timebased WF to sync outside of @Future or Batch</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MDM_Set_Role_Sync_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>MDM_Set_Role_Sync_Status_In_Progress</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>MDM_Role__c.Sync_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Set Role</fullName>
        <actions>
            <name>Set_Role_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Role Stamped value on the MDM Role record.</description>
        <formula>Role_Stamped__c != Valid_Role_Combination__r.Role__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
