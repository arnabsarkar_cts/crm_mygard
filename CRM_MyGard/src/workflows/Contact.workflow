<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contact_Information_Change_Acknowledgement</fullName>
        <description>Contact Information Change Acknowledgement</description>
        <protected>false</protected>
        <recipients>
            <field>Email_Temp__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@gard.no</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GARD_Templates/Contact_Details_Change_Acknowledgement</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_Check_The_Admin_Checkbox</fullName>
        <description>Email Alert Check The Admin Checkbox</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@gard.no</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GARD_Templates/MyGard_Set_Admin_Checkbox</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_Uncheck_The_Admin_Checkbox</fullName>
        <description>Email Alert Uncheck The Admin Checkbox</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GARD_Templates/MyGard_Reset_Admin_Checkbox</template>
    </alerts>
    <alerts>
        <fullName>GIC_id_missing_for_LoC_contact</fullName>
        <description>GIC id missing for LoC contact</description>
        <protected>false</protected>
        <recipients>
            <recipient>tor.dahl@gard.org</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>System_Administration/GIC_id_missing_for_LoC_contact</template>
    </alerts>
    <alerts>
        <fullName>Outbound_Sync_Failure_Alert_Contact</fullName>
        <description>Outbound Sync Failure Alert - Contact</description>
        <protected>false</protected>
        <recipients>
            <recipient>Outbound_Sync_Failure_Notification_Users</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>System_Administration/Outbound_Sync_Failure_Contact</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Status</fullName>
        <field>Request_Status__c</field>
        <literalValue>Submit for Approval</literalValue>
        <name>Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDM_Set_Contact_Sync_Date</fullName>
        <field>Sync_Date__c</field>
        <name>MDM Set Contact Sync Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDM_Set_Contact_Sync_Status_In_Progress</fullName>
        <field>Synchronisation_Status__c</field>
        <literalValue>Sync In Progress</literalValue>
        <name>MDM Set Contact Sync Status In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_LoC_order</fullName>
        <field>LoC_Order__c</field>
        <name>Remove LoC order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_email_address_in_contact</fullName>
        <field>Email</field>
        <name>Remove email address in contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_from_Alerts</fullName>
        <field>Publication_Alerts__c</field>
        <literalValue>0</literalValue>
        <name>Remove from Alerts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_from_Director_s_report</fullName>
        <field>Publication_Directors_Report__c</field>
        <literalValue>0</literalValue>
        <name>Remove from Director&apos;s report</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_from_Event_Invitations</fullName>
        <field>Event_invitations__c</field>
        <literalValue>0</literalValue>
        <name>Remove from Event Invitations</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_from_Gard_Rules</fullName>
        <field>Publications_Gard_Rules__c</field>
        <literalValue>0</literalValue>
        <name>Remove from Gard Rules</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_from_Gard_Rules_Norwegian</fullName>
        <field>Publication_Gard_Rules_Norwegian__c</field>
        <literalValue>0</literalValue>
        <name>Remove from Gard Rules Norwegian</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_from_Guidance_to_Master</fullName>
        <field>Publication_Guidance_to_Master__c</field>
        <literalValue>0</literalValue>
        <name>Remove from Guidance to Master</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_from_Insights</fullName>
        <field>Publication_Insights__c</field>
        <literalValue>0</literalValue>
        <name>Remove from Insights</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_from_LP</fullName>
        <field>Publication_Loss_prevention_circulars__c</field>
        <literalValue>0</literalValue>
        <name>Remove from LP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_from_MC</fullName>
        <field>Publication_Member_circulars__c</field>
        <literalValue>0</literalValue>
        <name>Remove from MC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Status</fullName>
        <field>Request_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Request Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Deleted_Flag</fullName>
        <description>Resets deleted flag once NLE is set to false</description>
        <field>Deleted__c</field>
        <literalValue>0</literalValue>
        <name>Reset Deleted Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Contact_Owner_to_VP_Claims</fullName>
        <description>Set contact owner to the VP of Claims, currently 	
Geir Sandnes</description>
        <field>OwnerId</field>
        <lookupValue>geir.sandnes@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Contact Owner to VP Claims</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Deleted_Flag</fullName>
        <field>Deleted__c</field>
        <literalValue>1</literalValue>
        <name>Set Deleted Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StampingEmail</fullName>
        <field>Email</field>
        <formula>IF(Email_Temp__c &lt;&gt; null &amp;&amp; Email_Temp__c &lt;&gt; &apos;&apos;, Email_Temp__c , Email)</formula>
        <name>StampingEmail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StampingFirstName</fullName>
        <field>FirstName</field>
        <formula>IF(First_Name_Temp__c &lt;&gt; null &amp;&amp; First_Name_Temp__c &lt;&gt; &apos;&apos;, First_Name_Temp__c, FirstName)</formula>
        <name>StampingFirstName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>StampingLastName</fullName>
        <field>LastName</field>
        <formula>IF(Last_Name_Temp__c &lt;&gt; null &amp;&amp; Last_Name_Temp__c &lt;&gt; &apos;&apos;, Last_Name_Temp__c, LastName)</formula>
        <name>StampingLastName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Pending_With_Gard_checkbox</fullName>
        <field>Pending_With_Gard_Admin__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Pending With Gard checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>final_approval_status</fullName>
        <description>final approval status</description>
        <field>Request_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>final approval status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isChanged</fullName>
        <description>isChanged</description>
        <field>IsChanged__c</field>
        <literalValue>0</literalValue>
        <name>isChanged</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Gard_User_Management_Reset_Contact_Admin</fullName>
        <actions>
            <name>Email_Alert_Uncheck_The_Admin_Checkbox</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow fires when the administrator__c check box is unchecked!</description>
        <formula>IF(ISNEW(), false,  		  IF(PRIORVALUE(Administrator__c),  						  IF(Administrator__c , false, true), false)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Gard_User_Management_Set_Contact_Admin</fullName>
        <actions>
            <name>Email_Alert_Check_The_Admin_Checkbox</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Administrator__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This workflow fires when the administrator__c check box is checked!</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MDM Set Contact Sync Status</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Synchronisation_Status__c</field>
            <operation>equals</operation>
            <value>Waiting To Sync</value>
        </criteriaItems>
        <description>Timebased WF to sync outside of @Future or Batch</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MDM_Set_Contact_Sync_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>MDM_Set_Contact_Sync_Status_In_Progress</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Contact.Sync_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>No longer employed</fullName>
        <actions>
            <name>Remove_LoC_order</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_email_address_in_contact</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_from_Alerts</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_from_Director_s_report</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_from_Event_Invitations</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_from_Gard_Rules</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_from_Gard_Rules_Norwegian</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_from_Guidance_to_Master</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_from_Insights</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Remove_from_MC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.No_Longer_Employed_by_Company__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Remove email address when contact set as no longer employed,, remove from publications and org chart</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Outbound Sync Failure Notification - Contact</fullName>
        <actions>
            <name>Outbound_Sync_Failure_Alert_Contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Synchronisation_Status__c</field>
            <operation>equals</operation>
            <value>Sync Failed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reset Deleted Flag</fullName>
        <actions>
            <name>Reset_Deleted_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.No_Longer_Employed_by_Company__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Reset the flag once NLE is false</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Reset Pending With Gard checkbox</fullName>
        <actions>
            <name>Uncheck_Pending_With_Gard_checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.No_Longer_Employed_by_Company__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>if not employed by company is checked, &apos;pending with gard admin&apos;  checkbox will be reset.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Correspondent Contact Owner</fullName>
        <actions>
            <name>Set_Contact_Owner_to_VP_Claims</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.RecordTypeId</field>
            <operation>equals</operation>
            <value>Correspondent Contact</value>
        </criteriaItems>
        <description>Correspondent Contacts should be owned by the VP of claims operations.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Deleted Flag</fullName>
        <actions>
            <name>Set_Deleted_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.No_Longer_Employed_by_Company__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
