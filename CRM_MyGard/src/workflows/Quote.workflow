<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Quote_Name</fullName>
        <field>Name</field>
        <formula>IF( FIND(Opportunity.Name, Name)&gt;0,  Name, Opportunity.Name + &apos; : &apos; + Name)</formula>
        <name>Set Quote Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Quote Name</fullName>
        <actions>
            <name>Set_Quote_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This sets a standard quote name of Opportunity name : quote number</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
