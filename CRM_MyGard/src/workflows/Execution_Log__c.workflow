<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Non_Failed_Email_Alert</fullName>
        <description>Send Non Failed Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>olga.tome@gard.no</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>System_Administration/Process_Execution_Alert</template>
    </alerts>
    <rules>
        <fullName>Integration Process Non Fatal Error</fullName>
        <actions>
            <name>Send_Non_Failed_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Execution_Log__c.Priority__c</field>
            <operation>greaterThan</operation>
            <value>4</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
