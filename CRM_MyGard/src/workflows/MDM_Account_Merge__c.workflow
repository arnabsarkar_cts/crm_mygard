<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Outbound_Sync_Failure_Alert_MDM_Account_Merge</fullName>
        <description>Outbound Sync Failure Alert - MDM Account Merge</description>
        <protected>false</protected>
        <recipients>
            <recipient>Outbound_Sync_Failure_Notification_Users</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>System_Administration/Outbound_Sync_Failure_MDM_Account_Merge</template>
    </alerts>
    <fieldUpdates>
        <fullName>MDM_Set_Merge_Sync_Date</fullName>
        <field>Sync_Date__c</field>
        <name>MDM Set Merge Sync Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDM_Set_Merge_Sync_Status_In_Progress</fullName>
        <field>Synchronisation_Status__c</field>
        <literalValue>Sync In Progress</literalValue>
        <name>MDM Set Merge Sync Status In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>MDM Set Account Merge Sync Status</fullName>
        <active>true</active>
        <criteriaItems>
            <field>MDM_Account_Merge__c.Synchronisation_Status__c</field>
            <operation>equals</operation>
            <value>Waiting To Sync</value>
        </criteriaItems>
        <description>Timebased WF to sync outside of @Future or Batch</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MDM_Set_Merge_Sync_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>MDM_Set_Merge_Sync_Status_In_Progress</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>MDM_Account_Merge__c.Sync_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Outbound Sync Failure Notification - MDM Account Merge</fullName>
        <actions>
            <name>Outbound_Sync_Failure_Alert_MDM_Account_Merge</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>MDM_Account_Merge__c.Synchronisation_Status__c</field>
            <operation>equals</operation>
            <value>Sync Failed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
