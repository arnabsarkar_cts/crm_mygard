<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ITOPF</fullName>
        <description>record type updatation</description>
        <field>RecordTypeId</field>
        <lookupValue>ITOPF</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type ITOPF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lay_up_Marine</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Lay_up_Marine</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type Lay up_Marine</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lay_up_PI</fullName>
        <description>update record type</description>
        <field>RecordTypeId</field>
        <lookupValue>Lay_up_P_I</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type Lay up_PI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mobile_Offshore_Unit</fullName>
        <description>Update record type</description>
        <field>RecordTypeId</field>
        <lookupValue>Mobile_offshore_unit</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type Mobile Offshore Unit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offshore_object</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Offshore_object</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type Offshore object</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RI_declaration</fullName>
        <description>update record type</description>
        <field>RecordTypeId</field>
        <lookupValue>RI_declaration</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type RI declaration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Ship_owner_layout</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Ship_owner</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type Ship owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_Charterers_Renewal_Qu</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Charterers_Renewal_Questionnaire</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type Charterers Renewal Qu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>charter_client</fullName>
        <description>update record type</description>
        <field>RecordTypeId</field>
        <lookupValue>charter_clients</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type charter client</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>charter_object</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Charter_object</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type charter object</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update record type Charterers Renewal Questionnaire</fullName>
        <actions>
            <name>Update_record_type_Charterers_Renewal_Qu</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Charterers Renewal Questionnaire</value>
        </criteriaItems>
        <description>Update record type for Charterers Renewal Questionnaire</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type ITOPF</fullName>
        <actions>
            <name>ITOPF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>ITOPF form for new entries</value>
        </criteriaItems>
        <description>Update record type
value changed for SF-3951 - &quot;ITOPF&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type Lay up_Marine</fullName>
        <actions>
            <name>Lay_up_Marine</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Layup form - Marine</value>
        </criteriaItems>
        <description>record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type Lay up_PI</fullName>
        <actions>
            <name>Lay_up_PI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Layup form - PI</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type Mobile Offshore Unit</fullName>
        <actions>
            <name>Mobile_Offshore_Unit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Application form for Mobile offshore unit</value>
        </criteriaItems>
        <description>to update record type
value changed for SF-3951 - &quot;Mobile Offshore Unit&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type Offshore object</fullName>
        <actions>
            <name>Offshore_object</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Offshore Vessel</value>
        </criteriaItems>
        <description>to update record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type RI declaration</fullName>
        <actions>
            <name>RI_declaration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>RI declaration form</value>
        </criteriaItems>
        <description>update record type
value changed for SF-3951 - &quot;RI Declaration&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type Ship owner</fullName>
        <actions>
            <name>Ship_owner_layout</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Application form for Ship owners</value>
        </criteriaItems>
        <description>to update record type
value changed for SF-3951 - &quot;Ship Owners&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type charter client</fullName>
        <actions>
            <name>charter_client</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Comprehensive charterers cover entry form</value>
        </criteriaItems>
        <description>to update record type 
value changed for SF-3951 - &quot;CharterClients&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type charter object</fullName>
        <actions>
            <name>charter_object</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Application form for Chartered object</value>
        </criteriaItems>
        <description>to update record type
value changed for SF-3951 - &quot;Charter Vessel&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
