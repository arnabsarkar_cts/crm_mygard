<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Uncheck_isNoLongerEmployed</fullName>
        <description>Uncheck isNoLongerEmployed__c on user level, isNoLongerEmployed__c field is created only for access revocation.</description>
        <field>isNoLongerEmployed__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck isNoLongerEmployed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Uncheck isNoLongerEmployed field after user deactivation</fullName>
        <actions>
            <name>Uncheck_isNoLongerEmployed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Used to uncheck isNoLongerEmployed__c field on user level. IsNoLongerEmployed__c is used only for access revocation.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
