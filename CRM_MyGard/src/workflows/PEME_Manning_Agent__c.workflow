<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Ex_Date_Of_Approve_Manning_Agent</fullName>
        <field>Expiration_Date__c</field>
        <name>Update Ex_Date Of Approve Manning Agent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Expire_Date_Of_Manning_Agent</fullName>
        <field>Expiration_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Expire Date Of Manning Agent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Expire Date Of Approved Manning Agent</fullName>
        <actions>
            <name>Update_Ex_Date_Of_Approve_Manning_Agent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PEME_Manning_Agent__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Expire Date Of Manning Agent</fullName>
        <actions>
            <name>Update_Expire_Date_Of_Manning_Agent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PEME_Manning_Agent__c.Status__c</field>
            <operation>equals</operation>
            <value>Expired</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
