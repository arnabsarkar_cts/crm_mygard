<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_UnderwriterId</fullName>
        <field>UnderwriterId__c</field>
        <formula>Underwriter__r.ContactId__c</formula>
        <name>Update UnderwriterId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Underwriter_Text</fullName>
        <field>UnderwriterNameTemp__c</field>
        <formula>Underwriter__r.FirstName+&apos; &apos;+Underwriter__r.LastName</formula>
        <name>Update Underwriter Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>asset_field_update</fullName>
        <field>Gard_Share_Internal2__c</field>
        <formula>IF((CONTAINS(Gard_Share__c, &quot;.&quot;)),
text(round(value(Gard_Share__c),2))&amp;&quot;0&quot;,Gard_Share__c)</formula>
        <name>asset_field_update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Asset_1</fullName>
        <actions>
            <name>asset_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT ISNULL( Id )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Copy Underwriter Text</fullName>
        <actions>
            <name>Update_UnderwriterId</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Underwriter_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.Underwriter__c</field>
            <operation>notEqual</operation>
            <value>Hizibizi</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
