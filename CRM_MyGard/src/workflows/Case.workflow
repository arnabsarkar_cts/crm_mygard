<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AddNewUsrActionCompletionMail</fullName>
        <description>Send mail when case status changed to close</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>no-reply@gard.no</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GARD_Templates/MyGard_ActionCompletionEmail_NewUserAccess</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_on_Reject_Case</fullName>
        <description>Send Email on Reject Case</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GARD_Templates/Reject_Case_Template</template>
    </alerts>
    <alerts>
        <fullName>Send_email_for_case_status_change_to_Closed</fullName>
        <description>Send email for case status change to Closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@gard.no</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_templates/Notification_Case_status_change_to_Closed</template>
    </alerts>
    <alerts>
        <fullName>Send_email_for_case_status_change_to_Waiting_for_Customer</fullName>
        <description>Send email for case status change to Waiting for Customer</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>no-reply@gard.no</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_templates/Notification_Case_status_change_to_Waiting_for_Customer</template>
    </alerts>
    <fieldUpdates>
        <fullName>Admin_request_Update_record_type</fullName>
        <description>This is the record type field update for cases generated from all types of admin request like Admin user change, Add new user, Contact me and Revoke user access</description>
        <field>RecordTypeId</field>
        <lookupValue>Admin_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Admin request - Update record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_Recordtype_AdminRequest</fullName>
        <description>Admin user change, Add new user, Contact me and Revoke user access</description>
        <field>RecordTypeId</field>
        <lookupValue>Admin_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Assign_Recordtype_AdminRequest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Blue_card_record_type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Blue_card</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Blue card - record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_record_type_update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>UWR_Forms</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Case record type update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_set_to_OPEN</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Case set to OPEN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Review_status_change</fullName>
        <description>Contract Review status change on loc__c field update</description>
        <field>Status</field>
        <literalValue>Documentation Ready</literalValue>
        <name>Contract Review status change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Review_status_change_1</fullName>
        <description>Contract Review status change on loc__c field update</description>
        <field>Status</field>
        <literalValue>Documentation Ready</literalValue>
        <name>Contract Review status change_1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MLC_Certificate_record_type_update</fullName>
        <description>for details SF-3703</description>
        <field>RecordTypeId</field>
        <lookupValue>MLC_Certificate</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>MLC Certificate record type update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_change_Case_type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Request_change</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Request change - Case type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_status_to_In_Progress</fullName>
        <description>Setting status to In Progress</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Set status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRejectionDate</fullName>
        <description>UpdateRejectionDate</description>
        <field>Claim_Rejection_Date__c</field>
        <formula>TODAY()</formula>
        <name>UpdateRejectionDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_COFR_record_type</fullName>
        <description>Update COFR record type to &apos;COFR Request&apos; type of cases</description>
        <field>RecordTypeId</field>
        <lookupValue>COFR_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update COFR record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contract_Review_External_ID</fullName>
        <description>This field update, updates the field - DM_Contract_Review_Case_ID__c - with the value in the standard caseNumber field, so that middleware can identify the case for update.</description>
        <field>DM_Contract_Review_Case_ID__c</field>
        <formula>CaseNumber</formula>
        <name>Update_Contract Review_External_ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_DM_document_number</fullName>
        <description>DM document number should be blank when status is In Progress</description>
        <field>LOC__c</field>
        <name>Update DM document number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_External_ID</fullName>
        <field>SFDC_Claim_Ref_ID__c</field>
        <formula>Case_Autonumber__c</formula>
        <name>Update_External_ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_LOC_RecordType</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Contract_Review_Documentation_Ready</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update LOC Case&apos;s RecordType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MyGard_Claim_ID</fullName>
        <description>Update MyGard_Claim_ID__c</description>
        <field>MyGard_Claim_ID__c</field>
        <formula>Case_Autonumber__c</formula>
        <name>Update MyGard Claim ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Open</fullName>
        <description>Updates the status to Open when &apos;Contract Uploaded in DM&apos; equals true and case type is Crew Contract Review</description>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Update Status of Crew Contract Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_Charterers_Renewal_Qu</fullName>
        <field>RecordTypeId</field>
        <lookupValue>UWR_Charterers_Renewal_Questionnaire</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type Charterers Renewal Qu</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_ITOPF</fullName>
        <description>record type updatation</description>
        <field>RecordTypeId</field>
        <lookupValue>UWR_ITOPF</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type ITOPF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_Lay_up_Marine</fullName>
        <field>RecordTypeId</field>
        <lookupValue>UWR_Lay_up_Marine</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type Lay up_Marine</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_Lay_up_PI</fullName>
        <description>update record type</description>
        <field>RecordTypeId</field>
        <lookupValue>UWR_Lay_up_P_I</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type Lay up_PI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_Mobile_Offshore_Unit</fullName>
        <description>Update record type</description>
        <field>RecordTypeId</field>
        <lookupValue>UWR_Mobile_offshore_unit</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type Mobile Offshore Unit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_Offshore_object</fullName>
        <field>RecordTypeId</field>
        <lookupValue>UWR_Offshore_object</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type Offshore object</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_RI_declaration</fullName>
        <description>update record type</description>
        <field>RecordTypeId</field>
        <lookupValue>UWR_RI_declaration</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type RI declaration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_Ship_owner</fullName>
        <field>RecordTypeId</field>
        <lookupValue>UWR_Ship_owner</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type Ship owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_charter_client</fullName>
        <description>update record type</description>
        <field>RecordTypeId</field>
        <lookupValue>UWR_Charter_clients</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type charter client</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_charter_object</fullName>
        <field>RecordTypeId</field>
        <lookupValue>UWR_Charter_object</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type charter object</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_for_Contract_review</fullName>
        <description>Update record type for contract review type claims</description>
        <field>RecordTypeId</field>
        <lookupValue>Crew_Contract_LOC_save_locked</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type for Contract review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_for_status_Closed</fullName>
        <description>Update record type for status Closed</description>
        <field>RecordTypeId</field>
        <lookupValue>Contract_Review_Closed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type for status Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_for_status_Doc_ready</fullName>
        <description>Update record type for status Doc ready</description>
        <field>RecordTypeId</field>
        <lookupValue>Contract_Review_Documentation_Ready</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type for status Doc ready</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_for_status_Doc_ready1</fullName>
        <description>Update record type for status Doc ready</description>
        <field>RecordTypeId</field>
        <lookupValue>Contract_Review_Documentation_Ready</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type for status Doc ready1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_for_status_Open</fullName>
        <description>Update record type for status update of crew contract LOC to Open</description>
        <field>RecordTypeId</field>
        <lookupValue>Crew_Contract_LOC_save_locked</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type for status Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_for_status_compl_revi</fullName>
        <description>Update record type for status complete review</description>
        <field>RecordTypeId</field>
        <lookupValue>Contract_Review_Review_Completed</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type for status compl revi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_for_status_inProgress</fullName>
        <description>Update record type for status inProgress</description>
        <field>RecordTypeId</field>
        <lookupValue>Crew_Contract_LOC_with_send_LOC_button</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type for status inProgress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_for_status_start_revi</fullName>
        <description>Contract review : Update record type for status update of crew contract LOC to start review</description>
        <field>RecordTypeId</field>
        <lookupValue>Crew_Contract_LOC_with_send_LOC_button</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type for status start revi</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_for_status_update_DM</fullName>
        <description>DM Sync</description>
        <field>RecordTypeId</field>
        <lookupValue>Contract_Review</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type for status update DM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_record_type_for_status_waitForLOC</fullName>
        <description>Update record type for status update waiting for LOC</description>
        <field>RecordTypeId</field>
        <lookupValue>Contract_Review</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update record type for status waitForLOC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>case_recordtype_update_for_req_change</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Request_change</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>case recordtype update for req change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Admin Request Case assigned</fullName>
        <actions>
            <name>Case_set_to_OPEN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Admin Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>MyGard_Gard_Administrator</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>deactivated for SF-3951</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Admin user change - user management</fullName>
        <actions>
            <name>Admin_request_Update_record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Admin user change</value>
        </criteriaItems>
        <description>Update record type for Admin user change type cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Admin_Request_Case_assigned_V2</fullName>
        <actions>
            <name>Set_status_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(ISNEW()), ISCHANGED(OwnerId), ISPICKVAL(Status,&apos;New&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Blue card - case layout</fullName>
        <actions>
            <name>Blue_card_record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Renew Blue Card</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>New Blue Card</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Blue card for new object</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Contact me</fullName>
        <actions>
            <name>Admin_request_Update_record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Add new user,Contact me</value>
        </criteriaItems>
        <description>Will update the record type of cases which are of contact me and Add new user types</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Generate_Case_External_ID</fullName>
        <actions>
            <name>Update_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_MyGard_Claim_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>notEqual</operation>
            <value>integration</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>notEqual</operation>
            <value>integration_Mule</value>
        </criteriaItems>
        <description>The workflow generates the internal Mygard claim id for the case record created by the users.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Generate_Contract_Review_External_ID</fullName>
        <actions>
            <name>Update_Contract_Review_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Crew Contract Review</value>
        </criteriaItems>
        <description>It copies the casenumber (standard OOTB field) to the DM_Contract_Review_Case_ID__c so that middleware can properly identify and update this case during integrations.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>MLC Certificate</fullName>
        <actions>
            <name>MLC_Certificate_record_type_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Request new MLC Certificate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Request change to MLC Certificate</value>
        </criteriaItems>
        <description>for details SF-3703</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Remove Admin Access - user management</fullName>
        <actions>
            <name>Admin_request_Update_record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Remove Admin Access</value>
        </criteriaItems>
        <description>Update record type for Remove Admin Access type cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Request change</fullName>
        <actions>
            <name>case_recordtype_update_for_req_change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Request covers for existing Objects,Request covers for new objects,Request miscellaneous changes,Request change to assureds,Request Change to Terms,Request change to mortgagees,Request to change company address</value>
        </criteriaItems>
        <description>Assign record type for request change cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Revoke user access - user management</fullName>
        <actions>
            <name>Admin_request_Update_record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Remove access</value>
        </criteriaItems>
        <description>Update record type for Remove access type cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send ActionCompletion mail for AddNewUser</fullName>
        <actions>
            <name>AddNewUsrActionCompletionMail</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Add new user</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>On case status changed to close

deactivated for SF-3951</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Email on Case Rejection</fullName>
        <actions>
            <name>Send_Email_on_Reject_Case</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Email to be sent to the requestor if one case is termed as rejected.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send email on Case status change to Closed</fullName>
        <actions>
            <name>Send_email_for_case_status_change_to_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COFR Request,Admin Request,Blue card,Request change,UWR Forms,MLC Certificate,UWR - Charter clients,UWR - Charter object,UWR - Offshore object,UWR - RI declaration,UWR - Ship owner,UWR - Mobile offshore unit,UWR - Lay up P&amp;I,UWR - Lay up Marine,U</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>notEqual</operation>
            <value>Problem,Feature Request,Question,Contract Review,Request covers for existing Objects,Request covers for new objects</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_me_area__c</field>
            <operation>notEqual</operation>
            <value>Underwriting,Claims,Accounting,General,PEME,General request</value>
        </criteriaItems>
        <description>Send email when Case status changed to Closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send email on Case status change to Waiting for Customer</fullName>
        <actions>
            <name>Send_email_for_case_status_change_to_Waiting_for_Customer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for customer</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>COFR Request,Admin Request,Blue card,Request change,UWR Forms,MLC Certificate,UWR - Charter clients,UWR - Charter object,UWR - Offshore object,UWR - RI declaration,UWR - Ship owner,UWR - Mobile offshore unit,UWR - Lay up P&amp;I,UWR - Lay up Marine,U</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>notEqual</operation>
            <value>Problem,Feature Request,Question,Contract Review,Request covers for existing Objects,Request covers for new objects</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contact_me_area__c</field>
            <operation>notEqual</operation>
            <value>Underwriting,Claims,Accounting,General,PEME,General request</value>
        </criteriaItems>
        <description>Send email when Case status changed to Waiting for Customer.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Service Request Case assigned</fullName>
        <actions>
            <name>Case_set_to_OPEN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Service Request,Blue card</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.OwnerId</field>
            <operation>notEqual</operation>
            <value>Customer Transactions Nordic queue,Trading Certificates Team</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UWR Forms</fullName>
        <actions>
            <name>Case_record_type_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13 OR 14</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Ship Owners</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Ship Owners</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Layup form - Marine</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>CharterClients</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>RI Declaration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Charter Vessel</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Mobile Offshore Unit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>ITOPF</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Layup form - PI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Offshore Vessel</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>&quot;LOU-LOI for Gard P&amp;I , ShipOwners&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Free Pass Agreement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Charterers Renewal Questionnaire</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>&quot;LOU-LOI for Gard P&amp;I, MOU&quot;</value>
        </criteriaItems>
        <description>This is deactivated. The new one is with V2.0</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UWR Forms V2</fullName>
        <actions>
            <name>Case_record_type_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11 OR 12 OR 13 OR 14</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Application form for Ship owners</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Application form for Ship owners</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Layup form - Marine</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Comprehensive charterers cover entry form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>RI declaration form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Application form for Chartered object</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Application form for Mobile offshore unit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>ITOPF form for new entries</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Layup form - PI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Application form for Offshore object</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>&quot;Renewal LOU-LOI for Gard P&amp;I, Shipowners&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Free Pass Agreement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Charterers Renewal Questionnaire</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>&quot;Renewal LOU-LOI for Gard P&amp;I, MOU&quot;</value>
        </criteriaItems>
        <description>The V2. V1 is deactivated now after SF-3951.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UWR Forms V3</fullName>
        <actions>
            <name>Case_record_type_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>&quot;Renewal LOU-LOI for Gard P&amp;I, Shipowners&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>&quot;Renewal LOU-LOI for Gard P&amp;I, MOU&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Free Pass Agreement</value>
        </criteriaItems>
        <description>The V3. V2 is deactivated now after SF-4679.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update COFR Case RecordType</fullName>
        <actions>
            <name>Update_COFR_record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>COFR Request</value>
        </criteriaItems>
        <description>used to update the record type field of &apos;COFR Request&apos; type of cases</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update case status to Documentation Ready on middleware LOC update</fullName>
        <actions>
            <name>Contract_Review_status_change_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_record_type_for_status_Doc_ready1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.LOC__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Crew Contract Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contract_Uploaded_in_DM__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update Case status to &apos;Documentation Ready&apos; once middleware updates the LOC number.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type Charterers Renewal Questionnaire</fullName>
        <actions>
            <name>Update_record_type_Charterers_Renewal_Qu</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Charterers Renewal Questionnaire</value>
        </criteriaItems>
        <description>Update record type for Charterers Renewal Questionnaire</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type ITOPF</fullName>
        <actions>
            <name>Update_record_type_ITOPF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>ITOPF form for new entries</value>
        </criteriaItems>
        <description>Update record type
value changed for SF-3951 - &quot;ITOPF&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type Lay up_Marine</fullName>
        <actions>
            <name>Update_record_type_Lay_up_Marine</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Layup form - Marine</value>
        </criteriaItems>
        <description>record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type Lay up_PI</fullName>
        <actions>
            <name>Update_record_type_Lay_up_PI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Layup form - PI</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type Mobile Offshore Unit</fullName>
        <actions>
            <name>Update_record_type_Mobile_Offshore_Unit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Application form for Mobile offshore unit</value>
        </criteriaItems>
        <description>to update record type
value changed for SF-3951 - &quot;Mobile Offshore Unit&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type Offshore object</fullName>
        <actions>
            <name>Update_record_type_Offshore_object</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Application form for Offshore object</value>
        </criteriaItems>
        <description>to update record type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type RI declaration</fullName>
        <actions>
            <name>Update_record_type_RI_declaration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>RI declaration form</value>
        </criteriaItems>
        <description>update record type
value changed for SF-3951 - &quot;RI Declaration&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type Ship owner</fullName>
        <actions>
            <name>Update_record_type_Ship_owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Application form for Ship owners</value>
        </criteriaItems>
        <description>to update record type
value changed for SF-3951 - &quot;Ship Owners&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type charter client</fullName>
        <actions>
            <name>Update_record_type_charter_client</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Comprehensive charterers cover entry form</value>
        </criteriaItems>
        <description>to update record type 
value changed for SF-3951 - &quot;CharterClients&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type charter object</fullName>
        <actions>
            <name>Update_record_type_charter_object</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Application form for Chartered object</value>
        </criteriaItems>
        <description>to update record type
value changed for SF-3951 - &quot;Charter Vessel&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type for Contract review</fullName>
        <actions>
            <name>Update_Status_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_record_type_for_Contract_review</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Crew Contract Review</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Contract_Uploaded_in_DM__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Update record type to &apos;Contract Review - OPEN&apos; on &apos;Contract Uploaded in DM&apos; field check.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type for status Closed</fullName>
        <actions>
            <name>Update_record_type_for_status_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Crew Contract Review</value>
        </criteriaItems>
        <description>Contract review : Update record type for status Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type for status Doc ready</fullName>
        <actions>
            <name>Update_record_type_for_status_Doc_ready</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Documentation Ready</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Crew Contract Review</value>
        </criteriaItems>
        <description>Contract review : Update record type for status Doc ready</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type for status complete review</fullName>
        <actions>
            <name>Update_record_type_for_status_compl_revi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Review Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Crew Contract Review</value>
        </criteriaItems>
        <description>Contract review : Update record type for status complete review</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type for status inProgress</fullName>
        <actions>
            <name>Update_DM_document_number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_record_type_for_status_inProgress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Crew Contract Review</value>
        </criteriaItems>
        <description>Contract review : Update record type for status inProgress</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type for status update of crew contract LOC to DM Sync</fullName>
        <actions>
            <name>Update_record_type_for_status_update_DM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>DM Synchronising</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Crew Contract Review</value>
        </criteriaItems>
        <description>Contract review : Update record type for status update of crew contract LOC DM Sync.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type for status update of crew contract LOC to Open</fullName>
        <actions>
            <name>Update_record_type_for_status_Open</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Crew Contract Review</value>
        </criteriaItems>
        <description>Contract review : Update record type for status update of crew contract LOC to Open</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type for status update of crew contract LOC to start review</fullName>
        <actions>
            <name>Update_record_type_for_status_start_revi</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Review Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Crew Contract Review</value>
        </criteriaItems>
        <description>Update record type for status update of crew contract LOC to start review</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update record type for status update waiting for LOC</fullName>
        <actions>
            <name>Update_record_type_for_status_waitForLOC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Waiting for LOC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Crew Contract Review</value>
        </criteriaItems>
        <description>Contract review : Update record type for status update waiting for LOC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UpdateRejectionDate</fullName>
        <actions>
            <name>UpdateRejectionDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Update Rejection Date by today&apos;s date, if status changed to rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
