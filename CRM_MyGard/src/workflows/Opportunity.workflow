<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CT_Marine_notification_not_WON_Renewal_opp</fullName>
        <description>CT Marine notification not WON Renewal opp</description>
        <protected>false</protected>
        <recipients>
            <field>CT_Team_Marine_email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Not_WON_Renewal_notification</template>
    </alerts>
    <alerts>
        <fullName>CT_P_I_notification_not_WON_opp</fullName>
        <description>CT P&amp;I notification not WON opp</description>
        <protected>false</protected>
        <recipients>
            <field>CT_Team_PI_email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Not_WON_Renewal_notification</template>
    </alerts>
    <alerts>
        <fullName>CT_and_Charterers_P_I_notification_not_WON_opp</fullName>
        <ccEmails>Charterers.Traders.Support@gard.no</ccEmails>
        <description>CT and Charterers P&amp;I notification not WON opp</description>
        <protected>false</protected>
        <recipients>
            <field>CT_Team_PI_email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Not_WON_Renewal_notification</template>
    </alerts>
    <alerts>
        <fullName>New_biz_Upsell_Energy_close_WON_Notification_to_Head_claims_operation</fullName>
        <description>New biz/Upsell Energy close WON Notification to Head claims operation and Accounting</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>jan-hugo.marthinsen@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>malin.gustavi@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olga.tome@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>susanne.sorhult@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vivi.sandsten@gard.no</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/New_biz_Energy_WON_notification</template>
    </alerts>
    <alerts>
        <fullName>New_biz_Upsell_MBR_close_WON_Notification_to_CT_team</fullName>
        <description>New biz/Upsell MBR close WON Notification to CT team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>olga.tome@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>susanne.sorhult@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vivi.sandsten@gard.no</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/New_biz_Energy_WON_notification</template>
    </alerts>
    <alerts>
        <fullName>New_biz_Upsell_Marine_close_WON_Notification_to_Head_claims_operation</fullName>
        <description>New biz/Upsell Marine close WON Notification to Head claims operation and Accounting</description>
        <protected>false</protected>
        <recipients>
            <field>CT_Team_Marine_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>malin.gustavi@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olga.tome@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vivi.sandsten@gard.no</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/New_biz_Marine_WON_notification</template>
    </alerts>
    <alerts>
        <fullName>New_biz_Upsell_P_I_Charterers_close_WON_Notification_to_Head_claims_operation</fullName>
        <ccEmails>Charterers.Traders.Support@gard.no</ccEmails>
        <description>New biz/Upsell P&amp;I Charterers close WON Notification to Head claims operation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>olga.tome@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vivi.sandsten@gard.no</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/New_biz_P_I_WON_notification</template>
    </alerts>
    <alerts>
        <fullName>New_biz_Upsell_P_I_Defence_close_WON_Notification_to_Head_claims_operation</fullName>
        <description>New biz/Upsell P&amp;I Defence close WON Notification to Head claims operation</description>
        <protected>false</protected>
        <recipients>
            <recipient>joakim.bronder@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olga.tome@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vivi.sandsten@gard.no</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/New_biz_P_I_WON_notification</template>
    </alerts>
    <alerts>
        <fullName>New_biz_Upsell_P_I_MOU_close_WON_Notification_to_Head_claims_operation</fullName>
        <description>New biz/Upsell P&amp;I MOU close WON Notification to Head claims operation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>olga.tome@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>susanne.sorhult@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vivi.sandsten@gard.no</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/New_biz_P_I_WON_notification</template>
    </alerts>
    <alerts>
        <fullName>New_biz_Upsell_P_I_close_WON_Notification_to_Head_claims_operation</fullName>
        <description>New biz/Upsell P&amp;I close WON Notification to Head claims operation</description>
        <protected>false</protected>
        <recipients>
            <field>CT_Team_PI_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>malin.gustavi@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olga.tome@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vivi.sandsten@gard.no</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/New_biz_P_I_WON_notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Area_Managers</fullName>
        <description>Notify Area Managers</description>
        <protected>false</protected>
        <recipients>
            <recipient>AreaManagers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Marine_Opportunity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Key_Individuals</fullName>
        <description>Notify Key Individuals</description>
        <protected>false</protected>
        <recipients>
            <recipient>AreaManagers</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Marine_Opportunity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_UWR_Management</fullName>
        <ccEmails>team_notification@gard.no</ccEmails>
        <description>Notify UWR Management including AM</description>
        <protected>false</protected>
        <recipients>
            <recipient>AreaManagers</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>UWRManagement</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Marine_Opportunity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Approval_Alert</fullName>
        <description>Opportunity Approval Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Oppty_Approved</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Recall_Alert</fullName>
        <description>Opportunity Recall Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_approval_submitter_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Oppty_Approval_Recalled</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Rejection_Alert</fullName>
        <description>Opportunity Rejection Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Oppty_Rejected</template>
    </alerts>
    <alerts>
        <fullName>P_I_Opportunity_Approval_Alert</fullName>
        <description>P&amp;I Opportunity Approval Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Approver_1_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Approver_2_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Approver_3_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Approver_4_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Approver_5_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Opportunity_approval_submitter_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Oppty_Approved_with_History</template>
    </alerts>
    <alerts>
        <fullName>P_I_Opportunity_Rejection_Alert</fullName>
        <description>P&amp;I Opportunity Rejection Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Approver_1_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Approver_2_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Approver_3_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Approver_4_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Approver_5_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Opportunity_approval_submitter_email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Oppty_Rejected_with_History</template>
    </alerts>
    <alerts>
        <fullName>Send_Marine_Renewal_Notification_Email</fullName>
        <description>Send Marine Renewal Notification Email</description>
        <protected>false</protected>
        <recipients>
            <field>Account_UWR_Assistant_2_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Owner_P_I_Checklist_completed_notification</template>
    </alerts>
    <alerts>
        <fullName>Send_Renewal_Notification_Email</fullName>
        <description>Send Renewal Notification Email</description>
        <protected>false</protected>
        <recipients>
            <field>Account_UWR_Assistant_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Renewal_P_I_Checklist_completed_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Append_Account_Name</fullName>
        <description>Appends Account Name to Opportunity Name</description>
        <field>Name</field>
        <formula>LEFT(Account.Name,15) &amp;&apos; (&apos;&amp; $RecordType.Name &amp;&apos;)&apos; &amp;&apos; : &apos; &amp; TEXT(Budget_Year__c) &amp; &apos; - &apos; &amp;  TEXT(Type) &amp; &apos; &apos; &amp; LEFT(Name, 50)</formula>
        <name>Append Account Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Submitter</fullName>
        <field>Opportunity_approval_submitter_email__c</field>
        <formula>$User.Email</formula>
        <name>Approval Submitter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved_for_cancellation</fullName>
        <field>Approved_for_cancellation__c</field>
        <literalValue>1</literalValue>
        <name>Approved on approval process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approver_LastModifiedBy</fullName>
        <description>stores approver&apos;s email address</description>
        <field>Approver_1_email__c</field>
        <formula>LastModifiedBy.Email</formula>
        <name>Approver - LastModifiedBy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approver_Product_Manager_Approver</fullName>
        <field>Approver_4_email__c</field>
        <formula>PM_Approver__r.Email</formula>
        <name>Approver - Product Manager Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approver_Responsible_VP</fullName>
        <field>Approver_5_email__c</field>
        <formula>Area_Manager__r.Email</formula>
        <name>Approver - Responsible VP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approver_SVP</fullName>
        <field>Approver_3_email__c</field>
        <formula>Senior_Approver__r.Email</formula>
        <name>Approver - SVP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approver_Tech_UWR_Manager</fullName>
        <field>Approver_2_email__c</field>
        <formula>Tech_UWR_Manager__r.Email</formula>
        <name>Approver - Tech UWR Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Blank_LastModifiedBy</fullName>
        <field>Approver_1_email__c</field>
        <name>Blank - LastModifiedBy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Blank_Product_Manager_Approver</fullName>
        <field>Approver_4_email__c</field>
        <name>Blank - Product Manager Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Blank_Responsible_VP</fullName>
        <field>Approver_5_email__c</field>
        <name>Blank - Responsible VP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Blank_SVP</fullName>
        <field>Approver_3_email__c</field>
        <name>Blank - SVP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Blank_Tech_UWR_Manager</fullName>
        <field>Approver_2_email__c</field>
        <name>Blank - Tech UWR Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_date_to_prior_close_date</fullName>
        <description>PRIORVALUE(CloseDate)</description>
        <field>CloseDate</field>
        <formula>PRIORVALUE(CloseDate)</formula>
        <name>Close date to prior close date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Message_Approval_started</fullName>
        <field>Approval_Message__c</field>
        <formula>&quot;The Approval process has started&quot;</formula>
        <name>Message - Approval started</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Message_Opportunity_Approved</fullName>
        <field>Approval_Message__c</field>
        <formula>&quot;Opportunity Approved&quot;</formula>
        <name>Message - Opportunity Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_biz_Upsell_P_I_close_WON_Notificatio</fullName>
        <description>When P&amp;I new biz /upsel close WON</description>
        <field>New_biz_Upsell_P_I_close_WON_Notificatio__c</field>
        <literalValue>1</literalValue>
        <name>New biz/Upsell P&amp;I close WON Notificatio</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reject_on_approval_process</fullName>
        <field>Approved_for_cancellation__c</field>
        <literalValue>0</literalValue>
        <name>Reject on approval process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_Create_Marine_Client</fullName>
        <field>Create_Marine_Client__c</field>
        <literalValue>1</literalValue>
        <name>Set Account Create Marine Client</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_ID_Builders</fullName>
        <description>Updates Opportunity.Approval_Process_ID__c</description>
        <field>Approval_Process_ID__c</field>
        <formula>&quot;04aD0000000TOjq&quot;</formula>
        <name>Set Approval Process ID Builders</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_ID_Energy</fullName>
        <description>Updates Opportunity.Approval_Process_ID__c</description>
        <field>Approval_Process_ID__c</field>
        <formula>&quot;04aD0000000TOjW&quot;</formula>
        <name>Set Approval Process ID Energy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_ID_PI_Marine</fullName>
        <description>Updates Opportunity.Approval_Process_ID__c</description>
        <field>Approval_Process_ID__c</field>
        <formula>&quot;04aD0000000TOrf&quot;</formula>
        <name>Set Approval Process ID PI/Marine</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approval_Process_ID_PI_Marine_nomail</fullName>
        <description>Updates Opportunity.Approval_Process_ID__c</description>
        <field>Approval_Process_ID__c</field>
        <formula>&quot;04aD0000000TOsn&quot;</formula>
        <name>Set Approval Process ID PI/Marine nomail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver_AASGUN</fullName>
        <field>Area_Manager__c</field>
        <lookupValue>gunnar.aasberg@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set AASGUN as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver_ANDBJO</fullName>
        <field>Senior_Approver__c</field>
        <lookupValue>baa@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set SVP Approver ANDBJO</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver_ANDBJO_Small_Craft</fullName>
        <field>Senior_Approver__c</field>
        <lookupValue>baa@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Approver ANDBJO Small Craft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver_Bjornar_Andresen</fullName>
        <description>TOMOLG 2012.12.05 Bjørnar as AM? do we need this filed update?</description>
        <field>Area_Manager__c</field>
        <lookupValue>baa@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set AM Approver Bjørnar Andresen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver_FINKNU</fullName>
        <field>Area_Manager__c</field>
        <lookupValue>knut.morten.finckenhagen@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set FINKNU as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver_KROAND</fullName>
        <description>2015.10.05 to Knut as requested
2014.03.10 Changed from Andre to Bart on Andre request</description>
        <field>PM_Approver__c</field>
        <lookupValue>knut.goderstad@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set PM Approver GODKNU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver_NILMAG</fullName>
        <field>Area_Manager__c</field>
        <lookupValue>magne.nilssen@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set NILMAG as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approver_Rolf_Thore</fullName>
        <description>TOMOLG 2012.12.05 RTR as AM? do we need this filed update?</description>
        <field>Area_Manager__c</field>
        <lookupValue>rolf.thore.roppestad@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set AM Approver Rolf Thore</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Budget_Approved</fullName>
        <field>Budget_Status__c</field>
        <literalValue>Budget Approved</literalValue>
        <name>Set Budget Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Budget_In_Progress</fullName>
        <field>Budget_Status__c</field>
        <literalValue>Budget In Progress</literalValue>
        <name>Set Budget In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Budget_Status_In_Progress</fullName>
        <field>Budget_Status__c</field>
        <literalValue>Budget In Progress</literalValue>
        <name>Set Budget Status In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Budget_Under_Approval_AM</fullName>
        <description>Sets the Budget Status field once the opportunity has been submitted for approval</description>
        <field>Budget_Status__c</field>
        <literalValue>Under Approval - AM</literalValue>
        <name>Set Budget Under Approval AM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Budget_Under_Approval_HUW</fullName>
        <field>Budget_Status__c</field>
        <literalValue>Under Approval - HUWR</literalValue>
        <name>Set Budget Under Approval HUW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_FREBJO_as_AM_approver</fullName>
        <field>Area_Manager__c</field>
        <lookupValue>bjorn.fremmerlid@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set FREBJO as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_FURTOR_as_AM_approver</fullName>
        <field>Area_Manager__c</field>
        <lookupValue>tore.furnes@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set FURTOR as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_GODKNU_as_AM_approver</fullName>
        <field>Area_Manager__c</field>
        <lookupValue>knut.goderstad@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set GODKNU as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_JAYTER_as_AM</fullName>
        <field>Area_Manager__c</field>
        <lookupValue>terri.lynn.jay@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set JAYTER as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_LAIIAI_as_approver</fullName>
        <field>Area_Manager__c</field>
        <lookupValue>iain.laird@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set LAIIAI as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_LOCSID_as_approver</fullName>
        <field>Area_Manager__c</field>
        <lookupValue>andre.kroneberg@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set LOCSID as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MERBAR_as_AM_approver</fullName>
        <field>Area_Manager__c</field>
        <lookupValue>bart.mertens@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set MERBAR as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_NORTHO_as_approver</fullName>
        <description>Set Reidun as AM in the opp</description>
        <field>Area_Manager__c</field>
        <lookupValue>reidun.haahjem@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set HAAREI as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_OLSESP_as_AM_approver</fullName>
        <field>Area_Manager__c</field>
        <lookupValue>espen.olsen@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set OLSESP as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_Expiry_Date</fullName>
        <field>Expiry_Date__c</field>
        <formula>CloseDate</formula>
        <name>Set Opp Expiry Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_Approved_Budget_Amount</fullName>
        <field>Budget_Amount__c</field>
        <formula>Budget_Premium__c</formula>
        <name>Set Opportunity Approved Budget Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PETAUD_as_approver</fullName>
        <field>Area_Manager__c</field>
        <lookupValue>audun.pettersen@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set PETAUD as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_PM_Approver_BRETON</fullName>
        <field>PM_Approver__c</field>
        <lookupValue>tonje.breivik@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set PM Approver BRETON</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ROPROL_Approver</fullName>
        <field>Senior_Approver__c</field>
        <lookupValue>rolf.thore.roppestad@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set ROPROL as SVP Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SVP_Approver_KROAND</fullName>
        <field>Senior_Approver__c</field>
        <lookupValue>andre.kroneberg@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set SVP Approver KROAND</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SVP_Approver_NILMAG</fullName>
        <field>Senior_Approver__c</field>
        <lookupValue>magne.nilssen@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set SVP Approver NILMAG</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_SVP_Approver_PETAUD</fullName>
        <field>Senior_Approver__c</field>
        <lookupValue>audun.pettersen@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set SVP Approver PETAUD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_Budget_Set</fullName>
        <field>StageName</field>
        <literalValue>Budget Set</literalValue>
        <name>Set Stage Budget Set</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_Risk_Evaluation</fullName>
        <field>StageName</field>
        <literalValue>Risk Evaluation</literalValue>
        <name>Set Stage Risk Evaluation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Marine_email_from_CT_team</fullName>
        <field>CT_Team_Marine_email__c</field>
        <formula>Account.Gard_Team__r.Marine_Email__c</formula>
        <name>Update Marine email from CT team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_Stage_Risk_Evaluation</fullName>
        <field>StageName</field>
        <literalValue>Quote</literalValue>
        <name>Update Oppty Stage - Risk Evaluation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_PI_email_from_CT_team</fullName>
        <field>CT_Team_PI_email__c</field>
        <formula>Account.Gard_Team__r.P_I_email__c</formula>
        <name>Update PI email from CT team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_Closed_Lost</fullName>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Update Stage - Closed Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_Closed_Other</fullName>
        <field>StageName</field>
        <literalValue>Closed Other</literalValue>
        <name>Update Stage - Closed Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_Declined_By_Gard</fullName>
        <field>StageName</field>
        <literalValue>Closed Declined</literalValue>
        <name>Update Stage - Declined By Gard</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_Quote</fullName>
        <field>StageName</field>
        <literalValue>Quote</literalValue>
        <name>Update Stage - Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_Risk_Evaluation</fullName>
        <field>StageName</field>
        <literalValue>Risk Evaluation</literalValue>
        <name>Update Stage - Risk Evaluation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_Under_Approval</fullName>
        <field>StageName</field>
        <literalValue>Under Approval</literalValue>
        <name>Update Stage - Under Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Submitted_Date</fullName>
        <field>Approval_Submitted__c</field>
        <formula>TODAY()</formula>
        <name>Update Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_UWR_Assistant_2_Email</fullName>
        <description>Not currently possible to send email alert to related object user (i.e. user lookup on parent account). Instead, this update sets the email field locally and the email alert references the email field.</description>
        <field>Account_UWR_Assistant_2_Email__c</field>
        <formula>Account.U_W_Assistant_2__r.Email</formula>
        <name>Update UWR Assistant 2 Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_UWR_Assistant_Email</fullName>
        <description>Not currently possible to send email alert to related object user (i.e. user lookup on parent account). Instead, this update sets the email field locally and the email alert references the email field.</description>
        <field>Account_UWR_Assistant_Email__c</field>
        <formula>Account.UW_Assistant_1__r.Email</formula>
        <name>Update UWR Assistant Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Close NOT won Marine Renewal</fullName>
        <actions>
            <name>CT_Marine_notification_not_WON_Renewal_opp</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Declined,Closed Lost,Closed Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marine</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Renewal</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close NOT won P%26I Charterers Renewal</fullName>
        <actions>
            <name>CT_and_Charterers_P_I_notification_not_WON_opp</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Declined,Closed Lost,Closed Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>P&amp;I</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Renewal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Charterers &amp; Traders</value>
        </criteriaItems>
        <description>for Charterers MA</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close NOT won P%26I Renewal</fullName>
        <actions>
            <name>CT_P_I_notification_not_WON_opp</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Declined,Closed Lost,Closed Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>P&amp;I</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Renewal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>notEqual</operation>
            <value>Charterers &amp; Traders</value>
        </criteriaItems>
        <description>Excluding Charterers MA</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Close date no changed when close Won</fullName>
        <actions>
            <name>Close_date_to_prior_close_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When an opportunity is closed won, we set the close date back to the original value, not to the day it is closed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Marine Opportunity Closed</fullName>
        <actions>
            <name>Send_Marine_Renewal_Notification_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_UWR_Assistant_2_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeName__c</field>
            <operation>equals</operation>
            <value>Marine</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Renewal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>25 10 2016. Replaced by https://eu1.salesforce.com/01QD0000000Y6gP TOMOLG
Emails Gard team when Marine opportunity is closed.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Marine Opportunity Won</fullName>
        <actions>
            <name>Set_Account_Create_Marine_Client</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marine,Builders Risk,Energy</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sets the &apos;Create Marine Client&apos; on the account when a Marine, Energy or MBR opp is won.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Quote - Update Opportunity</fullName>
        <actions>
            <name>Update_Oppty_Stage_Risk_Evaluation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This workflow updates the parent Opportunity Stage when a new Quote record is added. If Quotes are deleted the workflow is not executed.
AND( ISCHANGED( Quote_Count__c ) , PRIORVALUE( Quote_Count__c ) &lt; Quote_Count__c )</description>
        <formula>false</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New biz Energy Opportunity Closed</fullName>
        <actions>
            <name>New_biz_Upsell_Energy_close_WON_Notification_to_Head_claims_operation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Marine_email_from_CT_team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>equals</operation>
            <value>Energy New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business,Up Sell</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Budget_Year__c</field>
            <operation>greaterOrEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <description>Emails Head of Energy Claims operation when Energy opportunity is closed WON.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New biz MBR Opportunity Closed</fullName>
        <actions>
            <name>New_biz_Upsell_MBR_close_WON_Notification_to_CT_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>equals</operation>
            <value>Builders Risk New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business,Up Sell</value>
        </criteriaItems>
        <description>Email to CT when MBR opportunity is closed WON.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New biz Marine Opportunity Closed</fullName>
        <actions>
            <name>New_biz_Upsell_Marine_close_WON_Notification_to_Head_claims_operation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Marine_email_from_CT_team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeName__c</field>
            <operation>equals</operation>
            <value>Marine,Builders Risk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business,Up Sell</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>notEqual</operation>
            <value>Small Craft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Budget_Year__c</field>
            <operation>greaterOrEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.M_E_Client_Flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Emails Head of Marine Claims operation when Marine and MBR opportunity is closed WON.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New biz P%26I Charterers Opportunity Closed</fullName>
        <actions>
            <name>New_biz_Upsell_P_I_Charterers_close_WON_Notification_to_Head_claims_operation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_PI_email_from_CT_team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeName__c</field>
            <operation>equals</operation>
            <value>P_I</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business,Up Sell</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.P_I_Member_Flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Budget_Year__c</field>
            <operation>greaterOrEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>equals</operation>
            <value>Charters and Traders</value>
        </criteriaItems>
        <description>Emails Head of P&amp;I Claims operation when P&amp;I opportunity is closed WON.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New biz P%26I Defence Opportunity Closed</fullName>
        <actions>
            <name>New_biz_Upsell_P_I_Defence_close_WON_Notification_to_Head_claims_operation</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeName__c</field>
            <operation>equals</operation>
            <value>P_I</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business,Up Sell</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.P_I_Member_Flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Budget_Year__c</field>
            <operation>greaterOrEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Defence_covers__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <description>Emails Head of P&amp;I Claims operation when P&amp;I opportunity is closed WON.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New biz P%26I MOU Opportunity Closed</fullName>
        <actions>
            <name>New_biz_Upsell_P_I_MOU_close_WON_Notification_to_Head_claims_operation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_PI_email_from_CT_team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeName__c</field>
            <operation>equals</operation>
            <value>P_I</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business,Up Sell</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.P_I_Member_Flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Budget_Year__c</field>
            <operation>greaterOrEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>equals</operation>
            <value>MOUs</value>
        </criteriaItems>
        <description>Emails Head of P&amp;I Claims operation when P&amp;I opportunity is closed WON.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New biz P%26I Opportunity Closed</fullName>
        <actions>
            <name>New_biz_Upsell_P_I_close_WON_Notificatio</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeName__c</field>
            <operation>equals</operation>
            <value>P_I</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business,Up Sell</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.P_I_Member_Flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Budget_Year__c</field>
            <operation>greaterOrEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <description>Emails Head of P&amp;I Claims operation when P&amp;I opportunity is closed WON.
New biz and upsell when not P&amp;I Member. FLAG set. them email is sent once the risk is in GIC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New biz P%26I Opportunity Closedv2</fullName>
        <actions>
            <name>New_biz_Upsell_P_I_close_WON_Notification_to_Head_claims_operation</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_PI_email_from_CT_team</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeName__c</field>
            <operation>equals</operation>
            <value>P_I</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>New Business,Up Sell</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.P_I_Member_Flag__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Budget_Year__c</field>
            <operation>greaterOrEqual</operation>
            <value>2014</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>notEqual</operation>
            <value>Charters and Traders,MOUs</value>
        </criteriaItems>
        <description>Emails Head of P&amp;I Claims operation when P&amp;I opportunity is closed WON.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>P%26I Opportunity Closed</fullName>
        <actions>
            <name>Send_Renewal_Notification_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_UWR_Assistant_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeName__c</field>
            <operation>equals</operation>
            <value>P_I</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Renewal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.IsWon</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Emails Gard team when P&amp;I opportunity is closed.
26 01 2015 This was triggering an email notification to the P&amp;I assistant, but it is enough with the notifications provided when parcial checklist completed: Owners, MOU or Char</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver ANDBJO non Small Craft</fullName>
        <actions>
            <name>Set_Approver_ANDBJO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Senior_Approver__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Criteria__c</field>
            <operation>excludes</operation>
            <value>Self Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>notEqual</operation>
            <value>Small Craft</value>
        </criteriaItems>
        <description>2014.12.16 Separate rule for Small Craft
2013.12.20 All teams go to ANDBJO
CharterersTraders is Bjørnar, also Small Craft Nordic
2012.12.12 TOMOLG New market areas incorporated. I have not removed the old market areas yet.  (see issue 345)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver ANDBJOv2</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Senior_Approver__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Criteria__c</field>
            <operation>excludes</operation>
            <value>Self Approval</value>
        </criteriaItems>
        <description>2013.12.20 All teams go to Bjørnar</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver Energy</fullName>
        <actions>
            <name>Set_Approver_AASGUN</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SVP_Approver_NILMAG</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR (3 AND 4) OR (3 AND 5)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>equals</operation>
            <value>Energy Renewal,Energy New Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Criteria__c</field>
            <operation>notEqual</operation>
            <value>Self Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Area_Manager_Changed__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Exploration &amp; Production</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Energy</value>
        </criteriaItems>
        <description>2015.10.05 New UWR org
2012.12.12 New market area  (see issue 345)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver LAIIAI</fullName>
        <actions>
            <name>Set_LAIIAI_as_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>&quot;Americas, Middle East and London&quot;</value>
        </criteriaItems>
        <description>2015 DEACTIVATED, business need to be approved in Norway</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver MBR</fullName>
        <actions>
            <name>Set_Approver_FINKNU</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SVP_Approver_NILMAG</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR (3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>equals</operation>
            <value>Builders Risk New Business,Builders Risk Renewal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Criteria__c</field>
            <operation>excludes</operation>
            <value>Self Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Area_Manager_Changed__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Marine Builders Risks</value>
        </criteriaItems>
        <description>2015.10.05 New UWR org</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver MOU</fullName>
        <actions>
            <name>Set_FURTOR_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SVP_Approver_NILMAG</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>equals</operation>
            <value>MOUs,Ship Owners</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Offshore</value>
        </criteriaItems>
        <description>2015.10.05 New UWR org
2012.12.12 New market area  (see issue 345)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver MOU v2</fullName>
        <actions>
            <name>Set_FURTOR_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SVP_Approver_NILMAG</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>equals</operation>
            <value>MOUs</value>
        </criteriaItems>
        <description>Only based on risk criteria</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver NTSC for SC and Shipowners</fullName>
        <actions>
            <name>Set_PM_Approver_BRETON</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>equals</operation>
            <value>Small Craft,Ship Owners</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Criteria__c</field>
            <operation>includes</operation>
            <value>Non Standard T&amp;Cs</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver Nordic Marine</fullName>
        <actions>
            <name>Set_NORTHO_as_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SVP_Approver_PETAUD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Nordic and North America</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marine</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>notEqual</operation>
            <value>Small Craft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Country_Code__c</field>
            <operation>notEqual</operation>
            <value>USA,CAN,BMU,BHS,CRI</value>
        </criteriaItems>
        <description>Updated Requested by Knut Go. 3/5/2016 as Nordic and NA AM</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver Nordic NA Marine</fullName>
        <actions>
            <name>Set_OLSESP_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SVP_Approver_PETAUD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Nordic and North America</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Marine</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>notEqual</operation>
            <value>Small Craft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Country_Code__c</field>
            <operation>equals</operation>
            <value>USA,CAN,BMU,BHS,CRI</value>
        </criteriaItems>
        <description>Requested by Knut Go. 3/5/2016 as Nordic and NA AM</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver Nordic P%26I and SC</fullName>
        <actions>
            <name>Set_GODKNU_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SVP_Approver_PETAUD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2) OR (1 AND 3)</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Nordic and North America</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Marine</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>equals</operation>
            <value>Small Craft</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver ROPROL</fullName>
        <actions>
            <name>Set_ROPROL_Approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 2 OR 3 OR 4 OR 5) AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Asia East Incl. Japan</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Southern Europe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Northern Europe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Asia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Northern Europe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Senior_Approver__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Criteria__c</field>
            <operation>excludes</operation>
            <value>Self Approval</value>
        </criteriaItems>
        <description>2012.11-06 Not charterers for RTR, so that is why NOrthern Europe twice
2012.11.21 Not Small craft
2012.12.12 Asia Market area incorporated. Not removed the old market areas (see issue 345)</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver Small Craft</fullName>
        <actions>
            <name>Set_SVP_Approver_PETAUD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 3 AND ((4 AND (2 AND 7)) OR (5 AND (2 AND 6 AND 7)))</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Senior_Approver__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Criteria__c</field>
            <operation>excludes</operation>
            <value>Self Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>equals</operation>
            <value>Small Craft</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeName__c</field>
            <operation>equals</operation>
            <value>P_I</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeName__c</field>
            <operation>equals</operation>
            <value>Marine</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Criteria__c</field>
            <operation>excludes</operation>
            <value>Vessel &gt; 1500 GT</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Criteria__c</field>
            <operation>excludes</operation>
            <value>RA &lt; 90%</value>
        </criteriaItems>
        <description>2014.12.16 Separate rule for Small Craft</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for AMEL</fullName>
        <actions>
            <name>Set_LAIIAI_as_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SVP_Approver_PETAUD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>&quot;Americas, Middle East and London&quot;</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for Asia</fullName>
        <actions>
            <name>Set_LOCSID_as_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SVP_Approver_KROAND</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Asia</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for NE</fullName>
        <actions>
            <name>Set_FREBJO_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SVP_Approver_PETAUD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Northern Europe</value>
        </criteriaItems>
        <description>Set Approver for Northern Europe MA</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for SE</fullName>
        <actions>
            <name>Set_MERBAR_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SVP_Approver_PETAUD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Gard_Market_Area__c</field>
            <operation>equals</operation>
            <value>Southern Europe</value>
        </criteriaItems>
        <description>Set Approvers for Southern Europe MA</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Budget Status In Progress</fullName>
        <actions>
            <name>Set_Budget_Status_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName_RO__c</field>
            <operation>equals</operation>
            <value>Renewable Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Budget_Status__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Sets the budget status field to In Progress for Budget opportunities</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Charterers Approver</fullName>
        <actions>
            <name>Set_JAYTER_as_AM</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_SVP_Approver_NILMAG</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>equals</operation>
            <value>Charters and Traders</value>
        </criteriaItems>
        <description>2015.10.05 New UWR org
2013.01.31 No Terry but Magne as default. Following new req from Andre k</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Closed Declined</fullName>
        <actions>
            <name>Update_Stage_Declined_By_Gard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Reason_Lost_Declined__c</field>
            <operation>equals</operation>
            <value>Outside UWR requirements,Nothing to renew,Non-renewal,Non-payment of premium,Our quality assessment</value>
        </criteriaItems>
        <description>Depending on the reason lost field value set the opp stage to Closed Declined</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Closed Lost</fullName>
        <actions>
            <name>Update_Stage_Closed_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Reason_Lost_Declined__c</field>
            <operation>equals</operation>
            <value>Terms and conditions,Service,Price</value>
        </criteriaItems>
        <description>Depending on the reason lost field value set the opp stage to Closed Lost</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Closed Other</fullName>
        <actions>
            <name>Update_Stage_Closed_Other</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Reason_Lost_Declined__c</field>
            <operation>equals</operation>
            <value>Other</value>
        </criteriaItems>
        <description>Depending on the reason lost field value set the opp stage to Closed Other</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Expiry Date on Creation</fullName>
        <actions>
            <name>Set_Opp_Expiry_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Expiry_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Sets the Expiry date to be the Close date if the expiry date has not been specified by the user.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Name</fullName>
        <actions>
            <name>Append_Account_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow appends the company name to the Opportunity Name if it is not already included</description>
        <formula>NOT(CONTAINS(Name, LEFT(Account.Name,15)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportuntity Close declined if approved for cancellation</fullName>
        <actions>
            <name>Update_Stage_Declined_By_Gard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Approved_for_cancellation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Quote</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Criteria__c</field>
            <operation>equals</operation>
            <value>Non-renewal</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set SVP Approver NO SC and no Selfapproval</fullName>
        <actions>
            <name>Set_Approver_ANDBJO</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Senior_Approver__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Approval_Criteria__c</field>
            <operation>excludes</operation>
            <value>Self Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Business_Type__c</field>
            <operation>notEqual</operation>
            <value>Small Craft</value>
        </criteriaItems>
        <description>Replaced by separate rules for small craft and non small craft</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
