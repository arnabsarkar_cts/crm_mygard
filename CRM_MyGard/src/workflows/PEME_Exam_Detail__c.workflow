<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>FUInvoiceDateOFV</fullName>
        <field>Date_of_Final_Verification__c</field>
        <formula>DATEVALUE(NOW())</formula>
        <name>FUInvoiceDateOFV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Invoice__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FUStatusApproved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>FUStatusApproved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FUStatusRejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected-Need Input</literalValue>
        <name>FUStatusRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateApprovalDate</fullName>
        <description>update the ApprovalRejectionDate__c with approval date field</description>
        <field>ApprovalRejectionDate__c</field>
        <formula>Today()</formula>
        <name>UpdateApprovalDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRejectionDate</fullName>
        <description>update the ApprovalRejectionDate__c with rejection date field</description>
        <field>ApprovalRejectionDate__c</field>
        <formula>Today()</formula>
        <name>UpdateRejectionDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
