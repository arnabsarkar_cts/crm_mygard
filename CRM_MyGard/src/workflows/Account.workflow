<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Address_Change_Acknowledgement</fullName>
        <description>Account Address Change Acknowledgement</description>
        <protected>false</protected>
        <recipients>
            <field>Initial_Submitter__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GARD_Templates/Company_Address_Change_Acknowledgement</template>
    </alerts>
    <alerts>
        <fullName>Account_Address_Change_Approved</fullName>
        <description>Account Address Change Approved</description>
        <protected>false</protected>
        <recipients>
            <field>Initial_Submitter__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GARD_Templates/Company_Address_Change_Approved</template>
    </alerts>
    <alerts>
        <fullName>Account_Address_Change_Rejected</fullName>
        <description>Account Address Change Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Initial_Submitter__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GARD_Templates/Company_Address_Change_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Account_Deactivation_Approved</fullName>
        <description>Account Deactivation Approved</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Approval_Process_Submitter__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Company_Deactivation_Approved</template>
    </alerts>
    <alerts>
        <fullName>Account_Deactivation_Rejected</fullName>
        <description>Account Deactivation Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Approval_Process_Submitter__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Company_Deactivation_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Account_Name_Changed</fullName>
        <description>Account Name Changed</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Company_Name_Change_Notification</template>
    </alerts>
    <alerts>
        <fullName>Account_Role_Change_Approved</fullName>
        <description>Account Role Change Approved</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Approval_Process_Submitter__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Company_Role_Change_Approved</template>
    </alerts>
    <alerts>
        <fullName>Account_Role_Change_Rejected</fullName>
        <description>Account Role Change Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Approval_Process_Submitter__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Company_Role_Change_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Change_Company_Information</fullName>
        <description>Change Company Information</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GARD_Templates/MyGard_Company_address_change</template>
    </alerts>
    <alerts>
        <fullName>Company_Owner_Assignment</fullName>
        <description>Company Owner Assignment</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderAddress>no-reply@gard.no</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>GARD_Templates/Company_Owner_Assignment</template>
    </alerts>
    <alerts>
        <fullName>ESP_created</fullName>
        <description>ESP created</description>
        <protected>false</protected>
        <recipients>
            <recipient>clarice.tan@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olga.tome@gard.no</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>KYC_templates/ESP_Created</template>
    </alerts>
    <alerts>
        <fullName>Inform_Area_Manager_of_New_Client</fullName>
        <description>Inform Area Manager of New Client</description>
        <protected>false</protected>
        <recipients>
            <field>Area_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Prospect_Client_Created</template>
    </alerts>
    <alerts>
        <fullName>Membership_Expired_Email</fullName>
        <description>Membership Expired Email</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Member_Expiry</template>
    </alerts>
    <alerts>
        <fullName>New_Prospect_Company_Created</fullName>
        <description>New Prospect Company Created</description>
        <protected>false</protected>
        <recipients>
            <field>Area_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Prospect_Client_Created</template>
    </alerts>
    <alerts>
        <fullName>New_biz_Upsell_P_I_close_WON_Notification_to_Head_claims_operationv2</fullName>
        <description>New biz/Upsell P&amp;I close WON Notification to Head claims operation and Accounting</description>
        <protected>false</protected>
        <recipients>
            <recipient>geir.sandnes@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>malin.gustavi@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olga.tome@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vivi.sandsten@gard.no</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/New_biz_P_I_WON_notification</template>
    </alerts>
    <alerts>
        <fullName>Outbound_Sync_Failure_Alert_Account</fullName>
        <description>Outbound Sync Failure Alert - Account</description>
        <protected>false</protected>
        <recipients>
            <recipient>Outbound_Sync_Failure_Notification_Users</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>System_Administration/Outbound_Sync_Failure_Account</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Status</fullName>
        <field>Request_Status__c</field>
        <literalValue>Submit for Approval</literalValue>
        <name>Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Ischanged_To_false</fullName>
        <description>Change Ischanged To false</description>
        <field>IsChanged__c</field>
        <literalValue>0</literalValue>
        <name>Change Ischanged To false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_member_approved_date</fullName>
        <field>Membership_Approved_Date__c</field>
        <name>Clear member approved date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Company_Status</fullName>
        <field>Company_Status__c</field>
        <literalValue>Inactive</literalValue>
        <name>Company Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Company_Status_to_Active</fullName>
        <field>Company_Status__c</field>
        <literalValue>Active</literalValue>
        <name>Company Status to Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Disable_email_notif_not_to_Head_Claims</fullName>
        <field>New_biz_Upsell_P_I_close_WON_Notificatio__c</field>
        <literalValue>0</literalValue>
        <name>Disable email notif not to Head Claims</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IsRejected</fullName>
        <description>To check whether company role is rejcted or not</description>
        <field>IsRejected__c</field>
        <literalValue>1</literalValue>
        <name>IsRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDM_Set_Billing_Address_Sync_Date</fullName>
        <field>Sync_Billing_Address_Date__c</field>
        <name>MDM Set Billing Address Sync Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDM_Set_Billing_Address_Sync_In_Progress</fullName>
        <field>Billing_Address_Sync_Status__c</field>
        <literalValue>Sync In Progress</literalValue>
        <name>MDM Set Billing Address Sync In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDM_Set_ShippingAddress_Sync_In_Progress</fullName>
        <field>Shipping_Address_Sync_Status__c</field>
        <literalValue>Sync In Progress</literalValue>
        <name>MDM Set ShippingAddress Sync In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDM_Set_Shipping_Address_Sync_Date</fullName>
        <field>Sync_Shipping_Address_Date__c</field>
        <name>MDM Set Shipping Address Sync Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDM_Set_Sync_Date</fullName>
        <field>Sync_Date__c</field>
        <name>MDM Set Sync Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MDM_Set_Sync_Status_In_Progress</fullName>
        <field>Synchronisation_Status__c</field>
        <literalValue>Sync In Progress</literalValue>
        <name>MDM Set Sync Status In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>P_I_Member_Ever_Flag</fullName>
        <field>P_I_Member_Ever__c</field>
        <literalValue>1</literalValue>
        <name>P&amp;I Member Ever Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_Update_Client</fullName>
        <description>Record Type Update Client</description>
        <field>RecordTypeId</field>
        <lookupValue>Client</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type Update Client</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_Updated_To_Bank</fullName>
        <description>Record Type Updated To Bank</description>
        <field>RecordTypeId</field>
        <lookupValue>Bank</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type Updated To Bank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_Updated_To_Broker</fullName>
        <description>Record Type Updated To Broker</description>
        <field>RecordTypeId</field>
        <lookupValue>Broker</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type Updated To Broker</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_Updated_To_ESP</fullName>
        <description>Record Type Updated To ESP</description>
        <field>RecordTypeId</field>
        <lookupValue>External_Service_Provider</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type Updated To ESP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_Updated_To_Insurance_Company</fullName>
        <description>Record Type Updated To Insurance Company</description>
        <field>RecordTypeId</field>
        <lookupValue>Insurance_Company</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type Updated To Insurance Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_Updated_To_Other</fullName>
        <description>Record Type Updated To Other</description>
        <field>RecordTypeId</field>
        <lookupValue>Other</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type Updated To Other</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Remove_on_risk</fullName>
        <field>On_Risk__c</field>
        <literalValue>0</literalValue>
        <name>Remove on risk?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Request_Status</fullName>
        <field>Request_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Request Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Risk_assessment_HIGH</fullName>
        <description>Set KYC Risk assessment to high</description>
        <field>KYC_Risk_assesment__c</field>
        <literalValue>High</literalValue>
        <name>Risk assessment HIGH</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Risk_assessment_LOW</fullName>
        <field>KYC_Risk_assesment__c</field>
        <literalValue>Low</literalValue>
        <name>Risk assessment LOW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Risk_assessment_MEDIUM</fullName>
        <field>KYC_Risk_assesment__c</field>
        <literalValue>Medium</literalValue>
        <name>Risk assessment MEDIUM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SanctionsChkBx</fullName>
        <description>Checkbox indicating if company is on the BvD screening list.</description>
        <field>Automatic_Sanctions_Screening__c</field>
        <literalValue>0</literalValue>
        <name>SanctionsChkBx</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_AasbergGunnar_as_AM_approver</fullName>
        <description>Set Gunnar Aasberg as an AM when marketing area is &apos;Energy&apos;</description>
        <field>Area_Manager__c</field>
        <lookupValue>gunnar.aasberg@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set AasbergGunnar  as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_Deleted_Flag</fullName>
        <field>Deleted__c</field>
        <literalValue>1</literalValue>
        <name>Set Account Deleted Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_By</fullName>
        <field>Approved_By__c</field>
        <formula>$User.FirstName &amp;&apos; &apos; &amp;  $User.LastName</formula>
        <name>Set Approved By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_date</fullName>
        <field>Membership_Approved_Date__c</field>
        <formula>TODAY()</formula>
        <name>Set Approved date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_date_to_null</fullName>
        <field>Membership_Approved_Date__c</field>
        <name>Set Approved date to null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Awaiting_Approval</fullName>
        <field>Awaiting_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Set Awaiting Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Bank_Flag</fullName>
        <field>Role_Bank__c</field>
        <literalValue>1</literalValue>
        <name>Set Bank Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Bank_Role_Approved</fullName>
        <field>Role_Status_Bank__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Bank Role Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Bank_Role_Rejected</fullName>
        <field>Role_Status_Bank__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Bank Role Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Broker_Flag</fullName>
        <field>Role_Broker__c</field>
        <literalValue>1</literalValue>
        <name>Set Broker Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Broker_Reinsurance_Flag</fullName>
        <field>Role_Reinsurance_Broker__c</field>
        <literalValue>1</literalValue>
        <name>Set Broker Reinsurance Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Broker_Role_Approved</fullName>
        <field>Role_Status_Broker__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Broker Role Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Broker_Role_Rejected</fullName>
        <field>Role_Status_Broker__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Broker Role Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Client_Role_Approved</fullName>
        <field>Role_Status_Client__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Client Role Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Client_Role_Rejected</fullName>
        <field>Role_Status_Client__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Client Role Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Company_Approved</fullName>
        <field>Membership_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Company Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Correspondent_Role_Approved</fullName>
        <field>Role_Status_Correspondent__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Correspondent Role Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Correspondent_Role_Rejected</fullName>
        <field>Role_Status_Correspondent__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Correspondent Role Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_DM_Folder_Flag</fullName>
        <field>DM_Folder_Created__c</field>
        <literalValue>1</literalValue>
        <name>Set DM Folder Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ESP_Flag</fullName>
        <field>Role_ESP__c</field>
        <literalValue>1</literalValue>
        <name>Set ESP Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ESP_Role_Approved</fullName>
        <field>Role_Status_External_Service_Provider__c</field>
        <literalValue>Approved</literalValue>
        <name>Set ESP Role Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ESP_Role_Rejected</fullName>
        <field>Role_Status_External_Service_Provider__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set ESP Role Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_FremmerlidBjorn_as_AM_approver</fullName>
        <description>Set Bjorn  Fremmerlid as an area manager when market area is Northern Europe</description>
        <field>Area_Manager__c</field>
        <lookupValue>bjorn.fremmerlid@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set FremmerlidBjorn as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_FurnesTore_as_AM_approver</fullName>
        <description>Set Tore Furnes as an AM when marketing area is &apos;Offshore&apos;</description>
        <field>Area_Manager__c</field>
        <lookupValue>tore.furnes@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set FurnesTore as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_GoderstadKnut_as_AM_approver</fullName>
        <description>Set Knut Goderstad as AM when marketing area is Nordic and North America and country code doesn&apos;t equal to USA,CAN,BMU,BHS,CRI</description>
        <field>Area_Manager__c</field>
        <lookupValue>knut.goderstad@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set GoderstadKnut  as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Inactive_Status</fullName>
        <field>Company_Status__c</field>
        <literalValue>Inactive</literalValue>
        <name>Set Inactive Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Insurance_Company_Flag</fullName>
        <field>Role_Insurance_Company__c</field>
        <literalValue>1</literalValue>
        <name>Set Insurance Company Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Insurance_Company_Role_Approved</fullName>
        <field>Role_Status_Insurance_Company__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Insurance Company Role Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Insurance_Company_Role_Rejected</fullName>
        <field>Role_Status_Insurance_Company__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Insurance Company Role Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Investments_Flag</fullName>
        <field>Role_Investments__c</field>
        <literalValue>1</literalValue>
        <name>Set Investments Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Investments_Role_Approved</fullName>
        <field>Role_Status_Investments__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Investments Role Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Investments_Role_Rejected</fullName>
        <field>Role_Status_Investments__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Investments Role Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Jay_Ter_as_AM_approver</fullName>
        <description>Set Sid Lock as an area manager when market area is &apos;Charterers and Traders&apos;</description>
        <field>Area_Manager__c</field>
        <lookupValue>terri.lynn.jay@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Jay Ter as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_KnutMortenFinckenhagen_as_AM</fullName>
        <description>Knut Morten Finckenhagen as an area manager when marketing area is &apos;Marine Builders Risks&apos;</description>
        <field>Area_Manager__c</field>
        <lookupValue>knut.morten.finckenhagen@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set KnutMortenFinckenhagen as AM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_LairdIain_as_AM_approver</fullName>
        <description>Set Iain Laird as an area manager when market area is &apos;Americas, Middle East and London&apos;</description>
        <field>Area_Manager__c</field>
        <lookupValue>iain.laird@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set LairdIain as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lock_Sid_as_AM_approver</fullName>
        <description>Set Sid Lock as an area manager when market area is &apos;Asia&apos;</description>
        <field>Area_Manager__c</field>
        <lookupValue>andre.kroneberg@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Lock Sid as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Member_status_to_Expired</fullName>
        <field>Membership_Status__c</field>
        <literalValue>Expired</literalValue>
        <name>Set Member status to Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Member_status_to_Ready_for_Approval</fullName>
        <field>Membership_Status__c</field>
        <literalValue>Ready for Approval</literalValue>
        <name>Set Member status to Ready for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Membership_Status</fullName>
        <field>Membership_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Membership Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MertensBart_as_AM_approver</fullName>
        <description>Set Bart  Mertens as an AM when marketing area is &apos;SE&apos;</description>
        <field>Area_Manager__c</field>
        <lookupValue>bart.mertens@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set MertensBart as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_OlsenEspen_as_AM_approver</fullName>
        <description>Set Espen Olsen as an AM when marketing area is Nordic and North America and country code equals to &apos;USA,CAN,BMU,BHS,CRI&apos;</description>
        <field>Area_Manager__c</field>
        <lookupValue>espen.olsen@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set OlsenEspen as AM approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Other_Flag</fullName>
        <field>Role_Other__c</field>
        <literalValue>1</literalValue>
        <name>Set Other Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Other_Role_Approved</fullName>
        <field>Role_Status_Other__c</field>
        <literalValue>Approved</literalValue>
        <name>Set Other Role Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Other_Role_Rejected</fullName>
        <field>Role_Status_Other__c</field>
        <literalValue>Rejected</literalValue>
        <name>Set Other Role Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_to_MD_of_Lingard</fullName>
        <field>OwnerId</field>
        <lookupValue>graham.everard@lingard.bm</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Owner to MD of Lingard</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_to_Mgr_UWR_Operations</fullName>
        <field>OwnerId</field>
        <lookupValue>ingebjorg.eliassen@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Owner to Mgr. UWR Operations</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_to_Sen_Mgr_Accounting</fullName>
        <field>OwnerId</field>
        <lookupValue>jorunn.bjorkli@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Owner to Sen. Mgr. Accounting</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_to_VP_Claims_Operations</fullName>
        <field>OwnerId</field>
        <lookupValue>geir.sandnes@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Owner to VP Claims Operations</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Owner_to_VP_UWR_Operations</fullName>
        <field>OwnerId</field>
        <lookupValue>olga.tome@gard.no</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Owner to VP UWR Operations</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Previous_Approved_Date</fullName>
        <field>Previous_Member_Approved_Date__c</field>
        <formula>Membership_Approved_Date__c</formula>
        <name>Set Previous Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Previous_True</fullName>
        <field>Previous_or_Currently_Insured_by_Gard__c</field>
        <literalValue>1</literalValue>
        <name>Set Previous True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Reinsurance_Provider_Flag</fullName>
        <field>Role_Reinsurance_Provider__c</field>
        <literalValue>1</literalValue>
        <name>Set Reinsurance Provider Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Role_Change_Approved</fullName>
        <field>Role_Change_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Set Role Change Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Role_Client_Flag</fullName>
        <field>Role_Client__c</field>
        <literalValue>1</literalValue>
        <name>Set Role Client Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Role_Correspondent_Flag</fullName>
        <field>Role_Correspondent__c</field>
        <literalValue>1</literalValue>
        <name>Set Role Correspondent Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Role_Investments_Flag</fullName>
        <field>Role_Investments__c</field>
        <literalValue>1</literalValue>
        <name>Set Role Investments Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Role_Other_Rating_Company</fullName>
        <field>Role_Other_Rating_Company__c</field>
        <literalValue>1</literalValue>
        <name>Set Role - Other Rating Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Role_Sync_Status_Failed</fullName>
        <field>Roles_Sync_Status__c</field>
        <literalValue>Sync Failed</literalValue>
        <name>Set Role Sync Status Failed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Role_Sync_Status_In_Progress</fullName>
        <field>Roles_Sync_Status__c</field>
        <literalValue>Sync In Progress</literalValue>
        <name>Set Role Sync Status In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Role_Sync_Status_Synchronised</fullName>
        <field>Roles_Sync_Status__c</field>
        <literalValue>Synchronised</literalValue>
        <name>Set Role Sync Status Synchronised</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Send_Prospect_Notification_Flag</fullName>
        <field>Send_Prospect_Created_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Set Send Prospect Notification Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Type_Customer</fullName>
        <description>Sets the Account Type to Customer</description>
        <field>Type</field>
        <literalValue>Customer</literalValue>
        <name>Set Type Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Type_Prospect</fullName>
        <description>Sets Account type to Prospect</description>
        <field>Type</field>
        <literalValue>Prospect</literalValue>
        <name>Set Type Prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Type_to_Non_Prospect</fullName>
        <field>Type</field>
        <literalValue>Non Prospect</literalValue>
        <name>Set Type to Non Prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_on_risk</fullName>
        <field>On_Risk__c</field>
        <literalValue>1</literalValue>
        <name>Set on risk?</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sync_Account_Sync_Status</fullName>
        <field>Synchronisation_Status__c</field>
        <literalValue>Synchronised</literalValue>
        <name>Sync Account Sync Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Account_Inactivate_Request</fullName>
        <field>Inactivate_Company_Request__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Account Inactivate Request</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Bank_Flag</fullName>
        <field>Role_Bank__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Bank Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Broker_Flag</fullName>
        <field>Role_Broker__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Broker Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Correspondent_Flag</fullName>
        <field>Role_Correspondent__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Correspondent Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_ESP_Flag</fullName>
        <field>Role_ESP__c</field>
        <literalValue>0</literalValue>
        <name>UnSet ESP Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Insurance_Company_Flag</fullName>
        <field>Role_Insurance_Company__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Insurance Company Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Investments_Flag</fullName>
        <field>Role_Investments__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Investments Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Other_Flag</fullName>
        <field>Role_Other__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Other Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Reinsurance_Broker_Flag</fullName>
        <field>Role_Reinsurance_Broker__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Reinsurance Broker Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Reinsurance_Provider_Flag</fullName>
        <field>Role_Reinsurance_Provider__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Reinsurance Provider Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Role_Client_Flag</fullName>
        <field>Role_Client__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Role Client Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Role_Investments_Flag</fullName>
        <field>Role_Investments__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Role Investments Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Role_Other_Rating_Company</fullName>
        <field>Role_Other_Rating_Company__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Role - Other Rating Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UnSet_Send_Prospect_Notification</fullName>
        <field>Send_Prospect_Created_Notification__c</field>
        <literalValue>0</literalValue>
        <name>UnSet Send Prospect Notification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_IsRejected</fullName>
        <description>Uncheck the IsRejected checkbox</description>
        <field>IsRejected__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck IsRejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unlock_Account_for_Approval</fullName>
        <field>IsLocked__c</field>
        <literalValue>0</literalValue>
        <name>Unlock Account for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unset_Awaiting_Approval</fullName>
        <field>Awaiting_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Unset Awaiting Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unset_Deleted_Flag</fullName>
        <field>Deleted__c</field>
        <literalValue>0</literalValue>
        <name>Unset Deleted Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_City</fullName>
        <field>BillingCity</field>
        <formula>Site</formula>
        <name>Update Billing City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Billing_Country</fullName>
        <field>BillingCountry</field>
        <formula>IF( ISBLANK(BillingCountry ) , Country__r.Country_Name__c , BillingCountry )</formula>
        <name>Update Billing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Client</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_City</fullName>
        <field>ShippingCity</field>
        <formula>Site</formula>
        <name>Update Shipping City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_Country</fullName>
        <field>ShippingCountry</field>
        <formula>IF( ISBLANK(ShippingCountry ) , Country__r.Country_Name__c , ShippingCountry )</formula>
        <name>Update Shipping Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Synchronisation_Status</fullName>
        <description>Update Synchronisation Status (Role)</description>
        <field>Roles_Sync_Status__c</field>
        <literalValue>Synchronised</literalValue>
        <name>Update Synchronisation Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>final_approval_status</fullName>
        <field>Request_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>final approval status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>isChanged</fullName>
        <field>IsChanged__c</field>
        <literalValue>0</literalValue>
        <name>isChanged</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Create_DM_folder</fullName>
        <apiVersion>21.0</apiVersion>
        <endpointUrl>https://soa.gard.no/createdmfolder</endpointUrl>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>eDocs_UID__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>tor.dahl@gard.org</integrationUser>
        <name>Create DM folder</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>1%2E0 Set Correspondent Owner</fullName>
        <actions>
            <name>Set_Owner_to_VP_Claims_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c  &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c  &amp;&amp; INCLUDES( Company_Role__c, &apos;Correspondent&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>1%2E0 Set Correspondent Owner v2</fullName>
        <actions>
            <name>Set_Owner_to_VP_Claims_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c  &amp;&amp; INCLUDES( Company_Role__c, &apos;Correspondent&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>3%2E1 Set Reinsurance Broker Owner</fullName>
        <actions>
            <name>Set_Owner_to_MD_of_Lingard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c  &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c  &amp;&amp; INCLUDES( Company_Role__c, &apos;Broker&apos;) &amp;&amp; NOT( INCLUDES( Company_Role__c, &apos;Correspondent&apos;) || INCLUDES( Company_Role__c, &apos;Client&apos;) ) &amp;&amp; INCLUDES( Sub_Roles__c, &apos;Broker - Reinsurance Broker&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>3%2E1 Set Reinsurance Broker Owner v2</fullName>
        <actions>
            <name>Set_Owner_to_MD_of_Lingard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>to work without governance</description>
        <formula>$Setup.DQ__c.Enable_Workflow__c   &amp;&amp; INCLUDES( Company_Role__c, &apos;Broker&apos;) &amp;&amp; NOT( INCLUDES( Company_Role__c, &apos;Correspondent&apos;) || INCLUDES( Company_Role__c, &apos;Client&apos;) ) &amp;&amp; INCLUDES( Sub_Roles__c, &apos;Broker - Reinsurance Broker&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>4%2E1 Set Reinsurance Provider Owner</fullName>
        <actions>
            <name>Set_Owner_to_MD_of_Lingard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c &amp;&amp; INCLUDES( Company_Role__c, &apos;Insurance Company&apos;) &amp;&amp; NOT( INCLUDES( Company_Role__c, &apos;Correspondent&apos;) || INCLUDES( Company_Role__c, &apos;Client&apos;) || INCLUDES( Company_Role__c, &apos;Broker&apos;) ) &amp;&amp; INCLUDES( Sub_Roles__c, &apos;Insurance Company - Reinsurance Provider&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>4%2E2 Set Insurance Company Owner</fullName>
        <actions>
            <name>Set_Owner_to_VP_UWR_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c &amp;&amp; INCLUDES( Company_Role__c, &apos;Insurance Company&apos;) &amp;&amp; NOT( INCLUDES( Company_Role__c, &apos;Correspondent&apos;) || INCLUDES( Company_Role__c, &apos;Client&apos;) || INCLUDES( Company_Role__c, &apos;Broker&apos;) ||  INCLUDES( Sub_Roles__c, &apos;Insurance Company - Reinsurance Provider&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>4%2E2 Set Insurance Company Owner v2</fullName>
        <actions>
            <name>Set_Owner_to_VP_UWR_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; INCLUDES( Company_Role__c, &apos;Insurance Company&apos;) &amp;&amp; NOT( INCLUDES( Company_Role__c, &apos;Correspondent&apos;) || INCLUDES( Company_Role__c, &apos;Client&apos;) || INCLUDES( Company_Role__c, &apos;Broker&apos;) ||  INCLUDES( Sub_Roles__c, &apos;Insurance Company - Reinsurance Provider&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>5%2E Set ESP Owner</fullName>
        <actions>
            <name>Set_Owner_to_VP_Claims_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Deactivated on 8/9/18.Not required since we have now version 2.</description>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c &amp;&amp; INCLUDES( Company_Role__c, &apos;External Service Provider&apos;) &amp;&amp; NOT( INCLUDES( Company_Role__c, &apos;Correspondent&apos;) || INCLUDES( Company_Role__c, &apos;Client&apos;) || INCLUDES( Company_Role__c, &apos;Broker&apos;) ||  INCLUDES( Company_Role__c, &apos;Insurance Company&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>5%2E Set ESP Owner v2</fullName>
        <actions>
            <name>Set_Owner_to_VP_Claims_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>deactivated for SF4272</description>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; INCLUDES( Company_Role__c, &apos;External Service Provider&apos;) &amp;&amp; NOT( INCLUDES( Company_Role__c, &apos;Correspondent&apos;) || INCLUDES( Company_Role__c, &apos;Client&apos;) || INCLUDES( Company_Role__c, &apos;Broker&apos;) ||  INCLUDES( Company_Role__c, &apos;Insurance Company&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>6%2E1 Set Bank Guarantee Provider Owner</fullName>
        <actions>
            <name>Set_Owner_to_VP_Claims_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c &amp;&amp;  INCLUDES(Company_Role__c, &apos;Bank&apos;)  &amp;&amp;  NOT(  INCLUDES( Company_Role__c, &apos;Correspondent&apos;) ||  INCLUDES( Company_Role__c, &apos;Client&apos;) ||  INCLUDES( Company_Role__c, &apos;Broker&apos;) ||  INCLUDES( Company_Role__c, &apos;Insurance Company&apos;) || INCLUDES( Company_Role__c, &apos;External Service Provider&apos;)  )  &amp;&amp;  INCLUDES(Sub_Roles__c, &apos;Bank - Guarantee Provider&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>6%2E2 Set Bank Mortgagee Owner</fullName>
        <actions>
            <name>Set_Owner_to_Mgr_UWR_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c &amp;&amp;  INCLUDES(Company_Role__c, &apos;Bank&apos;) &amp;&amp;  NOT(  INCLUDES( Company_Role__c, &apos;Correspondent&apos;) ||  INCLUDES( Company_Role__c, &apos;Client&apos;) ||  INCLUDES( Company_Role__c, &apos;Broker&apos;) ||  INCLUDES( Company_Role__c, &apos;Insurance Company&apos;) || INCLUDES( Company_Role__c, &apos;External Service Provider&apos;) || INCLUDES(Sub_Roles__c, &apos;Bank - Guarantee Provider&apos;) ) &amp;&amp; INCLUDES(Sub_Roles__c, &apos;Bank - Mortgagee&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>6%2E3 Set Bank Owner</fullName>
        <actions>
            <name>Set_Owner_to_Sen_Mgr_Accounting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c &amp;&amp;  INCLUDES(Company_Role__c, &apos;Bank&apos;)  &amp;&amp;  NOT(  INCLUDES( Company_Role__c, &apos;Correspondent&apos;) ||  INCLUDES( Company_Role__c, &apos;Client&apos;) ||  INCLUDES( Company_Role__c, &apos;Broker&apos;) ||  INCLUDES( Company_Role__c, &apos;Insurance Company&apos;) ||  INCLUDES(Sub_Roles__c, &apos;Bank - Guarantee Provider&apos;) || INCLUDES(Sub_Roles__c, &apos;Bank - Mortgagee&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>6%2E3 Set Bank Owner v2</fullName>
        <actions>
            <name>Set_Owner_to_Sen_Mgr_Accounting</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c  &amp;&amp;  INCLUDES(Company_Role__c, &apos;Bank&apos;)  &amp;&amp;  NOT(  INCLUDES( Company_Role__c, &apos;Correspondent&apos;) ||  INCLUDES( Company_Role__c, &apos;Client&apos;) ||  INCLUDES( Company_Role__c, &apos;Broker&apos;) ||  INCLUDES( Company_Role__c, &apos;Insurance Company&apos;) ||  INCLUDES(Sub_Roles__c, &apos;Bank - Guarantee Provider&apos;) || INCLUDES(Sub_Roles__c, &apos;Bank - Mortgagee&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>7%2E1 Set Guarantee Provider Owner</fullName>
        <actions>
            <name>Set_Owner_to_VP_Claims_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c &amp;&amp;  INCLUDES(Company_Role__c, &apos;Other&apos;)  &amp;&amp;  NOT(  INCLUDES( Company_Role__c, &apos;Correspondent&apos;) ||  INCLUDES( Company_Role__c, &apos;Client&apos;) ||  INCLUDES( Company_Role__c, &apos;Broker&apos;) ||  INCLUDES(Company_Role__c, &apos;Insurance Company&apos;) || INCLUDES(Company_Role__c, &apos;External Service Provider&apos;) || INCLUDES(Company_Role__c, &apos;Bank&apos;)  )  &amp;&amp;  (  INCLUDES(Sub_Roles__c, &apos;Other - Guarantee Provider&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>7%2E1 Set Other Claims or Guarantee Provider Owner</fullName>
        <actions>
            <name>Set_Owner_to_VP_Claims_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c &amp;&amp;  INCLUDES(Company_Role__c, &apos;Other&apos;)  &amp;&amp;  NOT(  INCLUDES( Company_Role__c, &apos;Correspondent&apos;) ||  INCLUDES( Company_Role__c, &apos;Client&apos;) ||  INCLUDES( Company_Role__c, &apos;Broker&apos;) ||  INCLUDES(Company_Role__c, &apos;Insurance Company&apos;) || INCLUDES(Company_Role__c, &apos;External Service Provider&apos;) || INCLUDES(Company_Role__c, &apos;Bank&apos;)  )  &amp;&amp;  ( INCLUDES(Sub_Roles__c, &apos;Other - Claims&apos;) ||  INCLUDES(Sub_Roles__c, &apos;Other - Guarantee Provider&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>7%2E2 Set Other UWR Industry COFR Owner</fullName>
        <actions>
            <name>Set_Owner_to_VP_UWR_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>RCallear 09 04 2014 - removed INCLUDES(Sub_Roles__c, &apos;Other - Underwriting&apos;) ||   from inclusion</description>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c &amp;&amp; INCLUDES(Company_Role__c, &apos;Other&apos;)  &amp;&amp; NOT( INCLUDES(Company_Role__c, &apos;Correspondent&apos;) || INCLUDES(Company_Role__c, &apos;Client&apos;) || INCLUDES(Company_Role__c, &apos;Broker&apos;) || INCLUDES(Company_Role__c, &apos;Insurance Company&apos;) || INCLUDES(Company_Role__c, &apos;External Service Provider&apos;) || INCLUDES(Company_Role__c, &apos;Bank&apos;) || INCLUDES(Sub_Roles__c, &apos;Other - Claims&apos;) ||  INCLUDES(Sub_Roles__c, &apos;Other - Guarantee Provider&apos;) ) &amp;&amp; ( INCLUDES(Sub_Roles__c, &apos;Other - Industry Association&apos;) || INCLUDES(Sub_Roles__c, &apos;Other - COFR Guarantor&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>7%2E4 Set Other Maritime Owner</fullName>
        <actions>
            <name>Set_Owner_to_VP_UWR_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c &amp;&amp; INCLUDES(Company_Role__c, &apos;Other&apos;)  &amp;&amp; NOT( INCLUDES(Company_Role__c, &apos;Correspondent&apos;) || INCLUDES(Company_Role__c, &apos;Client&apos;) || INCLUDES(Company_Role__c, &apos;Broker&apos;) || INCLUDES(Company_Role__c, &apos;Insurance Company&apos;) || INCLUDES(Company_Role__c, &apos;External Service Provider&apos;) || INCLUDES(Company_Role__c, &apos;Bank&apos;) || INCLUDES(Sub_Roles__c, &apos;Other - Claims&apos;) ||  INCLUDES(Sub_Roles__c, &apos;Other - Guarantee Provider&apos;) || INCLUDES(Sub_Roles__c, &apos;Other - Underwriting&apos;) ||  INCLUDES(Sub_Roles__c, &apos;Other - Industry Association&apos;) || INCLUDES(Sub_Roles__c, &apos;Other - COFR Guarantor&apos;) || INCLUDES(Sub_Roles__c, &apos;Other - Ship Operating Company&apos;) ) &amp;&amp; ( INCLUDES(Sub_Roles__c, &apos;Other - Maritime Authority&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>7%2E5 Set Other Mortgagee Society Owner</fullName>
        <actions>
            <name>Set_Owner_to_Mgr_UWR_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c  &amp;&amp; INCLUDES(Company_Role__c, &apos;Other&apos;)  &amp;&amp; NOT( INCLUDES(Company_Role__c, &apos;Correspondent&apos;) || INCLUDES(Company_Role__c, &apos;Client&apos;) || INCLUDES(Company_Role__c, &apos;Broker&apos;) || INCLUDES(Company_Role__c, &apos;Insurance Company&apos;) || INCLUDES(Company_Role__c, &apos;External Service Provider&apos;) || INCLUDES(Company_Role__c, &apos;Bank&apos;) || INCLUDES(Sub_Roles__c, &apos;Other - Claims&apos;) ||  INCLUDES(Sub_Roles__c, &apos;Other - Guarantee Provider&apos;) || INCLUDES(Sub_Roles__c, &apos;Other - Underwriting&apos;) ||  INCLUDES(Sub_Roles__c, &apos;Other - Industry Association&apos;) || INCLUDES(Sub_Roles__c, &apos;Other - COFR Guarantor&apos;) || INCLUDES(Sub_Roles__c, &apos;Other - Ship Operating Company&apos;) || INCLUDES(Sub_Roles__c, &apos;Other - Maritime Authority&apos;) ) &amp;&amp; ( INCLUDES(Sub_Roles__c, &apos;Other - Mortgagee&apos;) || INCLUDES(Sub_Roles__c, &apos;Other - Society&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>7%2E6 Set Other Rating Company Owner</fullName>
        <actions>
            <name>Set_Owner_to_MD_of_Lingard</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>MG 16/06/2014 MDM Phase 2 Changes</description>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c &amp;&amp;  INCLUDES( Company_Role__c, &apos;Other&apos;) &amp;&amp;  NOT(   INCLUDES( Company_Role__c, &apos;Broker&apos;) || INCLUDES( Company_Role__c, &apos;Client&apos;) ||  INCLUDES( Company_Role__c, &apos;Correspondent&apos;) ||  INCLUDES( Company_Role__c, &apos;External Service Provider&apos;) ||  INCLUDES( Company_Role__c, &apos;Insurance Company&apos;) || INCLUDES( Company_Role__c, &apos;Bank&apos;) ||   INCLUDES( Sub_Roles__c, &apos;Other - Claims&apos;) ||  INCLUDES( Sub_Roles__c, &apos;Other - COFR Guarantor&apos;) ||  INCLUDES( Sub_Roles__c, &apos;Other - Guarantee Provider&apos;)||  INCLUDES( Sub_Roles__c, &apos;Other - Industry Association&apos;)||  INCLUDES( Sub_Roles__c, &apos;Other - Maritime Authority&apos;)||  INCLUDES( Sub_Roles__c, &apos;Other - Mortgagee&apos;)||  INCLUDES( Sub_Roles__c, &apos;Other - Ship Operating Company&apos;)||  INCLUDES( Sub_Roles__c, &apos;Other - Society&apos;)||  INCLUDES( Sub_Roles__c, &apos;Other - Underwriting&apos;) )  &amp;&amp; INCLUDES( Sub_Roles__c, &apos;Other - Rating Company&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Name Changed</fullName>
        <actions>
            <name>Account_Name_Changed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>$User.Id &lt;&gt; Owner.Id &amp;&amp; $Setup.DQ__c.Enable_Workflow__c &amp;&amp; $Setup.DQ__c.Enable_Role_Governance__c &amp;&amp;  ISCHANGED( Name ) &amp;&amp; ( INCLUDES( Company_Role__c, &apos;Client&apos; ) || INCLUDES( Company_Role__c, &apos;Broker&apos; ) || INCLUDES( Company_Role__c, &apos;Insurance Company&apos; ) || INCLUDES( Company_Role__c, &apos;External Service Provider&apos; ) || INCLUDES( Company_Role__c, &apos;Bank&apos; ) || INCLUDES( Company_Role__c, &apos;Other&apos; ) || INCLUDES( Company_Role__c, &apos;Correspondent&apos; ) || INCLUDES( Company_Role__c, &apos;Investments&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Account Name Changed v1%2E1</fullName>
        <actions>
            <name>Account_Name_Changed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>$User.Id &lt;&gt; Owner.Id &amp;&amp; $Setup.DQ__c.Enable_Workflow__c &amp;&amp;  ISCHANGED( Name ) &amp;&amp; ( INCLUDES( Company_Role__c, &apos;Client&apos; ) || INCLUDES( Company_Role__c, &apos;Broker&apos; ) || INCLUDES( Company_Role__c, &apos;Insurance Company&apos; ) || INCLUDES( Company_Role__c, &apos;External Service Provider&apos; ) || INCLUDES( Company_Role__c, &apos;Bank&apos; ) || INCLUDES( Company_Role__c, &apos;Other&apos; ) || INCLUDES( Company_Role__c, &apos;Correspondent&apos; ) || INCLUDES( Company_Role__c, &apos;Investments&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Company Information</fullName>
        <actions>
            <name>Change_Company_Information</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Change_Ischanged_To_false</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.IsChanged__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>An email is sent to the Account Owner when any change to the company address is made through extranet</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type To Bank</fullName>
        <actions>
            <name>Record_Type_Updated_To_Bank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(5 AND 6) AND (3 AND 4) AND (1 AND 2) AND (7 AND 8) AND (9 AND 10) AND (11 OR 12)</booleanFilter>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Broker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Broker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Correspondent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Correspondent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>External Service Provider</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>External Service Provider</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Insurance Company</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Insurance Company</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>includes</operation>
            <value>Bank</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>includes</operation>
            <value>Bank</value>
        </criteriaItems>
        <description>Change Record Type according to the Company Role - Bank</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type To Broker</fullName>
        <actions>
            <name>Record_Type_Updated_To_Broker</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND (3 AND 4)</booleanFilter>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>includes</operation>
            <value>Broker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>includes</operation>
            <value>Broker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Client</value>
        </criteriaItems>
        <description>Change Record Type according to the Company Role - Broker</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type To Client</fullName>
        <actions>
            <name>Record_Type_Update_Client</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2)</booleanFilter>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>includes</operation>
            <value>Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>includes</operation>
            <value>Client</value>
        </criteriaItems>
        <description>Change Record Type according to the Company Role - Client</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type To ESP</fullName>
        <actions>
            <name>Record_Type_Updated_To_ESP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(5 AND 6) AND (3 AND 4) AND (1 AND 2) AND (7 OR 8)</booleanFilter>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Broker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Broker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Correspondent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Correspondent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>includes</operation>
            <value>External Service Provider</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>includes</operation>
            <value>External Service Provider</value>
        </criteriaItems>
        <description>Change Record Type according to the Company Role - ESP</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type To Insurance Company</fullName>
        <actions>
            <name>Record_Type_Updated_To_Insurance_Company</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(5 AND 6) AND (3 AND 4) AND (1 AND 2) AND (7 AND 8) AND (9 OR 10)</booleanFilter>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Broker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Broker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Correspondent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Correspondent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>External Service Provider</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>External Service Provider</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>includes</operation>
            <value>Insurance Company</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>includes</operation>
            <value>Insurance Company</value>
        </criteriaItems>
        <description>Change Record Type according to the Company Role - Insurance Company</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type To Other</fullName>
        <actions>
            <name>Record_Type_Updated_To_Other</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(5 AND 6) AND (3 AND 4) AND (1 AND 2) AND (7 AND 8) AND (9 AND 10)  AND (11 AND 12) AND (15 AND 16) AND (13 OR 14)</booleanFilter>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Broker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Broker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Correspondent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Correspondent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>External Service Provider</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>External Service Provider</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Insurance Company</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Insurance Company</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Bank</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Bank</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>includes</operation>
            <value>Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>includes</operation>
            <value>Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>excludes</operation>
            <value>Investments</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Unapproved_Company_Roles__c</field>
            <operation>excludes</operation>
            <value>Investments</value>
        </criteriaItems>
        <description>Change Record Type according to the Company Role - Other</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Clear member approved date</fullName>
        <actions>
            <name>Clear_member_approved_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Clears the PI member approved date when the member status is changed from Approved</description>
        <formula>AND(TEXT(PRIORVALUE(Membership_Status__c))=&apos;Approved&apos;,NOT(TEXT(Membership_Status__c)=&apos;Approved&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create DM folder OLD</fullName>
        <actions>
            <name>Set_DM_Folder_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Create_DM_folder</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.eDocs_UID__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Client</value>
        </criteriaItems>
        <description>NOT USED ANYMORE. Create DM folder</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create or Update DM folder</fullName>
        <actions>
            <name>Set_DM_Folder_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Create_DM_folder</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <description>NOT USED ANYMORE. Create or update DM folder

Criteria changed from ( ISCHANGED (Name)  ||  ISNEW() ) &amp;&amp;  ( RecordTypeId  = &apos;01220000000U0DM&apos;)</description>
        <formula>( ISCHANGED (Name)  ||  ISNEW() || ISCHANGED (Company_Role__c)) &amp;&amp;  INCLUDES (Company_Role__c, &quot;Client&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ESP Created</fullName>
        <actions>
            <name>ESP_created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.Role_ESP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_Correspondent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ESP Risk assessment set HIGH</fullName>
        <actions>
            <name>Risk_assessment_HIGH</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Account.TI__c</field>
            <operation>lessOrEqual</operation>
            <value>24</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_ESP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_Correspondent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_Client__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_Broker__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.TI__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ESP Risk assessment set LOW</fullName>
        <actions>
            <name>Risk_assessment_LOW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Account.TI__c</field>
            <operation>greaterOrEqual</operation>
            <value>51</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_ESP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_Correspondent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_Client__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_Broker__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ESP Risk assessment set MEDIUM</fullName>
        <actions>
            <name>Risk_assessment_MEDIUM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3) AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Account.TI__c</field>
            <operation>lessOrEqual</operation>
            <value>50</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_ESP__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_Correspondent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_Client__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_Broker__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.TI__c</field>
            <operation>greaterThan</operation>
            <value>25</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Inactive Company Status</fullName>
        <actions>
            <name>Company_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8 AND 9</booleanFilter>
        <criteriaItems>
            <field>Account.IsRejected__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>notEqual</operation>
            <value>Broker</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>notEqual</operation>
            <value>Client</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>notEqual</operation>
            <value>Correspondent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>notEqual</operation>
            <value>External Service Provider</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>notEqual</operation>
            <value>Insurance Company</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>notEqual</operation>
            <value>Bank</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>notEqual</operation>
            <value>Investments</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Company_Role__c</field>
            <operation>notEqual</operation>
            <value>Other</value>
        </criteriaItems>
        <description>Inactive Company status only when Company has no role</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Licence pending</fullName>
        <actions>
            <name>Follow_up_collection_of_license_details</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Licensed__c</field>
            <operation>equals</operation>
            <value>Pending</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MDM Set Billing Address Sync Status</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Billing_Address_Sync_Status__c</field>
            <operation>equals</operation>
            <value>Waiting To Sync</value>
        </criteriaItems>
        <description>Timebased WF to sync outside of @Future or Batch</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MDM_Set_Billing_Address_Sync_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>MDM_Set_Billing_Address_Sync_In_Progress</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Sync_Billing_Address_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MDM Set Shipping Address Sync Status</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Shipping_Address_Sync_Status__c</field>
            <operation>equals</operation>
            <value>Waiting To Sync</value>
        </criteriaItems>
        <description>Timebased WF to sync outside of @Future or Batch</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MDM_Set_ShippingAddress_Sync_In_Progress</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>MDM_Set_Shipping_Address_Sync_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Sync_Shipping_Address_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>MDM Set Sync Status</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Synchronisation_Status__c</field>
            <operation>equals</operation>
            <value>Waiting To Sync</value>
        </criteriaItems>
        <description>Timebased WF to sync outside of @Future or Batch</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>MDM_Set_Sync_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>MDM_Set_Sync_Status_In_Progress</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Sync_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Member Expires</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.P_I_Member_Ever__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Membership_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Role_Client__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Client_On_Risk_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>1 3 2019 this description adedd
see https://um1.salesforce.com/01QD0000000UOU9
it is already implemented to cover when the client come on risk, not only when the P&amp;I members is on</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Membership_Expired_Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Set_Approved_date_to_null</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_Member_status_to_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Set_Previous_Approved_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Membership_Approval_Expiry_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>New Customer workflow</fullName>
        <actions>
            <name>Set_Previous_True</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Type_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change in field status when a Client goes on risk for the first time
RCallear 24 02 14 - removed &quot;On_Risk__c ||&quot;</description>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; INCLUDES( Company_Role__c , &apos;Client&apos;) &amp;&amp;  (  Client_On_Risk_Flag__c || Child_Company_On_Risk__c) &amp;&amp;  NOT(Previous_or_Currently_Insured_by_Gard__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New biz%2FUpsell P%26I close WON Notification to Head claims operation</fullName>
        <actions>
            <name>New_biz_Upsell_P_I_close_WON_Notification_to_Head_claims_operationv2</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Disable_email_notif_not_to_Head_Claims</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.P_I_Member_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.New_biz_Upsell_P_I_close_WON_Notificatio__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Emails Head of P&amp;I Claims operation when P&amp;I opportunity is closed WON and the risk is in GIC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Outbound Sync Failure Notification - Account</fullName>
        <actions>
            <name>Outbound_Sync_Failure_Alert_Account</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Synchronisation_Status__c</field>
            <operation>equals</operation>
            <value>Sync Failed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>P%26I Member Flag</fullName>
        <actions>
            <name>P_I_Member_Ever_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Client_On_Risk_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>After KYC is not only for P&amp;I but also for All clients</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Prospect Client Created</fullName>
        <actions>
            <name>New_Prospect_Company_Created</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>UnSet_Send_Prospect_Notification</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;  $Setup.DQ__c.Enable_Role_Governance__c  &amp;&amp;   Send_Prospect_Created_Notification__c &amp;&amp;  Send_Prospect_Created_Notification_Calc__c</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Reactivate Company</fullName>
        <actions>
            <name>Unset_Deleted_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Deleted__c, Account_Sync_Status__c =&apos;Synchronised&apos;, ISCHANGED( Company_Status__c ),  ISPICKVAL(Company_Status__c , &apos;Active&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Remove On Risk flag</fullName>
        <actions>
            <name>Remove_on_risk</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Client_On_Risk_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Broker_On_Risk_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reset to Customer</fullName>
        <actions>
            <name>Set_Type_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change in field status when a Client goes back on-risk
RCallear 24 02 14 - removed &quot; On_Risk__c || &quot;  old flag</description>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; INCLUDES( Company_Role__c , &apos;Client&apos;) &amp;&amp; (Client_On_Risk_Flag__c || Child_Company_On_Risk__c) &amp;&amp; Previous_or_Currently_Insured_by_Gard__c   &amp;&amp;  NOT( Non_Prospect__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Reset to Prospect</fullName>
        <actions>
            <name>Set_Type_Prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Change in field status when a Client goes off risk
RCallear 24 02 14 - removed &quot; On_Risk__c || &quot;  old flag</description>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; INCLUDES( Company_Role__c , &apos;Client&apos;) &amp;&amp;  NOT( Client_On_Risk_Flag__c ||  Child_Company_On_Risk__c) &amp;&amp;  NOT( Non_Prospect__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Account Billing Address City</fullName>
        <actions>
            <name>Update_Billing_City</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingStreet</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCity</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Account Billing Address Country</fullName>
        <actions>
            <name>Update_Billing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingStreet</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Account Shipping Address City</fullName>
        <actions>
            <name>Update_Shipping_City</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ShippingStreet</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingCity</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Account Shipping Address Country</fullName>
        <actions>
            <name>Update_Shipping_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.ShippingStreet</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.ShippingCountry</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for AMEL</fullName>
        <actions>
            <name>Set_LairdIain_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Market_Area_Description__c</field>
            <operation>equals</operation>
            <value>&quot;Americas, Middle East and London&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Area_Manager__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Setting area manager when marketing area is &apos;Americas, Middle East and London&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for Asia</fullName>
        <actions>
            <name>Set_Lock_Sid_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Market_Area_Description__c</field>
            <operation>equals</operation>
            <value>Asia</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Area_Manager__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Setting area manager when marketing area is &apos;Asia</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for Charterers and Traders</fullName>
        <actions>
            <name>Set_Jay_Ter_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Market_Area_Description__c</field>
            <operation>equals</operation>
            <value>Charterers &amp; Traders</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Area_Manager__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Setting area manager when marketing area is &apos;Charterers and Traders</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for Energy</fullName>
        <actions>
            <name>Set_AasbergGunnar_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Market_Area_Description__c</field>
            <operation>equals</operation>
            <value>Energy</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Area_Manager__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Setting area manager when marketing area is &apos;Energy&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for Marine Builders Risks</fullName>
        <actions>
            <name>Set_KnutMortenFinckenhagen_as_AM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Market_Area_Description__c</field>
            <operation>equals</operation>
            <value>Marine Builders Risks</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Area_Manager__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Setting area manager when marketing area is &apos;Marine Builders Risks&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for Nordic and North America</fullName>
        <actions>
            <name>Set_GoderstadKnut_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Market_Area_Description__c</field>
            <operation>equals</operation>
            <value>Nordic and North America</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Country_Code__c</field>
            <operation>notEqual</operation>
            <value>USA,CAN,BMU,BHS,CRI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Area_Manager__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Setting area manager when marketing area is &apos;Nordic and North America&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for Nordic and North America and country code USA%2CCAN%2CBMU%2CBHS%2CCRI</fullName>
        <actions>
            <name>Set_OlsenEspen_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Market_Area_Description__c</field>
            <operation>equals</operation>
            <value>Nordic and North America</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Country_Code__c</field>
            <operation>equals</operation>
            <value>USA,CAN,BMU,BHS,CRI</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Area_Manager__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Setting area manager when marketing area is &apos;Nordic and North America&apos; and country code is USA,CAN,BMU,BHS,CRI</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for Northern Europe</fullName>
        <actions>
            <name>Set_FremmerlidBjorn_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Market_Area_Description__c</field>
            <operation>equals</operation>
            <value>Northern Europe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Area_Manager__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Setting area manager when marketing area is &apos;NE</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for Offshore</fullName>
        <actions>
            <name>Set_FurnesTore_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Market_Area_Description__c</field>
            <operation>equals</operation>
            <value>Offshore</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Area_Manager__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Setting area manager when marketing area is &apos;Offshore&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Approver for Southern Europe</fullName>
        <actions>
            <name>Set_MertensBart_as_AM_approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Market_Area_Description__c</field>
            <operation>equals</operation>
            <value>Southern Europe</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Area_Manager__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Setting area manager when marketing area is &apos;SE&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Bank Flag</fullName>
        <actions>
            <name>Set_Bank_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   INCLUDES(Company_Role__c, &apos;Bank&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Broker Flag</fullName>
        <actions>
            <name>Set_Broker_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   INCLUDES(Company_Role__c, &apos;Broker&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Client Flag</fullName>
        <actions>
            <name>Inform_Area_Manager_of_New_Client</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Role_Client_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>INCLUDES(Company_Role__c  , &apos;Client&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Client Flag v2</fullName>
        <actions>
            <name>Set_Role_Client_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>V2 without email notification to AM, because it did not work (email were sent when the opp were updated???)</description>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   INCLUDES(Company_Role__c, &apos;Client&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Correspondent Flag</fullName>
        <actions>
            <name>Set_Owner_to_VP_Claims_Operations</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Role_Correspondent_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>INCLUDES(Company_Role__c , &apos;Correspondent&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set ESP Flag</fullName>
        <actions>
            <name>Set_ESP_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;  INCLUDES(Company_Role__c, &apos;External Service Provider&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Inactive Status</fullName>
        <actions>
            <name>Set_Inactive_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(Deleted__c, Account_Sync_Status__c =&apos;Synchronised&apos;, NOT(ISCHANGED( Company_Status__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Insurance Company Flag</fullName>
        <actions>
            <name>Set_Insurance_Company_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   INCLUDES(Company_Role__c, &apos;Insurance Company&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Investments Flag</fullName>
        <actions>
            <name>Set_Investments_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   INCLUDES(Company_Role__c, &apos;Investments&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Lawyer ESP Role</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>External Service Provider</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>equals</operation>
            <value>Lawyer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set On Risk flag</fullName>
        <actions>
            <name>Set_on_risk</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Account.Client_On_Risk_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Broker_On_Risk_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Other Flag</fullName>
        <actions>
            <name>Set_Other_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   INCLUDES(Company_Role__c, &apos;Other&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Other Rating Company Flag</fullName>
        <actions>
            <name>Set_Role_Other_Rating_Company</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   INCLUDES(Company_Role__c, &apos;Other&apos;) &amp;&amp;  INCLUDES( Sub_Roles__c , &apos;Other - Rating Company&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set P%26I Member Flag to True</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.On_Risk__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Membership_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Set P&amp;I Member Flag to True</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set PI member approved date</fullName>
        <actions>
            <name>Set_Approved_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Membership_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Sets the PI member approved date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Reinsurance Broker Flag</fullName>
        <actions>
            <name>Set_Broker_Reinsurance_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Includes( Sub_Roles__c , &apos;Broker - Reinsurance Broker&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Reinsurance Provider Flag</fullName>
        <actions>
            <name>Set_Reinsurance_Provider_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>INCLUDES(Sub_Roles__c  , &apos;Insurance Company - Reinsurance Provider&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Type to Non Prospect</fullName>
        <actions>
            <name>Set_Type_to_Non_Prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Non_Prospect__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UWR main contact set</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Underwriter_main_contact__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>UnSet Bank Flag</fullName>
        <actions>
            <name>UnSet_Bank_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   NOT(INCLUDES(Company_Role__c, &apos;Bank&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnSet Broker Flag</fullName>
        <actions>
            <name>UnSet_Broker_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   NOT(INCLUDES(Company_Role__c, &apos;Broker&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnSet Client Flag</fullName>
        <actions>
            <name>UnSet_Role_Client_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(INCLUDES(Company_Role__c  , &apos;Client&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnSet Correspondent Flag</fullName>
        <actions>
            <name>UnSet_Correspondent_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(INCLUDES(Company_Role__c , &apos;Correspondent&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnSet ESP Flag</fullName>
        <actions>
            <name>UnSet_ESP_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;  NOT(INCLUDES(Company_Role__c, &apos;External Service Provider&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnSet Insurance Company Flag</fullName>
        <actions>
            <name>UnSet_Insurance_Company_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   NOT(INCLUDES(Company_Role__c, &apos;Insurance Company&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnSet Investments Flag</fullName>
        <actions>
            <name>UnSet_Investments_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   NOT(INCLUDES(Company_Role__c, &apos;Investments&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnSet Other Flag</fullName>
        <actions>
            <name>UnSet_Other_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   NOT(INCLUDES(Company_Role__c, &apos;Other&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnSet Other Rating Company Flag</fullName>
        <actions>
            <name>UnSet_Role_Other_Rating_Company</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp;   NOT(INCLUDES(Company_Role__c, &apos;Other&apos;) &amp;&amp; INCLUDES( Sub_Roles__c , &apos;Other - Rating Company&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnSet Reinsurance Broker Flag</fullName>
        <actions>
            <name>UnSet_Reinsurance_Broker_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(Includes( Sub_Roles__c , &apos;Broker - Reinsurance Broker&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UnSet Reinsurance Provider Flag</fullName>
        <actions>
            <name>UnSet_Reinsurance_Provider_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(INCLUDES(Sub_Roles__c  , &apos;Insurance Company - Reinsurance Provider&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Role Sync Status Failed</fullName>
        <actions>
            <name>Set_Role_Sync_Status_Failed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (  OR(   ISCHANGED( Role_Sync_Failure_Count__c ) ,    ISCHANGED( Role_Sync_In_Progress_Count__c )  ),   Role_Sync_Failure_Count__c &gt; 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Role Sync Status In Progress</fullName>
        <actions>
            <name>Set_Role_Sync_Status_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (  OR(   ISCHANGED( Role_Sync_Failure_Count__c ) ,    ISCHANGED( Role_Sync_In_Progress_Count__c )  ),   Role_Sync_Failure_Count__c = 0,  Role_Sync_In_Progress_Count__c &gt; 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Role Sync Status Synchronised</fullName>
        <actions>
            <name>Set_Role_Sync_Status_Synchronised</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (  OR(   ISCHANGED( Role_Sync_Failure_Count__c ) ,    ISCHANGED( Role_Sync_In_Progress_Count__c )  ),   Role_Sync_Failure_Count__c = 0,  Role_Sync_In_Progress_Count__c = 0 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update account Sync Status Synchronised after account deactivation</fullName>
        <actions>
            <name>Sync_Account_Sync_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Ones the account is deactivated, set the account sync status as &apos;synchronized&apos;</description>
        <formula>AND (  OR(   ISCHANGED( Role_Sync_Failure_Count__c ) ,    ISCHANGED( Role_Sync_In_Progress_Count__c )  ),   Role_Sync_Failure_Count__c = 0,  Role_Sync_In_Progress_Count__c = 0 ,ISPICKVAL(Company_Status__c , &apos;Inactive&apos;) , Deleted__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>updateSanctionsChkbx</fullName>
        <actions>
            <name>SanctionsChkBx</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Uncheck the Sanctions checkbox if the company name, location, country or address changes.</description>
        <formula>$Setup.DQ__c.Enable_Workflow__c &amp;&amp; Automatic_Sanctions_Screening__c = true &amp;&amp; (ISCHANGED(Name) || ISCHANGED(Site) || ISCHANGED(Country__c ) || ISCHANGED(Billing_Street_Address_Line_1__c) || ISCHANGED(Billing_Street_Address_Line_2__c) || ISCHANGED(Billing_Street_Address_Line_3__c) || ISCHANGED(Billing_Street_Address_Line_4__c) || ISCHANGED(BillingCity__c) || ISCHANGED(BillingPostalcode__c) || ISCHANGED(BillingState__c) || ISCHANGED(BillingCountry__c) || ISCHANGED(Shipping_Street_Address_Line_1__c) || ISCHANGED(Shipping_Street_Address_Line_2__c) || ISCHANGED(Shipping_Street_Address_Line_3__c) || ISCHANGED(Shipping_Street_Address_Line_4__c) || ISCHANGED(ShippingCity__c) || ISCHANGED(ShippingPostalCode__c) || ISCHANGED(ShippingState__c) || ISCHANGED(ShippingCountry__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Auto_Approval_Successfull</fullName>
        <assignedTo>integration@gard.no</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Auto Approval Successfull</subject>
    </tasks>
    <tasks>
        <fullName>Auto_Approve_Rejected</fullName>
        <assignedTo>integration@gard.no</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Auto Approve Rejected</subject>
    </tasks>
    <tasks>
        <fullName>Follow_up_collection_of_license_details</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Follow up collection of license details</subject>
    </tasks>
</Workflow>
