<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Favorite_Lawyer_Approved_alert</fullName>
        <description>Favorite Lawyer Approved alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Favorite_Lawyer_Approved</template>
    </alerts>
    <alerts>
        <fullName>Favorite_Lawyer_Rejected_alert</fullName>
        <description>Favorite Lawyer Rejected alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/Favorite_Lawyer_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_as_Not_approved</fullName>
        <field>Status__c</field>
        <literalValue>Not approved</literalValue>
        <name>Set as Not approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_as_Under_approval</fullName>
        <field>Status__c</field>
        <literalValue>Under approval</literalValue>
        <name>Set as Under approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_as_approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Set as approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Status set</fullName>
        <actions>
            <name>Set_as_approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Related_company__c.Status__c</field>
            <operation>equals</operation>
            <value>,Ready for approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>Related_company__c.Related_company_subrole__c</field>
            <operation>notContain</operation>
            <value>ESP - Lawyer</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
