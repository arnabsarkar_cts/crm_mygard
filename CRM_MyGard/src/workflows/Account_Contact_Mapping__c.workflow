<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_for_Admin_user</fullName>
        <description>Email Alert set picklist field Adminstrator__c to Admin</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GARD_Templates/MyGard_Set_Admin_Checkbox</template>
    </alerts>
    <alerts>
        <fullName>Email_Alert_for_Normal_user</fullName>
        <description>send mail to previous admin user</description>
        <protected>false</protected>
        <recipients>
            <field>Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>GARD_Templates/MyGard_Reset_Admin_Checkbox</template>
    </alerts>
    <rules>
        <fullName>MyGard_Reset_Contact_Admin</fullName>
        <actions>
            <name>Email_Alert_for_Normal_user</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow fires when the administrator__c picklist is set to Normal</description>
        <formula>AND(NOT(ISNEW()),ISPICKVAL( Administrator__c  , &quot;Normal&quot;),ISCHANGED(Administrator__c), Active__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MyGard_Set_Contact_Admin</fullName>
        <actions>
            <name>Email_Alert_for_Admin_user</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This workflow fires when the administrator__c set to admin</description>
        <formula>AND(NOT(ISNEW()),ISPICKVAL( Administrator__c , &quot;Admin&quot;),ISCHANGED(Administrator__c), Active__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
