<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_PEME_Invoice_approved_to_robot</fullName>
        <description>Notification PEME Invoice approved to robot</description>
        <protected>false</protected>
        <recipients>
            <recipient>helge.christensen@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>robot1@gard.no</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/PEME_invoice_approved_details_to_robot</template>
    </alerts>
    <rules>
        <fullName>PEME invoice approved details to robot</fullName>
        <actions>
            <name>Notification_PEME_Invoice_approved_to_robot</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PEME_Invoice__c.Status__c</field>
            <operation>equals</operation>
            <value>Approved,Partially Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
