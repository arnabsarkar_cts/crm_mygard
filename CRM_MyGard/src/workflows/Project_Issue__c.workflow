<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Project_issue_Prio_to_Low</fullName>
        <field>Priority__c</field>
        <literalValue>3 - Low</literalValue>
        <name>Project issue Prio to Low</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_issue_Prio_to_Medium</fullName>
        <field>Priority__c</field>
        <literalValue>2 - Medium</literalValue>
        <name>Project issue Prio to Medium</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_issue_to_Change_Approved</fullName>
        <field>Issue_Status__c</field>
        <literalValue>Change Approved</literalValue>
        <name>Project issue to Change Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_issue_to_Closed</fullName>
        <field>Issue_Status__c</field>
        <literalValue>Closed</literalValue>
        <name>Project issue to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Project_issue_to_Prio_High</fullName>
        <field>Priority__c</field>
        <literalValue>1 - High</literalValue>
        <name>Project issue to Prio High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>defaultProjectField</fullName>
        <field>Name</field>
        <formula>&apos;a0N20000002bnxD&apos;</formula>
        <name>defaultProjectField</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>Project_Name__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Project issue Prio High</fullName>
        <actions>
            <name>Project_issue_to_Prio_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Project_Issue__c.CAB_priority__c</field>
            <operation>equals</operation>
            <value>2 High,1 Very High</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Project issue Prio Low</fullName>
        <actions>
            <name>Project_issue_Prio_to_Low</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Project_Issue__c.CAB_priority__c</field>
            <operation>equals</operation>
            <value>4 Low,5 Very Low</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Project issue Prio Medium</fullName>
        <actions>
            <name>Project_issue_Prio_to_Medium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Project_Issue__c.CAB_priority__c</field>
            <operation>equals</operation>
            <value>3 Medium</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Project issue approved</fullName>
        <actions>
            <name>Project_issue_to_Change_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Project_Issue__c.CAB_resolution__c</field>
            <operation>equals</operation>
            <value>Approved for implementation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Project issue closed</fullName>
        <actions>
            <name>Project_issue_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Project_Issue__c.CAB_resolution__c</field>
            <operation>equals</operation>
            <value>Not approved for implementation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
