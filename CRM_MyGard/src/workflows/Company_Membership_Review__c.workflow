<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>P_I_Membership_Review_Approved</fullName>
        <description>P&amp;I Membership Review Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/PI_Membership_Approved</template>
    </alerts>
    <alerts>
        <fullName>P_I_Membership_Review_Recalled</fullName>
        <description>P&amp;I Membership Review Recalled</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/PI_Membership_Recalled</template>
    </alerts>
    <alerts>
        <fullName>P_I_Membership_Review_Rejected</fullName>
        <description>P&amp;I Membership Review Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/PI_Membership_Rejected</template>
    </alerts>
    <alerts>
        <fullName>P_I_Membership_review_Approved_alert</fullName>
        <description>P&amp;I Membership review Approved alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/PI_Membership_Approved</template>
    </alerts>
    <alerts>
        <fullName>P_I_Membership_review_Recalled_alert</fullName>
        <description>P&amp;I Membership review Recalled alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/PI_Membership_Recalled</template>
    </alerts>
    <alerts>
        <fullName>P_I_Membership_review_Rejected_alert</fullName>
        <description>P&amp;I Membership review Rejected alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Approval_Templates/PI_Membership_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Record_Type_for_broker_Account</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Broker_KYC_review</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type for broker Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type_for_client_Account</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Client_KYC_review</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type for client Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Approval_in_Pro</fullName>
        <field>Status__c</field>
        <formula>&quot;In Approval Process&quot;</formula>
        <name>Status to In Approval Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Approved</fullName>
        <field>Status__c</field>
        <formula>&quot;Approved&quot;</formula>
        <name>Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Declined</fullName>
        <field>Status__c</field>
        <formula>&quot;Declined&quot;</formula>
        <name>Status to Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Draft</fullName>
        <field>Status__c</field>
        <formula>&quot;Draft&quot;</formula>
        <name>Status to Draft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Approved</fullName>
        <field>Status__c</field>
        <formula>&quot;Approved&quot;</formula>
        <name>Update Status - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Declined</fullName>
        <field>Status__c</field>
        <formula>&quot;Declined&quot;</formula>
        <name>Update Status - Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_In_Review</fullName>
        <field>Status__c</field>
        <formula>&quot;In Review&quot;</formula>
        <name>Update Status - In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Kyc Review Record Type for Broker</fullName>
        <actions>
            <name>Set_Record_Type_for_broker_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the record type of kyc review</description>
        <formula>CONTAINS(Company_Name__r.RecordType.Name,&apos;Broker&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Kyc Review Record Type for Client</fullName>
        <actions>
            <name>Set_Record_Type_for_client_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>CONTAINS(Company_Name__r.RecordType.Name,&apos;Client&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set default approver</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Company_Membership_Review__c.Approval_type_required__c</field>
            <operation>equals</operation>
            <value>&quot;Shipowners, MOU, Charterers P&amp;I approval required&quot;,&quot;Small Craft, Fixed P&amp;I approval required&quot;</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
