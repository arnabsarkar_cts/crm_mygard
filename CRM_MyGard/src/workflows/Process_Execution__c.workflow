<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_integration_failure_to_system_administrators</fullName>
        <ccEmails>_BI_Operations@gard.no</ccEmails>
        <description>Send integration failure to system administrators</description>
        <protected>false</protected>
        <recipients>
            <recipient>nils.petter.nilsen@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>olga.tome@gard.no</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tor.dahl@gard.org</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>System_Administration/Process_Failure_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Set_Process_Execution_Archive_Flag</fullName>
        <field>Archive__c</field>
        <literalValue>1</literalValue>
        <name>Set Process Execution Archive Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Archive Process Executions</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Process_Execution__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Process_Execution_Archive_Flag</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Integration Failure Email</fullName>
        <actions>
            <name>Send_integration_failure_to_system_administrators</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Process_Execution__c.Status__c</field>
            <operation>equals</operation>
            <value>Failed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
