<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Budget_Premium_for_Rollup</fullName>
        <field>Budget_Premium_for_Rollup__c</field>
        <formula>Budget_Premium__c</formula>
        <name>Set Budget Premium for Rollup</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_Product_UnitPrice_to_EPI</fullName>
        <description>Sets the opportunity product unit price to be the Prognosis EPI</description>
        <field>UnitPrice</field>
        <formula>Prognosis_EPI__c</formula>
        <name>Set Opp Product UnitPrice to EPI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_Stage_to_Risk_Evaluation</fullName>
        <description>PCI-000137 change Sales Stage when Prognisis renewal premium or EPI entered</description>
        <field>StageName</field>
        <literalValue>Risk Evaluation</literalValue>
        <name>Set Opp Stage to Risk Evaluation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>OpportunityId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opportunity_Line_Item_Product_Name</fullName>
        <field>Product_Name_Set_By_Workflow__c</field>
        <formula>PricebookEntry.Product2.Name</formula>
        <name>Set Opportunity Line Item Product Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Budget Premium for Rollup</fullName>
        <actions>
            <name>Set_Budget_Premium_for_Rollup</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Budget_Premium__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opp Stage when EPI Changed</fullName>
        <actions>
            <name>Set_Opp_Stage_to_Risk_Evaluation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>PCI-000137 - Workflow on opp prodcut. If one of the fields is entered which will affect the EPI calculation and the parent opp is currently &quot;Renewable opportunity&quot; then change the sales stage to &quot;Risk evaluation&quot;.
MM PCI1502: changed criteria...</description>
        <formula>ISPICKVAL(Opportunity.StageName, &apos;Renewable Opportunity&apos;) &amp;&amp;  ( ISCHANGED( Index__c ) ||  ISCHANGED( UnitPrice ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Opportunity Line Item Product Name</fullName>
        <actions>
            <name>Set_Opportunity_Line_Item_Product_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>OpportunityLineItem.Product_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Sets the text field Product Name on Opportunity Product as existing formula field not available in rollup-summary field criteria, which is used by validation to prevent setting to Closed Won of opportunities with Shipoweners P&amp;I lines but no Tonnage value</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set UnitPrice from EPI</fullName>
        <actions>
            <name>Set_Opp_Product_UnitPrice_to_EPI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Set the standard unit price field from the Prognosis EPI Calculation. Only do so for budget years 2014 onwards.</description>
        <formula>UnitPrice &lt;&gt; Prognosis_EPI__c &amp;&amp; NOT( ISPICKVAL(Opportunity.Budget_Year__c, &apos;2010&apos;) || ISPICKVAL(Opportunity.Budget_Year__c, &apos;2011&apos;) || ISPICKVAL(Opportunity.Budget_Year__c, &apos;2012&apos;) || ISPICKVAL(Opportunity.Budget_Year__c, &apos;2013&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
