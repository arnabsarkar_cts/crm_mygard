<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Expire_Date_Update</fullName>
        <description>This is to update the Expire date of Dis-enrolled one&apos;s.</description>
        <field>DisEnrolled_Expire_Date__c</field>
        <formula>(ADDMONTHS(TODAY(),12) )</formula>
        <name>Expire Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Expire_Date_Update_for_Under_Approval</fullName>
        <field>DisEnrolled_Expire_Date__c</field>
        <name>Expire Date Update for Under Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FUStatusDisenrolled</fullName>
        <field>Enrollment_Status__c</field>
        <literalValue>Not Enrolled</literalValue>
        <name>FUStatusDisenrolled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FUStatusDraft</fullName>
        <description>When process is recalled the status is set draft.</description>
        <field>Enrollment_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>FUStatusDraft</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FUStatusEnrolled</fullName>
        <field>Enrollment_Status__c</field>
        <literalValue>Enrolled</literalValue>
        <name>FUStatusEnrolled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FUStatusUnderApproval</fullName>
        <description>Created for SF-4971, sets the status Under-Approval</description>
        <field>Enrollment_Status__c</field>
        <literalValue>Under Approval</literalValue>
        <name>FUStatusUnderApproval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Expiration date</fullName>
        <actions>
            <name>Expire_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PEME_Enrollment_Form__c.Enrollment_Status__c</field>
            <operation>equals</operation>
            <value>Disenrolled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Expiration date if Under Approval</fullName>
        <actions>
            <name>Expire_Date_Update_for_Under_Approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>PEME_Enrollment_Form__c.Enrollment_Status__c</field>
            <operation>equals</operation>
            <value>Under Approval</value>
        </criteriaItems>
        <criteriaItems>
            <field>PEME_Enrollment_Form__c.Enrollment_Status__c</field>
            <operation>equals</operation>
            <value>Enrolled</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>The_enrollment_is_rejected</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>7</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>The enrollment is rejected</subject>
    </tasks>
</Workflow>
